using System;

namespace Matrix_Lib
{
	internal class MatrixSingularException : ApplicationException
	{
		public MatrixSingularException()
			: base("Matrix is singular this operation cannot continue !")
		{
		}
	}
}
