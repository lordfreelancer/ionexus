using System;

namespace Matrix_Lib
{
	internal class MatrixLibraryExceptions : ApplicationException
	{
		public MatrixLibraryExceptions(string message)
			: base(message)
		{
		}
	}
}
