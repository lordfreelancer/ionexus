﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matrix_Lib
{
    [Serializable]
    public class FormulaMatrix
    {
        private string[,] in_Mat;

        public string this[int Row, int Col]
        {
            get
            {
                if (Row < 0)
                {
                    Row += this.in_Mat.GetLength(0);
                }
                if (Col < 0)
                {
                    Col += this.in_Mat.GetLength(1);
                }
                return this.in_Mat[Row, Col];
            }
            set
            {
                if (Row < 0)
                {
                    Row += this.in_Mat.GetLength(0);
                }
                if (Col < 0)
                {
                    Col += this.in_Mat.GetLength(1);
                }
                this.in_Mat[Row, Col] = value;
            }
        }

        public int NoRows
        {
            get
            {
                return this.in_Mat.GetUpperBound(0) + 1;
            }
            set
            {
                this.in_Mat = new string[value, this.in_Mat.GetUpperBound(0)];
            }
        }

        public int NoCols
        {
            get
            {
                return this.in_Mat.GetUpperBound(1) + 1;
            }
            set
            {
                this.in_Mat = new string[this.in_Mat.GetUpperBound(0), value];
            }
        }

        public string[,] toArray
        {
            get
            {
                return this.in_Mat;
            }
        }

        public FormulaMatrix(int noRows, int noCols)
        {
            this.in_Mat = new string[noRows, noCols];
        }

        public FormulaMatrix(string[,] Mat)
        {
            this.in_Mat = (string[,])Mat.Clone();
        }

        public FormulaMatrix Clone()
        {
            return new FormulaMatrix(this.in_Mat);
        }

    }
}
