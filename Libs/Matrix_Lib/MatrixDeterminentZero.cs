using System;

namespace Matrix_Lib
{
	internal class MatrixDeterminentZero : ApplicationException
	{
		public MatrixDeterminentZero()
			: base("Determinent of matrix equals zero, inverse can't be found !")
		{
		}
	}
}
