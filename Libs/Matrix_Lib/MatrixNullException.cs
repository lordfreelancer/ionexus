using System;

namespace Matrix_Lib
{
	internal class MatrixNullException : ApplicationException
	{
		public MatrixNullException()
			: base("To do this operation, matrix can not be null")
		{
		}
	}
}
