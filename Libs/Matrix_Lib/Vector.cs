using System;

namespace Matrix_Lib
{
	[Serializable]
	public class Vector
	{
		private double[] in_Vect;

		public double this[int Row]
		{
			get
			{
				if (Row < 0)
				{
					Row += this.in_Vect.Length;
				}
				return this.in_Vect[Row];
			}
			set
			{
				if (Row < 0)
				{
					Row += this.in_Vect.Length;
				}
				this.in_Vect[Row] = value;
			}
		}

		public int Count
		{
			get
			{
				return this.in_Vect.Length;
			}
			set
			{
				this.in_Vect = new double[value];
			}
		}

		public double[] toArray
		{
			get
			{
				return this.in_Vect;
			}
		}

		public Vector(int noRows)
		{
			this.in_Vect = new double[noRows];
		}

		public Vector(double[] Vect)
		{
			this.in_Vect = (double[])Vect.Clone();
		}

		public Vector Clone()
		{
			return new Vector(this.in_Vect);
		}

		public Matrix toRowMatrix()
		{
			Matrix matrix = new Matrix(1, this.in_Vect.Length);
			for (int i = 0; i < this.in_Vect.Length; i++)
			{
				matrix[0, i] = this.in_Vect[i];
			}
			return matrix;
		}

		public Matrix toColMatrix()
		{
			return new Matrix(Matrix.OneD_2_TwoD(this.in_Vect));
		}

		public static bool IsEqual(double[] Vect1, double[] Vect2)
		{
			int upperBound;
			int upperBound2;
			try
			{
				upperBound = Vect1.GetUpperBound(0);
				upperBound2 = Vect2.GetUpperBound(0);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (upperBound != upperBound2)
			{
				throw new MatrixDimensionException();
			}
			for (int i = 0; i <= upperBound; i++)
			{
				if (Math.Abs(Vect1[i] - Vect2[i]) > 1E-14)
				{
					return false;
				}
			}
			return true;
		}

		public static bool IsEqual(Vector Vect1, Vector Vect2)
		{
			return Vector.IsEqual(Vect1.in_Vect, Vect2.in_Vect);
		}

		public static bool operator ==(Vector Vect1, Vector Vect2)
		{
			return Vector.IsEqual(Vect1.in_Vect, Vect2.in_Vect);
		}

		public static bool operator !=(Vector Vect1, Vector Vect2)
		{
			return !Vector.IsEqual(Vect1.in_Vect, Vect2.in_Vect);
		}

		public override bool Equals(object obj)
		{
			try
			{
				return this == (Vector)obj;
			}
			catch
			{
				return false;
			}
		}

		public bool Equals(Vector obj)
		{
			if (object.ReferenceEquals(null, obj))
			{
				return false;
			}
			return object.ReferenceEquals(this, obj) || object.Equals(obj.in_Vect, this.in_Vect);
		}

		public static double[] Reciprocal(double[] Vect)
		{
			int upperBound = Vect.GetUpperBound(0);
			double[] array = new double[upperBound];
			for (int i = 0; i < upperBound; i++)
			{
				try
				{
					array[i] = 1.0 / Vect[i];
				}
				catch
				{
					array[i] = double.PositiveInfinity;
				}
			}
			return array;
		}

		public static Vector Reciprocal(Vector Vect)
		{
			return new Vector(Vector.Reciprocal(Vect.in_Vect));
		}

		public static Vector ScalarDivide(Vector Num, Vector Denom)
		{
			if (Num.Count != Denom.Count)
			{
				throw new MatrixDimensionException();
			}
			Vector vector = new Vector(Num.Count);
			for (int i = 0; i < vector.Count; i++)
			{
				try
				{
					vector[i] = Num[i] / Denom[i];
				}
				catch
				{
					vector[i] = double.PositiveInfinity;
				}
			}
			return vector;
		}

		public static Vector ScalarDivide(Vector Vect, double Denom)
		{
			Vector vector = new Vector(Vect.Count);
			for (int i = 0; i < vector.Count; i++)
			{
				vector[i] = Vect[i] / Denom;
			}
			return vector;
		}

		public static Vector operator /(Vector Vect, double Denom)
		{
			return Vector.ScalarDivide(Vect, Denom);
		}

		public static Vector operator /(Vector Vect, Vector Denom)
		{
			return Vector.ScalarDivide(Vect, Denom);
		}

		public static Vector ScalarMultiply(Vector V1, Vector V2)
		{
			if (V1.Count != V2.Count)
			{
				throw new MatrixDimensionException();
			}
			Vector vector = new Vector(V1.Count);
			for (int i = 0; i < vector.Count; i++)
			{
				vector[i] = V1[i] * V2[i];
			}
			return vector;
		}

		public static Vector operator *(Vector V1, Vector V2)
		{
			return Vector.ScalarMultiply(V1, V2);
		}

		public static double sum(Vector vect)
		{
			double num = 0.0;
			for (int i = 0; i < vect.Count; i++)
			{
				num += vect[i];
			}
			return num;
		}

		public static Vector Add(Vector v1, Vector v2)
		{
			if (v1.Count != v2.Count)
			{
				throw new MatrixDimensionException();
			}
			Vector vector = new Vector(v1.Count);
			for (int i = 0; i < v1.Count; i++)
			{
				vector[i] = v1[i] + v2[i];
			}
			return vector;
		}

		public static Vector operator +(Vector v1, Vector v2)
		{
			return Vector.Add(v1, v2);
		}

		public static Vector Add(Vector v1, double d)
		{
			Vector vector = new Vector(v1.Count);
			for (int i = 0; i < v1.Count; i++)
			{
				vector[i] = v1[i] + d;
			}
			return vector;
		}

		public static Vector operator +(Vector v1, double d)
		{
			return Vector.Add(v1, d);
		}

		public void Add(Vector v2)
		{
			if (this.in_Vect.Length != v2.Count)
			{
				throw new MatrixDimensionException();
			}
			for (int i = 0; i < this.in_Vect.Length; i++)
			{
				this.in_Vect[i] += v2[i];
			}
		}

		public void ReDimPreserve(int Elems)
		{
			double[] array = new double[Elems];
			int num = Math.Min(Elems, this.in_Vect.GetUpperBound(0) + 1);
			for (int i = 0; i < num; i++)
			{
				array[i] = this.in_Vect[i];
			}
			this.in_Vect = array;
		}

		public static Vector ReDimPreserve(Vector vect, int Elems)
		{
			double[] array = new double[Elems];
			int num = Math.Min(Elems, vect.Count);
			for (int i = 0; i < num; i++)
			{
				array[i] = vect[i];
			}
			return new Vector(array);
		}

		public static Vector Subtract(Vector v1, Vector v2)
		{
			if (v1.Count != v2.Count)
			{
				throw new MatrixDimensionException();
			}
			Vector vector = new Vector(v1.Count);
			for (int i = 0; i < v1.Count; i++)
			{
				vector[i] = v1[i] - v2[i];
			}
			return vector;
		}

		public static Vector operator -(Vector v1, Vector v2)
		{
			return Vector.Subtract(v1, v2);
		}

		public void Subtract(Vector v2)
		{
			if (this.in_Vect.Length != v2.Count)
			{
				throw new MatrixDimensionException();
			}
			for (int i = 0; i < this.in_Vect.Length; i++)
			{
				this.in_Vect[i] -= v2[i];
			}
		}

		public Matrix diag()
		{
			return new Matrix(Matrix.diag(this.in_Vect));
		}

		public override int GetHashCode()
		{
			return (this.in_Vect != null) ? this.in_Vect.GetHashCode() : 0;
		}
	}
}
