using System;

namespace Matrix_Lib
{
	[Serializable]
	public class Matrix
	{
		private double[,] in_Mat;

		public double this[int Row, int Col]
		{
			get
			{
				if (Row < 0)
				{
					Row += this.in_Mat.GetLength(0);
				}
				if (Col < 0)
				{
					Col += this.in_Mat.GetLength(1);
				}
				return this.in_Mat[Row, Col];
			}
			set
			{
				if (Row < 0)
				{
					Row += this.in_Mat.GetLength(0);
				}
				if (Col < 0)
				{
					Col += this.in_Mat.GetLength(1);
				}
				this.in_Mat[Row, Col] = value;
			}
		}

		public int NoRows
		{
			get
			{
				return this.in_Mat.GetUpperBound(0) + 1;
			}
			set
			{
				this.in_Mat = new double[value, this.in_Mat.GetUpperBound(0)];
			}
		}

		public int NoCols
		{
			get
			{
				return this.in_Mat.GetUpperBound(1) + 1;
			}
			set
			{
				this.in_Mat = new double[this.in_Mat.GetUpperBound(0), value];
			}
		}

		public double[,] toArray
		{
			get
			{
				return this.in_Mat;
			}
		}

		public Matrix(int noRows, int noCols)
		{
			this.in_Mat = new double[noRows, noCols];
		}

		public Matrix(double[,] Mat)
		{
			this.in_Mat = (double[,])Mat.Clone();
		}

		public Matrix Clone()
		{
			return new Matrix(this.in_Mat);
		}

		public Vector getRowVector(int index)
		{
			Vector vector = new Vector(this.NoCols);
			for (int i = 0; i < vector.Count; i++)
			{
				vector[i] = this.in_Mat[index, i];
			}
			return vector;
		}

		public Vector getColVector(int index)
		{
			Vector vector = new Vector(this.NoRows);
			for (int i = 0; i < vector.Count; i++)
			{
				vector[i] = this.in_Mat[i, index];
			}
			return vector;
		}

		private static void Find_R_C(double[] Mat, out int Row)
		{
			Row = Mat.GetUpperBound(0);
		}

		private static void Find_R_C(double[,] Mat, out int Row, out int Col)
		{
			Row = Mat.GetUpperBound(0);
			Col = Mat.GetUpperBound(1);
		}

		public static double[,] OneD_2_TwoD(double[] Mat)
		{
			int num = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num);
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[,] array = new double[num + 1, 1];
			for (int i = 0; i <= num; i++)
			{
				double[,] array2 = array;
				int num2 = i;
				double num3 = Mat[i];
				array2[num2, 0] = num3;
			}
			return array;
		}

		public static Matrix OneD_2_TwoD(Vector Vect)
		{
			return new Matrix(Matrix.OneD_2_TwoD(Vect.toArray));
		}

		public static double[] TwoD_2_OneD(double[,] Mat)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num2 != 0)
			{
				throw new MatrixDimensionException();
			}
			double[] array = new double[num + 1];
			for (int i = 0; i <= num; i++)
			{
				array[i] = Mat[i, 0];
			}
			return array;
		}

		public static Vector TwoD_2_OneD(Matrix Mat)
		{
			return new Vector(Matrix.TwoD_2_OneD(Mat.in_Mat));
		}

		public static Matrix I(int n)
		{
			Matrix matrix = new Matrix(n, n);
			for (int i = 0; i < n; i++)
			{
				matrix[i, i] = 1.0;
			}
			return matrix;
		}

		public static Matrix IMinus(Matrix mat)
		{
			return Matrix.I(mat.NoRows) - mat;
		}

		public static double[,] Add(double[,] Mat1, double[,] Mat2)
		{
			int num = default(int);
			int num2 = default(int);
			int num3 = default(int);
			int num4 = default(int);
			try
			{
				Matrix.Find_R_C(Mat1, out num, out num2);
				Matrix.Find_R_C(Mat2, out num3, out num4);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != num3 || num2 != num4)
			{
				throw new MatrixDimensionException();
			}
			double[,] array = new double[num + 1, num2 + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					double[,] array2 = array;
					int num5 = i;
					int num6 = j;
					double num7 = Mat1[i, j] + Mat2[i, j];
					array2[num5, num6] = num7;
				}
			}
			return array;
		}

		public static Matrix Add(Matrix Mat1, Matrix Mat2)
		{
			return new Matrix(Matrix.Add(Mat1.in_Mat, Mat2.in_Mat));
		}

		public static Matrix operator +(Matrix Mat1, Matrix Mat2)
		{
			return new Matrix(Matrix.Add(Mat1.in_Mat, Mat2.in_Mat));
		}

		public void Add(Matrix Mat1)
		{
			this.in_Mat = Matrix.Add(this.in_Mat, Mat1.in_Mat);
		}

		public static double[,] Subtract(double[,] Mat1, double[,] Mat2)
		{
			int num = default(int);
			int num2 = default(int);
			int num3 = default(int);
			int num4 = default(int);
			try
			{
				Matrix.Find_R_C(Mat1, out num, out num2);
				Matrix.Find_R_C(Mat2, out num3, out num4);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != num3 || num2 != num4)
			{
				throw new MatrixDimensionException();
			}
			double[,] array = new double[num + 1, num2 + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					double[,] array2 = array;
					int num5 = i;
					int num6 = j;
					double num7 = Mat1[i, j] - Mat2[i, j];
					array2[num5, num6] = num7;
				}
			}
			return array;
		}

		public static Matrix Subtract(Matrix Mat1, Matrix Mat2)
		{
			return new Matrix(Matrix.Subtract(Mat1.in_Mat, Mat2.in_Mat));
		}

		public static Matrix operator -(Matrix Mat1, Matrix Mat2)
		{
			return new Matrix(Matrix.Subtract(Mat1.in_Mat, Mat2.in_Mat));
		}

		public void Subtract(Matrix Mat1)
		{
			this.in_Mat = Matrix.Subtract(this.in_Mat, Mat1.in_Mat);
		}

		public static double[,] Multiply(double[,] Mat1, double[,] Mat2)
		{
			double num = 0.0;
			int num2 = default(int);
			int num3 = default(int);
			int num4 = default(int);
			int num5 = default(int);
			try
			{
				Matrix.Find_R_C(Mat1, out num2, out num3);
				Matrix.Find_R_C(Mat2, out num4, out num5);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num3 != num4)
			{
				throw new MatrixDimensionException();
			}
			double[,] array = new double[num2 + 1, num5 + 1];
			for (int i = 0; i <= num2; i++)
			{
				for (int j = 0; j <= num5; j++)
				{
					for (int k = 0; k <= num3; k++)
					{
						num += Mat1[i, k] * Mat2[k, j];
					}
					array[i, j] = num;
					num = 0.0;
				}
			}
			return array;
		}

		public static Matrix Multiply(Matrix Mat1, Matrix Mat2)
		{
			if (Mat1.NoRows == 3 && Mat2.NoRows == 3 && Mat1.NoCols == 1 && Mat1.NoCols == 1)
			{
				return new Matrix(Matrix.CrossProduct(Mat1.in_Mat, Mat2.in_Mat));
			}
			return new Matrix(Matrix.Multiply(Mat1.in_Mat, Mat2.in_Mat));
		}

		public static Matrix operator *(Matrix Mat1, Matrix Mat2)
		{
			if (Mat1.NoRows == 3 && Mat2.NoRows == 3 && Mat1.NoCols == 1 && Mat1.NoCols == 1)
			{
				return new Matrix(Matrix.CrossProduct(Mat1.in_Mat, Mat2.in_Mat));
			}
			return new Matrix(Matrix.Multiply(Mat1.in_Mat, Mat2.in_Mat));
		}

		public static Vector Multiply(Matrix Mat1, Vector Vect)
		{
			Matrix matrix = new Matrix(Vect.Count, 1);
			for (int i = 0; i < Vect.Count; i++)
			{
				matrix[i, 0] = Vect[i];
			}
			return Matrix.Multiply(Mat1, matrix).getColVector(0);
		}

		public static Vector operator *(Matrix Mat1, Vector Vect)
		{
			return Matrix.Multiply(Mat1, Vect);
		}

		public void Multiply(Matrix Mat1)
		{
			this.in_Mat = Matrix.Multiply(this.in_Mat, Mat1.in_Mat);
		}

		public static double Det(double[,] Mat)
		{
			double[,] array;
			int num = default(int);
			int num2 = default(int);
			try
			{
				array = (double[,])Mat.Clone();
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != num2)
			{
				throw new MatrixNotSquare();
			}
			int num3 = num;
			double num4 = 1.0;
			for (int i = 0; i <= num3; i++)
			{
				if (array[i, i] == 0.0)
				{
					int j;
					for (j = i; j < num3 && array[i, j] == 0.0; j++)
					{
					}
					if (array[i, j] == 0.0)
					{
						return 0.0;
					}
					for (int k = i; k <= num3; k++)
					{
						double num5 = array[k, j];
						double[,] array2 = array;
						int num6 = k;
						int num7 = j;
						double num8 = array[k, i];
						array2[num6, num7] = num8;
						array[k, i] = num5;
					}
					num4 = 0.0 - num4;
				}
				double num9 = array[i, i];
				num4 *= num9;
				if (i < num3)
				{
					int num10 = i + 1;
					for (int k = num10; k <= num3; k++)
					{
						for (int j = num10; j <= num3; j++)
						{
							double[,] array3 = array;
							int num11 = k;
							int num12 = j;
							double num13 = array[k, j] - array[k, i] * (array[i, j] / num9);
							array3[num11, num12] = num13;
						}
					}
				}
			}
			return num4;
		}

		public static double Det(Matrix Mat)
		{
			return Matrix.Det(Mat.in_Mat);
		}

		public static double[,] Inverse(double[,] Mat)
		{
			int num = default(int);
			int num2 = default(int);
			double[,] array;
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
				array = (double[,])Mat.Clone();
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != num2)
			{
				throw new MatrixNotSquare();
			}
			if (Matrix.Det(Mat) == 0.0)
			{
				throw new MatrixDeterminentZero();
			}
			int num3 = num;
			double[,] array2 = new double[num3 + 1, num3 + 1];
			for (int i = 0; i <= num3; i++)
			{
				for (int j = 0; j <= num3; j++)
				{
					array2[j, i] = 0.0;
				}
				array2[i, i] = 1.0;
			}
			for (int k = 0; k <= num3; k++)
			{
				if (Math.Abs(array[k, k]) < 1E-10)
				{
					int num4 = k + 1;
					while (num4 <= num3)
					{
						if (num4 == k || !(Math.Abs(array[k, num4]) > 1E-10))
						{
							num4++;
							continue;
						}
						for (int l = 0; l <= num3; l++)
						{
							double[,] array3 = array;
							int num5 = l;
							int num6 = k;
							double num7 = array[l, k] + array[l, num4];
							array3[num5, num6] = num7;
							double[,] array4 = array2;
							int num8 = l;
							int num9 = k;
							double num10 = array2[l, k] + array2[l, num4];
							array4[num8, num9] = num10;
						}
						break;
					}
				}
				double num11 = 1.0 / array[k, k];
				for (int num4 = 0; num4 <= num3; num4++)
				{
					double[,] array5 = array;
					int num12 = num4;
					int num13 = k;
					double num14 = num11 * array[num4, k];
					array5[num12, num13] = num14;
					double[,] array6 = array2;
					int num15 = num4;
					int num16 = k;
					double num17 = num11 * array2[num4, k];
					array6[num15, num16] = num17;
				}
				for (int num4 = 0; num4 <= num3; num4++)
				{
					if (num4 != k)
					{
						double num18 = array[k, num4];
						for (int l = 0; l <= num3; l++)
						{
							double[,] array7 = array;
							int num19 = l;
							int num20 = num4;
							double num21 = array[l, num4] - num18 * array[l, k];
							array7[num19, num20] = num21;
							double[,] array8 = array2;
							int num22 = l;
							int num23 = num4;
							double num24 = array2[l, num4] - num18 * array2[l, k];
							array8[num22, num23] = num24;
						}
					}
				}
			}
			return array2;
		}

		public static Matrix Inverse(Matrix Mat)
		{
			return new Matrix(Matrix.Inverse(Mat.in_Mat));
		}

		public void Inverse()
		{
			this.in_Mat = Matrix.Inverse(this.in_Mat);
		}

		public static double[,] Transpose(double[,] Mat)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[,] array = new double[num2 + 1, num + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					double[,] array2 = array;
					int num3 = j;
					int num4 = i;
					double num5 = Mat[i, j];
					array2[num3, num4] = num5;
				}
			}
			return array;
		}

		public static Matrix Transpose(Matrix Mat)
		{
			return new Matrix(Matrix.Transpose(Mat.in_Mat));
		}

		public void Transpose()
		{
			this.in_Mat = Matrix.Transpose(this.in_Mat);
		}

		public static void SVD(double[,] Mat_, out double[,] S_, out double[,] U_, out double[,] V_)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat_, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			int num3 = num + 1;
			int num4 = num2 + 1;
			int num6;
			int num5;
			if (num3 < num4)
			{
				num3 = num4;
				num6 = (num5 = num4);
			}
			else if (num3 > num4)
			{
				num4 = num3;
				num5 = (num6 = num3);
			}
			else
			{
				num6 = num3;
				num5 = num4;
			}
			double[,] array = new double[num3 + 1, num4 + 1];
			for (int i = 1; i <= num + 1; i++)
			{
				for (int j = 1; j <= num2 + 1; j++)
				{
					double[,] array2 = array;
					int num7 = i;
					int num8 = j;
					double num9 = Mat_[i - 1, j - 1];
					array2[num7, num8] = num9;
				}
			}
			double[,] array3 = new double[num5 + 1, num5 + 1];
			double[] array4 = new double[num5 + 1];
			double[] array5 = new double[100];
			int num10 = 0;
			int num11 = 0;
			double num12 = 0.0;
			double num13 = 0.0;
			double num14 = 0.0;
			for (int k = 1; k <= num4; k++)
			{
				num10 = k + 1;
				array5[k] = num13 * num12;
				double num15;
				num12 = (num15 = (num13 = 0.0));
				if (k <= num3)
				{
					for (int l = k; l <= num3; l++)
					{
						num13 += Math.Abs(array[l, k]);
					}
					if (num13 != 0.0)
					{
						for (int l = k; l <= num3; l++)
						{
							array[l, k] /= num13;
							num15 += array[l, k] * array[l, k];
						}
						double num16 = array[k, k];
						num12 = 0.0 - Matrix.Sign(Math.Sqrt(num15), num16);
						double num17 = num16 * num12 - num15;
						array[k, k] = num16 - num12;
						if (k != num4)
						{
							for (int m = num10; m <= num4; m++)
							{
								num15 = 0.0;
								for (int l = k; l <= num3; l++)
								{
									num15 += array[l, k] * array[l, m];
								}
								num16 = num15 / num17;
								for (int l = k; l <= num3; l++)
								{
									array[l, m] += num16 * array[l, k];
								}
							}
						}
						for (int l = k; l <= num3; l++)
						{
							array[l, k] *= num13;
						}
					}
				}
				array4[k] = num13 * num12;
				num12 = (num15 = (num13 = 0.0));
				if (k <= num3 && k != num4)
				{
					for (int l = num10; l <= num4; l++)
					{
						num13 += Math.Abs(array[k, l]);
					}
					if (num13 != 0.0)
					{
						for (int l = num10; l <= num4; l++)
						{
							array[k, l] /= num13;
							num15 += array[k, l] * array[k, l];
						}
						double num16 = array[k, num10];
						num12 = 0.0 - Matrix.Sign(Math.Sqrt(num15), num16);
						double num17 = num16 * num12 - num15;
						array[k, num10] = num16 - num12;
						for (int l = num10; l <= num4; l++)
						{
							array5[l] = array[k, l] / num17;
						}
						if (k != num3)
						{
							for (int m = num10; m <= num3; m++)
							{
								num15 = 0.0;
								for (int l = num10; l <= num4; l++)
								{
									num15 += array[m, l] * array[k, l];
								}
								for (int l = num10; l <= num4; l++)
								{
									array[m, l] += num15 * array5[l];
								}
							}
						}
						for (int l = num10; l <= num4; l++)
						{
							array[k, l] *= num13;
						}
					}
				}
				num14 = Math.Max(num14, Math.Abs(array4[k]) + Math.Abs(array5[k]));
			}
			for (int k = num4; k >= 1; k--)
			{
				if (k < num4)
				{
					if (num12 != 0.0)
					{
						for (int m = num10; m <= num4; m++)
						{
							double[,] array6 = array3;
							int num18 = m;
							int num19 = k;
							double num20 = array[k, m] / array[k, num10] / num12;
							array6[num18, num19] = num20;
						}
						for (int m = num10; m <= num4; m++)
						{
							double num15 = 0.0;
							for (int l = num10; l <= num4; l++)
							{
								num15 += array[k, l] * array3[l, m];
							}
							for (int l = num10; l <= num4; l++)
							{
								array3[l, m] += num15 * array3[l, k];
							}
						}
					}
					for (int m = num10; m <= num4; m++)
					{
						double[,] array7 = array3;
						int num21 = k;
						int num22 = m;
						double[,] array8 = array3;
						int num23 = m;
						int num24 = k;
						double num25;
						double num26 = num25 = 0.0;
						array8[num23, num24] = num26;
						array7[num21, num22] = num25;
					}
				}
				array3[k, k] = 1.0;
				num12 = array5[k];
				num10 = k;
			}
			for (int k = num4; k >= 1; k--)
			{
				num10 = k + 1;
				num12 = array4[k];
				if (k < num4)
				{
					for (int m = num10; m <= num4; m++)
					{
						array[k, m] = 0.0;
					}
				}
				if (num12 != 0.0)
				{
					num12 = 1.0 / num12;
					if (k != num4)
					{
						for (int m = num10; m <= num4; m++)
						{
							double num15 = 0.0;
							for (int l = num10; l <= num3; l++)
							{
								num15 += array[l, k] * array[l, m];
							}
							double num16 = num15 / array[k, k] * num12;
							for (int l = k; l <= num3; l++)
							{
								array[l, m] += num16 * array[l, k];
							}
						}
					}
					for (int m = k; m <= num3; m++)
					{
						array[m, k] *= num12;
					}
				}
				else
				{
					for (int m = k; m <= num3; m++)
					{
						array[m, k] = 0.0;
					}
				}
				array[k, k] += 1.0;
			}
			for (int l = num4; l >= 1; l--)
			{
				for (int n = 1; n <= 30; n++)
				{
					int num27 = 1;
					num10 = l;
					while (num10 >= 1)
					{
						num11 = num10 - 1;
						if (Math.Abs(array5[num10]) + num14 == num14)
						{
							num27 = 0;
						}
						else if (Math.Abs(array4[num11]) + num14 != num14)
						{
							num10--;
							continue;
						}
						break;
					}
					double num28;
					double num15;
					double num16;
					double num17;
					double num29;
					double num30;
					if (num27 != 0)
					{
						num15 = 1.0;
						for (int k = num10; k <= l; k++)
						{
							num16 = num15 * array5[k];
							if (Math.Abs(num16) + num14 != num14)
							{
								num12 = array4[k];
								num17 = 1.0 / (array4[k] = Matrix.PYTHAG(num16, num12));
								num28 = num12 * num17;
								num15 = (0.0 - num16) * num17;
								for (int m = 1; m <= num3; m++)
								{
									num29 = array[m, num11];
									num30 = array[m, k];
									array[m, num11] = num29 * num28 + num30 * num15;
									array[m, k] = num30 * num28 - num29 * num15;
								}
							}
						}
					}
					num30 = array4[l];
					if (num10 == l)
					{
						if (num30 < 0.0)
						{
							array4[l] = 0.0 - num30;
							for (int m = 1; m <= num4; m++)
							{
								double[,] array9 = array3;
								int num31 = m;
								int num32 = l;
								double num33 = 0.0 - array3[m, l];
								array9[num31, num32] = num33;
							}
						}
						break;
					}
					if (n == 30)
					{
						Console.WriteLine("No convergence in 30 SVDCMP iterations");
					}
					double num34 = array4[num10];
					num11 = l - 1;
					num29 = array4[num11];
					num12 = array5[num11];
					num17 = array5[l];
					num16 = ((num29 - num30) * (num29 + num30) + (num12 - num17) * (num12 + num17)) / (2.0 * num17 * num29);
					num12 = Matrix.PYTHAG(num16, 1.0);
					num16 = ((num34 - num30) * (num34 + num30) + num17 * (num29 / (num16 + Matrix.Sign(num12, num16)) - num17)) / num34;
					num28 = (num15 = 1.0);
					for (int m = num10; m <= num11; m++)
					{
						int k = m + 1;
						num12 = array5[k];
						num29 = array4[k];
						num17 = num15 * num12;
						num12 = num28 * num12;
						num30 = (array5[m] = Matrix.PYTHAG(num16, num17));
						num28 = num16 / num30;
						num15 = num17 / num30;
						num16 = num34 * num28 + num12 * num15;
						num12 = num12 * num28 - num34 * num15;
						num17 = num29 * num15;
						num29 *= num28;
						for (int num35 = 1; num35 <= num4; num35++)
						{
							num34 = array3[num35, m];
							num30 = array3[num35, k];
							array3[num35, m] = num34 * num28 + num30 * num15;
							array3[num35, k] = num30 * num28 - num34 * num15;
						}
						num30 = (array4[m] = Matrix.PYTHAG(num16, num17));
						if (num30 != 0.0)
						{
							num30 = 1.0 / num30;
							num28 = num16 * num30;
							num15 = num17 * num30;
						}
						num16 = num28 * num12 + num15 * num29;
						num34 = num28 * num29 - num15 * num12;
						for (int num35 = 1; num35 <= num3; num35++)
						{
							num29 = array[num35, m];
							num30 = array[num35, k];
							array[num35, m] = num29 * num28 + num30 * num15;
							array[num35, k] = num30 * num28 - num29 * num15;
						}
					}
					array5[num10] = 0.0;
					array5[l] = num16;
					array4[l] = num34;
				}
			}
			double[,] array10 = new double[num5, num5];
			double[,] array11 = new double[num5, num5];
			double[,] array12 = new double[num6, num5];
			for (int k = 1; k <= num5; k++)
			{
				double[,] array13 = array10;
				int num36 = k - 1;
				int num37 = k - 1;
				double num38 = array4[k];
				array13[num36, num37] = num38;
			}
			S_ = array10;
			for (int k = 1; k <= num5; k++)
			{
				for (int m = 1; m <= num5; m++)
				{
					double[,] array14 = array11;
					int num39 = k - 1;
					int num40 = m - 1;
					double num41 = array3[k, m];
					array14[num39, num40] = num41;
				}
			}
			V_ = array11;
			for (int k = 1; k <= num6; k++)
			{
				for (int m = 1; m <= num5; m++)
				{
					double[,] array15 = array12;
					int num42 = k - 1;
					int num43 = m - 1;
					double num44 = array[k, m];
					array15[num42, num43] = num44;
				}
			}
			U_ = array12;
		}

		private static double SQR(double a)
		{
			return a * a;
		}

		private static double Sign(double a, double b)
		{
			if (b >= 0.0)
			{
				return Math.Abs(a);
			}
			return 0.0 - Math.Abs(a);
		}

		private static double PYTHAG(double a, double b)
		{
			double num = Math.Abs(a);
			double num2 = Math.Abs(b);
			if (num > num2)
			{
				return num * Math.Sqrt(1.0 + Matrix.SQR(num2 / num));
			}
			return (num2 == 0.0) ? 0.0 : (num2 * Math.Sqrt(1.0 + Matrix.SQR(num / num2)));
		}

		public static void SVD(Matrix Mat, out Matrix S, out Matrix U, out Matrix V)
		{
			double[,] mat = default(double[,]);
			double[,] mat2 = default(double[,]);
			double[,] mat3 = default(double[,]);
			Matrix.SVD(Mat.in_Mat, out mat, out mat2, out mat3);
			S = new Matrix(mat);
			U = new Matrix(mat2);
			V = new Matrix(mat3);
		}

		public static void LU(double[,] Mat, out double[,] L, out double[,] U, out double[,] P)
		{
			double[,] array;
			int num = default(int);
			int num2 = default(int);
			try
			{
				array = (double[,])Mat.Clone();
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != num2)
			{
				throw new MatrixNotSquare();
			}
			int num3 = 0;
			int num4 = num;
			int[] array2 = new int[num4 + 1];
			double[] array3 = new double[num4 * 10];
			double num5 = 1.0;
			for (int i = 0; i <= num4; i++)
			{
				double num6 = 0.0;
				for (int j = 0; j <= num4; j++)
				{
					if (Math.Abs(array[i, j]) > num6)
					{
						num6 = Math.Abs(array[i, j]);
					}
				}
				if (num6 == 0.0)
				{
					throw new MatrixSingularException();
				}
				array3[i] = 1.0 / num6;
			}
			for (int j = 0; j <= num4; j++)
			{
				if (j > 0)
				{
					for (int i = 0; i <= j - 1; i++)
					{
						double num7 = array[i, j];
						if (i > 0)
						{
							for (int k = 0; k <= i - 1; k++)
							{
								num7 -= array[i, k] * array[k, j];
							}
							array[i, j] = num7;
						}
					}
				}
				double num6 = 0.0;
				for (int i = j; i <= num4; i++)
				{
					double num7 = array[i, j];
					if (j > 0)
					{
						for (int k = 0; k <= j - 1; k++)
						{
							num7 -= array[i, k] * array[k, j];
						}
						array[i, j] = num7;
					}
					double num8 = array3[i] * Math.Abs(num7);
					if (num8 >= num6)
					{
						num3 = i;
						num6 = num8;
					}
				}
				if (j != num3)
				{
					for (int k = 0; k <= num4; k++)
					{
						double num8 = array[num3, k];
						double[,] array4 = array;
						int num9 = num3;
						int num10 = k;
						double num11 = array[j, k];
						array4[num9, num10] = num11;
						array[j, k] = num8;
					}
					num5 = 0.0 - num5;
					array3[num3] = array3[j];
				}
				array2[j] = num3;
				if (j != num4)
				{
					if (array[j, j] == 0.0)
					{
						array[j, j] = 1E-20;
					}
					double num8 = 1.0 / array[j, j];
					for (int i = j + 1; i <= num4; i++)
					{
						double[,] array5 = array;
						int num12 = i;
						int num13 = j;
						double num14 = array[i, j] * num8;
						array5[num12, num13] = num14;
					}
				}
			}
			if (array[num4, num4] == 0.0)
			{
				array[num4, num4] = 1E-20;
			}
			int num15 = 0;
			double[,] array6 = new double[num4 + 1, num4 + 1];
			double[,] array7 = new double[num4 + 1, num4 + 1];
			for (int i = 0; i <= num4; i++)
			{
				for (int j = 0; j <= num15; j++)
				{
					if (i != 0)
					{
						double[,] array8 = array6;
						int num16 = i;
						int num17 = j;
						double num18 = array[i, j];
						array8[num16, num17] = num18;
					}
					if (i == j)
					{
						array6[i, j] = 1.0;
					}
					double[,] array9 = array7;
					int num19 = num4 - i;
					int num20 = num4 - j;
					double num21 = array[num4 - i, num4 - j];
					array9[num19, num20] = num21;
				}
				num15++;
			}
			L = array6;
			U = array7;
			P = Matrix.I(num4 + 1).in_Mat;
			for (int i = 0; i <= num4; i++)
			{
				Matrix.SwapRows(P, i, array2[i]);
			}
		}

		private static void SwapRows(double[,] Mat, int Row, int toRow)
		{
			int upperBound = Mat.GetUpperBound(0);
			double[,] array = new double[1, upperBound + 1];
			for (int i = 0; i <= upperBound; i++)
			{
				double[,] array2 = array;
				int num = i;
				double num2 = Mat[Row, i];
				array2[0, num] = num2;
				int num3 = i;
				double num4 = Mat[toRow, i];
				Mat[Row, num3] = num4;
				int num5 = i;
				double num6 = array[0, i];
				Mat[toRow, num5] = num6;
			}
		}

		public static void LU(Matrix Mat, out Matrix L, out Matrix U, out Matrix P)
		{
			double[,] mat = default(double[,]);
			double[,] mat2 = default(double[,]);
			double[,] mat3 = default(double[,]);
			Matrix.LU(Mat.in_Mat, out mat, out mat2, out mat3);
			L = new Matrix(mat);
			U = new Matrix(mat2);
			P = new Matrix(mat3);
		}

		public static double[,] SolveLinear(double[,] MatA, double[,] MatB)
		{
			double[,] array;
			double[,] array2;
			int num = default(int);
			int num2 = default(int);
			try
			{
				array = (double[,])MatA.Clone();
				array2 = (double[,])MatB.Clone();
				Matrix.Find_R_C(array, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != num2)
			{
				throw new MatrixNotSquare();
			}
			if (array2.GetUpperBound(0) != num || array2.GetUpperBound(1) != 0)
			{
				throw new MatrixDimensionException();
			}
			int num3 = 0;
			int num4 = num;
			int[] array3 = new int[num4 + 1];
			double[] array4 = new double[num4 * 10];
			double num5 = 1.0;
			for (int i = 0; i <= num4; i++)
			{
				double num6 = 0.0;
				for (int j = 0; j <= num4; j++)
				{
					if (Math.Abs(array[i, j]) > num6)
					{
						num6 = Math.Abs(array[i, j]);
					}
				}
				if (num6 == 0.0)
				{
					throw new MatrixSingularException();
				}
				array4[i] = 1.0 / num6;
			}
			for (int j = 0; j <= num4; j++)
			{
				if (j > 0)
				{
					for (int i = 0; i <= j - 1; i++)
					{
						double num7 = array[i, j];
						if (i > 0)
						{
							for (int k = 0; k <= i - 1; k++)
							{
								num7 -= array[i, k] * array[k, j];
							}
							array[i, j] = num7;
						}
					}
				}
				double num6 = 0.0;
				for (int i = j; i <= num4; i++)
				{
					double num7 = array[i, j];
					if (j > 0)
					{
						for (int k = 0; k <= j - 1; k++)
						{
							num7 -= array[i, k] * array[k, j];
						}
						array[i, j] = num7;
					}
					double num8 = array4[i] * Math.Abs(num7);
					if (num8 >= num6)
					{
						num3 = i;
						num6 = num8;
					}
				}
				if (j != num3)
				{
					for (int k = 0; k <= num4; k++)
					{
						double num8 = array[num3, k];
						double[,] array5 = array;
						int num9 = num3;
						int num10 = k;
						double num11 = array[j, k];
						array5[num9, num10] = num11;
						array[j, k] = num8;
					}
					num5 = 0.0 - num5;
					array4[num3] = array4[j];
				}
				array3[j] = num3;
				if (j != num4)
				{
					if (array[j, j] == 0.0)
					{
						array[j, j] = 1E-20;
					}
					double num8 = 1.0 / array[j, j];
					for (int i = j + 1; i <= num4; i++)
					{
						double[,] array6 = array;
						int num12 = i;
						int num13 = j;
						double num14 = array[i, j] * num8;
						array6[num12, num13] = num14;
					}
				}
			}
			if (array[num4, num4] == 0.0)
			{
				array[num4, num4] = 1E-20;
			}
			int num15 = -1;
			for (int i = 0; i <= num4; i++)
			{
				int num16 = array3[i];
				double num17 = array2[num16, 0];
				double[,] array7 = array2;
				int num18 = num16;
				double num19 = array2[i, 0];
				array7[num18, 0] = num19;
				if (num15 != -1)
				{
					for (int j = num15; j <= i - 1; j++)
					{
						num17 -= array[i, j] * array2[j, 0];
					}
				}
				else if (num17 != 0.0)
				{
					num15 = i;
				}
				array2[i, 0] = num17;
			}
			for (int i = num4; i >= 0; i--)
			{
				double num17 = array2[i, 0];
				if (i < num4)
				{
					for (int j = i + 1; j <= num4; j++)
					{
						num17 -= array[i, j] * array2[j, 0];
					}
				}
				double[,] array8 = array2;
				int num20 = i;
				double num21 = num17 / array[i, i];
				array8[num20, 0] = num21;
			}
			return array2;
		}

		public static Matrix SolveLinear(Matrix MatA, Matrix MatB)
		{
			return new Matrix(Matrix.SolveLinear(MatA.in_Mat, MatB.in_Mat));
		}

		public static int Rank(double[,] Mat)
		{
			int num = 0;
			try
			{
				int num2 = default(int);
				int num3 = default(int);
				Matrix.Find_R_C(Mat, out num2, out num3);
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[,] array = default(double[,]);
			double[,] array2 = default(double[,]);
			double[,] array3 = default(double[,]);
			Matrix.SVD(Mat, out array, out array2, out array3);
			for (int i = 0; i <= array.GetUpperBound(0); i++)
			{
				if (Math.Abs(array[i, i]) > 2.2204E-16)
				{
					num++;
				}
			}
			return num;
		}

		public static int Rank(Matrix Mat)
		{
			return Matrix.Rank(Mat.in_Mat);
		}

		public int Rank()
		{
			return Matrix.Rank(this.in_Mat);
		}

		public static double[,] PINV(double[,] Mat)
		{
			double num = 0.0;
			int num2 = default(int);
			int num3 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num2, out num3);
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[,] array = default(double[,]);
			double[,] array2 = default(double[,]);
			double[,] array3 = default(double[,]);
			Matrix.SVD(Mat, out array, out array2, out array3);
			double num4 = 2.2204E-16;
			num2++;
			num3++;
			double[,] array4 = new double[num2, num3];
			double[,] array5 = new double[num2, num3];
			double[,] array6 = new double[num3, num3];
			double num5 = 0.0;
			for (int i = 0; i <= array.GetUpperBound(0); i++)
			{
				if (i == 0)
				{
					num = array[0, 0];
				}
				if (num < array[i, i])
				{
					num = array[i, i];
				}
			}
			double num6 = (num2 <= num3) ? ((double)num3 * num * num4) : ((double)num2 * num * num4);
			for (int i = 0; i < num3; i++)
			{
				double[,] array7 = array6;
				int num7 = i;
				int num8 = i;
				double num9 = array[i, i];
				array7[num7, num8] = num9;
			}
			for (int i = 0; i < num2; i++)
			{
				for (int j = 0; j < num3; j++)
				{
					for (int k = 0; k < num3; k++)
					{
						if (array6[k, j] > num6)
						{
							num5 += array2[i, k] * (1.0 / array6[k, j]);
						}
					}
					array4[i, j] = num5;
					num5 = 0.0;
				}
			}
			for (int i = 0; i < num2; i++)
			{
				for (int j = 0; j < num3; j++)
				{
					for (int k = 0; k < num3; k++)
					{
						num5 += array4[i, k] * array3[j, k];
					}
					array5[i, j] = num5;
					num5 = 0.0;
				}
			}
			return Matrix.Transpose(array5);
		}

		public static Matrix PINV(Matrix Mat)
		{
			return new Matrix(Matrix.PINV(Mat.in_Mat));
		}

		public static void Eigen(double[,] Mat, out double[,] d, out double[,] v)
		{
			int num = default(int);
			int num2 = default(int);
			double[,] array;
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
				array = (double[,])Mat.Clone();
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != num2)
			{
				throw new MatrixNotSquare();
			}
			int num3 = num;
			d = new double[num3 + 1, 1];
			v = new double[num3 + 1, num3 + 1];
			double[] array2 = new double[num3 + 1];
			double[] array3 = new double[num3 + 1];
			for (int i = 0; i <= num3; i++)
			{
				for (int j = 0; j <= num3; j++)
				{
					v[i, j] = 0.0;
				}
				v[i, i] = 1.0;
			}
			for (int i = 0; i <= num3; i++)
			{
				double[] array4 = array2;
				int num4 = i;
				double[,] obj2 = d;
				int num5 = i;
				double num6;
				double num7 = num6 = array[i, i];
				obj2[num5, 0] = num7;
				array4[num4] = num6;
				array3[i] = 0.0;
			}
			for (int k = 0; k <= 50; k++)
			{
				double num8 = 0.0;
				for (int i = 0; i <= num3 - 1; i++)
				{
					for (int j = i + 1; j <= num3; j++)
					{
						num8 += Math.Abs(array[i, j]);
					}
				}
				if (num8 == 0.0)
				{
					return;
				}
				double num9 = (k >= 4) ? 0.0 : (0.2 * num8 / (double)(num3 * num3));
				for (int i = 0; i <= num3 - 1; i++)
				{
					for (int j = i + 1; j <= num3; j++)
					{
						double num10 = 100.0 * Math.Abs(array[i, j]);
						if (k > 4 && Math.Abs(d[i, 0]) + num10 == Math.Abs(d[i, 0]) && Math.Abs(d[j, 0]) + num10 == Math.Abs(d[j, 0]))
						{
							array[i, j] = 0.0;
						}
						else if (Math.Abs(array[i, j]) > num9)
						{
							double num11 = d[j, 0] - d[i, 0];
							double num12;
							if (Math.Abs(num11) + num10 == Math.Abs(num11))
							{
								num12 = array[i, j] / num11;
							}
							else
							{
								double num13 = 0.5 * num11 / array[i, j];
								num12 = 1.0 / (Math.Abs(num13) + Math.Sqrt(1.0 + num13 * num13));
								if (num13 < 0.0)
								{
									num12 = 0.0 - num12;
								}
							}
							double num14 = 1.0 / Math.Sqrt(1.0 + num12 * num12);
							double num15 = num12 * num14;
							double tau = num15 / (1.0 + num14);
							num11 = num12 * array[i, j];
							array3[i] -= num11;
							array3[j] += num11;
							d[i, 0] -= num11;
							d[j, 0] += num11;
							array[i, j] = 0.0;
							for (int l = 0; l <= i - 1; l++)
							{
								Matrix.ROT(num10, num11, num15, tau, array, l, i, l, j);
							}
							for (int l = i + 1; l <= j - 1; l++)
							{
								Matrix.ROT(num10, num11, num15, tau, array, i, l, l, j);
							}
							for (int l = j + 1; l <= num3; l++)
							{
								Matrix.ROT(num10, num11, num15, tau, array, i, l, j, l);
							}
							for (int l = 0; l <= num3; l++)
							{
								Matrix.ROT(num10, num11, num15, tau, v, l, i, l, j);
							}
						}
					}
				}
				for (int i = 0; i <= num3; i++)
				{
					array2[i] += array3[i];
					double[,] obj3 = d;
					int num16 = i;
					double num17 = array2[i];
					obj3[num16, 0] = num17;
					array3[i] = 0.0;
				}
			}
			Console.WriteLine("Too many iterations in routine jacobi");
		}

		private static void ROT(double g, double h, double s, double tau, double[,] a, int i, int j, int k, int l)
		{
			g = a[i, j];
			h = a[k, l];
			a[i, j] = g - s * (h + g * tau);
			a[k, l] = h + s * (g - h * tau);
		}

		public static void Eigen(Matrix Mat, out Matrix d, out Matrix v)
		{
			double[,] mat = default(double[,]);
			double[,] mat2 = default(double[,]);
			Matrix.Eigen(Mat.in_Mat, out mat, out mat2);
			d = new Matrix(mat);
			v = new Matrix(mat2);
		}

		public static double[,] ScalarMultiply(double[,] Mat, double Value)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[,] array = new double[num + 1, num2 + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					double[,] array2 = array;
					int num3 = i;
					int num4 = j;
					double num5 = Mat[i, j] * Value;
					array2[num3, num4] = num5;
				}
			}
			return array;
		}

		public static Matrix ScalarMultiply(Matrix Mat, double Value)
		{
			return new Matrix(Matrix.ScalarMultiply(Mat.in_Mat, Value));
		}

		public static Matrix operator *(Matrix Mat, double Value)
		{
			return new Matrix(Matrix.ScalarMultiply(Mat.in_Mat, Value));
		}

		public static double[,] ScalarDivide(double[,] Mat, double Value)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[,] array = new double[num + 1, num2 + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					double[,] array2 = array;
					int num3 = i;
					int num4 = j;
					double num5 = Mat[i, j] / Value;
					array2[num3, num4] = num5;
				}
			}
			return array;
		}

		public static Matrix ScalarDivide(Matrix Mat, double Value)
		{
			return new Matrix(Matrix.ScalarDivide(Mat.in_Mat, Value));
		}

		public static Matrix operator /(Matrix Mat, double Value)
		{
			return new Matrix(Matrix.ScalarDivide(Mat.in_Mat, Value));
		}

		public static double[] CrossProduct(double[] V1, double[] V2)
		{
			double[] array = new double[2];
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(V1, out num);
				Matrix.Find_R_C(V2, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != 2)
			{
				throw new VectorDimensionException();
			}
			if (num2 != 2)
			{
				throw new VectorDimensionException();
			}
			double num3 = V1[1] * V2[2] - V1[2] * V2[1];
			double num4 = V1[2] * V2[0] - V1[0] * V2[2];
			double num5 = V1[0] * V2[1] - V1[1] * V2[0];
			array[0] = num3;
			array[1] = num4;
			array[2] = num5;
			return array;
		}

		public static double[,] CrossProduct(double[,] V1, double[,] V2)
		{
			double[,] array = new double[3, 1];
			int num = default(int);
			int num2 = default(int);
			int num3 = default(int);
			int num4 = default(int);
			try
			{
				Matrix.Find_R_C(V1, out num, out num2);
				Matrix.Find_R_C(V2, out num3, out num4);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != 2 || num2 != 0)
			{
				throw new VectorDimensionException();
			}
			if (num3 != 2 || num4 != 0)
			{
				throw new VectorDimensionException();
			}
			double num5 = V1[1, 0] * V2[2, 0] - V1[2, 0] * V2[1, 0];
			double num6 = V1[2, 0] * V2[0, 0] - V1[0, 0] * V2[2, 0];
			double num7 = V1[0, 0] * V2[1, 0] - V1[1, 0] * V2[0, 0];
			array[0, 0] = num5;
			array[1, 0] = num6;
			array[2, 0] = num7;
			return array;
		}

		public static Matrix CrossProduct(Matrix V1, Matrix V2)
		{
			return new Matrix(Matrix.CrossProduct(V1.in_Mat, V2.in_Mat));
		}

		public static double DotProduct(double[] V1, double[] V2)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(V1, out num);
				Matrix.Find_R_C(V2, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != 2)
			{
				throw new VectorDimensionException();
			}
			if (num2 != 2)
			{
				throw new VectorDimensionException();
			}
			return V1[0] * V2[0] + V1[1] * V2[1] + V1[2] * V2[2];
		}

		public static double DotProduct(double[,] V1, double[,] V2)
		{
			int num = default(int);
			int num2 = default(int);
			int num3 = default(int);
			int num4 = default(int);
			try
			{
				Matrix.Find_R_C(V1, out num, out num2);
				Matrix.Find_R_C(V2, out num3, out num4);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != 2 || num2 != 0)
			{
				throw new VectorDimensionException();
			}
			if (num3 != 2 || num4 != 0)
			{
				throw new VectorDimensionException();
			}
			return V1[0, 0] * V2[0, 0] + V1[1, 0] * V2[1, 0] + V1[2, 0] * V2[2, 0];
		}

		public static double DotProduct(Matrix V1, Matrix V2)
		{
			return Matrix.DotProduct(V1.in_Mat, V2.in_Mat);
		}

		public static double VectorMagnitude(double[] V)
		{
			int num = default(int);
			try
			{
				Matrix.Find_R_C(V, out num);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != 2)
			{
				throw new VectorDimensionException();
			}
			return Math.Sqrt(V[0] * V[0] + V[1] * V[1] + V[2] * V[2]);
		}

		public static double VectorMagnitude(double[,] V)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(V, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != 2 || num2 != 0)
			{
				throw new VectorDimensionException();
			}
			return Math.Sqrt(V[0, 0] * V[0, 0] + V[1, 0] * V[1, 0] + V[2, 0] * V[2, 0]);
		}

		public static double VectorMagnitude(Matrix V)
		{
			return Matrix.VectorMagnitude(V.in_Mat);
		}

		public static bool IsEqual(double[,] Mat1, double[,] Mat2)
		{
			int num = default(int);
			int num2 = default(int);
			int num3 = default(int);
			int num4 = default(int);
			try
			{
				Matrix.Find_R_C(Mat1, out num, out num2);
				Matrix.Find_R_C(Mat2, out num3, out num4);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (num != num3 || num2 != num4)
			{
				throw new MatrixDimensionException();
			}
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					if (Math.Abs(Mat1[i, j] - Mat2[i, j]) > 1E-14)
					{
						return false;
					}
				}
			}
			return true;
		}

		public static bool IsEqual(Matrix Mat1, Matrix Mat2)
		{
			return Matrix.IsEqual(Mat1.in_Mat, Mat2.in_Mat);
		}

		public static bool operator ==(Matrix Mat1, Matrix Mat2)
		{
			return Matrix.IsEqual(Mat1.in_Mat, Mat2.in_Mat);
		}

		public static bool operator !=(Matrix Mat1, Matrix Mat2)
		{
			return !Matrix.IsEqual(Mat1.in_Mat, Mat2.in_Mat);
		}

		public override bool Equals(object obj)
		{
			try
			{
				return this == (Matrix)obj;
			}
			catch
			{
				return false;
			}
		}

		public bool Equals(Matrix obj)
		{
			if (object.ReferenceEquals(null, obj))
			{
				return false;
			}
			if (object.ReferenceEquals(this, obj))
			{
				return true;
			}
			return object.Equals(obj.in_Mat, this.in_Mat);
		}

		public static double[] ColSum(double[,] Mat)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[] array = new double[num2 + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					array[j] += Mat[i, j];
				}
			}
			return array;
		}

		public static Vector ColSum(Matrix Mat)
		{
			return new Vector(Matrix.ColSum(Mat.in_Mat));
		}

		public static double ColSum(double[,] Mat, int Col)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (Col < 0 || Col > num2)
			{
				throw new IndexOutOfRangeException();
			}
			double num3 = 0.0;
			for (int i = 0; i <= num; i++)
			{
				num3 += Mat[i, Col];
			}
			return num3;
		}

		public static double ColSum(Matrix Mat, int Col)
		{
			return Matrix.ColSum(Mat.in_Mat, Col);
		}

		public Vector ColSum()
		{
			return new Vector(Matrix.ColSum(this.in_Mat));
		}

		public double ColSum(int index)
		{
			return Matrix.ColSum(this.in_Mat, index);
		}

		public static double[] RowSum(double[,] Mat)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[] array = new double[num + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					array[i] += Mat[i, j];
				}
			}
			return array;
		}

		public static Vector RowSum(Matrix Mat)
		{
			return new Vector(Matrix.RowSum(Mat.in_Mat));
		}

		public double RowSum(int index)
		{
			return Matrix.RowSum(this.in_Mat, index);
		}

		public Vector RowSum()
		{
			return new Vector(Matrix.RowSum(this.in_Mat));
		}

		public static double RowSum(double[,] Mat, int RowIndex)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (RowIndex < 0 || RowIndex > num)
			{
				throw new IndexOutOfRangeException();
			}
			double num3 = 0.0;
			for (int i = 0; i <= num2; i++)
			{
				num3 += Mat[RowIndex, i];
			}
			return num3;
		}

		public static double RowSum(Matrix Mat, int RowIndex)
		{
			return Matrix.RowSum(Mat.in_Mat, RowIndex);
		}

		public static double Sum(Matrix mat)
		{
			return Matrix.Sum(mat.in_Mat);
		}

		public static double Sum(double[,] Mat)
		{
			double num = 0.0;
			for (int i = 0; i < Mat.GetUpperBound(0); i++)
			{
				for (int j = 0; j < Mat.GetUpperBound(1); j++)
				{
					num += Mat[i, j];
				}
			}
			return num;
		}

		public double Sum()
		{
			return Matrix.Sum(this.in_Mat);
		}

		public static double[,] diag(double[] vect)
		{
			int num;
			try
			{
				num = vect.GetUpperBound(0) + 1;
			}
			catch
			{
				throw new MatrixNullException();
			}
			double[,] array = new double[num, num];
			for (int i = 0; i < num; i++)
			{
				double[,] array2 = array;
				int num2 = i;
				int num3 = i;
				double num4 = vect[i];
				array2[num2, num3] = num4;
			}
			return array;
		}

		public static Matrix diag(Vector v)
		{
			return new Matrix(Matrix.diag(v.toArray));
		}

		public static string PrintMat(double[,] Mat)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			string text = "";
			string text2 = "";
			string text3 = "";
			int[] array = new int[num2 + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					string text4;
					if (i == 0)
					{
						array[j] = 0;
						for (int k = 0; k <= num; k++)
						{
							text4 = Mat[k, j].ToString("0.0000");
							int length = text4.Length;
							if (array[j] < length)
							{
								array[j] = length;
								text2 = text4;
							}
						}
						if (text2.StartsWith("-"))
						{
							array[j] = array[j];
						}
					}
					text4 = Mat[i, j].ToString("0.0000");
					if (text4.StartsWith("-"))
					{
						int length = text4.Length;
						if (array[j] >= length)
						{
							for (int l = 1; l <= array[j] - length; l++)
							{
								text3 += "  ";
							}
							text3 += " ";
						}
					}
					else
					{
						int length = text4.Length;
						if (array[j] > length)
						{
							for (int l = 1; l <= array[j] - length; l++)
							{
								text3 += "  ";
							}
						}
					}
					text3 = text3 + "  " + Mat[i, j].ToString("0.0000");
				}
				if (i != num)
				{
					text = text + text3 + "\n";
					text3 = "";
				}
				text += text3;
				text3 = "";
			}
			return text;
		}

		public static string PrintMat(Matrix Mat)
		{
			return Matrix.PrintMat(Mat.in_Mat);
		}

		public override string ToString()
		{
			return Matrix.PrintMat(this.in_Mat);
		}

		public static double[,] ScaleRow(double[,] Mat, double[] Vect)
		{
			int noRow = default(int);
			int noCol = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out noRow, out noCol);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (Vect.Length - 1 != noRow)
			{
				throw new MatrixDimensionException();
			}
			double[,] array = new double[noRow + 1, noCol + 1];
			for (int i = 0; i <= noRow; i++)
			{
				for (int j = 0; j <= noCol; j++)
				{
					double[,] array2 = array;
					int num3 = i;
					int num4 = j;
					double num5 = Mat[i, j] * Vect[i];
					array2[num3, num4] = num5;
				}
			}
			return array;
		}

		public static Matrix ScaleRow(Matrix Mat, Vector Vect)
		{
			return new Matrix(Matrix.ScaleRow(Mat.in_Mat, Vect.toArray));
		}

		public void ScaleRow(Vector Vect)
		{
			this.in_Mat = Matrix.ScaleRow(this.in_Mat, Vect.toArray);
		}

		public static double[,] ScaleCol(double[,] Mat, double[] Vect)
		{
			int num = default(int);
			int num2 = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out num, out num2);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (Vect.Length - 1 != num2)
			{
				throw new MatrixDimensionException();
			}
			double[,] array = new double[num + 1, num2 + 1];
			for (int i = 0; i <= num; i++)
			{
				for (int j = 0; j <= num2; j++)
				{
					double[,] array2 = array;
					int num3 = i;
					int num4 = j;
					double num5 = Mat[i, j] * Vect[j];
					array2[num3, num4] = num5;
				}
			}
			return array;
		}

		public static Matrix ScaleCol(Matrix Mat, Vector Vect)
		{
			return new Matrix(Matrix.ScaleCol(Mat.in_Mat, Vect.toArray));
		}

		public void ScaleCol(Vector Vect)
		{
			this.in_Mat = Matrix.ScaleCol(this.in_Mat, Vect.toArray);
		}

		public static double[,] ScaleDivCol(double[,] Mat, double[] Vect)
		{
			int noRow = default(int);
			int noCol = default(int);
			try
			{
				Matrix.Find_R_C(Mat, out noRow, out noCol);
			}
			catch
			{
				throw new MatrixNullException();
			}
			if (Vect.Length - 1 != noCol)
			{
				throw new MatrixDimensionException();
			}
			double[,] array = new double[noRow + 1, noCol + 1];
			for (int i = 0; i <= noRow; i++)
			{
				for (int j = 0; j <= noCol; j++)
				{
					if (Math.Abs(Vect[j]) < 1E-05)
					{
						array[i, j] = 0.0;
					}
					else
					{
						double[,] array2 = array;
						int num3 = i;
						int num4 = j;
						double num5 = Mat[i, j] / Vect[j];
						array2[num3, num4] = num5;
					}
				}
			}
			return array;
		}

		public static Matrix ScaleDivCol(Matrix Mat, Vector Vect)
		{
			return new Matrix(Matrix.ScaleDivCol(Mat.in_Mat, Vect.toArray));
		}

		public void ScaleDivCol(Vector Vect)
		{
			this.in_Mat = Matrix.ScaleDivCol(this.in_Mat, Vect.toArray);
		}

		public static double[,] Append(double[,] Mat, double[] Col, double[] Row)
		{
			int noRow = Mat.GetUpperBound(0) + ((Row != null && Row.Length != 0) ? 1 : 0);
			int noCol = Mat.GetUpperBound(1) + ((Col != null && Col.Length != 0) ? 1 : 0);
			double[,] array = new double[noRow + 1, noCol + 1];
			if (Col != null)
			{
				if (Col.GetUpperBound(0) > noRow)
				{
					throw new MatrixDimensionException();
				}
				for (int i = 0; i <= Col.GetUpperBound(0); i++)
				{
					double[,] array2 = array;
					int num3 = i;
					int num4 = noCol;
					double num5 = Col[i];
					array2[num3, num4] = num5;
				}
			}
			if (Row != null)
			{
				if (Row.GetUpperBound(0) > noCol)
				{
					throw new MatrixDimensionException();
				}
				for (int i = 0; i <= Row.GetUpperBound(0); i++)
				{
					double[,] array3 = array;
					int num6 = noRow;
					int num7 = i;
					double num8 = Row[i];
					array3[num6, num7] = num8;
				}
			}
			for (noRow = 0; noRow <= Mat.GetUpperBound(0); noRow++)
			{
				for (noCol = 0; noCol <= Mat.GetUpperBound(1); noCol++)
				{
					double[,] array4 = array;
					int num9 = noRow;
					int num10 = noCol;
					double num11 = Mat[noRow, noCol];
					array4[num9, num10] = num11;
				}
			}
			return array;
		}

		public void Append(double[] Col, double[] Row)
		{
			this.in_Mat = Matrix.Append(this.in_Mat, Col, Row);
		}

		public void Append(Vector Col, Vector Row)
		{
			this.in_Mat = Matrix.Append(this.in_Mat, Col.toArray, Row.toArray);
		}

		public static Matrix Append(Matrix Mat, Vector Col, Vector Row)
		{
			return new Matrix(Matrix.Append(Mat.in_Mat, Col.toArray, Row.toArray));
		}

        public void Remove(int rowNo)
        {
            this.in_Mat = Matrix.Remove(this.in_Mat, rowNo);
        }

        public static Matrix Remove(Matrix Mat, int rowNo)
        {
            return new Matrix(Matrix.Remove(Mat.in_Mat, rowNo));
        }

        public static double[,] Remove(double[,] Mat, int rowIdx)
        {
            int noRow = Mat.GetUpperBound(0)+1;
            int noCol = Mat.GetUpperBound(1)+1;

            double[,] array = new double[noRow - 1, noCol];
            int skip = 0;
            for (noRow = 0; noRow <= Mat.GetUpperBound(0); noRow++)
            {
                if (noRow == rowIdx)
                {
                    skip = 1;
                    continue;
                }
                for (noCol = 0; noCol <= Mat.GetUpperBound(1); noCol++)
                {
                    array[noRow - skip, noCol] = Mat[noRow, noCol];
                }
            }
            return array;
        }


        public static Matrix ReDimPreserve(Matrix arr, int Rows, int Cols)
		{
			Matrix matrix = new Matrix(Rows, Cols);
			int num = Math.Min(Rows, arr.NoRows - 1);
			int num2 = Math.Min(Cols, arr.NoCols - 1);
			for (int i = 0; i < num; i++)
			{
				for (int j = 0; j < num2; j++)
				{
					matrix[i, j] = arr[i, j];
				}
			}
			return matrix;
		}

		public void ReDimPreserve(int Rows, int Cols)
		{
			double[,] array = new double[Rows, Cols];
			int num = Math.Min(Rows, this.in_Mat.GetUpperBound(0) + 1);
			int num2 = Math.Min(Cols, this.in_Mat.GetUpperBound(1) + 1);
			for (int i = 0; i < num; i++)
			{
				for (int j = 0; j < num2; j++)
				{
					double[,] array2 = array;
					int num3 = i;
					int num4 = j;
					double num5 = this.in_Mat[i, j];
					array2[num3, num4] = num5;
				}
			}
			this.in_Mat = array;
		}

		public static Matrix InvI_BD(Matrix B, Matrix D)
		{
			Matrix matrix = B * D;
			return Matrix.Inverse(Matrix.I(matrix.NoRows) - matrix);
		}

		public static double[][] Mat2DblArr(Matrix arr)
		{
			int noCols = arr.NoCols;
			double[][] array = new double[arr.NoRows][];
			for (int i = 0; i < arr.NoRows; i++)
			{
				array[i] = new double[noCols];
				for (int j = 0; j < noCols; j++)
				{
					array[i][j] = arr[i, j];
				}
			}
			return array;
		}

		public static Vector[] Mat2VectArr(Matrix arr)
		{
			int noCols = arr.NoCols;
			Vector[] array = new Vector[arr.NoRows];
			for (int i = 0; i < arr.NoRows; i++)
			{
				array[i] = new Vector(noCols);
				for (int j = 0; j < noCols; j++)
				{
					array[i][j] = arr[i, j];
				}
			}
			return array;
		}

		public override int GetHashCode()
		{
			return (this.in_Mat != null) ? this.in_Mat.GetHashCode() : 0;
		}
	}
}
