using System;

namespace Matrix_Lib
{
	internal class MatrixDimensionException : ApplicationException
	{
		public MatrixDimensionException()
			: base("Dimension of the two matrices not suitable for this operation !")
		{
		}
	}
}
