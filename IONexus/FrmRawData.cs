﻿using IONexus.Model;
using Matrix_Lib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IONexus
{
    public partial class FrmRawData : Form
    {
        public NexusInput Input { get; set; }
        public DataTable dt { get; set; }
        public FrmRawData()
        {
            InitializeComponent();
        }

        public void InitGrid()
        {
            //grid.DataSource = dt;
            grid.SuspendLayout();
            BindGridManually();
            grid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedHeaders;
            grid.RowHeadersWidth = 60;
            //grid.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
            FillRecordNo();
            FillColumnName();
            FillSummary();
            FormatGridStyle();
            grid.ResumeLayout();
        }

        public void FormatGridStyle()
        {
            for (int c = 1; c < grid.ColumnCount; c++)
            {
                grid.Columns[c].DefaultCellStyle.Format = "N2";
                grid.Columns[c].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            for (int c = 0; c < grid.ColumnCount; c++)
            {
                grid.Rows[0].Cells[c].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            }
            grid.Columns[0].Frozen = true;
            grid.Rows[0].Frozen = true;


        }

        public void BindGridManually()
        {
            for (int c = 0; c < dt.Columns.Count; c++)
            {
                grid.Columns.Add(dt.Columns[c].ColumnName, "");
            }

            for (int r = 0; r < dt.Rows.Count; r++)
            {
                grid.Rows.Add();
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    grid.Rows[r].Cells[c].Value = dt.Rows[r][c];
                }
            }
            grid.Rows.RemoveAt(grid.Rows.Count - 1);
        }

        public void FillSummary()
        {
            //Fill Column first
            //double[] columns = new double[grid.Columns.Count];
            grid.Rows.Add();
            for (int c =0; c< grid.Columns.Count; c++)
            {
                if (c == 0)
                {
                    grid.Rows[grid.Rows.Count - 1].Cells[0].Value = "Sum Column";
                    FormatNoEditCellStyle(grid.Rows.Count - 1, c);
                    //columns[0] = 0;
                    //continue;
                }
                //columns[c] = CalculateColSum(c);
                //grid.Rows[grid.Rows.Count - 1].Cells[c].Value= columns[c];
                FormatNoEditCellStyle(grid.Rows.Count - 1, c);
            }
            ReCalculateColumnSummary();

            grid.Columns.Add("SumRow", "");
            grid.Rows[0].Cells[grid.ColumnCount - 1].Value = "Sum Row";
            FormatNoEditCellStyle(0, grid.ColumnCount - 1);
            grid.Columns[grid.ColumnCount - 1].ReadOnly = true;
            grid.Columns.Add("SumColumn", "");
            grid.Rows[0].Cells[grid.ColumnCount - 1].Value = "Sum Column";
            FormatNoEditCellStyle(0, grid.ColumnCount - 1);
            grid.Columns[grid.ColumnCount - 1].ReadOnly = true;
            grid.Columns.Add("Different", "");
            grid.Rows[0].Cells[grid.ColumnCount - 1].Value = "Different";
            FormatNoEditCellStyle(0, grid.ColumnCount - 1);
            grid.Columns[grid.ColumnCount - 1].ReadOnly = true;

            ReCalculateRowSummary();

            //for (int r = 1; r < grid.Rows.Count; r++)
            //{
            //    grid.Rows[r].Cells[grid.ColumnCount - 3].Value = CalculateRowSum(r);
            //    FormatNoEditCellStyle(r, grid.ColumnCount - 3);
            //}

            //for (int r = 1; r < grid.Rows.Count; r++)
            //{
            //    grid.Rows[r].Cells[grid.ColumnCount - 2].Value = r < columns.Length ? columns[r] : 0;
            //    FormatNoEditCellStyle(r, grid.ColumnCount - 2);
            //}

            //for (int r = 1; r < grid.Rows.Count; r++)
            //{
            //    grid.Rows[r].Cells[grid.ColumnCount - 1].Value = (double)grid.Rows[r].Cells[grid.ColumnCount - 3].Value
            //        - (double)grid.Rows[r].Cells[grid.ColumnCount - 2].Value;
            //    FormatNoEditCellStyle(r, grid.ColumnCount - 1);
            //}
        }

        public void ReCalculateColumnSummary()
        {
            for (int c = 1; c < grid.Columns.Count; c++)
            {
                if (grid.Rows[1].Cells[c].ReadOnly == true) continue;

                grid.Rows[grid.Rows.Count - 1].Cells[c].Value = CalculateColSum(c);
                //FormatNoEditCellStyle(grid.Rows.Count - 1, c);
            }
        }

        public void ReCalculateRowSummary()
        {
            for (int r = 1; r < grid.Rows.Count; r++)
            {
                grid.Rows[r].Cells[grid.ColumnCount - 3].Value = CalculateRowSum(r);
            }

            for (int r = 1; r < grid.Rows.Count; r++)
            {
                object dValue = null;
                if (r < grid.ColumnCount - 3)
                {
                    dValue = grid.Rows[grid.RowCount - 1].Cells[r].Value;
                }
                grid.Rows[r].Cells[grid.ColumnCount - 2].Value = dValue;
            }

            for (int r = 1; r < grid.Rows.Count-1; r++)
            {
                grid.Rows[r].Cells[grid.ColumnCount - 1].Value = (double)grid.Rows[r].Cells[grid.ColumnCount - 3].Value
                    - (double)grid.Rows[r].Cells[grid.ColumnCount - 2].Value;
            }
        }

        //Recalculate

        public void FormatNoEditCellStyle(int r, int c)
        {
            grid.Rows[r].Cells[c].ReadOnly = true;
            grid.Rows[r].Cells[c].Style.BackColor = Color.FromArgb(255, 252, 213, 180);
        }

        public double CalculateColSum(int column)
        {
            double result = 0;
            double value = 0;
            for (int r = 1; r < grid.Rows.Count-1; r++)
            {
                double.TryParse(grid.Rows[r].Cells[column].Value.ToString(), out value);
                result += value;
            }
            return result;
        }

        public double CalculateRowSum(int row)
        {
            double result = 0;
            double value = 0;
            for (int c = 1; c < grid.Columns.Count; c++)
            {
                if (grid.Rows[row].Cells[c].ReadOnly == true) break;
                double.TryParse(grid.Rows[row].Cells[c].Value.ToString(), out value);
                result += value;
            }
            return result;
        }

        private void FillRecordNo()
        {
            grid.RowHeadersDefaultCellStyle.BackColor = Color.FromArgb(255, 252, 213, 180);
            for (int i = 0; i < this.grid.Rows.Count; i++)
            {
                this.grid.Rows[i].HeaderCell.Value = (i + 1).ToString();
                //this.grid.Rows[i].HeaderCell.Style.BackColor = Color.FromArgb(0, 252, 213, 180);
            }
        }

        private void FillColumnName()
        {
            grid.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(255, 252, 213, 180);
            grid.EnableHeadersVisualStyles = false;
            for (int i = 0; i < this.grid.Columns.Count; i++)
            {
                this.grid.Columns[i].HeaderCell.Value = GetCode(i+1);
                //this.grid.Columns[i].HeaderCell.Style.BackColor = Color.FromArgb(0, 252, 213, 180);
            }
        }

        public string GetCode(int number)
        {
            int start = (int)'A' - 1;
            if (number <= 26) return ((char)(number + start)).ToString() + "("+ number.ToString() + ")";

            StringBuilder str = new StringBuilder();
            int nxt = number;

            List<char> chars = new List<char>();

            while (nxt != 0)
            {
                int rem = nxt % 26;
                if (rem == 0) rem = 26;

                chars.Add((char)(rem + start));
                nxt = nxt / 26;

                if (rem == 26) nxt = nxt - 1;
            }


            for (int i = chars.Count - 1; i >= 0; i--)
            {
                str.Append((char)(chars[i]));
            }
            str.Append("(" + number.ToString() +")");
            return str.ToString();
        }

        public int GetIntCode(string colAdress)
        {
            int[] digits = new int[colAdress.Length];
            for (int i = 0; i < colAdress.Length; ++i)
            {
                digits[i] = Convert.ToInt32(colAdress[i]) - 64;
            }
            int mul = 1; int res = 0;
            for (int pos = digits.Length - 1; pos >= 0; --pos)
            {
                res += digits[pos] * mul;
                mul *= 26;
            }
            return res;
        }

        private void FrmRawData_Load(object sender, EventArgs e)
        {
            InitGrid();
        }

        private void grid_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //FillSummary();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            frmInsert insFrm = new frmInsert();
            if (insFrm.ShowDialog() == DialogResult.OK)
            {
                //Do insert Row
                int type = insFrm.ActionType;
                string name = insFrm.OrdinalName;
                int ordinal = insFrm.OrdinalNumber;
                ordinal = ordinal - 1; //To Map To Array In C#
                if (type == 0)
                { //Row
                    if (ordinal <= 0 || ordinal > grid.RowCount - 2) return; // Out Of Range
                    grid.Rows.Insert(ordinal);
                    grid.Rows[ordinal].Cells[0].Value = name;
                    for (int i = ordinal; i < this.grid.Rows.Count; i++)
                    {
                        this.grid.Rows[i].HeaderCell.Value = (i + 1).ToString();
                    }
                    for (int i = 1; i < this.grid.Columns.Count; i++)
                    {
                        grid.Rows[ordinal].Cells[i].Value = 0;
                        if (i >= grid.Columns.Count - 3)
                        {
                            FormatNoEditCellStyle(ordinal, i);
                        }
                    }
                } else
                {
                    if (ordinal <= 0 || ordinal >= grid.ColumnCount - 3) return; // Out Of Range
                    grid.Columns.Insert(ordinal, new DataGridViewTextBoxColumn());
                    grid.Columns[ordinal].DefaultCellStyle.Format = "N2";
                    grid.Columns[ordinal].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    grid.Rows[0].Cells[ordinal].Value = name;
                    grid.Rows[0].Cells[ordinal].Style.Alignment = DataGridViewContentAlignment.MiddleLeft;

                    for (int i = ordinal; i < this.grid.Columns.Count; i++)
                    {
                        //Rename column
                        grid.Columns[i].HeaderCell.Value = GetCode(i + 1);
                    }
                    for (int i = 1; i < grid.Rows.Count; i++)
                    {
                        grid.Rows[i].Cells[ordinal].Value = 0;
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            frmDelete delFrm = new frmDelete();
            if (delFrm.ShowDialog() == DialogResult.OK)
            {
                //Do insert Row
                int type = delFrm.ActionType;
                int ordinal = delFrm.OrdinalNumber;
                ordinal = ordinal - 1; //To Map To Array In C#
                if (type == 0)
                { //Row
                    if (ordinal <= 0 || ordinal > grid.RowCount - 2) return; // Out Of Range
                    grid.Rows.RemoveAt(ordinal);

                    for (int i = ordinal; i < this.grid.Rows.Count; i++)
                    {
                        this.grid.Rows[i].HeaderCell.Value = (i + 1).ToString();
                    }
                    for (int i = 1; i < this.grid.Columns.Count-3; i++)
                    {
                        grid.Rows[grid.RowCount-1].Cells[i].Value = CalculateColSum(i);
                    }
                }
                else
                {
                    if (ordinal <= 0 || ordinal >= grid.ColumnCount - 3) return; // Out Of Range
                    grid.Columns.RemoveAt(ordinal);
                    for (int i = ordinal; i < this.grid.Columns.Count; i++)
                    {
                        //Rename column
                        grid.Columns[i].HeaderCell.Value = GetCode(i + 1);
                    }
                    ReCalculateRowSummary();
                }
            }
        }

        private bool ValidateDifferent(out int rowError)
        {
            rowError = -1;
            ReCalculateColumnSummary();
            ReCalculateRowSummary();
            //TODO Wait for excel and uncomment this check.
            //for (int r = 1; r < grid.Rows.Count - 1; r++)
            //{
            //    if (double.Parse(grid.Rows[r].Cells[grid.ColumnCount - 1].Value.ToString()) != 0) {
            //        rowError = r;
            //        return false;
            //    }
            //}
            return true;
        }

        private void grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }

        private void btnProceed_Click(object sender, EventArgs e)
        {
            int rowError = 0;
            if (ValidateDifferent(out rowError))
            {
                FrmProceed frm = new FrmProceed();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    //Parse Range
                    string bpFrom = frm.bPFrom;
                    string bpTo = frm.bPTo;
                    string ipFrom = frm.ipFrom;
                    string ipTo = frm.ipTo;
                    string name = frm.TableName;
                    int bpFromC=0, bpFromR=0, bpToC=0, bpToR=0, ipFromC=0, ipFromR=0, ipToC=0, ipToR=0;
                    ParseRange(bpFrom, ref bpFromC, ref bpFromR);
                    ParseRange(bpTo, ref bpToC, ref bpToR);
                    ParseRange(ipFrom, ref ipFromC, ref ipFromR);
                    ParseRange(ipTo, ref ipToC, ref ipToR);
                    if (bpToC - bpFromC != bpToR - bpFromR)
                    {
                        MessageBox.Show("Domesitc Total Columns is not equal Total Rows", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (bpToC - bpFromC != ipToC - ipFromC)
                    {
                        MessageBox.Show("Import Total Columns is not equal Domestic Total Columns", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    Input = ProceedGrid(name, bpFromC, bpFromR, bpToC, bpToR, ipFromC, ipFromR, ipToC, ipToR);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            } else
            {
                MessageBox.Show("Data is not balance in row:" + rowError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private NexusInput ProceedGrid(string name, int bpFromC, int bpFromR, int bpToC, int bpToR, int ipFromC, int ipFromR, int ipToC, int ipToR)
        {
            NexusInput data = new NexusInput();
            data.TableName = name;
            int totalCol = Math.Abs(bpFromC - bpToC) + 1;
            List<string> _bpColumns = new List<string>();
            for (int i = bpFromC; i <= bpToC; i++)
                _bpColumns.Add(grid.Rows[i].Cells[0].Value.ToString());
            data.basePriceColumns = _bpColumns.ToArray();
            data.BasePriceData = new Matrix(totalCol, totalCol);
            double dValue = 0;
            for (int r = bpFromR; r <= bpToR; r++)
            {
                for (int c = bpFromC; c <= bpToC; c++)
                {
                    dValue = 0;
                    double.TryParse(grid.Rows[r].Cells[c].Value.ToString(), out dValue);
                    data.BasePriceData[r - bpFromR, c - bpFromC] = dValue;
                }
            }

            //Import part 
            int totalIpCol = Math.Abs(ipFromC - ipToC) + 1;
            List<string> _ipColumns = new List<string>();
            for (int i = ipFromC; i <= ipToC; i++)
                _ipColumns.Add(grid.Rows[0].Cells[i].Value.ToString());
            data.ImportColumns = _ipColumns.ToArray();
            data.ImportData = new Matrix(ipToR-ipFromR+1, totalIpCol);
            for (int r = ipFromR; r <= ipToR; r++)
            {
                for (int c = ipFromC; c <= ipToC; c++)
                {
                    dValue = 0;
                    double.TryParse(grid.Rows[r].Cells[c].Value.ToString(), out dValue);
                    data.ImportData[r - ipFromR, c - ipFromC] = dValue;
                }
            }

            //Final Demand part
            int fdFromC = Math.Max(ipToC, bpToC) + 1;
            int fdToR = Math.Max(ipToR, bpToR);
            List<string> _fdColumns = new List<string>();
            for (int i = fdFromC; i < grid.ColumnCount - 3; i++)
                _fdColumns.Add(grid.Rows[0].Cells[i].Value.ToString());
            data.finalDemandColumns = _fdColumns.ToArray();
            data.FinalDemandData = new Matrix(fdToR - 1, _fdColumns.Count);
            for (int r = 1; r < fdToR; r++)
            {
                for (int c = fdFromC; c < grid.ColumnCount - 3; c++)
                {
                    dValue = 0;
                    double.TryParse(grid.Rows[r].Cells[c].Value.ToString(), out dValue);
                    data.FinalDemandData[r - 1, c - fdFromC] = dValue;
                }
            }

            //Primary Part
            int prToC = Math.Max(ipToC, bpToC);
            int prFromR = Math.Max(ipToR, bpToR) + 1;
            List<string> _prColumns = new List<string>();
            for (int i = prFromR; i < grid.RowCount - 1; i++)
                _prColumns.Add(grid.Rows[i].Cells[0].Value.ToString());
            data.primaryFactorRows = _prColumns.ToArray();
            data.PrimaryFactorData = new Matrix(_prColumns.Count, prToC - 1);
            for (int r = prFromR; r < grid.RowCount - 1; r++)
            {
                for (int c = 1; c < prToC; c++)
                {
                    dValue = 0;
                    double.TryParse(grid.Rows[r].Cells[c].Value.ToString(), out dValue);
                    data.PrimaryFactorData[r - prFromR, c - 1] = dValue;
                }
            }


            return data;
        }

        private void ParseRange(string value, ref int column, ref int row)
        {
            int i = 0;
            while (i < value.Length)
            {
                if (char.IsLetter(value[i]))
                    i++;
                else
                    break;
            }
            column = GetIntCode(value.Substring(0, i));
            int.TryParse(value.Substring(i), out row);
        }
    }
}
