using IONexus.Model;
//using MsgBox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace IONexus
{
	public class frmAgg : Form
	{

		public ListView lbAggs;

		public ListView lbMaster;

		public ListView lbSectInAgg;

		public Button cmdAddAgg;

		public Button cmdRemAgg;

		public Button cmdEditAgg;

		public Button cmdAddSect;

		public Button cmdRemSect;

		public Button cmdCancel;

		public Button cmdSet;

		public Button cmdSave;

		public Button Button1;

		//private NexusInput _NexusInput;

        public NexusOutput NexusOut { get; set; }


        //private List<clsAgg> clsAggs;

        //private List<clsSect> clsSects;

        //public int[] Agg65;

        //public int[] Agg67;

        //public string[] hdrInd;

        //public string[] hdrComm;

        //public string[] hdrIndAbbr;
        public Label label;
        public Label label2;
        private ColumnHeader col1;
        private ColumnHeader colSec1;
        private ColumnHeader colInAgg;
        public Label label3;
        //public string[] hdrCommAbbr;

		public frmAgg()
		{
			//base.Load += this.frmAgg_Load;
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
				base.Dispose(disposing);
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.lbAggs = new System.Windows.Forms.ListView();
            this.lbMaster = new System.Windows.Forms.ListView();
            this.lbSectInAgg = new System.Windows.Forms.ListView();
            this.cmdAddAgg = new System.Windows.Forms.Button();
            this.cmdRemAgg = new System.Windows.Forms.Button();
            this.cmdEditAgg = new System.Windows.Forms.Button();
            this.cmdAddSect = new System.Windows.Forms.Button();
            this.cmdRemSect = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdSet = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.col1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSec1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colInAgg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lbAggs
            // 
            this.lbAggs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col1});
            this.lbAggs.FullRowSelect = true;
            this.lbAggs.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lbAggs.Location = new System.Drawing.Point(39, 56);
            this.lbAggs.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.lbAggs.MultiSelect = false;
            this.lbAggs.Name = "lbAggs";
            this.lbAggs.ShowGroups = false;
            this.lbAggs.Size = new System.Drawing.Size(319, 152);
            this.lbAggs.TabIndex = 0;
            this.lbAggs.UseCompatibleStateImageBehavior = false;
            this.lbAggs.View = System.Windows.Forms.View.Details;
            this.lbAggs.SelectedIndexChanged += new System.EventHandler(this.lbAggs_SelectedIndexChanged);
            // 
            // lbMaster
            // 
            this.lbMaster.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSec1});
            this.lbMaster.FullRowSelect = true;
            this.lbMaster.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lbMaster.Location = new System.Drawing.Point(38, 284);
            this.lbMaster.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.lbMaster.Name = "lbMaster";
            this.lbMaster.ShowGroups = false;
            this.lbMaster.Size = new System.Drawing.Size(229, 226);
            this.lbMaster.TabIndex = 1;
            this.lbMaster.UseCompatibleStateImageBehavior = false;
            this.lbMaster.View = System.Windows.Forms.View.Details;
            // 
            // lbSectInAgg
            // 
            this.lbSectInAgg.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colInAgg});
            this.lbSectInAgg.FullRowSelect = true;
            this.lbSectInAgg.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lbSectInAgg.Location = new System.Drawing.Point(393, 284);
            this.lbSectInAgg.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.lbSectInAgg.Name = "lbSectInAgg";
            this.lbSectInAgg.Size = new System.Drawing.Size(234, 226);
            this.lbSectInAgg.TabIndex = 2;
            this.lbSectInAgg.UseCompatibleStateImageBehavior = false;
            this.lbSectInAgg.View = System.Windows.Forms.View.Details;
            // 
            // cmdAddAgg
            // 
            this.cmdAddAgg.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAddAgg.Location = new System.Drawing.Point(452, 56);
            this.cmdAddAgg.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdAddAgg.Name = "cmdAddAgg";
            this.cmdAddAgg.Size = new System.Drawing.Size(112, 39);
            this.cmdAddAgg.TabIndex = 6;
            this.cmdAddAgg.Text = "Add Sector";
            this.cmdAddAgg.UseVisualStyleBackColor = true;
            this.cmdAddAgg.Click += new System.EventHandler(this.cmdAddAgg_Click);
            // 
            // cmdRemAgg
            // 
            this.cmdRemAgg.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRemAgg.Location = new System.Drawing.Point(452, 105);
            this.cmdRemAgg.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdRemAgg.Name = "cmdRemAgg";
            this.cmdRemAgg.Size = new System.Drawing.Size(112, 39);
            this.cmdRemAgg.TabIndex = 7;
            this.cmdRemAgg.Text = "Remove";
            this.cmdRemAgg.UseVisualStyleBackColor = true;
            this.cmdRemAgg.Click += new System.EventHandler(this.cmdRemAgg_Click);
            // 
            // cmdEditAgg
            // 
            this.cmdEditAgg.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEditAgg.Location = new System.Drawing.Point(452, 154);
            this.cmdEditAgg.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdEditAgg.Name = "cmdEditAgg";
            this.cmdEditAgg.Size = new System.Drawing.Size(112, 39);
            this.cmdEditAgg.TabIndex = 8;
            this.cmdEditAgg.Text = "Rename";
            this.cmdEditAgg.UseVisualStyleBackColor = true;
            this.cmdEditAgg.Click += new System.EventHandler(this.cmdEditAgg_Click);
            // 
            // cmdAddSect
            // 
            this.cmdAddSect.Location = new System.Drawing.Point(306, 354);
            this.cmdAddSect.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdAddSect.Name = "cmdAddSect";
            this.cmdAddSect.Size = new System.Drawing.Size(52, 39);
            this.cmdAddSect.TabIndex = 9;
            this.cmdAddSect.Text = ">>";
            this.cmdAddSect.UseVisualStyleBackColor = true;
            this.cmdAddSect.Click += new System.EventHandler(this.cmdAddSect_Click);
            // 
            // cmdRemSect
            // 
            this.cmdRemSect.Location = new System.Drawing.Point(306, 403);
            this.cmdRemSect.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdRemSect.Name = "cmdRemSect";
            this.cmdRemSect.Size = new System.Drawing.Size(52, 39);
            this.cmdRemSect.TabIndex = 10;
            this.cmdRemSect.Text = "<<";
            this.cmdRemSect.UseVisualStyleBackColor = true;
            this.cmdRemSect.Click += new System.EventHandler(this.cmdRemSect_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancel.Location = new System.Drawing.Point(515, 547);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(112, 39);
            this.cmdCancel.TabIndex = 11;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSet
            // 
            this.cmdSet.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdSet.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSet.Location = new System.Drawing.Point(347, 547);
            this.cmdSet.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdSet.Name = "cmdSet";
            this.cmdSet.Size = new System.Drawing.Size(144, 39);
            this.cmdSet.TabIndex = 12;
            this.cmdSet.Text = "Aggregate";
            this.cmdSet.UseVisualStyleBackColor = true;
            this.cmdSet.Click += new System.EventHandler(this.cmdSet_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Location = new System.Drawing.Point(113, 547);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(108, 39);
            this.cmdSave.TabIndex = 13;
            this.cmdSave.Text = "Save as";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.SaveAggregationScheme);
            // 
            // Button1
            // 
            this.Button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.Location = new System.Drawing.Point(238, 547);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(91, 39);
            this.Button1.TabIndex = 14;
            this.Button1.Text = "Load";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(34, 29);
            this.label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(235, 37);
            this.label.TabIndex = 3;
            this.label.Text = "Aggregate Sectors";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 254);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(295, 37);
            this.label2.TabIndex = 4;
            this.label2.Text = "Un-Aggregated Sectors";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(389, 254);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(319, 37);
            this.label3.TabIndex = 5;
            this.label3.Text = "Sectors within Aggregate";
            // 
            // col1
            // 
            this.col1.Text = "";
            // 
            // colSec1
            // 
            this.colSec1.Text = "";
            // 
            // colInAgg
            // 
            this.colInAgg.Text = "";
            // 
            // frmAgg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 601);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.cmdSet);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdRemSect);
            this.Controls.Add(this.cmdAddSect);
            this.Controls.Add(this.cmdEditAgg);
            this.Controls.Add(this.cmdRemAgg);
            this.Controls.Add(this.cmdAddAgg);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label);
            this.Controls.Add(this.lbSectInAgg);
            this.Controls.Add(this.lbMaster);
            this.Controls.Add(this.lbAggs);
            this.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frmAgg";
            this.Text = "Aggregation Form";
            this.Load += new System.EventHandler(this.frmAgg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		private void frmAgg_Load(object sender, EventArgs e)
		{
			this.LoadData();
			this.PopulateForm();
		}

		//private void LoadDataAfterAggLoad()
		//{
		//	checked
		//	{
		//		int num = this.hdrInd.Length - 1;
		//		for (int i = 0; i <= num; i++)
		//		{
		//			bool flag = false;
		//			if (i > this.hdrInd.Length - 5)
		//			{
		//				flag = true;
		//			}
		//		}
		//	}
		//}

		private void LoadData()
		{
        }

        private void PopulateForm()
		{
			this.PopulateAggregates();
			this.PopulateMaster();
            lbAggs.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            lbMaster.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            lbSectInAgg.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void PopulateAggregates()
		{
            if (NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList == null) return;
			this.lbSectInAgg.Items.Clear();
			this.lbAggs.Items.Clear();
			foreach (AggreateGroup agg in NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList)
			{
                ListViewItem item = new ListViewItem(agg.Name);
                item.Tag = agg.Position;

                this.lbAggs.Items.Add(item);
			}
		}

		private void PopulateMaster()
		{
            if (NexusOut.SummarySteps[NexusOut.CurrentStep].ParentSector == null) return;
			this.lbMaster.Items.Clear();
            int i = 0;
			foreach (Sector sec in NexusOut.SummarySteps[NexusOut.CurrentStep].ParentSector)
			{
                if (!sec.IsGrouped && !sec.IsSepareted)
                {
                    ListViewItem item = new ListViewItem(sec.Name);
                    item.Tag = i;
                    lbMaster.Items.Add(item);
                }
                i++;
            }
		}

		private void cmdAddSect_Click(object sender, EventArgs e)
		{
			if (lbAggs.SelectedItems.Count > 0 && !string.IsNullOrEmpty(lbMaster.SelectedItems[0].ToString()))
			{
                foreach (ListViewItem source in lbMaster.SelectedItems)
                {
                    ListViewItem item = new ListViewItem(source.Text);
                    item.Tag = source.Tag;
                    lbSectInAgg.Items.Add(item);
                    int i = (int)item.Tag;
                    Sector sec = NexusOut.SummarySteps[NexusOut.CurrentStep].ParentSector[i];
                    sec.IsGrouped = true;
                    NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList[lbAggs.SelectedIndices[0]].Children.Add(sec);
                    lbMaster.Items.RemoveAt(lbMaster.SelectedIndices[0]);
                }
            }
		}

		private void cmdEditAgg_Click(object sender, EventArgs e)
		{
			//if (!string.IsNullOrEmpty(this.lbAggs.SelectedIndices[0].ToString()))
			//{
   //             //string text = Interaction.InputBox("Enter new Selected Aggregate Name", "Aggregate Title", this.lbAggs.SelectedItem.ToString(), -1, -1);
   //             DialogResult res = InputBox.ShowDialog("Enter new Selected Aggregate Name",
   //                         "Aggregate Title", InputBox.Icon.Information, InputBox.Buttons.OkCancel, InputBox.Type.TextBox);
   //             string text = "";
   //             if (res == System.Windows.Forms.DialogResult.OK || res == System.Windows.Forms.DialogResult.Yes)
   //             {
   //                 text = InputBox.ResultValue;
   //             }

   //             if (text.Length > 0)
			//	{
   //                 foreach (AggreateGroup clsAgg in NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList)
   //                 {
   //                     if (clsAgg.Name.Equals(lbAggs.SelectedItems[0].ToString()))
   //                     {
   //                         clsAgg.Name = text;
   //                         break;
   //                     }
   //                 }
   //                 ListViewItem item = lbAggs.SelectedItems[0];
   //                 item.Text = text;
   //                 //int selectedIndex = lbAggs.SelectedIndices[0];
   //                 //this.lbAggs.Items.RemoveAt(selectedIndex);
   //                 //this.lbAggs.Items.Insert(selectedIndex, text);
   //                 //lbAggs.SelectedIndices.Add(selectedIndex);
   //                 //this.lbAggs.SelectedIndices[0] = selectedIndex;
   //             }
			//}
		}

		private void cmdCancel_Click(object sender, EventArgs e)
		{
			base.Close();
			base.Dispose();
		}

		private void lbAggs_SelectedIndexChanged(object sender, EventArgs e)
		{
            lbSectInAgg.Items.Clear();
            if (this.lbAggs.SelectedItems.Count > 0)
            {
                ListViewItem item = lbAggs.SelectedItems[0];
                AggreateGroup group = NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList[(int)item.Tag];
                foreach (Sector clsSect in group.Children)
                {
                    ListViewItem inItem = new ListViewItem(clsSect.Name);
                    inItem.Tag = clsSect.Position;
                    this.lbSectInAgg.Items.Add(inItem);
                }
            }
        }

		private void cmdRemAgg_Click(object sender, EventArgs e)
		{
            if (lbAggs.SelectedItems.Count > 0)
            {
                int i = (int)lbAggs.SelectedItems[0].Tag;
                AggreateGroup group = NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList[i];
                foreach (Sector sec in group.Children)
                {
                    Sector baseSec = NexusOut.SummarySteps[NexusOut.CurrentStep].ParentSector[sec.Position];
                    baseSec.IsGrouped = false;
                }
                group.Children.Clear();
                lbSectInAgg.Items.Clear();
                lbAggs.Items.RemoveAt(lbAggs.SelectedIndices[0]);
                PopulateMaster();
            }
        }

		private void cmdAddAgg_Click(object sender, EventArgs e)
		{
            //string text = Interaction.InputBox("Enter Aggregate Sector Name", "Aggregate Title", "", -1, -1);
            //DialogResult res = InputBox.ShowDialog("Enter Aggregate Sector Name",
            //            "Aggregate Title", InputBox.Icon.Information, InputBox.Buttons.OkCancel, InputBox.Type.TextBox);
            //string text = "";
            //if (res == System.Windows.Forms.DialogResult.OK || res == System.Windows.Forms.DialogResult.Yes)
            //{
            //    text = InputBox.ResultValue;
            //}

            //if (text.Length > 0)
            //{
            //    AggreateGroup clsAgg = new AggreateGroup();
            //    clsAgg.Name = text;
            //    clsAgg.Position = NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList.Count;
            //    NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList.Add(clsAgg);
            //    ListViewItem item = new ListViewItem(text);
            //    item.Tag = clsAgg.Position;
            //    lbAggs.Items.Add(item);
            //    lbAggs.SelectedIndices.Add(0);
            //}
            //lbAggs.SelectedIndices.Add(0);
		}

		private void cmdRemSect_Click(object sender, EventArgs e)
		{
            if (this.lbSectInAgg.Items.Count > 0 && this.lbSectInAgg.SelectedItems != null && NexusOut.SummarySteps[NexusOut.CurrentStep].ParentSector.Count > 0)
            {
                foreach (ListViewItem inItem in lbSectInAgg.SelectedItems)
                {
                    int i = (int)inItem.Tag;
                    Sector clsSect = NexusOut.SummarySteps[NexusOut.CurrentStep].ParentSector[i];
                    if (clsSect.Name.Equals(inItem.Text))
                    {
                        clsSect.IsGrouped = false;
                        AggreateGroup group = NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList[lbAggs.SelectedIndices[0]];
                        if (group.Name.Equals(lbAggs.SelectedItems[0].ToString()))
                        {
                            group.Children.Remove(clsSect);
                        }
                    }

                    lbSectInAgg.Items.Remove(inItem);
                }
                PopulateMaster();
			}
		}

		private void cmdSet_Click(object sender, EventArgs e)
		{
			SetAgg();
			base.Close();
		}

		private void SetAgg()
		{

		}

		private void SaveAggregationScheme(object sender, EventArgs e)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Title = "Enter Filename to Save Aggregation Scheme";
            saveFileDialog.InitialDirectory = Environment.CurrentDirectory;
			saveFileDialog.Filter = "Aggregation Scheme files (*.agg)|*.agg|All files (*.*)|*.*";
			saveFileDialog.FilterIndex = 1;
			saveFileDialog.RestoreDirectory = true;
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				string fileName = saveFileDialog.FileName;
				string path = saveFileDialog.FileName + "_bin";
				try
				{
                    using (var ms = new MemoryStream())
                    {
                        binaryFormatter.Serialize(ms, NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList);
                        byte[] bytes = ms.ToArray();

                        FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                        fileStream.Write(bytes, 0, bytes.Length);
                        //binaryFormatter.Serialize(fileStream, str);
                        fileStream.Close();
                        string graph = Convert.ToBase64String(bytes);
                        fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        binaryFormatter.Serialize(fileStream, graph);
                        fileStream.Close();
                    }
				}
				catch (Exception ex)
				{
					Exception ex2 = ex;
				}
			}
		}

		public bool LoadAgg(string URL = "")
		{
            bool result = false;
			if (URL.Length == 0)
			{
				OpenFileDialog openFileDialog = new OpenFileDialog();
				openFileDialog.Title = "Select Aggregation Scheme to Load";
                openFileDialog.InitialDirectory = Environment.CurrentDirectory;
				openFileDialog.Filter = "agg files (*.agg)|*.agg|All files (*.*)|*.*";
				openFileDialog.FilterIndex = 1;
				openFileDialog.RestoreDirectory = true;
				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					URL = openFileDialog.FileName;
				}
			}
            try
            {
                using (var fileStream = new FileStream(URL, FileMode.Open, FileAccess.Read))
                {
                    var binForm = new BinaryFormatter();
                    string base64 = (string)binForm.Deserialize(fileStream);
                    fileStream.Close();
                    byte[] bytes = Convert.FromBase64String(base64);
                    List<AggreateGroup> group = bytes.Deserialize<List<AggreateGroup>>();
                    foreach (AggreateGroup g in group)
                    {
                        foreach (Sector s in g.Children)
                        {
                            foreach (Sector bs in NexusOut.SummarySteps[NexusOut.CurrentStep].ParentSector)
                            {
                                if (bs.Name.Equals(s.Name, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    bs.IsGrouped = true;
                                    s.Position = bs.Position;
                                    break;
                                }
                            }
                        }
                    }
                    NexusOut.SummarySteps[NexusOut.CurrentStep].AggregateList = group;
                    PopulateAggregates();
                    PopulateMaster();
                }
            } catch (Exception ex)
            {
                MessageBox.Show("Error in Load Aggregate file:" +
                    ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return result;
        }



		private void LoadButton_Click(object sender, EventArgs e)
		{
			string text = "";
			OpenFileDialog openFileDialog = new OpenFileDialog();
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			string text2 = "";
			openFileDialog.Title = "Select Aggregation Scheme to Load";
            openFileDialog.InitialDirectory = Environment.CurrentDirectory;
			openFileDialog.Filter = "IO-SNAP aggregation data files (*.agg)|*.agg";
			openFileDialog.FilterIndex = 1;
			openFileDialog.RestoreDirectory = true;
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				text = openFileDialog.FileName;
                LoadAgg(text);
			}
		}
	}
}
