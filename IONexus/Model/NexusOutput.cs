﻿using Matrix_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IONexus.Model
{
    [Serializable]
    public class NexusOutput
    {
        public List<NexusSummary> SummarySteps { get; set; }
        public int CurrentStep { get; set; }

        public NexusOutput()
        {
            SummarySteps = new List<NexusSummary>();
            //AggregateList = new List<AggreateGroup>();
            //Columns = new List<BaseGroup>();
            //SeparateList = new List<SeparateGroup>();
        }

        public void DoAggregate()
        {
        }

        public void ReCalculateAggregate()
        {

            //if (AggregateList.Count > 0)
            //{
            //    int[] MapRowAggregate = new int[BaseInput.BasePriceData.NoRows];
            //    AggregateData = new List<Vector>();
            //    for (int i=0; i < AggregateList.Count; i++)
            //    {
            //        AggreateGroup g = AggregateList[i];
            //        Vector r = new Vector(BaseInput.BasePriceData.NoRows);
            //        Vector c = new Vector(BaseInput.BasePriceData.NoCols);
            //        foreach (Sector s in g.Children)
            //        {
            //            if (s.Position >= MapRowAggregate.Length) continue;
            //            MapRowAggregate[s.Position] = i;
            //            Vector v = BaseInput.BasePriceData.getRowVector(s.Position);
            //            r = r + v;
            //            Vector v2 = BaseInput.BasePriceData.getColVector(s.Position);
            //            c = c + v2;
            //        }
            //        Vector total = new Vector(r.Count + c.Count);
            //        //Fill total vector
            //        //int passCol = 0;
            //        for (int j=0; j < r.Count; j++)
            //        {
            //            total[j] = r[j];
            //            //if (MapRowAggregate[j] == i)
            //            //{
            //            //    //Sum to first.
            //            //    total[0] = total[0] + r[j];
            //            //    passCol += 1;
            //            //} else
            //            //{
            //            //    //Enter to correct position
            //            //    total[j + 1 - passCol] = r[j];
            //            //}
            //        }
            //        for (int j = 0; j < c.Count; j++)
            //        {
            //            total[r.Count + j] = c[j];
            //            //if (MapRowAggregate[j] == i)
            //            //{
            //            //    //Don't calculate in columns.
            //            //}
            //            //else
            //            //{
            //            //    //Enter to correct position
            //            //    total[r.Count - passCol + j + 1 - passCol] = c[j];
            //            //}
            //        }
            //        AggregateData.Add(total);
            //    }
            //}
        }
        public NexusSummary CalculateSummary()
        {
            NexusSummary sum = new NexusSummary();
            //int passedCol = 0;
            //sum.FinalData = new Matrix(BaseInput.BasePriceData.NoRows, 1);

            //bool flagNew = true;
            //for (int i=0;i< BaseInput.BasePriceData.NoCols; i++)
            //{
            //    if (!BaseSector[i].IsGrouped && !BaseSector[i].IsSepareted)
            //    {
            //        sum.Columns.Add(new BaseGroup(BaseSector[i].Name, i - passedCol));
            //        if (flagNew)
            //        {
            //            for (int j = 0; j < sum.FinalData.NoCols; j++)
            //            {
            //                sum.FinalData[0, j] = BaseInput.BasePriceData.getColVector(i)[j];
            //            }
            //            flagNew = false;
            //        }
            //        else
            //        {
            //            sum.FinalData.Append(BaseInput.BasePriceData.getColVector(i).toArray, null);
            //        }
            //    } else if (BaseSector[i].IsGrouped)
            //    {
            //        passedCol += 1;
            //    }
            //}

            //for (int i=0; i < AggregateList.Count; i++)
            //{
            //    sum.Columns.Add(new BaseGroup(AggregateList[i].Name, BaseSector.Count - passedCol + i));
            //    double[] col = new double[BaseInput.BasePriceData.NoCols];
            //    for (int j=0;j< BaseInput.BasePriceData.NoRows; j++)
            //    {
            //        Vector total = AggregateData[i];
            //        col[j] = total[j];
            //    }
            //    sum.FinalData.Append(col, null);
            //}
            ////Remove Row that is aggregated.
            //for (int i = BaseInput.BasePriceData.NoRows -1; i>=0 ; i--)
            //{
            //    if (BaseSector[i].IsGrouped && i < sum.FinalData.NoRows)
            //    {
            //        //Remove this row
            //        sum.FinalData.Remove(i);
            //    }
            //}

            return sum;
        }

        public List<Vector> AggregateData { get; set; }

        public NexusInput BaseInput { get; set; }

        //public List<AggreateGroup> AggregateList { get; set; }

        //public List<SeparateGroup> SeparateList { get; set; }

        private NexusSummary _summaryData;
        public NexusSummary SummaryData
        {
            get
            {
                if (_summaryData == null)
                {
                    _summaryData = CalculateSummary();
                }
                return _summaryData;
            }
        }

    }
}
