using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace IONexus.Model
{
	[Serializable]
	public class RegionalData : ICloneable
	{
		public string[] States;

		private ArrayList stnames;

		public List<StateData> StateDt;

		private int isize;

		public RegionalData(string[] _states, int arrsize)
		{
			this.stnames = new ArrayList();
			this.StateDt = new List<StateData>();
			this.isize = 0;
			this.States = _states;
			this.isize = arrsize;
			string[] states = this.States;
			foreach (string text in states)
			{
				StateData item = new StateData(text, arrsize);
				this.stnames.Add(text);
				this.StateDt.Add(item);
			}
		}

		public RegionalData()
		{
			this.stnames = new ArrayList();
			this.StateDt = new List<StateData>();
			this.isize = 0;
		}

		public void addState(string sname, ref StateData sstate)
		{
			this.stnames.Add(sname);
			this.StateDt.Add(sstate);
			this.States = (string[])this.stnames.ToArray(typeof(string));
		}

		public StateData GetState(ref string stname)
		{
			StateData result = this.StateDt[0];
			foreach (StateData item in this.StateDt)
			{
				if (item.Name.Equals(stname))
				{
					result = item;
				}
			}
			return result;
		}

		public void replaceState(object stname, ref StateData sts)
		{
			bool flag = false;
			StateData item = default(StateData);
			foreach (StateData item2 in this.StateDt)
			{
				if (item2.Name.Equals(RuntimeHelpers.GetObjectValue(stname)))
				{
					flag = true;
					item = item2;
				}
			}
			if (flag)
			{
				this.StateDt.Remove(item);
				this.StateDt.Add(sts);
			}
		}

		public object Clone()
		{
			RegionalData regionalData = new RegionalData();
			regionalData.States = this.States;
			regionalData.isize = this.isize;
			string[] states = this.States;
			for (int i = 0; i < states.Length; i = checked(i + 1))
			{
				string st = states[i];
				StateData stateData = new StateData(st, regionalData.isize);
				stateData = (StateData)this.GetState(ref st).Clone();
				regionalData.StateDt.Add(stateData);
			}
			return regionalData;
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Clone
			return this.Clone();
		}

		public int getNumberOfStates()
		{
			return this.StateDt.Count;
		}

		public string[] getStatesNames()
		{
			return this.States;
		}

		public void Aggregate(int[] AggInd, int[] AggComm, int N_IND_BASE, ref FTE_RATIO_CLASS fte_bis)
		{
			foreach (StateData item in this.StateDt)
			{
				item.Aggregate(AggInd, AggComm, N_IND_BASE, fte_bis);
			}
		}
	}
}
