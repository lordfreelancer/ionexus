﻿using Matrix_Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IONexus.Model
{
    [Serializable]
    public class NexusInput
    {
        public string TableName { get; set; }

        public string[] basePriceColumns;
        public Matrix BasePriceData { get; set; }

        public string[] ImportColumns;
        public Matrix ImportData { get; set; }

        public string[] primaryFactorRows;
        public Matrix PrimaryFactorData { get; set; }

        public string[] finalDemandColumns;
        public Matrix FinalDemandData { get; set; }

        public static NexusInput Parse(DataSet dataset)
        {
            NexusInput data = new NexusInput();

            //Parse Base Price
            DataTable dt = dataset.Tables["Basic price"];
            List<string> _bpColumns = new List<string>();
            List<string> _bpRow = new List<string>();
            for (int i = 0; i < dt.Columns.Count; i++)
                _bpRow.Add(dt.Rows[0][i].ToString());
            for (int i = 1; i < dt.Rows.Count; i++)
                _bpColumns.Add(dt.Rows[i][0].ToString());

            data.basePriceColumns = _bpColumns.ToArray();
            //data.basePriceRow = _bpRow.ToArray();
            data.BasePriceData = new Matrix(dt.Rows.Count-1, dt.Columns.Count-1);
            double dValue = 0;
            for (int r = 1; r < dt.Rows.Count; r++)
            {
                for (int c = 1; c < dt.Columns.Count; c++)
                {
                    dValue = 0;
                    double.TryParse(dt.Rows[r][c].ToString(), out dValue);
                    data.BasePriceData[r - 1, c - 1] = dValue;
                }
            }

            //Parse Primary Factor
            dt = dataset.Tables["Primary Factor"];
            List<string> _pfColumn = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
                _pfColumn.Add(dt.Rows[i][0].ToString());

            data.primaryFactorRows = _pfColumn.ToArray();
            data.PrimaryFactorData = new Matrix(dt.Rows.Count, dt.Columns.Count - 1);
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                for (int c = 1; c < dt.Columns.Count; c++)
                {
                    dValue = 0;
                    double.TryParse(dt.Rows[r][c].ToString(), out dValue);
                    data.PrimaryFactorData[r, c - 1] = dValue;
                }
            }

            dt = dataset.Tables["Final demand"];
            List<string> _fdRow = new List<string>();
            for (int i = 0; i < dt.Columns.Count; i++)
                _fdRow.Add(dt.Rows[0][i].ToString());

            data.finalDemandColumns = _fdRow.ToArray();
            data.FinalDemandData = new Matrix(dt.Rows.Count - 1, dt.Columns.Count);
            for (int r = 1; r < dt.Rows.Count; r++)
            {
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    dValue = 0;
                    double.TryParse(dt.Rows[r][c].ToString(), out dValue);
                    data.FinalDemandData[r - 1, c] = dValue;
                }
            }

            return data;
        }
        public NexusInput()
        {
        }
        public object Clone()
        {
            NexusInput data = new NexusInput();
            data.BasePriceData = this.BasePriceData.Clone();
            data.PrimaryFactorData = this.PrimaryFactorData.Clone();
            data.FinalDemandData = this.FinalDemandData.Clone();
            data.basePriceColumns = (string[])this.basePriceColumns.Clone();
            data.primaryFactorRows = (string[])this.primaryFactorRows.Clone();
            data.finalDemandColumns = (string[])this.finalDemandColumns.Clone();
            return data;
        }
    }
}
