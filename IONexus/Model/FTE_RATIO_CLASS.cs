using Matrix_Lib;
using System;

namespace IONexus.Model
{
	[Serializable]
	public class FTE_RATIO_CLASS : ICloneable
	{
		public Vector jobs2fte_ratio_imputed;

		public Vector jobs2fte_ratio_fedstate;

		public Vector jobs2fte_ratio_totalgov;

		public GenDataStatus GovAggLevel;

		public Vector fte2jobs_ratio_imputed
		{
			get
			{
				Vector vector = new Vector(this.jobs2fte_ratio_imputed.Count);
				int num = checked(this.jobs2fte_ratio_imputed.Count - 1);
				for (int i = 0; i <= num; i = checked(i + 1))
				{
					vector[i] = 1.0 / this.jobs2fte_ratio_imputed[i];
				}
				return vector;
			}
		}

		public Vector fte2jobs_ratio_fedstate
		{
			get
			{
				Vector vector = new Vector(this.jobs2fte_ratio_fedstate.Count);
				int num = checked(this.jobs2fte_ratio_fedstate.Count - 1);
				for (int i = 0; i <= num; i = checked(i + 1))
				{
					vector[i] = 1.0 / this.jobs2fte_ratio_fedstate[i];
				}
				return vector;
			}
		}

		public Vector fte2jobs_ratio_totalgov
		{
			get
			{
				Vector vector = new Vector(this.jobs2fte_ratio_totalgov.Count);
				int num = checked(this.jobs2fte_ratio_totalgov.Count - 1);
				for (int i = 0; i <= num; i = checked(i + 1))
				{
					vector[i] = 1.0 / this.jobs2fte_ratio_totalgov[i];
				}
				return vector;
			}
		}

		public FTE_RATIO_CLASS()
		{
			this.GovAggLevel = new GenDataStatus();
		}

		public void setupFTE(ref Vector fte_imp, ref Vector emp_imp)
		{
			int num = 0;
			num = fte_imp.Count;
			this.jobs2fte_ratio_imputed = fte_imp;
			double num3;
			double num4;
			double num5;
			double num6;
			double num7;
			checked
			{
				this.jobs2fte_ratio_totalgov = new Vector(num - 3);
				this.jobs2fte_ratio_fedstate = new Vector(num - 2);
				int num2 = num - 4;
				for (int i = 0; i <= num2; i++)
				{
					this.jobs2fte_ratio_totalgov[i] = fte_imp[i];
					this.jobs2fte_ratio_fedstate[i] = fte_imp[i];
				}
				num3 = 0.0;
				num4 = 0.0;
				num5 = 0.0;
				num6 = 0.0;
				num7 = 0.0;
			}
			num3 = emp_imp[checked(num - 4)] + emp_imp[checked(num - 3)] + emp_imp[checked(num - 2)] + emp_imp[checked(num - 1)];
			num4 = fte_imp[checked(num - 4)] * emp_imp[checked(num - 4)];
			num5 = fte_imp[checked(num - 3)] * emp_imp[checked(num - 3)];
			num6 = fte_imp[checked(num - 2)] * emp_imp[checked(num - 2)];
			num7 = fte_imp[checked(num - 1)] * emp_imp[checked(num - 1)];
			this.jobs2fte_ratio_totalgov[checked(num - 4)] = (num7 + num6 + num5 + num4) / num3;
			double num8 = 0.0;
			double num9 = 0.0;
			num8 = emp_imp[checked(num - 4)] + emp_imp[checked(num - 3)];
			num9 = emp_imp[checked(num - 2)] + emp_imp[checked(num - 1)];
			this.jobs2fte_ratio_fedstate[checked(num - 4)] = (num5 + num4) / num8;
			this.jobs2fte_ratio_fedstate[checked(num - 3)] = (num7 + num6) / num9;
		}

		public Vector getFTE_RATIO()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.jobs2fte_ratio_fedstate;
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.jobs2fte_ratio_totalgov;
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.jobs2fte_ratio_imputed;
			default:
				return this.jobs2fte_ratio_imputed;
			}
		}

		public object Clone()
		{
			FTE_RATIO_CLASS fTE_RATIO_CLASS = new FTE_RATIO_CLASS();
			FTE_RATIO_CLASS fTE_RATIO_CLASS2 = fTE_RATIO_CLASS;
			fTE_RATIO_CLASS2.jobs2fte_ratio_imputed = this.jobs2fte_ratio_imputed.Clone();
			fTE_RATIO_CLASS2.jobs2fte_ratio_fedstate = this.jobs2fte_ratio_fedstate.Clone();
			fTE_RATIO_CLASS2.jobs2fte_ratio_totalgov = this.jobs2fte_ratio_totalgov.Clone();
			fTE_RATIO_CLASS2.GovAggLevel = (GenDataStatus)this.GovAggLevel.Clone();
			fTE_RATIO_CLASS2 = null;
			return fTE_RATIO_CLASS;
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Clone
			return this.Clone();
		}

		public void Aggregate(int[] AggInd, int[] AggComm, int N_IND, Vector Emp_F, Vector Emp_J, ref GenDataStatus g)
		{
			int num = 0;
			int upperBound = AggInd.GetUpperBound(0);
			for (int i = 0; i <= upperBound; i = checked(i + 1))
			{
				if (AggInd[i] > num)
				{
					num = AggInd[i];
				}
			}
			this.GovAggLevel = new GenDataStatus();
			Vector vector = new Vector(1);
			Vector vector2 = new Vector(1);
			Vector vector3 = new Vector(1);
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				vector2 = Emp_F / Emp_J;
				break;
			case GenDataStatus.dataStatusType.TotalGovernment:
				vector3 = Emp_F / Emp_J;
				break;
			case GenDataStatus.dataStatusType.ImputedGov:
				vector = Emp_F / Emp_J;
				break;
			default:
				//Interaction.MsgBox("Code FTE 5787 - unknown government data aggregation method", MsgBoxStyle.OkOnly, null);
				break;
			}
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				//Interaction.MsgBox("Agg code 6799 - Not Implemented", MsgBoxStyle.OkOnly, null);
				break;
			case GenDataStatus.dataStatusType.TotalGovernment:
				//Interaction.MsgBox("Agg code 6798 - Not Implemented", MsgBoxStyle.OkOnly, null);
				break;
			case GenDataStatus.dataStatusType.ImputedGov:
			{
				Vector vector4;
				Vector vector5;
				Vector vector6;
				Vector vector7;
				checked
				{
					vector4 = new Vector(vector.Count - 4);
					vector5 = new Vector(4);
					vector6 = new Vector(2);
					vector7 = new Vector(1);
					int num2 = vector4.Count - 1;
					for (int j = 0; j <= num2; j++)
					{
						vector4[j] = vector[j];
					}
					int count = vector.Count;
					vector5[3] = vector[count - 1];
					vector5[2] = vector[count - 2];
					vector5[1] = vector[count - 3];
					vector5[0] = vector[count - 4];
				}
				vector6[1] = vector5[2] + vector5[3];
				vector6[0] = vector5[0] + vector5[1];
				vector7[0] = vector5[0] + vector5[1] + vector5[2] + vector5[3];
				this.jobs2fte_ratio_imputed = this.Append2Vectors(ref vector4, ref vector5);
				this.jobs2fte_ratio_fedstate = this.Append2Vectors(ref vector4, ref vector6);
				this.jobs2fte_ratio_totalgov = this.Append2Vectors(ref vector4, ref vector7);
				break;
			}
			default:
				//Interaction.MsgBox("Code FTE 5787 - unknown government data aggregation method", MsgBoxStyle.OkOnly, null);
				break;
			}
		}

		private Vector Append2Vectors(ref Vector v1, ref double v2)
		{
			checked
			{
				Vector vector = new Vector(v1.Count + 1);
				int num = v1.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					vector[i] = v1[i];
				}
				vector[v1.Count] = v2;
				return vector;
			}
		}

		private Vector Append2Vectors(ref Vector v1, ref Vector v2)
		{
			checked
			{
				Vector vector = new Vector(v1.Count + v2.Count);
				int num = v1.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					vector[i] = v1[i];
				}
				int num2 = v2.Count - 1;
				for (int j = 0; j <= num2; j++)
				{
					vector[j + v1.Count] = v2[j];
				}
				return vector;
			}
		}
	}
}
