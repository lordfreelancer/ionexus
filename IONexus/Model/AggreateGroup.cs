﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IONexus.Model
{
    [Serializable]
    public class BaseGroup : IComparable<BaseGroup>
    {
        public BaseGroup() { }
        public BaseGroup(string name, int position)
        {
            Name = name;
            Position = position;
        }
        public string Name { get; set; }
        public int Position { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as BaseGroup;

            if (item == null)
            {
                return false;
            }

            return this.Name.Equals(item.Name);
        }
        public int CompareTo(BaseGroup obj)
        {
            return Name.CompareTo(obj.Name);
        }
    }

    [Serializable]
    public class Sector : BaseGroup
    {
        public bool IsGrouped { get; set; }

        public bool IsSepareted { get; set; }
        public List<SeparateGroup> Children { get; set; }
    }

    [Serializable]
    public class AggreateGroup : BaseGroup
    {
        public AggreateGroup()
        {
            Children = new List<Sector>();
        }
        public List<Sector> Children { get; set; }
    }

    [Serializable]
    public class SeparateGroup : BaseGroup
    {
        public double Percentage { get; set; }
        public Sector Parent { get; set; }
    }
}
