﻿using Matrix_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IONexus.Model
{
    [Serializable]
    public class NexusSummary
    {
        public double ExChangeRate { set; get; }
        public List<AggreateGroup> AggregateList { get; set; }
        public List<SeparateGroup> Separate { get; set; }
        public List<BaseGroup> Columns { get; set; }
        public Matrix FinalData { get; set; }
        public Matrix SumFinalDemand { get; set; }
        public Matrix SumPrimary { get; set; }
        public Matrix SumImport { get; set; }

        private List<Sector> _parentSector = null;
        public List<Sector> ParentSector
        {
            get
            {
                return _parentSector;
            }
        }

        private List<Sector> _baseSector = null;
        public List<Sector> BaseSector
        {
            get
            {
                if (_baseSector != null) return _baseSector;
                if (Columns == null) return null;
                _baseSector = new List<Sector>();
                for (int i = 0; i < Columns.Count; i++)
                {
                    Sector sector = new Sector();
                    sector.Name = Columns[i].Name;
                    sector.Position = Columns[i].Position;
                    sector.IsGrouped = false;
                    sector.IsSepareted = false;
                    _baseSector.Add(sector);
                }
                return _baseSector;
            }
        }

        public void SetParentSector(NexusInput input)
        {
            _parentSector = new List<Sector>();
            for (int i = 0; i < input.basePriceColumns.Length; i++)
            {
                Sector sector = new Sector();
                sector.Name = input.basePriceColumns[i];
                sector.Position = i;
                sector.IsGrouped = false;
                sector.IsSepareted = false;
                _parentSector.Add(sector);
            }
        }
        public void SetParentSector(List<Sector> parents)
        {
            _parentSector = parents;
        }

        public NexusSummary()
        {
            Columns = new List<BaseGroup>();
            AggregateList = new List<AggreateGroup>();
            Separate = new List<SeparateGroup>();
        }

        public void DoSummarize(NexusInput input, NexusSummary previousStep)
        {
            if (AggregateList.Count > 0)
            {
                DoAggregate(input, previousStep);
            }
        }
        public void DoAggregate(NexusInput input, NexusSummary previousStep)
        {
            if (previousStep == null)
            {
                previousStep = new NexusSummary();
                previousStep.SetParentSector(input);
                foreach (Sector s in previousStep.ParentSector)
                {
                    previousStep.Columns.Add(new BaseGroup(s.Name, s.Position));
                }
                previousStep.FinalData = input.BasePriceData;
                previousStep.SumFinalDemand = input.FinalDemandData;
                previousStep.SumImport = input.ImportData;
                previousStep.SumPrimary = input.PrimaryFactorData;
            }
            int iColumn = previousStep.Columns.Count;
            //BaseInput.BasePriceData
            //Group Column First
            List<BaseGroup> newList = new List<BaseGroup>();
            int[] MapRowAggregate = new int[iColumn];
            int totalMerge = 0;
            for (int i = 0; i < AggregateList.Count; i++)
            {
                AggreateGroup g = AggregateList[i];
                //Vector r = new Vector(input.BasePriceData.NoRows);
                Vector c = new Vector(iColumn);
                foreach (Sector s in g.Children)
                {
                    if (s.Position >= MapRowAggregate.Length) continue;
                    MapRowAggregate[s.Position] = i;
                    totalMerge += 1;
                    Vector v2 = previousStep.FinalData.getColVector(s.Position);
                }
            }
            Matrix mid = new Matrix(iColumn, 1);
            for (int i = 0; i < ParentSector.Count - totalMerge; i++)
            {
                //mid.Append();
            }
        }
    }
}
