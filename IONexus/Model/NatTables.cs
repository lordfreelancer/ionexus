using Matrix_Lib;
using System;

namespace IONexus.Model
{
	[Serializable]
	public class NatTables : ICloneable
	{
		private Matrix Make_Table;

		private Matrix Make_Table_Total;

		private Matrix Make_Table_FedState;

		private Matrix Use_Table_Total;

		private Matrix Use_Table_FedState;

		private Matrix Use_Table;

		private Matrix VA_Table;

		private Matrix SCRAP_Table;

		private Matrix NCI_Table;

		private Matrix Imports_Table;

		private Matrix FinDem_Table;

		private Vector PCE_Table;

		public Vector Employment_FTE;

		public Vector Employment_JOBS;

		public FTE_RATIO_CLASS FTE_Ratio;

		public Vector RegEmployment_FTE;

		public Vector RegEmployment_JOBS;

		private int IndustryCount_FedState;

		private int CommodityCount_FedState;

		private int IndustryCount_Total;

		private int CommodityCount_Total;

		private int IndustryCount_Imputed;

		private int CommodityCount_Imputed;

		private string[] hdrInd_imputed;

		private string[] hdrInd_total;

		private string[] hdrInd_FedState;

		public string[] hdrIndBase;

		public string[] hdrIndAdjGov;

		public string[] hdrIndFedStateLocGov;

		public string[] hdrIndTotGov;

		public string[] hdrIndExpandedGov;

		private string[] hdrComm_Imputed;

		private string[] hdrComm_Total;

		private string[] hdrComm_FedState;

		public string[] hdrCommBase;

		public string[] hdrCommAdjGov;

		public string[] hdrCommFedStateLocGov;

		public string[] hdrCommTotGov;

		public string[] hdrCommExpandedGov;

		public GenDataStatus GovAggLevel;

		public Matrix Scrap
		{
			get
			{
				Matrix matrix = new Matrix(1, 1);
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.ImputedGov:
					matrix = this.SCRAP_Table;
					break;
				case GenDataStatus.dataStatusType.TotalGovernment:
				{
					matrix = new Matrix(1, this.getIndustryCount());
					int num4;
					checked
					{
						int num3 = this.getIndustryCount() - 1;
						for (int j = 0; j <= num3; j++)
						{
							matrix[0, j] = this.SCRAP_Table[0, j];
						}
						num4 = this.getIndustryCount() - 1;
					}
					matrix[0, num4] = this.SCRAP_Table[0, num4] + this.SCRAP_Table[0, checked(num4 + 1)] + this.SCRAP_Table[0, checked(num4 + 2)] + this.SCRAP_Table[0, checked(num4 + 3)];
					break;
				}
				case GenDataStatus.dataStatusType.FedStateLocal:
				{
					matrix = new Matrix(1, this.getIndustryCount());
					int num2;
					checked
					{
						int num = this.getIndustryCount() - 1;
						for (int i = 0; i <= num; i++)
						{
							matrix[0, i] = this.SCRAP_Table[0, i];
						}
						num2 = this.getIndustryCount() - 1;
						matrix[0, num2 - 1] = unchecked(this.SCRAP_Table[0, num2] + this.SCRAP_Table[0, checked(num2 - 1)]);
					}
					matrix[0, num2] = this.SCRAP_Table[0, checked(num2 + 2)] + this.SCRAP_Table[0, checked(num2 + 1)];
					break;
				}
				default:
					matrix = this.SCRAP_Table;
					break;
				}
				return matrix;
			}
			set
			{
				this.SCRAP_Table = value;
			}
		}

		public Matrix NCI
		{
			get
			{
				Matrix matrix = new Matrix(1, 1);
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.ImputedGov:
					matrix = this.NCI_Table;
					break;
				case GenDataStatus.dataStatusType.TotalGovernment:
				{
					matrix = new Matrix(1, this.getIndustryCount());
					int num4;
					checked
					{
						int num3 = this.getIndustryCount() - 1;
						for (int j = 0; j <= num3; j++)
						{
							matrix[0, j] = this.NCI_Table[0, j];
						}
						num4 = this.getIndustryCount() - 1;
					}
					matrix[0, num4] = this.NCI_Table[0, num4] + this.NCI_Table[0, checked(num4 + 1)] + this.NCI_Table[0, checked(num4 + 2)] + this.NCI_Table[0, checked(num4 + 3)];
					break;
				}
				case GenDataStatus.dataStatusType.FedStateLocal:
				{
					matrix = new Matrix(1, this.getIndustryCount());
					int num2;
					checked
					{
						int num = this.getIndustryCount() - 1;
						for (int i = 0; i <= num; i++)
						{
							matrix[0, i] = this.NCI_Table[0, i];
						}
						num2 = this.getIndustryCount() - 1;
						matrix[0, num2 - 1] = unchecked(this.NCI_Table[0, num2] + this.NCI_Table[0, checked(num2 - 1)]);
					}
					matrix[0, num2] = this.NCI_Table[0, checked(num2 + 2)] + this.NCI_Table[0, checked(num2 + 1)];
					break;
				}
				default:
					matrix = this.NCI_Table;
					break;
				}
				return matrix;
			}
			set
			{
				this.NCI_Table = value;
			}
		}

		public Vector PCE
		{
			get
			{
				Vector vector = new Vector(1);
				checked
				{
					switch (this.GovAggLevel.getAggLevel())
					{
					case GenDataStatus.dataStatusType.ImputedGov:
						vector = this.PCE_Table;
						break;
					case GenDataStatus.dataStatusType.TotalGovernment:
					{
						vector = new Vector(this.getCommodityCount());
						int num3 = this.getCommodityCount() - 1;
						for (int j = 0; j <= num3; j++)
						{
							vector[j] = this.PCE_Table[j];
						}
						int num4 = this.getCommodityCount() - 3;
						vector[num4] = unchecked(this.PCE_Table[num4] + this.PCE_Table[checked(num4 + 1)] + this.PCE_Table[checked(num4 + 2)] + this.PCE_Table[checked(num4 + 3)]);
						vector[num4 + 1] = this.PCE_Table[num4 + 4];
						vector[num4 + 2] = this.PCE_Table[num4 + 5];
						break;
					}
					case GenDataStatus.dataStatusType.FedStateLocal:
					{
						vector = new Vector(this.getCommodityCount());
						int num = this.getCommodityCount() - 1;
						for (int i = 0; i <= num; i++)
						{
							vector[i] = this.PCE_Table[i];
						}
						int num2 = this.getCommodityCount() - 1;
						vector[num2 - 1] = unchecked(this.PCE_Table[num2] + this.PCE_Table[checked(num2 - 1)]);
						vector[num2] = unchecked(this.PCE_Table[checked(num2 + 2)] + this.PCE_Table[checked(num2 + 1)]);
						break;
					}
					default:
						vector = this.PCE_Table;
						break;
					}
					return vector;
				}
			}
			set
			{
				this.PCE_Table = value;
			}
		}

		public Matrix Import
		{
			get
			{
				Matrix matrix = new Matrix(1, 1);
				checked
				{
					switch (this.GovAggLevel.getAggLevel())
					{
					case GenDataStatus.dataStatusType.ImputedGov:
						matrix = this.Imports_Table;
						break;
					case GenDataStatus.dataStatusType.TotalGovernment:
					{
						matrix = new Matrix(1, this.getCommodityCount());
						int num3 = this.getCommodityCount() - 1;
						for (int j = 0; j <= num3; j++)
						{
							matrix[0, j] = this.Imports_Table[0, j];
						}
						int num4 = this.getCommodityCount() - 3;
						matrix[0, num4] = unchecked(this.Imports_Table[0, num4] + this.Imports_Table[0, checked(num4 + 1)] + this.Imports_Table[0, checked(num4 + 2)] + this.Imports_Table[0, checked(num4 + 3)]);
						matrix[0, num4 + 1] = this.Imports_Table[0, num4 + 4];
						matrix[0, num4 + 2] = this.Imports_Table[0, num4 + 5];
						break;
					}
					case GenDataStatus.dataStatusType.FedStateLocal:
					{
						matrix = new Matrix(1, this.getCommodityCount());
						int num = this.getCommodityCount() - 1;
						for (int i = 0; i <= num; i++)
						{
							matrix[0, i] = this.Imports_Table[0, i];
						}
						int num2 = this.getCommodityCount() - 1;
						matrix[0, num2 - 1] = unchecked(this.Imports_Table[0, num2] + this.Imports_Table[0, checked(num2 - 1)]);
						matrix[0, num2] = unchecked(this.Imports_Table[0, checked(num2 + 2)] + this.Imports_Table[0, checked(num2 + 1)]);
						break;
					}
					default:
						matrix = this.Imports_Table;
						break;
					}
					return matrix;
				}
			}
			set
			{
				this.Imports_Table = value;
			}
		}

		public Matrix FinDem
		{
			get
			{
				Matrix matrix = new Matrix(1, 1);
				checked
				{
					switch (this.GovAggLevel.getAggLevel())
					{
					case GenDataStatus.dataStatusType.ImputedGov:
						matrix = this.FinDem_Table;
						break;
					case GenDataStatus.dataStatusType.TotalGovernment:
					{
						matrix = new Matrix(this.getCommodityCount(), this.FinDem_Table.NoCols);
						int num5 = this.getCommodityCount() - 3;
						int num6 = num5 - 1;
						for (int l = 0; l <= num6; l++)
						{
							int num7 = this.FinDem_Table.NoCols - 1;
							for (int m = 0; m <= num7; m++)
							{
								matrix[l, m] = this.FinDem_Table[l, m];
							}
						}
						int num8 = this.FinDem_Table.NoCols - 1;
						for (int n = 0; n <= num8; n++)
						{
							matrix[num5, n] = unchecked(this.FinDem_Table[num5, n] + this.FinDem_Table[checked(num5 + 1), n] + this.FinDem_Table[checked(num5 + 2), n] + this.FinDem_Table[checked(num5 + 3), n]);
							matrix[num5 + 1, n] = this.FinDem_Table[num5 + 4, n];
							matrix[num5 + 2, n] = this.FinDem_Table[num5 + 5, n];
						}
						break;
					}
					case GenDataStatus.dataStatusType.FedStateLocal:
					{
						matrix = new Matrix(this.getCommodityCount(), this.FinDem_Table.NoCols);
						int num = this.getCommodityCount() - 4;
						int num2 = num - 1;
						for (int i = 0; i <= num2; i++)
						{
							int num3 = this.FinDem_Table.NoCols - 1;
							for (int j = 0; j <= num3; j++)
							{
								matrix[i, j] = this.FinDem_Table[i, j];
							}
						}
						int num4 = this.FinDem_Table.NoCols - 1;
						for (int k = 0; k <= num4; k++)
						{
							matrix[num, k] = unchecked(this.FinDem_Table[num, k] + this.FinDem_Table[checked(num + 1), k]);
							matrix[num + 1, k] = unchecked(this.FinDem_Table[checked(num + 2), k] + this.FinDem_Table[checked(num + 3), k]);
							matrix[num + 2, k] = this.FinDem_Table[num + 4, k];
							matrix[num + 3, k] = this.FinDem_Table[num + 5, k];
						}
						break;
					}
					default:
						matrix = this.FinDem_Table;
						break;
					}
					return matrix;
				}
			}
			set
			{
				this.FinDem_Table = value;
			}
		}

		public Matrix Use
		{
			get
			{
				Matrix matrix = new Matrix(1, 1);
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.ImputedGov:
					return this.Use_Table;
				case GenDataStatus.dataStatusType.TotalGovernment:
					return this.Use_Table_Total;
				case GenDataStatus.dataStatusType.FedStateLocal:
					return this.Use_Table_FedState;
				default:
					return this.Use_Table;
				}
			}
			set
			{
				this.SetUse(ref value);
			}
		}

		public Vector Employment
		{
			get
			{
				Vector vector = new Vector(1);
				checked
				{
					switch (this.GovAggLevel.getFTE_Type())
					{
					case GenDataStatus.dataEmpStatus.FTE_BASED:
						switch (this.GovAggLevel.getAggLevel())
						{
						case GenDataStatus.dataStatusType.FedStateLocal:
						{
							vector = new Vector(this.Employment_FTE.Count - 2);
							int num2 = this.Employment_FTE.Count - 5;
							for (int j = 0; j <= num2; j++)
							{
								vector[j] = this.Employment_FTE[j];
							}
							vector[this.Employment_FTE.Count - 4] = unchecked(this.Employment_FTE[checked(this.Employment_FTE.Count - 4)] + this.Employment_FTE[checked(this.Employment_FTE.Count - 3)]);
							vector[this.Employment_FTE.Count - 3] = unchecked(this.Employment_FTE[checked(this.Employment_FTE.Count - 2)] + this.Employment_FTE[checked(this.Employment_FTE.Count - 1)]);
							break;
						}
						case GenDataStatus.dataStatusType.TotalGovernment:
						{
							vector = new Vector(this.Employment_FTE.Count - 3);
							int num = this.Employment_FTE.Count - 5;
							for (int i = 0; i <= num; i++)
							{
								vector[i] = this.Employment_FTE[i];
							}
							vector[this.Employment_FTE.Count - 4] = unchecked(this.Employment_FTE[checked(this.Employment_FTE.Count - 4)] + this.Employment_FTE[checked(this.Employment_FTE.Count - 3)] + this.Employment_FTE[checked(this.Employment_FTE.Count - 2)] + this.Employment_FTE[checked(this.Employment_FTE.Count - 1)]);
							break;
						}
						case GenDataStatus.dataStatusType.ImputedGov:
							vector = this.Employment_FTE;
							break;
						default:
							vector = this.Employment_FTE;
							break;
						}
						return vector;
					case GenDataStatus.dataEmpStatus.JOB_BASED:
						switch (this.GovAggLevel.getAggLevel())
						{
						case GenDataStatus.dataStatusType.FedStateLocal:
						{
							vector = new Vector(this.Employment_JOBS.Count - 2);
							int num4 = this.Employment_JOBS.Count - 5;
							for (int l = 0; l <= num4; l++)
							{
								vector[l] = this.Employment_JOBS[l];
							}
							vector[this.Employment_JOBS.Count - 4] = unchecked(this.Employment_JOBS[checked(this.Employment_JOBS.Count - 4)] + this.Employment_JOBS[checked(this.Employment_JOBS.Count - 3)]);
							vector[this.Employment_JOBS.Count - 3] = unchecked(this.Employment_JOBS[checked(this.Employment_JOBS.Count - 2)] + this.Employment_JOBS[checked(this.Employment_JOBS.Count - 1)]);
							break;
						}
						case GenDataStatus.dataStatusType.TotalGovernment:
						{
							vector = new Vector(this.Employment_JOBS.Count - 3);
							int num3 = this.Employment_JOBS.Count - 5;
							for (int k = 0; k <= num3; k++)
							{
								vector[k] = this.Employment_JOBS[k];
							}
							vector[this.Employment_JOBS.Count - 4] = unchecked(this.Employment_JOBS[checked(this.Employment_JOBS.Count - 4)] + this.Employment_JOBS[checked(this.Employment_JOBS.Count - 3)] + this.Employment_JOBS[checked(this.Employment_JOBS.Count - 2)] + this.Employment_JOBS[checked(this.Employment_JOBS.Count - 1)]);
							break;
						}
						case GenDataStatus.dataStatusType.ImputedGov:
							vector = this.Employment_JOBS;
							break;
						default:
							vector = this.Employment_JOBS;
							break;
						}
						return vector;
					default:
						return this.Employment_FTE;
					}
				}
			}
			set
			{
				switch (this.GovAggLevel.getFTE_Type())
				{
				case GenDataStatus.dataEmpStatus.FTE_BASED:
					this.Employment_FTE = value;
					switch (this.GovAggLevel.getAggLevel())
					{
					case GenDataStatus.dataStatusType.FedStateLocal:
						this.Employment_JOBS = this.Employment_FTE * this.FTE_Ratio.fte2jobs_ratio_fedstate;
						break;
					case GenDataStatus.dataStatusType.TotalGovernment:
						this.Employment_JOBS = this.Employment_FTE * this.FTE_Ratio.fte2jobs_ratio_totalgov;
						break;
					case GenDataStatus.dataStatusType.ImputedGov:
						this.Employment_JOBS = this.Employment_FTE * this.FTE_Ratio.fte2jobs_ratio_imputed;
						break;
					}
					break;
				case GenDataStatus.dataEmpStatus.JOB_BASED:
					this.Employment_JOBS = value;
					switch (this.GovAggLevel.getAggLevel())
					{
					case GenDataStatus.dataStatusType.FedStateLocal:
						this.Employment_FTE = this.Employment_JOBS * this.FTE_Ratio.jobs2fte_ratio_fedstate;
						break;
					case GenDataStatus.dataStatusType.TotalGovernment:
						this.Employment_FTE = this.Employment_JOBS * this.FTE_Ratio.jobs2fte_ratio_totalgov;
						break;
					case GenDataStatus.dataStatusType.ImputedGov:
						this.Employment_FTE = this.Employment_JOBS * this.FTE_Ratio.jobs2fte_ratio_imputed;
						break;
					}
					break;
				}
			}
		}

		public Vector RegEmployment
		{
			get
			{
				Vector vector = new Vector(1);
				checked
				{
					switch (this.GovAggLevel.getFTE_Type())
					{
					case GenDataStatus.dataEmpStatus.FTE_BASED:
						switch (this.GovAggLevel.getAggLevel())
						{
						case GenDataStatus.dataStatusType.FedStateLocal:
						{
							vector = new Vector(this.RegEmployment_FTE.Count - 2);
							int num2 = this.RegEmployment_FTE.Count - 5;
							for (int j = 0; j <= num2; j++)
							{
								vector[j] = this.RegEmployment_FTE[j];
							}
							vector[this.RegEmployment_FTE.Count - 4] = unchecked(this.RegEmployment_FTE[checked(this.RegEmployment_FTE.Count - 4)] + this.RegEmployment_FTE[checked(this.RegEmployment_FTE.Count - 3)]);
							vector[this.RegEmployment_FTE.Count - 3] = unchecked(this.RegEmployment_FTE[checked(this.RegEmployment_FTE.Count - 2)] + this.RegEmployment_FTE[checked(this.RegEmployment_FTE.Count - 1)]);
							break;
						}
						case GenDataStatus.dataStatusType.TotalGovernment:
						{
							vector = new Vector(this.RegEmployment_FTE.Count - 3);
							int num = this.RegEmployment_FTE.Count - 5;
							for (int i = 0; i <= num; i++)
							{
								vector[i] = this.RegEmployment_FTE[i];
							}
							vector[this.RegEmployment_FTE.Count - 4] = unchecked(this.RegEmployment_FTE[checked(this.RegEmployment_FTE.Count - 4)] + this.RegEmployment_FTE[checked(this.RegEmployment_FTE.Count - 3)] + this.RegEmployment_FTE[checked(this.RegEmployment_FTE.Count - 2)] + this.RegEmployment_FTE[checked(this.Employment_FTE.Count - 1)]);
							break;
						}
						case GenDataStatus.dataStatusType.ImputedGov:
							vector = this.RegEmployment_FTE;
							break;
						default:
							vector = this.RegEmployment_FTE;
							break;
						}
						return vector;
					case GenDataStatus.dataEmpStatus.JOB_BASED:
						switch (this.GovAggLevel.getAggLevel())
						{
						case GenDataStatus.dataStatusType.FedStateLocal:
						{
							vector = new Vector(this.RegEmployment_JOBS.Count - 2);
							int num4 = this.RegEmployment_JOBS.Count - 5;
							for (int l = 0; l <= num4; l++)
							{
								vector[l] = this.RegEmployment_JOBS[l];
							}
							vector[this.RegEmployment_JOBS.Count - 4] = unchecked(this.RegEmployment_JOBS[checked(this.RegEmployment_JOBS.Count - 4)] + this.RegEmployment_JOBS[checked(this.RegEmployment_JOBS.Count - 3)]);
							vector[this.RegEmployment_JOBS.Count - 3] = unchecked(this.RegEmployment_JOBS[checked(this.RegEmployment_JOBS.Count - 2)] + this.RegEmployment_JOBS[checked(this.RegEmployment_JOBS.Count - 1)]);
							break;
						}
						case GenDataStatus.dataStatusType.TotalGovernment:
						{
							vector = new Vector(this.RegEmployment_JOBS.Count - 3);
							int num3 = this.RegEmployment_JOBS.Count - 5;
							for (int k = 0; k <= num3; k++)
							{
								vector[k] = this.RegEmployment_JOBS[k];
							}
							vector[this.RegEmployment_JOBS.Count - 4] = unchecked(this.RegEmployment_JOBS[checked(this.RegEmployment_JOBS.Count - 4)] + this.RegEmployment_JOBS[checked(this.RegEmployment_JOBS.Count - 3)] + this.RegEmployment_JOBS[checked(this.RegEmployment_JOBS.Count - 2)] + this.RegEmployment_JOBS[checked(this.RegEmployment_JOBS.Count - 1)]);
							break;
						}
						case GenDataStatus.dataStatusType.ImputedGov:
							vector = this.RegEmployment_JOBS;
							break;
						default:
							vector = this.RegEmployment_JOBS;
							break;
						}
						return vector;
					default:
						return this.Employment_FTE;
					}
				}
			}
			set
			{
				switch (this.GovAggLevel.getFTE_Type())
				{
				case GenDataStatus.dataEmpStatus.FTE_BASED:
					this.RegEmployment_FTE = value;
					switch (this.GovAggLevel.getAggLevel())
					{
					case GenDataStatus.dataStatusType.FedStateLocal:
						this.RegEmployment_JOBS = this.RegEmployment_FTE * this.FTE_Ratio.fte2jobs_ratio_fedstate;
						break;
					case GenDataStatus.dataStatusType.TotalGovernment:
						this.RegEmployment_JOBS = this.RegEmployment_FTE * this.FTE_Ratio.fte2jobs_ratio_totalgov;
						break;
					case GenDataStatus.dataStatusType.ImputedGov:
						this.RegEmployment_JOBS = this.RegEmployment_FTE * this.FTE_Ratio.fte2jobs_ratio_imputed;
						break;
					}
					break;
				case GenDataStatus.dataEmpStatus.JOB_BASED:
					this.RegEmployment_JOBS = value;
					switch (this.GovAggLevel.getAggLevel())
					{
					case GenDataStatus.dataStatusType.FedStateLocal:
						this.RegEmployment_FTE = this.RegEmployment_JOBS * this.FTE_Ratio.jobs2fte_ratio_fedstate;
						break;
					case GenDataStatus.dataStatusType.TotalGovernment:
						this.RegEmployment_FTE = this.RegEmployment_JOBS * this.FTE_Ratio.jobs2fte_ratio_totalgov;
						break;
					case GenDataStatus.dataStatusType.ImputedGov:
						this.RegEmployment_FTE = this.RegEmployment_JOBS * this.FTE_Ratio.jobs2fte_ratio_imputed;
						break;
					}
					break;
				}
			}
		}

		public Matrix VA
		{
			get
			{
				Matrix matrix = new Matrix(0, 0);
				checked
				{
					switch (this.GovAggLevel.getAggLevel())
					{
					case GenDataStatus.dataStatusType.ImputedGov:
						matrix = this.VA_Table;
						break;
					case GenDataStatus.dataStatusType.TotalGovernment:
					{
						matrix = new Matrix(this.VA_Table.NoRows, this.VA_Table.NoCols - 3);
						int num3 = this.VA_Table.NoRows - 1;
						for (int k = 0; k <= num3; k++)
						{
							int num4 = this.VA_Table.NoCols - 5;
							for (int l = 0; l <= num4; l++)
							{
								matrix[k, l] = this.VA_Table[k, l];
							}
							matrix[k, this.VA_Table.NoCols - 4] = unchecked(this.VA_Table[k, checked(this.VA_Table.NoCols - 4)] + this.VA_Table[k, checked(this.VA_Table.NoCols - 3)] + this.VA_Table[k, checked(this.VA_Table.NoCols - 2)] + this.VA_Table[k, checked(this.VA_Table.NoCols - 1)]);
						}
						break;
					}
					case GenDataStatus.dataStatusType.FedStateLocal:
					{
						matrix = new Matrix(this.VA_Table.NoRows, this.VA_Table.NoCols - 2);
						int num = this.VA_Table.NoRows - 1;
						for (int i = 0; i <= num; i++)
						{
							int num2 = this.VA_Table.NoCols - 5;
							for (int j = 0; j <= num2; j++)
							{
								matrix[i, j] = this.VA_Table[i, j];
							}
							matrix[i, this.VA_Table.NoCols - 4] = unchecked(this.VA_Table[i, checked(this.VA_Table.NoCols - 4)] + this.VA_Table[i, checked(this.VA_Table.NoCols - 3)]);
							matrix[i, this.VA_Table.NoCols - 3] = unchecked(this.VA_Table[i, checked(this.VA_Table.NoCols - 2)] + this.VA_Table[i, checked(this.VA_Table.NoCols - 1)]);
						}
						break;
					}
					default:
						matrix = this.VA_Table;
						break;
					}
					return matrix;
				}
			}
			set
			{
				this.VA_Table = value;
			}
		}

		public string[] hdrInd
		{
			get
			{
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.ImputedGov:
					return this.hdrInd_imputed;
				case GenDataStatus.dataStatusType.TotalGovernment:
					return this.hdrInd_total;
				case GenDataStatus.dataStatusType.FedStateLocal:
					return this.hdrInd_FedState;
				default:
					return this.hdrInd_imputed;
				}
			}
			set
			{
				this.setIndHeaders(ref value);
			}
		}

		public string[] hdrComm
		{
			get
			{
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.ImputedGov:
					return this.hdrComm_Imputed;
				case GenDataStatus.dataStatusType.TotalGovernment:
					return this.hdrComm_Total;
				case GenDataStatus.dataStatusType.FedStateLocal:
					return this.hdrComm_FedState;
				default:
					return this.hdrComm_Imputed;
				}
			}
			set
			{
				this.setCommHeaders(ref value);
			}
		}

		public NatTables()
		{
			this.Employment_FTE = new Vector(1);
			this.Employment_JOBS = new Vector(1);
			this.FTE_Ratio = new FTE_RATIO_CLASS();
			this.RegEmployment_FTE = new Vector(1);
			this.RegEmployment_JOBS = new Vector(1);
			this.GovAggLevel = new GenDataStatus();
		}

		public object Clone()
		{
			NatTables natTables = new NatTables();
			natTables.Make_Table = this.Make_Table.Clone();
			natTables.Make_Table_Total = this.Make_Table_Total.Clone();
			natTables.Make_Table_FedState = this.Make_Table_FedState.Clone();
			natTables.Use_Table = this.Use_Table.Clone();
			natTables.Use_Table_Total = this.Use_Table_Total.Clone();
			natTables.Use_Table_FedState = this.Use_Table_FedState.Clone();
			natTables.VA_Table = this.VA_Table.Clone();
			natTables.FTE_Ratio = (FTE_RATIO_CLASS)this.FTE_Ratio.Clone();
			natTables.SCRAP_Table = this.SCRAP_Table.Clone();
			natTables.NCI_Table = this.NCI_Table.Clone();
			natTables.FinDem_Table = this.FinDem_Table.Clone();
			natTables.Imports_Table = this.Imports_Table.Clone();
			natTables.Employment_FTE = this.Employment_FTE.Clone();
			natTables.Employment_JOBS = this.Employment_JOBS.Clone();
			natTables.RegEmployment_FTE = this.RegEmployment_FTE.Clone();
			natTables.RegEmployment_JOBS = this.RegEmployment_JOBS.Clone();
			natTables.PCE_Table = this.PCE_Table.Clone();
			natTables.hdrInd_FedState = (string[])this.hdrInd_FedState.Clone();
			natTables.hdrInd_imputed = (string[])this.hdrInd_imputed.Clone();
			natTables.hdrInd_total = (string[])this.hdrInd_total.Clone();
			natTables.hdrIndBase = (string[])this.hdrIndBase.Clone();
			natTables.hdrIndAdjGov = (string[])this.hdrIndAdjGov.Clone();
			natTables.hdrIndFedStateLocGov = (string[])this.hdrIndFedStateLocGov.Clone();
			natTables.hdrIndTotGov = (string[])this.hdrIndTotGov.Clone();
			natTables.hdrComm_FedState = (string[])this.hdrComm_FedState.Clone();
			natTables.hdrComm_Imputed = (string[])this.hdrComm_Imputed.Clone();
			natTables.hdrComm_Total = (string[])this.hdrComm_Total.Clone();
			natTables.IndustryCount_Total = this.IndustryCount_Total;
			natTables.IndustryCount_Imputed = this.IndustryCount_Imputed;
			natTables.IndustryCount_FedState = this.IndustryCount_FedState;
			natTables.CommodityCount_Total = this.CommodityCount_Total;
			natTables.CommodityCount_Imputed = this.CommodityCount_Imputed;
			natTables.CommodityCount_FedState = this.CommodityCount_FedState;
			return natTables;
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Clone
			return this.Clone();
		}

		public int getIndustryCount()
		{
			int num = 0;
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.IndustryCount_Imputed;
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.IndustryCount_Total;
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.IndustryCount_FedState;
			default:
				return this.IndustryCount_Imputed;
			}
		}

		public int getCommodityCount()
		{
			int num = 0;
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.CommodityCount_Imputed;
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.CommodityCount_Total;
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.CommodityCount_FedState;
			default:
				return this.CommodityCount_Imputed;
			}
		}

		public Matrix Make()
		{
			Matrix matrix = new Matrix(1, 1);
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Make_Table;
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Make_Table_Total;
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Make_Table_FedState;
			default:
				return this.Make_Table;
			}
		}

		public void SetMake(ref Matrix _make)
		{
			this.Make_Table = _make;
			this.IndustryCount_Imputed = this.Make_Table.NoRows;
			this.CommodityCount_Imputed = this.Make_Table.NoCols;
			Vector vector;
			Vector vector2;
			Vector vector3;
			int num4;
			int num5;
			checked
			{
				this.IndustryCount_FedState = this.IndustryCount_Imputed - 2;
				this.IndustryCount_Total = this.IndustryCount_Imputed - 3;
				this.CommodityCount_FedState = this.CommodityCount_Imputed - 2;
				this.CommodityCount_Total = this.CommodityCount_Imputed - 3;
				this.Make_Table_Total = new Matrix(this.IndustryCount_Total, this.CommodityCount_Total);
				this.Make_Table_FedState = new Matrix(this.IndustryCount_FedState, this.CommodityCount_FedState);
				vector = new Vector(this.CommodityCount_Imputed);
				vector2 = new Vector(this.CommodityCount_Imputed);
				vector3 = new Vector(this.CommodityCount_Imputed);
				int num = this.IndustryCount_Imputed - 4;
				for (int i = 0; i <= num; i++)
				{
					int num2 = this.CommodityCount_Imputed - 6;
					int num3 = this.CommodityCount_Imputed - 7;
					for (int j = 0; j <= num3; j++)
					{
						this.Make_Table_Total[i, j] = this.Make_Table[i, j];
						this.Make_Table_FedState[i, j] = this.Make_Table[i, j];
					}
					this.Make_Table_Total[i, num2] = unchecked(this.Make_Table[i, num2] + this.Make_Table[i, checked(num2 + 1)] + this.Make_Table[i, checked(num2 + 2)] + this.Make_Table[i, checked(num2 + 3)]);
					this.Make_Table_Total[i, num2 + 1] = this.Make_Table[i, num2 + 4];
					this.Make_Table_Total[i, num2 + 2] = this.Make_Table[i, num2 + 5];
					this.Make_Table_FedState[i, num2] = unchecked(this.Make_Table[i, num2] + this.Make_Table[i, checked(num2 + 1)]);
					this.Make_Table_FedState[i, num2 + 1] = unchecked(this.Make_Table[i, checked(num2 + 2)] + this.Make_Table[i, checked(num2 + 3)]);
					this.Make_Table_FedState[i, num2 + 2] = this.Make_Table[i, num2 + 4];
					this.Make_Table_FedState[i, num2 + 3] = this.Make_Table[i, num2 + 5];
				}
				num4 = this.IndustryCount_Total - 1;
				num5 = this.CommodityCount_Imputed - 6;
				int num6 = this.CommodityCount_Imputed - 1;
				for (int k = 0; k <= num6; k++)
				{
					unchecked
					{
						vector[k] = this.Make_Table[num4, k] + this.Make_Table[checked(num4 + 1), k] + this.Make_Table[checked(num4 + 2), k] + this.Make_Table[checked(num4 + 3), k];
						vector2[k] = this.Make_Table[num4, k] + this.Make_Table[checked(num4 + 1), k];
						vector3[k] = this.Make_Table[checked(num4 + 2), k] + this.Make_Table[checked(num4 + 3), k];
					}
				}
				this.Make_Table_Total[num4, num5 + 1] = vector[num5 + 4];
				this.Make_Table_Total[num4, num5 + 2] = vector[num5 + 5];
				this.Make_Table_FedState[num4, num5 + 2] = vector2[num5 + 4];
				this.Make_Table_FedState[num4 + 1, num5 + 2] = vector3[num5 + 4];
				this.Make_Table_FedState[num4, num5 + 3] = vector2[num5 + 5];
				this.Make_Table_FedState[num4 + 1, num5 + 3] = vector3[num5 + 5];
			}
			this.Make_Table_Total[num4, num5] = vector[num5] + vector[checked(num5 + 1)] + vector[checked(num5 + 2)] + vector[checked(num5 + 3)];
			this.Make_Table_FedState[num4, num5] = vector2[num4] + vector2[checked(num4 + 1)];
			this.Make_Table_FedState[num4, checked(num5 + 1)] = vector2[checked(num4 + 2)] + vector2[checked(num4 + 3)];
			this.Make_Table_FedState[checked(num4 + 1), num5] = vector3[num4] + vector3[checked(num4 + 1)];
			checked(this.Make_Table_FedState[num4 + 1, num5 + 1]) = vector3[checked(num4 + 2)] + vector3[checked(num4 + 3)];
			checked
			{
				int num7 = this.CommodityCount_Imputed - 7;
				for (int l = 0; l <= num7; l++)
				{
					this.Make_Table_Total[num4, l] = vector[l];
					this.Make_Table_FedState[num4, l] = vector2[l];
					this.Make_Table_FedState[num4 + 1, l] = vector3[l];
				}
			}
		}

		public void SetUse(ref Matrix _use)
		{
			this.Use_Table_Total = new Matrix(this.CommodityCount_Total, this.IndustryCount_Total);
			this.Use_Table_FedState = new Matrix(this.CommodityCount_FedState, this.IndustryCount_FedState);
			this.Use_Table = _use;
			Vector vector = new Vector(this.IndustryCount_Imputed);
			Vector vector2 = new Vector(this.IndustryCount_Imputed);
			Vector vector3 = new Vector(this.IndustryCount_Imputed);
			Vector vector4 = new Vector(this.IndustryCount_Imputed);
			Vector vector5 = new Vector(this.IndustryCount_Imputed);
			int num4;
			int num6;
			checked
			{
				int num = this.CommodityCount_Imputed - 6;
				for (int i = 0; i <= num; i++)
				{
					int num2 = this.IndustryCount_Imputed - 4;
					int num3 = this.IndustryCount_Imputed - 5;
					for (int j = 0; j <= num3; j++)
					{
						this.Use_Table_Total[i, j] = this.Use_Table[i, j];
						this.Use_Table_FedState[i, j] = this.Use_Table[i, j];
					}
					unchecked
					{
						this.Use_Table_Total[i, num2] = this.Use_Table[i, num2] + this.Use_Table[i, checked(num2 + 1)] + this.Use_Table[i, checked(num2 + 2)] + this.Use_Table[i, checked(num2 + 3)];
						this.Use_Table_FedState[i, num2] = this.Use_Table[i, num2] + this.Use_Table[i, checked(num2 + 1)];
						this.Use_Table_FedState[i, checked(num2 + 1)] = this.Use_Table[i, checked(num2 + 2)] + this.Use_Table[i, checked(num2 + 3)];
					}
				}
				num4 = this.CommodityCount_Total - 3;
				int num5 = this.IndustryCount_Imputed - 1;
				for (int k = 0; k <= num5; k++)
				{
					unchecked
					{
						vector[k] = this.Use_Table[num4, k] + this.Use_Table[checked(num4 + 1), k] + this.Use_Table[checked(num4 + 2), k] + this.Use_Table[checked(num4 + 3), k];
						vector2[k] = this.Use_Table[num4, k] + this.Use_Table[checked(num4 + 1), k];
						vector3[k] = this.Use_Table[checked(num4 + 2), k] + this.Use_Table[checked(num4 + 3), k];
					}
					vector5[k] = this.Use_Table[num4 + 4, k];
					vector4[k] = this.Use_Table[num4 + 5, k];
				}
				num6 = this.IndustryCount_Imputed - 4;
			}
			vector[num6] = vector[num6] + vector[checked(num6 + 1)] + vector[checked(num6 + 2)] + vector[checked(num6 + 3)];
			vector2[num6] += vector2[checked(num6 + 1)];
			vector3[num6] += vector3[checked(num6 + 1)];
			vector2[checked(num6 + 1)] = vector2[checked(num6 + 2)] + vector2[checked(num6 + 3)];
			vector3[checked(num6 + 1)] = vector3[checked(num6 + 2)] + vector3[checked(num6 + 3)];
			vector5[num6] += vector5[checked(num6 + 1)];
			vector4[num6] += vector4[checked(num6 + 1)];
			vector5[checked(num6 + 1)] = vector5[checked(num6 + 2)] + vector5[checked(num6 + 3)];
			vector4[checked(num6 + 1)] = vector4[checked(num6 + 2)] + vector4[checked(num6 + 3)];
			checked
			{
				int num7 = this.IndustryCount_Imputed - 3;
				for (int l = 0; l <= num7; l++)
				{
					if (l < this.IndustryCount_Imputed - 3)
					{
						this.Use_Table_Total[num4, l] = vector[l];
						if (l < this.IndustryCount_Imputed - 4)
						{
							this.Use_Table_Total[num4 + 1, l] = vector5[l];
							this.Use_Table_Total[num4 + 2, l] = vector4[l];
						}
						else
						{
							this.Use_Table_Total[num4 + 1, l] = unchecked(vector5[l] + vector5[checked(l + 1)]);
							this.Use_Table_Total[num4 + 2, l] = unchecked(vector4[l] + vector4[checked(l + 1)]);
						}
					}
					this.Use_Table_FedState[num4, l] = vector2[l];
					this.Use_Table_FedState[num4 + 1, l] = vector3[l];
					this.Use_Table_FedState[num4 + 2, l] = vector5[l];
					this.Use_Table_FedState[num4 + 3, l] = vector4[l];
				}
			}
		}

		public void CalcAgg()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.ImputedGov:
				break;
			case GenDataStatus.dataStatusType.FedStateLocal:
				break;
			case GenDataStatus.dataStatusType.TotalGovernment:
				break;
			}
		}

		public void setIndHeaders(ref string[] _h)
		{
			this.hdrInd_imputed = _h;
			checked
			{
				this.hdrInd_FedState = new string[this.hdrInd_imputed.Length - 3 + 1];
				this.hdrInd_total = new string[this.hdrInd_imputed.Length - 4 + 1];
				int num = this.hdrInd_imputed.Length - 4;
				for (int i = 0; i <= num; i++)
				{
					this.hdrInd_FedState[i] = this.hdrInd_imputed[i];
					this.hdrInd_total[i] = this.hdrInd_imputed[i];
				}
				this.hdrInd_total[this.hdrInd_imputed.Length - 4] = "Total Government";
				this.hdrInd_FedState[this.hdrInd_imputed.Length - 3] = "State and Local Government";
				this.hdrInd_FedState[this.hdrInd_imputed.Length - 4] = "Federal Government";
			}
		}

		public void setCommHeaders(ref string[] _h)
		{
			this.hdrComm_Imputed = _h;
			checked
			{
				this.hdrComm_FedState = new string[this.hdrComm_Imputed.Length - 3 + 1];
				this.hdrComm_Total = new string[this.hdrComm_Imputed.Length - 4 + 1];
				int num = this.hdrComm_Imputed.Length - 6;
				for (int i = 0; i <= num; i++)
				{
					this.hdrComm_FedState[i] = this.hdrComm_Imputed[i];
					this.hdrComm_Total[i] = this.hdrComm_Imputed[i];
				}
				this.hdrComm_Total[this.hdrComm_Imputed.Length - 6] = "Total Government";
				this.hdrComm_Total[this.hdrComm_Imputed.Length - 5] = this.hdrComm_Imputed[this.hdrComm_Imputed.Length - 2];
				this.hdrComm_Total[this.hdrComm_Imputed.Length - 4] = this.hdrComm_Imputed[this.hdrComm_Imputed.Length - 1];
				this.hdrComm_FedState[this.hdrComm_Imputed.Length - 5] = "State and Local Government";
				this.hdrComm_FedState[this.hdrComm_Imputed.Length - 6] = "Federal Government";
				this.hdrComm_FedState[this.hdrComm_Imputed.Length - 4] = this.hdrComm_Imputed[this.hdrComm_Imputed.Length - 2];
				this.hdrComm_FedState[this.hdrComm_Imputed.Length - 3] = this.hdrComm_Imputed[this.hdrComm_Imputed.Length - 1];
			}
		}

		public string[] getCommHeadersFedState()
		{
			return this.hdrComm_FedState;
		}

		public string[] getCommHeadersTotal()
		{
			return this.hdrComm_Total;
		}

		public string[] getCommHeadersImputed()
		{
			return this.hdrComm_Imputed;
		}

		public object getIndustryHeadersImputed()
		{
			return this.hdrInd_imputed;
		}

		public void setCountsInd(int nind)
		{
			this.IndustryCount_Imputed = nind;
			checked
			{
				this.IndustryCount_FedState = this.IndustryCount_Imputed - 2;
				this.IndustryCount_Total = this.IndustryCount_Imputed - 3;
			}
		}

		public void setCountsComm(int ncomm)
		{
			this.CommodityCount_Imputed = ncomm;
			checked
			{
				this.CommodityCount_FedState = this.CommodityCount_Imputed - 2;
				this.CommodityCount_Total = this.CommodityCount_Imputed - 3;
			}
		}
	}
}
