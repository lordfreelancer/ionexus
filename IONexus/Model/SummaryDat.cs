using System;
using System.Runtime.CompilerServices;

namespace IONexus.Model
{
	[Serializable]
	public class SummaryDat : ICloneable
	{
		public enum bcode
		{
			BASE_DATA,
			USER_DATA,
			REGION_DATA
		}

		public string datayear;

		public string dataregionalized;

		public string requirementsTables;

		public string regmethod;

		public string regby;

		public string dataaggregated;

		public bool notFirstRead;

		public string EmploymentStatus;

		public string DataLabel;

		public bcode DataTypeCode;

		public string DataVersion;

		public object DataTimeStamp;

		public string CurrentLabel;

		public object DataType;

		public bool IsRegionalizationUserDefined;

		public const string EMPLOYMENT_FTE = "FTE";

		public const string EMPLOYMENT_JOBS = "Jobs";

		public const string DATACODE_BASE = "Base Data";

		public const string DATACODE_USER = "User Defined";

		public const string DATACODE_REGION = "User Registered";

		public const string YES = "Yes";

		public const string NO = "No";

		public object getEmploymentStatus()
		{
			string result = "FTE";
			GenDataStatus genDataStatus = new GenDataStatus();
			if (genDataStatus.getFTE_Type() == GenDataStatus.dataEmpStatus.JOB_BASED)
			{
				result = "Jobs";
			}
			return result;
		}

		public object getDataType()
		{
			string result = "";
			if (this.DataTypeCode == bcode.BASE_DATA)
			{
				result = "Base Data";
			}
			if (this.DataTypeCode == bcode.USER_DATA)
			{
				result = "User Defined";
			}
			if (this.DataTypeCode == bcode.REGION_DATA)
			{
				result = "User Registered";
			}
			return result;
		}

		public void InitSetup()
		{
			this.datayear = "";
			this.dataregionalized = "No";
			this.requirementsTables = "No";
			this.regmethod = "";
			this.dataaggregated = "No";
			this.regby = "";
			this.EmploymentStatus = "FTE";
			this.DataLabel = this.datayear;
			this.DataVersion = "1.0.2012.1";
		}

		public void SetRegionalizationStatus(bool v)
		{
            //TODO Later
			//if (this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv_0024Init.State = 2;
			//		this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv = "No";
			//	}
			//	else if (this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv_0024Init);
			//}
			//if (v)
			//{
			//	this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv = "Yes";
			//}
			//this.dataregionalized = this._0024STATIC_0024SetRegionalizationStatus_002420112_0024cv;
		}

		public SummaryDat()
		{
			this.datayear = "";
			this.dataregionalized = "";
			this.requirementsTables = "";
			this.regmethod = "";
			this.regby = "";
			this.dataaggregated = "";
			this.notFirstRead = false;
			this.EmploymentStatus = "";
			this.DataLabel = "";
			this.DataTypeCode = bcode.BASE_DATA;
			this.DataVersion = "1.0";
			this.DataTimeStamp = "";
			this.CurrentLabel = "INIT";
			this.DataType = "DEMO";
			this.IsRegionalizationUserDefined = false;
			this.InitSetup();
		}

		public object Clone()
		{
			SummaryDat summaryDat = new SummaryDat();
			SummaryDat summaryDat2 = summaryDat;
			summaryDat2.datayear = this.datayear;
			summaryDat2.dataregionalized = this.dataregionalized;
			summaryDat2.requirementsTables = this.requirementsTables;
			summaryDat2.regmethod = this.regmethod;
			summaryDat2.regby = this.regby;
			summaryDat2.dataaggregated = this.dataaggregated;
			summaryDat2.notFirstRead = this.notFirstRead;
			summaryDat2.EmploymentStatus = this.EmploymentStatus;
			summaryDat2.DataLabel = this.DataLabel;
			summaryDat2.DataTypeCode = this.DataTypeCode;
			summaryDat2.CurrentLabel = this.CurrentLabel;
			summaryDat2.DataTimeStamp = RuntimeHelpers.GetObjectValue(this.DataTimeStamp);
			summaryDat2.DataVersion = this.DataVersion;
			summaryDat2.DataType = RuntimeHelpers.GetObjectValue(this.DataType);
			summaryDat2.IsRegionalizationUserDefined = this.IsRegionalizationUserDefined;
			summaryDat2 = null;
			return summaryDat;
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Clone
			return this.Clone();
		}

		public object getSummary(ref BEAData b)
		{
			string text = "";
			text = "Data Label\t\t\t\t" + this.DataLabel + "\r\n";
			text = text + "Year\t\t\t\t\t" + this.datayear + "\r\n";
            //TODO Need to convert to C# Code
			//text = Conversions.ToString(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(text + "Data Type\t\t\t\t", this.getDataType()), " ("), this.DataType), ")"), "\r\n"));
			text = text + "Data Regionalized\t\t\t" + this.dataregionalized + "\r\n";
			text = text + "Regionalization Method\t\t\t" + this.regmethod + "\r\n";
			text = text + "Regionalized by\t\t\t\t" + this.regby + "\r\n";
			text = text + "Requirements Tables\t\t\t" + this.requirementsTables + "\r\n";
			text = text + "Number of Industries\t\t\t" + b.N_INDUSTRIES.ToString() + "\r\n";
			text = text + "Number of Commodities\t\t\t" + b.N_COMMODITIES.ToString() + "\r\n";
			text = text + "Data Aggregated\t\t\t\t" + this.dataaggregated + "\r\n";
			string text2 = "";
			switch (b.GovernmentAggStatus.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				text2 = "Federal and State with Local";
				break;
			case GenDataStatus.dataStatusType.ImputedGov:
				text2 = "Imputed IO";
				break;
			case GenDataStatus.dataStatusType.TotalGovernment:
				text2 = "Total Government";
				break;
			default:
				text2 = "Unknown";
				break;
			}
			text = text + "Government Aggregation Method\t\t" + text2 + "\r\n";
			text = text + "Employment Units\t\t\t" + this.getEmploymentStatus().ToString() + "\r\n";
			text = text + "Data Version\t\t\t\t" + this.DataVersion + "\r\n";
			return text + "Timestamp\t\t\t\t"+ this.DataTimeStamp.ToString() + "\r\n";
		}
	}
}
