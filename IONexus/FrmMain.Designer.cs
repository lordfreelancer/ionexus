﻿namespace IONexus
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.midgvPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.midgvCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.midgvUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.midgvCopyHdrs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.miDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.ActivateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeactivateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.miRegionalize = new System.Windows.Forms.ToolStripMenuItem();
            this.ImpactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miImpactInd1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miImpactInd2 = new System.Windows.Forms.ToolStripMenuItem();
            this.miImpactComm1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miImpactComm2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.miSave = new System.Windows.Forms.ToolStripButton();
            this.miPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.miCopy = new System.Windows.Forms.ToolStripButton();
            this.miPaste = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miDecDecimal = new System.Windows.Forms.ToolStripButton();
            this.miIncDecimal = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.RegButton = new System.Windows.Forms.ToolStripButton();
            this.TSB4 = new System.Windows.Forms.ToolStripButton();
            this.YearSelect_CBoxTS = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewIOAccountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RawDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miStateData = new System.Windows.Forms.ToolStripMenuItem();
            this.miCRateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miEmployment = new System.Windows.Forms.ToolStripMenuItem();
            this.miFTERatio = new System.Windows.Forms.ToolStripMenuItem();
            this.miGRPMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miGRP = new System.Windows.Forms.ToolStripMenuItem();
            this.RequirementsTablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MultipliersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SectorDistributionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IndustryAccountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CommodityAccountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IndustryLaborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CommodityTradeDistributionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showChartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miAggregate = new System.Windows.Forms.ToolStripMenuItem();
            this.miNewAgg = new System.Windows.Forms.ToolStripMenuItem();
            this.miLoadAgg = new System.Windows.Forms.ToolStripMenuItem();
            this._ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ModifyDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UseEditMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MakeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FinalDemandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miWorkspaceSummary = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miLoadData = new System.Windows.Forms.ToolStripMenuItem();
            this.miSave1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miPrint1 = new System.Windows.Forms.ToolStripMenuItem();
            this.PreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.IncreateDecimalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DecreaseDecimalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyWHeadersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UseAbbrevHeadersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OptionsTool = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmOtherOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.ProgramResetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnu1 = new System.Windows.Forms.MenuStrip();
            this.miOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.miCopy1 = new System.Windows.Forms.ToolStripMenuItem();
            this.miPaste1 = new System.Windows.Forms.ToolStripMenuItem();
            this.TabMain = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.dgvMain = new System.Windows.Forms.DataGridView();
            this.Table_Label = new System.Windows.Forms.Label();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvMenu.SuspendLayout();
            this.ToolStrip1.SuspendLayout();
            this.mnu1.SuspendLayout();
            this.TabMain.SuspendLayout();
            this.TabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(185, 6);
            // 
            // midgvPaste
            // 
            this.midgvPaste.Name = "midgvPaste";
            this.midgvPaste.Size = new System.Drawing.Size(188, 22);
            this.midgvPaste.Text = "Paste";
            // 
            // midgvCopy
            // 
            this.midgvCopy.Name = "midgvCopy";
            this.midgvCopy.Size = new System.Drawing.Size(188, 22);
            this.midgvCopy.Text = "Copy";
            // 
            // dgvMenu
            // 
            this.dgvMenu.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.dgvMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.midgvCopy,
            this.midgvPaste,
            this.midgvUndo,
            this.toolStripSeparator,
            this.midgvCopyHdrs});
            this.dgvMenu.Name = "dgvMenu";
            this.dgvMenu.Size = new System.Drawing.Size(189, 98);
            // 
            // midgvUndo
            // 
            this.midgvUndo.Enabled = false;
            this.midgvUndo.Name = "midgvUndo";
            this.midgvUndo.Size = new System.Drawing.Size(188, 22);
            this.midgvUndo.Text = "Undo";
            // 
            // midgvCopyHdrs
            // 
            this.midgvCopyHdrs.Name = "midgvCopyHdrs";
            this.midgvCopyHdrs.Size = new System.Drawing.Size(188, 22);
            this.midgvCopyHdrs.Text = "Not Copying Headers";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAbout,
            this.miDocument,
            this.ActivateToolStripMenuItem,
            this.DeactivateToolStripMenuItem});
            this.toolStripMenuItem5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(51, 23);
            this.toolStripMenuItem5.Text = "Help";
            // 
            // miAbout
            // 
            this.miAbout.Name = "miAbout";
            this.miAbout.Size = new System.Drawing.Size(177, 24);
            this.miAbout.Text = "About";
            // 
            // miDocument
            // 
            this.miDocument.Name = "miDocument";
            this.miDocument.Size = new System.Drawing.Size(177, 24);
            this.miDocument.Text = "Documentation";
            this.miDocument.Visible = false;
            // 
            // ActivateToolStripMenuItem
            // 
            this.ActivateToolStripMenuItem.Name = "ActivateToolStripMenuItem";
            this.ActivateToolStripMenuItem.Size = new System.Drawing.Size(177, 24);
            this.ActivateToolStripMenuItem.Text = "Activate...";
            this.ActivateToolStripMenuItem.ToolTipText = "Activate an IO-Snap License";
            this.ActivateToolStripMenuItem.Visible = false;
            // 
            // DeactivateToolStripMenuItem
            // 
            this.DeactivateToolStripMenuItem.Name = "DeactivateToolStripMenuItem";
            this.DeactivateToolStripMenuItem.Size = new System.Drawing.Size(177, 24);
            this.DeactivateToolStripMenuItem.Text = "Deactivate...";
            this.DeactivateToolStripMenuItem.ToolTipText = "Deactivate current IO-Snap License";
            this.DeactivateToolStripMenuItem.Visible = false;
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miRegionalize,
            this.ImpactsToolStripMenuItem});
            this.toolStripMenuItem6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(75, 23);
            this.toolStripMenuItem6.Text = "Analysis";
            // 
            // miRegionalize
            // 
            this.miRegionalize.Name = "miRegionalize";
            this.miRegionalize.Size = new System.Drawing.Size(149, 24);
            this.miRegionalize.Text = "Grouping...";
            // 
            // ImpactsToolStripMenuItem
            // 
            this.ImpactsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miImpactInd1,
            this.miImpactInd2,
            this.miImpactComm1,
            this.miImpactComm2});
            this.ImpactsToolStripMenuItem.Name = "ImpactsToolStripMenuItem";
            this.ImpactsToolStripMenuItem.Size = new System.Drawing.Size(149, 24);
            this.ImpactsToolStripMenuItem.Text = "Impacts";
            // 
            // miImpactInd1
            // 
            this.miImpactInd1.Name = "miImpactInd1";
            this.miImpactInd1.Size = new System.Drawing.Size(145, 24);
            this.miImpactInd1.Text = "Scenario 1";
            // 
            // miImpactInd2
            // 
            this.miImpactInd2.Name = "miImpactInd2";
            this.miImpactInd2.Size = new System.Drawing.Size(145, 24);
            this.miImpactInd2.Text = "Scenario 2";
            // 
            // miImpactComm1
            // 
            this.miImpactComm1.Name = "miImpactComm1";
            this.miImpactComm1.Size = new System.Drawing.Size(145, 24);
            this.miImpactComm1.Text = "Scenario 3";
            // 
            // miImpactComm2
            // 
            this.miImpactComm2.Name = "miImpactComm2";
            this.miImpactComm2.Size = new System.Drawing.Size(145, 24);
            this.miImpactComm2.Text = "Scenario 4";
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStrip1.GripMargin = new System.Windows.Forms.Padding(0);
            this.ToolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton2,
            this.miSave,
            this.miPrint,
            this.toolStripSeparator2,
            this.miCopy,
            this.miPaste,
            this.toolStripSeparator3,
            this.miDecDecimal,
            this.miIncDecimal,
            this.toolStripSeparator4,
            this.ToolStripButton4,
            this.ToolStripButton1,
            this.RegButton,
            this.TSB4,
            this.YearSelect_CBoxTS,
            this.toolStripSeparator7});
            this.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.ToolStrip1.Location = new System.Drawing.Point(0, 54);
            this.ToolStrip1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(662, 25);
            this.ToolStrip1.Stretch = true;
            this.ToolStrip1.TabIndex = 11;
            this.ToolStrip1.Text = "ToolStrip1";
            this.ToolStrip1.Visible = false;
            // 
            // ToolStripButton2
            // 
            this.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton2.Name = "ToolStripButton2";
            this.ToolStripButton2.Size = new System.Drawing.Size(23, 4);
            this.ToolStripButton2.Text = "ToolStripButton2";
            this.ToolStripButton2.ToolTipText = "Load Data";
            // 
            // miSave
            // 
            this.miSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.miSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miSave.Name = "miSave";
            this.miSave.Size = new System.Drawing.Size(23, 4);
            this.miSave.Text = "&Save";
            this.miSave.ToolTipText = "Save this Data";
            // 
            // miPrint
            // 
            this.miPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.miPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miPrint.Name = "miPrint";
            this.miPrint.Size = new System.Drawing.Size(23, 4);
            this.miPrint.Text = "&Print";
            this.miPrint.ToolTipText = "Print";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // miCopy
            // 
            this.miCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.miCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miCopy.Name = "miCopy";
            this.miCopy.Size = new System.Drawing.Size(23, 4);
            this.miCopy.Text = "&Copy";
            this.miCopy.ToolTipText = "Copy selected Data";
            // 
            // miPaste
            // 
            this.miPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.miPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miPaste.Name = "miPaste";
            this.miPaste.Size = new System.Drawing.Size(23, 4);
            this.miPaste.Text = "&Paste";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // miDecDecimal
            // 
            this.miDecDecimal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.miDecDecimal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miDecDecimal.Name = "miDecDecimal";
            this.miDecDecimal.Size = new System.Drawing.Size(23, 4);
            this.miDecDecimal.Text = "ToolStripButton1";
            this.miDecDecimal.ToolTipText = "Decrease Decimals";
            // 
            // miIncDecimal
            // 
            this.miIncDecimal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.miIncDecimal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miIncDecimal.Name = "miIncDecimal";
            this.miIncDecimal.Size = new System.Drawing.Size(23, 4);
            this.miIncDecimal.Text = "ToolStripButton2";
            this.miIncDecimal.ToolTipText = "Increase Decimals";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 23);
            // 
            // ToolStripButton4
            // 
            this.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton4.Name = "ToolStripButton4";
            this.ToolStripButton4.Size = new System.Drawing.Size(23, 4);
            this.ToolStripButton4.Text = "ToolStripButton1";
            this.ToolStripButton4.ToolTipText = "Remove All Tabs";
            // 
            // ToolStripButton1
            // 
            this.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton1.Name = "ToolStripButton1";
            this.ToolStripButton1.Size = new System.Drawing.Size(23, 4);
            this.ToolStripButton1.Text = "ToolStripButton1";
            this.ToolStripButton1.ToolTipText = "Remove Current Tab";
            // 
            // RegButton
            // 
            this.RegButton.Font = new System.Drawing.Font("Calibri", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RegButton.Name = "RegButton";
            this.RegButton.Size = new System.Drawing.Size(77, 21);
            this.RegButton.Text = "Regionalize";
            this.RegButton.ToolTipText = "Start Regionalization";
            // 
            // TSB4
            // 
            this.TSB4.Font = new System.Drawing.Font("Calibri", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TSB4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TSB4.Name = "TSB4";
            this.TSB4.Size = new System.Drawing.Size(93, 21);
            this.TSB4.Text = "Table Headers";
            this.TSB4.ToolTipText = "Changes table headers copy status ";
            // 
            // YearSelect_CBoxTS
            // 
            this.YearSelect_CBoxTS.AutoToolTip = true;
            this.YearSelect_CBoxTS.Font = new System.Drawing.Font("Calibri", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YearSelect_CBoxTS.Name = "YearSelect_CBoxTS";
            this.YearSelect_CBoxTS.Size = new System.Drawing.Size(258, 25);
            this.YearSelect_CBoxTS.ToolTipText = "ghghghjghg";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.AutoSize = false;
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ViewToolStripMenuItem,
            this.EditToolStripMenuItem,
            this.miWorkspaceSummary});
            this.toolStripMenuItem4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(52, 23);
            this.toolStripMenuItem4.Text = "Data";
            // 
            // ViewToolStripMenuItem
            // 
            this.ViewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ViewIOAccountsToolStripMenuItem,
            this.RawDataToolStripMenuItem,
            this.miGRP,
            this.RequirementsTablesToolStripMenuItem,
            this.MultipliersToolStripMenuItem,
            this.SectorDistributionsToolStripMenuItem,
            this.showChartToolStripMenuItem,
            this.configToolStripMenuItem});
            this.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem";
            this.ViewToolStripMenuItem.Size = new System.Drawing.Size(212, 24);
            this.ViewToolStripMenuItem.Text = "View";
            // 
            // ViewIOAccountsToolStripMenuItem
            // 
            this.ViewIOAccountsToolStripMenuItem.Name = "ViewIOAccountsToolStripMenuItem";
            this.ViewIOAccountsToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.ViewIOAccountsToolStripMenuItem.Text = "IO Accounts";
            // 
            // RawDataToolStripMenuItem
            // 
            this.RawDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miStateData,
            this.miCRateMenuItem,
            this.miEmployment,
            this.miFTERatio,
            this.miGRPMenuItem});
            this.RawDataToolStripMenuItem.Name = "RawDataToolStripMenuItem";
            this.RawDataToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.RawDataToolStripMenuItem.Text = "State Industry Data";
            this.RawDataToolStripMenuItem.Visible = false;
            // 
            // miStateData
            // 
            this.miStateData.Name = "miStateData";
            this.miStateData.Size = new System.Drawing.Size(308, 24);
            this.miStateData.Text = "By State";
            // 
            // miCRateMenuItem
            // 
            this.miCRateMenuItem.Name = "miCRateMenuItem";
            this.miCRateMenuItem.Size = new System.Drawing.Size(308, 24);
            this.miCRateMenuItem.Text = "Compensation Rates (All States)";
            // 
            // miEmployment
            // 
            this.miEmployment.Name = "miEmployment";
            this.miEmployment.Size = new System.Drawing.Size(308, 24);
            this.miEmployment.Text = "Employment (All States)";
            // 
            // miFTERatio
            // 
            this.miFTERatio.Name = "miFTERatio";
            this.miFTERatio.Size = new System.Drawing.Size(308, 24);
            this.miFTERatio.Text = "FTE-Job Ratios";
            // 
            // miGRPMenuItem
            // 
            this.miGRPMenuItem.Name = "miGRPMenuItem";
            this.miGRPMenuItem.Size = new System.Drawing.Size(308, 24);
            this.miGRPMenuItem.Text = "Gross Industrial Product (All States)";
            // 
            // miGRP
            // 
            this.miGRP.Name = "miGRP";
            this.miGRP.Size = new System.Drawing.Size(214, 24);
            this.miGRP.Text = "Gross Product";
            this.miGRP.Visible = false;
            // 
            // RequirementsTablesToolStripMenuItem
            // 
            this.RequirementsTablesToolStripMenuItem.Name = "RequirementsTablesToolStripMenuItem";
            this.RequirementsTablesToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.RequirementsTablesToolStripMenuItem.Text = "Requirements Tables";
            this.RequirementsTablesToolStripMenuItem.Visible = false;
            // 
            // MultipliersToolStripMenuItem
            // 
            this.MultipliersToolStripMenuItem.Name = "MultipliersToolStripMenuItem";
            this.MultipliersToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.MultipliersToolStripMenuItem.Text = "Multipliers";
            this.MultipliersToolStripMenuItem.Visible = false;
            // 
            // SectorDistributionsToolStripMenuItem
            // 
            this.SectorDistributionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IndustryAccountsToolStripMenuItem,
            this.CommodityAccountsToolStripMenuItem,
            this.IndustryLaborToolStripMenuItem,
            this.CommodityTradeDistributionsToolStripMenuItem});
            this.SectorDistributionsToolStripMenuItem.Name = "SectorDistributionsToolStripMenuItem";
            this.SectorDistributionsToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.SectorDistributionsToolStripMenuItem.Text = "Sector Distributions";
            this.SectorDistributionsToolStripMenuItem.Visible = false;
            // 
            // IndustryAccountsToolStripMenuItem
            // 
            this.IndustryAccountsToolStripMenuItem.Name = "IndustryAccountsToolStripMenuItem";
            this.IndustryAccountsToolStripMenuItem.Size = new System.Drawing.Size(278, 24);
            this.IndustryAccountsToolStripMenuItem.Text = "Industry Accounts";
            // 
            // CommodityAccountsToolStripMenuItem
            // 
            this.CommodityAccountsToolStripMenuItem.Name = "CommodityAccountsToolStripMenuItem";
            this.CommodityAccountsToolStripMenuItem.Size = new System.Drawing.Size(278, 24);
            this.CommodityAccountsToolStripMenuItem.Text = "Commodity Accounts";
            // 
            // IndustryLaborToolStripMenuItem
            // 
            this.IndustryLaborToolStripMenuItem.Name = "IndustryLaborToolStripMenuItem";
            this.IndustryLaborToolStripMenuItem.Size = new System.Drawing.Size(278, 24);
            this.IndustryLaborToolStripMenuItem.Text = "Industry Labor";
            // 
            // CommodityTradeDistributionsToolStripMenuItem
            // 
            this.CommodityTradeDistributionsToolStripMenuItem.Name = "CommodityTradeDistributionsToolStripMenuItem";
            this.CommodityTradeDistributionsToolStripMenuItem.Size = new System.Drawing.Size(278, 24);
            this.CommodityTradeDistributionsToolStripMenuItem.Text = "Commodity Trade Distributions";
            // 
            // showChartToolStripMenuItem
            // 
            this.showChartToolStripMenuItem.Name = "showChartToolStripMenuItem";
            this.showChartToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.showChartToolStripMenuItem.Text = "Show Chart";
            this.showChartToolStripMenuItem.Click += new System.EventHandler(this.showChartToolStripMenuItem_Click);
            // 
            // EditToolStripMenuItem
            // 
            this.EditToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAggregate,
            this.ModifyDataToolStripMenuItem});
            this.EditToolStripMenuItem.Name = "EditToolStripMenuItem";
            this.EditToolStripMenuItem.Size = new System.Drawing.Size(212, 24);
            this.EditToolStripMenuItem.Text = "Edit";
            // 
            // miAggregate
            // 
            this.miAggregate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miNewAgg,
            this.miLoadAgg,
            this._ToolStripSeparator3});
            this.miAggregate.Name = "miAggregate";
            this.miAggregate.Size = new System.Drawing.Size(158, 24);
            this.miAggregate.Text = "Aggregation";
            // 
            // miNewAgg
            // 
            this.miNewAgg.Name = "miNewAgg";
            this.miNewAgg.Size = new System.Drawing.Size(145, 24);
            this.miNewAgg.Text = "Aggregate";
            this.miNewAgg.Click += new System.EventHandler(this.miAggregate_Click);
            // 
            // miLoadAgg
            // 
            this.miLoadAgg.Enabled = false;
            this.miLoadAgg.Name = "miLoadAgg";
            this.miLoadAgg.Size = new System.Drawing.Size(145, 24);
            this.miLoadAgg.Text = "Browse...";
            // 
            // _ToolStripSeparator3
            // 
            this._ToolStripSeparator3.Name = "_ToolStripSeparator3";
            this._ToolStripSeparator3.Size = new System.Drawing.Size(142, 6);
            // 
            // ModifyDataToolStripMenuItem
            // 
            this.ModifyDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UseEditMenuItem,
            this.MakeToolStripMenuItem,
            this.FinalDemandToolStripMenuItem,
            this.StateToolStripMenuItem});
            this.ModifyDataToolStripMenuItem.Name = "ModifyDataToolStripMenuItem";
            this.ModifyDataToolStripMenuItem.Size = new System.Drawing.Size(158, 24);
            this.ModifyDataToolStripMenuItem.Text = "Modify Data";
            // 
            // UseEditMenuItem
            // 
            this.UseEditMenuItem.Name = "UseEditMenuItem";
            this.UseEditMenuItem.Size = new System.Drawing.Size(167, 24);
            this.UseEditMenuItem.Text = "Use";
            // 
            // MakeToolStripMenuItem
            // 
            this.MakeToolStripMenuItem.Name = "MakeToolStripMenuItem";
            this.MakeToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.MakeToolStripMenuItem.Text = "Make";
            // 
            // FinalDemandToolStripMenuItem
            // 
            this.FinalDemandToolStripMenuItem.Name = "FinalDemandToolStripMenuItem";
            this.FinalDemandToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.FinalDemandToolStripMenuItem.Text = "Final Demand";
            // 
            // StateToolStripMenuItem
            // 
            this.StateToolStripMenuItem.Name = "StateToolStripMenuItem";
            this.StateToolStripMenuItem.Size = new System.Drawing.Size(167, 24);
            this.StateToolStripMenuItem.Text = "State";
            // 
            // miWorkspaceSummary
            // 
            this.miWorkspaceSummary.Name = "miWorkspaceSummary";
            this.miWorkspaceSummary.Size = new System.Drawing.Size(212, 24);
            this.miWorkspaceSummary.Text = "Workspace Summary";
            // 
            // toolStripMenuItem
            // 
            this.toolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miLoadData,
            this.miSave1,
            this.miPrint1,
            this.PreferencesToolStripMenuItem,
            this.ProgramResetToolStripMenuItem,
            this.miExit});
            this.toolStripMenuItem.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem.Name = "toolStripMenuItem";
            this.toolStripMenuItem.Size = new System.Drawing.Size(44, 23);
            this.toolStripMenuItem.Text = "File";
            // 
            // miLoadData
            // 
            this.miLoadData.Name = "miLoadData";
            this.miLoadData.Size = new System.Drawing.Size(173, 24);
            this.miLoadData.Text = "Load Data";
            this.miLoadData.Click += new System.EventHandler(this.miLoadData_Click);
            // 
            // miSave1
            // 
            this.miSave1.Name = "miSave1";
            this.miSave1.Size = new System.Drawing.Size(173, 24);
            this.miSave1.Text = "Save Data";
            this.miSave1.Click += new System.EventHandler(this.miSave1_Click);
            // 
            // miPrint1
            // 
            this.miPrint1.Name = "miPrint1";
            this.miPrint1.Size = new System.Drawing.Size(173, 24);
            this.miPrint1.Text = "Print";
            // 
            // PreferencesToolStripMenuItem
            // 
            this.PreferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.OptionsTool,
            this.tsmOtherOptions});
            this.PreferencesToolStripMenuItem.Name = "PreferencesToolStripMenuItem";
            this.PreferencesToolStripMenuItem.Size = new System.Drawing.Size(173, 24);
            this.PreferencesToolStripMenuItem.Text = "Preferences";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IncreateDecimalsToolStripMenuItem,
            this.DecreaseDecimalsToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(129, 24);
            this.toolStripMenuItem2.Text = "Display";
            // 
            // IncreateDecimalsToolStripMenuItem
            // 
            this.IncreateDecimalsToolStripMenuItem.Name = "IncreateDecimalsToolStripMenuItem";
            this.IncreateDecimalsToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.IncreateDecimalsToolStripMenuItem.Text = "Increase Decimals";
            // 
            // DecreaseDecimalsToolStripMenuItem
            // 
            this.DecreaseDecimalsToolStripMenuItem.Name = "DecreaseDecimalsToolStripMenuItem";
            this.DecreaseDecimalsToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.DecreaseDecimalsToolStripMenuItem.Text = "Decrease Decimals";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyWHeadersToolStripMenuItem,
            this.UseAbbrevHeadersToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(129, 24);
            this.toolStripMenuItem3.Text = "Other";
            this.toolStripMenuItem3.Visible = false;
            // 
            // CopyWHeadersToolStripMenuItem
            // 
            this.CopyWHeadersToolStripMenuItem.Name = "CopyWHeadersToolStripMenuItem";
            this.CopyWHeadersToolStripMenuItem.Size = new System.Drawing.Size(215, 24);
            this.CopyWHeadersToolStripMenuItem.Text = "Not Copying Headers";
            // 
            // UseAbbrevHeadersToolStripMenuItem
            // 
            this.UseAbbrevHeadersToolStripMenuItem.Name = "UseAbbrevHeadersToolStripMenuItem";
            this.UseAbbrevHeadersToolStripMenuItem.Size = new System.Drawing.Size(215, 24);
            this.UseAbbrevHeadersToolStripMenuItem.Text = "Use Abbrev Headers";
            // 
            // OptionsTool
            // 
            this.OptionsTool.Name = "OptionsTool";
            this.OptionsTool.Size = new System.Drawing.Size(129, 24);
            this.OptionsTool.Text = "ICOR";
            this.OptionsTool.Click += new System.EventHandler(this.OptionsTool_Click);
            // 
            // tsmOtherOptions
            // 
            this.tsmOtherOptions.Name = "tsmOtherOptions";
            this.tsmOtherOptions.Size = new System.Drawing.Size(129, 24);
            this.tsmOtherOptions.Text = "Options";
            this.tsmOtherOptions.Click += new System.EventHandler(this.tsmOtherOptions_Click);
            // 
            // ProgramResetToolStripMenuItem
            // 
            this.ProgramResetToolStripMenuItem.Name = "ProgramResetToolStripMenuItem";
            this.ProgramResetToolStripMenuItem.Size = new System.Drawing.Size(173, 24);
            this.ProgramResetToolStripMenuItem.Text = "Program Reset";
            this.ProgramResetToolStripMenuItem.Visible = false;
            // 
            // miExit
            // 
            this.miExit.Name = "miExit";
            this.miExit.Size = new System.Drawing.Size(173, 24);
            this.miExit.Text = "Exit";
            // 
            // mnu1
            // 
            this.mnu1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnu1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.mnu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem,
            this.miOptions,
            this.toolStripMenuItem4,
            this.toolStripMenuItem6,
            this.toolStripMenuItem5});
            this.mnu1.Location = new System.Drawing.Point(0, 0);
            this.mnu1.Name = "mnu1";
            this.mnu1.Padding = new System.Windows.Forms.Padding(6, 1, 0, 1);
            this.mnu1.Size = new System.Drawing.Size(502, 25);
            this.mnu1.TabIndex = 10;
            this.mnu1.Text = "MenuStrip1";
            // 
            // miOptions
            // 
            this.miOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCopy1,
            this.miPaste1});
            this.miOptions.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miOptions.Name = "miOptions";
            this.miOptions.Size = new System.Drawing.Size(46, 23);
            this.miOptions.Text = "Edit";
            // 
            // miCopy1
            // 
            this.miCopy1.Name = "miCopy1";
            this.miCopy1.ShortcutKeyDisplayString = "CTRL + C";
            this.miCopy1.Size = new System.Drawing.Size(177, 24);
            this.miCopy1.Text = "Copy";
            // 
            // miPaste1
            // 
            this.miPaste1.Name = "miPaste1";
            this.miPaste1.Size = new System.Drawing.Size(177, 24);
            this.miPaste1.Text = "Paste";
            // 
            // TabMain
            // 
            this.TabMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabMain.Controls.Add(this.TabPage1);
            this.TabMain.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabMain.HotTrack = true;
            this.TabMain.Location = new System.Drawing.Point(0, 29);
            this.TabMain.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.TabMain.Name = "TabMain";
            this.TabMain.SelectedIndex = 0;
            this.TabMain.Size = new System.Drawing.Size(496, 176);
            this.TabMain.TabIndex = 13;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.dgvMain);
            this.TabPage1.Location = new System.Drawing.Point(4, 23);
            this.TabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.TabPage1.Size = new System.Drawing.Size(488, 149);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Input";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvMain
            // 
            this.dgvMain.AllowUserToAddRows = false;
            this.dgvMain.AllowUserToDeleteRows = false;
            this.dgvMain.CausesValidation = false;
            this.dgvMain.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dgvMain.ColumnHeadersHeight = 40;
            this.dgvMain.ContextMenuStrip = this.dgvMenu;
            this.dgvMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMain.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvMain.Location = new System.Drawing.Point(2, 2);
            this.dgvMain.Margin = new System.Windows.Forms.Padding(1);
            this.dgvMain.Name = "dgvMain";
            this.dgvMain.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvMain.RowHeadersWidth = 175;
            this.dgvMain.RowTemplate.Height = 40;
            this.dgvMain.Size = new System.Drawing.Size(484, 145);
            this.dgvMain.TabIndex = 0;
            // 
            // Table_Label
            // 
            this.Table_Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Table_Label.AutoSize = true;
            this.Table_Label.Font = new System.Drawing.Font("Calibri", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Table_Label.Location = new System.Drawing.Point(8, 234);
            this.Table_Label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Table_Label.Name = "Table_Label";
            this.Table_Label.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Table_Label.Size = new System.Drawing.Size(46, 18);
            this.Table_Label.TabIndex = 12;
            this.Table_Label.Text = "Ready";
            this.Table_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.configToolStripMenuItem.Text = "Config";
            this.configToolStripMenuItem.Click += new System.EventHandler(this.configToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 285);
            this.Controls.Add(this.ToolStrip1);
            this.Controls.Add(this.mnu1);
            this.Controls.Add(this.TabMain);
            this.Controls.Add(this.Table_Label);
            this.Name = "FrmMain";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.dgvMenu.ResumeLayout(false);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.mnu1.ResumeLayout(false);
            this.mnu1.PerformLayout();
            this.TabMain.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        public System.Windows.Forms.ToolStripMenuItem midgvPaste;
        public System.Windows.Forms.ToolStripMenuItem midgvCopy;
        public System.Windows.Forms.ContextMenuStrip dgvMenu;
        public System.Windows.Forms.ToolStripMenuItem midgvUndo;
        public System.Windows.Forms.ToolStripMenuItem midgvCopyHdrs;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        public System.Windows.Forms.ToolStripMenuItem miAbout;
        public System.Windows.Forms.ToolStripMenuItem miDocument;
        public System.Windows.Forms.ToolStripMenuItem ActivateToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem DeactivateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        public System.Windows.Forms.ToolStripMenuItem miRegionalize;
        public System.Windows.Forms.ToolStripMenuItem ImpactsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem miImpactInd1;
        public System.Windows.Forms.ToolStripMenuItem miImpactInd2;
        public System.Windows.Forms.ToolStripMenuItem miImpactComm1;
        public System.Windows.Forms.ToolStripMenuItem miImpactComm2;
        public System.Windows.Forms.ToolStrip ToolStrip1;
        public System.Windows.Forms.ToolStripButton ToolStripButton2;
        public System.Windows.Forms.ToolStripButton miSave;
        public System.Windows.Forms.ToolStripButton miPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        public System.Windows.Forms.ToolStripButton miCopy;
        public System.Windows.Forms.ToolStripButton miPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        public System.Windows.Forms.ToolStripButton miDecDecimal;
        public System.Windows.Forms.ToolStripButton miIncDecimal;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        public System.Windows.Forms.ToolStripButton ToolStripButton4;
        public System.Windows.Forms.ToolStripButton ToolStripButton1;
        public System.Windows.Forms.ToolStripButton RegButton;
        public System.Windows.Forms.ToolStripButton TSB4;
        public System.Windows.Forms.ToolStripComboBox YearSelect_CBoxTS;
        public System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        public System.Windows.Forms.ToolStripMenuItem ViewToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem ViewIOAccountsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem miGRP;
        public System.Windows.Forms.ToolStripMenuItem RequirementsTablesToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem MultipliersToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem SectorDistributionsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem IndustryAccountsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem CommodityAccountsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem IndustryLaborToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem CommodityTradeDistributionsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem EditToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem miAggregate;
        public System.Windows.Forms.ToolStripMenuItem miNewAgg;
        public System.Windows.Forms.ToolStripMenuItem miLoadAgg;
        public System.Windows.Forms.ToolStripSeparator _ToolStripSeparator3;
        public System.Windows.Forms.ToolStripMenuItem ModifyDataToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem UseEditMenuItem;
        public System.Windows.Forms.ToolStripMenuItem MakeToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem FinalDemandToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem StateToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem miWorkspaceSummary;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem miLoadData;
        public System.Windows.Forms.ToolStripMenuItem miSave1;
        public System.Windows.Forms.ToolStripMenuItem miPrint1;
        public System.Windows.Forms.ToolStripMenuItem PreferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        public System.Windows.Forms.ToolStripMenuItem IncreateDecimalsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem DecreaseDecimalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        public System.Windows.Forms.ToolStripMenuItem CopyWHeadersToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem UseAbbrevHeadersToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem OptionsTool;
        public System.Windows.Forms.ToolStripMenuItem ProgramResetToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem miExit;
        public System.Windows.Forms.MenuStrip mnu1;
        public System.Windows.Forms.ToolStripMenuItem miOptions;
        public System.Windows.Forms.ToolStripMenuItem miCopy1;
        public System.Windows.Forms.ToolStripMenuItem miPaste1;
        public System.Windows.Forms.TabControl TabMain;
        public System.Windows.Forms.Label Table_Label;
        public System.Windows.Forms.ToolStripMenuItem RawDataToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem miStateData;
        public System.Windows.Forms.ToolStripMenuItem miCRateMenuItem;
        public System.Windows.Forms.ToolStripMenuItem miEmployment;
        public System.Windows.Forms.ToolStripMenuItem miFTERatio;
        public System.Windows.Forms.ToolStripMenuItem miGRPMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmOtherOptions;
        public System.Windows.Forms.TabPage TabPage1;
        private System.Windows.Forms.DataGridView dgvMain;
        private System.Windows.Forms.ToolStripMenuItem showChartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
    }
}

