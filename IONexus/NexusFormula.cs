﻿using Matrix_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IONexus
{
    [Serializable]
    public class NexusFormula : ICloneable
    {
        //public string[] interColumns;
        //public string[] findemColumns;
        //public string[] primaryFactors;

        public FormulaMatrix DosmeticFinDemTable { get; set; }
        public FormulaMatrix DosmeticInterDemTable { get; set; }
        public FormulaMatrix ImportFinDemTable { get; set; }
        public FormulaMatrix ImportInterDemTable { get; set; }
        public FormulaMatrix PrimaryInterDemTable { get; set; }
        public FormulaMatrix PrimaryFinDemTable { get; set; }
        public FormulaMatrix AddedValueTable { get; set; }

        public string[,] Print()
        {
            string[,] values = new string[DosmeticInterDemTable.NoRows + ImportInterDemTable.NoRows + PrimaryInterDemTable.NoRows, DosmeticInterDemTable.NoCols + DosmeticFinDemTable.NoCols];

            for (int r = 0; r < DosmeticInterDemTable.NoRows; r++)
            {
                for (int c = 0; c < DosmeticInterDemTable.NoCols; c++)
                {
                    values[r, c] = DosmeticInterDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < DosmeticFinDemTable.NoRows; r++)
            {
                for (int c = 0; c < DosmeticFinDemTable.NoCols; c++)
                {
                    values[ r,  DosmeticInterDemTable.NoCols + c] = DosmeticFinDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < ImportInterDemTable.NoRows; r++)
            {
                for (int c = 0; c < ImportInterDemTable.NoCols; c++)
                {
                    values[ DosmeticInterDemTable.NoRows + r,  c] = ImportInterDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < ImportFinDemTable.NoRows; r++)
            {
                for (int c = 0; c < ImportFinDemTable.NoCols; c++)
                {
                    values[ DosmeticInterDemTable.NoRows + r, ImportInterDemTable.NoCols + c] = ImportFinDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < PrimaryInterDemTable.NoRows; r++)
            {
                for (int c = 0; c < PrimaryInterDemTable.NoCols; c++)
                {
                    values[ DosmeticInterDemTable.NoRows + ImportInterDemTable.NoRows + r, c] = PrimaryInterDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < PrimaryFinDemTable.NoRows; r++)
            {
                for (int c = 0; c < PrimaryFinDemTable.NoCols; c++)
                {
                    values[ DosmeticInterDemTable.NoRows + ImportInterDemTable.NoRows + r, ImportInterDemTable.NoCols + c] = PrimaryFinDemTable[r, c].ToString();
                }
            }
            return values;
        }
        public static NexusFormula Parse(string [,]values, NexusData parent)
        {
            NexusFormula data = new NexusFormula();
            int maxDomestic = parent.DosmeticInterDemTable.NoRows, 
                maxImport = parent.DosmeticInterDemTable.NoRows + parent.ImportInterDemTable.NoRows, 
                maxPrimary = parent.DosmeticInterDemTable.NoRows + parent.ImportInterDemTable.NoRows + parent.PrimaryInterDemTable.NoRows,
                maxInter = parent.DosmeticInterDemTable.NoCols, 
                maxDemand = parent.DosmeticInterDemTable.NoCols + parent.DosmeticFinDemTable.NoCols;
            data.ImportInterDemTable = new FormulaMatrix(parent.ImportInterDemTable.NoRows, parent.ImportInterDemTable.NoCols);
            data.ImportFinDemTable = new FormulaMatrix(parent.ImportFinDemTable.NoRows, parent.ImportFinDemTable.NoCols);
            data.DosmeticInterDemTable = new FormulaMatrix(parent.DosmeticInterDemTable.NoRows, parent.DosmeticInterDemTable.NoCols);
            data.DosmeticFinDemTable = new FormulaMatrix(parent.DosmeticFinDemTable.NoRows, parent.DosmeticFinDemTable.NoCols);
            data.PrimaryInterDemTable = new FormulaMatrix(parent.PrimaryInterDemTable.NoRows, parent.PrimaryInterDemTable.NoCols);
            data.PrimaryFinDemTable = new FormulaMatrix(parent.PrimaryFinDemTable.NoRows, parent.PrimaryFinDemTable.NoCols);
            //Parse Data
            for (int r = 0; r < maxDomestic; r++)
            {
                for (int c = 0; c < maxInter; c++)
                {
                    try
                    {
                        data.DosmeticInterDemTable[r, c] = values[r, c];
                    }
                    catch { }
                }
            }
            for (int r = 0; r < maxDomestic; r++)
            {
                for (int c = maxInter; c < maxDemand; c++)
                {
                    try
                    {
                        data.DosmeticFinDemTable[r, c - maxInter] = values[r, c];
                    }
                    catch { }
                }
            }
            for (int r = maxDomestic; r < maxImport; r++)
            {
                for (int c = 0; c < maxInter; c++)
                {
                    try
                    {
                        data.ImportInterDemTable[r - maxDomestic, c] = values[r, c];
                    }
                    catch { }
                }
            }
            for (int r = maxDomestic; r < maxImport; r++)
            {
                for (int c = maxInter; c < maxDemand; c++)
                {
                    try
                    {
                        data.ImportFinDemTable[r - maxDomestic, c - maxInter] = values[r, c];
                    }
                    catch { }
                }
            }
            for (int r = maxImport; r < maxPrimary; r++)
            {
                for (int c = 0; c < maxInter; c++)
                {
                    try
                    {
                        data.PrimaryInterDemTable[r - maxImport, c] = values[r, c];
                    }
                    catch { }
                }
            }
            for (int r = maxImport; r < maxPrimary; r++)
            {
                for (int c = maxInter; c < maxDemand; c++)
                {
                    try
                    {
                        data.PrimaryFinDemTable[r - maxImport, c - maxInter] = values[r, c];
                    }
                    catch { }
                }
            }
            return data;
        }
        public NexusFormula()
        {
        }
        public object Clone()
        {
            NexusFormula data = new NexusFormula();
            data.DosmeticFinDemTable = this.DosmeticFinDemTable.Clone();
            data.DosmeticInterDemTable = this.DosmeticInterDemTable.Clone();
            data.ImportFinDemTable = this.ImportFinDemTable.Clone();
            data.ImportInterDemTable = this.ImportInterDemTable.Clone();
            data.PrimaryInterDemTable = this.PrimaryInterDemTable.Clone();
            data.PrimaryFinDemTable = this.PrimaryFinDemTable.Clone();
            data.AddedValueTable = this.AddedValueTable.Clone();
            //data.interColumns = (string[])this.interColumns.Clone();
            //data.findemColumns = (string[])this.findemColumns.Clone();
            //data.primaryFactors = (string[])this.primaryFactors.Clone();
            return data;
        }
    }
}
