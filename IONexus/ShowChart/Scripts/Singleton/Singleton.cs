﻿namespace ShowChart
{
    class Singleton<T> where T : new()
    {
        private static T _intance;
        public static T Intance
        {
            get
            {
                if (_intance == null)
                {
                    _intance = new T();
                }

                return _intance;
            }
        }
    }
}