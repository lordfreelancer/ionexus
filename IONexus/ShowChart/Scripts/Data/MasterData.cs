﻿namespace ShowChart
{
    class MasterData
    {
        public string Name;
        public double Value;
        public string Label;

        public MasterData(string name, double value, string label = "")
        {
            Name = name;
            Value = value;
            Label = label;
        }
    }
}