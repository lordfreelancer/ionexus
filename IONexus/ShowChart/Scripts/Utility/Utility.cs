﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Reflection;
using System.ComponentModel;

namespace ShowChart
{
    class Utility
    {
        public static void LoadDataForComboxWithEnum(Type type, ComboBox comboBox, int indexDefault = -1)
        {
            comboBox.Items.Clear();

            Array arrEnum = Enum.GetValues(type);

            for (int i = 0; i < arrEnum.Length; i++)
            {
                comboBox.Items.Add(arrEnum.GetValue(i));
            }

            if (arrEnum.Length > 0 && indexDefault != -1)
            {
                comboBox.SelectedItem = arrEnum.GetValue(indexDefault);
            }
        }

        public static void LoadChart(SeriesConfig config, Chart chart)
        {
            if (chart.Series.FindByName(config.SeriesName) != null)
            {
                return;
            }

            chart.Series.Add(config.SeriesName);
            chart.Series[config.SeriesName].ChartType = config.ChartType;
            chart.Series[config.SeriesName].Color = config.SeriesColor;
            chart.Series[config.SeriesName].YAxisType = config.AxisType;
            chart.Series[config.SeriesName]["StackedGroupName"] = config.StackedGroup;
            chart.Series[config.SeriesName].BorderWidth = config.BorderWidth;

            if (config.DashStyle != ChartDashStyle.NotSet)
            {
                chart.Series[config.SeriesName].BorderDashStyle = config.DashStyle;
            }

            if (config.ChartType == SeriesChartType.Radar)
            {
                chart.Series[config.SeriesName]["AreaDrawingStyle"] = "Polygon";
            }

            LoadDataForChart(config, chart);
        }

        //Load Data For Chart
        public static void LoadDataForChart(SeriesConfig config, Chart chart)
        {
            List<MasterData> list = GetData(config.ChartData);

            for (int i = 0; i < list.Count; i++)
            {
                chart.Series[config.SeriesName].Points.AddXY(list[i].Name, list[i].Value);
                chart.Series[config.SeriesName].Points[i].Label = config.ShowPercentage ? list[i].Label : list[i].Value.ToString();
            }
        }

        public static List<MasterData> GetData(KeyData keyData)
        {
            return DataManager.Intance.GetData(keyData);
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes[0].Description;
        }
    }

    public static class ControlExtensions
    {
        public static T Clone<T>(this T controlToClone, bool visible = true) where T : Control
        {
            PropertyInfo[] controlProperties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            T instance = Activator.CreateInstance<T>();

            if (controlProperties != null && controlProperties.Length > 0)
            {
                foreach (PropertyInfo propInfo in controlProperties)
                {
                    if (propInfo.CanWrite)
                    {
                        if (propInfo.Name != "WindowTarget")
                        {
                            propInfo.SetValue(instance, propInfo.GetValue(controlToClone, null), null);
                        }
                    }
                }
            }

            instance.Visible = visible;

            return instance;
        }
    }
}
