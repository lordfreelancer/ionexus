﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ShowChart.Scripts.IO
{
    class IOManager : Singleton<IOManager>
    {
        private Dictionary<string, Dictionary<int, object>> dictData = new Dictionary<string, Dictionary<int, object>>();

        public List<IOConfig> listIOConfig;
        public List<IOElementConfig> listIOElementConfig;

        public IOManager()
        {
            CheckConfigPath();
        }

        private void CheckConfigPath()
        {
            if (!File.Exists(Define.IOConfigPath))
            {
                if (!Directory.Exists(Define.IOConfigFolderPath))
                {
                    Directory.CreateDirectory(Define.IOConfigFolderPath);
                }

                File.CreateText(Define.IOConfigPath);
            }

            if (!File.Exists(Define.IOElementConfigPath))
            {
                if (!Directory.Exists(Define.IOElementConfigFolderPath))
                {
                    Directory.CreateDirectory(Define.IOElementConfigFolderPath);
                }

                File.CreateText(Define.IOElementConfigPath);
            }

            if (!Directory.Exists(Define.IOFileContentFolderPath))
            {
                Directory.CreateDirectory(Define.IOFileContentFolderPath);
            }

            LoadDataFromDataBase<IOConfig>(Define.IOConfigPath);
            LoadDataFromDataBase<IOElementConfig>(Define.IOElementConfigPath);

            listIOConfig = GetDataAll<IOConfig>();
            listIOElementConfig = GetDataAll<IOElementConfig>();
        }

        private void LoadDataFromDataBase<T>(string path) where T : IOData, new()
        {
            string dataText = "";
            string tableName = typeof(T).Name;

            dataText = File.ReadAllText(path);

            if (string.IsNullOrEmpty(dataText))
            {
                return;
            }

            string json = "{\"list\":[" + dataText + "]}";

            List<object> list = JsonParser.GetListDict((Dictionary<string, object>)MiniJson.Json.Deserialize(json), "list");

            Dictionary<int, object> dict = new Dictionary<int, object>();

            for (int i = 0; i < list.Count; i++)
            {
                T data = new T();
                data.Deserialize((Dictionary<string, object>)list[i]);
                dict.Add(data.ORDER_NUMBER, list[i]);
            }

            dictData.Add(tableName, dict);
        }

        public List<T> GetDataAll<T>() where T : IOData, new()
        {
            List<T> result = new List<T>();

            foreach (var item in dictData[typeof(T).Name])
            {
                T data = new T();
                data.Deserialize((Dictionary<string, object>)item.Value);
                result.Add(data);
            }

            result = result.OrderBy(io => io.ORDER_NUMBER).ToList();

            return result;
        }

        public T GetData<T>(int id) where T : IOData, new()
        {
            T result = new T();

            result.Deserialize((Dictionary<string, object>)dictData[typeof(T).Name][id]);

            return result;
        }

        public string Serialize<T>(List<T> list) where T : IOData
        {
            List<string> listString = new List<string>();

            for (int i = 0; i < list.Count; i++)
            {
                string serialize = list[i].Serialize();

                listString.Add(serialize);
            }

            return MiniJson.Json.Serialize(listString);
        }
    }
}