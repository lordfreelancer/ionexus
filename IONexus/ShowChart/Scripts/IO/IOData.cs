﻿using System.Collections.Generic;

namespace ShowChart.Scripts.IO
{
    public class IOData : JsonModel
    {
        public virtual int ORDER_NUMBER { get; set; }

        public override void Deserialize(Dictionary<string, object> dict)
        {
            ORDER_NUMBER = JsonParser.GetInt(dict, "ORDER_NUMBER", -1);
        }
    }

    public class IOConfig : IOData
    {
        public override int ORDER_NUMBER { get; set; }
        public string IO_NUMBER { get; set; }
        public string IO_NAME { get; set; }
        public string PATH_FILE { get; set; }

        public override void Deserialize(Dictionary<string, object> dict)
        {
            base.Deserialize(dict);

            IO_NUMBER = JsonParser.GetString(dict, "IO_NUMBER", "");
            IO_NAME = JsonParser.GetString(dict, "IO_NAME", "");
            PATH_FILE = JsonParser.GetString(dict, "PATH_FILE", "");
        }
    }

    class IOElementConfig : IOData
    {
        public override int ORDER_NUMBER { get; set; }
        public string IOE_NUMBER { get; set; }
        public string IOE_NAME { get; set; }
        public string PATH_FILE { get; set; }

        public override void Deserialize(Dictionary<string, object> dict)
        {
            base.Deserialize(dict);

            IOE_NUMBER = JsonParser.GetString(dict, "IOE_NUMBER", "");
            IOE_NAME = JsonParser.GetString(dict, "IOE_NAME", "");
            PATH_FILE = JsonParser.GetString(dict, "PATH_FILE", "");
        }
    }
}