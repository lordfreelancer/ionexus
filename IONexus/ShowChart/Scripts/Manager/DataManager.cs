﻿using System.Collections.Generic;

namespace ShowChart
{
    class DataManager : Singleton<DataManager>
    {
        public List<MasterData> listData = new List<MasterData>
        {
            new MasterData("Max", 33, "30%"),
            new MasterData("Jimmy", -45, "15%"),
            new MasterData("Katarina", 20, "30%"),
            new MasterData("Lissanra", 10, "57%"),
            new MasterData("Bambo", 15,"60%"),
            new MasterData("Gara", -10, "80%"),
            new MasterData("Ezreal", 55, "45%"),
            new MasterData("Lux", 10, "30%"),
            new MasterData("Lucian", 100, "90%"),
        };

        public List<MasterData> listDataPig = new List<MasterData>
        {
            new MasterData("Jan", 29, "15%"),
            new MasterData("Feb", 30, "25%"),
            new MasterData("Mar", 56,  "50%"),
            new MasterData("Apr", 40, "70%"),
        };

        public List<MasterData> listDataCow = new List<MasterData>
        {
            new MasterData("Jan", 40, "85%"),
            new MasterData("Feb", 52, "75%"),
            new MasterData("Mar", 20,  "50%"),
            new MasterData("Apr", 30, "30%"),
        };

        public List<MasterData> listDataChicken = new List<MasterData>
        {
            new MasterData("Jan", 23, "35%"),
            new MasterData("Feb", 45, "55%"),
            new MasterData("Mar", 56,  "76%"),
            new MasterData("Apr", 45, "78%"),
        };

        public List<MasterData> listDataPigLine = new List<MasterData>
        {
            new MasterData("Jan", 8, "8%"),
            new MasterData("Feb", 7, "7%"),
            new MasterData("Mar", 8,  "8%"),
            new MasterData("Apr", 10, "10%"),
        };

        public List<MasterData> listDataCowLine = new List<MasterData>
        {
            new MasterData("Jan", 9, "8%"),
            new MasterData("Feb", 8, "7%"),
            new MasterData("Mar", 9,  "8%"),
            new MasterData("Apr", 11, "10%"),
        };

        public List<MasterData> listDataPig3 = new List<MasterData>
        {
            new MasterData("Jan", 29, "15%"),
            new MasterData("Feb", 30, "25%"),
            new MasterData("Mar", 56,  "50%"),
        };

        public List<MasterData> listDataCow3 = new List<MasterData>
        {
            new MasterData("Feb", 52, "75%"),
            new MasterData("Mar", 20,  "50%"),
            new MasterData("Apr", 30, "30%"),
        };

        public List<MasterData> listDataPig6 = new List<MasterData>
        {
            new MasterData("Jan", 29, "15%"),
            new MasterData("Feb", 30, "25%"),
            new MasterData("Mar", 56,  "50%"),
            new MasterData("Apr", 40, "70%"),
            new MasterData("May", 23, "35%"),
            new MasterData("Jun", 45, "55%"),
        };

        public List<MasterData> listDataCow6 = new List<MasterData>
        {
            new MasterData("Jan", 40, "85%"),
            new MasterData("Feb", 52, "75%"),
            new MasterData("Mar", 20,  "50%"),
            new MasterData("Apr", 30, "30%"),
            new MasterData("May", 56,  "76%"),
            new MasterData("Jun", 45, "78%"),
        };

        public List<MasterData> GetData(KeyData keyData)
        {
            List<MasterData> result = null;

            switch (keyData)
            {
                case KeyData.Norman: result = listData; break;
                case KeyData.Pig: result = listDataPig; break;
                case KeyData.Pig3: result = listDataPig3; break;
                case KeyData.Pig6: result = listDataPig6; break;
                case KeyData.PigLine: result = listDataPigLine; break;
                case KeyData.Cow: result = listDataCow; break;
                case KeyData.Cow3: result = listDataCow3; break;
                case KeyData.Cow6: result = listDataCow6; break;
                case KeyData.CowLine: result = listDataCowLine; break;
                case KeyData.Chicken: result = listDataChicken; break;
            }

            return result;
        }
    }
}