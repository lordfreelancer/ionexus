﻿using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace ShowChart
{
    class Define
    {
        public const string IOConfigPath = IOConfigFolderPath + @"\IOConfig.json";
        public const string IOElementConfigPath = IOElementConfigFolderPath + @"\IOElementConfig.json";

        public const string IOConfigFolderPath = "IOConfig";
        public const string IOElementConfigFolderPath = "IOElementConfig";
        public const string IOFileContentFolderPath = "IOFileContent";
    }

    public enum ChartMode
    {
        Single,
        Multi
    }

    public enum SingleChart
    {
        Point = 0,
        Line = 3,
        Column = 10,
        Pie = 17,
        Radar = 25,
    }

    public enum MultiChart
    {
        Column_Line,
        Three_StackedColumn_Line,
        Line_Point,
        Column_Line_Point,
        Three_Column_In_One,
        Three_Line,
        Radar_3,
        Radar_6,
    }

    //Hard code for Demo
    public enum KeyData
    {
        Norman = 0,
        Pig = 1,
        Cow = 2,
        Chicken = 3,
        PigLine = 4,
        CowLine = 5,
        Pig3 = 6,
        Pig6 = 7,
        Cow3 = 8,
        Cow6 = 9,
    }

    public class SeriesConfig
    {
        public SeriesChartType ChartType;
        public string SeriesName;
        public Color SeriesColor;
        public bool ShowPercentage;
        public ChartDashStyle DashStyle;
        public string StackedGroup;
        public int BorderWidth;
        public KeyData ChartData;
        public AxisType AxisType;

        public SeriesConfig()
        {
            ChartType = SeriesChartType.Column;
            SeriesName = ChartType.ToString();
            SeriesColor = Color.Green;
            ShowPercentage = true;
            DashStyle = ChartDashStyle.NotSet;
            AxisType = AxisType.Primary;
            ChartData = 0;
            BorderWidth = 1;
        }
    }
}