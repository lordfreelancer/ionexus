using System;
using System.Collections.Generic;
using System.Text;

public class JsonParser
{
    public static List<string> GetListString(IDictionary<string, object> dict, string key, List<string> def = null)
    {
        List<string> listString = new List<string>();
        List<object> list = (List<object>)dict[key];

        if (list != null)
        {
            foreach (object obj in list)
            {
                listString.Add(obj.ToString());
            }
        }

        return listString;
    }

    public static List<T> GetListEnum<T>(IDictionary<string, object> dict, string key, List<T> def = null)
    {
        List<T> result = new List<T>();

        if (dict.ContainsKey(key))
        {
            List<object> list = (List<object>)dict[key];

            if (list != null)
            {
                foreach (object obj in list)
                {
                    if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                    {
                        continue;
                    }

                    T item = (T)Enum.Parse(typeof(T), obj.ToString(), true);

                    result.Add(item);
                }
            }
        }

        return result;
    }

    public static Dictionary<string, object> GetDict(IDictionary<string, object> dict, string key, Dictionary<string, object> def = null)
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return (Dictionary<string, object>)dict[key];
    }

    public static Dictionary<string, Dictionary<string, int>> GetDictDict(IDictionary<string, object> dict)
    {
        Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();

        foreach (KeyValuePair<string, object> ent in dict)
        {
            Dictionary<string, int> levelDict = new Dictionary<string, int>();

            foreach (KeyValuePair<string, object> entry in (Dictionary<string, object>)ent.Value)
            {
                levelDict.Add(entry.Key, int.Parse(entry.Value.ToString()));
            }

            result.Add(ent.Key, levelDict);
        }

        return result;
    }

    public static string GetString(IDictionary<string, object> dict, string key, string def = "")
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return dict[key].ToString();
    }

    public static bool GetBool(IDictionary<string, object> dict, string key, bool def = false)
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return (bool)dict[key];
    }

    public static byte GetByte(IDictionary<string, object> dict, string key, byte def = 0)
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return byte.Parse(dict[key].ToString());
    }

    public static ushort GetUshort(IDictionary<string, object> dict, string key, ushort def = 0)
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return ushort.Parse(dict[key].ToString());
    }

    public static int GetInt(IDictionary<string, object> dict, string key, int def = 0)
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return int.Parse(dict[key].ToString());
    }

    public static long GetLong(IDictionary<string, object> dict, string key, long def = 0)
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return long.Parse(dict[key].ToString());
    }

    public static float GetFloat(IDictionary<string, object> dict, string key, float def = 0.0f)
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return float.Parse(dict[key].ToString());
    }

    public static double GetDouble(IDictionary<string, object> dict, string key, double def = 0.0f)
    {
        if (!dict.ContainsKey(key))
        {
            return def;
        }

        return double.Parse(dict[key].ToString());
    }

    public static List<int> GetListInt(IDictionary<string, object> dict, string key, List<int> def = null)
    {
        if (dict.ContainsKey(key))
        {
            List<object> list = (List<object>)dict[key];
            List<int> listInt = new List<int>();

            foreach (object obj in list)
            {
                listInt.Add(int.Parse(obj.ToString()));
            }

            return listInt;
        }

        return def;
    }

    public static List<long> GetListLong(IDictionary<string, object> dict, string key, List<long> def = null)
    {
        if (dict.ContainsKey(key))
        {
            List<object> list = (List<object>)dict[key];
            List<long> listlong = new List<long>();

            foreach (object obj in list)
            {
                listlong.Add(long.Parse(obj.ToString()));
            }

            return listlong;
        }

        return def;
    }

    public static List<float> GetListFloat(IDictionary<string, object> dict, string key, List<float> def = null)
    {
        if (dict.ContainsKey(key))
        {
            List<object> list = (List<object>)dict[key];
            List<float> listfloat = new List<float>();

            foreach (object obj in list)
            {
                listfloat.Add(float.Parse(obj.ToString()));
            }

            return listfloat;
        }

        return def;
    }

    public static List<object> GetListDict(IDictionary<string, object> dict, string key, List<object> def = null)
    {
        if (dict.ContainsKey(key))
        {
            List<object> list = (List<object>)dict[key];
            List<object> listDict = new List<object>();

            foreach (object obj in list)
            {
                listDict.Add(obj);
            }

            return listDict;
        }

        return def;
    }

    public static T Get<T>(IDictionary<string, object> _dict, string _key) where T : JsonModel, new()
    {
        T t = new T();
        Dictionary<string, object> o = GetDict(_dict, _key);
        t.Deserialize(o);

        return t;
    }

    public static List<T> GetList<T>(IDictionary<string, object> dict, string key) where T : JsonModel, new()
    {
        List<object> listObject = GetListDict(dict, key);
        List<T> result = new List<T>();

        foreach (var item in listObject)
        {
            T t = new T();
            t.Deserialize((Dictionary<string, object>)item);
            result.Add(t);
        }

        return result;
    }

    public static T GetEnum<T>(IDictionary<string, object> dict, string key, T def)
    {
        string value = GetString(dict, key, def.ToString());

        return ParseEnum<T>(value);
    }

    public static List<IDictionary<string, object>> ConvertListToListDict<T>(List<T> list) where T : JsonModel, new()
    {
        List<IDictionary<string, object>> result = new List<IDictionary<string, object>>();

        foreach (T item in list)
        {
            result.Add(item.ToDict());
        }

        return result;
    }

    public static bool IsValidInTime(long startDate, long endDate, long date)
    {
        if (date < startDate) return false;
        if (date > endDate) return false;

        return true;
    }

    public static Dictionary<long, int> GetLongDict(Dictionary<string, object> dict, string key)
    {
        dict = GetDict(dict, key);

        Dictionary<long, int> result = new Dictionary<long, int>();

        if (dict != null)
        {
            foreach (KeyValuePair<string, object> keyVal in dict)
            {
                result.Add(long.Parse(keyVal.Key), int.Parse(keyVal.Value.ToString()));
            }

        }

        return result;
    }

    public static T ParseEnum<T>(string value)
    {
        return (T)Enum.Parse(typeof(T), value, true);
    }

    public static T ParseEnum<T>(string value, T def)
    {
        return (T)Enum.Parse(typeof(T), value, true);
    }

    static private void AddTime(Dictionary<string, object> dict)
    {
        var str = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
        dict.Add("time", str);
    }

    /**
     * Chuyen time seconds thanh dang "hh:mm.ss"
     **/
    public static string SecondsToSting(float time)
    {
        TimeSpan t = TimeSpan.FromSeconds(time);
        string timerFormatted = string.Format("{0:D2}:{1:D2}.{2:D2}", t.Hours, t.Minutes, t.Seconds);
        return timerFormatted;
    }

    public static string SecondsGameToString(float time)
    {
        TimeSpan t = TimeSpan.FromSeconds(time);
        string timerFormatted = "";
        string format = "{0:D2}:{1:D2}.{2:D2}";

        if (time < 0)
        {
            return "--:--.--";
        }

        if (t.Hours > 0)
        {
            timerFormatted = string.Format(format, 59, 59, 99);
        }
        else
        {
            timerFormatted = string.Format(format, t.Minutes, t.Seconds, t.Milliseconds / 10);
        }

        return timerFormatted;
    }

    public static string ConvertToUTF8String(string str)
    {
        UTF8Encoding utf8 = new UTF8Encoding();
        byte[] encodedBytes = utf8.GetBytes(str);
        string decodedString = utf8.GetString(encodedBytes);

        return decodedString;
    }

    //-------------------Ticks --- Timstamp -----------------------

    /**Date hien tai tren local. javaTimestamp: milis */
    public static DateTime DateTimeFromJavaTimestamp(long javaTimestamp)
    {
        long tick1970 = (new DateTime(1970, 1, 1)).Ticks;
        long timeStampInTicks = javaTimestamp * TimeSpan.TicksPerMillisecond;
        DateTime date = new DateTime(tick1970 + timeStampInTicks).ToLocalTime();

        return date;
    }

    /**Tra ve timestamp (milis), date: date hien tai tren local */
    public static long JavaTimestampFromDateTime(DateTime date)
    {
        long timestampInTicks = date.Ticks - (new DateTime(1970, 1, 1)).ToLocalTime().Ticks;

        return timestampInTicks / TimeSpan.TicksPerMillisecond;
    }

    static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

        return bytes;
    }

    static string GetString(byte[] bytes)
    {
        char[] chars = new char[bytes.Length / sizeof(char)];
        Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);

        return new string(chars);
    }

    public string SerializeListObject<T>(List<T> list) where T : JsonModel, new()
    {
        string json = "[";

        for (int i = 0; i < list.Count; i++)
        {
            json += list[i].Serialize();
            if (i < list.Count - 1)
            {
                json += ",";
            }
        }

        json += "]";

        return json;
    }

    public static string FormatSerializeList(string json)
    {
        json = json.Replace("\\\"", "\"");
        json = json.Replace("\"[", "[");
        json = json.Replace("]\"", "]");

        return json;
    }

    public static string SerializeList<T>(List<T> lst) where T : JsonModel, new()
    {
        StringBuilder builder = new StringBuilder();

        foreach (var c in lst)
        {
            builder.Append(c.Serialize()).Append("|"); // Append string to StringBuilder
        }

        return builder.ToString();
    }

    public static List<T> DeserializeList<T>(string json) where T : JsonModel, new()
    {
        List<T> rs = new List<T>();
        string[] parts = json.Split('|');

        foreach (var s in parts)
        {
            if (!string.IsNullOrEmpty(s))
            {
                var c = new T();
                c.Deserialize(s);
                rs.Add(c);
            }
        }

        return rs;
    }

    public static string Base10to32(int number)
    {
        string hexnumbers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
        string hex = "";
        int remainder;

        do
        {
            remainder = number % 32;
            number = number / 32;
            hex = hexnumbers[remainder] + hex;
        }
        while (number > 0);

        return hex;
    }

    public static string ExtractNumber(string s)
    {
        string result = string.Empty;

        for (int i = 0; i < s.Length; i++)
        {
            if (char.IsDigit(s[i]))
            {
                result += s[i];
            }
        }

        return result;
    }

    public static string UnescapeJsonString(string value)
    {
        const char BACK_SLASH = '\\';
        const char SLASH = '/';
        const char DBL_QUOTE = '"';
        StringBuilder output = new StringBuilder(value.Length);

        for (int i = 0; i < value.Length; i++)
        {
            char c = value[i];

            if (c == BACK_SLASH)
            {

                var d = value[i + 1];
                switch (d)
                {
                    case SLASH:
                        output.AppendFormat("{0}", SLASH);
                        i++;
                        break;
                    case BACK_SLASH:
                        output.AppendFormat("{0}", BACK_SLASH);
                        i++;
                        break;
                    case DBL_QUOTE:
                        output.AppendFormat("{0}", DBL_QUOTE);
                        i++;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                output.Append(c);
            }
        }

        return output.ToString();
    }

    public static string EscapeJsonString(string value)
    {
        const char BACK_SLASH = '\\';
        const char SLASH = '/';
        const char DBL_QUOTE = '"';
        StringBuilder output = new StringBuilder(value.Length);

        foreach (char c in value)
        {
            switch (c)
            {
                case SLASH:
                    output.AppendFormat("{0}{1}", BACK_SLASH, SLASH);
                    break;

                case BACK_SLASH:
                    output.AppendFormat("{0}{0}", BACK_SLASH);
                    break;

                case DBL_QUOTE:
                    output.AppendFormat("{0}{1}", BACK_SLASH, DBL_QUOTE);
                    break;

                default:
                    output.Append(c);
                    break;
            }
        }

        return output.ToString();
    }
}