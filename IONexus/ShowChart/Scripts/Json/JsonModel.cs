﻿using System.Collections.Generic;
using System.Reflection;

public class JsonModel
{
    public string Serialize()
    {
        return MiniJson.Json.Serialize(ToDict());
    }

    public void Deserialize(string json)
    {
        Deserialize(MiniJson.Json.Deserialize(json) as Dictionary<string, object>);
    }

    public virtual void Deserialize(Dictionary<string, object> dict)
    {

    }

    public Dictionary<string, object> ToDict()
    {
        Dictionary<string, object> dict = new Dictionary<string, object>();

        PropertyInfo[] arrPro = GetType().GetProperties();

        for (int i = 0; i < arrPro.Length; i++)
        {
            dict.Add(arrPro[i].Name, arrPro[i].GetValue(this));
        }

        return dict;
    }
}