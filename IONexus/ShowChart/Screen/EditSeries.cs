﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ShowChart.Screen
{
    public partial class EditSeries : Form
    {
        private List<Panel> listPanelSeriesConfig = new List<Panel>();
        private List<SeriesConfigView> listSeriesConfigView = new List<SeriesConfigView>();
        private List<SeriesConfig> listSeriesConfig = new List<SeriesConfig>();

        private Action<List<SeriesConfig>> OnOK;

        private ChartMode currentMode;

        public EditSeries(List<SeriesConfig> listSeriesConfig, Action<List<SeriesConfig>> onOK, ChartMode currentMode)
        {
            InitializeComponent();

            this.currentMode = currentMode;

            buttonAddChart.Visible = this.currentMode != ChartMode.Single;

            if (listSeriesConfig == null || listSeriesConfig.Count == 0)
            {
                listSeriesConfig = new List<SeriesConfig>
                {
                    new SeriesConfig()
                };
            }

            this.listSeriesConfig = listSeriesConfig;
            OnOK = onOK;

            LoadData();
        }

        public void LoadData()
        {
            flowLayoutPanel1.Controls.Clear();
            listPanelSeriesConfig.Clear();
            listSeriesConfigView.Clear();

            for (int i = 0; i < listSeriesConfig.Count; i++)
            {
                AddNewPanelSeriesConfig(i);
            }
        }

        private void CheckShowSettingLine(int index, SeriesChartType chartType)
        {
            listSeriesConfigView[index].ComboBoxDashStyle.Enabled = chartType == SeriesChartType.Line || chartType == SeriesChartType.Radar;
            listSeriesConfigView[index].ComboBoxBorderWidth.Enabled = chartType == SeriesChartType.Line || chartType == SeriesChartType.Radar;
        }

        private void OnClickChangeColor(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            int indexOfButton = int.Parse(button.Parent.Name.Split('_')[1]);

            colorDialog.Color = listSeriesConfigView[indexOfButton].TextBoxColor.BackColor;
            colorDialog.ShowDialog();
            listSeriesConfigView[indexOfButton].TextBoxColor.BackColor = colorDialog.Color;
        }

        private void OnClickRemove(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            int indexOfButton = int.Parse(button.Parent.Name.Split('_')[1]);

            Remove(indexOfButton);
        }

        public void Remove(int index)
        {
            listSeriesConfig.RemoveAt(index);
            listPanelSeriesConfig.RemoveAt(index);
            listSeriesConfigView.RemoveAt(index);
            flowLayoutPanel1.Controls.RemoveAt(index);

            //Update Pos
            for (int i = 0; i < listSeriesConfig.Count; i++)
            {
                listPanelSeriesConfig[i].Name = "Panel_" + i.ToString();
            }
        }

        private void AddNewPanelSeriesConfig(int index)
        {
            Panel panel = panelSeriesConfig.Clone();

            Label LabelChartName = labelChartName.Clone();
            ComboBox ComboBoxChartType = comboBoxChartType.Clone();
            Button ButtonChangeColor = buttonChangeColor.Clone();
            TextBox TextBoxColor = textBoxColor.Clone();
            Label LabelLineType = labelLineType.Clone();
            ComboBox ComboBoxDashStyle = comboBoxDashStyle.Clone();
            Label LabelBorderWidth = labelBorderWidth.Clone();
            ComboBox ComboBoxBorderWidth = comboBoxBorderWidth.Clone();
            Label LabelData = labelData.Clone();
            ComboBox ComboBoxData = comboBoxData.Clone();
            Button ButtonRemove = buttonRemove.Clone();

            panel.Controls.Add(LabelChartName);
            panel.Controls.Add(ComboBoxChartType);
            panel.Controls.Add(ButtonChangeColor);
            panel.Controls.Add(TextBoxColor);
            panel.Controls.Add(LabelLineType);
            panel.Controls.Add(ComboBoxDashStyle);
            panel.Controls.Add(LabelBorderWidth);
            panel.Controls.Add(ComboBoxBorderWidth);
            panel.Controls.Add(LabelData);
            panel.Controls.Add(ComboBoxData);
            panel.Controls.Add(ButtonRemove);

            SeriesConfigView seriesConfigView = new SeriesConfigView
            {
                LabelChartName = LabelChartName,
                ComboBoxChartType = ComboBoxChartType,
                ButtonChangeColor = ButtonChangeColor,
                TextBoxColor = TextBoxColor,
                LabelLineType = LabelLineType,
                ComboBoxDashStyle = ComboBoxDashStyle,
                LabelBorderWidth = LabelBorderWidth,
                ComboBoxBorderWidth = ComboBoxBorderWidth,
                LabelData = LabelData,
                ComboBoxData = ComboBoxData,
                ButtonRemove = ButtonRemove
            };

            listPanelSeriesConfig.Add(panel);
            listSeriesConfigView.Add(seriesConfigView);

            listPanelSeriesConfig[index].Name = "Panel_" + index.ToString();

            //Chart Type
            Utility.LoadDataForComboxWithEnum(typeof(SingleChart), listSeriesConfigView[index].ComboBoxChartType, -1);

            SingleChart chartType = SingleChart.Column;

            if (listSeriesConfig[index].ChartType.ToString() != "StackedColumn")
            {
                chartType = (SingleChart)listSeriesConfig[index].ChartType;
            }

            listSeriesConfigView[index].ComboBoxChartType.SelectedItem = chartType;
            listSeriesConfigView[index].ComboBoxChartType.SelectedIndexChanged += ComboBoxChartType_SelectedIndexChanged;
            //Color
            listSeriesConfigView[index].TextBoxColor.BackColor = listSeriesConfig[index].SeriesColor;
            listSeriesConfigView[index].ButtonChangeColor.Click += OnClickChangeColor;

            //Line Type
            Utility.LoadDataForComboxWithEnum(typeof(ChartDashStyle), listSeriesConfigView[index].ComboBoxDashStyle);
            listSeriesConfigView[index].ComboBoxDashStyle.SelectedItem = listSeriesConfig[index].DashStyle;

            //Border Width
            for (int border = 1; border <= 10; border++)
            {
                listSeriesConfigView[index].ComboBoxBorderWidth.Items.Add(border);
            }

            listSeriesConfigView[index].ComboBoxBorderWidth.SelectedItem = 1;

            //Data
            Utility.LoadDataForComboxWithEnum(typeof(KeyData), listSeriesConfigView[index].ComboBoxData);
            listSeriesConfigView[index].ComboBoxData.SelectedItem = listSeriesConfig[index].ChartData;

            //Button Remove
            listSeriesConfigView[index].ButtonRemove.Click += OnClickRemove;

            CheckShowSettingLine(index, (SeriesChartType)listSeriesConfigView[index].ComboBoxChartType.SelectedItem);

            flowLayoutPanel1.Controls.Add(listPanelSeriesConfig[index]);
        }

        private void ButtonOK_Click(object sender, EventArgs e)
        {
            ApproveData();
            OnOK(listSeriesConfig);
            Close();
        }

        private void ApproveData()
        {
            if (listSeriesConfig != null && listSeriesConfig.Count > 0)
            {
                for (int i = 0; i < listSeriesConfig.Count; i++)
                {
                    listSeriesConfig[i].SeriesName = listSeriesConfigView[i].ComboBoxChartType.Parent.Name;
                    listSeriesConfig[i].ChartType = (SeriesChartType)listSeriesConfigView[i].ComboBoxChartType.SelectedItem;
                    listSeriesConfig[i].SeriesColor = listSeriesConfigView[i].TextBoxColor.BackColor;
                    listSeriesConfig[i].DashStyle = (ChartDashStyle)listSeriesConfigView[i].ComboBoxDashStyle.SelectedItem;
                    listSeriesConfig[i].BorderWidth = (int)listSeriesConfigView[i].ComboBoxBorderWidth.SelectedItem;
                    listSeriesConfig[i].ChartData = (KeyData)listSeriesConfigView[i].ComboBoxData.SelectedItem;
                }
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ComboBoxChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox ComboBox = (ComboBox)sender;

            int indexOfComboBox = int.Parse(ComboBox.Parent.Name.Split('_')[1]);

            CheckShowSettingLine(indexOfComboBox, (SeriesChartType)listSeriesConfigView[indexOfComboBox].ComboBoxChartType.SelectedItem);
        }

        private void ButtonAddChart_Click(object sender, EventArgs e)
        {
            listSeriesConfig.Add(new SeriesConfig());
            AddNewPanelSeriesConfig(listSeriesConfig.Count - 1);
        }
    }

    public class SeriesConfigView
    {
        public Label LabelChartName;
        public ComboBox ComboBoxChartType;
        public Button ButtonChangeColor;
        public TextBox TextBoxColor;
        public Label LabelLineType;
        public ComboBox ComboBoxDashStyle;
        public Label LabelBorderWidth;
        public ComboBox ComboBoxBorderWidth;
        public Label LabelData;
        public ComboBox ComboBoxData;
        public Button ButtonRemove;

        public SeriesConfigView()
        {

        }
    }
}