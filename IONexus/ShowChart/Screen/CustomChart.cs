﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ShowChart.Screen
{
    public partial class CustomChart : Form
    {
        private ChartMode currentMode;
        private SeriesConfig seriesConfig = new SeriesConfig();
        private List<SeriesConfig> listSeriesConfig = new List<SeriesConfig>();

        public CustomChart()
        {
            InitializeComponent();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            LoadDataForCombobox();
        }

        private void LoadDataForCombobox()
        {
            Utility.LoadDataForComboxWithEnum(typeof(ChartMode), cbChartMode, 0);
            Utility.LoadDataForComboxWithEnum(typeof(SingleChart), cbChartStyle, 0);

            currentMode = (ChartMode)cbChartMode.SelectedItem;
        }

        private void ClearDataChart()
        {
            chartMain.Series.Clear();
            chartMain.ChartAreas[0].Area3DStyle.Enable3D = check3D.Checked;
        }

        private void Reload(List<SeriesConfig> config)
        {
            listSeriesConfig = config;

            ClearDataChart();
            LoadChart(config);
        }

        private void LoadChart(List<SeriesConfig> config)
        {
            for (int i = 0; i < config.Count; i++)
            {
                LoadSingleChart(config[i]);
            }
        }

        private void LoadSingleChart(SeriesConfig config)
        {
            Utility.LoadChart(config, chartMain);

            if (config.ChartType == SeriesChartType.Radar)
            {
                chartMain.Series[config.SeriesName]["RadarDrawingStyle"] = "Line";
            }
        }

        private void ShowSingleChart(SingleChart singleChart)
        {
            seriesConfig = new SeriesConfig
            {
                SeriesName = "Single",
                ChartType = (SeriesChartType)singleChart,
                SeriesColor = Color.Green,
                ShowPercentage = checkShowPercent.Checked
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        private void LoadTriangularRadar()
        {
            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Radar,
                SeriesName = "Radar1",
                SeriesColor = Color.Green,
                ShowPercentage = checkShowPercent.Checked,
                DashStyle = ChartDashStyle.Dash,
                ChartData = KeyData.Pig3
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Radar,
                SeriesName = "Radar2",
                SeriesColor = Color.Yellow,
                ShowPercentage = checkShowPercent.Checked,
                DashStyle = ChartDashStyle.Solid,
                ChartData = KeyData.Cow3
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        private void LoadHexagonalRadar()
        {
            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Radar,
                SeriesName = "Radar1",
                SeriesColor = Color.Green,
                ShowPercentage = checkShowPercent.Checked,
                DashStyle = ChartDashStyle.Dash,
                ChartData = KeyData.Pig6
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Radar,
                SeriesName = "Radar2",
                SeriesColor = Color.Yellow,
                ShowPercentage = checkShowPercent.Checked,
                DashStyle = ChartDashStyle.Solid,
                ChartData = KeyData.Cow6
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        private void ShowMultiChart(MultiChart multiChart)
        {
            ClearDataChart();

            switch (multiChart)
            {
                case MultiChart.Column_Line: ShowColumnLine(); break;
                case MultiChart.Three_StackedColumn_Line: ShowThreeColumnLine(); break;
                case MultiChart.Line_Point: ShowLinePoitChart(); break;
                case MultiChart.Column_Line_Point: ShowColumnLinePoitChart(); break;
                case MultiChart.Three_Column_In_One: ShowThreeColumnInOne(); break;
                case MultiChart.Three_Line: ShowThreeLine(); break;
                case MultiChart.Radar_3: LoadTriangularRadar(); break;
                case MultiChart.Radar_6: LoadHexagonalRadar(); break;
            }
        }

        #region Show Multi Chart

        private void ShowColumnLine()
        {
            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Column,
                SeriesName = "Column",
                SeriesColor = Color.Green,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.Cow
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Line,
                SeriesName = "Line",
                SeriesColor = Color.Blue,
                ShowPercentage = checkShowPercent.Checked,
                DashStyle = ChartDashStyle.Solid,
                AxisType = AxisType.Secondary,
                ChartData = KeyData.CowLine,
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        private void ShowThreeLine()
        {
            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Line,
                SeriesName = "Line1",
                SeriesColor = Color.Green,
                ShowPercentage = checkShowPercent.Checked,
                DashStyle = ChartDashStyle.Dash,
                ChartData = KeyData.Pig
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Line,
                SeriesName = "Line2",
                SeriesColor = Color.Yellow,
                ShowPercentage = checkShowPercent.Checked,
                DashStyle = ChartDashStyle.Solid,
                ChartData = KeyData.Cow
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Line,
                SeriesName = "Line3",
                SeriesColor = Color.Blue,
                ShowPercentage = checkShowPercent.Checked,
                DashStyle = ChartDashStyle.Dot,
                ChartData = KeyData.Chicken
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        private void ShowThreeColumnInOne()
        {
            seriesConfig = new SeriesConfig
            {
                StackedGroup = "Col",
                ChartType = SeriesChartType.StackedColumn,
                SeriesName = "Column1",
                SeriesColor = Color.Green,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.Pig
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                StackedGroup = "Col",
                ChartType = SeriesChartType.StackedColumn,
                SeriesName = "Column2",
                SeriesColor = Color.Yellow,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.Cow
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                StackedGroup = "Col",
                ChartType = SeriesChartType.StackedColumn,
                SeriesName = "Column3",
                SeriesColor = Color.Blue,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.Chicken
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        private void ShowLinePoitChart()
        {
            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Line,
                SeriesName = "Line",
                SeriesColor = Color.Blue,
                ShowPercentage = false,
                DashStyle = ChartDashStyle.Dash
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Point,
                SeriesName = "Point",
                SeriesColor = Color.Red,
                ShowPercentage = checkShowPercent.Checked,
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        private void ShowColumnLinePoitChart()
        {
            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Column,
                SeriesName = "Column",
                SeriesColor = Color.Green,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.Cow
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Line,
                SeriesName = "Line",
                SeriesColor = Color.Blue,
                ShowPercentage = false,
                DashStyle = ChartDashStyle.Dash,
                ChartData = KeyData.CowLine,
                AxisType = AxisType.Secondary
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                ChartType = SeriesChartType.Point,
                SeriesName = "Point",
                SeriesColor = Color.Red,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.CowLine,
                AxisType = AxisType.Secondary
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        private void ShowThreeColumnLine()
        {
            seriesConfig = new SeriesConfig
            {
                StackedGroup = "Col",
                ChartType = SeriesChartType.StackedColumn,
                SeriesName = "Column1",
                SeriesColor = Color.Green,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.Pig
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                StackedGroup = "Col",
                ChartType = SeriesChartType.StackedColumn,
                SeriesName = "Column2",
                SeriesColor = Color.Yellow,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.Cow
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                StackedGroup = "Col",
                ChartType = SeriesChartType.StackedColumn,
                SeriesName = "Column3",
                SeriesColor = Color.Blue,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.Chicken
            };

            listSeriesConfig.Add(seriesConfig);

            seriesConfig = new SeriesConfig
            {
                StackedGroup = "",
                ChartType = SeriesChartType.Line,
                SeriesName = "Line",
                SeriesColor = Color.Red,
                ShowPercentage = checkShowPercent.Checked,
                ChartData = KeyData.CowLine,
                AxisType = AxisType.Secondary
            };

            listSeriesConfig.Add(seriesConfig);

            LoadChart(listSeriesConfig);
        }

        #endregion

        private void ButtonShowChart_Click(object sender, EventArgs e)
        {
            ClearDataChart();
            listSeriesConfig.Clear();

            switch ((ChartMode)cbChartMode.SelectedItem)
            {
                case ChartMode.Single: ShowSingleChart((SingleChart)cbChartStyle.SelectedItem); break;
                case ChartMode.Multi: ShowMultiChart((MultiChart)cbChartStyle.SelectedItem); break;
            }
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonCustom_Click(object sender, EventArgs e)
        {
            EditSeries EditSeries = new EditSeries(listSeriesConfig, Reload, currentMode);

            EditSeries.ShowDialog();
        }

        private void ChartMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChartMode changeMode = (ChartMode)cbChartMode.SelectedItem;

            if (currentMode != changeMode)
            {
                currentMode = changeMode;

                switch (currentMode)
                {
                    case ChartMode.Single: Utility.LoadDataForComboxWithEnum(typeof(SingleChart), cbChartStyle, 0); break;
                    case ChartMode.Multi: Utility.LoadDataForComboxWithEnum(typeof(MultiChart), cbChartStyle, 0); break;
                }
            }
        }
    }
}