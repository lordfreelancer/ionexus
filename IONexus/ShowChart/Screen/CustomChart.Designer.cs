﻿namespace ShowChart.Screen
{
    partial class CustomChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.chartMain = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.check3D = new System.Windows.Forms.CheckBox();
            this.dialogPickColor1 = new System.Windows.Forms.ColorDialog();
            this.dialogPickColor2 = new System.Windows.Forms.ColorDialog();
            this.cbChartMode = new System.Windows.Forms.ComboBox();
            this.cbChartStyle = new System.Windows.Forms.ComboBox();
            this.buttonLoadChart = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonCustom = new System.Windows.Forms.Button();
            this.checkShowPercent = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.chartMain)).BeginInit();
            this.SuspendLayout();
            // 
            // chartMain
            // 
            chartArea1.BorderColor = System.Drawing.Color.Bisque;
            chartArea1.Name = "chartArea";
            this.chartMain.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartMain.Legends.Add(legend1);
            this.chartMain.Location = new System.Drawing.Point(12, 12);
            this.chartMain.Name = "chartMain";
            this.chartMain.Size = new System.Drawing.Size(522, 483);
            this.chartMain.TabIndex = 7;
            this.chartMain.Text = "chartMain";
            // 
            // check3D
            // 
            this.check3D.AutoSize = true;
            this.check3D.Location = new System.Drawing.Point(698, 66);
            this.check3D.Name = "check3D";
            this.check3D.Size = new System.Drawing.Size(40, 17);
            this.check3D.TabIndex = 10;
            this.check3D.Text = "3D";
            this.check3D.UseVisualStyleBackColor = true;
            // 
            // cbChartMode
            // 
            this.cbChartMode.FormattingEnabled = true;
            this.cbChartMode.Location = new System.Drawing.Point(540, 12);
            this.cbChartMode.Name = "cbChartMode";
            this.cbChartMode.Size = new System.Drawing.Size(171, 21);
            this.cbChartMode.TabIndex = 16;
            this.cbChartMode.SelectedIndexChanged += new System.EventHandler(this.ChartMode_SelectedIndexChanged);
            // 
            // cbChartStyle
            // 
            this.cbChartStyle.FormattingEnabled = true;
            this.cbChartStyle.Location = new System.Drawing.Point(540, 39);
            this.cbChartStyle.Name = "cbChartStyle";
            this.cbChartStyle.Size = new System.Drawing.Size(171, 21);
            this.cbChartStyle.TabIndex = 17;
            // 
            // buttonLoadChart
            // 
            this.buttonLoadChart.Location = new System.Drawing.Point(540, 66);
            this.buttonLoadChart.Name = "buttonLoadChart";
            this.buttonLoadChart.Size = new System.Drawing.Size(152, 43);
            this.buttonLoadChart.TabIndex = 20;
            this.buttonLoadChart.Text = "Show Chart";
            this.buttonLoadChart.UseVisualStyleBackColor = true;
            this.buttonLoadChart.Click += new System.EventHandler(this.ButtonShowChart_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(540, 501);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(152, 43);
            this.buttonClose.TabIndex = 23;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // buttonCustom
            // 
            this.buttonCustom.Location = new System.Drawing.Point(540, 115);
            this.buttonCustom.Name = "buttonCustom";
            this.buttonCustom.Size = new System.Drawing.Size(80, 43);
            this.buttonCustom.TabIndex = 24;
            this.buttonCustom.Text = "Style Custom";
            this.buttonCustom.UseVisualStyleBackColor = true;
            this.buttonCustom.Click += new System.EventHandler(this.ButtonCustom_Click);
            // 
            // checkShowPercent
            // 
            this.checkShowPercent.AutoSize = true;
            this.checkShowPercent.Location = new System.Drawing.Point(698, 92);
            this.checkShowPercent.Name = "checkShowPercent";
            this.checkShowPercent.Size = new System.Drawing.Size(63, 17);
            this.checkShowPercent.TabIndex = 25;
            this.checkShowPercent.Text = "Percent";
            this.checkShowPercent.UseVisualStyleBackColor = true;
            // 
            // CustomChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(809, 554);
            this.Controls.Add(this.checkShowPercent);
            this.Controls.Add(this.buttonCustom);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonLoadChart);
            this.Controls.Add(this.cbChartStyle);
            this.Controls.Add(this.cbChartMode);
            this.Controls.Add(this.check3D);
            this.Controls.Add(this.chartMain);
            this.Name = "CustomChart";
            this.Text = "Custom Chart";
            this.Load += new System.EventHandler(this.OnLoad);
            ((System.ComponentModel.ISupportInitialize)(this.chartMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataVisualization.Charting.Chart chartMain;
        private System.Windows.Forms.CheckBox check3D;
        private System.Windows.Forms.ColorDialog dialogPickColor1;
        private System.Windows.Forms.ColorDialog dialogPickColor2;
        private System.Windows.Forms.ComboBox cbChartMode;
        private System.Windows.Forms.ComboBox cbChartStyle;
        private System.Windows.Forms.Button buttonLoadChart;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonCustom;
        private System.Windows.Forms.CheckBox checkShowPercent;
    }
}

