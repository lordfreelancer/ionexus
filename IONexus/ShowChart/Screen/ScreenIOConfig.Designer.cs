﻿namespace ShowChart.Screen
{
    partial class ScreenIOConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridIOConfig = new System.Windows.Forms.DataGridView();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonPrevious = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.dataGridIOElement = new System.Windows.Forms.DataGridView();
            this.buttonSelectAll = new System.Windows.Forms.Button();
            this.buttonRemoveAllSelect = new System.Windows.Forms.Button();
            this.panelCalculator = new System.Windows.Forms.FlowLayoutPanel();
            this.paneCalculator = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxOperator = new System.Windows.Forms.ComboBox();
            this.textBoxFrom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.labelChartName = new System.Windows.Forms.Label();
            this.comboBoxFile = new System.Windows.Forms.ComboBox();
            this.panelSelectAll = new System.Windows.Forms.Panel();
            this.panelAddConfig = new System.Windows.Forms.Panel();
            this.buttonAddParam = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxIOConfig = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxIOElement = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIOConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIOElement)).BeginInit();
            this.panelCalculator.SuspendLayout();
            this.paneCalculator.SuspendLayout();
            this.panelSelectAll.SuspendLayout();
            this.panelAddConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridIOConfig
            // 
            this.dataGridIOConfig.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridIOConfig.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridIOConfig.Location = new System.Drawing.Point(12, 71);
            this.dataGridIOConfig.Name = "dataGridIOConfig";
            this.dataGridIOConfig.Size = new System.Drawing.Size(760, 228);
            this.dataGridIOConfig.TabIndex = 0;
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(620, 390);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(152, 43);
            this.buttonNext.TabIndex = 21;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // buttonPrevious
            // 
            this.buttonPrevious.Location = new System.Drawing.Point(462, 390);
            this.buttonPrevious.Name = "buttonPrevious";
            this.buttonPrevious.Size = new System.Drawing.Size(152, 43);
            this.buttonPrevious.TabIndex = 22;
            this.buttonPrevious.Text = "Previous";
            this.buttonPrevious.UseVisualStyleBackColor = true;
            this.buttonPrevious.Click += new System.EventHandler(this.ButtonPrevious_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(12, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(226, 46);
            this.labelTitle.TabIndex = 23;
            this.labelTitle.Text = "IO CONFIG";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dataGridIOElement
            // 
            this.dataGridIOElement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridIOElement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridIOElement.Location = new System.Drawing.Point(12, 71);
            this.dataGridIOElement.Name = "dataGridIOElement";
            this.dataGridIOElement.Size = new System.Drawing.Size(760, 228);
            this.dataGridIOElement.TabIndex = 24;
            // 
            // buttonSelectAll
            // 
            this.buttonSelectAll.Location = new System.Drawing.Point(3, 13);
            this.buttonSelectAll.Name = "buttonSelectAll";
            this.buttonSelectAll.Size = new System.Drawing.Size(101, 25);
            this.buttonSelectAll.TabIndex = 25;
            this.buttonSelectAll.Text = "Select All";
            this.buttonSelectAll.UseVisualStyleBackColor = true;
            this.buttonSelectAll.Click += new System.EventHandler(this.ButtonSelectAll_Click);
            // 
            // buttonRemoveAllSelect
            // 
            this.buttonRemoveAllSelect.Location = new System.Drawing.Point(119, 13);
            this.buttonRemoveAllSelect.Name = "buttonRemoveAllSelect";
            this.buttonRemoveAllSelect.Size = new System.Drawing.Size(119, 25);
            this.buttonRemoveAllSelect.TabIndex = 26;
            this.buttonRemoveAllSelect.Text = "Remove All Select";
            this.buttonRemoveAllSelect.UseVisualStyleBackColor = true;
            this.buttonRemoveAllSelect.Click += new System.EventHandler(this.ButtonRemoveAllSelect_Click);
            // 
            // panelCalculator
            // 
            this.panelCalculator.AutoScroll = true;
            this.panelCalculator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelCalculator.Controls.Add(this.paneCalculator);
            this.panelCalculator.Location = new System.Drawing.Point(51, 71);
            this.panelCalculator.Name = "panelCalculator";
            this.panelCalculator.Size = new System.Drawing.Size(721, 228);
            this.panelCalculator.TabIndex = 27;
            // 
            // paneCalculator
            // 
            this.paneCalculator.Controls.Add(this.label3);
            this.paneCalculator.Controls.Add(this.label2);
            this.paneCalculator.Controls.Add(this.comboBoxOperator);
            this.paneCalculator.Controls.Add(this.textBoxFrom);
            this.paneCalculator.Controls.Add(this.label1);
            this.paneCalculator.Controls.Add(this.textBoxTo);
            this.paneCalculator.Controls.Add(this.labelChartName);
            this.paneCalculator.Controls.Add(this.comboBoxFile);
            this.paneCalculator.Location = new System.Drawing.Point(3, 3);
            this.paneCalculator.Name = "paneCalculator";
            this.paneCalculator.Size = new System.Drawing.Size(682, 66);
            this.paneCalculator.TabIndex = 28;
            this.paneCalculator.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(506, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "OPERATOR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "FILE";
            // 
            // comboBoxOperator
            // 
            this.comboBoxOperator.FormattingEnabled = true;
            this.comboBoxOperator.Location = new System.Drawing.Point(509, 32);
            this.comboBoxOperator.Name = "comboBoxOperator";
            this.comboBoxOperator.Size = new System.Drawing.Size(120, 21);
            this.comboBoxOperator.TabIndex = 33;
            // 
            // textBoxFrom
            // 
            this.textBoxFrom.Location = new System.Drawing.Point(305, 31);
            this.textBoxFrom.Name = "textBoxFrom";
            this.textBoxFrom.Size = new System.Drawing.Size(95, 20);
            this.textBoxFrom.TabIndex = 32;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(405, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "TO COLUMN";
            // 
            // textBoxTo
            // 
            this.textBoxTo.Location = new System.Drawing.Point(408, 32);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.Size = new System.Drawing.Size(95, 20);
            this.textBoxTo.TabIndex = 30;
            // 
            // labelChartName
            // 
            this.labelChartName.AutoSize = true;
            this.labelChartName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelChartName.Location = new System.Drawing.Point(302, 14);
            this.labelChartName.Name = "labelChartName";
            this.labelChartName.Size = new System.Drawing.Size(98, 13);
            this.labelChartName.TabIndex = 29;
            this.labelChartName.Text = "FROM COLUMN";
            // 
            // comboBoxFile
            // 
            this.comboBoxFile.FormattingEnabled = true;
            this.comboBoxFile.Location = new System.Drawing.Point(13, 30);
            this.comboBoxFile.Name = "comboBoxFile";
            this.comboBoxFile.Size = new System.Drawing.Size(286, 21);
            this.comboBoxFile.TabIndex = 25;
            // 
            // panelSelectAll
            // 
            this.panelSelectAll.Controls.Add(this.buttonRemoveAllSelect);
            this.panelSelectAll.Controls.Add(this.buttonSelectAll);
            this.panelSelectAll.Location = new System.Drawing.Point(12, 305);
            this.panelSelectAll.Name = "panelSelectAll";
            this.panelSelectAll.Size = new System.Drawing.Size(241, 51);
            this.panelSelectAll.TabIndex = 37;
            // 
            // panelAddConfig
            // 
            this.panelAddConfig.Controls.Add(this.label5);
            this.panelAddConfig.Controls.Add(this.comboBoxIOElement);
            this.panelAddConfig.Controls.Add(this.label4);
            this.panelAddConfig.Controls.Add(this.comboBoxIOConfig);
            this.panelAddConfig.Controls.Add(this.buttonAddParam);
            this.panelAddConfig.Location = new System.Drawing.Point(161, 305);
            this.panelAddConfig.Name = "panelAddConfig";
            this.panelAddConfig.Size = new System.Drawing.Size(611, 67);
            this.panelAddConfig.TabIndex = 38;
            // 
            // buttonAddParam
            // 
            this.buttonAddParam.Location = new System.Drawing.Point(483, 29);
            this.buttonAddParam.Name = "buttonAddParam";
            this.buttonAddParam.Size = new System.Drawing.Size(119, 25);
            this.buttonAddParam.TabIndex = 26;
            this.buttonAddParam.Text = "Add Param";
            this.buttonAddParam.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "IO CONFIG";
            // 
            // comboBoxIOConfig
            // 
            this.comboBoxIOConfig.FormattingEnabled = true;
            this.comboBoxIOConfig.Location = new System.Drawing.Point(11, 32);
            this.comboBoxIOConfig.Name = "comboBoxIOConfig";
            this.comboBoxIOConfig.Size = new System.Drawing.Size(230, 21);
            this.comboBoxIOConfig.TabIndex = 37;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(244, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "IO ELEMENT";
            // 
            // comboBoxIOElement
            // 
            this.comboBoxIOElement.FormattingEnabled = true;
            this.comboBoxIOElement.Location = new System.Drawing.Point(247, 32);
            this.comboBoxIOElement.Name = "comboBoxIOElement";
            this.comboBoxIOElement.Size = new System.Drawing.Size(230, 21);
            this.comboBoxIOElement.TabIndex = 39;
            // 
            // ScreenIOConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 445);
            this.Controls.Add(this.panelAddConfig);
            this.Controls.Add(this.panelSelectAll);
            this.Controls.Add(this.panelCalculator);
            this.Controls.Add(this.dataGridIOElement);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonPrevious);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.dataGridIOConfig);
            this.Name = "ScreenIOConfig";
            this.Text = "CONFIG";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIOConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIOElement)).EndInit();
            this.panelCalculator.ResumeLayout(false);
            this.paneCalculator.ResumeLayout(false);
            this.paneCalculator.PerformLayout();
            this.panelSelectAll.ResumeLayout(false);
            this.panelAddConfig.ResumeLayout(false);
            this.panelAddConfig.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridIOConfig;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonPrevious;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.DataGridView dataGridIOElement;
        private System.Windows.Forms.Button buttonSelectAll;
        private System.Windows.Forms.Button buttonRemoveAllSelect;
        private System.Windows.Forms.FlowLayoutPanel panelCalculator;
        private System.Windows.Forms.Panel paneCalculator;
        private System.Windows.Forms.Label labelChartName;
        private System.Windows.Forms.ComboBox comboBoxFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxOperator;
        private System.Windows.Forms.TextBox textBoxFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelSelectAll;
        private System.Windows.Forms.Panel panelAddConfig;
        private System.Windows.Forms.Button buttonAddParam;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxIOConfig;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxIOElement;
    }
}