﻿namespace ShowChart.Screen
{
    partial class EditSeries
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panelSeriesConfig = new System.Windows.Forms.Panel();
            this.comboBoxBorderWidth = new System.Windows.Forms.ComboBox();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.comboBoxChartType = new System.Windows.Forms.ComboBox();
            this.comboBoxData = new System.Windows.Forms.ComboBox();
            this.labelData = new System.Windows.Forms.Label();
            this.labelChartName = new System.Windows.Forms.Label();
            this.labelBorderWidth = new System.Windows.Forms.Label();
            this.labelLineType = new System.Windows.Forms.Label();
            this.comboBoxDashStyle = new System.Windows.Forms.ComboBox();
            this.textBoxColor = new System.Windows.Forms.TextBox();
            this.buttonChangeColor = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.buttonAddChart = new System.Windows.Forms.Button();
            this.panelSeriesConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1088, 287);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panelSeriesConfig
            // 
            this.panelSeriesConfig.Controls.Add(this.comboBoxBorderWidth);
            this.panelSeriesConfig.Controls.Add(this.buttonRemove);
            this.panelSeriesConfig.Controls.Add(this.comboBoxChartType);
            this.panelSeriesConfig.Controls.Add(this.comboBoxData);
            this.panelSeriesConfig.Controls.Add(this.labelData);
            this.panelSeriesConfig.Controls.Add(this.labelChartName);
            this.panelSeriesConfig.Controls.Add(this.labelBorderWidth);
            this.panelSeriesConfig.Controls.Add(this.labelLineType);
            this.panelSeriesConfig.Controls.Add(this.comboBoxDashStyle);
            this.panelSeriesConfig.Controls.Add(this.textBoxColor);
            this.panelSeriesConfig.Controls.Add(this.buttonChangeColor);
            this.panelSeriesConfig.Location = new System.Drawing.Point(12, 351);
            this.panelSeriesConfig.Name = "panelSeriesConfig";
            this.panelSeriesConfig.Size = new System.Drawing.Size(1048, 46);
            this.panelSeriesConfig.TabIndex = 1;
            this.panelSeriesConfig.Visible = false;
            // 
            // comboBoxBorderWidth
            // 
            this.comboBoxBorderWidth.FormattingEnabled = true;
            this.comboBoxBorderWidth.Location = new System.Drawing.Point(702, 12);
            this.comboBoxBorderWidth.Name = "comboBoxBorderWidth";
            this.comboBoxBorderWidth.Size = new System.Drawing.Size(34, 21);
            this.comboBoxBorderWidth.TabIndex = 34;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(917, 6);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(100, 30);
            this.buttonRemove.TabIndex = 33;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            // 
            // comboBoxChartType
            // 
            this.comboBoxChartType.FormattingEnabled = true;
            this.comboBoxChartType.Location = new System.Drawing.Point(85, 11);
            this.comboBoxChartType.Name = "comboBoxChartType";
            this.comboBoxChartType.Size = new System.Drawing.Size(133, 21);
            this.comboBoxChartType.TabIndex = 32;
            // 
            // comboBoxData
            // 
            this.comboBoxData.FormattingEnabled = true;
            this.comboBoxData.Location = new System.Drawing.Point(778, 11);
            this.comboBoxData.Name = "comboBoxData";
            this.comboBoxData.Size = new System.Drawing.Size(133, 21);
            this.comboBoxData.TabIndex = 31;
            // 
            // labelData
            // 
            this.labelData.AutoSize = true;
            this.labelData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelData.Location = new System.Drawing.Point(742, 15);
            this.labelData.Name = "labelData";
            this.labelData.Size = new System.Drawing.Size(30, 13);
            this.labelData.TabIndex = 30;
            this.labelData.Text = "Data";
            // 
            // labelChartName
            // 
            this.labelChartName.AutoSize = true;
            this.labelChartName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelChartName.Location = new System.Drawing.Point(20, 14);
            this.labelChartName.Name = "labelChartName";
            this.labelChartName.Size = new System.Drawing.Size(59, 13);
            this.labelChartName.TabIndex = 29;
            this.labelChartName.Text = "Chart Type";
            // 
            // labelBorderWidth
            // 
            this.labelBorderWidth.AutoSize = true;
            this.labelBorderWidth.Location = new System.Drawing.Point(630, 15);
            this.labelBorderWidth.Name = "labelBorderWidth";
            this.labelBorderWidth.Size = new System.Drawing.Size(66, 13);
            this.labelBorderWidth.TabIndex = 27;
            this.labelBorderWidth.Text = "BorderWidth";
            // 
            // labelLineType
            // 
            this.labelLineType.AutoSize = true;
            this.labelLineType.Location = new System.Drawing.Point(384, 15);
            this.labelLineType.Name = "labelLineType";
            this.labelLineType.Size = new System.Drawing.Size(54, 13);
            this.labelLineType.TabIndex = 26;
            this.labelLineType.Text = "Line Type";
            // 
            // comboBoxDashStyle
            // 
            this.comboBoxDashStyle.FormattingEnabled = true;
            this.comboBoxDashStyle.Location = new System.Drawing.Point(453, 12);
            this.comboBoxDashStyle.Name = "comboBoxDashStyle";
            this.comboBoxDashStyle.Size = new System.Drawing.Size(171, 21);
            this.comboBoxDashStyle.TabIndex = 25;
            // 
            // textBoxColor
            // 
            this.textBoxColor.Location = new System.Drawing.Point(336, 12);
            this.textBoxColor.Name = "textBoxColor";
            this.textBoxColor.Size = new System.Drawing.Size(40, 20);
            this.textBoxColor.TabIndex = 1;
            // 
            // buttonChangeColor
            // 
            this.buttonChangeColor.Location = new System.Drawing.Point(230, 6);
            this.buttonChangeColor.Name = "buttonChangeColor";
            this.buttonChangeColor.Size = new System.Drawing.Size(100, 30);
            this.buttonChangeColor.TabIndex = 0;
            this.buttonChangeColor.Text = "Change Color";
            this.buttonChangeColor.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(532, 403);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(150, 40);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(376, 403);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(150, 40);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "Apply";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.ButtonOK_Click);
            // 
            // buttonAddChart
            // 
            this.buttonAddChart.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.buttonAddChart.Location = new System.Drawing.Point(950, 305);
            this.buttonAddChart.Name = "buttonAddChart";
            this.buttonAddChart.Size = new System.Drawing.Size(150, 40);
            this.buttonAddChart.TabIndex = 4;
            this.buttonAddChart.Text = "Add Chart";
            this.buttonAddChart.UseVisualStyleBackColor = true;
            this.buttonAddChart.Click += new System.EventHandler(this.ButtonAddChart_Click);
            // 
            // EditSeries
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 473);
            this.Controls.Add(this.panelSeriesConfig);
            this.Controls.Add(this.buttonAddChart);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "EditSeries";
            this.Text = "EditSeries";
            this.panelSeriesConfig.ResumeLayout(false);
            this.panelSeriesConfig.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panelSeriesConfig;
        private System.Windows.Forms.Button buttonChangeColor;
        private System.Windows.Forms.TextBox textBoxColor;
        private System.Windows.Forms.Label labelLineType;
        private System.Windows.Forms.ComboBox comboBoxDashStyle;
        private System.Windows.Forms.Label labelChartName;
        private System.Windows.Forms.Label labelBorderWidth;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ComboBox comboBoxChartType;
        private System.Windows.Forms.ComboBox comboBoxData;
        private System.Windows.Forms.Label labelData;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonAddChart;
        private System.Windows.Forms.ComboBox comboBoxBorderWidth;
    }
}