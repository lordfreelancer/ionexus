﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using ShowChart.Scripts.IO;

namespace ShowChart.Screen
{
    public partial class ScreenIOConfig : Form
    {
        private enum Page
        {
            [Description("IO CONFIG")]
            IOConfig = 0,
            [Description("IO ELEMENT CONFIG")]
            IOElementConfig = 1,
            [Description("CALCULATOR")]
            IOCalculator = 2
        }

        private int currentPage;
        private List<IOConfig> listIOConfigSelect = new List<IOConfig>();
        private List<IOElementConfig> listIOElementConfigSelect = new List<IOElementConfig>();

        public ScreenIOConfig()
        {
            InitializeComponent();
            currentPage = 0;

            ShowPage();
        }

        private void ShowPage()
        {
            buttonPrevious.Visible = currentPage != 0;
            buttonSelectAll.Visible = currentPage == 0;
            buttonRemoveAllSelect.Visible = currentPage == 0;

            labelTitle.Text = Utility.GetEnumDescription((Page)currentPage);

            dataGridIOConfig.Visible = (Page)currentPage == Page.IOConfig;
            dataGridIOElement.Visible = (Page)currentPage == Page.IOElementConfig;
            //dataGridCalculator.Visible = (Page)currentPage == Page.IOCalculator;
            panelAddConfig.Visible = (Page)currentPage == Page.IOCalculator;
            panelCalculator.Visible = (Page)currentPage == Page.IOCalculator;

            switch ((Page)currentPage)
            {
                case Page.IOConfig: LoadDataPageIOConfig(); break;
                case Page.IOElementConfig: LoadDataPageIOElement(); break;
                case Page.IOCalculator: LoadDataCalculator(); break;
            }

            CheckShowButtonNext();
        }

        private void LoadDataPageIOConfig()
        {
            if (dataGridIOConfig.RowCount == 0)
            {
                LoadDataForDataGridView(IOManager.Intance.listIOConfig, dataGridIOConfig, true, new List<int> { 2, 4 });
            }
        }

        private void LoadDataPageIOElement()
        {
            if (dataGridIOElement.RowCount == 0)
            {
                LoadDataForDataGridView(IOManager.Intance.listIOElementConfig, dataGridIOElement, true, new List<int> { 2, 4 });
            }
        }

        private void LoadDataCalculator()
        {
            List<string> listHeader = new List<string> { "FILE", "FROM", "TO", "OPERATOR" };

            for (int i = 0; i < listHeader.Count; i++)
            {

            }

            listIOConfigSelect.Clear();
            listIOElementConfigSelect.Clear();

            for (int i = 0; i < dataGridIOConfig.RowCount; i++)
            {
                if ((bool)dataGridIOConfig.Rows[i].Cells[4].Value)
                {
                    int id = (int)dataGridIOConfig.Rows[i].Cells[0].Value;

                    listIOConfigSelect.Add(IOManager.Intance.GetData<IOConfig>(id));
                }
            }

            for (int i = 0; i < dataGridIOElement.RowCount; i++)
            {
                if ((bool)dataGridIOElement.Rows[i].Cells[4].Value)
                {
                    int id = (int)dataGridIOConfig.Rows[i].Cells[0].Value;

                    listIOElementConfigSelect.Add(IOManager.Intance.GetData<IOElementConfig>(id));
                }
            }
        }

        private void CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            switch ((Page)currentPage)
            {
                case Page.IOConfig:
                    {
                        if (e.ColumnIndex == dataGridIOConfig.ColumnCount - 1 && e.RowIndex < dataGridIOConfig.RowCount)
                        {
                            dataGridIOConfig.CommitEdit(DataGridViewDataErrorContexts.Commit);
                            CheckShowButtonNext();
                        }
                    }
                    break;
                case Page.IOElementConfig:
                    {
                        if (e.ColumnIndex == dataGridIOElement.ColumnCount - 1 && e.RowIndex < dataGridIOConfig.RowCount)
                        {
                            dataGridIOElement.CommitEdit(DataGridViewDataErrorContexts.Commit);

                            if ((bool)dataGridIOElement.Rows[e.RowIndex].Cells[e.ColumnIndex].Value)
                            {
                                for (int i = 0; i < dataGridIOElement.RowCount; i++)
                                {
                                    if (i != e.RowIndex)
                                    {
                                        dataGridIOElement.Rows[i].Cells[e.ColumnIndex].Value = false;
                                    }
                                }
                            }

                            CheckShowButtonNext();
                        }
                    }
                    break;
            }
        }

        private void CheckShowButtonNext()
        {
            bool enabled = false;

            switch ((Page)currentPage)
            {
                case Page.IOConfig:
                    {
                        for (int i = 0; i < dataGridIOConfig.RowCount; i++)
                        {
                            if ((bool)dataGridIOConfig.Rows[i].Cells[4].Value)
                            {
                                enabled = true;
                                break;
                            }
                        }
                    }
                    break;
                case Page.IOElementConfig:
                    {
                        for (int i = 0; i < dataGridIOElement.RowCount; i++)
                        {
                            if ((bool)dataGridIOElement.Rows[i].Cells[4].Value)
                            {
                                enabled = true;
                                break;
                            }
                        }
                    }
                    break;
                case Page.IOCalculator:
                    {
                        enabled = false;
                    };
                    break;
            }

            buttonNext.Enabled = enabled;
        }

        private void LoadDataForDataGridView<T>(List<T> data, DataGridView dataGrid, bool haveCheckBox = true, List<int> hideColumn = null) where T : JsonModel
        {
            dataGrid.Columns.Clear();
            dataGrid.Rows.Clear();
            dataGrid.AllowUserToAddRows = false;
            dataGrid.SelectionMode = DataGridViewSelectionMode.CellSelect;
            dataGrid.CellContentClick += CellContentClick;

            dataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            System.Reflection.PropertyInfo[] arrPropertyInfo = typeof(T).GetProperties();

            for (int i = 0; i < arrPropertyInfo.Length; i++)
            {
                dataGrid.Columns.Add(arrPropertyInfo[i].Name, arrPropertyInfo[i].Name.Replace('_', ' '));
            }

            //Select Flag CheckBoxColumn
            if (haveCheckBox)
            {
                DataGridViewCheckBoxColumn cmb = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "SELECT"
                };

                dataGrid.Columns.Add(cmb);
            }

            foreach (T item in data)
            {
                List<object> values = new List<object>();

                for (int i = 0; i < arrPropertyInfo.Length; i++)
                {
                    values.Add(arrPropertyInfo[i].GetValue(item));
                }

                //Select Flag Row
                if (haveCheckBox)
                {
                    values.Add(false);
                }

                dataGrid.Rows.Add(values.ToArray());
            }

            if (hideColumn != null && hideColumn.Count > 0)
            {
                for (int i = 0; i < hideColumn.Count; i++)
                {
                    dataGrid.Columns[hideColumn[i] - 1].Visible = false;
                }
            }
        }

        private void ButtonNext_Click(object sender, EventArgs e)
        {
            int next = Math.Min(currentPage + 1, Enum.GetValues(typeof(Page)).Length - 1);

            if (next != currentPage)
            {
                currentPage = next;
                ShowPage();
            }
        }

        private void ButtonPrevious_Click(object sender, EventArgs e)
        {
            int previous = Math.Max(currentPage - 1, 0);

            if (previous != currentPage)
            {
                currentPage = previous;
                ShowPage();
            }
        }

        private void ButtonSelectAll_Click(object sender, EventArgs e)
        {
            Select(true);
        }

        private void ButtonRemoveAllSelect_Click(object sender, EventArgs e)
        {
            Select(false);
        }

        private void Select(bool select)
        {
            DataGridView dataGrid = null;

            switch ((Page)currentPage)
            {
                case Page.IOConfig: dataGrid = dataGridIOConfig; break;
            }

            for (int i = 0; i < dataGrid.RowCount; i++)
            {
                dataGrid.Rows[i].Cells[dataGrid.ColumnCount - 1].Value = select;
            }

            buttonNext.Enabled = select;
        }

        private enum Operator
        {
            [Description("+")]
            Plus = 0,
            [Description("-")]
            Minus = 1,
            [Description("x")]
            Multiplication = 2,
            [Description("/")]
            Division = 3,
        }

        private class DataForCalculator
        {
            public int ColumnCount { get; set; }
            public int From { get; set; }
            public int To { get; set; }
            public List<string> listPath { get; set; }
            public string Path { get; set; }
            public Operator Operator { get; set; }

            public DataForCalculator()
            {

            }
        }
    }
}