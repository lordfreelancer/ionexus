﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IONexus
{
    public partial class Options : Form
    {
        public double pubValue = 0.51;
        public double priValue = 0.49;

        public Options()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            pubValue = Double.Parse(txtPublic.Text);
            priValue = Double.Parse(txtPrivate.Text);
        }

        private void Options_Load(object sender, EventArgs e)
        {
            txtPublic.Text = pubValue.ToString();
            txtPrivate.Text = priValue.ToString();
        }
    }
}
