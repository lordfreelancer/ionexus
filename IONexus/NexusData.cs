﻿using Matrix_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IONexus
{
    [Serializable]
    public class NexusData : ICloneable
    {
        public string[] interColumns;
        public string[] findemColumns;
        public string[] primaryFactors;

        public Matrix DosmeticFinDemTable { get; set; }
        public Matrix DosmeticInterDemTable { get; set; }
        public Matrix ImportFinDemTable { get; set; }
        public Matrix ImportInterDemTable { get; set; }
        public Matrix PrimaryInterDemTable { get; set; }
        public Matrix PrimaryFinDemTable { get; set; }
        public Matrix AddedValueTable { get; set; }
        public Vector DosmeticFinDemVec { get; set; }
        public Vector ImportFinDemVec { get; set; }
        public Vector PrimaryFinDemVec { get; set; }
        public Vector FinDemInputParamVec { get; set; }

        public NexusFormula formulaChange { get; set; }

        public string[,] Print()
        {
            string[,] values = new string[1+ DosmeticInterDemTable.NoRows + ImportInterDemTable.NoRows + PrimaryInterDemTable.NoRows, 1 + findemColumns.Length + interColumns.Length];
            for (int i = 0; i < interColumns.Length; i++)
            {
                values[0, 1 + i] = interColumns[i];
            }
            for (int i = 0; i < findemColumns.Length; i++)
            {
                values[0, 1 + interColumns.Length + i] = findemColumns[i];
            }
            for (int j = 0; j < DosmeticInterDemTable.NoRows; j++)
            {
                values[1+j, 0] = "D_" + interColumns[j];
            }
            for (int j = 0; j < ImportInterDemTable.NoRows; j++)
            {
                values[1 + DosmeticInterDemTable.NoRows + j, 0] = "I_" + interColumns[j];
            }
            for (int j = 0; j < primaryFactors.Length; j++)
            {
                values[1 + DosmeticInterDemTable.NoRows + ImportInterDemTable.NoRows + j, 0] = "P_" + primaryFactors[j];
            }

            for (int r = 0; r < DosmeticInterDemTable.NoRows; r++)
            {
                for (int c = 0; c < DosmeticInterDemTable.NoCols; c++)
                {
                    values[1 + r, 1 + c] = DosmeticInterDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < DosmeticFinDemTable.NoRows; r++)
            {
                for (int c = 0; c < DosmeticFinDemTable.NoCols; c++)
                {
                    values[1 + r, 1 + DosmeticInterDemTable.NoCols + c] = DosmeticFinDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < ImportInterDemTable.NoRows; r++)
            {
                for (int c = 0; c < ImportInterDemTable.NoCols; c++)
                {
                    values[1 + DosmeticInterDemTable.NoRows + r, 1 + c] = ImportInterDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < ImportFinDemTable.NoRows; r++)
            {
                for (int c = 0; c < ImportFinDemTable.NoCols; c++)
                {
                    values[1 + DosmeticInterDemTable.NoRows + r, 1 + ImportInterDemTable.NoCols + c] = ImportFinDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < PrimaryInterDemTable.NoRows; r++)
            {
                for (int c = 0; c < PrimaryInterDemTable.NoCols; c++)
                {
                    values[1 + DosmeticInterDemTable.NoRows + ImportInterDemTable.NoRows + r, 1 + c] = PrimaryInterDemTable[r, c].ToString();
                }
            }
            for (int r = 0; r < PrimaryFinDemTable.NoRows; r++)
            {
                for (int c = 0; c < PrimaryFinDemTable.NoCols; c++)
                {
                    values[1 + DosmeticInterDemTable.NoRows + ImportInterDemTable.NoRows + r, 1 + ImportInterDemTable.NoCols + c] = PrimaryFinDemTable[r, c].ToString();
                }
            }
            return values;
        }
        public static NexusData Parse(string [,]values)
        {
            NexusData data = new NexusData();
            int num_rows = values.GetUpperBound(0)+1;
            int num_cols = values.GetUpperBound(1)+1;
            if (num_rows < 2 || num_cols < 2) return null;
            //Parse Columns And Range
            List<string> _interColumns = new List<string>();
            List<string> _findemColumns = new List<string>();
            List<string> _primaryFactors = new List<string>();
            int maxDomestic = 0, maxImport = 0, maxPrimary =0, maxInter = 0, maxDemand = 0;
            for (int c = 0; c < num_cols; c++)
            {
                if (values[0, c] == "Intermediate")
                {
                    maxInter = c+1;
                    _interColumns.Add(values[1, c]);
                }
                else if (c>2) //if (values[0, c] == "Final Demand") //Now Final Demand included export column. So add it to out result
                {
                    maxDemand = c+1;
                    _findemColumns.Add(values[1, c]);
                }
            }

            for (int r = 0; r < num_rows; r++)
            {
                if (values[r, 0] == "Domestic")
                {
                    maxDomestic = r+1;
                }
                else if (values[r, 0] == "Import")
                {
                    maxImport = r+1;
                } else if (values[r, 0] == "Primary Factor")
                {
                    maxPrimary = r+1;
                    _primaryFactors.Add(values[r, 1]);
                }
            }

            data.interColumns = _interColumns.ToArray();
            data.findemColumns = _findemColumns.ToArray();
            data.primaryFactors = _primaryFactors.ToArray();
            data.ImportInterDemTable = new Matrix(maxImport- maxDomestic, _interColumns.Count);
            data.ImportFinDemTable = new Matrix(maxImport - maxDomestic, _findemColumns.Count);
            data.DosmeticInterDemTable = new Matrix(maxDomestic - 2, _interColumns.Count);
            data.DosmeticFinDemTable = new Matrix(maxDomestic - 2, _findemColumns.Count);
            data.PrimaryInterDemTable = new Matrix(maxPrimary - maxImport, _interColumns.Count);
            data.PrimaryFinDemTable = new Matrix(maxPrimary - maxImport, _findemColumns.Count);

            //Parse Data
            for (int r = 2; r < maxDomestic; r++)
            {
                for (int c = 2; c < maxInter; c++)
                {
                    try
                    {
                        data.DosmeticInterDemTable[r-2, c-2] = double.Parse(values[r, c]);
                    }
                    catch { }
                }
            }
            for (int r = 2; r < maxDomestic; r++)
            {
                for (int c = maxInter; c < maxDemand; c++)
                {
                    try
                    {
                        data.DosmeticFinDemTable[r - 2, c - maxInter] = double.Parse(values[r, c]);
                    }
                    catch { }
                }
            }
            for (int r = maxDomestic; r < maxImport; r++)
            {
                for (int c = 2; c < maxInter; c++)
                {
                    try
                    {
                        data.ImportInterDemTable[r - maxDomestic, c - 2] = double.Parse(values[r, c]);
                    }
                    catch { }
                }
            }
            for (int r = maxDomestic; r < maxImport; r++)
            {
                for (int c = maxInter; c < maxDemand; c++)
                {
                    try
                    {
                        data.ImportFinDemTable[r - maxDomestic, c - maxInter] = double.Parse(values[r, c]);
                    }
                    catch { }
                }
            }
            for (int r = maxImport; r < maxPrimary; r++)
            {
                for (int c = 2; c < maxInter; c++)
                {
                    try
                    {
                        data.PrimaryInterDemTable[r - maxImport, c - 2] = double.Parse(values[r, c]);
                    }
                    catch { }
                }
            }
            for (int r = maxImport; r < maxPrimary; r++)
            {
                for (int c = maxInter; c < maxDemand; c++)
                {
                    try
                    {
                        data.PrimaryFinDemTable[r - maxImport, c - maxInter] = double.Parse(values[r, c]);
                    }
                    catch { }
                }
            }

            return data;
        }
        public NexusData()
        {
        }
        public object Clone()
        {
            NexusData data = new NexusData();
            data.DosmeticFinDemTable = this.DosmeticFinDemTable.Clone();
            data.DosmeticInterDemTable = this.DosmeticInterDemTable.Clone();
            data.ImportFinDemTable = this.ImportFinDemTable.Clone();
            data.ImportInterDemTable = this.ImportInterDemTable.Clone();
            data.PrimaryInterDemTable = this.PrimaryInterDemTable.Clone();
            data.PrimaryFinDemTable = this.PrimaryFinDemTable.Clone();
            data.AddedValueTable = this.AddedValueTable.Clone();
            data.interColumns = (string[])this.interColumns.Clone();
            data.findemColumns = (string[])this.findemColumns.Clone();
            data.primaryFactors = (string[])this.primaryFactors.Clone();
            data.DosmeticFinDemVec = this.DosmeticFinDemVec.Clone();
            data.ImportFinDemVec = this.ImportFinDemVec.Clone();
            data.PrimaryFinDemVec = this.PrimaryFinDemVec.Clone();
            data.FinDemInputParamVec = this.FinDemInputParamVec.Clone();
            data.formulaChange = (NexusFormula)this.formulaChange.Clone();
            return data;
        }

        public string GetRowName(int index)
        {
            if (index < 0) return ""; 
            if (index < DosmeticInterDemTable.NoRows) return "D_" + interColumns[index];
            else if (index < DosmeticInterDemTable.NoRows + ImportInterDemTable.NoRows) return "I_" + interColumns[index - DosmeticInterDemTable.NoRows];
            else return "P_" + primaryFactors[index - DosmeticInterDemTable.NoRows - ImportInterDemTable.NoRows];
        }

        public void MappingFormula()
        {

        }
    }
}
