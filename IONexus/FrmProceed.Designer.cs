﻿namespace IONexus
{
    partial class FrmProceed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBPFrom = new System.Windows.Forms.TextBox();
            this.txtBPTo = new System.Windows.Forms.TextBox();
            this.txtIpFrom = new System.Windows.Forms.TextBox();
            this.txtIpTo = new System.Windows.Forms.TextBox();
            this.txtTableName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(95, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 32);
            this.label3.TabIndex = 7;
            this.label3.Text = "Import";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 32);
            this.label1.TabIndex = 6;
            this.label1.Text = "Domestic";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(291, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 32);
            this.label2.TabIndex = 8;
            this.label2.Text = "From";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(502, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 32);
            this.label4.TabIndex = 9;
            this.label4.Text = "To";
            // 
            // txtBPFrom
            // 
            this.txtBPFrom.Location = new System.Drawing.Point(260, 101);
            this.txtBPFrom.Name = "txtBPFrom";
            this.txtBPFrom.Size = new System.Drawing.Size(134, 38);
            this.txtBPFrom.TabIndex = 10;
            this.txtBPFrom.Text = "B2";
            // 
            // txtBPTo
            // 
            this.txtBPTo.Location = new System.Drawing.Point(446, 100);
            this.txtBPTo.Name = "txtBPTo";
            this.txtBPTo.Size = new System.Drawing.Size(134, 38);
            this.txtBPTo.TabIndex = 11;
            this.txtBPTo.Text = "EI139";
            // 
            // txtIpFrom
            // 
            this.txtIpFrom.Location = new System.Drawing.Point(260, 198);
            this.txtIpFrom.Name = "txtIpFrom";
            this.txtIpFrom.Size = new System.Drawing.Size(134, 38);
            this.txtIpFrom.TabIndex = 12;
            this.txtIpFrom.Text = "B140";
            // 
            // txtIpTo
            // 
            this.txtIpTo.Location = new System.Drawing.Point(446, 198);
            this.txtIpTo.Name = "txtIpTo";
            this.txtIpTo.Size = new System.Drawing.Size(134, 38);
            this.txtIpTo.TabIndex = 13;
            this.txtIpTo.Text = "EI139";
            // 
            // txtTableName
            // 
            this.txtTableName.Location = new System.Drawing.Point(260, 299);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(320, 38);
            this.txtTableName.TabIndex = 14;
            this.txtTableName.Text = "IO2016";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 302);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 32);
            this.label5.TabIndex = 15;
            this.label5.Text = "Table Name";
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(433, 379);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(147, 53);
            this.button2.TabIndex = 17;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(260, 379);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(147, 53);
            this.btnSave.TabIndex = 16;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmProceed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 458);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTableName);
            this.Controls.Add(this.txtIpTo);
            this.Controls.Add(this.txtIpFrom);
            this.Controls.Add(this.txtBPTo);
            this.Controls.Add(this.txtBPFrom);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "FrmProceed";
            this.Text = "Proceed Data To IO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBPFrom;
        private System.Windows.Forms.TextBox txtBPTo;
        private System.Windows.Forms.TextBox txtIpFrom;
        private System.Windows.Forms.TextBox txtIpTo;
        private System.Windows.Forms.TextBox txtTableName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnSave;
    }
}