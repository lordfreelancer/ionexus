﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IONexus
{
    public class TabWithGrid : TabPage
    {
        public DataGridView dgvnn;
        private string _frmTitle;
        public Size TabSize { get; set; }
        public TabWithGrid(string frmTitle)
        {
            _frmTitle = frmTitle;
        }

        public void Clear()
        {
            dgvnn.Rows.Clear();
            dgvnn.Columns.Clear();
        }

        public void Initialize()
        {
            this.Text = this._frmTitle;
            this.dgvnn = new DataGridView();
            base.Controls.Add(this.dgvnn);
            //this.dgvnn.SelectionMode = DataGridViewSelectionMode.CellSelect;
            //this.dgvnn.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
            //this.dgvnn.Size = this.tabsize;
            //this.dgvnn.RightToLeft = RightToLeft.No;
            //this.dgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            //this.dgvnn.RowHeadersWidth = 175;
            //this.dgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //this.dgvnn.RowTemplate.Height = 24;

            this.dgvnn.AllowUserToAddRows = false;
            this.dgvnn.AllowUserToDeleteRows = false;
            this.dgvnn.CausesValidation = false;
            this.dgvnn.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dgvnn.ColumnHeadersHeight = 40;
            //this.dgvnn.ContextMenuStrip = this.dgvMenu;
            this.dgvnn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvnn.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvnn.Location = new System.Drawing.Point(5, 5);
            this.dgvnn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvnn.RowHeadersWidth = 175;
            this.dgvnn.RowTemplate.Height = 40;
            this.dgvnn.Size = TabSize;
            //this.dgvnn.TabIndex = 0;
        }
    }
}
