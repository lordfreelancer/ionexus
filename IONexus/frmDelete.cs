﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IONexus
{
    public partial class frmDelete : Form
    {
        public int OrdinalNumber { get; set; }
        public int ActionType { get; set; }

        public frmDelete()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int ordinal = 0;
            int.TryParse(txtOrdinal.Text, out ordinal);
            this.ActionType = cboType.SelectedIndex;
            this.OrdinalNumber = ordinal;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
