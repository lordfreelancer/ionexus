﻿using CsvHelper;
using ExcelDataReader;
using FileHelpers;
using IONexus.Model;
using Matrix_Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace IONexus
{
    public partial class FrmMain : Form
    {
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);
        private const int WM_SETREDRAW = 11;

        public double pubValue = 0.51;
        public double priValue = 0.49;
        //public NexusData baseData = new NexusData();
        public NexusInput InputData { get; set; }
        public DataTable rawData = new DataTable();
        public NexusOutput OutputData { get; set; }

        public TabWithGrid TabAggregate { get; set; }

        public TabWithGrid TabIO { get; set; }

        public FrmMain()
        {
            InitializeComponent();
        }

        private string[,] LoadCsv(string filename)
        {
            // Get the file's text.
            string whole_file = System.IO.File.ReadAllText(filename);

            // Split into lines.
            whole_file = whole_file.Replace('\n', '\r');
            string[] lines = whole_file.Split(new char[] { '\r' },
                StringSplitOptions.RemoveEmptyEntries);

            // See how many rows and columns there are.
            int num_rows = lines.Length;
            int num_cols = lines[0].Split(',').Length;

            // Allocate the data array.
            string[,] values = new string[num_rows, num_cols];

            // Load the array.
            for (int r = 0; r < num_rows; r++)
            {
                string[] line_r = lines[r].Split(',');
                for (int c = 0; c < num_cols; c++)
                {
                    values[r, c] = line_r[c];
                }
            }

            // Return the values.
            return values;
        }
        private string[,] LoadCsv(List<dynamic> list)
        {
            if (list.Count > 0)
            {
                var num_rows = list.Count;
                var x = list[0] as IDictionary<string, object>;
                int num_cols = x.Keys.Count;
                string[,] values = new string[num_rows, num_cols];

                var r = 0;
                foreach (dynamic obj in list)
                {
                    var d = list[r] as IDictionary<string, object>;
                    var c = 0;
                    foreach (KeyValuePair<string, object> entry in d)
                    {
                        values[r, c] = entry.Value.ToString();
                        c++;
                        // do something with entry.Value or entry.Key
                    }
                    r++;
                }


                // Return the values.
                return values;
            }
            else
            {
                return null;
            }
        }

        private void PrintToGrid(string[,] values, DataGridView dgv)
        {
            int num_rows = values.GetUpperBound(0) + 1;
            int num_cols = values.GetUpperBound(1) + 1;
            dgv.Columns.Clear();
            for (int c = 0; c < num_cols; c++)
                dgv.Columns.Add(values[0, c], values[0, c]);

            for (int r = 1; r < num_rows; r++)
            {
                dgv.Rows.Add();
                for (int c = 0; c < num_cols; c++)
                {
                    dgv.Rows[r - 1].Cells[c].Value = values[r, c];
                    if (c > 0)
                    {
                        //DrawBackground(dgv, r - 1, c);
                    }
                }
            }
        }

        private void FillRowName(DataGridView dgv)
        {
            dgv.RowHeadersDefaultCellStyle.BackColor = Color.FromArgb(255, 252, 213, 180);
            if (dgv.RowCount < InputData.BasePriceData.NoRows)
            {
                //Initialize Rows.
                dgv.Rows.Clear();
                for (int i = 0; i < InputData.BasePriceData.NoRows; i++)
                {
                    dgv.Rows.Add();
                }
                for (int i = 0; i < InputData.PrimaryFactorData.NoRows; i++)
                {
                    dgv.Rows.Add();
                }
            }
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                dgv.Rows[i].HeaderCell.Value = i < InputData.BasePriceData.NoRows ?
                    InputData.basePriceColumns[i] : InputData.primaryFactorRows[i - InputData.BasePriceData.NoRows];

            }
        }

        private void FillColumnName(DataGridView dgv)
        {
            dgv.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#CCC0DA");
            if (dgv.ColumnCount < InputData.BasePriceData.NoCols)
            {
                //Initialize Rows.
                dgv.Columns.Clear();
                for (int i = 0; i < InputData.BasePriceData.NoCols; i++)
                {
                    dgv.Columns.Add(InputData.basePriceColumns[i], InputData.basePriceColumns[i]);
                    dgv.Columns[dgv.ColumnCount - 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    dgv.Columns[dgv.ColumnCount - 1].DefaultCellStyle.Format = "N2";
                    dgv.Columns[dgv.ColumnCount - 1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                //for (int i = 0; i < InputData.ImportData.NoCols; i++)
                //{
                //    dgv.Columns.Add(InputData.ImportColumns[i], InputData.ImportColumns[i]);
                //    dgv.Columns[dgv.ColumnCount - 1].DefaultCellStyle.Format = "N2";
                //    dgv.Columns[dgv.ColumnCount - 1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //}
                for (int i = 0; i < InputData.FinalDemandData.NoCols; i++)
                {
                    dgv.Columns.Add(InputData.finalDemandColumns[i], InputData.finalDemandColumns[i]);
                    dgv.Columns[dgv.ColumnCount - 1].DefaultCellStyle.Format = "N2";
                    dgv.Columns[dgv.ColumnCount - 1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
            }
        }

        private void FillBasePrice(DataGridView dgv)
        {
            for (int r = 0; r < InputData.BasePriceData.NoRows; r++)
            {
                for (int c = 0; c < InputData.BasePriceData.NoCols; c++)
                {
                    dgv.Rows[r].Cells[c].Value = InputData.BasePriceData[r, c];
                    DrawBackground(dgv, r, c, 1);
                }
            }
        }

        private void FillImport(DataGridView dgv)
        {
            int beginRow = InputData.BasePriceData.NoRows;
            for (int r = 0; r < InputData.ImportData.NoRows; r++)
            {
                for (int c = 0; c < InputData.ImportData.NoCols; c++)
                {
                    dgv.Rows[beginRow + r].Cells[c].Value = InputData.ImportData[r, c];
                    DrawBackground(dgv, beginRow + r, c, 2);
                }
            }
        }

        private void FillPrimary(DataGridView dgv)
        {
            int beginRow = InputData.BasePriceData.NoRows + InputData.ImportData.NoRows;
            for (int r = 0; r < InputData.PrimaryFactorData.NoRows; r++)
            {
                for (int c = 0; c < InputData.PrimaryFactorData.NoCols; c++)
                {
                    dgv.Rows[r + beginRow].Cells[c].Value = InputData.PrimaryFactorData[r, c];
                    DrawBackground(dgv, r + beginRow, c, 3);
                }
            }
        }

        private void FillFinalDemand(DataGridView dgv)
        {
            int beginColumn = InputData.BasePriceData.NoCols;
            for (int r = 0; r < InputData.FinalDemandData.NoRows; r++)
            {
                for (int c = 0; c < InputData.FinalDemandData.NoCols; c++)
                {
                    dgv.Rows[r].Cells[c + beginColumn].Value = InputData.BasePriceData[r, c];
                    DrawBackground(dgv, r, c + beginColumn, 4);
                }
            }
        }

        private void PrintToGrid(DataGridView dgv)
        {
            dgv.SuspendLayout();
            //dgv.SetStyle(ControlStyles.UserPaint, true);
            //dgv.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            //dgv.SetStyle(ControlStyles.DoubleBuffer, true);
            dgv.DoubleBuffered(true);
            SendMessage(dgv.Handle, WM_SETREDRAW, false, 0);
            TabMain.TabPages[0].Text = InputData.TableName;
            FillColumnName(dgv);
            FillRowName(dgv);
            FillBasePrice(dgv);
            FillImport(dgv);
            FillPrimary(dgv);
            FillFinalDemand(dgv);
            SendMessage(dgv.Handle, WM_SETREDRAW, true, 0);
            dgv.ResumeLayout();
            dgv.Refresh();
        }

        private void DrawBackground(DataGridView dgv, int r, int c, int type)
        {
            switch (type)
            {
                case 2:
                    dgv.Rows[r].Cells[c].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#B1A0C7");
                    break;
                case 3:
                    dgv.Rows[r].Cells[c].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#60497A");
                    break;
                case 4:
                    dgv.Rows[r].Cells[c].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#B8CCE4");
                    break;
                default:
                    dgv.Rows[r].Cells[c].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#CCC0DA");
                    break;
            }
        }

        Matrix_Lib.Matrix baseMatrix;
        Matrix_Lib.Matrix secondMatrix;
        private void bindGrid(string[,] values)
        {
        }

        private void ShowTotal(DataGridView dgv)
        {
            //if (dgv.Rows[dgv.Rows.Count-2].Cells[0].Value.ToString() != "Total")
            //{
            //    int r = dgv.Rows.Count-1;
            //    //dgv.Rows.Add();
            //    dgv.Rows[r].Cells[0].Value = "Total";
            //    for (int c = 1; c < dgv.ColumnCount; c++)
            //    {
            //        int index = c - 1;
            //        if (index < baseData.interColumns.Length)
            //            dgv.Rows[r].Cells[c].Value = baseData.DosmeticInterDemTable.ColSum(index) +
            //                baseData.ImportInterDemTable.ColSum(index) +
            //                baseData.PrimaryInterDemTable.ColSum(index);
            //        else
            //        {
            //            index = index - baseData.interColumns.Length;
            //            dgv.Rows[r].Cells[c].Value = baseData.DosmeticFinDemTable.ColSum(index) +
            //                baseData.ImportFinDemTable.ColSum(index) +
            //                baseData.PrimaryFinDemTable.ColSum(index);
            //        }
            //    }
            //}
            //if (dgv.Columns[dgv.Columns.Count - 1].Name != "Total Output")
            //{
            //    dgv.Columns.Add("Total Output", "Total Output");
            //    int c = dgv.Columns.Count - 1;
            //    for (int r = 0; r < dgv.RowCount - 1; r++)
            //    {
            //        int index = r;
            //        if (index < baseData.DosmeticInterDemTable.NoRows)
            //            dgv.Rows[r].Cells[c].Value = baseData.DosmeticInterDemTable.RowSum(index) +
            //                baseData.DosmeticFinDemTable.RowSum(index);
            //        else if (index < baseData.DosmeticInterDemTable.NoRows + baseData.ImportInterDemTable.NoRows)
            //        {
            //            index = index - baseData.DosmeticInterDemTable.NoRows;
            //            dgv.Rows[r].Cells[c].Value = baseData.ImportInterDemTable.RowSum(index) +
            //                baseData.ImportFinDemTable.RowSum(index);
            //        }
            //        else
            //        {
            //            index = index - baseData.DosmeticInterDemTable.NoRows - baseData.ImportInterDemTable.NoRows;
            //            dgv.Rows[r].Cells[c].Value = baseData.PrimaryInterDemTable.RowSum(index) +
            //                baseData.PrimaryFinDemTable.RowSum(index);
            //        }
            //    }
            //}
        }

        private void BindStep2Grid()
        {
        }
        private void BindStep3Grid()
        {
        }

        private void SaveToFile(NexusOutput outData)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Enter Filename to Save Nexus Modal Data";
            saveFileDialog.InitialDirectory = Environment.CurrentDirectory;
            saveFileDialog.Filter = "Nexus Modal Data files (*.nmd)|*.nmd";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                string fileName = saveFileDialog.FileName;
                try
                {
                    using (var ms = new MemoryStream())
                    {
                        binaryFormatter.Serialize(ms, outData);
                        byte[] bytes = ms.ToArray();

                        FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        fileStream.Write(bytes, 0, bytes.Length);
                        //binaryFormatter.Serialize(fileStream, str);
                        fileStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                }
            }
        }


        private void miLoadData_Click(object sender, EventArgs e)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            string path = "";
            bool flag = true;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Enter Filename to Load Data";
            openFileDialog.InitialDirectory = System.Environment.CurrentDirectory;
            openFileDialog.Filter = "Excel files |*.xls;*.xlsx;*.xlsm| Nexus Model Data (*.nmd)|*.nmd";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog.FileName;
                flag = true;
            }
            else
            {
                flag = false;
            }
            if (flag)
            {
                try
                {
                    using (var stream = File.Open(path, FileMode.Open, FileAccess.Read))
                    {
                        if (path.EndsWith(".nmd"))
                        {
                            var binForm = new BinaryFormatter();
                            NexusOutput outData = (NexusOutput)binForm.Deserialize(stream);
                            InputData = outData.BaseInput;
                            OutputData = outData;
                            PrintToGrid(dgvMain);
                            return;
                        }

                        // Auto-detect format, supports:
                        //  - Binary Excel files (2.0-2003 format; *.xls)
                        //  - OpenXml Excel files (2007 format; *.xlsx)
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            // 2. Use the AsDataSet extension method
                            var result = reader.AsDataSet();
                            // The result of each spreadsheet is in result.Tables
                            //inputData = NexusInput.Parse(result);
                            //PrintToGrid(inputData, dgvMain);
                            FrmRawData raw = new FrmRawData();
                            raw.dt = result.Tables[0];
                            if (raw.ShowDialog() == DialogResult.OK)
                            {
                                InputData = raw.Input;
                                PrintToGrid(dgvMain);
                                //Show Data to Tab.
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + " ", null, MessageBoxButtons.OK);
                }
            }
        }

        private void miSave1_Click(object sender, EventArgs e)
        {
            if (OutputData == null)
            {
                OutputData = new NexusOutput();
                OutputData.BaseInput = InputData;
            }
            SaveToFile(OutputData);
            MessageBox.Show("Save Completed.");
        }

        private void OptionsTool_Click(object sender, EventArgs e)
        {
            Options frmOption = new Options();
            frmOption.priValue = priValue;
            frmOption.pubValue = pubValue;
            if (frmOption.ShowDialog() == DialogResult.OK)
            {
                pubValue = frmOption.pubValue;
                priValue = frmOption.priValue;
            }
        }

        private void tsmOtherOptions_Click(object sender, EventArgs e)
        {
            Preferences frmOption = new Preferences();
            if (frmOption.ShowDialog() == DialogResult.OK)
            {
            }
        }

        private void btnEditFormula_Click(object sender, EventArgs e)
        {
        }

        private void miAggregate_Click(object sender, EventArgs e)
        {
            frmAgg frm = new frmAgg();
            if (OutputData == null)
            {
                OutputData = new NexusOutput();
                OutputData.BaseInput = InputData;
                NexusSummary step1 = new NexusSummary();
                step1.SetParentSector(InputData);
                OutputData.SummarySteps.Add(step1);
                OutputData.CurrentStep = 0;
            }
            else
            {
                NexusSummary step1 = new NexusSummary();
                step1.SetParentSector(InputData);
                OutputData.SummarySteps.Add(step1);
                OutputData.CurrentStep += 1;
            }
            frm.NexusOut = OutputData;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                bool bNeedToCalculate = false;
                int length = OutputData.SummarySteps.Count;
                //OutputData = frm.NexusOut;
                //Add Aggregate Tab. And Calculate IO Model Base on That
                if (OutputData.SummarySteps[length - 1].AggregateList.Count > 0)
                {
                    bNeedToCalculate = true;
                    TabWithGrid nextStep = new TabWithGrid("Step " + (OutputData.CurrentStep + 1).ToString());
                    nextStep.TabSize = TabPage1.Size;
                    nextStep.Initialize();
                    TabMain.TabPages.Insert(1, nextStep);
                    OutputData.DoAggregate();
                    //Reload Aggreagate Tab.
                    nextStep.Clear();
                    NexusSummary summary = OutputData.SummarySteps[OutputData.CurrentStep];
                    if (summary != null)
                    {
                        PrintToGrid(summary, nextStep.dgvnn);
                    }
                }
                else
                {
                    //Remove Step

                }
                //Recalculate IO Tab
                //if (bNeedToCalculate)
                //{
                //    if (TabIO == null)
                //    {
                //        TabIO = new TabWithGrid("IOFinal");
                //        TabIO.TabSize = TabPage1.Size;
                //        TabIO.Initialize();
                //        TabMain.TabPages.Add(TabIO);
                //    }
                //    //Reload Aggreagate Tab.
                //    TabIO.Clear();
                //    NexusSummary summary = OutputData.SummarySteps[OutputData.CurrentStep];
                //    if (summary != null)
                //    {
                //        PrintToGrid(summary, TabIO.dgvnn);
                //    }

                //}
            }
        }

        private void PrintToGrid(NexusSummary summary, DataGridView dgv)
        {
            int num_rows = summary.FinalData.NoRows;
            int num_cols = summary.FinalData.NoCols;
            dgv.Columns.Clear();
            for (int c = 0; c < num_cols; c++)
                dgv.Columns.Add(summary.Columns[c].Name, summary.Columns[c].Name);

            for (int r = 1; r < num_rows; r++)
            {
                dgv.Rows.Add();
                for (int c = 0; c < num_cols; c++)
                {
                    dgv.Rows[r - 1].Cells[c].Value = summary.FinalData[r, c];
                    if (c > 0)
                    {
                        //DrawBackground(dgv, r - 1, c);
                    }
                }
            }
        }

        private void showChartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChart.Screen.CustomChart CustomChart = new ShowChart.Screen.CustomChart();

            CustomChart.ShowDialog();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }

        private void configToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowChart.Screen.ScreenIOConfig ScreenIOConfig = new ShowChart.Screen.ScreenIOConfig();

            ScreenIOConfig.ShowDialog();
        }
    }
}