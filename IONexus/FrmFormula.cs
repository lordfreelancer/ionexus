﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IONexus
{
    public partial class FrmFormula : Form
    {
        private NexusData baseData;
        public FrmFormula(NexusData _base)
        {
            baseData = _base;
            InitializeComponent();
            initFormula();
        }

        private void initFormula()
        {
            string[,] values = baseData.Print();
            int num_rows = values.GetUpperBound(0) + 1;
            int num_cols = values.GetUpperBound(1) + 1;
            dgvStep1.Columns.Clear();
            for (int c = 0; c < num_cols; c++)
                dgvStep1.Columns.Add(values[0, c], values[0, c]);

            for (int r = 1; r < num_rows; r++)
            {
                dgvStep1.Rows.Add();
                for (int c = 0; c < num_cols; c++)
                {
                    if (c==0)
                        dgvStep1.Rows[r-1].Cells[c].Value = values[r, 0];
                    else
                        dgvStep1.Rows[r-1].Cells[c].Value = "*1.0";

                    if (c > 0)
                    {
                        DrawBackground(dgvStep1, r - 1, c);
                    }
                }
            }
        }

        private void reloadFormula()
        {
            string[,] values = baseData.formulaChange.Print();
            int num_rows = values.GetUpperBound(0) + 1;
            int num_cols = values.GetUpperBound(1) + 1;
            dgvStep1.Columns.Clear();
            dgvStep1.Columns.Add("", "");
            for (int c = 0; c < num_cols; c++)
            {
                string column = c < baseData.interColumns.Length ? baseData.interColumns[c] : baseData.findemColumns[c - baseData.interColumns.Length];
                dgvStep1.Columns.Add(column, column);
            }

            for (int r = 0; r < num_rows; r++)
            {
                dgvStep1.Rows.Add();
                for (int c = 0; c < num_cols+1; c++)
                {
                    if (c == 0) {
                        dgvStep1.Rows[r].Cells[c].Value = baseData.GetRowName(r);
                    }
                    else
                        dgvStep1.Rows[r].Cells[c].Value = values[r, c - 1];

                    if (c > 0)
                    {
                        DrawBackground(dgvStep1, r, c);
                    }
                }
            }
        }

        private void DrawBackground(DataGridView dgv, int r, int c)
        {
            if (c - 1 < baseData.interColumns.Length)
            {
                if (dgv.Rows[r].Cells[0].Value.ToString().StartsWith("D"))
                {
                    dgv.Rows[r].Cells[c].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF8C00");
                }
                else if (dgv.Rows[r].Cells[0].Value.ToString().StartsWith("I"))
                {
                    dgv.Rows[r].Cells[c].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#79DB75");
                }
                else
                {
                    dgv.Rows[r].Cells[c].Style.BackColor = Color.LightCyan;
                }
            }
            else
            {
                if (dgv.Rows[r].Cells[0].Value.ToString().StartsWith("D"))
                {
                    dgv.Rows[r].Cells[c].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#DEA2ED");
                }
                else if (dgv.Rows[r].Cells[0].Value.ToString().StartsWith("I"))
                {
                    dgv.Rows[r].Cells[c].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#0046FF");
                }
                else
                {
                    dgv.Rows[r].Cells[c].Style.BackColor = Color.LightCyan;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //Save Formula to Formula Model
            int rows = dgvStep1.RowCount - 1, cols = dgvStep1.ColumnCount - 1;
            string[,] values = new string[rows, cols];
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    values[r, c] = dgvStep1.Rows[r].Cells[c+1].Value.ToString();
                }
            }
            baseData.formulaChange = NexusFormula.Parse(values, baseData);
            string path = "formula.csv";
            using (TextWriter writer = new StreamWriter(path))
            {
                var csv = new CsvWriter(writer);
                //csv.Configuration. = Encoding.UTF8;
                for (int r = 0; r < rows; r++)
                {
                    for (int c = 0; c < cols; c++)
                    {
                        var field = values[r, c];
                        csv.WriteField(field);
                    }
                    csv.NextRecord();
                }
                writer.Close();
            }
            MessageBox.Show("Save Completed.");
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            string path = "";
            Boolean flag = false;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Enter Filename to Load Formular Data";
            openFileDialog.InitialDirectory = System.Environment.CurrentDirectory;
            openFileDialog.Filter = "CSV files (*.csv)|*.csv";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog.FileName;
                flag = true;
            }
            else
            {
                flag = false;
            }
            if (flag)
            {
                try
                {
                    using (TextReader reader = File.OpenText(path))
                    {
                        //TextReader reader = File.OpenText(path);
                        CsvReader csvFile = new CsvReader(reader);
                        csvFile.Configuration.HasHeaderRecord = false;
                        csvFile.Configuration.IgnoreBlankLines = true;
                        //csvFile.Read();
                        var records = csvFile.GetRecords<dynamic>().ToList();
                        string[,] values = LoadCsv(records);
                        baseData.formulaChange = NexusFormula.Parse(values, baseData);
                        reloadFormula();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + " ", null, MessageBoxButtons.OK);
                }
            }
        }

        private string[,] LoadCsv(List<dynamic> list)
        {
            if (list.Count > 0)
            {
                var num_rows = list.Count;
                var x = list[0] as IDictionary<string, object>;
                int num_cols = x.Keys.Count;
                string[,] values = new string[num_rows, num_cols];

                var r = 0;
                foreach (dynamic obj in list)
                {
                    var d = list[r] as IDictionary<string, object>;
                    var c = 0;
                    foreach (KeyValuePair<string, object> entry in d)
                    {
                        values[r, c] = entry.Value.ToString();
                        c++;
                        // do something with entry.Value or entry.Key
                    }
                    r++;
                }


                // Return the values.
                return values;
            }
            else
            {
                return null;
            }
        }
    }
}
