﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IONexus
{
    public partial class FrmProceed : Form
    {
        public string bPFrom { get; set; }
        public string bPTo { get; set; }
        public string ipFrom { get; set; }
        public string ipTo { get; set; }

        public string TableName { get; set; }

        public FrmProceed()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bPFrom = txtBPFrom.Text;
            bPTo = txtBPTo.Text;
            ipFrom = txtIpFrom.Text;
            ipTo = txtIpTo.Text;
            TableName = txtTableName.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
