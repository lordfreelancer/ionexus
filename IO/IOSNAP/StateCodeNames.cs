using System;
using System.Collections;
using System.Collections.Generic;

namespace IOSNAP
{
	[Serializable]
	public class StateCodeNames : ICloneable
	{
		public string[] StateCodes;

		public ArrayList StatesFullNames;

		private Dictionary<string, string> StatesList;

		public object clone()
		{
			return new StateCodeNames();
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in clone
			return this.clone();
		}

		public StateCodeNames()
		{
			this.StateCodes = new string[51]
			{
				"AL",
				"AK",
				"AZ",
				"AR",
				"CA",
				"CO",
				"CT",
				"DC",
				"DE",
				"FL",
				"GA",
				"HI",
				"ID",
				"IL",
				"IN",
				"IA",
				"KS",
				"KY",
				"LA",
				"ME",
				"MD",
				"MA",
				"MI",
				"MN",
				"MS",
				"MO",
				"MT",
				"NE",
				"NV",
				"NH",
				"NJ",
				"NY",
				"NC",
				"ND",
				"NM",
				"OH",
				"OK",
				"OR",
				"PA",
				"RI",
				"SC",
				"SD",
				"TN",
				"TX",
				"UT",
				"VT",
				"VA",
				"WA",
				"WV",
				"WI",
				"WY"
			};
			this.StatesFullNames = new ArrayList();
			this.StatesList = new Dictionary<string, string>();
			this.StatesList.Add("AL", "Alabama");
			this.StatesList.Add("AK", "Alaska");
			this.StatesList.Add("AZ", "Arizona");
			this.StatesList.Add("AR", "Arkansas");
			this.StatesList.Add("CA", "California");
			this.StatesList.Add("CO", "Colorado");
			this.StatesList.Add("CT", "Connecticut");
			this.StatesList.Add("DC", "District of Columbia");
			this.StatesList.Add("DE", "Delaware");
			this.StatesList.Add("FL", "Florida");
			this.StatesList.Add("GA", "Georgia");
			this.StatesList.Add("HI", "Hawaii");
			this.StatesList.Add("ID", "Idaho");
			this.StatesList.Add("IL", "Illinois");
			this.StatesList.Add("IN", "Indiana");
			this.StatesList.Add("IA", "Iowa");
			this.StatesList.Add("KS", "Kansas");
			this.StatesList.Add("KY", "Kentucky");
			this.StatesList.Add("LA", "Louisiana");
			this.StatesList.Add("ME", "Maine");
			this.StatesList.Add("MD", "Maryland");
			this.StatesList.Add("MA", "Massachusetts");
			this.StatesList.Add("MI", "Michigan");
			this.StatesList.Add("MN", "Minnesota");
			this.StatesList.Add("MS", "Mississippi");
			this.StatesList.Add("MO", "Missouri");
			this.StatesList.Add("MT", "Montana");
			this.StatesList.Add("NE", "Nebraska");
			this.StatesList.Add("NV", "Nevada");
			this.StatesList.Add("NH", "New Hampshire");
			this.StatesList.Add("NJ", "New Jersey");
			this.StatesList.Add("NM", "New Mexico");
			this.StatesList.Add("NY", "New York");
			this.StatesList.Add("NC", "North Carolina");
			this.StatesList.Add("ND", "North Dakota");
			this.StatesList.Add("OH", "Ohio");
			this.StatesList.Add("OK", "Oklahoma");
			this.StatesList.Add("OR", "Oregon");
			this.StatesList.Add("PA", "Pennsylvania");
			this.StatesList.Add("RI", "Rhode Island");
			this.StatesList.Add("SC", "South Carolina");
			this.StatesList.Add("SD", "South Dakota");
			this.StatesList.Add("TN", "Tennessee");
			this.StatesList.Add("TX", "Texas");
			this.StatesList.Add("UT", "Utah");
			this.StatesList.Add("VT", "Vermont");
			this.StatesList.Add("VA", "Virginia");
			this.StatesList.Add("WA", "Washington");
			this.StatesList.Add("WI", "Wisconsin");
			this.StatesList.Add("WY", "Wyoming");
			this.StatesList.Add("WV", "West Virginia");
			string[] stateCodes = this.StateCodes;
			foreach (string st in stateCodes)
			{
				this.StatesFullNames.Add(this.getStateName(st));
			}
		}

		public string getStateName(string st)
		{
			string result = "";
			result = ((!this.StatesList.TryGetValue(st, out result)) ? ("XXXX_" + st + "_X") : this.StatesList[st]);
			return result;
		}

		public bool addState(string stid, string stname)
		{
			bool flag = false;
			return !flag;
		}

		public string getStateID(string st)
		{
			string result = "xx";
			string[] stateCodes = this.StateCodes;
			foreach (string text in stateCodes)
			{
				if (this.StatesList.ContainsKey(text))
				{
					string text2 = this.StatesList[text];
					if (text2.Equals(st))
					{
						result = text;
					}
				}
			}
			return result;
		}
	}
}
