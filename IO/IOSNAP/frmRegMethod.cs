using IOSNAP.My;
using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class frmRegMethod : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cmdOk")]
		private Button _cmdOk;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cmdCancel")]
		private Button _cmdCancel;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RegionGroupBox")]
		private GroupBox _RegionGroupBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RegionComboBox")]
		private ComboBox _RegionComboBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox2")]
		private GroupBox _GroupBox2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("buttonSelectRegion")]
		private Button _buttonSelectRegion;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("comboBoxStateSelection")]
		private ComboBox _comboBoxStateSelection;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbSelRegion")]
		private RadioButton _rbSelRegion;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbStateSelection")]
		private RadioButton _rbStateSelection;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbUserDefined")]
		private RadioButton _rbUserDefined;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbUserDefMain")]
		private RadioButton _rbUserDefMain;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbSelRegMain")]
		private RadioButton _rbSelRegMain;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("tboxRegionName")]
		private TextBox _tboxRegionName;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ToolTip1")]
		private ToolTip _ToolTip1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbVA")]
		private RadioButton _rbVA;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbIncome2")]
		private RadioButton _rbIncome2;

		public const int DIGITS = 7;

		public BEAData bdat;

		public SummaryDat worksum;

		private string PSelect;

		private string UserSelectedRegion;

		private string multiStateRegion;

		public StateData regiondat;

		private bool isStateRegionCommitted;

		private bool isRegionCommitted;

		private object isStateFirstTime;

		private Vector Regionalizer;

		private Vector EC;

		private Vector GOS;

		private Vector T;

		private Vector Employment;

		internal virtual Button cmdOk
		{
			[CompilerGenerated]
			get
			{
				return this._cmdOk;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.cmdOk_Click;
				Button cmdOk = this._cmdOk;
				if (cmdOk != null)
				{
					cmdOk.Click -= value2;
				}
				this._cmdOk = value;
				cmdOk = this._cmdOk;
				if (cmdOk != null)
				{
					cmdOk.Click += value2;
				}
			}
		}

		internal virtual Button cmdCancel
		{
			[CompilerGenerated]
			get
			{
				return this._cmdCancel;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.cmdCancel_Click;
				Button cmdCancel = this._cmdCancel;
				if (cmdCancel != null)
				{
					cmdCancel.Click -= value2;
				}
				this._cmdCancel = value;
				cmdCancel = this._cmdCancel;
				if (cmdCancel != null)
				{
					cmdCancel.Click += value2;
				}
			}
		}

		internal virtual GroupBox RegionGroupBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ComboBox RegionComboBox
		{
			[CompilerGenerated]
			get
			{
				return this._RegionComboBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.RegionComboBox_DropDown;
				EventHandler value3 = this.RegionComboBox_SelectedIndexChanged;
				ComboBox regionComboBox = this._RegionComboBox;
				if (regionComboBox != null)
				{
					regionComboBox.DropDown -= value2;
					regionComboBox.SelectedIndexChanged -= value3;
				}
				this._RegionComboBox = value;
				regionComboBox = this._RegionComboBox;
				if (regionComboBox != null)
				{
					regionComboBox.DropDown += value2;
					regionComboBox.SelectedIndexChanged += value3;
				}
			}
		}

		internal virtual GroupBox GroupBox2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button buttonSelectRegion
		{
			[CompilerGenerated]
			get
			{
				return this._buttonSelectRegion;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.buttonSelectRegion_Click;
				Button buttonSelectRegion = this._buttonSelectRegion;
				if (buttonSelectRegion != null)
				{
					buttonSelectRegion.Click -= value2;
				}
				this._buttonSelectRegion = value;
				buttonSelectRegion = this._buttonSelectRegion;
				if (buttonSelectRegion != null)
				{
					buttonSelectRegion.Click += value2;
				}
			}
		}

		internal virtual ComboBox comboBoxStateSelection
		{
			[CompilerGenerated]
			get
			{
				return this._comboBoxStateSelection;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.comboBoxStateSelection_DropDown;
				EventHandler value3 = this.comboBoxStateSelection_SelectedIndexChanged;
				ComboBox comboBoxStateSelection = this._comboBoxStateSelection;
				if (comboBoxStateSelection != null)
				{
					comboBoxStateSelection.DropDown -= value2;
					comboBoxStateSelection.SelectedIndexChanged -= value3;
				}
				this._comboBoxStateSelection = value;
				comboBoxStateSelection = this._comboBoxStateSelection;
				if (comboBoxStateSelection != null)
				{
					comboBoxStateSelection.DropDown += value2;
					comboBoxStateSelection.SelectedIndexChanged += value3;
				}
			}
		}

		internal virtual RadioButton rbSelRegion
		{
			[CompilerGenerated]
			get
			{
				return this._rbSelRegion;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.rbSelRegion_CheckedChanged;
				RadioButton rbSelRegion = this._rbSelRegion;
				if (rbSelRegion != null)
				{
					rbSelRegion.CheckedChanged -= value2;
				}
				this._rbSelRegion = value;
				rbSelRegion = this._rbSelRegion;
				if (rbSelRegion != null)
				{
					rbSelRegion.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton rbStateSelection
		{
			[CompilerGenerated]
			get
			{
				return this._rbStateSelection;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.rbStateSelection_CheckedChanged;
				RadioButton rbStateSelection = this._rbStateSelection;
				if (rbStateSelection != null)
				{
					rbStateSelection.CheckedChanged -= value2;
				}
				this._rbStateSelection = value;
				rbStateSelection = this._rbStateSelection;
				if (rbStateSelection != null)
				{
					rbStateSelection.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton rbUserDefined
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rbUserDefMain
		{
			[CompilerGenerated]
			get
			{
				return this._rbUserDefMain;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.rbUDef_CheckedChanged;
				RadioButton rbUserDefMain = this._rbUserDefMain;
				if (rbUserDefMain != null)
				{
					rbUserDefMain.CheckedChanged -= value2;
				}
				this._rbUserDefMain = value;
				rbUserDefMain = this._rbUserDefMain;
				if (rbUserDefMain != null)
				{
					rbUserDefMain.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton rbSelRegMain
		{
			[CompilerGenerated]
			get
			{
				return this._rbSelRegMain;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.selRegMain_CheckedChanged;
				RadioButton rbSelRegMain = this._rbSelRegMain;
				if (rbSelRegMain != null)
				{
					rbSelRegMain.CheckedChanged -= value2;
				}
				this._rbSelRegMain = value;
				rbSelRegMain = this._rbSelRegMain;
				if (rbSelRegMain != null)
				{
					rbSelRegMain.CheckedChanged += value2;
				}
			}
		}

		internal virtual TextBox tboxRegionName
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label Label1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label Label2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ToolTip ToolTip1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rbVA
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rbIncome2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		public frmRegMethod()
		{
			base.Load += this.frmRegMethod_Load;
			this.PSelect = "Please Select ...";
			this.UserSelectedRegion = "User Supplied Data";
			this.multiStateRegion = "Multiple State/Region Selection";
			this.isStateRegionCommitted = false;
			this.isRegionCommitted = false;
			this.isStateFirstTime = true;
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			this.rbIncome2 = new RadioButton();
			this.rbVA = new RadioButton();
			this.Label2 = new Label();
			this.cmdOk = new Button();
			this.cmdCancel = new Button();
			this.RegionGroupBox = new GroupBox();
			this.tboxRegionName = new TextBox();
			this.rbUserDefMain = new RadioButton();
			this.rbSelRegMain = new RadioButton();
			this.Label1 = new Label();
			this.GroupBox2 = new GroupBox();
			this.buttonSelectRegion = new Button();
			this.comboBoxStateSelection = new ComboBox();
			this.rbSelRegion = new RadioButton();
			this.rbStateSelection = new RadioButton();
			this.rbUserDefined = new RadioButton();
			this.RegionComboBox = new ComboBox();
			this.ToolTip1 = new ToolTip(this.components);
			GroupBox groupBox = new GroupBox();
			groupBox.SuspendLayout();
			this.RegionGroupBox.SuspendLayout();
			this.GroupBox2.SuspendLayout();
			base.SuspendLayout();
			groupBox.Controls.Add(this.rbIncome2);
			groupBox.Controls.Add(this.rbVA);
			groupBox.Controls.Add(this.Label2);
			groupBox.FlatStyle = FlatStyle.Popup;
			groupBox.Font = new Font("Calibri", 11f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			groupBox.Location = new Point(654, 102);
			groupBox.Margin = new Padding(4, 6, 4, 6);
			groupBox.Name = "gbMethod";
			groupBox.Padding = new Padding(4, 6, 4, 6);
			groupBox.Size = new Size(300, 256);
			groupBox.TabIndex = 8;
			groupBox.TabStop = false;
			groupBox.Text = "Regionalization method";
			this.rbIncome2.AutoSize = true;
			this.rbIncome2.Font = new Font("Verdana", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.rbIncome2.Location = new Point(56, 173);
			this.rbIncome2.Name = "rbIncome2";
			this.rbIncome2.Size = new Size(74, 18);
			this.rbIncome2.TabIndex = 6;
			this.rbIncome2.TabStop = true;
			this.rbIncome2.Text = "Income";
			this.rbIncome2.UseVisualStyleBackColor = true;
			this.rbVA.AutoSize = true;
			this.rbVA.Checked = true;
			this.rbVA.Font = new Font("Verdana", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.rbVA.Location = new Point(56, 110);
			this.rbVA.Name = "rbVA";
			this.rbVA.Size = new Size(107, 18);
			this.rbVA.TabIndex = 5;
			this.rbVA.TabStop = true;
			this.rbVA.Text = "Value Added";
			this.rbVA.UseVisualStyleBackColor = true;
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Calibri", 9f, FontStyle.Bold | FontStyle.Italic | FontStyle.Underline, GraphicsUnit.Point, 0);
			this.Label2.Location = new Point(20, 47);
			this.Label2.Name = "Label2";
			this.Label2.Size = new Size(154, 14);
			this.Label2.TabIndex = 4;
			this.Label2.Text = "Data regionalization based on";
			this.cmdOk.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cmdOk.Location = new Point(817, 387);
			this.cmdOk.Margin = new Padding(4, 6, 4, 6);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Size = new Size(112, 39);
			this.cmdOk.TabIndex = 5;
			this.cmdOk.Text = "OK";
			this.cmdOk.UseVisualStyleBackColor = true;
			this.cmdCancel.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cmdCancel.Location = new Point(654, 387);
			this.cmdCancel.Margin = new Padding(4, 6, 4, 6);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new Size(112, 39);
			this.cmdCancel.TabIndex = 6;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.UseVisualStyleBackColor = true;
			this.RegionGroupBox.Controls.Add(this.tboxRegionName);
			this.RegionGroupBox.Controls.Add(this.rbUserDefMain);
			this.RegionGroupBox.Controls.Add(this.rbSelRegMain);
			this.RegionGroupBox.Controls.Add(this.Label1);
			this.RegionGroupBox.Controls.Add(this.GroupBox2);
			this.RegionGroupBox.Controls.Add(this.RegionComboBox);
			this.RegionGroupBox.Font = new Font("Calibri", 11f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.RegionGroupBox.Location = new Point(28, 29);
			this.RegionGroupBox.Name = "RegionGroupBox";
			this.RegionGroupBox.Size = new Size(598, 454);
			this.RegionGroupBox.TabIndex = 15;
			this.RegionGroupBox.TabStop = false;
			this.RegionGroupBox.Text = "Region selection";
			this.tboxRegionName.Font = new Font("Calibri", 11f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.tboxRegionName.Location = new Point(150, 409);
			this.tboxRegionName.Name = "tboxRegionName";
			this.tboxRegionName.Size = new Size(432, 25);
			this.tboxRegionName.TabIndex = 17;
			this.rbUserDefMain.AutoSize = true;
			this.rbUserDefMain.Font = new Font("Calibri", 11f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.rbUserDefMain.Location = new Point(28, 132);
			this.rbUserDefMain.Name = "rbUserDefMain";
			this.rbUserDefMain.Size = new Size(106, 22);
			this.rbUserDefMain.TabIndex = 16;
			this.rbUserDefMain.Text = "User defined";
			this.rbUserDefMain.UseVisualStyleBackColor = true;
			this.rbSelRegMain.AutoSize = true;
			this.rbSelRegMain.Checked = true;
			this.rbSelRegMain.Font = new Font("Calibri", 11f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.rbSelRegMain.Location = new Point(28, 32);
			this.rbSelRegMain.Name = "rbSelRegMain";
			this.rbSelRegMain.Size = new Size(107, 22);
			this.rbSelRegMain.TabIndex = 15;
			this.rbSelRegMain.TabStop = true;
			this.rbSelRegMain.Text = "Select region";
			this.rbSelRegMain.UseVisualStyleBackColor = true;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Calibri", 11f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label1.Location = new Point(6, 409);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(89, 18);
			this.Label1.TabIndex = 18;
			this.Label1.Text = "Region name";
			this.GroupBox2.Controls.Add(this.buttonSelectRegion);
			this.GroupBox2.Controls.Add(this.comboBoxStateSelection);
			this.GroupBox2.Controls.Add(this.rbSelRegion);
			this.GroupBox2.Controls.Add(this.rbStateSelection);
			this.GroupBox2.Location = new Point(41, 165);
			this.GroupBox2.Name = "GroupBox2";
			this.GroupBox2.Size = new Size(541, 221);
			this.GroupBox2.TabIndex = 16;
			this.GroupBox2.TabStop = false;
			this.GroupBox2.Text = "Region Template";
			this.buttonSelectRegion.Font = new Font("Calibri", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.buttonSelectRegion.Location = new Point(208, 132);
			this.buttonSelectRegion.Name = "buttonSelectRegion";
			this.buttonSelectRegion.Size = new Size(167, 35);
			this.buttonSelectRegion.TabIndex = 5;
			this.buttonSelectRegion.Text = "Select Region";
			this.buttonSelectRegion.UseVisualStyleBackColor = true;
			this.comboBoxStateSelection.Font = new Font("Calibri", 11f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.comboBoxStateSelection.FormattingEnabled = true;
			this.comboBoxStateSelection.Location = new Point(192, 73);
			this.comboBoxStateSelection.Name = "comboBoxStateSelection";
			this.comboBoxStateSelection.Size = new Size(305, 26);
			this.comboBoxStateSelection.TabIndex = 4;
			this.rbSelRegion.AutoSize = true;
			this.rbSelRegion.Location = new Point(21, 136);
			this.rbSelRegion.Name = "rbSelRegion";
			this.rbSelRegion.Size = new Size(109, 22);
			this.rbSelRegion.TabIndex = 2;
			this.rbSelRegion.Text = "Region based";
			this.rbSelRegion.UseVisualStyleBackColor = true;
			this.rbStateSelection.AutoSize = true;
			this.rbStateSelection.Checked = true;
			this.rbStateSelection.Location = new Point(21, 73);
			this.rbStateSelection.Name = "rbStateSelection";
			this.rbStateSelection.Size = new Size(98, 22);
			this.rbStateSelection.TabIndex = 1;
			this.rbStateSelection.TabStop = true;
			this.rbStateSelection.Text = "State based";
			this.rbStateSelection.UseVisualStyleBackColor = true;
			this.rbUserDefined.AutoSize = true;
			this.rbUserDefined.Enabled = false;
			this.rbUserDefined.Location = new Point(663, 396);
			this.rbUserDefined.Name = "rbUserDefined";
			this.rbUserDefined.Size = new Size(94, 18);
			this.rbUserDefined.TabIndex = 0;
			this.rbUserDefined.Text = "User defined";
			this.rbUserDefined.UseVisualStyleBackColor = true;
			this.RegionComboBox.Font = new Font("Calibri", 11f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.RegionComboBox.FormattingEnabled = true;
			this.RegionComboBox.Location = new Point(78, 74);
			this.RegionComboBox.Name = "RegionComboBox";
			this.RegionComboBox.Size = new Size(460, 26);
			this.RegionComboBox.TabIndex = 14;
			this.ToolTip1.Tag = "aaaa";
			this.ToolTip1.ToolTipTitle = "aaaaab";
			base.AutoScaleDimensions = new SizeF(6f, 14f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(1003, 531);
			base.Controls.Add(this.RegionGroupBox);
			base.Controls.Add(groupBox);
			base.Controls.Add(this.cmdCancel);
			base.Controls.Add(this.cmdOk);
			base.Controls.Add(this.rbUserDefined);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Margin = new Padding(4, 6, 4, 6);
			base.Name = "frmRegMethod";
			this.Text = "Regionalization: Method Selection";
			groupBox.ResumeLayout(false);
			groupBox.PerformLayout();
			this.RegionGroupBox.ResumeLayout(false);
			this.RegionGroupBox.PerformLayout();
			this.GroupBox2.ResumeLayout(false);
			this.GroupBox2.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private void frmRegMethod_Load(object sender, EventArgs e)
		{
			this.setMainRegionSelectionDropList();
			this.buttonSelectRegion.Enabled = false;
			this.comboBoxStateSelection.Enabled = false;
			this.rbUserDefined.Checked = false;
			this.rbSelRegion.Checked = false;
			this.rbStateSelection.Checked = true;
			this.rbIncome2.Checked = true;
			this.rbVA.Checked = false;
			this.setUdefStateSelection();
			RegionalData regStateData = this.bdat.RegStateData;
			string text = "AL";
			this.regiondat = new StateData("Region", regStateData.GetState(ref text).Employment_base.Count);
			this.regiondat.StateSetup(ref this.bdat.NTables.FTE_Ratio);
			this.tboxRegionName.Text = "";
			this.rbSelRegion.Enabled = false;
			this.rbStateSelection.Enabled = false;
			this.rbUserDefined.Enabled = false;
			this.comboBoxStateSelection.Enabled = false;
			this.buttonSelectRegion.Enabled = false;
			this.rbSelRegMain.Checked = true;
			this.rbUserDefMain.Checked = false;
		}

		private void setMainRegionSelectionDropList()
		{
			this.RegionComboBox.Items.Clear();
			this.RegionComboBox.Items.Add(this.multiStateRegion);
			StateCodeNames stateCodeNames = new StateCodeNames();
			checked
			{
				int num = stateCodeNames.StatesFullNames.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.RegionComboBox.Items.Add(RuntimeHelpers.GetObjectValue(stateCodeNames.StatesFullNames[i]));
				}
				this.RegionComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
				this.RegionComboBox.Items.Insert(0, this.PSelect);
				this.RegionComboBox.SelectedItem = this.PSelect;
			}
		}

		private void setUdefStateSelection()
		{
			StateCodeNames stateCodes = this.bdat.stateCodes;
			this.comboBoxStateSelection.Items.Clear();
			checked
			{
				int num = stateCodes.StatesFullNames.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.comboBoxStateSelection.Items.Add(RuntimeHelpers.GetObjectValue(stateCodes.StatesFullNames[i]));
				}
				this.comboBoxStateSelection.DropDownStyle = ComboBoxStyle.DropDownList;
				this.comboBoxStateSelection.Items.Insert(0, this.PSelect);
				this.comboBoxStateSelection.SelectedItem = this.PSelect;
			}
		}

		private void RegionComboBox_DropDown(object sender, EventArgs e)
		{
			this.RegionComboBox.Items.Remove(this.PSelect);
		}

		private void RegionComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.tboxRegionName.Text = Conversions.ToString(this.RegionComboBox.SelectedItem);
		}

		private void cmdCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void cmdOk_Click(object sender, EventArgs e)
		{
			string text;
			if (this.rbUserDefMain.Checked && (this.rbStateSelection.Checked & this.isStateRegionCommitted))
			{
				bool flag = false;
				BEAData bEAData = this.bdat;
				RegionalData regStateData = bEAData.RegStateData;
				text = "AL";
				StateData stateData = new StateData("Multistate Region", regStateData.GetState(ref text).nrec_base);
				stateData.StateSetup(ref bEAData.NTables.FTE_Ratio);
				stateData.setEmptyState();
				MyProject.Forms.frmMain.AL_cboxChecked = false;
				MyProject.Forms.frmMain.AK_cboxChecked = false;
				MyProject.Forms.frmMain.AR_cboxChecked = false;
				MyProject.Forms.frmMain.AZ_cboxChecked = false;
				MyProject.Forms.frmMain.CA_cboxChecked = false;
				MyProject.Forms.frmMain.CO_cboxChecked = false;
				MyProject.Forms.frmMain.CT_cboxChecked = false;
				MyProject.Forms.frmMain.DC_cboxChecked = false;
				MyProject.Forms.frmMain.DE_cboxChecked = false;
				MyProject.Forms.frmMain.FL_cboxChecked = false;
				MyProject.Forms.frmMain.GA_cboxChecked = false;
				MyProject.Forms.frmMain.IA_cboxChecked = false;
				MyProject.Forms.frmMain.ID_cboxChecked = false;
				MyProject.Forms.frmMain.IL_cboxChecked = false;
				MyProject.Forms.frmMain.IN_cboxChecked = false;
				MyProject.Forms.frmMain.KS_cboxChecked = false;
				MyProject.Forms.frmMain.KY_cboxChecked = false;
				MyProject.Forms.frmMain.LA_cboxChecked = false;
				MyProject.Forms.frmMain.MA_cboxChecked = false;
				MyProject.Forms.frmMain.ME_cboxChecked = false;
				MyProject.Forms.frmMain.MI_cboxChecked = false;
				MyProject.Forms.frmMain.MN_cboxChecked = false;
				MyProject.Forms.frmMain.MD_cboxChecked = false;
				MyProject.Forms.frmMain.MO_cboxChecked = false;
				MyProject.Forms.frmMain.MS_cboxChecked = false;
				MyProject.Forms.frmMain.MT_cboxChecked = false;
				MyProject.Forms.frmMain.NE_cboxChecked = false;
				MyProject.Forms.frmMain.ND_cboxChecked = false;
				MyProject.Forms.frmMain.NC_cboxChecked = false;
				MyProject.Forms.frmMain.NH_cboxChecked = false;
				MyProject.Forms.frmMain.NJ_cboxChecked = false;
				MyProject.Forms.frmMain.NM_cboxChecked = false;
				MyProject.Forms.frmMain.NY_cboxChecked = false;
				MyProject.Forms.frmMain.NV_cboxChecked = false;
				MyProject.Forms.frmMain.HI_cboxChecked = false;
				MyProject.Forms.frmMain.OH_cboxChecked = false;
				MyProject.Forms.frmMain.OK_cboxChecked = false;
				MyProject.Forms.frmMain.OR_cboxChecked = false;
				MyProject.Forms.frmMain.PA_cboxChecked = false;
				MyProject.Forms.frmMain.RI_cboxChecked = false;
				MyProject.Forms.frmMain.SC_cboxChecked = false;
				MyProject.Forms.frmMain.SD_cboxChecked = false;
				MyProject.Forms.frmMain.TN_cboxChecked = false;
				MyProject.Forms.frmMain.TX_cboxChecked = false;
				MyProject.Forms.frmMain.UT_cboxChecked = false;
				MyProject.Forms.frmMain.VA_cboxChecked = false;
				MyProject.Forms.frmMain.VT_cboxChecked = false;
				MyProject.Forms.frmMain.WA_cboxChecked = false;
				MyProject.Forms.frmMain.WI_cboxChecked = false;
				MyProject.Forms.frmMain.WV_cboxChecked = false;
				MyProject.Forms.frmMain.WY_cboxChecked = false;
				StateCodeNames stateCodeNames = new StateCodeNames();
				string st = Conversions.ToString(this.comboBoxStateSelection.SelectedItem);
				string stateID = stateCodeNames.getStateID(st);
				if (string.Compare(stateID, "AL") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.AL_cboxChecked = true;
					RegionalData regStateData2 = bEAData.RegStateData;
					text = "AL";
					StateData state = regStateData2.GetState(ref text);
					stateData.addState(ref state);
				}
				if (string.Compare(stateID, "AK") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.AK_cboxChecked = true;
					RegionalData regStateData3 = bEAData.RegStateData;
					text = "AK";
					StateData state2 = regStateData3.GetState(ref text);
					stateData.addState(ref state2);
				}
				if (string.Compare(stateID, "AR") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.AR_cboxChecked = true;
					RegionalData regStateData4 = bEAData.RegStateData;
					text = "AR";
					StateData state3 = regStateData4.GetState(ref text);
					stateData.addState(ref state3);
				}
				if (string.Compare(stateID, "AZ") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.AZ_cboxChecked = true;
					RegionalData regStateData5 = bEAData.RegStateData;
					text = "AZ";
					StateData state4 = regStateData5.GetState(ref text);
					stateData.addState(ref state4);
				}
				if (string.Compare(stateID, "CA") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.CA_cboxChecked = true;
					RegionalData regStateData6 = bEAData.RegStateData;
					text = "CA";
					StateData state5 = regStateData6.GetState(ref text);
					stateData.addState(ref state5);
				}
				if (string.Compare(stateID, "CO") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.CO_cboxChecked = true;
					RegionalData regStateData7 = bEAData.RegStateData;
					text = "CO";
					StateData state6 = regStateData7.GetState(ref text);
					stateData.addState(ref state6);
				}
				if (string.Compare(stateID, "CT") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.CT_cboxChecked = true;
					RegionalData regStateData8 = bEAData.RegStateData;
					text = "CT";
					StateData state7 = regStateData8.GetState(ref text);
					stateData.addState(ref state7);
				}
				if (string.Compare(stateID, "DC") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.DC_cboxChecked = true;
					RegionalData regStateData9 = bEAData.RegStateData;
					text = "DC";
					StateData state8 = regStateData9.GetState(ref text);
					stateData.addState(ref state8);
				}
				if (string.Compare(stateID, "DE") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.DE_cboxChecked = true;
					RegionalData regStateData10 = bEAData.RegStateData;
					text = "DE";
					StateData state9 = regStateData10.GetState(ref text);
					stateData.addState(ref state9);
				}
				if (string.Compare(stateID, "FL") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.FL_cboxChecked = true;
					RegionalData regStateData11 = bEAData.RegStateData;
					text = "FL";
					StateData state10 = regStateData11.GetState(ref text);
					stateData.addState(ref state10);
				}
				if (string.Compare(stateID, "GA") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.GA_cboxChecked = true;
					RegionalData regStateData12 = bEAData.RegStateData;
					text = "GA";
					StateData state11 = regStateData12.GetState(ref text);
					stateData.addState(ref state11);
				}
				if (string.Compare(stateID, "IA") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.IA_cboxChecked = true;
					RegionalData regStateData13 = bEAData.RegStateData;
					text = "IA";
					StateData state12 = regStateData13.GetState(ref text);
					stateData.addState(ref state12);
				}
				if (string.Compare(stateID, "ID") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.ID_cboxChecked = true;
					RegionalData regStateData14 = bEAData.RegStateData;
					text = "ID";
					StateData state13 = regStateData14.GetState(ref text);
					stateData.addState(ref state13);
				}
				if (string.Compare(stateID, "IL") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.IL_cboxChecked = true;
					RegionalData regStateData15 = bEAData.RegStateData;
					text = "IL";
					StateData state14 = regStateData15.GetState(ref text);
					stateData.addState(ref state14);
				}
				if (string.Compare(stateID, "IN") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.IN_cboxChecked = true;
					RegionalData regStateData16 = bEAData.RegStateData;
					text = "IN";
					StateData state15 = regStateData16.GetState(ref text);
					stateData.addState(ref state15);
				}
				if (string.Compare(stateID, "KS") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.KS_cboxChecked = true;
					RegionalData regStateData17 = bEAData.RegStateData;
					text = "KS";
					StateData state16 = regStateData17.GetState(ref text);
					stateData.addState(ref state16);
				}
				if (string.Compare(stateID, "KY") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.KY_cboxChecked = true;
					RegionalData regStateData18 = bEAData.RegStateData;
					text = "KY";
					StateData state17 = regStateData18.GetState(ref text);
					stateData.addState(ref state17);
				}
				if (string.Compare(stateID, "LA") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.LA_cboxChecked = true;
					RegionalData regStateData19 = bEAData.RegStateData;
					text = "LA";
					StateData state18 = regStateData19.GetState(ref text);
					stateData.addState(ref state18);
				}
				if (string.Compare(stateID, "MA") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.MA_cboxChecked = true;
					RegionalData regStateData20 = bEAData.RegStateData;
					text = "MA";
					StateData state19 = regStateData20.GetState(ref text);
					stateData.addState(ref state19);
				}
				if (string.Compare(stateID, "ME") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.ME_cboxChecked = true;
					RegionalData regStateData21 = bEAData.RegStateData;
					text = "ME";
					StateData state20 = regStateData21.GetState(ref text);
					stateData.addState(ref state20);
				}
				if (string.Compare(stateID, "MI") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.MI_cboxChecked = true;
					RegionalData regStateData22 = bEAData.RegStateData;
					text = "MI";
					StateData state21 = regStateData22.GetState(ref text);
					stateData.addState(ref state21);
				}
				if (string.Compare(stateID, "MN") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.MN_cboxChecked = true;
					RegionalData regStateData23 = bEAData.RegStateData;
					text = "MN";
					StateData state22 = regStateData23.GetState(ref text);
					stateData.addState(ref state22);
				}
				if (string.Compare(stateID, "MD") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.MD_cboxChecked = true;
					RegionalData regStateData24 = bEAData.RegStateData;
					text = "MD";
					StateData state23 = regStateData24.GetState(ref text);
					stateData.addState(ref state23);
				}
				if (string.Compare(stateID, "MO") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.MO_cboxChecked = true;
					RegionalData regStateData25 = bEAData.RegStateData;
					text = "MO";
					StateData state24 = regStateData25.GetState(ref text);
					stateData.addState(ref state24);
				}
				if (string.Compare(stateID, "MS") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.MS_cboxChecked = true;
					RegionalData regStateData26 = bEAData.RegStateData;
					text = "MS";
					StateData state25 = regStateData26.GetState(ref text);
					stateData.addState(ref state25);
				}
				if (string.Compare(stateID, "MT") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.MT_cboxChecked = true;
					RegionalData regStateData27 = bEAData.RegStateData;
					text = "MT";
					StateData state26 = regStateData27.GetState(ref text);
					stateData.addState(ref state26);
				}
				if (string.Compare(stateID, "NE") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.NE_cboxChecked = true;
					RegionalData regStateData28 = bEAData.RegStateData;
					text = "NE";
					StateData state27 = regStateData28.GetState(ref text);
					stateData.addState(ref state27);
				}
				if (string.Compare(stateID, "ND") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.ND_cboxChecked = true;
					RegionalData regStateData29 = bEAData.RegStateData;
					text = "ND";
					StateData state28 = regStateData29.GetState(ref text);
					stateData.addState(ref state28);
				}
				if (string.Compare(stateID, "NC") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.NC_cboxChecked = true;
					RegionalData regStateData30 = bEAData.RegStateData;
					text = "NC";
					StateData state29 = regStateData30.GetState(ref text);
					stateData.addState(ref state29);
				}
				if (string.Compare(stateID, "NH") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.NH_cboxChecked = true;
					RegionalData regStateData31 = bEAData.RegStateData;
					text = "NH";
					StateData state30 = regStateData31.GetState(ref text);
					stateData.addState(ref state30);
				}
				if (string.Compare(stateID, "NJ") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.NJ_cboxChecked = true;
					RegionalData regStateData32 = bEAData.RegStateData;
					text = "NJ";
					StateData state31 = regStateData32.GetState(ref text);
					stateData.addState(ref state31);
				}
				if (string.Compare(stateID, "NM") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.NM_cboxChecked = true;
					RegionalData regStateData33 = bEAData.RegStateData;
					text = "NM";
					StateData state32 = regStateData33.GetState(ref text);
					stateData.addState(ref state32);
				}
				if (string.Compare(stateID, "NY") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.NY_cboxChecked = true;
					RegionalData regStateData34 = bEAData.RegStateData;
					text = "NY";
					StateData state33 = regStateData34.GetState(ref text);
					stateData.addState(ref state33);
				}
				if (string.Compare(stateID, "NV") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.NV_cboxChecked = true;
					RegionalData regStateData35 = bEAData.RegStateData;
					text = "NV";
					StateData state34 = regStateData35.GetState(ref text);
					stateData.addState(ref state34);
				}
				if (string.Compare(stateID, "HI") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.HI_cboxChecked = true;
					RegionalData regStateData36 = bEAData.RegStateData;
					text = "HI";
					StateData state35 = regStateData36.GetState(ref text);
					stateData.addState(ref state35);
				}
				if (string.Compare(stateID, "OH") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.OH_cboxChecked = true;
					RegionalData regStateData37 = bEAData.RegStateData;
					text = "OH";
					StateData state36 = regStateData37.GetState(ref text);
					stateData.addState(ref state36);
				}
				if (string.Compare(stateID, "OK") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.OK_cboxChecked = true;
					RegionalData regStateData38 = bEAData.RegStateData;
					text = "OK";
					StateData state37 = regStateData38.GetState(ref text);
					stateData.addState(ref state37);
				}
				if (string.Compare(stateID, "OR") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.OR_cboxChecked = true;
					RegionalData regStateData39 = bEAData.RegStateData;
					text = "OR";
					StateData state38 = regStateData39.GetState(ref text);
					stateData.addState(ref state38);
				}
				if (string.Compare(stateID, "PA") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.PA_cboxChecked = true;
					RegionalData regStateData40 = bEAData.RegStateData;
					text = "PA";
					StateData state39 = regStateData40.GetState(ref text);
					stateData.addState(ref state39);
				}
				if (string.Compare(stateID, "RI") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.RI_cboxChecked = true;
					RegionalData regStateData41 = bEAData.RegStateData;
					text = "RI";
					StateData state40 = regStateData41.GetState(ref text);
					stateData.addState(ref state40);
				}
				if (string.Compare(stateID, "SC") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.SC_cboxChecked = true;
					RegionalData regStateData42 = bEAData.RegStateData;
					text = "SC";
					StateData state41 = regStateData42.GetState(ref text);
					stateData.addState(ref state41);
				}
				if (string.Compare(stateID, "SD") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.SD_cboxChecked = true;
					RegionalData regStateData43 = bEAData.RegStateData;
					text = "SD";
					StateData state42 = regStateData43.GetState(ref text);
					stateData.addState(ref state42);
				}
				if (string.Compare(stateID, "TN") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.TN_cboxChecked = true;
					RegionalData regStateData44 = bEAData.RegStateData;
					text = "TN";
					StateData state43 = regStateData44.GetState(ref text);
					stateData.addState(ref state43);
				}
				if (string.Compare(stateID, "TX") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.TX_cboxChecked = true;
					RegionalData regStateData45 = bEAData.RegStateData;
					text = "TX";
					StateData state44 = regStateData45.GetState(ref text);
					stateData.addState(ref state44);
				}
				if (string.Compare(stateID, "UT") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.UT_cboxChecked = true;
					RegionalData regStateData46 = bEAData.RegStateData;
					text = "UT";
					StateData state45 = regStateData46.GetState(ref text);
					stateData.addState(ref state45);
				}
				if (string.Compare(stateID, "VA") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.VA_cboxChecked = true;
					RegionalData regStateData47 = bEAData.RegStateData;
					text = "VA";
					StateData state46 = regStateData47.GetState(ref text);
					stateData.addState(ref state46);
				}
				if (string.Compare(stateID, "VT") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.VT_cboxChecked = true;
					RegionalData regStateData48 = bEAData.RegStateData;
					text = "VT";
					StateData state47 = regStateData48.GetState(ref text);
					stateData.addState(ref state47);
				}
				if (string.Compare(stateID, "WA") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.WA_cboxChecked = true;
					RegionalData regStateData49 = bEAData.RegStateData;
					text = "WA";
					StateData state48 = regStateData49.GetState(ref text);
					stateData.addState(ref state48);
				}
				if (string.Compare(stateID, "WI") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.WI_cboxChecked = true;
					RegionalData regStateData50 = bEAData.RegStateData;
					text = "WI";
					StateData state49 = regStateData50.GetState(ref text);
					stateData.addState(ref state49);
				}
				if (string.Compare(stateID, "WV") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.WV_cboxChecked = true;
					RegionalData regStateData51 = bEAData.RegStateData;
					text = "WV";
					StateData state50 = regStateData51.GetState(ref text);
					stateData.addState(ref state50);
				}
				if (string.Compare(stateID, "WY") == 0)
				{
					flag = true;
					MyProject.Forms.frmMain.WY_cboxChecked = true;
					RegionalData regStateData52 = bEAData.RegStateData;
					text = "WY";
					StateData state51 = regStateData52.GetState(ref text);
					stateData.addState(ref state51);
				}
				bEAData.worksum.regby = this.tboxRegionName.Text;
				if (Operators.CompareString(bEAData.worksum.EmploymentStatus, "FTE", false) == 0)
				{
					stateData.scaleEmploymentByFTE(bEAData.NTables.FTE_Ratio);
				}
				bEAData.RegionSelected = stateData;
				this.bdat = bEAData;
				this.regiondat = this.bdat.RegionSelected;
				this.isRegionCommitted = true;
			}
			string text2 = this.RegionComboBox.Text;
			bool flag2 = true;
			if (flag2 == this.rbVA.Checked)
			{
				this.bdat.RegMethod = BEAData.eMethod.ValueAdded;
				this.worksum.regmethod = "ValueAdded";
			}
			else if (flag2 == this.rbIncome2.Checked)
			{
				this.bdat.RegMethod = BEAData.eMethod.Income2;
				this.worksum.regmethod = "Income";
			}
			checked
			{
				if (this.rbUserDefMain.Checked)
				{
					if (this.tboxRegionName.Text.Trim().Length > 0)
					{
						RegionalDataEntryForm regionalDataEntryForm = new RegionalDataEntryForm();
						EditRegData editRegData = new EditRegData();
						bool flag3 = true;
						string text3 = this.tboxRegionName.Text.Trim();
						if (text3.Length < 1)
						{
							flag3 = false;
						}
						if (this.rbStateSelection.Checked & !this.isStateRegionCommitted)
						{
							flag3 = false;
						}
						if (this.rbSelRegion.Checked & !this.isRegionCommitted)
						{
							flag3 = false;
						}
						if (flag3)
						{
							if (this.rbUserDefined.Checked)
							{
								RegionalData regStateData53 = this.bdat.RegStateData;
								text = "XX";
								this.regiondat = new StateData("Region", regStateData53.GetState(ref text).nrec_base);
								this.regiondat.StateSetup(ref this.bdat.NTables.FTE_Ratio);
								this.regiondat.setEmptyState();
							}
							int noCols = 1;
							switch (this.bdat.RegMethod)
							{
							case BEAData.eMethod.Employment:
							{
								string[] hdrsCol2 = new string[1]
								{
									"Employment"
								};
								Matrix matrix = new Matrix(this.bdat.NTables.Employment.Count, noCols);
								int num2 = this.bdat.NTables.Employment.Count - 1;
								for (int j = 0; j <= num2; j++)
								{
									matrix[j, 0] = this.regiondat.getEmployment()[j];
								}
								editRegData.hdrsCol = hdrsCol2;
								editRegData.hdrsRow = this.bdat.NTables.hdrInd;
								editRegData.BaseTable = matrix;
								break;
							}
							case BEAData.eMethod.Income:
							{
								string[] hdrsCol5 = new string[1]
								{
									"Income"
								};
								Matrix matrix = new Matrix(this.bdat.EC.Count, noCols);
								int num5 = this.bdat.EC.Count - 1;
								for (int m = 0; m <= num5; m++)
								{
									matrix[m, 0] = this.regiondat.getEC()[m];
								}
								editRegData.hdrsCol = hdrsCol5;
								editRegData.hdrsRow = this.bdat.NTables.hdrInd;
								editRegData.BaseTable = matrix;
								break;
							}
							case BEAData.eMethod.Income2:
							{
								string[] hdrsCol3 = new string[5]
								{
									"Employment",
									"Employee Compensation",
									"Gross Operating Surplus",
									"Taxes",
									"Gross Regional product"
								};
								noCols = 5;
								Matrix matrix = new Matrix(this.bdat.EC.Count, noCols);
								int num3 = this.regiondat.GDP_imputed.Count - 1;
								for (int k = 0; k <= num3; k++)
								{
									matrix[k, 0] = this.regiondat.getEmployment()[k];
									matrix[k, 1] = this.regiondat.getEC()[k];
									matrix[k, 2] = this.regiondat.getGOS()[k];
									matrix[k, 3] = this.regiondat.getT()[k];
									matrix[k, 4] = this.regiondat.GDP_imputed[k];
								}
								editRegData.hdrsCol = hdrsCol3;
								editRegData.hdrsRow = this.bdat.NTables.hdrInd;
								editRegData.BaseTable = matrix;
								break;
							}
							case BEAData.eMethod.ValueAdded:
							{
								string[] hdrsCol4 = new string[5]
								{
									"Employment",
									"Employee Compensation",
									"Gross Operating Surplus",
									"Taxes",
									"Gross Regional product"
								};
								noCols = 5;
								Matrix matrix = new Matrix(this.bdat.EC.Count, noCols);
								int num4 = this.regiondat.GDP_imputed.Count - 1;
								for (int l = 0; l <= num4; l++)
								{
									matrix[l, 0] = this.regiondat.getEmployment()[l];
									matrix[l, 1] = this.regiondat.getEC()[l];
									matrix[l, 2] = this.regiondat.getGOS()[l];
									matrix[l, 3] = this.regiondat.getT()[l];
									matrix[l, 4] = this.regiondat.GDP_imputed[l];
								}
								editRegData.hdrsCol = hdrsCol4;
								editRegData.hdrsRow = this.bdat.NTables.hdrInd;
								editRegData.BaseTable = matrix;
								break;
							}
							case BEAData.eMethod.Output:
							{
								string[] hdrsCol = new string[1]
								{
									"Output"
								};
								Matrix matrix = new Matrix(this.bdat.EC.Count, noCols);
								int num = this.regiondat.getEC().Count - 1;
								for (int i = 0; i <= num; i++)
								{
									matrix[i, 0] = this.regiondat.getEC()[i];
								}
								editRegData.hdrsCol = hdrsCol;
								editRegData.hdrsRow = this.bdat.NTables.hdrInd;
								editRegData.BaseTable = matrix;
								break;
							}
							}
							regionalDataEntryForm.dispdat = editRegData;
							regionalDataEntryForm.testData();
							if (regionalDataEntryForm.ShowDialog() == DialogResult.OK)
							{
								switch (this.bdat.RegMethod)
								{
								case BEAData.eMethod.Employment:
								{
									int num8 = this.regiondat.getEmployment().Count - 1;
									for (int num9 = 0; num9 <= num8; num9++)
									{
										this.regiondat.setEmployment_AT(num9, regionalDataEntryForm.dispdat.BaseTable[num9, 0]);
									}
									break;
								}
								case BEAData.eMethod.Income:
								{
									int num12 = this.regiondat.getEC().Count - 1;
									for (int num13 = 0; num13 <= num12; num13++)
									{
										this.regiondat.setEC_AT(num13, regionalDataEntryForm.dispdat.BaseTable[num13, 0]);
									}
									break;
								}
								case BEAData.eMethod.Income2:
								{
									int num14 = this.regiondat.GDP_imputed.Count - 1;
									for (int num15 = 0; num15 <= num14; num15++)
									{
										double num16 = unchecked(regionalDataEntryForm.dispdat.BaseTable[num15, 0] - this.regiondat.GDP_imputed[num15]);
										this.regiondat.setEmployment_AT(num15, regionalDataEntryForm.dispdat.BaseTable[num15, 0]);
										this.regiondat.setEC_AT(num15, regionalDataEntryForm.dispdat.BaseTable[num15, 1]);
										this.regiondat.setGOS_at(num15, regionalDataEntryForm.dispdat.BaseTable[num15, 2]);
										this.regiondat.setT_at(num15, regionalDataEntryForm.dispdat.BaseTable[num15, 3]);
										this.regiondat.GDP_imputed[num15] = regionalDataEntryForm.dispdat.BaseTable[num15, 4];
										this.regiondat.setGDP_at(num15, regionalDataEntryForm.dispdat.BaseTable[num15, 4]);
									}
									break;
								}
								case BEAData.eMethod.Output:
								{
									int num10 = this.regiondat.getEC().Count - 1;
									for (int num11 = 0; num11 <= num10; num11++)
									{
										this.regiondat.setEC_AT(num11, regionalDataEntryForm.dispdat.BaseTable[num11, 0]);
									}
									break;
								}
								case BEAData.eMethod.ValueAdded:
								{
									int num6 = this.regiondat.GDP_imputed.Count - 1;
									for (int n = 0; n <= num6; n++)
									{
										double num7 = unchecked(regionalDataEntryForm.dispdat.BaseTable[n, 0] - this.regiondat.GDP_imputed[n]);
										this.regiondat.setEmployment_AT(n, regionalDataEntryForm.dispdat.BaseTable[n, 0]);
										this.regiondat.setEC_AT(n, regionalDataEntryForm.dispdat.BaseTable[n, 1]);
										this.regiondat.setGOS_at(n, regionalDataEntryForm.dispdat.BaseTable[n, 2]);
										this.regiondat.setT_at(n, regionalDataEntryForm.dispdat.BaseTable[n, 3]);
										this.regiondat.GDP_imputed[n] = regionalDataEntryForm.dispdat.BaseTable[n, 4];
										this.regiondat.setGDP_at(n, regionalDataEntryForm.dispdat.BaseTable[n, 4]);
									}
									break;
								}
								}
								this.bdat.worksum.regby = text3;
								this.bdat.RegionSelected = this.regiondat;
								this.bdat.userNCI = false;
								this.bdat.userScrap = false;
								base.DialogResult = DialogResult.OK;
								base.Close();
							}
						}
					}
					else
					{
						Interaction.MsgBox("Please Enter Region Name.", MsgBoxStyle.OkOnly, null);
						this.tboxRegionName.Focus();
					}
				}
				if (this.rbSelRegMain.Checked)
				{
					if (!text2.Equals(this.PSelect))
					{
						string left = text2;
						if (Operators.CompareString(left, this.multiStateRegion, false) == 0)
						{
							MultistateForm multistateForm = new MultistateForm(ref this.bdat);
							if (multistateForm.ShowDialog() == DialogResult.OK)
							{
								this.bdat.userNCI = false;
								this.bdat.userScrap = false;
								base.DialogResult = DialogResult.OK;
								base.Close();
							}
						}
						else if (Operators.CompareString(left, "", false) == 0)
						{
							this.RegionComboBox.Focus();
						}
						else
						{
							string stateID2 = this.bdat.StateIDs.getStateID(text2);
							StateData state52 = this.bdat.RegStateData.GetState(ref stateID2);
							this.bdat.worksum.regby = text2;
							this.bdat.RegionSelected = state52;
							this.bdat.userNCI = false;
							this.bdat.userScrap = false;
							this.tboxRegionName.Text = text2;
							base.DialogResult = DialogResult.OK;
							base.Close();
						}
					}
					else
					{
						this.RegionComboBox.Focus();
					}
				}
				if (this.rbUserDefMain.Checked)
				{
					this.bdat.worksum.IsRegionalizationUserDefined = true;
				}
				else
				{
					this.bdat.worksum.IsRegionalizationUserDefined = false;
				}
			}
		}

		private void buttonSelectRegion_Click(object sender, EventArgs e)
		{
			MultistateForm multistateForm = new MultistateForm(ref this.bdat);
			if (multistateForm.ShowDialog() == DialogResult.OK)
			{
				this.regiondat = this.bdat.RegionSelected;
				this.isRegionCommitted = true;
				this.tboxRegionName.Text = "Modified " + this.bdat.worksum.regby;
			}
		}

		private void comboBoxStateSelection_DropDown(object sender, EventArgs e)
		{
			this.comboBoxStateSelection.Items.Remove(this.PSelect);
		}

		private void rbStateSelection_CheckedChanged(object sender, EventArgs e)
		{
			this.isStateRegionCommitted = false;
			if (this.rbStateSelection.Checked)
			{
				this.comboBoxStateSelection.Enabled = true;
				this.buttonSelectRegion.Enabled = false;
				this.tboxRegionName.Text = "";
				this.isRegionCommitted = false;
				if (Conversions.ToBoolean(this.isStateFirstTime))
				{
					this.isStateFirstTime = false;
				}
				else if (Operators.ConditionalCompareObjectEqual(this.comboBoxStateSelection.SelectedItem, this.PSelect, false))
				{
					this.isStateRegionCommitted = false;
				}
				else
				{
					this.cBoxStateSelect();
					this.isStateRegionCommitted = true;
				}
			}
			else
			{
				this.comboBoxStateSelection.Enabled = false;
				this.isStateRegionCommitted = false;
			}
		}

		private void rbSelRegion_CheckedChanged(object sender, EventArgs e)
		{
			if (this.rbSelRegion.Checked)
			{
				this.buttonSelectRegion.Enabled = true;
				this.comboBoxStateSelection.Enabled = false;
				this.tboxRegionName.Text = "";
				this.isStateRegionCommitted = false;
			}
			else
			{
				this.buttonSelectRegion.Enabled = false;
				this.isRegionCommitted = false;
			}
		}

		private void comboBoxStateSelection_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.cBoxStateSelect();
		}

		private void cBoxStateSelect()
		{
			StateCodeNames stateCodeNames = new StateCodeNames();
			double num = 1000000.0;
			string text = Conversions.ToString(this.comboBoxStateSelection.SelectedItem);
			string stateID = stateCodeNames.getStateID(text);
			StateData state = this.bdat.RegStateData.GetState(ref stateID);
			this.tboxRegionName.Text = "Modified " + text;
			this.regiondat = state;
			this.isStateRegionCommitted = true;
		}

		private void rbUDef_CheckedChanged(object sender, EventArgs e)
		{
			if (this.rbUserDefMain.Checked)
			{
				this.rbSelRegion.Enabled = true;
				this.rbStateSelection.Enabled = true;
				this.rbUserDefined.Enabled = true;
				this.rbStateSelection.Checked = true;
				this.setUdefStateSelection();
				this.tboxRegionName.Text = "";
				if (this.rbStateSelection.Checked)
				{
					this.comboBoxStateSelection.Enabled = true;
					this.buttonSelectRegion.Enabled = false;
					this.tboxRegionName.Text = "";
					this.isRegionCommitted = false;
					if (Conversions.ToBoolean(this.isStateFirstTime))
					{
						this.isStateFirstTime = false;
					}
					else if (Operators.ConditionalCompareObjectEqual(this.comboBoxStateSelection.SelectedItem, this.PSelect, false))
					{
						this.isStateRegionCommitted = false;
					}
					else
					{
						this.cBoxStateSelect();
						this.isStateRegionCommitted = true;
					}
				}
				else
				{
					this.comboBoxStateSelection.Enabled = false;
					this.isStateRegionCommitted = false;
				}
			}
			else
			{
				this.rbSelRegion.Enabled = false;
				this.rbStateSelection.Enabled = false;
				this.rbUserDefined.Enabled = false;
				this.comboBoxStateSelection.Enabled = false;
				this.buttonSelectRegion.Enabled = false;
			}
		}

		private void selRegMain_CheckedChanged(object sender, EventArgs e)
		{
			if (this.rbSelRegMain.Checked)
			{
				this.RegionComboBox.Enabled = true;
				this.setMainRegionSelectionDropList();
				this.tboxRegionName.Text = "";
			}
			else
			{
				this.RegionComboBox.Enabled = false;
			}
		}
	}
}
