using IOSNAP.My;
using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[Serializable]
	public class BEAData : ICloneable
	{
		public enum DataState : byte
		{
			Empty,
			Loaded,
			Regionalized = 8,
			Calculated = 4
		}

		public enum VAs
		{
			TotalVA,
			GOS,
			T,
			EC
		}

		public enum DataFotmatType
		{
			General = 1,
			Coefficients,
			Rounded
		}

		public enum eSelections
		{
			TVA,
			EC,
			GOS,
			T,
			Imp,
			Emp,
			TFinDem,
			PCE,
			TI,
			PFI,
			CPI,
			TGE,
			TFE,
			NDCE,
			NDGI,
			DCE,
			DGI,
			TSLE,
			CEE,
			GIE,
			CEO,
			GIO
		}

		public enum eMethod : byte
		{
			ValueAdded,
			Employment,
			Income,
			Output,
			Income2
		}

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static double _EC_T_Sum;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static double _GDP_T_Sum;

		private string[] _hdrFinDem;

		private string[] _hdrVA;

		public string[] IndBaseClass;

		public string[] IndAdjGovClass;

		public string[] IndFedStateLocGovClass;

		public string[] IndTotGovClass;

		public string[] IndExpandedGovClass;

		public bool[] isIndGov;

		public StateCodeNames stateCodes;

		public string[] hdrCommBase;

		public string[] hdrCommAdjGov;

		public string[] hdrCommFedStateLocGov;

		public string[] hdrCommTotGov;

		public string[] hdrNoncompImp;

		public string[] hdrScrap;

		public string[] hdrCommExpandedGov;

		public SummaryDat worksum;

		public bool isEdited;

		public bool isFromRegionalData;

		public int N_BASE_STATE;

		public double CHRatio;

		public StateCodeNames StateIDs;

		public RegionalData RegStateData;

		public StateData RegionSelected;

		public GenDataStatus GovernmentAggStatus;

		public eMethod RegMethod;

		public DataState eDataState;

		public bool userNCI;

		public bool userScrap;

		public const int ScrapMakeIndex = 2;

		public const int NonCompImportsMakeIndex = 1;

		public double DIFFCORECCTION;

		public Matrix[] dblFill;

		public bool[] Selections;

		public bool[] VAProvided;

		public bool UseAbbrHdrs;

		public bool isDataRegionalized;

		public bool isRequirementTablesCalculated;

		public string[] dispHdrsCol;

		public string[] dispHdrsRow;

		public Vector PFI_r;

		public Vector PFI_n;

		public Vector CPI_r;

		public Vector CPI_n;

		public Vector X_r;

		public Vector X_n;

		public Vector PCE_r;

		public Vector PCE_n;

		public const string FormatGeneralRef = "#,##0.00";

		public const string FormatCoefficientsRef = "#,##0.000";

		public const string FormatRoundedRef = "#,##0";

		public const int MaxFormatGeneralExtraDigits = 6;

		public const int MaxFormatCoefficientsExtraDigits = 4;

		public string FormatGeneral;

		public int decimalsFormGen;

		public string FormatCoefficients;

		public int decimalsFormCoeff;

		public string FormatRounded;

		public double PersonalIncome;

		public double DisposableIncome;

		public double TotFedExp_2005;

		public double TotSLExp_2008;

		public NatTables NTables;

		public Matrix RegMake;

		public Matrix RegUse;

		public Matrix RegVA;

		public Matrix RegFinDem;

		public Matrix RegImports;

		public Matrix RegScrap;

		public Matrix RegNCI;

		public Vector epsilonReg;

		private Vector _e;

		private Vector _q;

		private Vector _g_N;

		private Vector g_R;

		private Vector s_N;

		private Vector s_R;

		private Vector r_c;

		private Matrix _B;

		private Matrix _D;

		private Matrix _DT;

		private Vector _p;

		private Matrix _W;

		private Matrix _WT;

		public Matrix dgIndImp;

		public Matrix deltaFDInd;

		public Matrix dgIndImpEC;

		public Matrix dgIndImpT;

		public Matrix dgIndImpGOS;

		public Matrix dgIndImpEMP;

		public Matrix dgIndImpVA;

		public Matrix dgCommImpEC;

		public Matrix dgCommImpT;

		public Matrix dgCommImpGOS;

		public Matrix dgCommImpEMP;

		public Matrix dgCommImpVA;

		public Vector regdatinp;

		public Matrix regdatmed;

		public Matrix dqCommImp;

		public Matrix dgCommImp;

		public Matrix deltaFDComm;

		private Vector _X;

		public Vector Regionalizer;

		public Vector EmploymentReg;

		public static double EC_T_Sum
		{
			get;
			set;
		} = 0.0;


		public static double GDP_T_Sum
		{
			get;
			set;
		} = 0.0;


		public Matrix Use
		{
			get
			{
				return this.NTables.Use;
			}
			set
			{
				this.NTables.SetUse(ref value);
			}
		}

		public Vector EC
		{
			get
			{
				return this.NTables.VA.getRowVector(0);
			}
			[MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
			set
			{
				checked
				{
					if (value.Count != this.NTables.VA.NoCols)
					{
						Information.Err().Raise(5008, null, "Dimension does not match Value Added", null, null);
					}
					else
					{
						int num = this.NTables.getIndustryCount() - 1;
						for (int i = 0; i <= num; i++)
						{
							this.NTables.VA[0, i] = value[i];
						}
					}
				}
			}
		}

        public double get_ECDouble(int index)
		{
            return this.NTables.VA[0, index];
        }

        public void set_ECDouble(int index, double value)
        {
            this.NTables.VA[0, index] = value;
        }

        public Vector GOS
		{
			get
			{
				return this.NTables.VA.getRowVector(2);
			}
			[MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
			set
			{
				checked
				{
					if (value.Count != this.NTables.VA.NoCols)
					{
						Information.Err().Raise(5008, null, "Dimension does not match Value Added", null, null);
					}
					else
					{
						int num = this.NTables.getIndustryCount() - 1;
						for (int i = 0; i <= num; i++)
						{
							this.NTables.VA[2, i] = value[i];
						}
					}
				}
			}
		}

        public double get_GOSDouble(int index)
        {
            return NTables.VA [2, index];
        }

        public void set_GOSDouble(int index, double value)
        {
            this.NTables.VA[2, index] = value;
        }

        //public double GOSDouble
        //{
        //	get
        //	{
        //		return this.NTables.VA[2, index];
        //	}
        //	set
        //	{
        //		this.NTables.VA[2, index] = value;
        //	}
        //}

        public Vector T
		{
			get
			{
				return this.NTables.VA.getRowVector(1);
			}
			[MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
			set
			{
				checked
				{
					if (value.Count != this.NTables.VA.NoCols)
					{
						Information.Err().Raise(5008, null, "Dimension does not match Value Added", null, null);
					}
					else
					{
						int num = this.NTables.getIndustryCount() - 1;
						for (int i = 0; i <= num; i++)
						{
							this.NTables.VA[1, i] = value[i];
						}
					}
				}
			}
		}

		//public double TDouble
		//{
		//	get
		//	{
		//		return this.NTables.VA[1, index];
		//	}
		//	set
		//	{
		//		this.NTables.VA[1, index] = value;
		//	}
		//}
        public double get_TDouble(int index)
        {
            return this.NTables.VA[1, index];
        }

        public void set_TDouble(int index, double value)
        {
            this.NTables.VA[1, index] = value;
        }

		public Matrix FinDem
		{
			get
			{
				return this.NTables.FinDem;
			}
			set
			{
				this.NTables.FinDem = value;
			}
		}

		public Vector get_FinDemVector(int ColIndex)
        {
			//get
			//{
				return this.NTables.FinDem.getColVector(ColIndex);
			//}
		}
        public double get_FinDemVector(int RowIndex, int ColIndex)
        {
            //get
            //{
            return this.NTables.FinDem[RowIndex, ColIndex];
            //}
        }

        public double get_FinDemDouble(int RowIndex, int ColIndex)
        {
			//get
			//{
				return this.NTables.FinDem[RowIndex, ColIndex];
			//}
			//set
			//{
			//	this.NTables.FinDem[RowIndex, ColIndex] = value;
			//}
		}

        public void set_FinDemDouble(int RowIndex, int ColIndex, double value)
        {
            NTables.FinDem[RowIndex, ColIndex] = value;
        }


        public Matrix Import
		{
			get
			{
				return this.NTables.Import;
			}
			set
			{
				this.NTables.Import = value;
			}
		}

		public Matrix Scrap
		{
			get
			{
				return this.NTables.Scrap;
			}
			set
			{
				this.NTables.Scrap = value;
			}
		}

		public Matrix NCI
		{
			get
			{
				return this.NTables.NCI;
			}
			set
			{
				this.NTables.NCI = value;
			}
		}

		public Vector Scrap1
		{
			get
			{
				return this.NTables.Scrap.getRowVector(0);
			}
		}

		public Vector e
		{
			get
			{
				return this._e;
			}
		}

		public Vector q
		{
			get
			{
				return this._q;
			}
		}

		public Vector g
		{
			get
			{
				return this._g_N;
			}
		}

		public Matrix Bo
		{
			get
			{
				return this._B;
			}
		}

		public Matrix D
		{
			get
			{
				return this._D;
			}
		}

		public Vector p
		{
			get
			{
				return this._p;
			}
		}

		public Matrix Wo
		{
			get
			{
				return this._W;
			}
		}

		public Matrix WoT
		{
			get
			{
				return this._WT;
			}
		}

		public string[] hdrFinDem
		{
			get
			{
				return this._hdrFinDem;
			}
			set
			{
				this._hdrFinDem = value;
			}
		}

		public string[] hdrVA
		{
			get
			{
				return this._hdrVA;
			}
			set
			{
				this._hdrVA = value;
			}
		}

		public string[] hdrComm
		{
			get
			{
				return this.NTables.hdrComm;
			}
			set
			{
				this.NTables.hdrComm = value;
			}
		}

		public int N_INDUSTRIES
		{
			get
			{
				return this.NTables.getIndustryCount();
			}
			set
			{
				this.NTables.setCountsInd(value);
			}
		}

		public int N_COMMODITIES
		{
			get
			{
				return this.NTables.getCommodityCount();
			}
			set
			{
				this.NTables.setCountsComm(value);
			}
		}

		public int N_INDUSTRIES_BASE
		{
			get
			{
				return this.N_BASE_STATE;
			}
			set
			{
				this.N_BASE_STATE = value;
			}
		}

		public string comments
		{
			get
			{
				return "";
			}
		}

		public Matrix Ao
		{
			get
			{
				return this.WoT * this.Bo;
			}
		}

		public Matrix Ac
		{
			get
			{
				return (Matrix)Operators.MultiplyObject(this.WcT, this.Bc);
			}
		}

		public Matrix LeonOpen
		{
			get
			{
				return Matrix.Inverse(Matrix.I(this.NTables.getIndustryCount()) - this.Ao);
			}
		}

		public Matrix LeonClosed
		{
			get
			{
				return Matrix.Inverse(Matrix.I(checked(this.NTables.getIndustryCount() + 1)) - this.Ac);
			}
		}

		public Matrix CxC_DRTo
		{
			get
			{
				return this.Bo * this.WoT;
			}
		}

		public Matrix CxC_DRTc
		{
			get
			{
				return (Matrix)Operators.MultiplyObject(this.Bc, this.WcT);
			}
		}

		public Matrix IxC_DRTo
		{
			get
			{
				return this.Wo * this.Bo * this.WoT;
			}
		}

		public Matrix IxC_DRTc
		{
			get
			{
				return (Matrix)Operators.MultiplyObject(Operators.MultiplyObject(this.Wc, this.Bc), this.WcT);
			}
		}

		public Matrix CxC_TRc
		{
			get
			{
				return Matrix.InvI_BD((Matrix)this.Bc, (Matrix)this.WcT);
			}
		}

		public Matrix CxC_TRo
		{
			get
			{
				return Matrix.InvI_BD(this.Bo, this.WoT);
			}
		}

		public Matrix IxC_TRo
		{
			get
			{
				return this.Wo * this.CxC_TRo;
			}
		}

		public Matrix IxC_TRc
		{
			get
			{
				return (Matrix)this.Wc * this.CxC_TRc;
			}
		}

		public object Bc
		{
			get
			{
				if (!this.isRequirementTablesCalculated)
				{
					this.Calculate();
				}
				Matrix matrix = this.Bo.Clone();
				Vector colVector = this.FinDem.getColVector(0);
				double denom = Vector.sum(colVector);
				colVector /= denom;
				if (string.Compare(this.worksum.dataregionalized, "NO") == -1)
				{
					double x = this.DisposableIncome / this.PersonalIncome;
					colVector /= Math.Pow(x, -1.0);
				}
				else
				{
                    //TODO CHeck frmMain
					//double x = frmMain.MeDisposableIncomeM[MyProject.Forms.frmMain.YearSelect_CBoxTS.SelectedIndex] / MyProject.Forms.frmMain.MePersonalIncomeM[MyProject.Forms.frmMain.YearSelect_CBoxTS.SelectedIndex];
					//if (MyProject.Forms.frmMain.MeDisposableIncomeM[MyProject.Forms.frmMain.YearSelect_CBoxTS.SelectedIndex] == 0.0 & MyProject.Forms.frmMain.MePersonalIncomeM[MyProject.Forms.frmMain.YearSelect_CBoxTS.SelectedIndex] == 0.0)
					//{
					//	x = this.DisposableIncome / this.PersonalIncome;
					//	MyProject.Forms.frmMain.MeDisposableIncomeM[MyProject.Forms.frmMain.YearSelect_CBoxTS.SelectedIndex] = this.DisposableIncome;
					//	MyProject.Forms.frmMain.MePersonalIncomeM[MyProject.Forms.frmMain.YearSelect_CBoxTS.SelectedIndex] = this.PersonalIncome;
					//}
					//colVector /= Math.Pow(x, -1.0);
				}
				Vector row = this.EC / this.g;
				matrix.Append(colVector, row);
				checked(matrix[matrix.NoRows - 1, matrix.NoCols - 1]) = 0.0;
				return matrix;
			}
		}

		public object Wc
		{
			get
			{
				Matrix matrix = this.Wo.Clone();
				checked
				{
					double[] row = new double[matrix.NoCols + 1];
					double[] col = new double[matrix.NoRows + 1];
					matrix.Append(col, row);
					matrix[matrix.NoRows - 1, matrix.NoCols - 1] = 1.0;
					return matrix;
				}
			}
		}

		public object WcT
		{
			get
			{
				Matrix matrix = this.WoT.Clone();
				checked
				{
					double[] row = new double[matrix.NoCols + 1];
					double[] col = new double[matrix.NoRows + 1];
					matrix.Append(col, row);
					matrix[matrix.NoRows - 1, matrix.NoCols - 1] = 1.0;
					return matrix;
				}
			}
		}

		public object Clone()
		{
			BEAData bEAData = new BEAData();
			BEAData bEAData2 = bEAData;
			bEAData2.NTables = (NatTables)this.NTables.Clone();
			bEAData2.PersonalIncome = this.PersonalIncome + 0.0;
			bEAData2.RegMake = this.RegMake.Clone();
			bEAData2.Regionalizer = this.Regionalizer.Clone();
			bEAData2.RegStateData = (RegionalData)this.RegStateData.Clone();
			bEAData2.StateIDs = (StateCodeNames)this.StateIDs.clone();
			bEAData2._q = this._q.Clone();
			bEAData2._g_N = this._g_N.Clone();
			bEAData2._B = this._B.Clone();
			bEAData2._D = this._D.Clone();
			bEAData2._DT = this._DT;
			bEAData2._p = this._p.Clone();
			bEAData2._W = this._W.Clone();
			bEAData2._WT = this._WT;
			bEAData2.r_c = this.r_c;
			bEAData2._X = this._X.Clone();
			bEAData2.s_N = this.s_N;
			bEAData2.stateCodes = (StateCodeNames)this.stateCodes.clone();
			bEAData2.EmploymentReg = this.EmploymentReg.Clone();
			bEAData2.IndBaseClass = (string[])this.IndBaseClass.Clone();
			bEAData2.IndAdjGovClass = (string[])this.IndAdjGovClass.Clone();
			bEAData2.IndFedStateLocGovClass = (string[])this.IndFedStateLocGovClass.Clone();
			bEAData2.IndTotGovClass = (string[])this.IndTotGovClass.Clone();
			bEAData2._hdrFinDem = (string[])this._hdrFinDem.Clone();
			bEAData2._hdrVA = (string[])this._hdrVA.Clone();
			bEAData2.N_BASE_STATE = this.N_BASE_STATE;
			bEAData2.RegMethod = this.RegMethod;
			bEAData2.userNCI = this.userNCI;
			bEAData2.userScrap = this.userScrap;
			bEAData2.Selections = this.Selections;
			bEAData2.VAProvided = this.VAProvided;
			bEAData2.DisposableIncome = this.DisposableIncome;
			bEAData2.TotFedExp_2005 = this.TotFedExp_2005;
			bEAData2.TotSLExp_2008 = this.TotSLExp_2008;
			bEAData2.UseAbbrHdrs = this.UseAbbrHdrs;
			bEAData2.eDataState = this.eDataState;
			bEAData2.isEdited = this.isEdited;
			bEAData2.isDataRegionalized = this.isDataRegionalized;
			bEAData2.isFromRegionalData = this.isFromRegionalData;
			bEAData2.isRequirementTablesCalculated = this.isRequirementTablesCalculated;
			bEAData2.PFI_r = this.PFI_r.Clone();
			bEAData2.PFI_n = this.PFI_n.Clone();
			bEAData2.CPI_r = this.CPI_r.Clone();
			bEAData2.CPI_n = this.CPI_n.Clone();
			bEAData2.X_r = this.X_r.Clone();
			bEAData2.X_n = this.X_n.Clone();
			bEAData2.PCE_r = this.PCE_r.Clone();
			bEAData2.PCE_n = this.PCE_n.Clone();
			bEAData2.FormatGeneral = this.FormatGeneral;
			bEAData2.decimalsFormGen = this.decimalsFormGen;
			bEAData2.FormatCoefficients = this.FormatCoefficients;
			bEAData2.decimalsFormCoeff = this.decimalsFormCoeff;
			bEAData2.FormatRounded = this.FormatRounded;
			bEAData2.userNCI = this.userNCI;
			bEAData2.userScrap = this.userScrap;
			bEAData2.CHRatio = this.CHRatio;
			bEAData2.worksum = (SummaryDat)this.worksum.Clone();
			bEAData2 = null;
			return bEAData;
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Clone
			return this.Clone();
		}

		public void Aggregate(int[] AggInd, int[] AggComm)
		{
			Matrix matrix = new Matrix(this.N_COMMODITIES, this.N_INDUSTRIES);
			Vector regionalizer = new Vector(this.N_INDUSTRIES);
			Matrix matrix2 = new Matrix(1, this.N_COMMODITIES);
			Vector vector = new Vector(this.N_COMMODITIES);
			Matrix matrix3 = new Matrix(1, this.N_INDUSTRIES);
			Matrix matrix4 = new Matrix(1, this.N_INDUSTRIES);
			Matrix matrix5 = new Matrix(this.N_INDUSTRIES, this.N_COMMODITIES);
			Matrix matrix6 = new Matrix(this.N_COMMODITIES, 10);
			Matrix matrix7 = new Matrix(this.NTables.VA.NoRows, this.N_INDUSTRIES);
			Vector vector2 = new Vector(this.N_INDUSTRIES);
			Vector vector3 = new Vector(this.N_INDUSTRIES);
			Vector vector4 = new Vector(this.N_INDUSTRIES);
			Vector vector5 = new Vector(this.N_INDUSTRIES);
			Vector vector6 = new Vector(this.N_INDUSTRIES);
			Vector vector7 = new Vector(this.N_INDUSTRIES);
			Vector vector8 = new Vector(this.N_INDUSTRIES);
			this.isRequirementTablesCalculated = false;
			checked
			{
				int[] array = new int[this.N_INDUSTRIES + 1];
				int upperBound = AggInd.GetUpperBound(0);
				for (int i = 0; i <= upperBound; i++)
				{
					int upperBound2 = AggComm.GetUpperBound(0);
					for (int j = 0; j <= upperBound2; j++)
					{
						Matrix matrix8;
						int row;
						int col;
						(matrix8 = matrix5)[row = AggInd[i], col = AggComm[j]] = unchecked(matrix8[row, col] + this.NTables.Make()[i, j]);
					}
				}
				int upperBound3 = AggComm.GetUpperBound(0);
				for (int i = 0; i <= upperBound3; i++)
				{
					int upperBound4 = AggInd.GetUpperBound(0);
					for (int j = 0; j <= upperBound4; j++)
					{
						Matrix matrix8;
						int col;
						int row;
						(matrix8 = matrix)[col = AggComm[i], row = AggInd[j]] = unchecked(matrix8[col, row] + this.Use[i, j]);
					}
				}
				int upperBound5 = AggInd.GetUpperBound(0);
				for (int i = 0; i <= upperBound5; i++)
				{
					unchecked
					{
						Matrix matrix8;
						int row;
						(matrix8 = matrix3)[0, row = AggInd[i]] = matrix8[0, row] + this.Scrap[0, i];
						(matrix8 = matrix4)[0, row = AggInd[i]] = matrix8[0, row] + this.NCI[0, i];
					}
				}
				int num = this.NTables.VA.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					int upperBound6 = AggInd.GetUpperBound(0);
					for (int j = 0; j <= upperBound6; j++)
					{
						Matrix matrix8;
						int row;
						int col;
						(matrix8 = matrix7)[row = i, col = AggInd[j]] = unchecked(matrix8[row, col] + this.NTables.VA[i, j]);
					}
				}
				int upperBound7 = AggComm.GetUpperBound(0);
				for (int i = 0; i <= upperBound7; i++)
				{
					int num2 = this.FinDem.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						Matrix matrix8;
						int col;
						int row;
						(matrix8 = matrix6)[col = AggComm[i], row = j] = unchecked(matrix8[col, row] + this.get_FinDemVector(i, j));
					}
					unchecked
					{
						Matrix matrix8;
						int row;
						(matrix8 = matrix2)[0, row = AggComm[i]] = matrix8[0, row] + this.Import[0, i];
						Vector vector9;
						(vector9 = vector)[row = AggComm[i]] = vector9[row] + this.NTables.PCE[i];
					}
				}
				int upperBound8 = AggInd.GetUpperBound(0);
				for (int i = 0; i <= upperBound8; i++)
				{
					unchecked
					{
						Vector vector9;
						int row;
						(vector9 = vector2)[row = AggInd[i]] = vector9[row] + this.get_ECDouble(i);
						(vector9 = vector4)[row = AggInd[i]] = vector9[row] + this.get_GOSDouble(i);
						(vector9 = vector3)[row = AggInd[i]] = vector9[row] + this.get_TDouble(i);
						if (this.worksum.dataregionalized.Contains("Yes"))
						{
							(vector9 = vector7)[row = AggInd[i]] = vector9[row] + this.NTables.RegEmployment_FTE[i];
							(vector9 = vector8)[row = AggInd[i]] = vector9[row] + this.NTables.RegEmployment_JOBS[i];
						}
						(vector9 = vector5)[row = AggInd[i]] = vector9[row] + this.NTables.Employment_FTE[i];
						(vector9 = vector6)[row = AggInd[i]] = vector9[row] + this.NTables.Employment_JOBS[i];
					}
				}
				vector[vector.Count - 2] = this.NTables.PCE[this.NTables.PCE.Count - 2];
				vector[vector.Count - 1] = this.NTables.PCE[this.NTables.PCE.Count - 1];
				this.NTables.SetMake(ref matrix5);
				this.Use = matrix;
				this.Scrap = matrix3;
				this.NTables.PCE = vector;
				this.NCI = matrix4;
				this.NTables.VA = matrix7;
				this.FinDem = matrix6;
				this.Import = matrix2;
				this.Regionalizer = regionalizer;
				this.EC = vector2;
				this.GOS = vector4;
				this.T = vector3;
				this.NTables.FTE_Ratio.Aggregate(AggInd, AggComm, this.N_INDUSTRIES_BASE, vector5, vector6, ref this.GovernmentAggStatus);
				this.NTables.Employment_FTE = vector5;
				this.NTables.Employment_JOBS = vector6;
				if (this.worksum.dataregionalized.Contains("Yes"))
				{
					this.NTables.RegEmployment_FTE = vector7;
					this.NTables.RegEmployment_JOBS = vector8;
				}
				this.RegStateData.Aggregate(AggInd, AggComm, this.N_INDUSTRIES_BASE, ref this.NTables.FTE_Ratio);
			}
		}

		public void RegReset()
		{
			this.isDataRegionalized = false;
			this.RegMake = new Matrix(this.NTables.Make().NoRows, this.NTables.Make().NoCols);
			this.RegUse = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
			this.RegVA = new Matrix(this.NTables.VA.NoRows, this.NTables.VA.NoCols);
			this.RegImports = new Matrix(1, this.NTables.Import.NoCols);
			this.RegScrap = new Matrix(1, this.NTables.Import.NoCols);
			this.RegFinDem = new Matrix(this.FinDem.NoRows, this.FinDem.NoCols);
		}

		public void Regionalize()
		{
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			switch (this.RegMethod)
			{
			case eMethod.ValueAdded:
				this.Regionalize_VA();
				break;
			case eMethod.Income2:
				this.Regionalize_Income();
				break;
			default:
				Interaction.MsgBox("Unknown regionalization method", MsgBoxStyle.OkOnly, null);
				this.epsilonReg = this.EC / this.EC;
				break;
			}
			this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
		}

		public void Regionalize_Income()
		{
			Matrix matrix = new Matrix(this.NTables.Use.NoRows, 4);
			Matrix matrix2 = new Matrix(this.NTables.Use.NoRows, 4);
			Matrix matrix3 = new Matrix(this.NTables.Use.NoRows, 2);
			Matrix matrix4 = new Matrix(this.NTables.Use.NoRows, 2);
			Matrix matrix5 = new Matrix(this.NTables.Use.NoRows, 8);
			Matrix matrix6 = new Matrix(this.NTables.Use.NoRows, 8);
			this.RegFinDem = new Matrix(this.FinDem.NoRows, this.FinDem.NoCols);
			this.RegMake = new Matrix(this.NTables.Make().NoRows, this.NTables.Make().NoCols);
			this.RegUse = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
			this.RegVA = new Matrix(this.NTables.VA.NoRows, this.NTables.VA.NoCols);
			this.RegImports = new Matrix(1, this.NTables.Import.NoCols);
			this.RegScrap = new Matrix(1, this.NTables.Import.NoCols);
			Vector vector = new Vector(this.NTables.Import.NoCols);
			Vector vector2 = new Vector(this.EC.Count);
			Vector vector3 = new Vector(this.GOS.Count);
			Vector vector4 = new Vector(this.T.Count);
			string name = this.RegionSelected.Name;
			RegionalData regStateData = this.RegStateData;
			string text = "AL";
			StateData stateData = new StateData(name, regStateData.GetState(ref text).nrec_base);
			stateData.StateSetup(ref this.NTables.FTE_Ratio);
			stateData.setEmptyState();
			stateData.addState(ref this.RegionSelected);
			this.RegionSelected = stateData;
			this.NTables.RegEmployment = this.RegionSelected.getEmployment();
			this.epsilonReg = this.EC / this.EC;
			Vector vector5 = new Vector(vector2.Count);
			eMethod regMethod = this.RegMethod;
			Vector vector8;
			Vector vector6;
			Vector vector7;
			double num14;
			checked
			{
				if (regMethod == eMethod.Income2)
				{
					vector2 = this.RegionSelected.getEC();
					vector4 = this.RegionSelected.getT();
					vector3 = this.RegionSelected.getGOS();
					this.epsilonReg = vector2 / this.EC;
					this.RemoveScrapFromMake();
					this.putNCIToMake();
					this._g_N = Matrix.RowSum(this.NTables.Make()) + this.Scrap1;
					this.putScrapToMake();
					this.g_R = new Vector(this._g_N.Count);
					int num = vector5.Count - 1;
					for (int i = 0; i <= num; i++)
					{
						vector5[i] = unchecked(vector2[i] + vector4[i] + vector3[i]);
					}
					int num2 = this.g_R.Count - 1;
					for (int j = 0; j <= num2; j++)
					{
						unchecked
						{
							this.g_R[j] = this._g_N[j] * (vector2[j] / this.get_ECDouble(j));
							if (this.g_R[j] - vector5[j] < 0.0)
							{
								this.g_R[j] = (vector2[j] + vector4[j] + vector3[j]) * (this._g_N[j] / (this.NTables.VA[0, j] + this.NTables.VA[1, j] + this.NTables.VA[2, j]));
							}
						}
					}
					this.epsilonReg = this.g_R / this._g_N;
				}
				else
				{
					Interaction.MsgBox("Unknown regionalization method", MsgBoxStyle.OkOnly, null);
					this.epsilonReg = this.EC / this.EC;
				}
				Matrix matrix7 = Matrix.ReDimPreserve(this.NTables.Make(), this.epsilonReg.Count, this.NTables.Make().NoCols);
				this.putNCIToMake();
				this.putScrapToMake();
				this.RegMake = Matrix.diag(this.epsilonReg) * this.NTables.Make();
				int num3 = this.NTables.Make().NoCols - 2;
				this.RegScrap = new Matrix(1, this.Scrap.NoCols);
				this.RegNCI = new Matrix(1, this.Import.NoCols);
				int num4 = this.Scrap.NoCols - 1;
				for (int k = 0; k <= num4; k++)
				{
					this.RegScrap[0, k] = this.NTables.Make()[k, num3];
					this.RegNCI[0, k] = this.NTables.Make()[k, num3 + 1];
				}
				int num5 = this.N_INDUSTRIES - 1;
				for (int l = 0; l <= num5; l++)
				{
					this.NTables.Make()[l, num3] = 0.0;
					this.NTables.Make()[l, num3 + 1] = 0.0;
				}
				Matrix matrix8 = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
				Matrix matrix9 = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
				Matrix matrix10 = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
				int num6 = this.NTables.Use.NoRows - 1;
				for (int m = 0; m <= num6; m++)
				{
					int num7 = this.NTables.Use.NoCols - 1;
					for (int n = 0; n <= num7; n++)
					{
						matrix8[m, n] = this.NTables.Use[m, n];
					}
				}
				matrix10 = matrix8 * Matrix.Inverse(Matrix.diag(Matrix.ColSum(matrix8)));
				vector5 = new Vector(vector2.Count);
				int num8 = vector5.Count - 1;
				for (int num9 = 0; num9 <= num8; num9++)
				{
					vector5[num9] = unchecked(vector2[num9] + vector4[num9] + vector3[num9]);
				}
				matrix9 = matrix10 * Matrix.diag(this.g_R - vector5);
				int num10 = this.Use.NoRows - 1;
				for (int num11 = 0; num11 <= num10; num11++)
				{
					int num12 = this.Use.NoCols - 1;
					for (int num13 = 0; num13 <= num12; num13++)
					{
						this.RegUse[num11, num13] = matrix9[num11, num13];
					}
				}
				bool flag = false;
				this.PFI_n = new Vector(this.FinDem.NoRows);
				this.PFI_r = new Vector(this.FinDem.NoRows);
				this.CPI_n = new Vector(this.FinDem.NoRows);
				this.CPI_r = new Vector(this.FinDem.NoRows);
				this.X_n = new Vector(this.FinDem.NoRows);
				this.X_r = new Vector(this.FinDem.NoRows);
				this.PCE_n = new Vector(this.FinDem.NoRows);
				this.PCE_r = new Vector(this.FinDem.NoRows);
				vector6 = new Vector(this.FinDem.NoRows);
				vector7 = new Vector(this.FinDem.NoRows);
				vector8 = new Vector(this.FinDem.NoRows);
				num14 = 0.0;
			}
			double num15 = this.RegionSelected.DisposableIncome / this.DisposableIncome;
			double num16 = this.RegionSelected.PersonalIncome / this.PersonalIncome;
			vector6 = Matrix.ColSum(this.RegMake);
			vector7 = Matrix.ColSum(this.NTables.Make());
			num14 = Vector.sum(Matrix.ColSum(this.RegMake)) / Vector.sum(Matrix.ColSum(this.NTables.Make()));
			checked
			{
				int num17 = this.FinDem.NoRows - 1;
				for (int num18 = 0; num18 <= num17; num18++)
				{
					if (vector7[num18] != 0.0)
					{
						vector8[num18] = unchecked(vector6[num18] / vector7[num18]);
					}
					else
					{
						vector8[num18] = 0.0;
					}
				}
				int num19 = this.FinDem.NoRows - 1;
				for (int num20 = 0; num20 <= num19; num20++)
				{
					this.PFI_n[num20] = this.get_FinDemVector(num20, 1);
					this.CPI_n[num20] = this.get_FinDemVector(num20, 2);
					this.X_n[num20] = this.get_FinDemVector(num20, 3);
					this.PCE_n[num20] = this.get_FinDemVector(num20, 0);
					matrix[num20, 0] = this.get_FinDemVector(num20, 4);
					matrix[num20, 1] = this.get_FinDemVector(num20, 5);
					matrix[num20, 2] = this.get_FinDemVector(num20, 6);
					matrix[num20, 3] = this.get_FinDemVector(num20, 7);
					matrix3[num20, 0] = this.get_FinDemVector(num20, 8);
					matrix3[num20, 1] = this.get_FinDemVector(num20, 9);
				}
				double num21 = unchecked(Vector.sum(vector2 + vector4 + vector3) / BEAData.GDP_T_Sum);
				if (this.worksum.IsRegionalizationUserDefined)
				{
					int num22 = this.FinDem.NoRows - 1;
					for (int num23 = 0; num23 <= num22; num23++)
					{
						unchecked
						{
							this.PFI_r[num23] = num14 * this.PFI_n[num23];
							this.CPI_r[num23] = num14 * this.CPI_n[num23];
							this.X_r[num23] = vector8[num23] * this.X_n[num23];
							matrix2[num23, 0] = num21 * (this.RegionSelected.TotFedExp / this.TotFedExp_2005) * matrix[num23, 0];
							matrix4[num23, 0] = num21 * (this.RegionSelected.TotSLEExp / this.TotSLExp_2008) * matrix3[num23, 0];
							matrix2[num23, 1] = num21 * (this.RegionSelected.TotFedExp / this.TotFedExp_2005) * matrix[num23, 1];
							matrix4[num23, 1] = num21 * (this.RegionSelected.TotSLEExp / this.TotSLExp_2008) * matrix3[num23, 1];
							matrix2[num23, 2] = num21 * (this.RegionSelected.TotFedExp / this.TotFedExp_2005) * matrix[num23, 2];
							matrix2[num23, 3] = num21 * (this.RegionSelected.TotFedExp / this.TotFedExp_2005) * matrix[num23, 3];
						}
					}
				}
				else
				{
					int num24 = this.FinDem.NoRows - 1;
					for (int num25 = 0; num25 <= num24; num25++)
					{
						unchecked
						{
							this.PFI_r[num25] = num14 * this.PFI_n[num25];
							this.CPI_r[num25] = num14 * this.CPI_n[num25];
							this.X_r[num25] = vector8[num25] * this.X_n[num25];
							matrix2[num25, 0] = this.RegionSelected.TotFedExp / this.TotFedExp_2005 * matrix[num25, 0];
							matrix4[num25, 0] = this.RegionSelected.TotSLEExp / this.TotSLExp_2008 * matrix3[num25, 0];
							matrix2[num25, 1] = this.RegionSelected.TotFedExp / this.TotFedExp_2005 * matrix[num25, 1];
							matrix4[num25, 1] = this.RegionSelected.TotSLEExp / this.TotSLExp_2008 * matrix3[num25, 1];
							matrix2[num25, 2] = this.RegionSelected.TotFedExp / this.TotFedExp_2005 * matrix[num25, 2];
							matrix2[num25, 3] = this.RegionSelected.TotFedExp / this.TotFedExp_2005 * matrix[num25, 3];
						}
					}
				}
				if (this.RegMethod == eMethod.ValueAdded | this.RegMethod == eMethod.Income2)
				{
					int num26 = this.PCE_r.Count - 1;
					for (int num27 = 0; num27 <= num26; num27++)
					{
						this.PCE_r[num27] = unchecked(num16 * this.PCE_n[num27]);
					}
				}
				double num28 = unchecked(Vector.sum(vector2) / BEAData.EC_T_Sum);
				if (this.worksum.IsRegionalizationUserDefined)
				{
					int num29 = this.PCE_r.Count - 1;
					for (int num30 = 0; num30 <= num29; num30++)
					{
						this.PCE_r[num30] = unchecked(num28 * this.RegionSelected.PCE_Table[num30]);
					}
				}
				else
				{
					this.PCE_r = this.RegionSelected.PCE_Table;
				}
				double num31 = 0.0;
				int num32 = this.RegFinDem.NoRows - 1;
				for (int num33 = 0; num33 <= num32; num33++)
				{
					this.RegFinDem[num33, 1] = this.PFI_r[num33];
					this.RegFinDem[num33, 2] = this.CPI_r[num33];
					this.RegFinDem[num33, 3] = this.X_r[num33];
					this.RegFinDem[num33, 0] = this.PCE_r[num33];
					this.RegFinDem[num33, 4] = matrix2[num33, 0];
					this.RegFinDem[num33, 5] = matrix2[num33, 1];
					this.RegFinDem[num33, 6] = matrix2[num33, 2];
					this.RegFinDem[num33, 7] = matrix2[num33, 3];
					this.RegFinDem[num33, 8] = matrix4[num33, 0];
					this.RegFinDem[num33, 9] = matrix4[num33, 1];
				}
				this.RegMake = this.addNCIScrapToRegMake(ref this.RegMake, ref this.RegNCI, ref this.RegScrap);
				bool flag2 = true;
				int num34 = this.Import.NoCols - 1;
				for (int num35 = 0; num35 <= num34; num35++)
				{
					this.RegFinDem[num35, 3] = this.X_r[num35];
					double num36 = 0.0;
					double num37 = this.RegUse.RowSum(num35);
					double num38 = this.RegFinDem.RowSum(num35);
					double num39 = this.RegMake.ColSum(num35);
					unchecked
					{
						num36 = num37 + num38 - num39;
						double num40 = this.X_r[num35];
						bool flag3 = false;
						if (num36 >= 0.0)
						{
							flag3 = true;
							vector[num35] = num36;
						}
						else
						{
							vector[num35] = 0.0;
							this.X_r[num35] = this.X_r[num35] - num36;
						}
						this.RegImports[0, num35] = vector[num35];
					}
				}
				int num41 = vector6.Count - 1;
				for (int num42 = 0; num42 <= num41; num42++)
				{
					if (vector6[num42] == 0.0)
					{
						vector6[num42] = 1E-07;
					}
				}
				Vector vector9 = new Vector(this.FinDem.NoRows);
				int num43 = vector9.Count - 1;
				for (int num44 = 0; num44 <= num43; num44++)
				{
					unchecked
					{
						vector9[num44] = 0.1 * this.CHRatio * vector6[num44] + 0.9 * this.CHRatio * this.X_r[num44];
						this.RegImports[0, num44] = this.RegImports[0, num44] + vector9[num44];
						this.X_r[num44] = this.X_r[num44] + vector9[num44];
						this.RegFinDem[num44, 3] = this.X_r[num44];
					}
				}
				this.regdatmed = this.RegMake;
				string[] array = this.dispHdrsRow = new string[this.RegMake.NoRows + 1];
				this.isDataRegionalized = true;
				this.worksum.dataregionalized = "Yes";
				this.isRequirementTablesCalculated = false;
				int num45 = this.RegVA.NoCols - 1;
				for (int num46 = 0; num46 <= num45; num46++)
				{
					this.RegVA[0, num46] = vector2[num46];
					this.RegVA[1, num46] = vector4[num46];
					this.RegVA[2, num46] = vector3[num46];
				}
			}
		}

		public void Regionalize_VA()
		{
			Matrix matrix = new Matrix(this.NTables.Use.NoRows, 4);
			Matrix matrix2 = new Matrix(this.NTables.Use.NoRows, 4);
			Matrix matrix3 = new Matrix(this.NTables.Use.NoRows, 2);
			Matrix matrix4 = new Matrix(this.NTables.Use.NoRows, 2);
			Matrix matrix5 = new Matrix(this.NTables.Use.NoRows, 8);
			Matrix matrix6 = new Matrix(this.NTables.Use.NoRows, 8);
			this.RegFinDem = new Matrix(this.FinDem.NoRows, this.FinDem.NoCols);
			this.RegMake = new Matrix(this.NTables.Make().NoRows, this.NTables.Make().NoCols);
			this.RegUse = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
			this.RegVA = new Matrix(this.NTables.VA.NoRows, this.NTables.VA.NoCols);
			this.RegImports = new Matrix(1, this.NTables.Import.NoCols);
			this.RegScrap = new Matrix(1, this.NTables.Import.NoCols);
			Vector vector = new Vector(this.NTables.Import.NoCols);
			Vector vector2 = new Vector(this.EC.Count);
			Vector vector3 = new Vector(this.GOS.Count);
			Vector vector4 = new Vector(this.T.Count);
			string name = this.RegionSelected.Name;
			RegionalData regStateData = this.RegStateData;
			string text = "AL";
			StateData stateData = new StateData(name, regStateData.GetState(ref text).nrec_base);
			stateData.StateSetup(ref this.NTables.FTE_Ratio);
			stateData.setEmptyState();
			stateData.addState(ref this.RegionSelected);
			this.RegionSelected = stateData;
			this.NTables.RegEmployment = this.RegionSelected.getEmployment();
			this.epsilonReg = this.EC / this.EC;
			Vector vector10;
			Vector vector8;
			Vector vector9;
			double num9;
			checked
			{
				if (this.RegMethod == eMethod.ValueAdded)
				{
					Vector vector5 = this.EC + this.GOS + this.T;
					vector2 = this.RegionSelected.getEC();
					vector4 = this.RegionSelected.getT();
					vector3 = this.RegionSelected.getGOS();
					Vector vector6 = vector2 + vector3 + vector4;
					double num = Vector.sum(vector6);
					double num2 = Vector.sum(vector5);
					this.RemoveScrapFromMake();
					this.putNCIToMake();
					this._g_N = Matrix.RowSum(this.NTables.Make()) + this.Scrap1;
					this.putScrapToMake();
					this.g_R = new Vector(this._g_N.Count);
					int num3 = this._g_N.Count - 1;
					for (int i = 0; i <= num3; i++)
					{
						this.g_R[i] = unchecked(vector6[i] * (this._g_N[i] / vector5[i]));
					}
					this.epsilonReg = this.g_R / this._g_N;
				}
				else
				{
					Interaction.MsgBox("Unknown regionalization method", MsgBoxStyle.OkOnly, null);
					this.epsilonReg = this.EC / this.EC;
				}
				Matrix matrix7 = Matrix.ReDimPreserve(this.NTables.Make(), this.epsilonReg.Count, this.NTables.Make().NoCols);
				if (this.RegMethod == eMethod.ValueAdded)
				{
					this.putNCIToMake();
					this.putScrapToMake();
				}
				this.RegMake = Matrix.diag(this.epsilonReg) * this.NTables.Make();
				if (this.RegMethod == eMethod.ValueAdded)
				{
					int num4 = this.NTables.Make().NoCols - 2;
					this.RegScrap = new Matrix(1, this.Scrap.NoCols);
					this.RegNCI = new Matrix(1, this.Import.NoCols);
					int num5 = this.Scrap.NoCols - 1;
					for (int j = 0; j <= num5; j++)
					{
						this.RegScrap[0, j] = this.NTables.Make()[j, num4];
						this.RegNCI[0, j] = this.NTables.Make()[j, num4 + 1];
					}
					int num6 = this.N_INDUSTRIES - 1;
					for (int k = 0; k <= num6; k++)
					{
						this.NTables.Make()[k, num4] = 0.0;
						this.NTables.Make()[k, num4 + 1] = 0.0;
					}
				}
				Matrix matrix8 = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
				Matrix matrix9 = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
				Matrix matrix10 = new Matrix(this.NTables.Use.NoRows, this.NTables.Use.NoCols);
				int num7 = this.NTables.Use.NoRows - 1;
				for (int l = 0; l <= num7; l++)
				{
					int num8 = this.NTables.Use.NoCols - 1;
					for (int m = 0; m <= num8; m++)
					{
						matrix8[l, m] = this.NTables.Use[l, m];
					}
				}
				matrix10 = matrix8 * Matrix.Inverse(Matrix.diag(Matrix.ColSum(matrix8)));
				this.RegUse = this.Use * Matrix.diag(this.epsilonReg);
				if (this.RegMethod == eMethod.Income2)
				{
					Vector vector7 = new Vector(vector2.Count);
					bool flag = false;
				}
				this.PFI_n = new Vector(this.FinDem.NoRows);
				this.PFI_r = new Vector(this.FinDem.NoRows);
				this.CPI_n = new Vector(this.FinDem.NoRows);
				this.CPI_r = new Vector(this.FinDem.NoRows);
				this.X_n = new Vector(this.FinDem.NoRows);
				this.X_r = new Vector(this.FinDem.NoRows);
				this.PCE_n = new Vector(this.FinDem.NoRows);
				this.PCE_r = new Vector(this.FinDem.NoRows);
				vector8 = new Vector(this.FinDem.NoRows);
				vector9 = new Vector(this.FinDem.NoRows);
				vector10 = new Vector(this.FinDem.NoRows);
				num9 = 0.0;
			}
			double num10 = this.RegionSelected.DisposableIncome / this.DisposableIncome;
			double num11 = this.RegionSelected.PersonalIncome / this.PersonalIncome;
			vector8 = Matrix.ColSum(this.RegMake);
			vector9 = Matrix.ColSum(this.NTables.Make());
			num9 = Vector.sum(Matrix.ColSum(this.RegMake)) / Vector.sum(Matrix.ColSum(this.NTables.Make()));
			checked
			{
				int num12 = this.FinDem.NoRows - 1;
				for (int n = 0; n <= num12; n++)
				{
					if (vector9[n] != 0.0)
					{
						vector10[n] = unchecked(vector8[n] / vector9[n]);
					}
					else
					{
						vector10[n] = 0.0;
					}
				}
				int num13 = this.FinDem.NoRows - 1;
				for (int num14 = 0; num14 <= num13; num14++)
				{
					this.PFI_n[num14] = this.get_FinDemVector(num14, 1);
					this.CPI_n[num14] = this.get_FinDemVector(num14, 2);
					this.X_n[num14] = this.get_FinDemVector(num14, 3);
					this.PCE_n[num14] = this.get_FinDemVector(num14, 0);
					matrix[num14, 0] = this.get_FinDemVector(num14, 4);
					matrix[num14, 1] = this.get_FinDemVector(num14, 5);
					matrix[num14, 2] = this.get_FinDemVector(num14, 6);
					matrix[num14, 3] = this.get_FinDemVector(num14, 7);
					matrix3[num14, 0] = this.get_FinDemVector(num14, 8);
					matrix3[num14, 1] = this.get_FinDemVector(num14, 9);
				}
				double num15 = unchecked(Vector.sum(vector2 + vector4 + vector3) / BEAData.GDP_T_Sum);
				if (this.worksum.IsRegionalizationUserDefined)
				{
					int num16 = this.FinDem.NoRows - 1;
					for (int num17 = 0; num17 <= num16; num17++)
					{
						unchecked
						{
							this.PFI_r[num17] = num9 * this.PFI_n[num17];
							this.CPI_r[num17] = num9 * this.CPI_n[num17];
							this.X_r[num17] = vector10[num17] * this.X_n[num17];
							matrix2[num17, 0] = num15 * (this.RegionSelected.TotFedExp / this.TotFedExp_2005) * matrix[num17, 0];
							matrix4[num17, 0] = num15 * (this.RegionSelected.TotSLEExp / this.TotSLExp_2008) * matrix3[num17, 0];
							matrix2[num17, 1] = num15 * (this.RegionSelected.TotFedExp / this.TotFedExp_2005) * matrix[num17, 1];
							matrix4[num17, 1] = num15 * (this.RegionSelected.TotSLEExp / this.TotSLExp_2008) * matrix3[num17, 1];
							matrix2[num17, 2] = num15 * (this.RegionSelected.TotFedExp / this.TotFedExp_2005) * matrix[num17, 2];
							matrix2[num17, 3] = num15 * (this.RegionSelected.TotFedExp / this.TotFedExp_2005) * matrix[num17, 3];
						}
					}
				}
				else
				{
					int num18 = this.FinDem.NoRows - 1;
					for (int num19 = 0; num19 <= num18; num19++)
					{
						unchecked
						{
							this.PFI_r[num19] = num9 * this.PFI_n[num19];
							this.CPI_r[num19] = num9 * this.CPI_n[num19];
							this.X_r[num19] = vector10[num19] * this.X_n[num19];
							matrix2[num19, 0] = this.RegionSelected.TotFedExp / this.TotFedExp_2005 * matrix[num19, 0];
							matrix4[num19, 0] = this.RegionSelected.TotSLEExp / this.TotSLExp_2008 * matrix3[num19, 0];
							matrix2[num19, 1] = this.RegionSelected.TotFedExp / this.TotFedExp_2005 * matrix[num19, 1];
							matrix4[num19, 1] = this.RegionSelected.TotSLEExp / this.TotSLExp_2008 * matrix3[num19, 1];
							matrix2[num19, 2] = this.RegionSelected.TotFedExp / this.TotFedExp_2005 * matrix[num19, 2];
							matrix2[num19, 3] = this.RegionSelected.TotFedExp / this.TotFedExp_2005 * matrix[num19, 3];
						}
					}
				}
				if (this.RegMethod == eMethod.ValueAdded | this.RegMethod == eMethod.Income2)
				{
					int num20 = this.PCE_r.Count - 1;
					for (int num21 = 0; num21 <= num20; num21++)
					{
						this.PCE_r[num21] = unchecked(num11 * this.PCE_n[num21]);
					}
				}
				double num22 = unchecked(Vector.sum(vector2) / BEAData.EC_T_Sum);
				if (this.worksum.IsRegionalizationUserDefined)
				{
					int num23 = this.PCE_r.Count - 1;
					for (int num24 = 0; num24 <= num23; num24++)
					{
						this.PCE_r[num24] = unchecked(num22 * this.RegionSelected.PCE_Table[num24]);
					}
				}
				else
				{
					this.PCE_r = this.RegionSelected.PCE_Table;
				}
				double num25 = 0.0;
				int num26 = this.RegFinDem.NoRows - 1;
				for (int num27 = 0; num27 <= num26; num27++)
				{
					this.RegFinDem[num27, 1] = this.PFI_r[num27];
					this.RegFinDem[num27, 2] = this.CPI_r[num27];
					this.RegFinDem[num27, 3] = this.X_r[num27];
					this.RegFinDem[num27, 0] = this.PCE_r[num27];
					this.RegFinDem[num27, 4] = matrix2[num27, 0];
					this.RegFinDem[num27, 5] = matrix2[num27, 1];
					this.RegFinDem[num27, 6] = matrix2[num27, 2];
					this.RegFinDem[num27, 7] = matrix2[num27, 3];
					this.RegFinDem[num27, 8] = matrix4[num27, 0];
					this.RegFinDem[num27, 9] = matrix4[num27, 1];
				}
				this.RegMake = this.addNCIScrapToRegMake(ref this.RegMake, ref this.RegNCI, ref this.RegScrap);
				bool flag2 = true;
				int num28 = this.Import.NoCols - 1;
				for (int num29 = 0; num29 <= num28; num29++)
				{
					this.RegFinDem[num29, 3] = this.X_r[num29];
					double num30 = 0.0;
					double num31 = this.RegUse.RowSum(num29);
					double num32 = this.RegFinDem.RowSum(num29);
					double num33 = this.RegMake.ColSum(num29);
					unchecked
					{
						num30 = num31 + num32 - num33;
						double num34 = this.X_r[num29];
						bool flag3 = false;
						if (num30 >= 0.0)
						{
							flag3 = true;
							vector[num29] = num30;
						}
						else
						{
							vector[num29] = 0.0;
							this.X_r[num29] = this.X_r[num29] - num30;
						}
						this.RegImports[0, num29] = vector[num29];
					}
				}
				int num35 = vector8.Count - 1;
				for (int num36 = 0; num36 <= num35; num36++)
				{
					if (vector8[num36] == 0.0)
					{
						vector8[num36] = 1E-07;
					}
				}
				Vector vector11 = new Vector(this.FinDem.NoRows);
				int num37 = vector11.Count - 1;
				for (int num38 = 0; num38 <= num37; num38++)
				{
					unchecked
					{
						vector11[num38] = 0.1 * this.CHRatio * vector8[num38] + 0.9 * this.CHRatio * this.X_r[num38];
						this.RegImports[0, num38] = this.RegImports[0, num38] + vector11[num38];
						this.X_r[num38] = this.X_r[num38] + vector11[num38];
						this.RegFinDem[num38, 3] = this.X_r[num38];
					}
				}
				this.regdatmed = this.RegMake;
				string[] array = this.dispHdrsRow = new string[this.RegMake.NoRows + 1];
				this.isDataRegionalized = true;
				this.worksum.dataregionalized = "Yes";
				this.isRequirementTablesCalculated = false;
				int num39 = this.RegVA.NoCols - 1;
				for (int num40 = 0; num40 <= num39; num40++)
				{
					this.RegVA[0, num40] = vector2[num40];
					this.RegVA[1, num40] = vector4[num40];
					this.RegVA[2, num40] = vector3[num40];
				}
			}
		}

		public void ExtractScrapFromMake()
		{
			checked
			{
				int col = this.NTables.getIndustryCount() - 2;
				this.Scrap = new Matrix(1, this.NTables.getIndustryCount());
				int num = this.NTables.getIndustryCount() - 1;
				for (int i = 0; i <= num; i++)
				{
					this.Scrap[0, i] = this.NTables.Make()[i, col];
				}
			}
		}

		public void ExtractNCIFromMake()
		{
			checked
			{
				int col = this.NTables.getIndustryCount() - 1;
				this.NCI = new Matrix(1, this.NTables.getIndustryCount());
				int num = this.NTables.getIndustryCount() - 1;
				for (int i = 0; i <= num; i++)
				{
					this.NCI[0, i] = this.NTables.Make()[i, col];
				}
			}
		}

		public void putNCIToMake()
		{
			checked
			{
				int col = this.NTables.Make().NoCols - 1;
				int num = this.NTables.getIndustryCount() - 1;
				for (int i = 0; i <= num; i++)
				{
					this.NTables.Make()[i, col] = this.NCI[0, i];
				}
			}
		}

		public Matrix addNCIScrapToRegMake(ref Matrix m, ref Matrix rnci, ref Matrix rscrap)
		{
			checked
			{
				int col = rnci.NoCols - 1;
				int col2 = rnci.NoCols - 2;
				int num = rnci.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					m[i, col] = rnci[0, i];
					m[i, col2] = rscrap[0, i];
				}
				return m;
			}
		}

		public void putScrapToMake()
		{
			checked
			{
				int col = this.NTables.Make().NoCols - 2;
				int num = this.NTables.getIndustryCount() - 1;
				for (int i = 0; i <= num; i++)
				{
					this.NTables.Make()[i, col] = this.Scrap[0, i];
				}
			}
		}

		public void RemoveScrapFromMake()
		{
			checked
			{
				int num = this.NTables.Make().NoCols - 2;
				int num2 = this.NTables.Make().NoRows - 1;
				for (int i = 0; i <= num2; i++)
				{
					this.NTables.Make()[i, num] = 0.0;
					this.NTables.Make()[i, num + 1] = 0.0;
				}
			}
		}

		public void Calculate()
		{
			int num = default(int);
			int num6 = default(int);
			try
			{
				ProjectData.ClearProjectError();
				num = 2;
				this.RemoveScrapFromMake();
				this._q = Matrix.ColSum(this.NTables.Make());
				this.s_N = Matrix.ColSum(this.NTables.Make()) + Matrix.ColSum(this.NTables.Import);
				checked
				{
					int num2 = this._q.Count - 1;
					for (int i = 0; i <= num2; i++)
					{
						if (this._q[i] == 0.0)
						{
							this._q[i] = 999888999888.0;
						}
						if (this.s_N[i] == 0.0)
						{
							this.s_N[i] = 999888999888.0;
						}
					}
					this._g_N = Matrix.RowSum(this.NTables.Make()) + this.Scrap1;
					int num3 = this._g_N.Count - 1;
					for (int j = 0; j <= num3; j++)
					{
						if (this._g_N[j] == 0.0)
						{
							this._g_N[j] = 999888999888.0;
						}
					}
					this._B = Matrix.ScaleDivCol(this.Use, this._g_N);
					this._D = Matrix.ScaleDivCol(this.NTables.Make(), this._q);
					this._DT = Matrix.ScaleDivCol(this.NTables.Make(), this.s_N);
					this._p = this.Scrap1 / this._g_N;
					int num4 = this._p.Count - 1;
					for (int k = 0; k <= num4; k++)
					{
						if (double.IsNaN(this._p[k]))
						{
							this._p[k] = 0.0;
						}
					}
					this._W = Matrix.Inverse(Matrix.I(this.NTables.getIndustryCount()) - Matrix.diag(this._p)) * this._D;
					this._WT = Matrix.Inverse(Matrix.I(this.NTables.getIndustryCount()) - Matrix.diag(this._p)) * this._DT;
					this.r_c = this._q / this.s_N;
					Vector col = this._q - this._B * this._D * this._q;
					string text = "";
					text = this.CheckColRowSums(new Matrix[2]
					{
						this.NTables.Make(),
						Matrix.Append(this.NTables.Use, col, new Vector(0))
					}, false);
					if (text.Length > 0)
					{
						text = "Inconsistent data in Make and Use tables.\r\nPlease correct.\r\n" + text;
						Interaction.MsgBox(text, MsgBoxStyle.OkOnly, null);
					}
					else
					{
						MyProject.Forms.frmMain.Table_Label.Text = "Requirements Tables Generated";
						this.worksum.requirementsTables = "Yes";
						this.isRequirementTablesCalculated = true;
					}
					goto end_IL_0001;
				}
				IL_0350:
				MsgBoxResult num5 = Interaction.MsgBox(Information.Err().Description + "\r\n" + Information.Err().Source + Information.Err().ToString(), MsgBoxStyle.OkOnly, null);
				goto end_IL_0001;
				IL_0383:
				num6 = -1;
				switch (num)
				{
				case 2:
					goto IL_0350;
				default:
					goto IL_03b9;
				}
				end_IL_0001:;
			}
			catch (Exception obj) when (obj is Exception & num != 0 & num6 == 0)
			{
				ProjectData.SetProjectError((Exception)obj);
				/*Error near IL_03b7: Could not find block for branch target IL_0383*/;
			}
			if (num6 != 0)
			{
				ProjectData.ClearProjectError();
			}
			return;
			IL_03b9:
			throw ProjectData.CreateProjectError(-2146828237);
		}

		public void IndImpactType1()
		{
			this.dgIndImp = Matrix.Inverse(Matrix.I(this.NTables.getIndustryCount()) - this.Ao) * this.deltaFDInd;
			Vector colVector = this.dgIndImp.getColVector(0);
			Vector vector = this.EC / this.g * colVector;
			this.dgIndImpEC = vector.toColMatrix();
			Vector vector2 = (!this.worksum.dataregionalized.Contains("Yes")) ? (this.NTables.Employment / this.g * colVector) : (this.NTables.RegEmployment / this.g * colVector);
			this.dgIndImpEMP = vector2.toColMatrix();
			Vector vector3 = this.T / this.g * colVector;
			this.dgIndImpT = vector3.toColMatrix();
			Vector vector4 = this.GOS / this.g * colVector;
			this.dgIndImpGOS = vector4.toColMatrix();
			Vector vect = this.EC + this.T + this.GOS;
			Vector vector5 = vect / this.g * colVector;
			this.dgIndImpVA = vector5.toColMatrix();
		}

		public void IndImpactType2()
		{
			checked
			{
				this.dgIndImp = Matrix.Inverse(Matrix.I(this.NTables.getIndustryCount() + 1) - this.Ac) * this.deltaFDInd;
				Vector colVector = this.dgIndImp.getColVector(0);
				Vector vector = new Vector(this.g.Count + 1);
				int num = this.g.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					vector[i] = unchecked(this.g[i] + 0.0);
				}
				vector[this.g.Count] = Vector.sum(this.EC);
				Vector vector2 = this.EC.Clone();
				vector2.ReDimPreserve(this.EC.Count + 1);
				vector2[this.EC.Count] = 0.0;
				Vector vector3 = this.T.Clone();
				vector3.ReDimPreserve(this.T.Count + 1);
				vector3[this.T.Count] = 0.0;
				Vector vector4 = this.GOS.Clone();
				vector4.ReDimPreserve(this.GOS.Count + 1);
				vector4[this.GOS.Count] = 0.0;
				Vector vector5 = new Vector(this.NTables.Employment.Count);
				vector5 = ((!this.worksum.dataregionalized.Contains("Yes")) ? this.NTables.Employment.Clone() : this.NTables.RegEmployment.Clone());
				vector5.ReDimPreserve(this.NTables.Employment.Count + 1);
				vector5[this.NTables.Employment.Count] = 0.0;
				Vector vector6 = vector2 / vector * colVector;
				this.dgIndImpEC = vector6.toColMatrix();
				Vector vector7 = vector5 / vector * colVector;
				this.dgIndImpEMP = vector7.toColMatrix();
				Vector vector8 = vector3 / vector * colVector;
				this.dgIndImpT = vector8.toColMatrix();
				Vector vector9 = vector4 / vector * colVector;
				this.dgIndImpGOS = vector9.toColMatrix();
				Vector vect = vector3 + vector4;
				Vector vector10 = vect / vector * colVector;
				this.dgIndImpVA = vector10.toColMatrix();
			}
		}

		public void CommImpact_Type1()
		{
			this.dqCommImp = Matrix.Inverse(Matrix.I(this.NTables.getCommodityCount()) - this.Bo * this.WoT) * this.deltaFDComm;
			this.dgCommImp = this.Wo * Matrix.Inverse(Matrix.I(this.NTables.getCommodityCount()) - this.Bo * this.WoT) * this.deltaFDComm;
			Vector colVector = this.dgCommImp.getColVector(0);
			Vector vector = this.EC / this.g * colVector;
			this.dgCommImpEC = vector.toColMatrix();
			Vector vector2 = (!this.worksum.dataregionalized.Contains("Yes")) ? (this.NTables.Employment / this.g * colVector) : (this.NTables.RegEmployment / this.g * colVector);
			this.dgCommImpEMP = vector2.toColMatrix();
			Vector vector3 = this.T / this.g * colVector;
			this.dgCommImpT = vector3.toColMatrix();
			Vector vector4 = this.GOS / this.g * colVector;
			this.dgCommImpGOS = vector4.toColMatrix();
			Vector vect = this.EC + this.T + this.GOS;
			Vector vector5 = vect / this.g * colVector;
			this.dgCommImpVA = vector5.toColMatrix();
		}

		public void CommImpact_Type2()
		{
			checked
			{
				Matrix matrix = new Matrix(this.deltaFDComm.NoRows + 1, this.deltaFDComm.NoCols);
				int num = this.deltaFDComm.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					int num2 = this.deltaFDComm.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						matrix[i, j] = this.deltaFDComm[i, j];
					}
				}
				matrix[matrix.NoRows - 1, 0] = MyProject.Forms.frmMain.DD;
				try
				{
					Matrix matrix3;
					unchecked
					{
						Matrix matrix2 = (Matrix)this.Bc;
						matrix3 = (Matrix)Operators.MultiplyObject(this.Bc, 1);
						Matrix matrix4 = (Matrix)Operators.MultiplyObject(this.Wc, 1);
					}
					int num3 = matrix3.NoRows - 1;
					for (int k = 0; k <= num3; k++)
					{
						matrix3[k, matrix3.NoCols - 1] = unchecked(matrix3[k, checked(matrix3.NoCols - 1)] * 1.0);
					}
					this.dqCommImp = Matrix.Inverse(Matrix.I(this.NTables.getCommodityCount() + 1) - unchecked((Matrix)Operators.MultiplyObject(matrix3, this.WcT))) * matrix;
					this.dgCommImp = unchecked((Matrix)this.Wc) * Matrix.Inverse(Matrix.I(this.NTables.getCommodityCount() + 1) - unchecked((Matrix)Operators.MultiplyObject(matrix3, this.WcT))) * matrix;
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.ToString(), MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
				Vector vector = new Vector(this.g.Count + 1);
				int num4 = this.g.Count - 1;
				for (int l = 0; l <= num4; l++)
				{
					vector[l] = unchecked(this.g[l] + 0.0);
				}
				vector[this.g.Count] = 0.0;
				Vector vector2 = this.EC.Clone();
				vector2.ReDimPreserve(this.EC.Count + 1);
				vector2[this.EC.Count] = 0.0;
				Vector vector3 = this.T.Clone();
				vector3.ReDimPreserve(this.T.Count + 1);
				vector3[this.T.Count] = 0.0;
				Vector vector4 = this.GOS.Clone();
				vector4.ReDimPreserve(this.GOS.Count + 1);
				vector4[this.GOS.Count] = 0.0;
				int num5 = (!this.worksum.dataregionalized.Contains("Yes")) ? this.NTables.Employment.Count : this.NTables.RegEmployment.Count;
				Vector vector5 = new Vector(num5);
				vector5 = ((!this.worksum.dataregionalized.Contains("Yes")) ? this.NTables.Employment.Clone() : this.NTables.RegEmployment.Clone());
				vector5.ReDimPreserve(num5 + 1);
				vector5[num5] = 0.0;
				Vector colVector = this.dgCommImp.getColVector(0);
				Vector vector6 = vector2 / vector * colVector;
				this.dgCommImpEC = vector6.toColMatrix();
				Vector vector7 = vector5 / vector * colVector;
				this.dgCommImpEMP = vector7.toColMatrix();
				Vector vector8 = vector3 / vector * colVector;
				this.dgCommImpT = vector8.toColMatrix();
				Vector vector9 = vector4 / vector * colVector;
				this.dgCommImpGOS = vector9.toColMatrix();
				Vector vect = vector2 + vector3 + vector4;
				Vector vector10 = vect / vector * colVector;
				this.dgCommImpVA = vector10.toColMatrix();
			}
		}

		public string CheckColRowSums(Matrix[] Tbls, bool bAdjustGOS = false)
		{
			string result = "";
			Vector vector = Matrix.ColSum(this.NTables.Use) + Matrix.ColSum(this.NTables.VA);
			Vector vector2 = Matrix.RowSum(this.NTables.Make()) + this.Scrap1;
			Vector vector3 = Matrix.ColSum(this.Import);
			Vector vector4 = new Vector(this.Scrap1.Count);
			Vector vector5;
			int num2;
			checked
			{
				int num = vector4.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					vector4[i] = vector3[i];
				}
				vector5 = Matrix.RowSum(this.NTables.Make()) + this.Scrap1 + vector4;
				num2 = Math.Min(vector.Count - 1, vector2.Count - 1);
			}
			for (int j = 0; j <= num2; j = checked(j + 1))
			{
				double num3 = Math.Abs(vector[j] - vector2[j]) / vector[j];
				double num4 = Math.Abs(vector[j] - vector5[j]) / vector[j];
			}
			return result;
		}

		public BEAData()
		{
			this.isEdited = false;
			this.isFromRegionalData = false;
			this.N_BASE_STATE = 61;
			this.CHRatio = 0.1;
			this.StateIDs = new StateCodeNames();
			this.RegStateData = new RegionalData();
			this.RegionSelected = new StateData("AL", this.N_BASE_STATE);
			this.GovernmentAggStatus = new GenDataStatus();
			this.eDataState = DataState.Empty;
			this.userNCI = false;
			this.userScrap = false;
			this.DIFFCORECCTION = 0.001;
			this.Selections = new bool[22];
			this.VAProvided = new bool[4];
			this.UseAbbrHdrs = false;
			this.isDataRegionalized = false;
			this.isRequirementTablesCalculated = false;
			this.PFI_r = new Vector(1);
			this.PFI_n = new Vector(1);
			this.CPI_r = new Vector(1);
			this.CPI_n = new Vector(1);
			this.X_r = new Vector(1);
			this.X_n = new Vector(1);
			this.PCE_r = new Vector(1);
			this.PCE_n = new Vector(1);
			this.FormatGeneral = "#,##0.00";
			this.decimalsFormGen = 0;
			this.FormatCoefficients = "#,##0.000";
			this.decimalsFormCoeff = 0;
			this.FormatRounded = "#,##0";
			this.PersonalIncome = 0.0;
			this.DisposableIncome = 0.0;
			this.TotFedExp_2005 = 0.0;
			this.TotSLExp_2008 = 0.0;
			this._e = new Vector(1);
			this._q = new Vector(1);
			this._g_N = new Vector(1);
			this.g_R = new Vector(1);
			this.s_N = new Vector(1);
			this.s_R = new Vector(1);
			this.r_c = new Vector(1);
			this._B = new Matrix(1, 1);
			this._D = new Matrix(1, 1);
			this._DT = new Matrix(1, 1);
			this._p = new Vector(1);
			this._W = new Matrix(1, 1);
			this._WT = new Matrix(1, 1);
			this.dgIndImp = new Matrix(1, 1);
			this.deltaFDInd = new Matrix(1, 1);
			this.dgIndImpEC = new Matrix(1, 1);
			this.dgIndImpT = new Matrix(1, 1);
			this.dgIndImpGOS = new Matrix(1, 1);
			this.dgIndImpEMP = new Matrix(1, 1);
			this.dgIndImpVA = new Matrix(1, 1);
			this.dgCommImpEC = new Matrix(1, 1);
			this.dgCommImpT = new Matrix(1, 1);
			this.dgCommImpGOS = new Matrix(1, 1);
			this.dgCommImpEMP = new Matrix(1, 1);
			this.dgCommImpVA = new Matrix(1, 1);
			this.regdatinp = new Vector(1);
			this.regdatmed = new Matrix(1, 1);
			this.dqCommImp = new Matrix(1, 1);
			this.dgCommImp = new Matrix(1, 1);
			this.deltaFDComm = new Matrix(1, 1);
			this._X = new Vector(1);
			this.Regionalizer = new Vector(1);
			this.EmploymentReg = new Vector(1);
			this.dblFill = new Matrix[1];
			this.dblFill[0] = new Matrix(0, 0);
			this.NTables = new NatTables();
			this.stateCodes = new StateCodeNames();
		}

		private void WriteVals(StreamWriter objWriter, Matrix tbl)
		{
			checked
			{
				int num = tbl.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					int num2 = tbl.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						objWriter.Write("\t" + Conversions.ToString(tbl[i, j]));
					}
					objWriter.Write("\r\n");
				}
			}
		}

		private void WriteVals(StreamWriter objWriter, string[] tbl)
		{
			objWriter.WriteLine("\t" + string.Join("\t", tbl));
		}

		private void WriteVals(StreamWriter objWriter, Vector tbl)
		{
			checked
			{
				int num = tbl.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					objWriter.Write("\t" + Conversions.ToString(tbl[i]));
				}
				objWriter.Write("\r\n");
			}
		}

		public bool LoadModel(string URL = "")
		{
			if (URL.Length == 0)
			{
				OpenFileDialog openFileDialog = new OpenFileDialog();
				openFileDialog.Title = "Select Workspace to Load";
				openFileDialog.InitialDirectory = FileSystem.CurDir();
				openFileDialog.Filter = "Workspace files (*.wkspc)|*.wkspc";
				openFileDialog.FilterIndex = 1;
				openFileDialog.RestoreDirectory = true;
				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					URL = openFileDialog.FileName;
				}
			}
			checked
			{
				if (URL.Length > 0)
				{
					List<string> list = new List<string>();
					StreamReader streamReader = new StreamReader(URL);
					string text = "";
					while (!streamReader.EndOfStream)
					{
						string text2 = streamReader.ReadLine();
						if (text2.StartsWith("\t"))
						{
							list.Add(text2.Substring(1));
						}
						else
						{
							if (Operators.CompareString(text, "hdrVA", false) != 0)
							{
								if (Operators.CompareString(text, "hdrComm", false) != 0)
								{
									if (Operators.CompareString(text, "hdrInd", false) != 0)
									{
										if (Operators.CompareString(text, "hdrFinDem", false) != 0)
										{
											if (Operators.CompareString(text, "", false) != 0)
											{
												double[] array = LoadRegionalData.strArr2Dbl(list[0].Split('\t'));
												Matrix matrix = new Matrix(list.Count, array.GetUpperBound(0) + 1);
												Matrix matrix2 = new Matrix(list.Count, array.GetUpperBound(0) + 1);
												int num = list.Count - 1;
												for (int i = 0; i <= num; i++)
												{
													array = LoadRegionalData.strArr2Dbl(list[i].Split('\t'));
													int upperBound = array.GetUpperBound(0);
													for (int j = 0; j <= upperBound; j++)
													{
														matrix[i, j] = array[j];
													}
												}
												switch (ComputeStringHash(text))
												{
												case 3339451269u:
													if (Operators.CompareString(text, "B", false) == 0)
													{
														this._B = matrix;
														break;
													}
													goto default;
												case 3238785555u:
													if (Operators.CompareString(text, "D", false) == 0)
													{
														this._D = matrix;
														break;
													}
													goto default;
												case 3524005078u:
													if (Operators.CompareString(text, "W", false) == 0)
													{
														this._W = matrix;
														break;
													}
													goto default;
												case 3122420884u:
													if (Operators.CompareString(text, "Use", false) == 0)
													{
														this.NTables.SetUse(ref matrix);
														break;
													}
													goto default;
												case 1215032783u:
													if (Operators.CompareString(text, "Make", false) == 0)
													{
														this.NTables.SetMake(ref matrix);
														break;
													}
													goto default;
												case 4134655041u:
													if (Operators.CompareString(text, "Imp", false) == 0)
													{
														this.NTables.Import = matrix;
														break;
													}
													goto default;
												case 3794644288u:
													if (Operators.CompareString(text, "FinDem", false) == 0)
													{
														this.NTables.FinDem = matrix;
														break;
													}
													goto default;
												case 1677347064u:
													if (Operators.CompareString(text, "VA", false) == 0)
													{
														this.NTables.VA = matrix;
														break;
													}
													goto default;
												case 4004942930u:
													if (Operators.CompareString(text, "Scrap", false) == 0)
													{
														this.NTables.Scrap = matrix;
														break;
													}
													goto default;
												case 3758891744u:
													if (Operators.CompareString(text, "e", false) == 0)
													{
														this._e = new Vector(array);
														break;
													}
													goto default;
												case 4094444124u:
													if (Operators.CompareString(text, "q", false) == 0)
													{
														this._q = new Vector(array);
														break;
													}
													goto default;
												case 3792446982u:
													if (Operators.CompareString(text, "g", false) == 0)
													{
														this._g_N = new Vector(array);
														break;
													}
													goto default;
												case 4111221743u:
													if (Operators.CompareString(text, "p", false) == 0)
													{
														this._p = new Vector(array);
														break;
													}
													goto default;
												case 3708558887u:
													if (Operators.CompareString(text, "X", false) == 0)
													{
														this._X = new Vector(array);
														break;
													}
													goto default;
												case 3067654867u:
													if (Operators.CompareString(text, "Employment", false) == 0)
													{
														this.NTables.Employment = new Vector(array);
														break;
													}
													goto default;
												case 4250689464u:
													if (Operators.CompareString(text, "DisposableIncome", false) == 0)
													{
														this.DisposableIncome = array[0];
														break;
													}
													goto default;
												case 1962358934u:
													if (Operators.CompareString(text, "DataState", false) == 0)
													{
														this.eDataState = unchecked((DataState)checked((byte)Math.Round(array[0])));
														break;
													}
													goto default;
												default:
													Interaction.MsgBox("Unknown variable: " + text, MsgBoxStyle.OkOnly, null);
													break;
												}
											}
										}
										else
										{
											this.hdrFinDem = list[0].ToString().Split('\t');
										}
									}
									else
									{
										this.NTables.hdrInd = list[0].ToString().Split('\t');
									}
								}
								else
								{
									this.hdrComm = list[0].ToString().Split('\t');
								}
							}
							else
							{
								this.hdrVA = list[0].ToString().Split('\t');
							}
							text = text2.Substring(0, text2.IndexOf("\t"));
							list = new List<string>();
							list.Add(text2.Substring(text2.IndexOf("\t") + 1));
						}
					}
					return true;
				}
				bool result = default(bool);
				return result;
			}
		}

		public bool SaveModel()
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Title = "Enter Filename to Save Model";
			saveFileDialog.InitialDirectory = FileSystem.CurDir();
			saveFileDialog.Filter = "Workspace files (*.wkspc)|*.wkspc";
			saveFileDialog.FilterIndex = 1;
			saveFileDialog.RestoreDirectory = true;
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName, false);
				streamWriter.Write("Use");
				this.WriteVals(streamWriter, this.NTables.Use);
				streamWriter.Write("Make");
				this.WriteVals(streamWriter, this.NTables.Make());
				streamWriter.Write("VA");
				this.WriteVals(streamWriter, this.NTables.VA);
				streamWriter.Write("FinDem");
				this.WriteVals(streamWriter, this.NTables.FinDem);
				streamWriter.Write("Imp");
				this.WriteVals(streamWriter, this.NTables.Import);
				streamWriter.Write("Scrap");
				this.WriteVals(streamWriter, this.NTables.Scrap);
				streamWriter.Write("hdrInd");
				this.WriteVals(streamWriter, this.NTables.hdrInd);
				streamWriter.Write("hdrComm");
				this.WriteVals(streamWriter, this.NTables.hdrComm);
				streamWriter.Write("hdrFinDem");
				this.WriteVals(streamWriter, this._hdrFinDem);
				streamWriter.Write("hdrVA");
				this.WriteVals(streamWriter, this._hdrVA);
				streamWriter.Write("DisposableIncome");
				this.WriteVals(streamWriter, new string[1]
				{
					Conversions.ToString(this.DisposableIncome)
				});
				streamWriter.Write("Employment");
				this.WriteVals(streamWriter, this.NTables.Employment);
				if (this.isRequirementTablesCalculated)
				{
					streamWriter.Write("q");
					this.WriteVals(streamWriter, this._q);
					streamWriter.Write("g");
					this.WriteVals(streamWriter, this._g_N);
					streamWriter.Write("B");
					this.WriteVals(streamWriter, this._B);
					streamWriter.Write("D");
					this.WriteVals(streamWriter, this._D);
					streamWriter.Write("p");
					this.WriteVals(streamWriter, this._p);
					streamWriter.Write("W");
					this.WriteVals(streamWriter, this._W);
				}
				streamWriter.Write("DataState");
				this.WriteVals(streamWriter, new string[1]
				{
					Conversions.ToString((byte)this.eDataState)
				});
				streamWriter.WriteLine("EndOfFile\t");
				streamWriter.Close();
				return true;
			}
			bool result = default(bool);
			return result;
		}

		public double sumFinDem(int row, int[] cols)
		{
			double num = default(double);
			foreach (int colIndex in cols)
			{
				num += this.get_FinDemVector(row, colIndex);
			}
			return num;
		}

		public Vector sumFinDem(int[] cols)
		{
			Vector vector = new Vector(this.NTables.getCommodityCount());
			foreach (int colIndex in cols)
			{
				vector += this.get_FinDemVector(colIndex);
			}
			return vector;
		}

		public Vector IxIOutputo()
		{
			return Matrix.ColSum(this.LeonOpen);
		}

		public Vector IxIInco()
		{
			return Matrix.ColSum((this.EC / this.g).toRowMatrix() * this.LeonOpen);
		}

		public Vector IxIEmpo()
		{
			return Matrix.ColSum((this.NTables.Employment / this.g).toRowMatrix() * this.LeonOpen);
		}

		public Vector IxCOutputo()
		{
			return Matrix.ColSum(this.IxC_TRo);
		}

		public Vector IxCInco()
		{
			return Matrix.ColSum((this.EC / this.g).toRowMatrix() * this.IxC_TRo);
		}

		public Vector IxCEmpo()
		{
			return Matrix.ColSum((this.NTables.Employment / this.g).toRowMatrix() * this.IxC_TRo);
		}

		public Vector CxCOutputo()
		{
			return Matrix.ColSum(this.CxC_TRo);
		}

		public Vector CxCInco()
		{
			return Matrix.ColSum((this.EC / this.g).toRowMatrix() * this.CxC_TRo);
		}

		public Vector CxCEmpo()
		{
			return Matrix.ColSum((this.NTables.Employment / this.g).toRowMatrix() * this.CxC_TRo);
		}

		public Vector IxIOutputc()
		{
			return Matrix.ColSum(this.LeonClosed);
		}

		public Vector IxIIncc()
		{
			return Matrix.ColSum(Matrix.Append((this.EC / this.g).toRowMatrix(), new Vector(1), new Vector(0)) * this.LeonClosed);
		}

		public Vector IxIEmpc()
		{
			return Matrix.ColSum(Matrix.Append((this.EC / this.g).toRowMatrix(), new Vector(1), new Vector(0)) * this.LeonClosed);
		}

		public Vector IxCOutputc()
		{
			return Matrix.ColSum(this.IxC_TRc);
		}

		public Vector IxCIncc()
		{
			return Matrix.ColSum(Matrix.Append((this.EC / this.g).toRowMatrix(), new Vector(1), new Vector(0)) * this.IxC_TRc);
		}

		public Vector IxCEmpc()
		{
			return Matrix.ColSum(Matrix.Append((this.EC / this.g).toRowMatrix(), new Vector(1), new Vector(0)) * this.IxC_TRc);
		}

		public Vector CxCOutputc()
		{
			return Matrix.ColSum(this.CxC_TRc);
		}

		public Vector CxCIncc()
		{
			return Matrix.ColSum(Matrix.Append((this.EC / this.g).toRowMatrix(), new Vector(1), new Vector(0)) * this.CxC_TRc);
		}

		public Vector CxCEmpc()
		{
			return Matrix.ColSum(Matrix.Append((this.EC / this.g).toRowMatrix(), new Vector(1), new Vector(0)) * this.CxC_TRc);
		}

		public void SetupHeaders()
		{
			ArrayList arrayList = new ArrayList();
			ArrayList arrayList2 = new ArrayList();
			string[] array = this.hdrCommBase;
			foreach (string value in array)
			{
				arrayList.Add(value);
			}
			string[] hdrIndBase = this.NTables.hdrIndBase;
			foreach (string value2 in hdrIndBase)
			{
				arrayList2.Add(value2);
			}
			if (this.GovernmentAggStatus.getAggLevel() == GenDataStatus.dataStatusType.ImputedGov)
			{
				string[] array2 = this.hdrCommAdjGov;
				foreach (string value3 in array2)
				{
					arrayList.Add(value3);
				}
				string[] hdrIndAdjGov = this.NTables.hdrIndAdjGov;
				foreach (string value4 in hdrIndAdjGov)
				{
					arrayList2.Add(value4);
				}
			}
			else if (this.GovernmentAggStatus.getAggLevel() == GenDataStatus.dataStatusType.FedStateLocal)
			{
				string[] array3 = this.hdrCommFedStateLocGov;
				foreach (string value5 in array3)
				{
					arrayList.Add(value5);
				}
				string[] hdrIndFedStateLocGov = this.NTables.hdrIndFedStateLocGov;
				foreach (string value6 in hdrIndFedStateLocGov)
				{
					arrayList2.Add(value6);
				}
			}
			else if (this.GovernmentAggStatus.getAggLevel() == GenDataStatus.dataStatusType.TotalGovernment)
			{
				string[] array4 = this.hdrCommTotGov;
				foreach (string value7 in array4)
				{
					arrayList.Add(value7);
				}
				string[] hdrIndTotGov = this.NTables.hdrIndTotGov;
				foreach (string value8 in hdrIndTotGov)
				{
					arrayList2.Add(value8);
				}
			}
			arrayList.Add(this.hdrScrap[0]);
			arrayList.Add(this.hdrNoncompImp[0]);
			this.hdrComm = (string[])arrayList.ToArray(typeof(string));
			this.NTables.hdrInd = (string[])arrayList2.ToArray(typeof(string));
		}

		public void CleanHeaders()
		{
			char c = '"';
			checked
			{
				int num = this.hdrComm.Length - 1;
				for (int i = 0; i <= num; i++)
				{
					if (this.hdrComm[i][0] == c)
					{
						string text = "";
						int num2 = this.hdrComm[i].Length - 2;
						for (int j = 1; j <= num2; j++)
						{
							text += Conversions.ToString(this.hdrComm[i][j]);
						}
						this.hdrComm[i] = text;
					}
				}
				int num3 = this.NTables.hdrInd.Length - 1;
				for (int k = 0; k <= num3; k++)
				{
					if (this.NTables.hdrInd[k][0] == c)
					{
						string text2 = "";
						int num4 = this.NTables.hdrInd[k].Length - 2;
						for (int l = 1; l <= num4; l++)
						{
							text2 += Conversions.ToString(this.NTables.hdrInd[k][l]);
						}
						this.NTables.hdrInd[k] = text2;
					}
				}
				int num5 = this.hdrFinDem.Length - 1;
				for (int m = 0; m <= num5; m++)
				{
					if (this.hdrFinDem[m][0] == c)
					{
						string text3 = "";
						int num6 = this.hdrFinDem[m].Length - 2;
						for (int n = 1; n <= num6; n++)
						{
							text3 += Conversions.ToString(this.hdrFinDem[m][n]);
						}
						this.hdrFinDem[m] = text3;
					}
				}
				int num7 = this.hdrVA.Length - 1;
				for (int num8 = 0; num8 <= num7; num8++)
				{
					if (this.hdrVA[num8][0] == c)
					{
						string text4 = "";
						int num9 = this.hdrVA[num8].Length - 2;
						for (int num10 = 1; num10 <= num9; num10++)
						{
							text4 += Conversions.ToString(this.hdrVA[num8][num10]);
						}
						this.hdrVA[num8] = text4;
					}
				}
			}
		}

		public void updateSummary()
		{
		}

		public void ChangeGovAggregationLevel()
		{
			this.NTables.CalcAgg();
			switch (this.GovernmentAggStatus.getAggLevel())
			{
			case GenDataStatus.dataStatusType.ImputedGov:
				break;
			case GenDataStatus.dataStatusType.FedStateLocal:
				break;
			case GenDataStatus.dataStatusType.TotalGovernment:
				break;
			}
		}

		public void ConsolidateData()
		{
			Matrix matrix = new Matrix(this.FinDem.NoRows, this.FinDem.NoCols);
			Vector vector = new Vector(this.Import.NoCols);
			checked
			{
				int num = vector.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					vector[i] = this.Import[0, i];
				}
				try
				{
					Vector v = Matrix.RowSum(this.Use);
					Vector v2 = Matrix.ColSum(this.NTables.Make());
					Vector v3 = Matrix.RowSum(this.FinDem);
					Vector vector2 = v + v3 + vector;
					Vector vector3 = v2 - vector2;
					double num2 = 0.0;
					int num3 = vector2.Count - 1;
					for (int j = 0; j <= num3; j++)
					{
					}
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox("exception in processing edit of final demand", MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
		}

		public void ImportExportUpdate()
		{
			Vector vector = new Vector(this.Import.NoCols);
			this.X_n = new Vector(this.FinDem.NoRows);
			checked
			{
				int num = this.FinDem.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					this.X_n[i] = this.get_FinDemVector(i, 3);
				}
				object left = 0;
				int num2 = this.FinDem.NoRows - 1;
				for (int j = 0; j <= num2; j++)
				{
					this.set_FinDemDouble(j, 3, this.X_n[j]);
					double num3 = 0.0;
					double num4 = this.Use.RowSum(j);
					double num5 = this.FinDem.RowSum(j);
					double num6 = this.NTables.Make().ColSum(j);
					num3 = unchecked(num4 + num5 - num6);
					bool flag = false;
					flag = true;
					vector[j] = num3;
					this.set_FinDemDouble(j, 3, this.X_n[j]);
					if (!(num3 >= 0.0))
					{
						left = Operators.AddObject(left, 1);
					}
					this.Import[0, j] = vector[j];
				}
			}
		}

        public static uint ComputeStringHash(string text)
        {
            uint hashCode = 0;
            if (text != null)
            {
                hashCode = unchecked((uint)2166136261);

                int i = 0;
                goto start;

                again:
                hashCode = unchecked((text[i] ^ hashCode) * 16777619);
                i = i + 1;

                start:
                if (i < text.Length)
                    goto again;
            }
            return hashCode;
        }
    }
}
