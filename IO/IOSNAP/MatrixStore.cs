using Matrix_Lib;

namespace IOSNAP
{
	public class MatrixStore
	{
		public Matrix myMatrix;

		public string myName;

		public bool myFlag;

		public MatrixStore(ref string s, ref Matrix m)
		{
			this.myName = s;
			this.myMatrix = m;
			this.myFlag = true;
		}
	}
}
