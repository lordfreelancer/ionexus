using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class frmAgg : Form
	{
		private class clsSect
		{
			public clsAgg agg;

			public string Name;

			public string Abbrev;

			public bool isGov;

			public clsSect()
			{
				this.isGov = false;
			}

			public clsSect(string tn, bool tg, clsAgg tagg, string tabbrev)
			{
				this.isGov = false;
				this.Name = tn;
				this.isGov = tg;
				this.agg = tagg;
				this.Abbrev = tabbrev;
			}
		}

		private class clsAgg
		{
			public string Name;

			public int count;

			public int index;

			public string Abbrev;

			public bool isGov;

			public clsAgg()
			{
				this.Name = "";
				this.count = 0;
				this.index = -1;
				this.Abbrev = "";
				this.isGov = false;
			}

			public clsAgg(string _name, string _Abbrev, bool _isGov)
			{
				this.Name = "";
				this.count = 0;
				this.index = -1;
				this.Abbrev = "";
				this.isGov = false;
				this.Name = _name;
				this.Abbrev = _Abbrev;
				this.isGov = _isGov;
			}

			public clsAgg(string _name, bool _isGov, int _ind, int _cnt, string _Abbrev)
			{
				this.Name = "";
				this.count = 0;
				this.index = -1;
				this.Abbrev = "";
				this.isGov = false;
				this.Name = _name;
				this.Abbrev = _Abbrev;
				this.isGov = _isGov;
				this.count = _cnt;
				this.index = _ind;
			}
		}

		private IContainer components;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("lbAggs")]
		public ListBox lbAggs;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("lbMaster")]
		public ListBox lbMaster;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("lbSectInAgg")]
		public ListBox lbSectInAgg;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("cmdAddAgg")]
		public Button cmdAddAgg;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("cmdRemAgg")]
		public Button cmdRemAgg;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("cmdEditAgg")]
		public Button cmdEditAgg;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("cmdAddSect")]
		public Button cmdAddSect;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("cmdRemSect")]
		public Button cmdRemSect;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("cmdCancel")]
		public Button cmdCancel;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("cmdSet")]
		public Button cmdSet;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("cmdSave")]
		public Button cmdSave;

		//[CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		//[AccessedThroughProperty("Button1")]
		public Button Button1;

		private BEAData _BEAdat;

		private List<clsAgg> clsAggs;

		private List<clsSect> clsSects;

		public int[] Agg65;

		public int[] Agg67;

		public string[] hdrInd;

		public string[] hdrComm;

		public string[] hdrIndAbbr;
        private Label label;
        private Label label2;
        private Label label3;
        public string[] hdrCommAbbr;

		//internal virtual ListBox lbAggs
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._lbAggs;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.lbAggs_SelectedIndexChanged;
		//		ListBox lbAggs = this._lbAggs;
		//		if (lbAggs != null)
		//		{
		//			lbAggs.SelectedIndexChanged -= value2;
		//		}
		//		this._lbAggs = value;
		//		lbAggs = this._lbAggs;
		//		if (lbAggs != null)
		//		{
		//			lbAggs.SelectedIndexChanged += value2;
		//		}
		//	}
		//}

		//internal virtual ListBox lbMaster
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual ListBox lbSectInAgg
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual Button cmdAddAgg
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cmdAddAgg;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cmdAddAgg_Click;
		//		Button cmdAddAgg = this._cmdAddAgg;
		//		if (cmdAddAgg != null)
		//		{
		//			cmdAddAgg.Click -= value2;
		//		}
		//		this._cmdAddAgg = value;
		//		cmdAddAgg = this._cmdAddAgg;
		//		if (cmdAddAgg != null)
		//		{
		//			cmdAddAgg.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button cmdRemAgg
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cmdRemAgg;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cmdRemAgg_Click;
		//		Button cmdRemAgg = this._cmdRemAgg;
		//		if (cmdRemAgg != null)
		//		{
		//			cmdRemAgg.Click -= value2;
		//		}
		//		this._cmdRemAgg = value;
		//		cmdRemAgg = this._cmdRemAgg;
		//		if (cmdRemAgg != null)
		//		{
		//			cmdRemAgg.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button cmdEditAgg
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cmdEditAgg;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cmdEditAgg_Click;
		//		Button cmdEditAgg = this._cmdEditAgg;
		//		if (cmdEditAgg != null)
		//		{
		//			cmdEditAgg.Click -= value2;
		//		}
		//		this._cmdEditAgg = value;
		//		cmdEditAgg = this._cmdEditAgg;
		//		if (cmdEditAgg != null)
		//		{
		//			cmdEditAgg.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button cmdAddSect
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cmdAddSect;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cmdAddSect_Click;
		//		Button cmdAddSect = this._cmdAddSect;
		//		if (cmdAddSect != null)
		//		{
		//			cmdAddSect.Click -= value2;
		//		}
		//		this._cmdAddSect = value;
		//		cmdAddSect = this._cmdAddSect;
		//		if (cmdAddSect != null)
		//		{
		//			cmdAddSect.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button cmdRemSect
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cmdRemSect;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cmdRemSect_Click;
		//		Button cmdRemSect = this._cmdRemSect;
		//		if (cmdRemSect != null)
		//		{
		//			cmdRemSect.Click -= value2;
		//		}
		//		this._cmdRemSect = value;
		//		cmdRemSect = this._cmdRemSect;
		//		if (cmdRemSect != null)
		//		{
		//			cmdRemSect.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button cmdCancel
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cmdCancel;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cmdCancel_Click;
		//		Button cmdCancel = this._cmdCancel;
		//		if (cmdCancel != null)
		//		{
		//			cmdCancel.Click -= value2;
		//		}
		//		this._cmdCancel = value;
		//		cmdCancel = this._cmdCancel;
		//		if (cmdCancel != null)
		//		{
		//			cmdCancel.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button cmdSet
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cmdSet;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cmdSet_Click;
		//		Button cmdSet = this._cmdSet;
		//		if (cmdSet != null)
		//		{
		//			cmdSet.Click -= value2;
		//		}
		//		this._cmdSet = value;
		//		cmdSet = this._cmdSet;
		//		if (cmdSet != null)
		//		{
		//			cmdSet.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button cmdSave
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cmdSave;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.SaveAggregationScheme;
		//		Button cmdSave = this._cmdSave;
		//		if (cmdSave != null)
		//		{
		//			cmdSave.Click -= value2;
		//		}
		//		this._cmdSave = value;
		//		cmdSave = this._cmdSave;
		//		if (cmdSave != null)
		//		{
		//			cmdSave.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button Button1
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._Button1;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.LoadButton_Click;
		//		Button button = this._Button1;
		//		if (button != null)
		//		{
		//			button.Click -= value2;
		//		}
		//		this._Button1 = value;
		//		button = this._Button1;
		//		if (button != null)
		//		{
		//			button.Click += value2;
		//		}
		//	}
		//}

		public BEAData BEADat
		{
			get
			{
				return this._BEAdat;
			}
			set
			{
				this._BEAdat = value;
				checked
				{
					this.Agg65 = new int[this._BEAdat.NTables.hdrInd.GetUpperBound(0) + 1];
					this.Agg67 = new int[this.Agg65.GetUpperBound(0) + 2 + 1];
					int upperBound = this.Agg65.GetUpperBound(0);
					int i;
					for (i = 0; i <= upperBound; i++)
					{
						this.Agg65[i] = i;
						this.Agg67[i] = i;
					}
					this.Agg67[i] = i;
					this.Agg67[i + 1] = i + 1;
				}
				this.hdrInd = (string[])this.BEADat.NTables.hdrInd.Clone();
				this.hdrComm = (string[])this.BEADat.hdrComm.Clone();
			}
		}

		public frmAgg()
		{
			base.Load += this.frmAgg_Load;
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.lbAggs = new System.Windows.Forms.ListBox();
            this.lbMaster = new System.Windows.Forms.ListBox();
            this.lbSectInAgg = new System.Windows.Forms.ListBox();
            this.cmdAddAgg = new System.Windows.Forms.Button();
            this.cmdRemAgg = new System.Windows.Forms.Button();
            this.cmdEditAgg = new System.Windows.Forms.Button();
            this.cmdAddSect = new System.Windows.Forms.Button();
            this.cmdRemSect = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdSet = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbAggs
            // 
            this.lbAggs.FormattingEnabled = true;
            this.lbAggs.ItemHeight = 37;
            this.lbAggs.Location = new System.Drawing.Point(39, 56);
            this.lbAggs.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.lbAggs.Name = "lbAggs";
            this.lbAggs.Size = new System.Drawing.Size(319, 152);
            this.lbAggs.TabIndex = 0;
            this.lbAggs.SelectedIndexChanged += new System.EventHandler(this.lbAggs_SelectedIndexChanged);
            // 
            // lbMaster
            // 
            this.lbMaster.FormattingEnabled = true;
            this.lbMaster.ItemHeight = 37;
            this.lbMaster.Location = new System.Drawing.Point(38, 284);
            this.lbMaster.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.lbMaster.Name = "lbMaster";
            this.lbMaster.Size = new System.Drawing.Size(229, 226);
            this.lbMaster.TabIndex = 1;
            // 
            // lbSectInAgg
            // 
            this.lbSectInAgg.FormattingEnabled = true;
            this.lbSectInAgg.ItemHeight = 37;
            this.lbSectInAgg.Location = new System.Drawing.Point(393, 284);
            this.lbSectInAgg.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.lbSectInAgg.Name = "lbSectInAgg";
            this.lbSectInAgg.Size = new System.Drawing.Size(234, 226);
            this.lbSectInAgg.TabIndex = 2;
            // 
            // cmdAddAgg
            // 
            this.cmdAddAgg.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAddAgg.Location = new System.Drawing.Point(452, 56);
            this.cmdAddAgg.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdAddAgg.Name = "cmdAddAgg";
            this.cmdAddAgg.Size = new System.Drawing.Size(112, 39);
            this.cmdAddAgg.TabIndex = 6;
            this.cmdAddAgg.Text = "Add Sector";
            this.cmdAddAgg.UseVisualStyleBackColor = true;
            this.cmdAddAgg.Click += new System.EventHandler(this.cmdAddAgg_Click);
            // 
            // cmdRemAgg
            // 
            this.cmdRemAgg.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRemAgg.Location = new System.Drawing.Point(452, 105);
            this.cmdRemAgg.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdRemAgg.Name = "cmdRemAgg";
            this.cmdRemAgg.Size = new System.Drawing.Size(112, 39);
            this.cmdRemAgg.TabIndex = 7;
            this.cmdRemAgg.Text = "Remove";
            this.cmdRemAgg.UseVisualStyleBackColor = true;
            this.cmdRemAgg.Click += new System.EventHandler(this.cmdRemAgg_Click);
            // 
            // cmdEditAgg
            // 
            this.cmdEditAgg.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEditAgg.Location = new System.Drawing.Point(452, 154);
            this.cmdEditAgg.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdEditAgg.Name = "cmdEditAgg";
            this.cmdEditAgg.Size = new System.Drawing.Size(112, 39);
            this.cmdEditAgg.TabIndex = 8;
            this.cmdEditAgg.Text = "Rename";
            this.cmdEditAgg.UseVisualStyleBackColor = true;
            this.cmdEditAgg.Click += new System.EventHandler(this.cmdEditAgg_Click);
            // 
            // cmdAddSect
            // 
            this.cmdAddSect.Location = new System.Drawing.Point(306, 354);
            this.cmdAddSect.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdAddSect.Name = "cmdAddSect";
            this.cmdAddSect.Size = new System.Drawing.Size(52, 39);
            this.cmdAddSect.TabIndex = 9;
            this.cmdAddSect.Text = ">>";
            this.cmdAddSect.UseVisualStyleBackColor = true;
            this.cmdAddSect.Click += new System.EventHandler(this.cmdAddSect_Click);
            // 
            // cmdRemSect
            // 
            this.cmdRemSect.Location = new System.Drawing.Point(306, 403);
            this.cmdRemSect.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdRemSect.Name = "cmdRemSect";
            this.cmdRemSect.Size = new System.Drawing.Size(52, 39);
            this.cmdRemSect.TabIndex = 10;
            this.cmdRemSect.Text = "<<";
            this.cmdRemSect.UseVisualStyleBackColor = true;
            this.cmdRemSect.Click += new System.EventHandler(this.cmdRemSect_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCancel.Location = new System.Drawing.Point(515, 547);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(112, 39);
            this.cmdCancel.TabIndex = 11;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdSet
            // 
            this.cmdSet.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdSet.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSet.Location = new System.Drawing.Point(347, 547);
            this.cmdSet.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdSet.Name = "cmdSet";
            this.cmdSet.Size = new System.Drawing.Size(144, 39);
            this.cmdSet.TabIndex = 12;
            this.cmdSet.Text = "Aggregate";
            this.cmdSet.UseVisualStyleBackColor = true;
            this.cmdSet.Click += new System.EventHandler(this.cmdSet_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSave.Location = new System.Drawing.Point(113, 547);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(108, 39);
            this.cmdSave.TabIndex = 13;
            this.cmdSave.Text = "Save as";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.SaveAggregationScheme);
            // 
            // Button1
            // 
            this.Button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.Location = new System.Drawing.Point(238, 547);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(91, 39);
            this.Button1.TabIndex = 14;
            this.Button1.Text = "Load";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(34, 29);
            this.label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(235, 37);
            this.label.TabIndex = 3;
            this.label.Text = "Aggregate Sectors";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 254);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(295, 37);
            this.label2.TabIndex = 4;
            this.label2.Text = "Un-Aggregated Sectors";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(389, 254);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(319, 37);
            this.label3.TabIndex = 5;
            this.label3.Text = "Sectors within Aggregate";
            // 
            // frmAgg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 601);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.cmdSet);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdRemSect);
            this.Controls.Add(this.cmdAddSect);
            this.Controls.Add(this.cmdEditAgg);
            this.Controls.Add(this.cmdRemAgg);
            this.Controls.Add(this.cmdAddAgg);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label);
            this.Controls.Add(this.lbSectInAgg);
            this.Controls.Add(this.lbMaster);
            this.Controls.Add(this.lbAggs);
            this.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "frmAgg";
            this.Text = "Aggregation Form";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		private void frmAgg_Load(object sender, EventArgs e)
		{
			this.LoadData();
			this.PopulateForm();
		}

		private void LoadDataAfterAggLoad()
		{
			checked
			{
				int num = this.hdrInd.Length - 1;
				for (int i = 0; i <= num; i++)
				{
					bool flag = false;
					if (i > this.hdrInd.Length - 5)
					{
						flag = true;
					}
				}
			}
		}

		private unsafe void LoadData()
		{
			this.clsAggs = new List<clsAgg>();
			this.clsSects = new List<clsSect>();
			checked
			{
				int num = this.hdrInd.Length - 1;
				for (int i = 0; i <= num; i++)
				{
					bool isGov = false;
					if (i > this.hdrInd.Length - 5)
					{
						isGov = true;
					}
					this.clsAggs.Add(new clsAgg(this.hdrInd[i], "AA", isGov));
				}
				int upperBound = this.Agg65.GetUpperBound(0);
				for (int i = 0; i <= upperBound; i++)
				{
					clsSect clsSect = new clsSect();
					clsSect.agg = this.clsAggs[this.Agg65[i]];
					this.clsSects.Add(clsSect);
                    //ref int count;
                    //*(ref count = ref clsSect.agg.count) = count + 1;
                    clsSect.agg.count = clsSect.agg.count + 1;

                    clsSect.Name = this.BEADat.NTables.hdrInd[i];
					if (i > this.Agg65.Length - 5)
					{
						clsSect.isGov = true;
					}
					else
					{
						clsSect.isGov = false;
					}
				}
			}
		}

		private void PopulateForm()
		{
			this.PopulateAggregates();
			this.PopulateMaster();
		}

		private void PopulateAggregates()
		{
			this.lbSectInAgg.Items.Clear();
			this.lbAggs.Items.Clear();
			foreach (clsAgg clsAgg in this.clsAggs)
			{
				if (clsAgg.count >= 2)
				{
					this.lbAggs.Items.Add(clsAgg.Name);
				}
			}
		}

		private void PopulateMaster()
		{
			List<clsSect> list = this.clsSects;
			this.lbMaster.Items.Clear();
			this.clsSects = list;
			foreach (clsSect clsSect in this.clsSects)
			{
				if (clsSect.agg.count < 2)
				{
					this.lbMaster.Items.Add(clsSect.Name);
				}
			}
		}

		private void cmdAddSect_Click(object sender, EventArgs e)
		{
			if (this.lbAggs.SelectedItem != null && !string.IsNullOrEmpty(Conversions.ToString(this.lbMaster.SelectedItem)))
			{
				using (List<clsAgg>.Enumerator enumerator = this.clsAggs.GetEnumerator())
				{
					clsAgg current;
					do
					{
						if (!enumerator.MoveNext())
						{
							return;
						}
						current = enumerator.Current;
					}
					while (!current.Name.Equals(this.lbAggs.SelectedItem.ToString()));
					using (List<clsSect>.Enumerator enumerator2 = this.clsSects.GetEnumerator())
					{
						clsSect current2;
						do
						{
							if (!enumerator2.MoveNext())
							{
								return;
							}
							current2 = enumerator2.Current;
						}
						while (!(current2.Name.Equals(this.lbMaster.SelectedItem.ToString()) & !current2.isGov));
						this.clsAggs.Remove(current2.agg);
						current2.agg = current;
						this.lbSectInAgg.Items.Add(current2.Name);
						this.lbMaster.Items.Remove(current2.Name);
					}
				}
			}
		}

		private void cmdEditAgg_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(Conversions.ToString(this.lbAggs.SelectedItem)))
			{
				string text = Interaction.InputBox("Enter new Selected Aggregate Name", "Aggregate Title", this.lbAggs.SelectedItem.ToString(), -1, -1);
				if (text.Length > 0)
				{
					foreach (clsAgg clsAgg in this.clsAggs)
					{
						if (clsAgg.Name.Equals(this.lbAggs.SelectedItem.ToString()))
						{
							clsAgg.Name = text;
							clsAgg.Abbrev = text;
							break;
						}
					}
					int selectedIndex = this.lbAggs.SelectedIndex;
					this.lbAggs.Items.RemoveAt(selectedIndex);
					this.lbAggs.Items.Insert(selectedIndex, text);
					this.lbAggs.SelectedIndex = selectedIndex;
				}
			}
		}

		private void cmdCancel_Click(object sender, EventArgs e)
		{
			base.Close();
			base.Dispose();
		}

		private void lbAggs_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.lbSectInAgg.Items.Clear();
			if (this.lbAggs.SelectedItem != null)
			{
				foreach (clsSect clsSect in this.clsSects)
				{
					if (clsSect.agg.Name.Equals(this.lbAggs.SelectedItem.ToString()))
					{
						this.lbSectInAgg.Items.Add(clsSect.Name);
					}
				}
			}
		}

		private void cmdRemAgg_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(Conversions.ToString(this.lbAggs.SelectedItem)))
			{
				foreach (clsSect clsSect in this.clsSects)
				{
					if (clsSect.agg.Name.Equals(this.lbAggs.SelectedItem.ToString()))
					{
						clsAgg clsAgg = new clsAgg();
						clsAgg.Name = clsSect.Name;
						clsAgg.Abbrev = clsSect.Abbrev;
						clsSect.agg = clsAgg;
						this.clsAggs.Add(clsAgg);
					}
				}
				foreach (clsAgg clsAgg2 in this.clsAggs)
				{
					if (clsAgg2.Name.Equals(this.lbAggs.SelectedItem.ToString()))
					{
						this.clsAggs.Remove(clsAgg2);
						break;
					}
				}
				this.PopulateForm();
				this.lbAggs.Items.Remove(RuntimeHelpers.GetObjectValue(this.lbAggs.SelectedItem));
				this.lbSectInAgg.Items.Clear();
			}
		}

		private void cmdAddAgg_Click(object sender, EventArgs e)
		{
			string text = Interaction.InputBox("Enter Aggregate Sector Name", "Aggregate Title", "", -1, -1);
			if (text.Length > 0)
			{
				this.lbAggs.Items.Add(text);
				clsAgg clsAgg = new clsAgg();
				clsAgg.count = 2;
				clsAgg.Name = text;
				this.clsAggs.Add(clsAgg);
			}
			this.lbAggs.SelectedItem = text;
		}

		private void cmdRemSect_Click(object sender, EventArgs e)
		{
			if (this.lbSectInAgg.Items.Count > 0 && !string.IsNullOrEmpty(Conversions.ToString(this.lbSectInAgg.SelectedItem)) && this.clsSects.Count > 0)
			{
				foreach (clsSect clsSect in this.clsSects)
				{
					if (clsSect.Name.Equals(this.lbSectInAgg.SelectedItem.ToString()))
					{
						clsAgg clsAgg = new clsAgg();
						clsAgg.Name = clsSect.Name;
						clsSect.agg = clsAgg;
					}
				}
				this.lbSectInAgg.Items.Remove(RuntimeHelpers.GetObjectValue(this.lbSectInAgg.SelectedItem));
				this.PopulateMaster();
			}
		}

		private void cmdSet_Click(object sender, EventArgs e)
		{
			this.SetAgg();
			base.Close();
			this._BEAdat.worksum.dataaggregated = "Yes";
			this._BEAdat.isEdited = true;
			this._BEAdat.worksum.DataTimeStamp = DateAndTime.Now.ToString();
		}

		private void SetAgg()
		{
			int[] agg = this.Agg65;
			int num = 0;
			int num2 = default(int);
			checked
			{
				foreach (clsSect clsSect in this.clsSects)
				{
					if (clsSect.agg.index == -1)
					{
						clsSect.agg.index = num;
						num++;
					}
					agg[num2] = clsSect.agg.index;
					num2++;
				}
				string[] array = new string[num - 1 + 1];
				foreach (clsAgg clsAgg in this.clsAggs)
				{
					if (clsAgg.index > -1)
					{
						array[clsAgg.index] = clsAgg.Name;
					}
				}
				this.Agg65 = agg;
				this.BEADat.NTables.hdrInd = array;
				array = unchecked((string[])Utils.CopyArray(array, new string[checked(array.GetUpperBound(0) + 2 + 1)]));
				array[array.GetUpperBound(0)] = this.BEADat.hdrComm[this.BEADat.hdrComm.GetUpperBound(0)];
				array[array.GetUpperBound(0) - 1] = this.BEADat.hdrComm[this.BEADat.hdrComm.GetUpperBound(0) - 1];
				this.BEADat.hdrComm = array;
			}
			int[] arySrc = (int[])agg.Clone();
			arySrc = (int[])Utils.CopyArray(arySrc, new int[checked(agg.GetUpperBound(0) + 2 + 1)]);
			num2 = this.Max(agg);
			checked
			{
				arySrc[arySrc.GetUpperBound(0)] = num2 + 2;
				arySrc[arySrc.GetUpperBound(0) - 1] = num2 + 1;
				this.Agg67 = arySrc;
				int num3 = this.BEADat.N_INDUSTRIES - this.BEADat.NTables.hdrInd.Length;
				this.BEADat.N_INDUSTRIES = this.BEADat.NTables.hdrInd.Length;
				this.BEADat.N_COMMODITIES = this.BEADat.hdrComm.Length;
				this.BEADat.N_INDUSTRIES_BASE = this.BEADat.N_INDUSTRIES_BASE - num3;
				this.BEADat.Aggregate(agg, arySrc);
			}
		}

		private void SaveAggregationScheme(object sender, EventArgs e)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Title = "Enter Filename to Save Aggregation Scheme";
			saveFileDialog.InitialDirectory = FileSystem.CurDir();
			saveFileDialog.Filter = "agg files (*.agg)|*.agg|All files (*.*)|*.*";
			saveFileDialog.FilterIndex = 1;
			saveFileDialog.RestoreDirectory = true;
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				string fileName = saveFileDialog.FileName;
				string path = saveFileDialog.FileName + "_s2";
				string str = "Aggregation Scheme\r\n";
				str = str + fileName + "\r\n";
				str = str + DateAndTime.Now.ToString() + "\r\n";
				str = str + Conversions.ToString(this.clsAggs.Count) + "\r\n";
				str = str + Conversions.ToString(this.clsSects.Count) + "\r\n";
				str += "<ca>\r\n";
				foreach (clsAgg clsAgg in this.clsAggs)
				{
					str += "<sector>\r\n";
					str = str + clsAgg.Name + "\r\n" + Conversions.ToString(clsAgg.isGov) + "\r\n" + Conversions.ToString(clsAgg.index) + "\r\n" + Conversions.ToString(clsAgg.count) + "\r\n" + clsAgg.Abbrev + "\r\n";
					str += "</sector>\r\n";
				}
				str += "</ca>\r\n";
				str += "<sa>\r\n";
				foreach (clsSect clsSect in this.clsSects)
				{
					str += "<sector>\r\n";
					str = str + clsSect.Name + "\r\n" + Conversions.ToString(clsSect.isGov) + "\r\n" + clsSect.agg.Name + "\r\n" + clsSect.Abbrev + "\r\n";
					str += "</sector>\r\n";
				}
				str += "</sa>\r\n";
				try
				{
					FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
					binaryFormatter.Serialize(fileStream, str);
					fileStream.Close();
					ASCIIEncoding aSCIIEncoding = new ASCIIEncoding();
					byte[] bytes = aSCIIEncoding.GetBytes(str);
					string graph = Convert.ToBase64String(bytes);
					fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
					binaryFormatter.Serialize(fileStream, graph);
					fileStream.Close();
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
		}

		public unsafe bool LoadAgg(string URL = "")
		{
			if (URL.Length == 0)
			{
				OpenFileDialog openFileDialog = new OpenFileDialog();
				openFileDialog.Title = "Select Aggregation Scheme to Load";
				openFileDialog.InitialDirectory = FileSystem.CurDir();
				openFileDialog.Filter = "agg files (*.agg)|*.agg|All files (*.*)|*.*";
				openFileDialog.FilterIndex = 1;
				openFileDialog.RestoreDirectory = true;
				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					URL = openFileDialog.FileName;
				}
			}
			checked
			{
				bool result = default(bool);
				if (URL.Length > 0)
				{
					try
					{
						StreamReader streamReader = new StreamReader(URL);
						int i;
						while (!streamReader.EndOfStream)
						{
							string text = streamReader.ReadLine();
							if (text.Length > 2)
							{
								string left = text.Substring(0, text.IndexOf("\t"));
								text = text.Substring(text.IndexOf("\t") + 1);
								text = text.TrimEnd('\t', ' ');
								if (Operators.CompareString(left, "Agg65", false) != 0)
								{
									if (Operators.CompareString(left, "hdrInd", false) != 0)
									{
										if (Operators.CompareString(left, "hdrIndAbbr", false) == 0)
										{
											this.hdrIndAbbr = text.Split('\t');
										}
									}
									else
									{
										this.hdrInd = text.Split('\t');
									}
								}
								else
								{
									result = true;
									this.Agg65 = this.strArr2Int(text.Split('\t'));
									int num = Information.UBound(this.Agg65, 1);
									for (i = 0; i <= num; i++)
									{
                                        //ref int reference;
                                        //*(ref reference = ref this.Agg65[i]) = reference - 1;
                                        this.Agg65[i] = this.Agg65[i] - 1;

                                    }
									if (this.Agg65.GetUpperBound(0) != this.BEADat.NTables.hdrInd.GetUpperBound(0))
									{
										Interaction.MsgBox("The loaded aggregation scheme does not match the current industries", MsgBoxStyle.OkOnly, null);
										result = false;
										return result;
									}
								}
							}
						}
						this.Agg67 = this.Agg65;
                        //TODO Need to check logic
                        //ref int[] agg;
                        //*(ref agg = ref this.Agg67) = unchecked((int[])Utils.CopyArray(agg, new int[checked(this.Agg65.Length + 1 + 1)]));
                        this.Agg67 = unchecked((int[])Utils.CopyArray(this.Agg67, new int[checked(this.Agg65.Length + 1 + 1)]));
                        i = this.Max(this.Agg65);
						this.Agg67[this.Agg67.GetUpperBound(0) - 1] = i + 1;
						this.Agg67[this.Agg67.GetUpperBound(0)] = i + 2;
						this.hdrComm = this.hdrInd;
                        //ref string[] reference2;
                        //*(ref reference2 = ref this.hdrComm) = unchecked((string[])Utils.CopyArray(reference2, new string[checked(this.hdrComm.Length + 1 + 1)]));
                        this.hdrComm = unchecked((string[])Utils.CopyArray(hdrComm, new string[checked(this.hdrComm.Length + 1 + 1)]));
                        this.hdrComm[this.hdrComm.GetUpperBound(0) - 1] = "Non-Comparable Imports";
						this.hdrComm[this.hdrComm.GetUpperBound(0)] = "Scrap, used and secondhand goods";
						this.hdrCommAbbr = this.hdrIndAbbr;
                        //*(ref reference2 = ref this.hdrCommAbbr) = unchecked((string[])Utils.CopyArray(reference2, new string[checked(this.hdrCommAbbr.Length + 1 + 1)]));
                        this.hdrCommAbbr = unchecked((string[])Utils.CopyArray(hdrCommAbbr, new string[checked(this.hdrCommAbbr.Length + 1 + 1)]));
                        this.hdrCommAbbr[this.hdrCommAbbr.GetUpperBound(0) - 1] = "NCI";
						this.hdrCommAbbr[this.hdrCommAbbr.GetUpperBound(0)] = "Scrap";
						return result;
					}
					catch (IOException ex)
					{
						ProjectData.SetProjectError(ex);
						IOException ex2 = ex;
						if (Information.Err().Number == 57)
						{
							Interaction.MsgBox("Input data file appears to be in use, and cannot be opened\r\nPlease close " + URL, MsgBoxStyle.OkOnly, null);
						}
						else
						{
							Interaction.MsgBox("Unable to open \r\n" + URL + "\r\nCheck that it exists, and that it's not in use", MsgBoxStyle.OkOnly, null);
						}
						ProjectData.ClearProjectError();
						return result;
					}
				}
				return result;
			}
		}

		private string Int2StrPlus1(int obj)
		{
			return checked(obj + 1).ToString();
		}

		private int Max(int[] arr)
		{
			int num2 = default(int);
			foreach (int num in arr)
			{
				if (num > num2)
				{
					num2 = num;
				}
			}
			return num2;
		}

		private int[] strArr2Int(string[] arr)
		{
			checked
			{
				int[] array = new int[arr.GetUpperBound(0) + 1];
				int upperBound = arr.GetUpperBound(0);
				for (int i = 0; i <= upperBound; i++)
				{
					array[i] = int.Parse(arr[i]);
				}
				return array;
			}
		}

		private unsafe void LoadButton_Click(object sender, EventArgs e)
		{
			string text = "";
			OpenFileDialog openFileDialog = new OpenFileDialog();
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			string text2 = "";
			openFileDialog.Title = "Select Aggregation Scheme to Load";
			openFileDialog.InitialDirectory = FileSystem.CurDir();
			openFileDialog.Filter = "IO-SNAP aggregation data files (*.agg)|*.agg";
			openFileDialog.FilterIndex = 1;
			openFileDialog.RestoreDirectory = true;
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				text = openFileDialog.FileName;
				try
				{
					FileStream serializationStream = new FileStream(text, FileMode.Open, FileAccess.Read);
					string s = Conversions.ToString(binaryFormatter.Deserialize(serializationStream));
					byte[] bytes = Convert.FromBase64String(s);
					text2 = Encoding.ASCII.GetString(bytes);
					this.ProcessingAggregationLoad(ref text2);
					this.BEADat = this._BEAdat;
					this.Agg67 = this.Agg65;
                    //ref int[] agg;
                    //*(ref agg = ref this.Agg67) = (int[])Utils.CopyArray(agg, new int[checked(this.Agg65.Length + 1 + 1)]);
                    this.Agg67 = (int[])Utils.CopyArray(Agg67, new int[checked(this.Agg65.Length + 1 + 1)]);
                    object left = this.Max(this.Agg65);
					this.Agg67[checked(this.Agg67.GetUpperBound(0) - 1)] = Conversions.ToInteger(Operators.AddObject(left, 1));
					this.Agg67[this.Agg67.GetUpperBound(0)] = Conversions.ToInteger(Operators.AddObject(left, 2));
					this.PopulateFormV2();
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox("Error while reading aggregation file " + text + "\r\n" + ex2.Message + "\r\n" + ex2.ToString(), MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
		}

		private void PopulateFormV2()
		{
			this.PopulateAggregates();
			this.PopulateMaster();
		}

		private void ProcessingAggregationLoad(ref string SS)
		{
			object obj = this.CleanDataString(SS);
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			bool flag = false;
			bool flag2 = false;
			bool flag3 = false;
			int num4 = 0;
			string text = "";
			bool flag4 = false;
			int num5 = 0;
			int num6 = 0;
			string text2 = "";
			List<clsAgg> list = new List<clsAgg>();
			List<clsSect> list2 = new List<clsSect>();
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = ((IEnumerable)obj).GetEnumerator();
				checked
				{
					while (enumerator.MoveNext())
					{
						string text3 = Conversions.ToString(enumerator.Current);
						double a;
						switch (num)
						{
						case 3:
						{
							string s4 = text3;
							a = unchecked((double)num2);
							bool flag5 = double.TryParse(s4, out a);
							num2 = (int)Math.Round(a);
							if (!flag5)
							{
								break;
							}
							break;
						}
						case 4:
						{
							string s3 = text3;
							a = unchecked((double)num3);
							bool flag5 = double.TryParse(s3, out a);
							num3 = (int)Math.Round(a);
							if (!flag5)
							{
								break;
							}
							break;
						}
						default:
						{
							if (flag & flag3)
							{
								switch (num4)
								{
								case 0:
									text = text3;
									break;
								case 1:
									if (!bool.TryParse(text3, out flag4))
									{
										break;
									}
									break;
								case 2:
								{
									string s2 = text3;
									a = unchecked((double)num5);
									bool flag5 = double.TryParse(s2, out a);
									num5 = (int)Math.Round(a);
									if (!flag5)
									{
										break;
									}
									break;
								}
								case 3:
								{
									string s = text3;
									a = unchecked((double)num6);
									bool flag5 = double.TryParse(s, out a);
									num6 = (int)Math.Round(a);
									if (!flag5)
									{
										break;
									}
									break;
								}
								case 4:
								{
									text2 = text3;
									clsAgg item = new clsAgg(text, flag4, num5, num6, text2);
									list.Add(item);
									break;
								}
								}
								num4++;
							}
							if (flag2 & flag3)
							{
								clsAgg tagg = default(clsAgg);
								switch (num4)
								{
								case 0:
									text = text3;
									break;
								case 1:
									if (!bool.TryParse(text3, out flag4))
									{
										break;
									}
									break;
								case 2:
									foreach (clsAgg item3 in list)
									{
										if (string.Compare(item3.Name, text3) == 0)
										{
											tagg = item3;
										}
									}
									break;
								case 3:
								{
									text2 = text3;
									clsSect item2 = new clsSect(text, flag4, tagg, text2);
									list2.Add(item2);
									break;
								}
								}
								num4++;
							}
							string left = Strings.UCase(text3);
							if (Operators.CompareString(left, "<CA>", false) != 0)
							{
								if (Operators.CompareString(left, "</CA>", false) != 0)
								{
									if (Operators.CompareString(left, "<SECTOR>", false) != 0)
									{
										if (Operators.CompareString(left, "</SECTOR>", false) != 0)
										{
											if (Operators.CompareString(left, "<SA>", false) != 0)
											{
												if (Operators.CompareString(left, "</SA>", false) == 0)
												{
													flag2 = false;
												}
											}
											else
											{
												flag2 = true;
											}
										}
										else
										{
											flag3 = false;
										}
									}
									else
									{
										flag3 = true;
										num4 = 0;
									}
								}
								else
								{
									flag = false;
								}
							}
							else
							{
								flag = true;
							}
							break;
						}
						case 0:
						case 1:
						case 2:
							break;
						}
						num++;
					}
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			this.clsAggs = list;
			this.clsSects = list2;
		}

		private List<string> CleanDataString(string s)
		{
			List<string> list = new List<string>();
			string text = "";
			string str = "";
			int i = 0;
			for (string[] array = s.ToString().Split('\r'); i < array.Length; i = checked(i + 1))
			{
				text = array[i].Trim('\t', ' ', '\r', '\n', '"');
				if (!(text.StartsWith(new string(new char[1]
				{
					'#'
				})) | text.Length == 0))
				{
					list.Add(str + text);
				}
			}
			return list;
		}
	}
}
