using Matrix_Lib;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace IOSNAP
{
	public class TabContentHighlighted : TabPage
	{
		private enum DispTotal : byte
		{
			Yes,
			No,
			Unchanged
		}

		public DataGridView dgvnn;

		private string _frmTitle;

		private DispTotal DispTotals;

		public Size tabsize;

		public string dispformat;

		public ViewOptions options;

		private int cellwidth;

		public DispData dispdat;

		private bool bCoeff;

		private Vector colSum;

		public TabContentHighlighted(string frmTitle, byte p2)
		{
			this.cellwidth = 150;
			this.bCoeff = false;
			this._frmTitle = frmTitle;
			this.DispTotals = (DispTotal)p2;
			this.options = new ViewOptions();
		}

		public void setup()
		{
			this.Text = this._frmTitle;
			this.dgvnn = new DataGridView();
			base.Controls.Add(this.dgvnn);
			this.dgvnn.SelectionMode = DataGridViewSelectionMode.CellSelect;
			this.dgvnn.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvnn.Size = this.tabsize;
			this.dgvnn.RightToLeft = RightToLeft.No;
			this.dgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnn.RowHeadersWidth = 175;
			this.dgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvnn.RowTemplate.Height = 24;
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle.BackColor = SystemColors.Control;
			dataGridViewCellStyle.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvnn.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle2.BackColor = SystemColors.Window;
			dataGridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle2.Format = this.options.getGeneralFormat;
			dataGridViewCellStyle2.NullValue = null;
			dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
			this.dgvnn.DefaultCellStyle = dataGridViewCellStyle2;
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = SystemColors.Control;
			dataGridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle3.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = DataGridViewTriState.True;
			this.dgvnn.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			switch (this.DispTotals)
			{
			case DispTotal.No:
				TabContent.DispTot = false;
				break;
			case DispTotal.Yes:
				TabContent.DispTot = true;
				break;
			}
			if (this.bCoeff | TabContent.DispTot)
			{
				this.colSum = this.dispdat.getColSum();
			}
			DataGridView dataGridView = this.dgvnn;
			dataGridView.Columns.Clear();
			dataGridView.DefaultCellStyle.Format = this.options.getGeneralFormat;
			string[] hdrsCol = this.dispdat.hdrsCol;
			foreach (string text in hdrsCol)
			{
				dataGridView.Columns.Add(text, text);
			}
			checked
			{
				if (this.dispdat.BaseTable.NoCols > this.dispdat.hdrsCol.Length)
				{
					int num = this.dispdat.hdrsCol.Length;
					int num2 = this.dispdat.BaseTable.NoCols - 1;
					for (int j = num; j <= num2; j++)
					{
						dataGridView.Columns.Add("Col " + Conversions.ToString(j), "Col " + Conversions.ToString(j));
					}
				}
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = dataGridView.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = unchecked((DataGridViewColumn)enumerator.Current);
						dataGridViewColumn.ReadOnly = true;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				dataGridView.RowHeadersVisible = true;
				int num3 = this.dispdat.BaseTable.NoRows - 1;
				for (int j = 0; j <= num3; j++)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					int num4 = this.dispdat.BaseTable.NoCols - 1;
					for (int k = 0; k <= num4; k++)
					{
						if (this.bCoeff)
						{
							dataGridViewRow.Cells[k].Value = unchecked(this.dispdat.BaseTable[j, k] / this.colSum[k]);
						}
						else
						{
							dataGridViewRow.Cells[k].Value = this.dispdat.BaseTable[j, k];
						}
					}
					dataGridViewRow.HeaderCell.Value = this.dispdat.hdrsRow[j];
					dataGridView.Rows.Add(dataGridViewRow);
				}
				if (unchecked((object)this.dispdat.ExtraCols) != null)
				{
					int noCols = this.dispdat.BaseTable.NoCols;
					int num5 = this.dispdat.ExtraCols.NoCols - 1;
					for (int l = 0; l <= num5; l++)
					{
						int index = dataGridView.Columns.Add(this.dispdat.hdrsXCols[l], this.dispdat.hdrsXCols[l]);
						dataGridView.Columns[index].DefaultCellStyle.BackColor = Color.LightBlue;
						int num6 = this.dispdat.ExtraCols.NoRows - 1;
						for (int j = 0; j <= num6; j++)
						{
							dataGridView.Rows[j].Cells[noCols + l].Value = this.dispdat.ExtraCols[j, l];
						}
						dataGridView.Columns[index].ReadOnly = true;
					}
				}
				if (unchecked((object)this.dispdat.ExtraRows) != null)
				{
					int num7 = this.dispdat.ExtraRows.NoRows - 1;
					for (int j = 0; j <= num7; j++)
					{
						DataGridViewRow dataGridViewRow = new DataGridViewRow();
						dataGridViewRow.CreateCells(this.dgvnn);
						dataGridViewRow.DefaultCellStyle.BackColor = Color.LightBlue;
						dataGridViewRow.HeaderCell.Value = this.dispdat.hdrsXRows[j];
						int num8 = this.dispdat.ExtraRows.NoCols - 1;
						for (int m = 0; m <= num8; m++)
						{
							if (this.bCoeff)
							{
								dataGridViewRow.Cells[m].Value = unchecked(this.dispdat.ExtraRows[j, m] / this.colSum[m]);
							}
							else
							{
								dataGridViewRow.Cells[m].Value = this.dispdat.ExtraRows[j, m];
							}
						}
						dataGridView.Rows.Add(dataGridViewRow);
					}
				}
				IEnumerator enumerator2 = default(IEnumerator);
				try
				{
					enumerator2 = dataGridView.Columns.GetEnumerator();
					while (enumerator2.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn2 = unchecked((DataGridViewColumn)enumerator2.Current);
						dataGridViewColumn2.Width = this.cellwidth;
						if (this.dispdat.isSortable)
						{
							dataGridViewColumn2.SortMode = DataGridViewColumnSortMode.Automatic;
						}
						else
						{
							dataGridViewColumn2.SortMode = DataGridViewColumnSortMode.NotSortable;
						}
					}
				}
				finally
				{
					if (enumerator2 is IDisposable)
					{
						(enumerator2 as IDisposable).Dispose();
					}
				}
				dataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
				dataGridView = null;
				if (TabContent.DispTot)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					dataGridViewRow.ReadOnly = true;
					dataGridViewRow.DefaultCellStyle.BackColor = Color.LightGray;
					dataGridViewRow.HeaderCell.Value = "Column Sum";
					int num9 = this.dispdat.BaseTable.NoCols - 1;
					for (int n = 0; n <= num9; n++)
					{
						dataGridViewRow.Cells[n].Value = this.colSum[n];
					}
					this.dgvnn.Rows.Add(dataGridViewRow);
					int count = this.dgvnn.Columns.Count;
					int index2 = this.dgvnn.Columns.Add("RowSum", "Row Sum");
					this.dgvnn.Columns[index2].DefaultCellStyle.BackColor = Color.LightGray;
					Vector vector = this.dispdat.BaseTable.RowSum();
					if (unchecked((object)this.dispdat.ExtraCols) != null)
					{
						int num10 = vector.Count - 1;
						for (int j = 0; j <= num10; j++)
						{
							int num11 = this.dispdat.ExtraCols.NoCols - 1;
							for (int num12 = 0; num12 <= num11; num12++)
							{
								Vector vector2;
								int row;
								(vector2 = vector)[row = j] = unchecked(vector2[row] + this.dispdat.ExtraCols[j, num12]);
							}
						}
					}
					int num13 = vector.Count - 1;
					for (int j = 0; j <= num13; j++)
					{
						this.dgvnn.Rows[j].Cells[count].Value = vector[j];
					}
				}
			}
		}
	}
}
