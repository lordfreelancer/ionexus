using Matrix_Lib;
using System;

namespace IOSNAP
{
	[Serializable]
	public class GenDataStatus : ICloneable
	{
		public enum dataStatusType
		{
			ImputedGov,
			FedStateLocal,
			TotalGovernment
		}

		public enum dataEmpStatus
		{
			FTE_BASED,
			JOB_BASED
		}

		public const double COMPENSATION_RATE_SCALE_FACTOR = 1000000.0;

		private static dataStatusType aggLevel;

		private dataStatusType _tmpAgglevel;

		private static dataEmpStatus FTE_Type;

		public object Clone()
		{
			return new GenDataStatus();
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Clone
			return this.Clone();
		}

		public object getCompRateScaleFactorVECTOR(int n)
		{
			Vector vector = new Vector(n);
			checked
			{
				int num = vector.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					vector[i] = 1000000.0;
				}
				return vector;
			}
		}

		public void Initiate()
		{
			GenDataStatus.aggLevel = dataStatusType.ImputedGov;
			this._tmpAgglevel = dataStatusType.ImputedGov;
			GenDataStatus.FTE_Type = dataEmpStatus.FTE_BASED;
		}

		public dataStatusType getTMP_AggLevel()
		{
			return this._tmpAgglevel;
		}

		public void setTMP_AggLevel(dataStatusType v1)
		{
			this._tmpAgglevel = v1;
		}

		public dataStatusType getAggLevel()
		{
			return GenDataStatus.aggLevel;
		}

		public void setAggLevel(dataStatusType v1)
		{
			GenDataStatus.aggLevel = v1;
		}

		public dataEmpStatus getFTE_Type()
		{
			return GenDataStatus.FTE_Type;
		}

		public void setFTE_TYPE(dataEmpStatus v1)
		{
			GenDataStatus.FTE_Type = v1;
		}
	}
}
