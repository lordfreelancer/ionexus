using Matrix_Lib;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class dialogImpactEntryForm : Form
	{
		public class DispData
		{
			public Matrix BaseTable;

			public Matrix ExtraCols;

			public Matrix ExtraRows;

			public string[] hdrsCol;

			public string[] hdrsXCols;

			public string[] hdrsXRows;

			public ArrayList hdrsRow;

			public string mainHeader;

			public bool isReadOnly;

			public DispData()
			{
				this.hdrsRow = new ArrayList();
				this.isReadOnly = false;
			}

			public Vector getColSum()
			{
				Vector vector = Matrix.ColSum(this.BaseTable);
				if ((object)this.ExtraRows != null)
				{
					checked
					{
						int num = this.ExtraRows.NoRows - 1;
						for (int i = 0; i <= num; i++)
						{
							int num2 = this.ExtraRows.NoCols - 1;
							for (int j = 0; j <= num2; j++)
							{
								Vector vector2;
								int row;
								(vector2 = vector)[row = j] = unchecked(vector2[row] + this.ExtraRows[i, j]);
							}
						}
					}
				}
				return vector;
			}
		}

		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("TableLayoutPanel1")]
		private TableLayoutPanel _TableLayoutPanel1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OK_Button")]
		private Button _OK_Button;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Cancel_Button")]
		private Button _Cancel_Button;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("dgv3")]
		private DataGridView _dgv3;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		public DispData data;

		internal virtual TableLayoutPanel TableLayoutPanel1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button OK_Button
		{
			[CompilerGenerated]
			get
			{
				return this._OK_Button;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OK_Button_Click;
				Button oK_Button = this._OK_Button;
				if (oK_Button != null)
				{
					oK_Button.Click -= value2;
				}
				this._OK_Button = value;
				oK_Button = this._OK_Button;
				if (oK_Button != null)
				{
					oK_Button.Click += value2;
				}
			}
		}

		internal virtual Button Cancel_Button
		{
			[CompilerGenerated]
			get
			{
				return this._Cancel_Button;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Cancel_Button_Click;
				Button cancel_Button = this._Cancel_Button;
				if (cancel_Button != null)
				{
					cancel_Button.Click -= value2;
				}
				this._Cancel_Button = value;
				cancel_Button = this._Cancel_Button;
				if (cancel_Button != null)
				{
					cancel_Button.Click += value2;
				}
			}
		}

		internal virtual DataGridView dgv3
		{
			[CompilerGenerated]
			get
			{
				return this._dgv3;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				DataGridViewCellEventHandler value2 = this.dgv3_CellValueChanged;
				DataGridViewCellEventHandler value3 = this.DataGridView1_CellContentClick;
				DataGridViewCellEventHandler value4 = this.dgv3_CellClick;
				DataGridView dgv = this._dgv3;
				if (dgv != null)
				{
					dgv.CellValueChanged -= value2;
					dgv.CellContentClick -= value3;
					dgv.CellClick -= value4;
				}
				this._dgv3 = value;
				dgv = this._dgv3;
				if (dgv != null)
				{
					dgv.CellValueChanged += value2;
					dgv.CellContentClick += value3;
					dgv.CellClick += value4;
				}
			}
		}

		internal virtual Button Button1
		{
			[CompilerGenerated]
			get
			{
				return this._Button1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Button1_Click;
				Button button = this._Button1;
				if (button != null)
				{
					button.Click -= value2;
				}
				this._Button1 = value;
				button = this._Button1;
				if (button != null)
				{
					button.Click += value2;
				}
			}
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			this.TableLayoutPanel1 = new TableLayoutPanel();
			this.Cancel_Button = new Button();
			this.OK_Button = new Button();
			this.dgv3 = new DataGridView();
			this.Button1 = new Button();
			this.TableLayoutPanel1.SuspendLayout();
			((ISupportInitialize)this.dgv3).BeginInit();
			base.SuspendLayout();
			this.TableLayoutPanel1.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.TableLayoutPanel1.ColumnCount = 2;
			this.TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
			this.TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
			this.TableLayoutPanel1.Controls.Add(this.Cancel_Button, 1, 0);
			this.TableLayoutPanel1.Controls.Add(this.OK_Button, 0, 0);
			this.TableLayoutPanel1.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.TableLayoutPanel1.Location = new Point(277, 416);
			this.TableLayoutPanel1.Margin = new Padding(4, 6, 4, 6);
			this.TableLayoutPanel1.Name = "TableLayoutPanel1";
			this.TableLayoutPanel1.RowCount = 1;
			this.TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
			this.TableLayoutPanel1.Size = new Size(268, 50);
			this.TableLayoutPanel1.TabIndex = 0;
			this.Cancel_Button.Anchor = AnchorStyles.None;
			this.Cancel_Button.DialogResult = DialogResult.Cancel;
			this.Cancel_Button.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Cancel_Button.Location = new Point(138, 6);
			this.Cancel_Button.Margin = new Padding(4, 6, 4, 6);
			this.Cancel_Button.Name = "Cancel_Button";
			this.Cancel_Button.Size = new Size(126, 38);
			this.Cancel_Button.TabIndex = 1;
			this.Cancel_Button.Text = "Cancel";
			this.OK_Button.Anchor = AnchorStyles.None;
			this.OK_Button.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.OK_Button.Location = new Point(4, 6);
			this.OK_Button.Margin = new Padding(4, 6, 4, 6);
			this.OK_Button.Name = "OK_Button";
			this.OK_Button.Size = new Size(126, 38);
			this.OK_Button.TabIndex = 0;
			this.OK_Button.Text = "Analyze";
			this.dgv3.AllowUserToAddRows = false;
			this.dgv3.AllowUserToDeleteRows = false;
			this.dgv3.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle.BackColor = SystemColors.Control;
			dataGridViewCellStyle.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgv3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle;
			this.dgv3.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle2.BackColor = SystemColors.Window;
			dataGridViewCellStyle2.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle2.Format = "N2";
			dataGridViewCellStyle2.NullValue = null;
			dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
			this.dgv3.DefaultCellStyle = dataGridViewCellStyle2;
			this.dgv3.GridColor = SystemColors.ControlLight;
			this.dgv3.Location = new Point(12, 15);
			this.dgv3.Margin = new Padding(3, 5, 3, 5);
			this.dgv3.Name = "dgv3";
			this.dgv3.RowHeadersWidth = 300;
			this.dgv3.RowTemplate.Height = 24;
			this.dgv3.Size = new Size(543, 381);
			this.dgv3.TabIndex = 1;
			this.Button1.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.Button1.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button1.Location = new Point(12, 422);
			this.Button1.Name = "Button1";
			this.Button1.Size = new Size(126, 38);
			this.Button1.TabIndex = 4;
			this.Button1.Text = "Paste";
			this.Button1.UseVisualStyleBackColor = true;
			base.AcceptButton = this.OK_Button;
			base.AutoScaleDimensions = new SizeF(6f, 14f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.CancelButton = this.Cancel_Button;
			base.ClientSize = new Size(567, 508);
			base.Controls.Add(this.Button1);
			base.Controls.Add(this.dgv3);
			base.Controls.Add(this.TableLayoutPanel1);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.Margin = new Padding(4, 6, 4, 6);
			base.MinimizeBox = false;
			this.MinimumSize = new Size(575, 546);
			base.Name = "dialogImpactEntryForm";
			base.ShowInTaskbar = false;
			this.Text = "Impact Form";
			this.TableLayoutPanel1.ResumeLayout(false);
			((ISupportInitialize)this.dgv3).EndInit();
			base.ResumeLayout(false);
		}

		public dialogImpactEntryForm()
		{
			base.Load += this.dialogImpactEntryForm_Load;
			this.InitializeComponent();
		}

		private void OK_Button_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void Cancel_Button_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void dialogImpactEntryForm_Load(object sender, EventArgs e)
		{
			this.Text = this.data.mainHeader;
			DataGridView dgv = this.dgv3;
			dgv.Columns.Clear();
			dgv.Rows.Clear();
			string[] hdrsCol = this.data.hdrsCol;
			foreach (string text in hdrsCol)
			{
				dgv.Columns.Add(text, text);
			}
			checked
			{
				if (this.data.BaseTable.NoCols > this.data.hdrsCol.Length)
				{
					int num = this.data.hdrsCol.Length;
					int num2 = this.data.BaseTable.NoCols - 1;
					for (int j = num; j <= num2; j++)
					{
						dgv.Columns.Add("Col " + Conversions.ToString(j), "Col " + Conversions.ToString(j));
					}
				}
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = dgv.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = unchecked((DataGridViewColumn)enumerator.Current);
						if (dataGridViewColumn.Index >= 1)
						{
							dataGridViewColumn.ReadOnly = true;
							dataGridViewColumn.DefaultCellStyle.BackColor = Color.BurlyWood;
						}
						if (dataGridViewColumn.Index == 0)
						{
							dataGridViewColumn.MinimumWidth = 100;
						}
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				dgv.RowHeadersVisible = true;
				int num3 = this.data.BaseTable.NoRows - 1;
				for (int k = 0; k <= num3; k++)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgv3);
					int num4 = this.data.BaseTable.NoCols - 1;
					for (int l = 0; l <= num4; l++)
					{
						dataGridViewRow.Cells[l].Value = this.data.BaseTable[k, l];
					}
					dataGridViewRow.HeaderCell.Value = RuntimeHelpers.GetObjectValue(this.data.hdrsRow[k]);
					dgv.Rows.Add(dataGridViewRow);
				}
				dgv = null;
				this.dgv3.CellValueChanged += this.dgv3_CellValueChanged;
			}
		}

		private void dgv3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			DataGridViewCell dataGridViewCell = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex];
			int rowIndex = dataGridViewCell.RowIndex;
			int columnIndex = dataGridViewCell.ColumnIndex;
			if (rowIndex < this.data.BaseTable.NoRows && columnIndex < this.data.BaseTable.NoCols)
			{
				string s = Conversions.ToString(dataGridViewCell.Value);
				Matrix baseTable;
				int row;
				int col;
				double value = (baseTable = this.data.BaseTable)[row = rowIndex, col = columnIndex];
				double.TryParse(s, out value);
				baseTable[row, col] = value;
			}
		}

		public void setDialogFrm()
		{
			this.data = new DispData();
		}

		private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
		}

		private void dgv3_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView)sender;
			dataGridView.BeginEdit(true);
		}

		public void setRowHeaders(ref string[] sh)
		{
			this.data.hdrsRow = new ArrayList();
			string[] array = sh;
			foreach (string value in array)
			{
				this.data.hdrsRow.Add(value);
			}
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			this.Paste(true);
		}

		private void Paste(bool overwriteSelectiona)
		{
			string text = Clipboard.GetText();
			string[] array = text.Split('\r');
			int num = 0;
			int num2 = 0;
			string[] array2 = new string[3]
			{
				"\r\n",
				"\t",
				" "
			};
			int num3 = array.Length;
			string[] array3 = array;
			checked
			{
				foreach (string text2 in array3)
				{
					if (num != num3 - 1)
					{
						string[] array4 = text2.Split('\t');
						array4[0] = array4[0].Trim();
						num2 = 0;
						string[] array5 = array4;
						foreach (string text3 in array5)
						{
							if (num2 == 0 & num < this.dgv3.RowCount - this.dgv3.SelectedCells[0].RowIndex)
							{
								this.dgv3.Rows[num + this.dgv3.SelectedCells[0].RowIndex].Cells[num2 + this.dgv3.SelectedCells[0].ColumnIndex].Value = text3.Trim(' ');
							}
							num2++;
						}
					}
					num++;
				}
			}
		}
	}
}
