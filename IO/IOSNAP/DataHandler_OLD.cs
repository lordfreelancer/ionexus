using IOSNAP.My;
using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Compression;

namespace IOSNAP
{
	public class DataHandler_OLD
	{
		private BEAData BeaDat;

		private SummaryDat worksum;

		private string XLSPath;

		private string ZippedPath;

		public int[] getYears()
		{
			string oldValue = ".txt";
			ReadOnlyCollection<string> files = MyProject.Computer.FileSystem.GetFiles(Microsoft.VisualBasic.FileSystem.CurDir(), Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "*");
			List<int> list = new List<int>();
			foreach (string item2 in files)
			{
				int item = default(int);
				if (int.TryParse(item2.Replace(Microsoft.VisualBasic.FileSystem.CurDir() + "\\", "").Replace(oldValue, ""), out item))
				{
					list.Add(item);
				}
			}
			return list.ToArray();
		}

		public bool LoadData()
		{
			List<string> list = new List<string>();
			string text = "";
			int num = Conversions.ToInteger(MyProject.Forms.frmGetYear.GetYear());
			MyProject.Forms.frmGetYear.Dispose();
			checked
			{
				if (num != 0)
				{
					StreamReader streamReader = new StreamReader(Microsoft.VisualBasic.FileSystem.CurDir() + "\\" + Conversions.ToString(num) + ".txt");
					while (!streamReader.EndOfStream)
					{
						string text2 = streamReader.ReadLine().TrimEnd('\t', ' ');
						if (!text2.Contains("\t"))
						{
							text2 += "\t";
						}
						if (text2.StartsWith("\t"))
						{
							list.Add(text2.Substring(1));
							continue;
						}
						Matrix matrix;
						int num2;
						int i;
						if (Operators.CompareString(text, "hdrVA", false) != 0)
						{
							if (Operators.CompareString(text, "hdrComm", false) != 0)
							{
								if (Operators.CompareString(text, "hdrInd", false) != 0)
								{
									if (Operators.CompareString(text, "hdrFinDem", false) != 0)
									{
										if (Operators.CompareString(text, "", false) != 0)
										{
											double[] array;
											switch (BEAData.ComputeStringHash(text))
											{
											case 3122420884u:
												if (Operators.CompareString(text, "Use", false) == 0)
												{
													matrix = new Matrix(list.Count, this.BeaDat.N_INDUSTRIES + 1);
													this.BeaDat.Use = matrix;
													goto IL_0578;
												}
												goto default;
											case 1215032783u:
												if (Operators.CompareString(text, "Make", false) == 0)
												{
													matrix = new Matrix(list.Count, this.BeaDat.N_COMMODITIES + 1);
													this.BeaDat.NTables.SetMake(ref matrix);
													goto IL_0578;
												}
												goto default;
											case 4134655041u:
												if (Operators.CompareString(text, "Imp", false) == 0)
												{
													matrix = new Matrix(1, this.BeaDat.N_COMMODITIES + 1);
													this.BeaDat.Import = matrix;
													goto IL_0578;
												}
												goto default;
											case 3794644288u:
												if (Operators.CompareString(text, "FinDem", false) == 0)
												{
													matrix = new Matrix(list.Count, this.BeaDat.hdrFinDem.Length);
													this.BeaDat.FinDem = matrix;
													goto IL_0578;
												}
												goto default;
											case 1677347064u:
												if (Operators.CompareString(text, "VA", false) == 0)
												{
													matrix = new Matrix(list.Count, this.BeaDat.N_INDUSTRIES + 1);
													this.BeaDat.NTables.VA = matrix;
													goto IL_0578;
												}
												goto default;
											case 3067654867u:
												if (Operators.CompareString(text, "Employment", false) == 0)
												{
													matrix = new Matrix(0, 0);
													this.BeaDat.NTables.Employment_JOBS = new Vector(LoadRegionalData.strArr2Dbl(list[0].Split('\t')));
													goto IL_0578;
												}
												goto default;
											case 3561759774u:
												if (Operators.CompareString(text, "FTE_Ratio", false) == 0)
												{
													matrix = new Matrix(0, 0);
													this.BeaDat.NTables.FTE_Ratio.jobs2fte_ratio_imputed = new Vector(LoadRegionalData.strArr2Dbl(list[0].Split('\t')));
													goto IL_0578;
												}
												goto default;
											case 520705717u:
												if (Operators.CompareString(text, "EndOfFile", false) == 0)
												{
													break;
												}
												goto default;
											default:
												{
													matrix = new Matrix(0, 0);
													Interaction.MsgBox("Unknown variable: " + text, MsgBoxStyle.OkOnly, null);
													goto IL_0578;
												}
												IL_0578:
												this.BeaDat.NTables.FTE_Ratio.setupFTE(ref this.BeaDat.NTables.FTE_Ratio.jobs2fte_ratio_imputed, ref this.BeaDat.NTables.Employment_JOBS);
												array = LoadRegionalData.strArr2Dbl(list[0].Split('\t'));
												num2 = matrix.NoRows - 1;
												i = 0;
												goto IL_062a;
											}
											break;
										}
									}
									else
									{
										this.BeaDat.hdrFinDem = list[0].ToString().Split('\t');
									}
								}
								else
								{
									NatTables nTables = this.BeaDat.NTables;
									string[] array2 = list[0].ToString().Split('\t');
									nTables.setIndHeaders(ref array2);
									unchecked
									{
										this.BeaDat.hdrComm = (string[])this.BeaDat.NTables.hdrInd.Clone();
										BEAData beaDat;
										(beaDat = this.BeaDat).hdrComm = (string[])Utils.CopyArray(beaDat.hdrComm, new string[checked(this.BeaDat.hdrComm.Length + 1 + 1)]);
									}
									this.BeaDat.hdrComm[this.BeaDat.hdrComm.Length - 2] = "Non-comparable imports";
									this.BeaDat.hdrComm[this.BeaDat.hdrComm.Length - 1] = "Scrap, used and secondhand goods";
								}
							}
							else
							{
								this.BeaDat.hdrComm = list[0].ToString().Split('\t');
							}
						}
						else
						{
							this.BeaDat.hdrVA = list[0].ToString().Split('\t');
						}
						goto IL_0631;
						IL_062a:
						for (; i <= num2; i++)
						{
							double[] array = LoadRegionalData.strArr2Dbl(list[i].Split('\t'));
							int upperBound = array.GetUpperBound(0);
							for (int j = 0; j <= upperBound; j++)
							{
								matrix[i, j] = array[j];
							}
						}
						goto IL_0631;
						IL_0631:
						text = text2.Substring(0, text2.IndexOf("\t"));
						list = new List<string>();
						list.Add(text2.Substring(text2.IndexOf("\t") + 1));
					}
					int n_COMMODITIES = this.BeaDat.N_COMMODITIES;
					for (int k = 0; k <= n_COMMODITIES; k++)
					{
						this.BeaDat.Import[0, k] = unchecked(-1.0 * this.BeaDat.Import[0, k]);
					}
					this.BeaDat.NTables.Employment_FTE = this.BeaDat.NTables.FTE_Ratio.getFTE_RATIO() * this.BeaDat.NTables.Employment_JOBS;
					this.BeaDat.NTables.Employment = this.BeaDat.NTables.Employment_FTE;
					this.PreAdjust();
					this.BeaDat.ExtractScrapFromMake();
					this.worksum.InitSetup();
					this.worksum.datayear = Conversions.ToString(num);
					this.BeaDat.eDataState = BEAData.DataState.Loaded;
					return true;
				}
				bool result = default(bool);
				return result;
			}
		}

		public bool LoadData2(string yr)
		{
			List<string> list = new List<string>();
			string text = "";
			checked
			{
				if (Conversions.ToDouble(yr) != 0.0)
				{
					StreamReader streamReader = new StreamReader(Microsoft.VisualBasic.FileSystem.CurDir() + "\\" + yr + ".txt");
					while (!streamReader.EndOfStream)
					{
						string text2 = streamReader.ReadLine().TrimEnd('\t', ' ');
						if (!text2.Contains("\t"))
						{
							text2 += "\t";
						}
						if (text2.StartsWith("\t"))
						{
							list.Add(text2.Substring(1));
							continue;
						}
						Matrix matrix;
						int num;
						int i;
						if (Operators.CompareString(text, "hdrVA", false) != 0)
						{
							if (Operators.CompareString(text, "hdrComm", false) != 0)
							{
								if (Operators.CompareString(text, "hdrInd", false) != 0)
								{
									if (Operators.CompareString(text, "hdrFinDem", false) != 0)
									{
										if (Operators.CompareString(text, "", false) != 0)
										{
											double[] array;
											switch (BEAData.ComputeStringHash(text))
											{
											case 3122420884u:
												if (Operators.CompareString(text, "Use", false) == 0)
												{
													matrix = new Matrix(list.Count, this.BeaDat.N_INDUSTRIES + 1);
													this.BeaDat.Use = matrix;
													goto IL_054b;
												}
												goto default;
											case 1215032783u:
												if (Operators.CompareString(text, "Make", false) == 0)
												{
													matrix = new Matrix(list.Count, this.BeaDat.N_COMMODITIES + 1);
													this.BeaDat.NTables.SetMake(ref matrix);
													goto IL_054b;
												}
												goto default;
											case 4134655041u:
												if (Operators.CompareString(text, "Imp", false) == 0)
												{
													matrix = new Matrix(1, this.BeaDat.N_COMMODITIES + 1);
													this.BeaDat.Import = matrix;
													goto IL_054b;
												}
												goto default;
											case 3794644288u:
												if (Operators.CompareString(text, "FinDem", false) == 0)
												{
													matrix = new Matrix(list.Count, this.BeaDat.hdrFinDem.Length);
													this.BeaDat.FinDem = matrix;
													goto IL_054b;
												}
												goto default;
											case 1677347064u:
												if (Operators.CompareString(text, "VA", false) == 0)
												{
													matrix = new Matrix(list.Count, this.BeaDat.N_INDUSTRIES + 1);
													this.BeaDat.NTables.VA = matrix;
													goto IL_054b;
												}
												goto default;
											case 3067654867u:
												if (Operators.CompareString(text, "Employment", false) == 0)
												{
													matrix = new Matrix(0, 0);
													this.BeaDat.NTables.Employment_JOBS = new Vector(LoadRegionalData.strArr2Dbl(list[0].Split('\t')));
													goto IL_054b;
												}
												goto default;
											case 3561759774u:
												if (Operators.CompareString(text, "FTE_Ratio", false) == 0)
												{
													matrix = new Matrix(0, 0);
													this.BeaDat.NTables.FTE_Ratio.jobs2fte_ratio_imputed = new Vector(LoadRegionalData.strArr2Dbl(list[0].Split('\t')));
													goto IL_054b;
												}
												goto default;
											case 520705717u:
												if (Operators.CompareString(text, "EndOfFile", false) == 0)
												{
													break;
												}
												goto default;
											default:
												{
													matrix = new Matrix(0, 0);
													Interaction.MsgBox("Unknown variable: " + text, MsgBoxStyle.OkOnly, null);
													goto IL_054b;
												}
												IL_054b:
												this.BeaDat.NTables.FTE_Ratio.setupFTE(ref this.BeaDat.NTables.FTE_Ratio.jobs2fte_ratio_imputed, ref this.BeaDat.NTables.Employment_JOBS);
												array = LoadRegionalData.strArr2Dbl(list[0].Split('\t'));
												num = matrix.NoRows - 1;
												i = 0;
												goto IL_05fb;
											}
											break;
										}
									}
									else
									{
										this.BeaDat.hdrFinDem = list[0].ToString().Split('\t');
									}
								}
								else
								{
									this.BeaDat.NTables.hdrInd = list[0].ToString().Split('\t');
									unchecked
									{
										this.BeaDat.hdrComm = (string[])this.BeaDat.NTables.hdrInd.Clone();
										BEAData beaDat;
										(beaDat = this.BeaDat).hdrComm = (string[])Utils.CopyArray(beaDat.hdrComm, new string[checked(this.BeaDat.hdrComm.Length + 1 + 1)]);
									}
									this.BeaDat.hdrComm[this.BeaDat.hdrComm.Length - 2] = "Non-comparable imports";
									this.BeaDat.hdrComm[this.BeaDat.hdrComm.Length - 1] = "Scrap, used and secondhand goods";
								}
							}
							else
							{
								this.BeaDat.hdrComm = list[0].ToString().Split('\t');
							}
						}
						else
						{
							this.BeaDat.hdrVA = list[0].ToString().Split('\t');
						}
						goto IL_0602;
						IL_05fb:
						for (; i <= num; i++)
						{
							double[] array = LoadRegionalData.strArr2Dbl(list[i].Split('\t'));
							int upperBound = array.GetUpperBound(0);
							for (int j = 0; j <= upperBound; j++)
							{
								matrix[i, j] = array[j];
							}
						}
						goto IL_0602;
						IL_0602:
						text = text2.Substring(0, text2.IndexOf("\t"));
						list = new List<string>();
						list.Add(text2.Substring(text2.IndexOf("\t") + 1));
					}
					int n_COMMODITIES = this.BeaDat.N_COMMODITIES;
					for (int k = 0; k <= n_COMMODITIES; k++)
					{
						this.BeaDat.Import[0, k] = unchecked(-1.0 * this.BeaDat.Import[0, k]);
					}
					this.BeaDat.NTables.Employment_FTE = this.BeaDat.NTables.FTE_Ratio.getFTE_RATIO() * this.BeaDat.NTables.Employment_JOBS;
					this.BeaDat.NTables.Employment = this.BeaDat.NTables.Employment_FTE;
					this.PreAdjust();
					this.BeaDat.ExtractScrapFromMake();
					this.worksum.InitSetup();
					this.worksum.datayear = yr;
					this.BeaDat.eDataState = BEAData.DataState.Loaded;
					return true;
				}
				bool result = default(bool);
				return result;
			}
		}

		private void PreAdjust()
		{
			checked
			{
				int num = this.BeaDat.NTables.Make().NoCols - 2;
				int num2 = this.BeaDat.NTables.Make().NoRows - 1;
				for (int i = 0; i <= num2; i++)
				{
					Console.Write(Conversions.ToString(this.BeaDat.NTables.Make()[i, num]) + ",");
					this.BeaDat.NTables.Make()[i, num + 1] = this.BeaDat.NTables.Make()[i, num];
					this.BeaDat.NTables.Make()[i, num] = 0.0;
				}
				num++;
				int num3 = this.BeaDat.NTables.Make().NoRows - 1;
				for (int i = 0; i <= num3; i++)
				{
					Console.Write(Conversions.ToString(this.BeaDat.NTables.Make()[i, num]) + ",");
				}
			}
		}

		private string Obj2Str(object obj)
		{
			return obj.ToString();
		}

		private string CheckDataSizes()
		{
			string text = "";
			int noRows = this.BeaDat.Use.NoRows;
			int noCols = this.BeaDat.Use.NoCols;
			if (this.BeaDat.NTables.Make().NoCols != noRows | this.BeaDat.NTables.Make().NoRows != noCols)
			{
				text = text + "\r\n" + string.Format("Make Dimensions ({0},{0}) do not match Use transposed dimensions({0},{0})", this.BeaDat.NTables.Make().NoRows, this.BeaDat.NTables.Make().NoCols, noRows, noCols);
			}
			if (this.BeaDat.NTables.VA.NoCols != noCols)
			{
				text = text + "\r\n" + string.Format("Value Added Columns ({0}) do not match Use Columns({0})", this.BeaDat.NTables.VA.NoCols, noCols);
			}
			if (this.BeaDat.NTables.VA.NoRows != 3)
			{
				text += "\r\nValue Added Rows is expected to be 3 (EC,GOS,T)";
			}
			if (this.BeaDat.FinDem.NoRows != noRows)
			{
				text = text + "\r\n" + string.Format("Final Demand Rows ({0}) do not match Use Rows({0})", this.BeaDat.FinDem.NoRows, noRows);
			}
			return text;
		}

		public DataHandler_OLD(BEAData BeaDat, ref SummaryDat worksum)
		{
			this.XLSPath = Microsoft.VisualBasic.FileSystem.CurDir() + "\\Data.xls";
			this.ZippedPath = Microsoft.VisualBasic.FileSystem.CurDir() + "\\Data.io";
			this.BeaDat = BeaDat;
			this.worksum = worksum;
		}

		private void Decompress(string inFilePath, string outFilePath)
		{
			FileInfo fileInfo = new FileInfo(inFilePath);
			using (FileStream fileStream = fileInfo.OpenRead())
			{
				using (FileStream fileStream2 = File.Create(outFilePath))
				{
					using (GZipStream gZipStream = new GZipStream(fileStream, CompressionMode.Decompress))
					{
						byte[] array = new byte[checked((int)fileStream.Length + 1)];
						for (int num = gZipStream.Read(array, 0, array.Length); num != 0; num = gZipStream.Read(array, 0, array.Length))
						{
							fileStream2.Write(array, 0, num);
						}
						gZipStream.Close();
					}
					fileStream2.Close();
				}
				fileStream.Close();
			}
		}

		public string Encrypt(string strInput)
		{
			string text = strInput;
			int num = 7;
			int num2 = Strings.Len(strInput);
			checked
			{
				for (int i = 1; i <= num2; i++)
				{
					StringType.MidStmtStr(ref text, i, 1, Conversions.ToString(Strings.Chr(Strings.Asc(Strings.Mid(text, i, 1)) + num)));
				}
				return text;
			}
		}

		public string Decrypt(string strInput)
		{
			string text = strInput;
			int num = 7;
			int num2 = Strings.Len(strInput);
			checked
			{
				for (int i = 1; i <= num2; i++)
				{
					StringType.MidStmtStr(ref text, i, 1, Conversions.ToString(Strings.Chr(Strings.Asc(Strings.Mid(text, i, 1)) - num)));
				}
				return text;
			}
		}


        ~DataHandler_OLD()
		{
			try
			{
				FileInfo fileInfo = new FileInfo(Path.GetTempPath() + "IOData.io");
				fileInfo.Delete();
			}
			catch (IOException ex)
			{
				ProjectData.SetProjectError(ex);
				IOException ex2 = ex;
				ProjectData.ClearProjectError();
			}
		}
	}
}
