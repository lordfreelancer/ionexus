using System;
using System.Collections;

namespace IOSNAP
{
	[Serializable]
	public class TabStorage
	{
		public ArrayList TabsShown;

		public TabStorage()
		{
			this.TabsShown = new ArrayList();
		}

		public void addTab(ref TabContent t)
		{
			if (!this.TabsShown.Contains(t))
			{
				bool flag = true;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.TabsShown.GetEnumerator();
					while (enumerator.MoveNext())
					{
						TabContent tabContent = (TabContent)enumerator.Current;
						if (string.Compare(tabContent.Text, t.Text) == 0)
						{
							flag = false;
						}
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				if (flag)
				{
					this.TabsShown.Add(t);
				}
			}
		}

		public void removeTab(ref TabContent t)
		{
			if (this.TabsShown.Contains(t))
			{
				this.TabsShown.Remove(t);
			}
		}
	}
}
