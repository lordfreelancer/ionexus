using Matrix_Lib;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class FormEditFD : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ButtonOK")]
		private Button _ButtonOK;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ButtonCancel")]
		private Button _ButtonCancel;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("dgvnn")]
		private DataGridView _dgvnn;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("LabelTittle")]
		private Label _LabelTittle;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ButtonRestore")]
		private Button _ButtonRestore;

		private BEAData beadat;

		private object DispDat;

		private string title;

		public bool isEdited;

		internal virtual Button ButtonOK
		{
			[CompilerGenerated]
			get
			{
				return this._ButtonOK;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ButtonOK_Click;
				Button buttonOK = this._ButtonOK;
				if (buttonOK != null)
				{
					buttonOK.Click -= value2;
				}
				this._ButtonOK = value;
				buttonOK = this._ButtonOK;
				if (buttonOK != null)
				{
					buttonOK.Click += value2;
				}
			}
		}

		internal virtual Button ButtonCancel
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual DataGridView dgvnn
		{
			[CompilerGenerated]
			get
			{
				return this._dgvnn;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				DataGridViewCellEventHandler value2 = this.dgv3_CellClick;
				DataGridViewCellEventHandler value3 = this.dgv3_CellChange;
				DataGridViewCellEventHandler value4 = this.dgv3_CellValueChanged;
				DataGridView dgvnn = this._dgvnn;
				if (dgvnn != null)
				{
					dgvnn.CellClick -= value2;
					dgvnn.CellEndEdit -= value3;
					dgvnn.CellValueChanged -= value4;
				}
				this._dgvnn = value;
				dgvnn = this._dgvnn;
				if (dgvnn != null)
				{
					dgvnn.CellClick += value2;
					dgvnn.CellEndEdit += value3;
					dgvnn.CellValueChanged += value4;
				}
			}
		}

		internal virtual Label LabelTittle
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button ButtonRestore
		{
			[CompilerGenerated]
			get
			{
				return this._ButtonRestore;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ButtonRestore_Click;
				Button buttonRestore = this._ButtonRestore;
				if (buttonRestore != null)
				{
					buttonRestore.Click -= value2;
				}
				this._ButtonRestore = value;
				buttonRestore = this._ButtonRestore;
				if (buttonRestore != null)
				{
					buttonRestore.Click += value2;
				}
			}
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.ButtonOK = new Button();
			this.ButtonCancel = new Button();
			this.dgvnn = new DataGridView();
			this.LabelTittle = new Label();
			this.ButtonRestore = new Button();
			((ISupportInitialize)this.dgvnn).BeginInit();
			base.SuspendLayout();
			this.ButtonOK.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.ButtonOK.DialogResult = DialogResult.OK;
			this.ButtonOK.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.ButtonOK.Location = new Point(696, 485);
			this.ButtonOK.Margin = new Padding(3, 4, 3, 4);
			this.ButtonOK.Name = "ButtonOK";
			this.ButtonOK.Size = new Size(112, 39);
			this.ButtonOK.TabIndex = 0;
			this.ButtonOK.Text = "OK";
			this.ButtonOK.UseVisualStyleBackColor = true;
			this.ButtonCancel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.ButtonCancel.DialogResult = DialogResult.Cancel;
			this.ButtonCancel.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.ButtonCancel.Location = new Point(549, 485);
			this.ButtonCancel.Margin = new Padding(3, 4, 3, 4);
			this.ButtonCancel.Name = "ButtonCancel";
			this.ButtonCancel.Size = new Size(112, 39);
			this.ButtonCancel.TabIndex = 1;
			this.ButtonCancel.Text = "Cancel";
			this.ButtonCancel.UseVisualStyleBackColor = true;
			this.dgvnn.AllowUserToAddRows = false;
			this.dgvnn.AllowUserToDeleteRows = false;
			this.dgvnn.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnn.Location = new Point(45, 92);
			this.dgvnn.Margin = new Padding(3, 4, 3, 4);
			this.dgvnn.Name = "dgvnn";
			this.dgvnn.RowTemplate.Height = 28;
			this.dgvnn.Size = new Size(762, 377);
			this.dgvnn.TabIndex = 2;
			this.LabelTittle.AutoSize = true;
			this.LabelTittle.Font = new Font("Calibri", 24f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.LabelTittle.Location = new Point(49, 10);
			this.LabelTittle.Name = "LabelTittle";
			this.LabelTittle.Size = new Size(300, 59);
			this.LabelTittle.TabIndex = 3;
			this.LabelTittle.Text = "Final Demand";
			this.ButtonRestore.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.ButtonRestore.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.ButtonRestore.Location = new Point(152, 485);
			this.ButtonRestore.Margin = new Padding(3, 4, 3, 4);
			this.ButtonRestore.Name = "ButtonRestore";
			this.ButtonRestore.Size = new Size(112, 39);
			this.ButtonRestore.TabIndex = 4;
			this.ButtonRestore.Text = "Restore";
			this.ButtonRestore.UseVisualStyleBackColor = true;
			base.AutoScaleDimensions = new SizeF(9f, 22f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(834, 583);
			base.Controls.Add(this.ButtonRestore);
			base.Controls.Add(this.LabelTittle);
			base.Controls.Add(this.dgvnn);
			base.Controls.Add(this.ButtonCancel);
			base.Controls.Add(this.ButtonOK);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.Margin = new Padding(3, 4, 3, 4);
			base.MinimizeBox = false;
			this.MinimumSize = new Size(842, 617);
			base.Name = "FormEditFD";
			this.Text = "The Final Demand table editing form";
			((ISupportInitialize)this.dgvnn).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public FormEditFD(ref BEAData b)
		{
			base.Load += this.FormEditFD_Load;
			this.DispDat = new DispData();
			this.title = "";
			this.isEdited = false;
			this.InitializeComponent();
			this.beadat = b;
		}

		private void FormEditFD_Load(object sender, EventArgs e)
		{
			NewLateBinding.LateSet(this.DispDat, null, "hdrsCol", new object[1]
			{
				this.beadat.hdrFinDem
			}, null, null);
			NewLateBinding.LateSet(this.DispDat, null, "hdrsRow", new object[1]
			{
				this.beadat.hdrComm
			}, null, null);
			NewLateBinding.LateSet(this.DispDat, null, "BaseTable", new object[1]
			{
				this.beadat.FinDem
			}, null, null);
			this.title = "Final Demand";
			this.dgvnn.Columns.Clear();
			this.dgvnn.RightToLeft = RightToLeft.No;
			this.dgvnn.RowHeadersWidth = 275;
			this.dgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvnn.RowTemplate.Height = 24;
			string[] hdrFinDem = this.beadat.hdrFinDem;
			foreach (string text in hdrFinDem)
			{
				this.dgvnn.Columns.Add(text, text);
			}
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = this.dgvnn.Columns.GetEnumerator();
				while (enumerator.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
					dataGridViewColumn.MinimumWidth = 200;
					dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
					dataGridViewColumn.ReadOnly = false;
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			this.dgvnn.RowHeadersVisible = true;
			this.dgvnn.AllowUserToAddRows = false;
			checked
			{
				int num = this.beadat.NTables.FinDem.NoRows - 1;
				for (int j = 0; j <= num; j++)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					int num2 = this.beadat.FinDem.NoCols - 1;
					for (int k = 0; k <= num2; k++)
					{
						dataGridViewRow.Cells[k].Value = this.beadat.get_FinDemVector(j, k);
					}
					dataGridViewRow.HeaderCell.Value = this.beadat.hdrComm[j];
					this.dgvnn.Rows.Add(dataGridViewRow);
				}
			}
		}

		private void dgv3_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView)sender;
			dataGridView.BeginEdit(true);
		}

		private void dgv3_CellChange(object sender, DataGridViewCellEventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init);
			//}
			int columnIndex = e.ColumnIndex;
			int rowIndex = e.RowIndex;
			double value = 0.0;
			if (double.TryParse(Conversions.ToString(this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value), out value))
			{
				decimal d = new decimal(value);
				string strB = Conversions.ToString(this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value);
				string text = decimal.Round(d, 2).ToString("f2");
				if (string.Compare(text, strB) != 0)
				{
					this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value = text;
					this.isEdited = true;
				}
			}
			else
			{
				this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value = this.beadat.get_FinDemVector(rowIndex, columnIndex);
			}
		}

		private void dgv3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
		}

		private void ButtonOK_Click(object sender, EventArgs e)
		{
			Matrix matrix = new Matrix(this.beadat.FinDem.NoRows, this.beadat.FinDem.NoCols);
			Vector vector = new Vector(this.beadat.Import.NoCols);
			checked
			{
				int num = this.beadat.FinDem.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					int num2 = this.beadat.FinDem.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						matrix[i, j] = Conversions.ToDouble(this.dgvnn.Rows[i].Cells[j].Value);
					}
				}
				int num3 = vector.Count - 1;
				for (int k = 0; k <= num3; k++)
				{
					vector[k] = this.beadat.Import[0, k];
				}
				this.beadat.FinDem = matrix;
				this.beadat.ImportExportUpdate();
			}
		}

		private void ButtonRestore_Click(object sender, EventArgs e)
		{
			checked
			{
				int num = this.beadat.hdrComm.Length - 1;
				for (int i = 0; i <= num; i++)
				{
					int num2 = this.beadat.FinDem.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						this.dgvnn.Rows[i].Cells[j].Value = this.beadat.get_FinDemVector(i, j);
					}
				}
			}
		}
	}
}
