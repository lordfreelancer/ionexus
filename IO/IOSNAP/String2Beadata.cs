using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;

namespace IOSNAP
{
	internal class String2Beadata
	{
		private BEAData bd;

		public bool isSuccess;

		public String2Beadata()
		{
			this.isSuccess = false;
		}

		public BEAData String2B(ref string Sval)
		{
			List<string> list = new List<string>();
			string dataVersion = "";
			int num = -1;
			int num2 = -1;
			int num3 = -1;
			string text = "";
			SummaryDat.bcode bcode = SummaryDat.bcode.BASE_DATA;
			int num4 = -1;
			string text2 = "";
			string text3 = "";
			checked
			{
				try
				{
					this.bd = new BEAData();
					this.bd.worksum = new SummaryDat();
					this.bd.worksum.InitSetup();
					list = this.CleanDataString(Sval);
					while (num4 < list.Count - 1)
					{
						num4++;
						text2 = list[num4];
						text3 = text2.ToLower();
						string left = text3;
						string[] hdrFinDem = default(string[]);
						string[] hdrIndBase = default(string[]);
						string[] indBaseClass = default(string[]);
						string[] hdrIndAdjGov = default(string[]);
						string[] indAdjGovClass = default(string[]);
						string[] hdrIndFedStateLocGov = default(string[]);
						string[] indFedStateLocGovClass = default(string[]);
						string[] hdrIndExpandedGov = default(string[]);
						string[] indExpandedGovClass = default(string[]);
						string[] hdrIndTotGov = default(string[]);
						string[] indTotGovClass = default(string[]);
						string[] hdrCommBase = default(string[]);
						string[] hdrCommAdjGov = default(string[]);
						string[] hdrCommFedStateLocGov = default(string[]);
						string[] hdrCommExpandedGov = default(string[]);
						string[] hdrCommTotGov = default(string[]);
						string[] hdrNoncompImp = default(string[]);
						string[] hdrScrap = default(string[]);
						string[] hdrVA = default(string[]);
						if (Operators.CompareString(left, "<version>".ToLower(), false) == 0)
						{
							while (!text3.Contains("</version>".ToLower()))
							{
								num4++;
								text2 = list[num4];
								text3 = text2.ToLower();
								if (text3.Contains("<value".ToLower()))
								{
									dataVersion = this.ExtractString(text2);
								}
							}
						}
						else if (Operators.CompareString(left, "<parameters>".ToLower(), false) == 0)
						{
							while (!text3.Contains("</parameters>".ToLower()))
							{
								num4++;
								text2 = list[num4];
								text3 = text2.ToLower();
								if (text3.Contains("<n_industries>".ToLower()))
								{
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									if (text3.Contains("<value".ToLower()))
									{
										num = this.ExtractInt(text2);
									}
								}
								if (text3.Contains("<n_commodities>".ToLower()))
								{
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									if (text3.Contains("<value".ToLower()))
									{
										num2 = this.ExtractInt(text2);
									}
								}
								if (text3.Contains("<n_base>".ToLower()))
								{
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									if (text3.Contains("<value".ToLower()))
									{
										num3 = this.ExtractInt(text2);
									}
								}
							}
						}
						else if (Operators.CompareString(left, "<headers>".ToLower(), false) == 0)
						{
							while (!text3.Contains("</headers>".ToLower()))
							{
								num4++;
								text2 = list[num4];
								text3 = text2.ToLower();
								string left2 = text3;
								if (Operators.CompareString(left2, "<hdrfindem>".ToLower(), false) == 0)
								{
									ArrayList arrayList = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrfindem>".ToLower()))
									{
										arrayList.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrFinDem = unchecked((string[])arrayList.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrind>".ToLower(), false) == 0)
								{
									ArrayList arrayList2 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrind>".ToLower()))
									{
										arrayList2.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrIndBase = unchecked((string[])arrayList2.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<indclassification>".ToLower(), false) == 0)
								{
									ArrayList arrayList3 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</indclassification>".ToLower()))
									{
										arrayList3.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									indBaseClass = unchecked((string[])arrayList3.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrindadjgov>".ToLower(), false) == 0)
								{
									ArrayList arrayList4 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrindadjgov>".ToLower()))
									{
										arrayList4.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrIndAdjGov = unchecked((string[])arrayList4.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<indadjgovclass>".ToLower(), false) == 0)
								{
									ArrayList arrayList5 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</indadjgovclass>".ToLower()))
									{
										arrayList5.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									indAdjGovClass = unchecked((string[])arrayList5.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrindfedstatelocgov>".ToLower(), false) == 0)
								{
									ArrayList arrayList6 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrindfedstatelocgov>".ToLower()))
									{
										arrayList6.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrIndFedStateLocGov = unchecked((string[])arrayList6.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<indfedstatelocgovclass>".ToLower(), false) == 0)
								{
									ArrayList arrayList7 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</indfedstatelocgovclass>".ToLower()))
									{
										arrayList7.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									indFedStateLocGovClass = unchecked((string[])arrayList7.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrindexpandedgov>".ToLower(), false) == 0)
								{
									ArrayList arrayList8 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrindexpandedgov>".ToLower()))
									{
										arrayList8.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrIndExpandedGov = unchecked((string[])arrayList8.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<indexpandedgovclass>".ToLower(), false) == 0)
								{
									ArrayList arrayList9 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</indexpandedgovclass>".ToLower()))
									{
										arrayList9.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									indExpandedGovClass = unchecked((string[])arrayList9.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrIndTotGov>".ToLower(), false) == 0)
								{
									ArrayList arrayList10 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrIndTotGov>".ToLower()))
									{
										arrayList10.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrIndTotGov = unchecked((string[])arrayList10.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<IndTotGovClass>".ToLower(), false) == 0)
								{
									ArrayList arrayList11 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</IndTotGovClass>".ToLower()))
									{
										arrayList11.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									indTotGovClass = unchecked((string[])arrayList11.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrComm>".ToLower(), false) == 0)
								{
									ArrayList arrayList12 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrComm>".ToLower()))
									{
										arrayList12.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrCommBase = unchecked((string[])arrayList12.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrCommAdjGov>".ToLower(), false) == 0)
								{
									ArrayList arrayList13 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrCommAdjGov>".ToLower()))
									{
										arrayList13.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrCommAdjGov = unchecked((string[])arrayList13.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrCommFedStateLocGov>".ToLower(), false) == 0)
								{
									ArrayList arrayList14 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrCommFedStateLocGov>".ToLower()))
									{
										arrayList14.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrCommFedStateLocGov = unchecked((string[])arrayList14.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrCommExpandedGov>".ToLower(), false) == 0)
								{
									ArrayList arrayList15 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrCommExpandedGov>".ToLower()))
									{
										arrayList15.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrCommExpandedGov = unchecked((string[])arrayList15.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrCommTotGov>".ToLower(), false) == 0)
								{
									ArrayList arrayList16 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrCommTotGov>".ToLower()))
									{
										arrayList16.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrCommTotGov = unchecked((string[])arrayList16.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrNoncompImp>".ToLower(), false) == 0)
								{
									ArrayList arrayList17 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrNoncompImp>".ToLower()))
									{
										arrayList17.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrNoncompImp = unchecked((string[])arrayList17.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrScrap>".ToLower(), false) == 0)
								{
									ArrayList arrayList18 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrScrap>".ToLower()))
									{
										arrayList18.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrScrap = unchecked((string[])arrayList18.ToArray(typeof(string)));
								}
								else if (Operators.CompareString(left2, "<hdrVA>".ToLower(), false) == 0)
								{
									ArrayList arrayList19 = new ArrayList();
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									while (!text3.Contains("</hdrVA>".ToLower()))
									{
										arrayList19.Add(text2);
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
									}
									hdrVA = unchecked((string[])arrayList19.ToArray(typeof(string)));
								}
							}
						}
						else if (Operators.CompareString(left, "<data>".ToLower(), false) == 0)
						{
							while (!text3.Contains("</data>".ToLower()))
							{
								num4++;
								text2 = list[num4];
								text3 = text2.ToLower();
								if (text3.Contains("<year>".ToLower()))
								{
									string datayear = "";
									Matrix matrix = new Matrix(3, num);
									Matrix matrix2 = new Matrix(num, num2);
									Matrix matrix3 = new Matrix(num2, num);
									Matrix matrix4 = new Matrix(num2, 10);
									Matrix matrix5 = new Matrix(1, num2);
									Matrix matrix6 = new Matrix(1, num2);
									Vector vector = new Vector(num);
									Vector vector2 = new Vector(num);
									Vector vector3 = new Vector(1);
									Vector vector4 = new Vector(1);
									Vector vector5 = new Vector(1);
									Vector vector6 = new Vector(1);
									num4++;
									text2 = list[num4];
									text3 = text2.ToLower();
									if (text3.Contains("<value".ToLower()))
									{
										datayear = this.ExtractString(text2);
									}
									this.bd = new BEAData();
									SummaryDat summaryDat = new SummaryDat();
									summaryDat.InitSetup();
									this.bd.worksum = summaryDat;
									this.bd.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
									this.bd.worksum.notFirstRead = true;
									while (!text3.Contains("</year>".ToLower()))
									{
										num4++;
										text2 = list[num4];
										text3 = text2.ToLower();
										if (text3.Contains("<DataLabel>".ToLower()))
										{
											while (!text3.Contains("</DataLabel>".ToLower()))
											{
												num4++;
												text2 = list[num4];
												text3 = text2.ToLower();
												if (!text3.Contains("</DataLabel>".ToLower()))
												{
													this.bd.worksum.DataLabel = text2;
												}
											}
										}
										if (text3.Contains("<DataType>".ToLower()))
										{
											while (!text3.Contains("</DataType>".ToLower()))
											{
												num4++;
												text2 = list[num4];
												text3 = text2.ToLower();
												unchecked
												{
													if (!text3.Contains("</DataType>".ToLower()))
													{
														string s = text2;
														int num5 = (int)bcode;
														int.TryParse(s, out num5);
														bcode = (SummaryDat.bcode)num5;
													}
												}
											}
										}
										if (text3.Contains("<TimeStamp>".ToLower()))
										{
											while (!text3.Contains("</TimeStamp>".ToLower()))
											{
												num4++;
												text2 = list[num4];
												text3 = text2.ToLower();
												if (!text3.Contains("</TimeStamp>".ToLower()))
												{
													this.bd.worksum.DataTimeStamp = text2;
												}
											}
										}
										if (text3.Contains("<IsRegionalized>".ToLower()))
										{
											while (!text3.Contains("</IsRegionalized>".ToLower()))
											{
												num4++;
												text2 = list[num4];
												text3 = text2.ToLower();
												if (!text3.Contains("</IsRegionalized>".ToLower()))
												{
													this.bd.worksum.dataregionalized = text2;
												}
											}
										}
										if (text3.Contains("<RegionalizationMethod>".ToLower()))
										{
											while (!text3.Contains("</RegionalizationMethod>".ToLower()))
											{
												num4++;
												text2 = list[num4];
												text3 = text2.ToLower();
												if (!text3.Contains("</RegionalizationMethod>".ToLower()))
												{
													this.bd.worksum.regmethod = text2;
												}
											}
										}
										if (text3.Contains("<Region>".ToLower()))
										{
											while (!text3.Contains("</Region>".ToLower()))
											{
												num4++;
												text2 = list[num4];
												text3 = text2.ToLower();
												if (!text3.Contains("</Region>".ToLower()))
												{
													this.bd.worksum.regby = text2;
												}
											}
										}
										if (text3.Contains("<national>".ToLower()))
										{
											while (!text3.Contains("</national>".ToLower()))
											{
												num4++;
												text2 = list[num4];
												text3 = text2.ToLower();
												string left3 = text3;
												if (Operators.CompareString(left3, "<tableVA>".ToLower(), false) == 0)
												{
													int num6 = 0;
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableVA>".ToLower()))
													{
														double[] array = this.StrArr_2_DblArr(text2.Split('\t'));
														int num7 = array.Length - 1;
														for (int i = 0; i <= num7; i++)
														{
															matrix[num6, i] = array[i];
														}
														num6++;
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableUse>".ToLower(), false) == 0)
												{
													int num8 = 0;
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableUse>".ToLower()))
													{
														double[] array2 = this.StrArr_2_DblArr(text2.Split('\t'));
														int upperBound = array2.GetUpperBound(0);
														for (int j = 0; j <= upperBound; j++)
														{
															matrix3[num8, j] = array2[j];
														}
														num8++;
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableMake>".ToLower(), false) == 0)
												{
													int num9 = 0;
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableMake>".ToLower()))
													{
														double[] array3 = this.StrArr_2_DblArr(text2.Split('\t'));
														int num10 = array3.Length - 1;
														for (int k = 0; k <= num10; k++)
														{
															matrix2[num9, k] = array3[k];
														}
														num9++;
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableFinDem>".ToLower(), false) == 0)
												{
													int num11 = 0;
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableFinDem>".ToLower()))
													{
														double[] array4 = this.StrArr_2_DblArr(text2.Split('\t'));
														int num12 = array4.Length - 1;
														for (int l = 0; l <= num12; l++)
														{
															matrix4[num11, l] = array4[l];
														}
														num11++;
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableImp>".ToLower(), false) == 0)
												{
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableImp>".ToLower()))
													{
														double[] array5 = this.StrArr_2_DblArr(text2.Split('\t'));
														int upperBound2 = array5.GetUpperBound(0);
														for (int m = 0; m <= upperBound2; m++)
														{
															matrix5[0, m] = array5[m];
														}
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tablePCE>".ToLower(), false) == 0)
												{
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tablePCE>".ToLower()))
													{
														double[] array6 = this.StrArr_2_DblArr(text2.Split('\t'));
														int upperBound3 = array6.GetUpperBound(0);
														for (int n = 0; n <= upperBound3; n++)
														{
															matrix6[0, n] = array6[n];
														}
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableEmployment>".ToLower(), false) == 0)
												{
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableEmployment>".ToLower()))
													{
														double[] array7 = this.StrArr_2_DblArr(text2.Split('\t'));
														int upperBound4 = array7.GetUpperBound(0);
														for (int num13 = 0; num13 <= upperBound4; num13++)
														{
															vector[num13] = array7[num13];
														}
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableFTE_Ratio>".ToLower(), false) == 0)
												{
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableFTE_Ratio>".ToLower()))
													{
														double[] array8 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
														int upperBound5 = array8.GetUpperBound(0);
														for (int num14 = 0; num14 <= upperBound5; num14++)
														{
															vector2[num14] = array8[num14];
														}
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tablePersonalIncome>".ToLower(), false) == 0)
												{
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tablePersonalIncome>".ToLower()))
													{
														double[] array9 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
														vector3[0] = array9[0];
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableDisposableIncome>".ToLower(), false) == 0)
												{
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableDisposableIncome>".ToLower()))
													{
														double[] array10 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
														vector4[0] = array10[0];
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableTotFedExp_2005>".ToLower(), false) == 0)
												{
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableTotFedExp_2005>".ToLower()))
													{
														double[] array11 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
														vector5[0] = array11[0];
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
												else if (Operators.CompareString(left3, "<tableTotSLExp_2008>".ToLower(), false) == 0)
												{
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													while (!text3.Contains("</tableTotSLExp_2008>".ToLower()))
													{
														double[] array12 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
														vector6[0] = array12[0];
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
													}
												}
											}
											if (this.bd.worksum.dataregionalized.Contains("Yes"))
											{
												this.bd.NTables.RegEmployment_JOBS = vector;
											}
											else
											{
												this.bd.NTables.Employment_JOBS = vector;
											}
											this.bd.NTables.FTE_Ratio.setupFTE(ref vector2, ref vector);
										}
										if (text3.Contains("<statedata>".ToLower()))
										{
											while (!text3.Contains("</statedata>".ToLower()))
											{
												num4++;
												text2 = list[num4];
												text3 = text2.ToLower();
												if (text3.Contains("<state>".ToLower()))
												{
													string text4 = "";
													Vector vector7 = new Vector(1);
													Vector vector8 = new Vector(1);
													Vector vector9 = new Vector(1);
													Vector vector10 = new Vector(1);
													Vector vector11 = new Vector(num3);
													Vector vector12 = new Vector(num3);
													Vector vector13 = new Vector(2);
													Vector vector14 = new Vector(2);
													Vector vector15 = new Vector(4);
													Vector vector16 = new Vector(4);
													Vector vector17 = new Vector(1);
													Vector vector18 = new Vector(1);
													Vector vector19 = new Vector(num3);
													Vector vector20 = new Vector(num3);
													Vector vector21 = new Vector(2);
													Vector vector22 = new Vector(2);
													Vector vector23 = new Vector(4);
													Vector vector24 = new Vector(4);
													Vector vector25 = new Vector(1);
													Vector vector26 = new Vector(1);
													Vector vector27 = new Vector(num3);
													Vector vector28 = new Vector(num3);
													Vector vector29 = new Vector(2);
													Vector vector30 = new Vector(2);
													Vector vector31 = new Vector(4);
													Vector vector32 = new Vector(4);
													Vector vector33 = new Vector(1);
													Vector vector34 = new Vector(1);
													Vector vector35 = new Vector(num3);
													Vector vector36 = new Vector(num3);
													Vector vector37 = new Vector(2);
													Vector vector38 = new Vector(2);
													Vector vector39 = new Vector(4);
													Vector vector40 = new Vector(4);
													Vector vector41 = new Vector(1);
													Vector vector42 = new Vector(1);
													Vector vector43 = new Vector(num3);
													Vector vector44 = new Vector(num3);
													Vector vector45 = new Vector(2);
													Vector vector46 = new Vector(2);
													Vector vector47 = new Vector(4);
													Vector vector48 = new Vector(4);
													Vector vector49 = new Vector(1);
													Vector vector50 = new Vector(1);
													Vector vector51 = new Vector(num2);
													num4++;
													text2 = list[num4];
													text3 = text2.ToLower();
													if (text3.Contains("<value".ToLower()))
													{
														text4 = this.ExtractString(text2);
													}
													while (!text3.Contains("</state>".ToLower()))
													{
														num4++;
														text2 = list[num4];
														text3 = text2.ToLower();
														string left4 = text3;
														if (Operators.CompareString(left4, "<statePersonalIncome>".ToLower(), false) == 0)
														{
															int num15 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</statePersonalIncome>".ToLower()))
															{
																double[] array13 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector7[0] = array13[0];
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateDisposableIncome>".ToLower(), false) == 0)
														{
															int num16 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateDisposableIncome>".ToLower()))
															{
																double[] array14 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector8[0] = array14[0];
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateTotFedExp>".ToLower(), false) == 0)
														{
															int num17 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateTotFedExp>".ToLower()))
															{
																double[] array15 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector10[0] = array15[0];
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateTotSLEExp>".ToLower(), false) == 0)
														{
															int num18 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateTotSLEExp>".ToLower()))
															{
																double[] array16 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector9[0] = array16[0];
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateEmploymentBase>".ToLower(), false) == 0)
														{
															int num19 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateEmploymentBase>".ToLower()))
															{
																double[] array17 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector11[num19] = array17[0];
																vector12[num19] = array17[1];
																num19++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateEmploymentFedStateGov>".ToLower(), false) == 0)
														{
															int num20 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateEmploymentFedStateGov>".ToLower()))
															{
																double[] array18 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector13[num20] = array18[0];
																vector14[num20] = array18[1];
																num20++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateEmploymentImputedGov>".ToLower(), false) == 0)
														{
															int num21 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateEmploymentImputedGov>".ToLower()))
															{
																double[] array19 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector15[num21] = array19[0];
																vector16[num21] = array19[1];
																num21++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateEmploymentTotalGov>".ToLower(), false) == 0)
														{
															int num22 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateEmploymentTotalGov>".ToLower()))
															{
																double[] array20 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector17[num22] = array20[0];
																vector18[num22] = array20[1];
																num22++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateECBase>".ToLower(), false) == 0)
														{
															int num23 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateECBase>".ToLower()))
															{
																double[] array21 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector35[num23] = array21[0];
																vector36[num23] = array21[1];
																num23++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateECFedStateGov>".ToLower(), false) == 0)
														{
															int num24 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateECFedStateGov>".ToLower()))
															{
																double[] array22 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector37[num24] = array22[0];
																vector38[num24] = array22[1];
																num24++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateECImputedGov>".ToLower(), false) == 0)
														{
															int num25 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateECImputedGov>".ToLower()))
															{
																double[] array23 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector39[num25] = array23[0];
																vector40[num25] = array23[1];
																num25++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateECTotalGov>".ToLower(), false) == 0)
														{
															int num26 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateECTotalGov>".ToLower()))
															{
																double[] array24 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector41[0] = array24[0];
																vector42[0] = array24[1];
																num26++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateTBase>".ToLower(), false) == 0)
														{
															int num27 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateTBase>".ToLower()))
															{
																double[] array25 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector43[num27] = array25[0];
																vector44[num27] = array25[1];
																num27++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateTFedStateGov>".ToLower(), false) == 0)
														{
															int num28 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateTFedStateGov>".ToLower()))
															{
																double[] array26 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector45[num28] = array26[0];
																vector46[num28] = array26[1];
																num28++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateTImputedGov>".ToLower(), false) == 0)
														{
															int num29 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateTImputedGov>".ToLower()))
															{
																double[] array27 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector47[num29] = array27[0];
																vector48[num29] = array27[1];
																num29++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateTTotalGov>".ToLower(), false) == 0)
														{
															int num30 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateTTotalGov>".ToLower()))
															{
																double[] array28 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector49[num30] = array28[0];
																vector50[num30] = array28[1];
																num30++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateGOSBase>".ToLower(), false) == 0)
														{
															int num31 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateGOSBase>".ToLower()))
															{
																double[] array29 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector27[num31] = array29[0];
																vector28[num31] = array29[1];
																num31++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateGOSFedStateGov>".ToLower(), false) == 0)
														{
															int num32 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateGOSFedStateGov>".ToLower()))
															{
																double[] array30 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector29[num32] = array30[0];
																vector30[num32] = array30[1];
																num32++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateGOSImputedGov>".ToLower(), false) == 0)
														{
															int num33 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateGOSImputedGov>".ToLower()))
															{
																double[] array31 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector31[num33] = array31[0];
																vector32[num33] = array31[1];
																num33++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateGOSTotalGov>".ToLower(), false) == 0)
														{
															int num34 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateGOSTotalGov>".ToLower()))
															{
																double[] array32 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector33[num34] = array32[0];
																vector34[num34] = array32[1];
																num34++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateGDPBase>".ToLower(), false) == 0)
														{
															int num35 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateGDPBase>".ToLower()))
															{
																double[] array33 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector19[num35] = array33[0];
																vector20[num35] = array33[1];
																num35++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateGDPFedStateGov>".ToLower(), false) == 0)
														{
															int num36 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateGDPFedStateGov>".ToLower()))
															{
																double[] array34 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector21[num36] = array34[0];
																vector22[num36] = array34[1];
																num36++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateGDPImputedGov>".ToLower(), false) == 0)
														{
															int num37 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateGDPImputedGov>".ToLower()))
															{
																double[] array35 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector23[num37] = array35[0];
																vector24[num37] = array35[1];
																num37++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<stateGDPTotalGov>".ToLower(), false) == 0)
														{
															int num38 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</stateGDPTotalGov>".ToLower()))
															{
																double[] array36 = this.StrArr_2_DblArr(text2.Split('\t', ' '));
																vector25[num38] = array36[0];
																vector26[num38] = array36[1];
																num38++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
														else if (Operators.CompareString(left4, "<statePCE>".ToLower(), false) == 0)
														{
															int num39 = 0;
															num4++;
															text2 = list[num4];
															text3 = text2.ToLower();
															while (!text3.Contains("</statePCE>".ToLower()))
															{
																double num41 = vector51[num39] = double.Parse(text3);
																num39++;
																num4++;
																text2 = list[num4];
																text3 = text2.ToLower();
															}
														}
													}
													StateData stateData = new StateData(text4, num3);
													stateData.PersonalIncome = vector7[0];
													stateData.DisposableIncome = vector8[0];
													stateData.TotFedExp = vector10[0];
													stateData.TotSLEExp = vector9[0];
													stateData.EC_TotGovSUM = vector41[0];
													stateData.EC_TotGovSUM_flag = vector42[0];
													stateData.Employment_TotGovSUM = vector17[0];
													stateData.Employment_TotGovSUM_flag = vector18[0];
													stateData.T_TotGovSUM = vector49[0];
													stateData.T_TotGovSUM_flag = vector50[0];
													stateData.GDP_TotGovSUM = vector25[0];
													stateData.GDP_TotGovSUM_flag = vector26[0];
													stateData.GOS_TotGovSUM = vector33[0];
													stateData.GOS_TotGovSUM_flag = vector34[0];
													stateData.EC_ImpGov = vector39;
													stateData.EC_ImpGov_flag = vector40;
													stateData.Employment_ImpGov = vector15;
													stateData.Employment_ImpGov_flag = vector16;
													stateData.T_ImpGov = vector47;
													stateData.T_ImpGov_flag = vector48;
													stateData.GDP_ImpGov = vector23;
													stateData.GDP_ImpGov_flag = vector24;
													stateData.GOS_ImpGov = vector31;
													stateData.GOS_ImpGov_flag = vector32;
													stateData.EC_FedState_Gov = vector37;
													stateData.EC_FedState_Gov_flag = vector38;
													stateData.Employment_FedState = vector13;
													stateData.Employment_FedState_flag = vector14;
													stateData.T_FedState = vector45;
													stateData.T_FedState_flag = vector46;
													stateData.GDP_FedState_Gov = vector21;
													stateData.GDP_FedState_Gov_flag = vector22;
													stateData.GOS_FedState = vector29;
													stateData.GOS_FedState_Gov_flag = vector30;
													stateData.EC_base = vector35;
													stateData.EC_base_flag = vector36;
													stateData.Employment_base = vector11;
													stateData.Employment_base_flag = vector12;
													stateData.T_base = vector43;
													stateData.T_base_flag = vector44;
													stateData.GDP_base = vector19;
													stateData.GDP_base_flag = vector20;
													stateData.GOS_base = vector27;
													stateData.GOS_base_flag = vector28;
													stateData.StateSetup(ref this.bd.NTables.FTE_Ratio);
													this.bd.RegStateData.addState(text4, ref stateData);
												}
											}
										}
									}
									this.bd.hdrVA = hdrVA;
									this.bd.hdrFinDem = hdrFinDem;
									this.bd.NTables.hdrIndBase = hdrIndBase;
									this.bd.NTables.hdrIndAdjGov = hdrIndAdjGov;
									this.bd.NTables.hdrIndFedStateLocGov = hdrIndFedStateLocGov;
									this.bd.NTables.hdrIndTotGov = hdrIndTotGov;
									this.bd.NTables.hdrIndExpandedGov = hdrIndExpandedGov;
									this.bd.IndBaseClass = indBaseClass;
									this.bd.IndAdjGovClass = indAdjGovClass;
									this.bd.IndFedStateLocGovClass = indFedStateLocGovClass;
									this.bd.IndTotGovClass = indTotGovClass;
									this.bd.IndExpandedGovClass = indExpandedGovClass;
									this.bd.hdrCommBase = hdrCommBase;
									this.bd.hdrCommAdjGov = hdrCommAdjGov;
									this.bd.hdrCommFedStateLocGov = hdrCommFedStateLocGov;
									this.bd.hdrCommTotGov = hdrCommTotGov;
									this.bd.hdrNoncompImp = hdrNoncompImp;
									this.bd.hdrScrap = hdrScrap;
									this.bd.hdrCommExpandedGov = hdrCommExpandedGov;
									this.bd.SetupHeaders();
									this.bd.N_INDUSTRIES = num;
									this.bd.N_COMMODITIES = num2;
									this.bd.N_INDUSTRIES_BASE = num3;
									this.bd.PersonalIncome = vector3[0];
									this.bd.DisposableIncome = vector4[0];
									this.bd.TotFedExp_2005 = vector5[0];
									this.bd.TotSLExp_2008 = vector6[0];
									this.bd.NTables.VA = matrix;
									this.bd.NTables.SetMake(ref matrix2);
									this.bd.Use = matrix3;
									this.bd.FinDem = matrix4;
									this.bd.Import = matrix5;
									if (this.bd.worksum.dataregionalized.Contains("Yes"))
									{
										this.bd.NTables.RegEmployment_JOBS = vector;
									}
									else
									{
										this.bd.NTables.Employment_JOBS = vector;
									}
									this.bd.NTables.FTE_Ratio.setupFTE(ref vector2, ref vector);
									this.bd.RegReset();
									this.bd.worksum.datayear = datayear;
									this.bd.worksum.DataTypeCode = bcode;
									this.bd.worksum.DataVersion = dataVersion;
									if (this.bd.worksum.dataregionalized.Contains("Yes"))
									{
										this.bd.NTables.RegEmployment_FTE = this.bd.NTables.FTE_Ratio.getFTE_RATIO() * this.bd.NTables.RegEmployment_JOBS;
										this.bd.NTables.RegEmployment = this.bd.NTables.RegEmployment_FTE;
										this.bd.NTables.Employment_FTE = this.bd.NTables.RegEmployment_FTE;
										this.bd.NTables.Employment = this.bd.NTables.Employment_FTE;
									}
									else
									{
										this.bd.NTables.Employment_FTE = this.bd.NTables.FTE_Ratio.getFTE_RATIO() * this.bd.NTables.Employment_JOBS;
										this.bd.NTables.Employment = this.bd.NTables.Employment_FTE;
									}
									this.bd.ConsolidateData();
									this.bd.ExtractScrapFromMake();
									this.bd.ExtractNCIFromMake();
									this.bd.putNCIToMake();
									this.bd.eDataState = BEAData.DataState.Loaded;
								}
							}
						}
					}
					this.isSuccess = true;
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox("Error while reading user modified data\r\n" + ex2.ToString(), MsgBoxStyle.OkOnly, null);
					this.isSuccess = false;
					ProjectData.ClearProjectError();
				}
				return this.bd;
			}
		}

		private List<string> CleanDataString(string s)
		{
			List<string> list = new List<string>();
			string text = "";
			string str = "";
			int i = 0;
			for (string[] array = s.ToString().Split('\r'); i < array.Length; i = checked(i + 1))
			{
				text = array[i].Trim('\t', ' ', '\r', '\n', '"');
				if (!(text.StartsWith(new string(new char[1]
				{
					'#'
				})) | text.Length == 0))
				{
					list.Add(str + text);
				}
			}
			return list;
		}

		private string ExtractString(object s)
		{
			return Conversions.ToString(NewLateBinding.LateGet(NewLateBinding.LateGet(s, null, "Substring", new object[1]
			{
				6
			}, null, null, null), null, "Trim", new object[1]
			{
				new char[2]
				{
					' ',
					'>'
				}
			}, null, null, null));
		}

		private int ExtractInt(object s)
		{
			int result = 0;
			int.TryParse(Conversions.ToString(NewLateBinding.LateGet(NewLateBinding.LateGet(s, null, "Substring", new object[1]
			{
				6
			}, null, null, null), null, "Trim", new object[1]
			{
				new char[2]
				{
					' ',
					'>'
				}
			}, null, null, null)), out result);
			return result;
		}

		private double[] StrArr_2_DblArr(string[] arr)
		{
			ArrayList arrayList = new ArrayList();
			double num = 0.0;
			checked
			{
				int num2 = arr.Length - 1;
				for (int i = 0; i <= num2; i++)
				{
					if (double.TryParse(arr[i], out num))
					{
						arrayList.Add(num);
					}
				}
			}
			return (double[])arrayList.ToArray(typeof(double));
		}
	}
}
