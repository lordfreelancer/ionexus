using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace IOSNAP
{
	public class LicenseDat
	{
		public bool Isvalid;

		public bool IsLicenseFile;

		public bool IsDataValid;

		public DateTime StartingDate;

		public DateTime ExpirationDate;

		public string LicenseKey;

		public string DataVersion;

		public string ProgramVersion;

		public int LicenseLength;

		public string LicenseType;

		public string MAC_Code;

		public string ActualMAC;

		public bool isMacRecognized;

		private string LicenseContent;

		public LicenseDat()
		{
            //this.Isvalid = false;
            this.Isvalid = true;
            this.IsLicenseFile = false;
			this.IsDataValid = false;
			this.StartingDate = DateTime.Now;
			this.ExpirationDate = this.StartingDate.AddMonths(3);
			this.LicenseKey = "AAAABBBBCCCCDDDD";
			this.DataVersion = "0.0";
			this.ProgramVersion = "0.0";
			this.LicenseLength = Conversions.ToInteger("0");
			this.LicenseType = "Demo";
			this.MAC_Code = "ZZZZZZ";
			this.ActualMAC = "CCCCCC";
			this.isMacRecognized = false;
			this.LicenseContent = "";
			this.IsLicenseFile = this.ReadLicenseFile();
			if (this.IsLicenseFile)
			{
				this.IsDataValid = this.ExtractData(ref this.LicenseContent);
				this.isMacRecognized = this.CheckMAC(ref this.MAC_Code);
			}
			if (this.isMacRecognized & this.IsLicenseFile & this.IsDataValid)
			{
				this.Isvalid = true;
			}
		}

		public LicenseDat(ref string Rcode)
		{
			this.Isvalid = false;
			this.IsLicenseFile = false;
			this.IsDataValid = false;
			this.StartingDate = DateTime.Now;
			this.ExpirationDate = this.StartingDate.AddMonths(3);
			this.LicenseKey = "AAAABBBBCCCCDDDD";
			this.DataVersion = "0.0";
			this.ProgramVersion = "0.0";
			this.LicenseLength = Conversions.ToInteger("0");
			this.LicenseType = "Demo";
			this.MAC_Code = "ZZZZZZ";
			this.ActualMAC = "CCCCCC";
			this.isMacRecognized = false;
			this.LicenseContent = "";
			this.LicenseKey = Rcode;
		}

		public bool CheckMAC(ref string mac)
		{
			bool result = false;
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			NetworkInterface[] array = allNetworkInterfaces;
			foreach (NetworkInterface networkInterface in array)
			{
				if (string.Equals(networkInterface.GetPhysicalAddress().ToString(), mac))
				{
					result = true;
					this.ActualMAC = networkInterface.GetPhysicalAddress().ToString();
				}
			}
			return result;
		}

		public bool ReadLicenseFile()
		{
			bool flag = false;
			string text = "";
			try
			{
				text = FileSystem.CurDir() + "\\License.dat";
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				FileStream fileStream = new FileStream(text, FileMode.Open, FileAccess.Read);
				string s = Conversions.ToString(binaryFormatter.Deserialize(fileStream));
				byte[] bytes = Convert.FromBase64String(s);
				this.LicenseContent = Encoding.ASCII.GetString(bytes);
				fileStream.Close();
				flag = true;
			}
			catch (Exception ex)
			{
				ProjectData.SetProjectError(ex);
				Exception ex2 = ex;
				flag = false;
				ProjectData.ClearProjectError();
			}
			return flag;
		}

		private bool ExtractData(ref string sl)
		{
			bool flag = false;
			List<string> list = new List<string>();
			list = this.CleanDataString(sl);
			int num = -1;
			string text = "";
			string text2 = "";
			checked
			{
				try
				{
					while (num < list.Count - 1)
					{
						num++;
						text = list[num];
						if (text.Contains("<RegistrationCode>"))
						{
							string text3 = this.LicenseKey = text.Substring(18, 16);
						}
						if (text.Contains("<LicenseLength>"))
						{
							string s = text.Substring(15, text.Length - 31);
							int.TryParse(s, out this.LicenseLength);
						}
						if (text.Contains("<StartingDate>"))
						{
							string stringDate = text.Substring(14, text.Length - 29);
							this.StartingDate = DateAndTime.DateValue(stringDate);
						}
						if (text.Contains("<AccessLevel>"))
						{
							string text4 = this.LicenseType = text.Substring(13, text.Length - 27);
						}
						if (text.Contains("<DataVersion>"))
						{
							string text5 = this.DataVersion = text.Substring(13, text.Length - 27);
						}
						if (text.Contains("<ProgramVersion>"))
						{
							string text6 = this.ProgramVersion = text.Substring(16, text.Length - 33);
						}
						if (text.Contains("<MAC>"))
						{
							string text7 = this.MAC_Code = text.Substring(5, text.Length - 11);
						}
					}
					this.CalculateExpirationDate();
					flag = true;
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.ToString(), MsgBoxStyle.OkOnly, null);
					flag = false;
					ProjectData.ClearProjectError();
				}
				return flag;
			}
		}

		private void CalculateExpirationDate()
		{
			if (Operators.CompareString(this.LicenseType, "Demo", false) == 0)
			{
				this.ExpirationDate = this.StartingDate.AddMonths(3);
			}
			string licenseType = this.LicenseType;
			if (Operators.CompareString(licenseType, "Student", false) != 0)
			{
				if (Operators.CompareString(licenseType, "Demo", false) != 0)
				{
					if (Operators.CompareString(licenseType, "Full", false) == 0)
					{
						this.ExpirationDate = this.StartingDate.AddYears(1);
					}
				}
				else
				{
					this.ExpirationDate = this.StartingDate.AddMonths(3);
				}
			}
			else
			{
				this.ExpirationDate = this.StartingDate.AddYears(1);
			}
		}

		private int ExtractInt(object s)
		{
			int result = 0;
			int.TryParse(Conversions.ToString(NewLateBinding.LateGet(NewLateBinding.LateGet(s, null, "Substring", new object[1]
			{
				6
			}, null, null, null), null, "Trim", new object[1]
			{
				new char[2]
				{
					' ',
					'>'
				}
			}, null, null, null)), out result);
			return result;
		}

		private List<string> CleanDataString(string s)
		{
			List<string> list = new List<string>();
			string text = "";
			string str = "";
			int i = 0;
			for (string[] array = s.ToString().Split('\r'); i < array.Length; i = checked(i + 1))
			{
				text = array[i].Trim('\t', ' ', '\r', '\n', '"');
				if (!(text.StartsWith(new string(new char[1]
				{
					'#'
				})) | text.Length == 0))
				{
					list.Add(str + text);
				}
			}
			return list;
		}

		public void saveLicense()
		{
			string str = "";
			str = str + "<RegistrationCode>" + this.LicenseKey + "</RegistrationCode>\r\n";
			str = str + "<LicenseLength>" + Conversions.ToString(this.LicenseLength) + "</LicenseLength>\r\n";
			str = str + "<StartingDate>" + this.StartingDate.ToString("MM/dd/yyyy") + "</StartingDate>\r\n";
			str = str + "<AccessLevel>" + this.LicenseType + "</AccessLevel>\r\n";
			str = str + "<DataVersion>" + this.DataVersion + "</DataVersion>\r\n";
			str = str + "<ProgramVersion>" + this.ProgramVersion + "</ProgramVersion>\r\n";
			str = str + "<MAC>" + this.ActualMAC + "</MAC>\r\n";
			if (!this.WriteLicenseFile(str))
			{
				Interaction.MsgBox("Error while generating license file!", MsgBoxStyle.OkOnly, null);
			}
			string licenseType = this.LicenseType;
			if (Operators.CompareString(licenseType, "Student", false) != 0)
			{
				if (Operators.CompareString(licenseType, "Demo", false) != 0)
				{
					if (Operators.CompareString(licenseType, "Full", false) == 0)
					{
						this.ExpirationDate = this.StartingDate.AddYears(1);
					}
				}
				else
				{
					this.ExpirationDate = this.StartingDate.AddMonths(3);
				}
			}
			else
			{
				this.ExpirationDate = this.StartingDate.AddYears(1);
			}
		}

		public bool WriteLicenseFile(string sl)
		{
			bool flag = false;
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			string text = "";
			try
			{
				text = FileSystem.CurDir() + "\\License.dat";
				ASCIIEncoding aSCIIEncoding = new ASCIIEncoding();
				byte[] bytes = aSCIIEncoding.GetBytes(sl);
				string graph = Convert.ToBase64String(bytes);
				FileStream fileStream = new FileStream(text, FileMode.OpenOrCreate, FileAccess.Write);
				binaryFormatter.Serialize(fileStream, graph);
				fileStream.Close();
				flag = true;
			}
			catch (Exception ex)
			{
				ProjectData.SetProjectError(ex);
				Exception ex2 = ex;
				string prompt = "There was an error while registering your product.In case of shared network locations, make sure the program is not being used somewhere else.Please try again.";
				Interaction.MsgBox(prompt, MsgBoxStyle.OkOnly, null);
				flag = false;
				ProjectData.ClearProjectError();
			}
			return flag;
		}
	}
}
