using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class RequirementsTablesForm : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("UserRegisteredRadioButton")]
		private RadioButton _UserRegisteredRadioButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NationalRadioButton")]
		private RadioButton _NationalRadioButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Panel1")]
		private Panel _Panel1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rb_TRC")]
		private RadioButton _rb_TRC;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rb_TRO")]
		private RadioButton _rb_TRO;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rb_DRC")]
		private RadioButton _rb_DRC;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rb_DRO")]
		private RadioButton _rb_DRO;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RegisteredDataCbox")]
		private ComboBox _RegisteredDataCbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NatYearCbox")]
		private ComboBox _NatYearCbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("DisplayButton")]
		private Button _DisplayButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox2")]
		private GroupBox _GroupBox2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox1")]
		private GroupBox _GroupBox1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cb_CxC")]
		private CheckBox _cb_CxC;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cb_IxC")]
		private CheckBox _cb_IxC;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cb_IxI")]
		private CheckBox _cb_IxI;

		private DataWrapREADER mainData;

		private DataWrapREADER yearlyData;

		private DataWrapREADER userData;

		private string PSelect;

		public BEAData bdat;

		public bool isCxC;

		public bool isIxC;

		public bool isIxI;

		public bool isDRO;

		public bool isDRC;

		public bool isTRO;

		public bool isTRC;

		public bool isnat;

		public int idxnat;

		internal virtual RadioButton UserRegisteredRadioButton
		{
			[CompilerGenerated]
			get
			{
				return this._UserRegisteredRadioButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.UserRegisteredRadioButton_CheckedChanged;
				RadioButton userRegisteredRadioButton = this._UserRegisteredRadioButton;
				if (userRegisteredRadioButton != null)
				{
					userRegisteredRadioButton.CheckedChanged -= value2;
				}
				this._UserRegisteredRadioButton = value;
				userRegisteredRadioButton = this._UserRegisteredRadioButton;
				if (userRegisteredRadioButton != null)
				{
					userRegisteredRadioButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton NationalRadioButton
		{
			[CompilerGenerated]
			get
			{
				return this._NationalRadioButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NationalRadioButton_CheckedChanged;
				RadioButton nationalRadioButton = this._NationalRadioButton;
				if (nationalRadioButton != null)
				{
					nationalRadioButton.CheckedChanged -= value2;
				}
				this._NationalRadioButton = value;
				nationalRadioButton = this._NationalRadioButton;
				if (nationalRadioButton != null)
				{
					nationalRadioButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual Panel Panel1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rb_TRC
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rb_TRO
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rb_DRC
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rb_DRO
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ComboBox RegisteredDataCbox
		{
			[CompilerGenerated]
			get
			{
				return this._RegisteredDataCbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.RegisteredDataCbox_SelectedIndexChanged;
				ComboBox registeredDataCbox = this._RegisteredDataCbox;
				if (registeredDataCbox != null)
				{
					registeredDataCbox.SelectedIndexChanged -= value2;
				}
				this._RegisteredDataCbox = value;
				registeredDataCbox = this._RegisteredDataCbox;
				if (registeredDataCbox != null)
				{
					registeredDataCbox.SelectedIndexChanged += value2;
				}
			}
		}

		internal virtual ComboBox NatYearCbox
		{
			[CompilerGenerated]
			get
			{
				return this._NatYearCbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.natYearCbox_SelectedIndexChanged;
				ComboBox natYearCbox = this._NatYearCbox;
				if (natYearCbox != null)
				{
					natYearCbox.SelectedIndexChanged -= value2;
				}
				this._NatYearCbox = value;
				natYearCbox = this._NatYearCbox;
				if (natYearCbox != null)
				{
					natYearCbox.SelectedIndexChanged += value2;
				}
			}
		}

		internal virtual Label Label1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button Button2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button DisplayButton
		{
			[CompilerGenerated]
			get
			{
				return this._DisplayButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Display_Click;
				Button displayButton = this._DisplayButton;
				if (displayButton != null)
				{
					displayButton.Click -= value2;
				}
				this._DisplayButton = value;
				displayButton = this._DisplayButton;
				if (displayButton != null)
				{
					displayButton.Click += value2;
				}
			}
		}

		internal virtual GroupBox GroupBox2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual GroupBox GroupBox1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox cb_CxC
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox cb_IxC
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox cb_IxI
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.UserRegisteredRadioButton = new RadioButton();
			this.NationalRadioButton = new RadioButton();
			this.Panel1 = new Panel();
			this.GroupBox2 = new GroupBox();
			this.cb_CxC = new CheckBox();
			this.cb_IxC = new CheckBox();
			this.cb_IxI = new CheckBox();
			this.GroupBox1 = new GroupBox();
			this.rb_DRO = new RadioButton();
			this.rb_DRC = new RadioButton();
			this.rb_TRO = new RadioButton();
			this.rb_TRC = new RadioButton();
			this.RegisteredDataCbox = new ComboBox();
			this.NatYearCbox = new ComboBox();
			this.Label1 = new Label();
			this.Button2 = new Button();
			this.DisplayButton = new Button();
			this.Panel1.SuspendLayout();
			this.GroupBox2.SuspendLayout();
			this.GroupBox1.SuspendLayout();
			base.SuspendLayout();
			this.UserRegisteredRadioButton.AutoSize = true;
			this.UserRegisteredRadioButton.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.UserRegisteredRadioButton.Location = new Point(86, 146);
			this.UserRegisteredRadioButton.Margin = new Padding(3, 4, 3, 4);
			this.UserRegisteredRadioButton.Name = "UserRegisteredRadioButton";
			this.UserRegisteredRadioButton.Size = new Size(131, 23);
			this.UserRegisteredRadioButton.TabIndex = 22;
			this.UserRegisteredRadioButton.TabStop = true;
			this.UserRegisteredRadioButton.Text = "User Registered";
			this.UserRegisteredRadioButton.UseVisualStyleBackColor = true;
			this.NationalRadioButton.AutoSize = true;
			this.NationalRadioButton.Checked = true;
			this.NationalRadioButton.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.NationalRadioButton.Location = new Point(86, 84);
			this.NationalRadioButton.Margin = new Padding(3, 4, 3, 4);
			this.NationalRadioButton.Name = "NationalRadioButton";
			this.NationalRadioButton.Size = new Size(82, 23);
			this.NationalRadioButton.TabIndex = 20;
			this.NationalRadioButton.TabStop = true;
			this.NationalRadioButton.Text = "National";
			this.NationalRadioButton.UseVisualStyleBackColor = true;
			this.Panel1.BorderStyle = BorderStyle.FixedSingle;
			this.Panel1.Controls.Add(this.GroupBox2);
			this.Panel1.Controls.Add(this.GroupBox1);
			this.Panel1.Location = new Point(40, 227);
			this.Panel1.Margin = new Padding(3, 4, 3, 4);
			this.Panel1.Name = "Panel1";
			this.Panel1.Size = new Size(739, 244);
			this.Panel1.TabIndex = 19;
			this.GroupBox2.Controls.Add(this.cb_CxC);
			this.GroupBox2.Controls.Add(this.cb_IxC);
			this.GroupBox2.Controls.Add(this.cb_IxI);
			this.GroupBox2.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.GroupBox2.Location = new Point(509, 16);
			this.GroupBox2.Margin = new Padding(3, 4, 3, 4);
			this.GroupBox2.Name = "GroupBox2";
			this.GroupBox2.Padding = new Padding(3, 4, 3, 4);
			this.GroupBox2.Size = new Size(199, 186);
			this.GroupBox2.TabIndex = 19;
			this.GroupBox2.TabStop = false;
			this.GroupBox2.Text = "Select Format";
			this.cb_CxC.AutoSize = true;
			this.cb_CxC.Checked = true;
			this.cb_CxC.CheckState = CheckState.Checked;
			this.cb_CxC.Location = new Point(89, 118);
			this.cb_CxC.Margin = new Padding(3, 4, 3, 4);
			this.cb_CxC.Name = "cb_CxC";
			this.cb_CxC.Size = new Size(51, 23);
			this.cb_CxC.TabIndex = 20;
			this.cb_CxC.Text = "CxC";
			this.cb_CxC.UseVisualStyleBackColor = true;
			this.cb_IxC.AutoSize = true;
			this.cb_IxC.Checked = true;
			this.cb_IxC.CheckState = CheckState.Checked;
			this.cb_IxC.Location = new Point(89, 79);
			this.cb_IxC.Margin = new Padding(3, 4, 3, 4);
			this.cb_IxC.Name = "cb_IxC";
			this.cb_IxC.Size = new Size(47, 23);
			this.cb_IxC.TabIndex = 19;
			this.cb_IxC.Text = "IxC";
			this.cb_IxC.UseVisualStyleBackColor = true;
			this.cb_IxI.AutoSize = true;
			this.cb_IxI.Checked = true;
			this.cb_IxI.CheckState = CheckState.Checked;
			this.cb_IxI.Location = new Point(89, 37);
			this.cb_IxI.Margin = new Padding(3, 4, 3, 4);
			this.cb_IxI.Name = "cb_IxI";
			this.cb_IxI.Size = new Size(43, 23);
			this.cb_IxI.TabIndex = 18;
			this.cb_IxI.Text = "IxI";
			this.cb_IxI.UseVisualStyleBackColor = true;
			this.GroupBox1.Controls.Add(this.rb_DRO);
			this.GroupBox1.Controls.Add(this.rb_DRC);
			this.GroupBox1.Controls.Add(this.rb_TRO);
			this.GroupBox1.Controls.Add(this.rb_TRC);
			this.GroupBox1.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.GroupBox1.Location = new Point(28, 16);
			this.GroupBox1.Margin = new Padding(3, 4, 3, 4);
			this.GroupBox1.Name = "GroupBox1";
			this.GroupBox1.Padding = new Padding(3, 4, 3, 4);
			this.GroupBox1.Size = new Size(454, 186);
			this.GroupBox1.TabIndex = 18;
			this.GroupBox1.TabStop = false;
			this.GroupBox1.Text = "Select Table";
			this.rb_DRO.AutoSize = true;
			this.rb_DRO.Checked = true;
			this.rb_DRO.Font = new Font("Calibri", 11f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.rb_DRO.Location = new Point(87, 30);
			this.rb_DRO.Margin = new Padding(3, 4, 3, 4);
			this.rb_DRO.Name = "rb_DRO";
			this.rb_DRO.Size = new Size(242, 22);
			this.rb_DRO.TabIndex = 6;
			this.rb_DRO.TabStop = true;
			this.rb_DRO.Text = "Direct Requirements - Open Model";
			this.rb_DRO.UseVisualStyleBackColor = true;
			this.rb_DRC.AutoSize = true;
			this.rb_DRC.Font = new Font("Calibri", 11f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.rb_DRC.Location = new Point(87, 66);
			this.rb_DRC.Margin = new Padding(3, 4, 3, 4);
			this.rb_DRC.Name = "rb_DRC";
			this.rb_DRC.Size = new Size(250, 22);
			this.rb_DRC.TabIndex = 7;
			this.rb_DRC.Text = "Direct Requirements - Closed Model";
			this.rb_DRC.UseVisualStyleBackColor = true;
			this.rb_TRO.AutoSize = true;
			this.rb_TRO.Font = new Font("Calibri", 11f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.rb_TRO.Location = new Point(87, 102);
			this.rb_TRO.Margin = new Padding(3, 4, 3, 4);
			this.rb_TRO.Name = "rb_TRO";
			this.rb_TRO.Size = new Size(235, 22);
			this.rb_TRO.TabIndex = 8;
			this.rb_TRO.Text = "Total Requirements - Open Model";
			this.rb_TRO.UseVisualStyleBackColor = true;
			this.rb_TRC.AutoSize = true;
			this.rb_TRC.Font = new Font("Calibri", 11f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.rb_TRC.Location = new Point(87, 139);
			this.rb_TRC.Margin = new Padding(3, 4, 3, 4);
			this.rb_TRC.Name = "rb_TRC";
			this.rb_TRC.Size = new Size(243, 22);
			this.rb_TRC.TabIndex = 9;
			this.rb_TRC.Text = "Total Requirements - Closed Model";
			this.rb_TRC.UseVisualStyleBackColor = true;
			this.RegisteredDataCbox.FormattingEnabled = true;
			this.RegisteredDataCbox.Location = new Point(283, 150);
			this.RegisteredDataCbox.Margin = new Padding(3, 4, 3, 4);
			this.RegisteredDataCbox.Name = "RegisteredDataCbox";
			this.RegisteredDataCbox.Size = new Size(495, 23);
			this.RegisteredDataCbox.TabIndex = 18;
			this.NatYearCbox.FormattingEnabled = true;
			this.NatYearCbox.Location = new Point(283, 84);
			this.NatYearCbox.Margin = new Padding(3, 4, 3, 4);
			this.NatYearCbox.Name = "NatYearCbox";
			this.NatYearCbox.Size = new Size(495, 23);
			this.NatYearCbox.TabIndex = 16;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Calibri", 14f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label1.Location = new Point(80, 11);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(158, 23);
			this.Label1.TabIndex = 15;
			this.Label1.Text = "Select IO Accounts";
			this.Button2.DialogResult = DialogResult.Cancel;
			this.Button2.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button2.Location = new Point(477, 500);
			this.Button2.Margin = new Padding(3, 4, 3, 4);
			this.Button2.Name = "Button2";
			this.Button2.Size = new Size(122, 48);
			this.Button2.TabIndex = 14;
			this.Button2.Text = "Cancel";
			this.Button2.UseVisualStyleBackColor = true;
			this.DisplayButton.DialogResult = DialogResult.OK;
			this.DisplayButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.DisplayButton.Location = new Point(657, 500);
			this.DisplayButton.Margin = new Padding(3, 4, 3, 4);
			this.DisplayButton.Name = "DisplayButton";
			this.DisplayButton.Size = new Size(122, 48);
			this.DisplayButton.TabIndex = 13;
			this.DisplayButton.Text = "Display";
			this.DisplayButton.UseVisualStyleBackColor = true;
			base.AutoScaleDimensions = new SizeF(7f, 15f);
			base.AutoScaleMode = AutoScaleMode.Font;
			this.AutoScroll = true;
			base.ClientSize = new Size(819, 587);
			base.Controls.Add(this.UserRegisteredRadioButton);
			base.Controls.Add(this.NationalRadioButton);
			base.Controls.Add(this.Panel1);
			base.Controls.Add(this.RegisteredDataCbox);
			base.Controls.Add(this.NatYearCbox);
			base.Controls.Add(this.Label1);
			base.Controls.Add(this.Button2);
			base.Controls.Add(this.DisplayButton);
			this.Font = new Font("Calibri", 10f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.Margin = new Padding(3, 4, 3, 4);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "RequirementsTablesForm";
			this.Text = "Requirements Tables";
			this.Panel1.ResumeLayout(false);
			this.GroupBox2.ResumeLayout(false);
			this.GroupBox2.PerformLayout();
			this.GroupBox1.ResumeLayout(false);
			this.GroupBox1.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public RequirementsTablesForm(ref DataWrapREADER d, ref int idx)
		{
			this.PSelect = "Please Select ...";
			this.isCxC = false;
			this.isIxC = false;
			this.isIxI = false;
			this.isDRO = true;
			this.isDRC = false;
			this.isTRO = false;
			this.isTRC = false;
			this.isnat = true;
			this.idxnat = 0;
			this.InitializeComponent();
			this.mainData = d;
			this.yearlyData = new DataWrapREADER();
			this.yearlyData.setEmpty();
			this.userData = new DataWrapREADER();
			this.userData.setEmpty();
			checked
			{
				int num = this.mainData.getNrec() - 1;
				for (int i = 0; i <= num; i++)
				{
					BEAData bEAData = unchecked((BEAData)this.mainData.bealist[i]);
					if (Conversions.ToBoolean(Operators.OrObject(Operators.CompareObjectEqual(bEAData.worksum.getDataType(), "Base Data", false), Operators.CompareObjectEqual(bEAData.worksum.getDataType(), "User Defined", false))))
					{
						this.yearlyData.addData(ref bEAData);
						if (i == idx)
						{
							this.isnat = true;
							this.idxnat = this.yearlyData.getNrec();
						}
					}
					else
					{
						this.userData.addData(ref bEAData);
						if (i == idx)
						{
							this.isnat = false;
							this.idxnat = this.userData.getNrec();
						}
					}
				}
				this.NatYearCbox.Items.Clear();
				int num2 = this.yearlyData.getNrec() - 1;
				for (int j = 0; j <= num2; j++)
				{
					this.NatYearCbox.Items.Add(this.yearlyData.getData(j).worksum.CurrentLabel);
				}
				this.NatYearCbox.DropDownStyle = ComboBoxStyle.DropDownList;
				if (this.userData.getNrec() > 0)
				{
					this.UserRegisteredRadioButton.Enabled = true;
					this.RegisteredDataCbox.Items.Clear();
					int num3 = this.userData.getNrec() - 1;
					for (int k = 0; k <= num3; k++)
					{
						this.RegisteredDataCbox.Items.Add(this.userData.getData(k).worksum.CurrentLabel);
					}
					this.RegisteredDataCbox.DropDownStyle = ComboBoxStyle.DropDownList;
					this.RegisteredDataCbox.SelectedItem = this.userData.getData(0).worksum.CurrentLabel;
				}
				else
				{
					this.UserRegisteredRadioButton.Enabled = false;
				}
				if (this.isnat)
				{
					this.NatYearCbox.Enabled = true;
					this.RegisteredDataCbox.Enabled = false;
					this.NationalRadioButton.Checked = true;
					this.UserRegisteredRadioButton.Checked = false;
					if (this.userData.getNrec() == 0)
					{
						this.UserRegisteredRadioButton.Enabled = false;
					}
					this.NatYearCbox.SelectedItem = this.yearlyData.getData(this.idxnat - 1).worksum.CurrentLabel;
					this.bdat = this.yearlyData.getData(this.idxnat - 1);
				}
				else
				{
					this.NatYearCbox.Enabled = false;
					this.RegisteredDataCbox.Enabled = true;
					this.NationalRadioButton.Checked = false;
					this.UserRegisteredRadioButton.Checked = true;
					this.RegisteredDataCbox.SelectedItem = this.userData.getData(this.idxnat - 1).worksum.CurrentLabel;
					this.bdat = this.userData.getData(this.idxnat - 1);
				}
			}
		}

		private void natYearCbox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = Conversions.ToString(this.NatYearCbox.SelectedItem);
            //TODO Later
			//if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 2;
			//		this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = false;
			//	}
			//	else if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//}
			//if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread)
			//{
			//	int selectedIndex = this.NatYearCbox.SelectedIndex;
			//	if (selectedIndex >= 0)
			//	{
			//		this.bdat = this.yearlyData.getData(selectedIndex);
			//	}
			//}
			//else
			//{
			//	this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = true;
			//}
		}

		private void RegisteredDataCbox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = Conversions.ToString(this.RegisteredDataCbox.SelectedItem);
            //TODO Later
			//if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 2;
			//		this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = false;
			//	}
			//	else if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//}
			//if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread)
			//{
			//	int selectedIndex = this.RegisteredDataCbox.SelectedIndex;
			//	if (selectedIndex >= 0)
			//	{
			//		this.bdat = this.userData.getData(selectedIndex);
			//	}
			//}
			//else
			//{
			//	this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = true;
			//}
		}

		private void UserRegisteredRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			if (this.UserRegisteredRadioButton.Checked)
			{
				this.NatYearCbox.Enabled = false;
				this.RegisteredDataCbox.Enabled = true;
				this.RegisteredDataCbox.SelectedItem = this.userData.getData(0).worksum.CurrentLabel;
				this.bdat = this.userData.getData(0);
			}
		}

		private void NationalRadioButton_CheckedChanged(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State = 2;
			//		this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun = true;
			//	}
			//	else if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init);
			//}
			//if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun)
			//{
			//	this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun = false;
			//}
			//else
			//{
			//	this.NatYearCbox.Enabled = true;
			//	this.RegisteredDataCbox.Enabled = false;
			//	this.NatYearCbox.SelectedItem = this.yearlyData.getData(0).worksum.CurrentLabel;
			//	this.bdat = this.yearlyData.getData(0);
			//}
		}

		public string getLabel()
		{
			return this.bdat.worksum.CurrentLabel;
		}

		private void Display_Click(object sender, EventArgs e)
		{
			this.isIxI = Conversions.ToBoolean(Interaction.IIf(this.cb_IxI.Checked, true, false));
			this.isIxC = Conversions.ToBoolean(Interaction.IIf(this.cb_IxC.Checked, true, false));
			this.isCxC = Conversions.ToBoolean(Interaction.IIf(this.cb_CxC.Checked, true, false));
			this.isDRO = Conversions.ToBoolean(Interaction.IIf(this.rb_DRO.Checked, true, false));
			this.isDRC = Conversions.ToBoolean(Interaction.IIf(this.rb_DRC.Checked, true, false));
			this.isTRO = Conversions.ToBoolean(Interaction.IIf(this.rb_TRO.Checked, true, false));
			this.isTRC = Conversions.ToBoolean(Interaction.IIf(this.rb_TRC.Checked, true, false));
		}
	}
}
