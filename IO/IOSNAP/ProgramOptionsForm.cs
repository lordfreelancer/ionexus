using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class ProgramOptionsForm : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OKButton")]
		private Button _OKButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("FederalStateGroupBox")]
		private GroupBox _FederalStateGroupBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ImputedIORadioButton")]
		private RadioButton _ImputedIORadioButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("FedStateLocalRadioButton")]
		private RadioButton _FedStateLocalRadioButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("TotGovRadioButton")]
		private RadioButton _TotGovRadioButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NumberFormatGroupBox")]
		private GroupBox _NumberFormatGroupBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CopyOptionsGroupBox")]
		private GroupBox _CopyOptionsGroupBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GenFormLabel")]
		private Label _GenFormLabel;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CopyTableHeadersCheckBox")]
		private CheckBox _CopyTableHeadersCheckBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox1")]
		private GroupBox _GroupBox1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("EmploymentJobsButton")]
		private RadioButton _EmploymentJobsButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("EmploymentFTEButton")]
		private RadioButton _EmploymentFTEButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CancelButtonOpt")]
		private Button _CancelButtonOpt;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CoeffFormatTBox")]
		private TextBox _CoeffFormatTBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GenFormTextBox")]
		private TextBox _GenFormTextBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ResetFormatButton")]
		private Button _ResetFormatButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RoundFormatTBox")]
		private TextBox _RoundFormatTBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		public BEAData bdat;

		public bool EmploymentChange;

		public bool TableHeaderChange;

		public bool isFormatChanged;

		public bool isGovAggLevelChanged;

		private ViewOptions options;

		private GenDataStatus EmpStatus;

		private bool copyHeaderStatus;

		private string initGovAggLevel;

		internal virtual Button OKButton
		{
			[CompilerGenerated]
			get
			{
				return this._OKButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OKButton_Click;
				Button oKButton = this._OKButton;
				if (oKButton != null)
				{
					oKButton.Click -= value2;
				}
				this._OKButton = value;
				oKButton = this._OKButton;
				if (oKButton != null)
				{
					oKButton.Click += value2;
				}
			}
		}

		internal virtual GroupBox FederalStateGroupBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton ImputedIORadioButton
		{
			[CompilerGenerated]
			get
			{
				return this._ImputedIORadioButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ImputedIORadioButton_CheckedChanged;
				RadioButton imputedIORadioButton = this._ImputedIORadioButton;
				if (imputedIORadioButton != null)
				{
					imputedIORadioButton.CheckedChanged -= value2;
				}
				this._ImputedIORadioButton = value;
				imputedIORadioButton = this._ImputedIORadioButton;
				if (imputedIORadioButton != null)
				{
					imputedIORadioButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton FedStateLocalRadioButton
		{
			[CompilerGenerated]
			get
			{
				return this._FedStateLocalRadioButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.FedStateLocalRadioButton_CheckedChanged;
				RadioButton fedStateLocalRadioButton = this._FedStateLocalRadioButton;
				if (fedStateLocalRadioButton != null)
				{
					fedStateLocalRadioButton.CheckedChanged -= value2;
				}
				this._FedStateLocalRadioButton = value;
				fedStateLocalRadioButton = this._FedStateLocalRadioButton;
				if (fedStateLocalRadioButton != null)
				{
					fedStateLocalRadioButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton TotGovRadioButton
		{
			[CompilerGenerated]
			get
			{
				return this._TotGovRadioButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.TotGovRadioButton_CheckedChanged;
				RadioButton totGovRadioButton = this._TotGovRadioButton;
				if (totGovRadioButton != null)
				{
					totGovRadioButton.CheckedChanged -= value2;
				}
				this._TotGovRadioButton = value;
				totGovRadioButton = this._TotGovRadioButton;
				if (totGovRadioButton != null)
				{
					totGovRadioButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual GroupBox NumberFormatGroupBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual GroupBox CopyOptionsGroupBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label GenFormLabel
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox CopyTableHeadersCheckBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual GroupBox GroupBox1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton EmploymentJobsButton
		{
			[CompilerGenerated]
			get
			{
				return this._EmploymentJobsButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.EmploymentJOBS_CheckedChanged;
				RadioButton employmentJobsButton = this._EmploymentJobsButton;
				if (employmentJobsButton != null)
				{
					employmentJobsButton.CheckedChanged -= value2;
				}
				this._EmploymentJobsButton = value;
				employmentJobsButton = this._EmploymentJobsButton;
				if (employmentJobsButton != null)
				{
					employmentJobsButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton EmploymentFTEButton
		{
			[CompilerGenerated]
			get
			{
				return this._EmploymentFTEButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.EmploymentFTE_CheckedChanged;
				RadioButton employmentFTEButton = this._EmploymentFTEButton;
				if (employmentFTEButton != null)
				{
					employmentFTEButton.CheckedChanged -= value2;
				}
				this._EmploymentFTEButton = value;
				employmentFTEButton = this._EmploymentFTEButton;
				if (employmentFTEButton != null)
				{
					employmentFTEButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual Button CancelButtonOpt
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label Label1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual TextBox CoeffFormatTBox
		{
			[CompilerGenerated]
			get
			{
				return this._CoeffFormatTBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = delegate
				{
					this.FormatChange_Listener();
				};
				TextBox coeffFormatTBox = this._CoeffFormatTBox;
				if (coeffFormatTBox != null)
				{
					coeffFormatTBox.ModifiedChanged -= value2;
				}
				this._CoeffFormatTBox = value;
				coeffFormatTBox = this._CoeffFormatTBox;
				if (coeffFormatTBox != null)
				{
					coeffFormatTBox.ModifiedChanged += value2;
				}
			}
		}

		internal virtual Label Label2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual TextBox GenFormTextBox
		{
			[CompilerGenerated]
			get
			{
				return this._GenFormTextBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = delegate
				{
					this.FormatChange_Listener();
				};
				TextBox genFormTextBox = this._GenFormTextBox;
				if (genFormTextBox != null)
				{
					genFormTextBox.ModifiedChanged -= value2;
				}
				this._GenFormTextBox = value;
				genFormTextBox = this._GenFormTextBox;
				if (genFormTextBox != null)
				{
					genFormTextBox.ModifiedChanged += value2;
				}
			}
		}

		internal virtual Button ResetFormatButton
		{
			[CompilerGenerated]
			get
			{
				return this._ResetFormatButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ResetFormatButton_Click;
				Button resetFormatButton = this._ResetFormatButton;
				if (resetFormatButton != null)
				{
					resetFormatButton.Click -= value2;
				}
				this._ResetFormatButton = value;
				resetFormatButton = this._ResetFormatButton;
				if (resetFormatButton != null)
				{
					resetFormatButton.Click += value2;
				}
			}
		}

		internal virtual TextBox RoundFormatTBox
		{
			[CompilerGenerated]
			get
			{
				return this._RoundFormatTBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = delegate
				{
					this.FormatChange_Listener();
				};
				TextBox roundFormatTBox = this._RoundFormatTBox;
				if (roundFormatTBox != null)
				{
					roundFormatTBox.ModifiedChanged -= value2;
				}
				this._RoundFormatTBox = value;
				roundFormatTBox = this._RoundFormatTBox;
				if (roundFormatTBox != null)
				{
					roundFormatTBox.ModifiedChanged += value2;
				}
			}
		}

		internal virtual Label Label3
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.OKButton = new Button();
			this.FederalStateGroupBox = new GroupBox();
			this.ImputedIORadioButton = new RadioButton();
			this.FedStateLocalRadioButton = new RadioButton();
			this.TotGovRadioButton = new RadioButton();
			this.NumberFormatGroupBox = new GroupBox();
			this.ResetFormatButton = new Button();
			this.RoundFormatTBox = new TextBox();
			this.Label3 = new Label();
			this.CoeffFormatTBox = new TextBox();
			this.Label2 = new Label();
			this.GenFormTextBox = new TextBox();
			this.GenFormLabel = new Label();
			this.CopyOptionsGroupBox = new GroupBox();
			this.CopyTableHeadersCheckBox = new CheckBox();
			this.GroupBox1 = new GroupBox();
			this.EmploymentJobsButton = new RadioButton();
			this.EmploymentFTEButton = new RadioButton();
			this.CancelButtonOpt = new Button();
			this.Label1 = new Label();
			this.FederalStateGroupBox.SuspendLayout();
			this.NumberFormatGroupBox.SuspendLayout();
			this.CopyOptionsGroupBox.SuspendLayout();
			this.GroupBox1.SuspendLayout();
			base.SuspendLayout();
			this.OKButton.DialogResult = DialogResult.OK;
			this.OKButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.OKButton.Location = new Point(672, 382);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new Size(122, 37);
			this.OKButton.TabIndex = 0;
			this.OKButton.Text = "OK";
			this.OKButton.UseVisualStyleBackColor = true;
			this.FederalStateGroupBox.Controls.Add(this.ImputedIORadioButton);
			this.FederalStateGroupBox.Controls.Add(this.FedStateLocalRadioButton);
			this.FederalStateGroupBox.Controls.Add(this.TotGovRadioButton);
			this.FederalStateGroupBox.Location = new Point(34, 38);
			this.FederalStateGroupBox.Name = "FederalStateGroupBox";
			this.FederalStateGroupBox.Size = new Size(297, 173);
			this.FederalStateGroupBox.TabIndex = 1;
			this.FederalStateGroupBox.TabStop = false;
			this.FederalStateGroupBox.Text = "Federal and state aggregation level";
			this.ImputedIORadioButton.AutoSize = true;
			this.ImputedIORadioButton.BackColor = SystemColors.Control;
			this.ImputedIORadioButton.Location = new Point(18, 113);
			this.ImputedIORadioButton.Name = "ImputedIORadioButton";
			this.ImputedIORadioButton.Size = new Size(86, 18);
			this.ImputedIORadioButton.TabIndex = 2;
			this.ImputedIORadioButton.Text = "Imputed IO";
			this.ImputedIORadioButton.UseVisualStyleBackColor = false;
			this.FedStateLocalRadioButton.AutoSize = true;
			this.FedStateLocalRadioButton.Location = new Point(18, 79);
			this.FedStateLocalRadioButton.Name = "FedStateLocalRadioButton";
			this.FedStateLocalRadioButton.Size = new Size(159, 18);
			this.FedStateLocalRadioButton.TabIndex = 1;
			this.FedStateLocalRadioButton.Text = "Federal & State with Local";
			this.FedStateLocalRadioButton.UseVisualStyleBackColor = true;
			this.TotGovRadioButton.AutoSize = true;
			this.TotGovRadioButton.Location = new Point(18, 45);
			this.TotGovRadioButton.Name = "TotGovRadioButton";
			this.TotGovRadioButton.Size = new Size(121, 18);
			this.TotGovRadioButton.TabIndex = 0;
			this.TotGovRadioButton.Text = "Total Government";
			this.TotGovRadioButton.UseVisualStyleBackColor = true;
			this.NumberFormatGroupBox.Controls.Add(this.ResetFormatButton);
			this.NumberFormatGroupBox.Controls.Add(this.RoundFormatTBox);
			this.NumberFormatGroupBox.Controls.Add(this.Label3);
			this.NumberFormatGroupBox.Controls.Add(this.CoeffFormatTBox);
			this.NumberFormatGroupBox.Controls.Add(this.Label2);
			this.NumberFormatGroupBox.Controls.Add(this.GenFormTextBox);
			this.NumberFormatGroupBox.Controls.Add(this.GenFormLabel);
			this.NumberFormatGroupBox.Location = new Point(434, 12);
			this.NumberFormatGroupBox.Name = "NumberFormatGroupBox";
			this.NumberFormatGroupBox.Size = new Size(337, 239);
			this.NumberFormatGroupBox.TabIndex = 2;
			this.NumberFormatGroupBox.TabStop = false;
			this.NumberFormatGroupBox.Text = "Number format";
			this.ResetFormatButton.Font = new Font("Calibri", 12f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.ResetFormatButton.Location = new Point(134, 194);
			this.ResetFormatButton.Name = "ResetFormatButton";
			this.ResetFormatButton.Size = new Size(181, 39);
			this.ResetFormatButton.TabIndex = 6;
			this.ResetFormatButton.Text = "Reset";
			this.ResetFormatButton.UseVisualStyleBackColor = true;
			this.RoundFormatTBox.Location = new Point(134, 141);
			this.RoundFormatTBox.Name = "RoundFormatTBox";
			this.RoundFormatTBox.Size = new Size(181, 22);
			this.RoundFormatTBox.TabIndex = 5;
			this.Label3.AutoSize = true;
			this.Label3.Location = new Point(20, 144);
			this.Label3.Name = "Label3";
			this.Label3.Size = new Size(36, 14);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "Fixed";
			this.CoeffFormatTBox.Location = new Point(134, 87);
			this.CoeffFormatTBox.Name = "CoeffFormatTBox";
			this.CoeffFormatTBox.Size = new Size(181, 22);
			this.CoeffFormatTBox.TabIndex = 3;
			this.Label2.AutoSize = true;
			this.Label2.Location = new Point(20, 90);
			this.Label2.Name = "Label2";
			this.Label2.Size = new Size(70, 14);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "Coefficients";
			this.GenFormTextBox.Location = new Point(134, 34);
			this.GenFormTextBox.Name = "GenFormTextBox";
			this.GenFormTextBox.Size = new Size(181, 22);
			this.GenFormTextBox.TabIndex = 1;
			this.GenFormLabel.AccessibleDescription = "";
			this.GenFormLabel.AutoSize = true;
			this.GenFormLabel.Location = new Point(20, 37);
			this.GenFormLabel.Name = "GenFormLabel";
			this.GenFormLabel.Size = new Size(51, 14);
			this.GenFormLabel.TabIndex = 0;
			this.GenFormLabel.Text = "General";
			this.CopyOptionsGroupBox.Controls.Add(this.CopyTableHeadersCheckBox);
			this.CopyOptionsGroupBox.Location = new Point(434, 257);
			this.CopyOptionsGroupBox.Name = "CopyOptionsGroupBox";
			this.CopyOptionsGroupBox.Size = new Size(227, 110);
			this.CopyOptionsGroupBox.TabIndex = 3;
			this.CopyOptionsGroupBox.TabStop = false;
			this.CopyOptionsGroupBox.Text = "Data copy option";
			this.CopyTableHeadersCheckBox.AutoSize = true;
			this.CopyTableHeadersCheckBox.Checked = true;
			this.CopyTableHeadersCheckBox.CheckState = CheckState.Checked;
			this.CopyTableHeadersCheckBox.Location = new Point(24, 45);
			this.CopyTableHeadersCheckBox.Name = "CopyTableHeadersCheckBox";
			this.CopyTableHeadersCheckBox.Size = new Size(133, 18);
			this.CopyTableHeadersCheckBox.TabIndex = 0;
			this.CopyTableHeadersCheckBox.Text = "Copy Table Headers";
			this.CopyTableHeadersCheckBox.UseVisualStyleBackColor = true;
			this.GroupBox1.Controls.Add(this.EmploymentJobsButton);
			this.GroupBox1.Controls.Add(this.EmploymentFTEButton);
			this.GroupBox1.Location = new Point(34, 257);
			this.GroupBox1.Name = "GroupBox1";
			this.GroupBox1.Size = new Size(331, 162);
			this.GroupBox1.TabIndex = 4;
			this.GroupBox1.TabStop = false;
			this.GroupBox1.Text = "Employment units";
			this.EmploymentJobsButton.AutoSize = true;
			this.EmploymentJobsButton.Location = new Point(25, 97);
			this.EmploymentJobsButton.Name = "EmploymentJobsButton";
			this.EmploymentJobsButton.Size = new Size(229, 18);
			this.EmploymentJobsButton.TabIndex = 1;
			this.EmploymentJobsButton.TabStop = true;
			this.EmploymentJobsButton.Text = "Part- and Full- Time Employees (Jobs)";
			this.EmploymentJobsButton.UseVisualStyleBackColor = true;
			this.EmploymentFTEButton.AutoSize = true;
			this.EmploymentFTEButton.Checked = true;
			this.EmploymentFTEButton.Location = new Point(25, 45);
			this.EmploymentFTEButton.Name = "EmploymentFTEButton";
			this.EmploymentFTEButton.Size = new Size(167, 18);
			this.EmploymentFTEButton.TabIndex = 0;
			this.EmploymentFTEButton.TabStop = true;
			this.EmploymentFTEButton.Text = "Full-Time Equivalent (FTE)";
			this.EmploymentFTEButton.UseVisualStyleBackColor = true;
			this.CancelButtonOpt.DialogResult = DialogResult.Cancel;
			this.CancelButtonOpt.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.CancelButtonOpt.Location = new Point(543, 382);
			this.CancelButtonOpt.Name = "CancelButtonOpt";
			this.CancelButtonOpt.Size = new Size(114, 37);
			this.CancelButtonOpt.TabIndex = 5;
			this.CancelButtonOpt.Text = "Cancel";
			this.CancelButtonOpt.UseVisualStyleBackColor = true;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Calibri", 8f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label1.ForeColor = Color.Red;
			this.Label1.Location = new Point(30, 448);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(510, 13);
			this.Label1.TabIndex = 6;
			this.Label1.Text = "Change in Federal, State aggregation level and/or employment units may require recalculation of some tables.";
			base.AutoScaleDimensions = new SizeF(6f, 14f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(823, 493);
			base.Controls.Add(this.Label1);
			base.Controls.Add(this.CancelButtonOpt);
			base.Controls.Add(this.GroupBox1);
			base.Controls.Add(this.CopyOptionsGroupBox);
			base.Controls.Add(this.NumberFormatGroupBox);
			base.Controls.Add(this.FederalStateGroupBox);
			base.Controls.Add(this.OKButton);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.ForeColor = SystemColors.HotTrack;
			base.Name = "ProgramOptionsForm";
			this.Text = "Options";
			this.FederalStateGroupBox.ResumeLayout(false);
			this.FederalStateGroupBox.PerformLayout();
			this.NumberFormatGroupBox.ResumeLayout(false);
			this.NumberFormatGroupBox.PerformLayout();
			this.CopyOptionsGroupBox.ResumeLayout(false);
			this.CopyOptionsGroupBox.PerformLayout();
			this.GroupBox1.ResumeLayout(false);
			this.GroupBox1.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public ProgramOptionsForm(ref BEAData b, bool cps)
		{
			base.Load += this.ProgramOptionsForm_Load;
			this.EmploymentChange = false;
			this.TableHeaderChange = false;
			this.isFormatChanged = false;
			this.isGovAggLevelChanged = false;
			this.EmpStatus = new GenDataStatus();
			this.copyHeaderStatus = false;
			this.initGovAggLevel = "";
			this.copyHeaderStatus = cps;
			this.bdat = b;
			this.InitializeComponent();
			this.initGovAggLevel = Conversions.ToString((int)this.bdat.GovernmentAggStatus.getAggLevel());
			switch (this.bdat.GovernmentAggStatus.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				this.FedStateLocalRadioButton.Select();
				break;
			case GenDataStatus.dataStatusType.ImputedGov:
				this.ImputedIORadioButton.Select();
				break;
			case GenDataStatus.dataStatusType.TotalGovernment:
				this.TotGovRadioButton.Select();
				break;
			default:
				Interaction.MsgBox("ERROR 504", MsgBoxStyle.OkOnly, null);
				break;
			}
			string employmentStatus = this.bdat.worksum.EmploymentStatus;
			if (Operators.CompareString(employmentStatus, "FTE", false) != 0)
			{
				if (Operators.CompareString(employmentStatus, "Jobs", false) == 0)
				{
					this.EmploymentJobsButton.Select();
				}
				else
				{
					Interaction.MsgBox("ERROR 505", MsgBoxStyle.OkOnly, null);
				}
			}
			else
			{
				this.EmploymentFTEButton.Select();
			}
			if (this.copyHeaderStatus)
			{
				this.CopyTableHeadersCheckBox.Checked = true;
			}
			else
			{
				this.CopyTableHeadersCheckBox.Checked = false;
			}
			this.options = new ViewOptions();
			this.GenFormTextBox.Text = this.bdat.FormatGeneral;
			this.CoeffFormatTBox.Text = this.bdat.FormatCoefficients;
			this.RoundFormatTBox.Text = this.bdat.FormatRounded;
		}

		private void ImputedIORadioButton_CheckedChanged(object sender, EventArgs e)
		{
		}

		private void FedStateLocalRadioButton_CheckedChanged(object sender, EventArgs e)
		{
		}

		private void TotGovRadioButton_CheckedChanged(object sender, EventArgs e)
		{
		}

		private void EmploymentFTE_CheckedChanged(object sender, EventArgs e)
		{
			if ((Operators.CompareString(this.bdat.worksum.EmploymentStatus, "FTE", false) == 0 & !this.EmploymentFTEButton.Checked) && MsgBoxResult.Yes != Interaction.MsgBox("Changing employment units will reset significant part of the data\r\nClick no to save the data and model", MsgBoxStyle.YesNo, "Continue with Calculations?"))
			{
				this.EmploymentFTEButton.Select();
			}
		}

		private void EmploymentJOBS_CheckedChanged(object sender, EventArgs e)
		{
			if ((Operators.CompareString(this.bdat.worksum.EmploymentStatus, "Jobs", false) == 0 & !this.EmploymentJobsButton.Checked) && MsgBoxResult.Yes != Interaction.MsgBox("Changing employment units will reset significant part of the data\r\nClick no to save the data and model", MsgBoxStyle.YesNo, "Continue with Calculations?"))
			{
				this.EmploymentJobsButton.Select();
			}
		}

		private void OKButton_Click(object sender, EventArgs e)
		{
			int num = this.bdat.StateIDs.StateCodes.Length;
			if (this.FedStateLocalRadioButton.Checked)
			{
				this.bdat.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.FedStateLocal);
			}
			if (this.TotGovRadioButton.Checked)
			{
				this.bdat.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.TotalGovernment);
			}
			if (this.ImputedIORadioButton.Checked)
			{
				this.bdat.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			}
			if (!this.initGovAggLevel.Contains(Conversions.ToString((int)this.bdat.GovernmentAggStatus.getAggLevel())))
			{
				this.isGovAggLevelChanged = true;
				this.bdat.isRequirementTablesCalculated = false;
			}
			GenDataStatus genDataStatus = new GenDataStatus();
			if (this.EmploymentFTEButton.Checked & Operators.CompareString(this.bdat.worksum.EmploymentStatus, "FTE", false) != 0)
			{
				this.EmploymentChange = true;
				this.bdat.worksum.EmploymentStatus = "FTE";
				genDataStatus.setFTE_TYPE(GenDataStatus.dataEmpStatus.FTE_BASED);
			}
			if (this.EmploymentJobsButton.Checked & Operators.CompareString(this.bdat.worksum.EmploymentStatus, "Jobs", false) != 0)
			{
				this.EmploymentChange = true;
				this.bdat.worksum.EmploymentStatus = "Jobs";
				genDataStatus.setFTE_TYPE(GenDataStatus.dataEmpStatus.JOB_BASED);
			}
			if (this.CopyTableHeadersCheckBox.CheckState == CheckState.Checked & !this.copyHeaderStatus)
			{
				this.TableHeaderChange = true;
				this.copyHeaderStatus = true;
			}
			if (this.CopyTableHeadersCheckBox.CheckState == CheckState.Unchecked & this.copyHeaderStatus)
			{
				this.TableHeaderChange = true;
				this.copyHeaderStatus = false;
			}
			string text = this.GenFormTextBox.Text;
			string text2 = this.CoeffFormatTBox.Text;
			string text3 = this.RoundFormatTBox.Text;
			this.bdat.FormatGeneral = text;
			this.bdat.FormatCoefficients = text2;
			this.bdat.FormatRounded = text3;
		}

		public bool getCopyHeaderStatus()
		{
			return this.copyHeaderStatus;
		}

		private void ProgramOptionsForm_Load(object sender, EventArgs e)
		{
		}

		private void ResetFormatButton_Click(object sender, EventArgs e)
		{
			this.GenFormTextBox.Text = "#,##0.00";
			this.CoeffFormatTBox.Text = "#,##0.000";
			this.RoundFormatTBox.Text = "#,##0";
			this.isFormatChanged = true;
		}

		private void FormatChange_Listener()
		{
			this.isFormatChanged = true;
		}

		[CompilerGenerated]
		[DebuggerHidden]
		private void _Lambda_0024__R61_002D1(object a0, EventArgs a1)
		{
			this.FormatChange_Listener();
		}

		[CompilerGenerated]
		[DebuggerHidden]
		private void _Lambda_0024__R69_002D2(object a0, EventArgs a1)
		{
			this.FormatChange_Listener();
		}

		[CompilerGenerated]
		[DebuggerHidden]
		private void _Lambda_0024__R77_002D3(object a0, EventArgs a1)
		{
			this.FormatChange_Listener();
		}
	}
}
