using IOSNAP.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public sealed class frmAboutBox : Form
	{
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("TableLayoutPanel")]
		private TableLayoutPanel _TableLayoutPanel;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("LogoPictureBox")]
		private PictureBox _LogoPictureBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("LabelProductName")]
		private Label _LabelProductName;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("LabelVersion")]
		private Label _LabelVersion;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("LabelCompanyName")]
		private Label _LabelCompanyName;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("TextBoxDescription")]
		private TextBox _TextBoxDescription;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OKButton")]
		private Button _OKButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("LabelCopyright")]
		private Label _LabelCopyright;

		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Email")]
		private Label _Email;

		internal TableLayoutPanel TableLayoutPanel
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal PictureBox LogoPictureBox
		{
			[CompilerGenerated]
			get
			{
				return this._LogoPictureBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.LogoPictureBox_Click;
				PictureBox logoPictureBox = this._LogoPictureBox;
				if (logoPictureBox != null)
				{
					logoPictureBox.Click -= value2;
				}
				this._LogoPictureBox = value;
				logoPictureBox = this._LogoPictureBox;
				if (logoPictureBox != null)
				{
					logoPictureBox.Click += value2;
				}
			}
		}

		internal Label LabelProductName
		{
			[CompilerGenerated]
			get
			{
				return this._LabelProductName;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.LabelProductName_Click;
				Label labelProductName = this._LabelProductName;
				if (labelProductName != null)
				{
					labelProductName.Click -= value2;
				}
				this._LabelProductName = value;
				labelProductName = this._LabelProductName;
				if (labelProductName != null)
				{
					labelProductName.Click += value2;
				}
			}
		}

		internal Label LabelVersion
		{
			[CompilerGenerated]
			get
			{
				return this._LabelVersion;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.LabelVersion_Click;
				Label labelVersion = this._LabelVersion;
				if (labelVersion != null)
				{
					labelVersion.Click -= value2;
				}
				this._LabelVersion = value;
				labelVersion = this._LabelVersion;
				if (labelVersion != null)
				{
					labelVersion.Click += value2;
				}
			}
		}

		internal Label LabelCompanyName
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal TextBox TextBoxDescription
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal Button OKButton
		{
			[CompilerGenerated]
			get
			{
				return this._OKButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OKButton_Click;
				Button oKButton = this._OKButton;
				if (oKButton != null)
				{
					oKButton.Click -= value2;
				}
				this._OKButton = value;
				oKButton = this._OKButton;
				if (oKButton != null)
				{
					oKButton.Click += value2;
				}
			}
		}

		internal Label LabelCopyright
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal Label Email
		{
			[CompilerGenerated]
			get
			{
				return this._Email;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Email_Click;
				Label email = this._Email;
				if (email != null)
				{
					email.Click -= value2;
				}
				this._Email = value;
				email = this._Email;
				if (email != null)
				{
					email.Click += value2;
				}
			}
		}

		public frmAboutBox()
		{
			base.Load += this.frmAboutBox_Load;
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frmAboutBox));
			this.TableLayoutPanel = new TableLayoutPanel();
			this.LabelProductName = new Label();
			this.LabelVersion = new Label();
			this.LabelCopyright = new Label();
			this.LabelCompanyName = new Label();
			this.TextBoxDescription = new TextBox();
			this.LogoPictureBox = new PictureBox();
			this.OKButton = new Button();
			this.Email = new Label();
			this.TableLayoutPanel.SuspendLayout();
			((ISupportInitialize)this.LogoPictureBox).BeginInit();
			base.SuspendLayout();
			this.TableLayoutPanel.ColumnCount = 2;
			this.TableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33f));
			this.TableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 67f));
			this.TableLayoutPanel.Controls.Add(this.LabelProductName, 1, 0);
			this.TableLayoutPanel.Controls.Add(this.LabelVersion, 1, 2);
			this.TableLayoutPanel.Controls.Add(this.LabelCopyright, 1, 3);
			this.TableLayoutPanel.Controls.Add(this.LabelCompanyName, 1, 4);
			this.TableLayoutPanel.Controls.Add(this.TextBoxDescription, 1, 5);
			this.TableLayoutPanel.Controls.Add(this.LogoPictureBox, 0, 0);
			this.TableLayoutPanel.Controls.Add(this.OKButton, 1, 6);
			this.TableLayoutPanel.Controls.Add(this.Email, 1, 1);
			this.TableLayoutPanel.Dock = DockStyle.Fill;
			this.TableLayoutPanel.Location = new Point(9, 9);
			this.TableLayoutPanel.Name = "TableLayoutPanel";
			this.TableLayoutPanel.RowCount = 7;
			this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 9.280073f));
			this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 8.256881f));
			this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 8.816067f));
			this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 9.375926f));
			this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 9.008244f));
			this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 46.05235f));
			this.TableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 9.210468f));
			this.TableLayoutPanel.Size = new Size(481, 341);
			this.TableLayoutPanel.TabIndex = 0;
			this.LabelProductName.Cursor = Cursors.Hand;
			this.LabelProductName.Dock = DockStyle.Fill;
			this.LabelProductName.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.LabelProductName.Location = new Point(164, 0);
			this.LabelProductName.Margin = new Padding(6, 0, 3, 0);
			this.LabelProductName.MaximumSize = new Size(0, 17);
			this.LabelProductName.Name = "LabelProductName";
			this.LabelProductName.Size = new Size(314, 17);
			this.LabelProductName.TabIndex = 0;
			this.LabelProductName.Text = "IO-Snap (http://www.IO-Snap.com)";
			this.LabelProductName.TextAlign = ContentAlignment.MiddleLeft;
			this.LabelVersion.AutoEllipsis = true;
			this.LabelVersion.Dock = DockStyle.Fill;
			this.LabelVersion.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.LabelVersion.Location = new Point(164, 59);
			this.LabelVersion.Margin = new Padding(6, 0, 3, 0);
			this.LabelVersion.MaximumSize = new Size(0, 17);
			this.LabelVersion.Name = "LabelVersion";
			this.LabelVersion.Size = new Size(314, 17);
			this.LabelVersion.TabIndex = 0;
			this.LabelVersion.Text = "Version 1.0.0";
			this.LabelVersion.TextAlign = ContentAlignment.MiddleLeft;
			this.LabelCopyright.Dock = DockStyle.Fill;
			this.LabelCopyright.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.LabelCopyright.Location = new Point(164, 89);
			this.LabelCopyright.Margin = new Padding(6, 0, 3, 0);
			this.LabelCopyright.MaximumSize = new Size(0, 17);
			this.LabelCopyright.Name = "LabelCopyright";
			this.LabelCopyright.Size = new Size(314, 17);
			this.LabelCopyright.TabIndex = 0;
			this.LabelCopyright.Text = "April 2014";
			this.LabelCopyright.TextAlign = ContentAlignment.MiddleLeft;
			this.LabelCompanyName.Dock = DockStyle.Fill;
			this.LabelCompanyName.Font = new Font("Calibri", 11f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.LabelCompanyName.Location = new Point(164, 120);
			this.LabelCompanyName.Margin = new Padding(6, 0, 3, 0);
			this.LabelCompanyName.MaximumSize = new Size(0, 17);
			this.LabelCompanyName.Name = "LabelCompanyName";
			this.LabelCompanyName.Size = new Size(314, 17);
			this.LabelCompanyName.TabIndex = 0;
			this.LabelCompanyName.Text = "Copyright West Virginia University 2011";
			this.LabelCompanyName.TextAlign = ContentAlignment.MiddleLeft;
			this.TextBoxDescription.Dock = DockStyle.Fill;
			this.TextBoxDescription.Font = new Font("Calibri", 10f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.TextBoxDescription.Location = new Point(164, 153);
			this.TextBoxDescription.Margin = new Padding(6, 3, 3, 3);
			this.TextBoxDescription.Multiline = true;
			this.TextBoxDescription.Name = "TextBoxDescription";
			this.TextBoxDescription.ReadOnly = true;
			this.TextBoxDescription.ScrollBars = ScrollBars.Both;
			this.TextBoxDescription.Size = new Size(314, 151);
			this.TextBoxDescription.TabIndex = 0;
			this.TextBoxDescription.TabStop = false;
			this.TextBoxDescription.Text = componentResourceManager.GetString("TextBoxDescription.Text");
			this.LogoPictureBox.Image = (Image)componentResourceManager.GetObject("LogoPictureBox.Image");
			this.LogoPictureBox.Location = new Point(3, 3);
			this.LogoPictureBox.Name = "LogoPictureBox";
			this.TableLayoutPanel.SetRowSpan(this.LogoPictureBox, 7);
			this.LogoPictureBox.Size = new Size(152, 117);
			this.LogoPictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
			this.LogoPictureBox.TabIndex = 0;
			this.LogoPictureBox.TabStop = false;
			this.OKButton.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.OKButton.DialogResult = DialogResult.Cancel;
			this.OKButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.OKButton.Location = new Point(398, 310);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new Size(80, 28);
			this.OKButton.TabIndex = 0;
			this.OKButton.Text = "&OK";
			this.Email.AutoSize = true;
			this.Email.Cursor = Cursors.Hand;
			this.Email.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Email.Location = new Point(161, 31);
			this.Email.Name = "Email";
			this.Email.Size = new Size(176, 19);
			this.Email.TabIndex = 1;
			this.Email.Text = "E-mail: Info@io-snap.com";
			this.Email.TextAlign = ContentAlignment.MiddleLeft;
			base.AutoScaleDimensions = new SizeF(6f, 13f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.CancelButton = this.OKButton;
			base.ClientSize = new Size(499, 359);
			base.Controls.Add(this.TableLayoutPanel);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "frmAboutBox";
			base.Padding = new Padding(9);
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			this.Text = "frmAboutBox";
			this.TableLayoutPanel.ResumeLayout(false);
			this.TableLayoutPanel.PerformLayout();
			((ISupportInitialize)this.LogoPictureBox).EndInit();
			base.ResumeLayout(false);
		}

		private void frmAboutBox_Load(object sender, EventArgs e)
		{
			string arg = (Operators.CompareString(MyProject.Application.Info.Title, "", false) == 0) ? Path.GetFileNameWithoutExtension(MyProject.Application.Info.AssemblyName) : MyProject.Application.Info.Title;
			string text = "Developers: \r\n\tRandall Jackson\r\n\tChrista Court\r\n\r\nProgrammers:\r\n\tAdam Fedorowicz\r\n\tMarwan Alkhweldi\r\n\r\n=========================================\r\n\tIO-SNAP WARRANTY\r\n\r\n=========================================\r\n\r\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS, DEVELOPERS, AND DISTRIBUTORS'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. \r\n\r\nIN NO EVENT SHALL THE COPYRIGHT HOLDERS, DEVELOPERS, OR DISTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.";
			this.Text = string.Format("About {0}", arg);
			this.LabelProductName.Text = "IO-Snap (http://www.IO-Snap.com)";
			this.LabelVersion.Text = "Version " + MyProject.Application.Info.Version.ToString();
			Label labelCopyright = this.LabelCopyright;
			DateTime dateTime = DateTime.Today;
			string str = dateTime.ToString("MMMM");
			dateTime = DateTime.Now;
			labelCopyright.Text = str + " " + dateTime.Year.ToString();
			this.LabelCompanyName.Text = "Copyright " + Conversions.ToString(Strings.Chr(169)) + " 2011 West Virginia University";
			this.TextBoxDescription.Text = text;
		}

		private void OKButton_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void LabelProductName_Click(object sender, EventArgs e)
		{
			Process.Start("http://www.IO-Snap.com");
		}

		private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			Process.Start("http://www.IO-Snap.com");
		}

		private void LogoPictureBox_Click(object sender, EventArgs e)
		{
			Process.Start("http://www.IO-Snap.com");
		}

		private void Email_Click(object sender, EventArgs e)
		{
			Process.Start("mailto:Info@io-snap.com");
		}

		private void LabelVersion_Click(object sender, EventArgs e)
		{
		}
	}
}
