using IOSNAP.My;
using Microsoft.VisualBasic.CompilerServices;

namespace IOSNAP
{
	[StandardModule]
	public sealed class Beadata2String
	{
		private static BEAData bdat;

		public static string B2String(ref BEAData b)
		{
			string str = "";
			string text = "";
			Beadata2String.bdat = b;
			str += "<Version>\r\n";
			str += "<value 1.0.2012.1>\r\n";
			str += "</Version>\r\n";
			str += "<parameters>\r\n";
			str += "<n_industries>\r\n";
			str = str + "<value " + Conversions.ToString(b.N_INDUSTRIES) + ">\r\n";
			str += "<n_commodities>\r\n";
			str = str + "<value " + Conversions.ToString(b.N_COMMODITIES) + ">\r\n";
			str += "<n_base>\r\n";
			checked
			{
				object right = b.N_INDUSTRIES - 4;
				str = Conversions.ToString(Operators.AddObject(str, Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject("<value ", right), ">"), "\r\n")));
				str += "<data_type>\r\n";
				str = str + "<value " + b.worksum.DataType.ToString().ToLower() + ">\r\n";
				str += "</parameters>\r\n";
				str += "<headers>\r\n";
				str += "<hdrFinDem>\r\n";
				int num = b.hdrFinDem.Length - 1;
				for (int i = 0; i <= num; i++)
				{
					str = str + b.hdrFinDem[i] + "\r\n";
				}
				str += "</hdrFinDem>\r\n";
				str += "<hdrInd>\r\n";
				int num2 = Conversions.ToInteger(Operators.SubtractObject(NewLateBinding.LateGet(b.NTables.getIndustryHeadersImputed(), null, "Length", new object[0], null, null, null), 1));
				for (int j = 0; j <= num2; j++)
				{
					str = str + b.NTables.hdrInd[j] + "\r\n";
				}
				str += "</hdrInd>\r\n";
				str += "<IndClassification>\r\n";
				int num3 = b.IndBaseClass.Length - 1;
				for (int k = 0; k <= num3; k++)
				{
					str = str + b.IndBaseClass[k] + "\r\n";
				}
				str += "</IndClassification>\r\n";
				str += "<hdrIndAdjGov>\r\n";
				int num4 = b.NTables.hdrIndAdjGov.Length - 1;
				for (int l = 0; l <= num4; l++)
				{
					str = str + b.NTables.hdrIndAdjGov[l] + "\r\n";
				}
				str += "</hdrIndAdjGov>\r\n";
				str += "<IndAdjGovClass>\r\n";
				int num5 = b.IndAdjGovClass.Length - 1;
				for (int m = 0; m <= num5; m++)
				{
					str = str + b.IndAdjGovClass[m] + "\r\n";
				}
				str += "</IndAdjGovClass>\r\n";
				str += "<hdrIndFedStateLocGov>\r\n";
				int num6 = b.NTables.hdrIndFedStateLocGov.Length - 1;
				for (int n = 0; n <= num6; n++)
				{
					str = str + b.NTables.hdrIndFedStateLocGov[n] + "\r\n";
				}
				str += "</hdrIndFedStateLocGov>\r\n";
				str += "<IndFedStateLocGovClass>\r\n";
				int num7 = b.IndFedStateLocGovClass.Length - 1;
				for (int num8 = 0; num8 <= num7; num8++)
				{
					str = str + b.IndFedStateLocGovClass[num8] + "\r\n";
				}
				str += "</IndFedStateLocGovClass>\r\n";
				str += "<hdrIndTotGov>\r\n";
				int num9 = b.NTables.hdrIndTotGov.Length - 1;
				for (int num10 = 0; num10 <= num9; num10++)
				{
					str = str + b.NTables.hdrIndTotGov[num10] + "\r\n";
				}
				str += "</hdrIndTotGov>\r\n";
				str += "<IndTotGovClass>\r\n";
				int num11 = b.IndTotGovClass.Length - 1;
				for (int num12 = 0; num12 <= num11; num12++)
				{
					str = str + b.IndTotGovClass[num12] + "\r\n";
				}
				str += "</IndTotGovClass>\r\n";
				str += "<hdrComm>\r\n";
				int num13 = b.NTables.getCommHeadersImputed().Length - 7;
				for (int num14 = 0; num14 <= num13; num14++)
				{
					str = str + b.NTables.getCommHeadersImputed()[num14] + "\r\n";
				}
				str += "</hdrComm>\r\n";
				str += "<hdrCommAdjGov>\r\n";
				int num15 = b.NTables.getCommHeadersImputed().Length - 6;
				int num16 = b.NTables.getCommHeadersImputed().Length - 3;
				for (int num17 = num15; num17 <= num16; num17++)
				{
					str = str + b.NTables.getCommHeadersImputed()[num17] + "\r\n";
				}
				str += "</hdrCommAdjGov>\r\n";
				str += "<hdrCommFedStateLocGov>\r\n";
				int num18 = b.NTables.getCommHeadersFedState().Length - 4;
				int num19 = b.NTables.getCommHeadersFedState().Length - 3;
				for (int num20 = num18; num20 <= num19; num20++)
				{
					str = str + b.NTables.getCommHeadersFedState()[num20] + "\r\n";
				}
				str += "</hdrCommFedStateLocGov>\r\n";
				str += "<hdrCommTotGov>\r\n";
				str = str + b.NTables.getCommHeadersTotal()[b.NTables.getCommHeadersTotal().Length - 3] + "\r\n";
				str += "</hdrCommTotGov>\r\n";
				str += "<hdrNoncompImp>\r\n";
				str = str + b.NTables.getCommHeadersImputed()[b.NTables.getCommHeadersImputed().Length - 1] + "\r\n";
				str += "</hdrNoncompImp>\r\n";
				str += "<hdrScrap>\r\n";
				str = str + b.NTables.getCommHeadersImputed()[b.NTables.getCommHeadersImputed().Length - 2] + "\r\n";
				str += "</hdrScrap>\r\n";
				str += "<hdrVA>\r\n";
				int num21 = b.hdrVA.Length - 1;
				for (int num22 = 0; num22 <= num21; num22++)
				{
					str = str + b.hdrVA[num22] + "\r\n";
				}
				str += "</hdrVA>\r\n";
				str += "</headers>\r\n";
				str += "<data>\r\n";
				str += "<year>\r\n";
				str = str + "<value " + b.worksum.datayear + ">\r\n";
				str += "<DataLabel>\r\n";
				str = str + b.worksum.DataLabel + "\r\n";
				str += "</DataLabel>\r\n";
				str += "<DataType>\r\n";
				str = str + Conversions.ToString(unchecked((int)b.worksum.DataTypeCode)) + "\r\n";
				str += "</DataType>\r\n";
				str += "<TimeStamp>\r\n";
				str = Conversions.ToString(Operators.AddObject(str, Operators.ConcatenateObject(b.worksum.DataTimeStamp, "\r\n")));
				str += "</TimeStamp>\r\n";
				str += "<IsRegionalized>\r\n";
				str = str + b.worksum.dataregionalized + "\r\n";
				str += "</IsRegionalized>\r\n";
				str += "<RegionalizationMethod>\r\n";
				str = str + b.worksum.regmethod + "\r\n";
				str += "</RegionalizationMethod>\r\n";
				str += "<Region>\r\n";
				str = str + b.worksum.regby + "\r\n";
				str += "</Region>\r\n";
				str += "<national>\r\n";
				str += "<tableVA>\r\n";
				str += Beadata2String.getVA();
				str += "</tableVA>\r\n";
				str += "<tableUse>\r\n";
				str += Beadata2String.getUSE();
				str += "</tableUse>\r\n";
				str += "<tableMake>\r\n";
				str += Beadata2String.getMAKE();
				str += "</tableMake>\r\n";
				str += "<tableFinDem>\r\n";
				str += Beadata2String.getFinDem();
				str += "</tableFinDem>\r\n";
				str += "<tableImp>\r\n";
				str += Beadata2String.getImports();
				str += "</tableImp>\r\n";
				str += "<tableEmployment>\r\n";
				str += Beadata2String.getEmployment();
				str += "</tableEmployment>\r\n";
				str += "<tableFTE_Ratio>\r\n";
				str += Beadata2String.getFTERatio();
				str += "</tableFTE_Ratio>\r\n";
				str += "<tablePCE>\r\n";
				str += Beadata2String.getPCE();
				str += "</tablePCE>\r\n";
				str += "<tablePersonalIncome>\r\n";
				str = str + Conversions.ToString(Beadata2String.getPersonalIncome()) + "\r\n";
				str += "</tablePersonalIncome>\r\n";
				str += "<tableDisposableIncome>\r\n";
				str = str + Conversions.ToString(Beadata2String.getDisposableIncome()) + "\r\n";
				str += "</tableDisposableIncome>\r\n";
				str += "<tableTotFedExp_2005>\r\n";
				str = str + Conversions.ToString(Beadata2String.bdat.TotFedExp_2005) + "\r\n";
				str += "</tableTotFedExp_2005>\r\n";
				str += "<tableTotSLExp_2008>\r\n";
				str = str + Conversions.ToString(Beadata2String.bdat.TotSLExp_2008) + "\r\n";
				str += "</tableTotSLExp_2008>\r\n";
				str += "</national>\r\n";
				str += "<StateData>\r\n";
				int numberOfStates = Beadata2String.bdat.RegStateData.getNumberOfStates();
				str = str + Conversions.ToString(numberOfStates) + "\r\n";
				string[] states = Beadata2String.bdat.RegStateData.States;
				for (int num23 = 0; num23 < states.Length; num23++)
				{
					string str2 = states[num23];
					str += "<State>\r\n";
					str = str + "<value " + str2 + ">\r\n";
					string str3 = str;
					StateData state = Beadata2String.bdat.RegStateData.GetState(ref str2);
					str = str3 + Beadata2String.getState(ref state);
					str += "</State>\r\n";
				}
				str += "</StateData>\r\n";
				str += "</year>\r\n";
				return str + "</data>\r\n";
			}
		}

		private static double getDisposableIncome()
		{
			double personalIncome = Beadata2String.bdat.PersonalIncome;
			if (Beadata2String.bdat.worksum.dataregionalized.Contains("Yes"))
			{
				return MyProject.Forms.frmMain.MeDisposableIncomeM[MyProject.Forms.frmMain.YearSelect_CBoxTS.SelectedIndex];
			}
			return Beadata2String.bdat.DisposableIncome;
		}

		private static double getPersonalIncome()
		{
			double personalIncome = Beadata2String.bdat.PersonalIncome;
			if (Beadata2String.bdat.worksum.dataregionalized.Contains("Yes"))
			{
				return MyProject.Forms.frmMain.MePersonalIncomeM[MyProject.Forms.frmMain.YearSelect_CBoxTS.SelectedIndex];
			}
			return Beadata2String.bdat.PersonalIncome;
		}

		private static string getVA()
		{
			string text = "";
			checked
			{
				int num = Beadata2String.bdat.NTables.VA.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					string text2 = "";
					int num2 = Beadata2String.bdat.NTables.VA.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						text2 = text2 + Conversions.ToString(Beadata2String.bdat.NTables.VA[i, j]) + "\t";
					}
					text = text + text2.Trim() + "\r\n";
				}
				return text;
			}
		}

		private static string getUSE()
		{
			string text = "";
			checked
			{
				int num = Beadata2String.bdat.Use.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					string text2 = "";
					int num2 = Beadata2String.bdat.Use.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						text2 = text2 + Conversions.ToString(Beadata2String.bdat.Use[i, j]) + "\t";
					}
					text = text + text2.Trim() + "\r\n";
				}
				return text;
			}
		}

		private static string getMAKE()
		{
			string text = "";
			checked
			{
				int num = Beadata2String.bdat.NTables.Make().NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					string text2 = "";
					int num2 = Beadata2String.bdat.NTables.Make().NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						text2 = text2 + Conversions.ToString(Beadata2String.bdat.NTables.Make()[i, j]) + "\t";
					}
					text = text + text2.Trim() + "\r\n";
				}
				return text;
			}
		}

		private static string getFinDem()
		{
			string text = "";
			checked
			{
				int num = Beadata2String.bdat.FinDem.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					string text2 = "";
					int num2 = Beadata2String.bdat.FinDem.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						text2 = text2 + Conversions.ToString(Beadata2String.bdat.get_FinDemVector(i, j)) + "\t";
					}
					text = text + text2.Trim() + "\r\n";
				}
				return text;
			}
		}

		private static string getImports()
		{
			string str = "";
			string text = "";
			checked
			{
				int num = Beadata2String.bdat.Import.NoCols - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(Beadata2String.bdat.Import[0, i]) + "\t";
				}
				return str + text.Trim() + "\r\n";
			}
		}

		private static string getPCE()
		{
			string str = "";
			string text = "";
			checked
			{
				int num = Beadata2String.bdat.NTables.PCE.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(Beadata2String.bdat.NTables.PCE[i]) + "\t";
				}
				return str + text.Trim() + "\r\n";
			}
		}

		private static string getEmployment()
		{
			string str = "";
			string text = "";
			checked
			{
				if (Beadata2String.bdat.worksum.dataregionalized.Contains("Yes"))
				{
					int num = Beadata2String.bdat.NTables.RegEmployment_JOBS.Count - 1;
					for (int i = 0; i <= num; i++)
					{
						text = text + Conversions.ToString(Beadata2String.bdat.NTables.RegEmployment_JOBS[i]) + "\t";
					}
				}
				else
				{
					int num2 = Beadata2String.bdat.NTables.Employment_JOBS.Count - 1;
					for (int j = 0; j <= num2; j++)
					{
						text = text + Conversions.ToString(Beadata2String.bdat.NTables.Employment_JOBS[j]) + "\t";
					}
				}
				return str + text.Trim() + "\r\n";
			}
		}

		private static string getFTERatio()
		{
			string str = "";
			string text = "";
			checked
			{
				int num = Beadata2String.bdat.NTables.FTE_Ratio.jobs2fte_ratio_imputed.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(Beadata2String.bdat.NTables.FTE_Ratio.jobs2fte_ratio_imputed[i]) + "\t";
				}
				return str + text.Trim() + "\r\n";
			}
		}

		private static string getState(ref StateData sd)
		{
			string str = "";
			str += "<statePersonalIncome>\r\n";
			str = str + Conversions.ToString(sd.PersonalIncome) + "\r\n";
			str += "</statePersonalIncome>\r\n";
			str += "<stateTotFedExp>\r\n";
			str = str + Conversions.ToString(sd.TotFedExp) + "\r\n";
			str += "</stateTotFedExp>\r\n";
			str += "<stateTotSLEExp>\r\n";
			str = str + Conversions.ToString(sd.TotSLEExp) + "\r\n";
			str += "</stateTotSLEExp>\r\n";
			str += "<stateDisposableIncome>\r\n";
			str = str + Conversions.ToString(sd.DisposableIncome) + "\r\n";
			str += "</stateDisposableIncome>\r\n";
			str += "<stateEmploymentBase>\r\n";
			str += Beadata2String.getStateEmploymentBase(ref sd);
			str += "</stateEmploymentBase>\r\n";
			str += "<stateEmploymentFedStateGov>\r\n";
			str += Beadata2String.getStateEmploymentFedStateGov(ref sd);
			str += "</stateEmploymentFedStateGov>\r\n";
			str += "<stateEmploymentImputedGov>\r\n";
			str += Beadata2String.getStateEmploymentImputedGov(ref sd);
			str += "</stateEmploymentImputedGov>\r\n";
			str += "<stateEmploymentTotalGov>\r\n";
			str += Beadata2String.getStateEmploymentTotalGov(ref sd);
			str += "</stateEmploymentTotalGov>\r\n";
			str += "<stateECBase>\r\n";
			str += Beadata2String.getStateECBase(ref sd);
			str += "</stateECBase>\r\n";
			str += "<stateECFedStateGov>\r\n";
			str += Beadata2String.getStateECFedStateGov(ref sd);
			str += "</stateECFedStateGov>\r\n";
			str += "<stateECImputedGov>\r\n";
			str += Beadata2String.getStateECImputedGov(ref sd);
			str += "</stateECImputedGov>\r\n";
			str += "<stateECTotalGov>\r\n";
			str += Beadata2String.getStateECTotalGov(ref sd);
			str += "</stateECTotalGov>\r\n";
			str += "<stateTBase>\r\n";
			str += Beadata2String.getStateTBase(ref sd);
			str += "</stateTBase>\r\n";
			str += "<stateTFedStateGov>\r\n";
			str += Beadata2String.getStateTFedStateGov(ref sd);
			str += "</stateTFedStateGov>\r\n";
			str += "<stateTImputedGov>\r\n";
			str += Beadata2String.getStateTImputedGov(ref sd);
			str += "</stateTImputedGov>\r\n";
			str += "<stateTTotalGov>\r\n";
			str += Beadata2String.getStateTTotalGov(ref sd);
			str += "</stateTTotalGov>\r\n";
			str += "<stateGOSBase>\r\n";
			str += Beadata2String.getStateGOSBase(ref sd);
			str += "</stateGOSBase>\r\n";
			str += "<stateGOSFedStateGov>\r\n";
			str += Beadata2String.getStateGOSFedStateGov(ref sd);
			str += "</stateGOSFedStateGov>\r\n";
			str += "<stateGOSImputedGov>\r\n";
			str += Beadata2String.getStateGOSImputedGov(ref sd);
			str += "</stateGOSImputedGov>\r\n";
			str += "<stateGOSTotalGov>\r\n";
			str += Beadata2String.getStateGOSTotalGov(ref sd);
			str += "</stateGOSTotalGov>\r\n";
			str += "<stateGDPBase>\r\n";
			str += Beadata2String.getStateGDPBase(ref sd);
			str += "</stateGDPBase>\r\n";
			str += "<stateGDPFedStateGov>\r\n";
			str += Beadata2String.getStateGDPFedStateGov(ref sd);
			str += "</stateGDPFedStateGov>\r\n";
			str += "<stateGDPImputedGov>\r\n";
			str += Beadata2String.getStateGDPImputedGov(ref sd);
			str += "</stateGDPImputedGov>\r\n";
			str += "<stateGDPTotalGov>\r\n";
			str += Beadata2String.getStateGDPTotalGov(ref sd);
			str += "</stateGDPTotalGov>\r\n";
			str += "<statePCE>\r\n";
			str += Beadata2String.getStatePCE(ref sd);
			return str + "</statePCE>\r\n";
		}

		private static string getStatePCE(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.PCE_Table.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.PCE_Table[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateEmploymentBase(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.Employment_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.Employment_base[i]) + "\t" + Conversions.ToString(sd2.Employment_base_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateEmploymentFedStateGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.Employment_FedState_Gov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.Employment_FedState_Gov[i]) + "\t" + Conversions.ToString(sd2.Employment_FedState_Gov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateEmploymentImputedGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.Employment_ImpGov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.Employment_ImpGov[i]) + "\t" + Conversions.ToString(sd2.Employment_ImpGov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateEmploymentTotalGov(ref StateData sd2)
		{
			string text = "";
			return text + Conversions.ToString(sd2.Employment_TotalGov[0]) + "\t" + Conversions.ToString(sd2.Employment_TotalGov_flag[0]) + "\r\n";
		}

		private static string getStateECBase(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.EC_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.EC_base[i]) + "\t" + Conversions.ToString(sd2.EC_base_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateECFedStateGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.EC_FedState_Gov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.EC_FedState_Gov[i]) + "\t" + Conversions.ToString(sd2.EC_FedState_Gov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateECImputedGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.EC_ImpGov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.EC_ImpGov[i]) + "\t" + Conversions.ToString(sd2.EC_ImpGov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateECTotalGov(ref StateData sd2)
		{
			string text = "";
			return text + Conversions.ToString(sd2.EC_TotalGov[0]) + "\t" + Conversions.ToString(sd2.EC_TotalGov_flag[0]) + "\r\n";
		}

		private static string getStateTBase(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.T_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.T_base[i]) + "\t" + Conversions.ToString(sd2.T_base_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateTFedStateGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.T_FedState_Gov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.T_FedState_Gov[i]) + "\t" + Conversions.ToString(sd2.T_FedState_Gov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateTImputedGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.T_ImpGov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.T_ImpGov[i]) + "\t" + Conversions.ToString(sd2.T_ImpGov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateTTotalGov(ref StateData sd2)
		{
			string text = "";
			return text + Conversions.ToString(sd2.T_TotalGov[0]) + "\t" + Conversions.ToString(sd2.T_TotalGov_flag[0]) + "\r\n";
		}

		private static string getStateGOSBase(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.GOS_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.GOS_base[i]) + "\t" + Conversions.ToString(sd2.GOS_base_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateGOSFedStateGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.GOS_FedState_Gov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.GOS_FedState_Gov[i]) + "\t" + Conversions.ToString(sd2.GOS_FedState_Gov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateGOSImputedGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.GOS_ImpGov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.GOS_ImpGov[i]) + "\t" + Conversions.ToString(sd2.GOS_ImpGov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateGOSTotalGov(ref StateData sd2)
		{
			string text = "";
			return text + Conversions.ToString(sd2.GOS_TotalGov[0]) + "\t" + Conversions.ToString(sd2.GOS_TotalGov_flag[0]) + "\r\n";
		}

		private static string getStateGDPBase(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.GDP_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.GDP_base[i]) + "\t" + Conversions.ToString(sd2.GDP_base_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateGDPFedStateGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.GDP_FedState_Gov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.GDP_FedState_Gov[i]) + "\t" + Conversions.ToString(sd2.GDP_FedState_Gov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateGDPImputedGov(ref StateData sd2)
		{
			string text = "";
			checked
			{
				int num = sd2.GDP_ImpGov.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					text = text + Conversions.ToString(sd2.GDP_ImpGov[i]) + "\t" + Conversions.ToString(sd2.GDP_ImpGov_flag[i]) + "\r\n";
				}
				return text;
			}
		}

		private static string getStateGDPTotalGov(ref StateData sd2)
		{
			string text = "";
			return text + Conversions.ToString(sd2.GDP_TotalGov[0]) + "\t" + Conversions.ToString(sd2.GDP_TotalGov_flag[0]) + "\r\n";
		}
	}
}
