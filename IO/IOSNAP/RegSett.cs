using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class RegSett : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cmdOk")]
		private Button _cmdOk;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cmdCancel")]
		private Button _cmdCancel;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RegionGroupBox")]
		private GroupBox _RegionGroupBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RegionComboBox")]
		private ComboBox _RegionComboBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox2")]
		private GroupBox _GroupBox2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("buttonSelectRegion")]
		private Button _buttonSelectRegion;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("comboBoxStateSelection")]
		private ComboBox _comboBoxStateSelection;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbSelRegion")]
		private RadioButton _rbSelRegion;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbStateSelection")]
		private RadioButton _rbStateSelection;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbUserDefined")]
		private RadioButton _rbUserDefined;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbUserDefMain")]
		private RadioButton _rbUserDefMain;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbSelRegMain")]
		private RadioButton _rbSelRegMain;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("tboxRegionName")]
		private TextBox _tboxRegionName;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ToolTip1")]
		private ToolTip _ToolTip1;

		public const int DIGITS = 7;

		public BEAData bdat;

		public SummaryDat worksum;

		private string PSelect;

		private string UserSelectedRegion;

		private string multiStateRegion;

		public StateData regiondat;

		private bool isStateRegionCommitted;

		private bool isRegionCommitted;

		private object isStateFirstTime;

		private Vector Regionalizer;

		private Vector EC;

		private Vector GOS;

		private Vector T;

		private Vector Employment;

		internal virtual Button cmdOk
		{
			[CompilerGenerated]
			get
			{
				return this._cmdOk;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.cmdOk_Click;
				Button cmdOk = this._cmdOk;
				if (cmdOk != null)
				{
					cmdOk.Click -= value2;
				}
				this._cmdOk = value;
				cmdOk = this._cmdOk;
				if (cmdOk != null)
				{
					cmdOk.Click += value2;
				}
			}
		}

		internal virtual Button cmdCancel
		{
			[CompilerGenerated]
			get
			{
				return this._cmdCancel;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.cmdCancel_Click;
				Button cmdCancel = this._cmdCancel;
				if (cmdCancel != null)
				{
					cmdCancel.Click -= value2;
				}
				this._cmdCancel = value;
				cmdCancel = this._cmdCancel;
				if (cmdCancel != null)
				{
					cmdCancel.Click += value2;
				}
			}
		}

		internal virtual GroupBox RegionGroupBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ComboBox RegionComboBox
		{
			[CompilerGenerated]
			get
			{
				return this._RegionComboBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.RegionComboBox_DropDown;
				EventHandler value3 = this.RegionComboBox_SelectedIndexChanged;
				ComboBox regionComboBox = this._RegionComboBox;
				if (regionComboBox != null)
				{
					regionComboBox.DropDown -= value2;
					regionComboBox.SelectedIndexChanged -= value3;
				}
				this._RegionComboBox = value;
				regionComboBox = this._RegionComboBox;
				if (regionComboBox != null)
				{
					regionComboBox.DropDown += value2;
					regionComboBox.SelectedIndexChanged += value3;
				}
			}
		}

		internal virtual GroupBox GroupBox2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button buttonSelectRegion
		{
			[CompilerGenerated]
			get
			{
				return this._buttonSelectRegion;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.buttonSelectRegion_Click;
				Button buttonSelectRegion = this._buttonSelectRegion;
				if (buttonSelectRegion != null)
				{
					buttonSelectRegion.Click -= value2;
				}
				this._buttonSelectRegion = value;
				buttonSelectRegion = this._buttonSelectRegion;
				if (buttonSelectRegion != null)
				{
					buttonSelectRegion.Click += value2;
				}
			}
		}

		internal virtual ComboBox comboBoxStateSelection
		{
			[CompilerGenerated]
			get
			{
				return this._comboBoxStateSelection;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.comboBoxStateSelection_DropDown;
				EventHandler value3 = this.comboBoxStateSelection_SelectedIndexChanged;
				ComboBox comboBoxStateSelection = this._comboBoxStateSelection;
				if (comboBoxStateSelection != null)
				{
					comboBoxStateSelection.DropDown -= value2;
					comboBoxStateSelection.SelectedIndexChanged -= value3;
				}
				this._comboBoxStateSelection = value;
				comboBoxStateSelection = this._comboBoxStateSelection;
				if (comboBoxStateSelection != null)
				{
					comboBoxStateSelection.DropDown += value2;
					comboBoxStateSelection.SelectedIndexChanged += value3;
				}
			}
		}

		internal virtual RadioButton rbSelRegion
		{
			[CompilerGenerated]
			get
			{
				return this._rbSelRegion;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.rbSelRegion_CheckedChanged;
				RadioButton rbSelRegion = this._rbSelRegion;
				if (rbSelRegion != null)
				{
					rbSelRegion.CheckedChanged -= value2;
				}
				this._rbSelRegion = value;
				rbSelRegion = this._rbSelRegion;
				if (rbSelRegion != null)
				{
					rbSelRegion.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton rbStateSelection
		{
			[CompilerGenerated]
			get
			{
				return this._rbStateSelection;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.rbStateSelection_CheckedChanged;
				RadioButton rbStateSelection = this._rbStateSelection;
				if (rbStateSelection != null)
				{
					rbStateSelection.CheckedChanged -= value2;
				}
				this._rbStateSelection = value;
				rbStateSelection = this._rbStateSelection;
				if (rbStateSelection != null)
				{
					rbStateSelection.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton rbUserDefined
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rbUserDefMain
		{
			[CompilerGenerated]
			get
			{
				return this._rbUserDefMain;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.rbUDef_CheckedChanged;
				RadioButton rbUserDefMain = this._rbUserDefMain;
				if (rbUserDefMain != null)
				{
					rbUserDefMain.CheckedChanged -= value2;
				}
				this._rbUserDefMain = value;
				rbUserDefMain = this._rbUserDefMain;
				if (rbUserDefMain != null)
				{
					rbUserDefMain.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton rbSelRegMain
		{
			[CompilerGenerated]
			get
			{
				return this._rbSelRegMain;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.selRegMain_CheckedChanged;
				RadioButton rbSelRegMain = this._rbSelRegMain;
				if (rbSelRegMain != null)
				{
					rbSelRegMain.CheckedChanged -= value2;
				}
				this._rbSelRegMain = value;
				rbSelRegMain = this._rbSelRegMain;
				if (rbSelRegMain != null)
				{
					rbSelRegMain.CheckedChanged += value2;
				}
			}
		}

		internal virtual TextBox tboxRegionName
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label Label1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ToolTip ToolTip1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			this.cmdOk = new Button();
			this.cmdCancel = new Button();
			this.RegionGroupBox = new GroupBox();
			this.tboxRegionName = new TextBox();
			this.rbUserDefMain = new RadioButton();
			this.rbSelRegMain = new RadioButton();
			this.Label1 = new Label();
			this.GroupBox2 = new GroupBox();
			this.buttonSelectRegion = new Button();
			this.comboBoxStateSelection = new ComboBox();
			this.rbSelRegion = new RadioButton();
			this.rbStateSelection = new RadioButton();
			this.rbUserDefined = new RadioButton();
			this.RegionComboBox = new ComboBox();
			this.ToolTip1 = new ToolTip(this.components);
			this.RegionGroupBox.SuspendLayout();
			this.GroupBox2.SuspendLayout();
			base.SuspendLayout();
			this.cmdOk.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cmdOk.Location = new Point(551, 519);
			this.cmdOk.Margin = new Padding(4, 6, 4, 6);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Size = new Size(124, 39);
			this.cmdOk.TabIndex = 5;
			this.cmdOk.Text = "OK";
			this.cmdOk.UseVisualStyleBackColor = true;
			this.cmdCancel.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.cmdCancel.Location = new Point(349, 519);
			this.cmdCancel.Margin = new Padding(4, 6, 4, 6);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new Size(124, 39);
			this.cmdCancel.TabIndex = 6;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.UseVisualStyleBackColor = true;
			this.RegionGroupBox.Controls.Add(this.tboxRegionName);
			this.RegionGroupBox.Controls.Add(this.rbUserDefMain);
			this.RegionGroupBox.Controls.Add(this.rbSelRegMain);
			this.RegionGroupBox.Controls.Add(this.Label1);
			this.RegionGroupBox.Controls.Add(this.GroupBox2);
			this.RegionGroupBox.Controls.Add(this.RegionComboBox);
			this.RegionGroupBox.Font = new Font("Calibri", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.RegionGroupBox.Location = new Point(31, 29);
			this.RegionGroupBox.Name = "RegionGroupBox";
			this.RegionGroupBox.Size = new Size(644, 454);
			this.RegionGroupBox.TabIndex = 15;
			this.RegionGroupBox.TabStop = false;
			this.RegionGroupBox.Text = "Region ";
			this.tboxRegionName.Location = new Point(167, 409);
			this.tboxRegionName.Name = "tboxRegionName";
			this.tboxRegionName.Size = new Size(453, 32);
			this.tboxRegionName.TabIndex = 17;
			this.rbUserDefMain.AutoSize = true;
			this.rbUserDefMain.Font = new Font("Calibri", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.rbUserDefMain.Location = new Point(31, 132);
			this.rbUserDefMain.Name = "rbUserDefMain";
			this.rbUserDefMain.Size = new Size(138, 28);
			this.rbUserDefMain.TabIndex = 16;
			this.rbUserDefMain.Text = "User defined";
			this.rbUserDefMain.UseVisualStyleBackColor = true;
			this.rbSelRegMain.AutoSize = true;
			this.rbSelRegMain.Checked = true;
			this.rbSelRegMain.Font = new Font("Calibri", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.rbSelRegMain.Location = new Point(31, 32);
			this.rbSelRegMain.Name = "rbSelRegMain";
			this.rbSelRegMain.Size = new Size(138, 28);
			this.rbSelRegMain.TabIndex = 15;
			this.rbSelRegMain.TabStop = true;
			this.rbSelRegMain.Text = "Select region";
			this.rbSelRegMain.UseVisualStyleBackColor = true;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Calibri", 12f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label1.Location = new Point(7, 408);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(146, 29);
			this.Label1.TabIndex = 18;
			this.Label1.Text = "Region name";
			this.GroupBox2.Controls.Add(this.buttonSelectRegion);
			this.GroupBox2.Controls.Add(this.comboBoxStateSelection);
			this.GroupBox2.Controls.Add(this.rbSelRegion);
			this.GroupBox2.Controls.Add(this.rbStateSelection);
			this.GroupBox2.Controls.Add(this.rbUserDefined);
			this.GroupBox2.Location = new Point(87, 165);
			this.GroupBox2.Name = "GroupBox2";
			this.GroupBox2.Size = new Size(533, 221);
			this.GroupBox2.TabIndex = 16;
			this.GroupBox2.TabStop = false;
			this.GroupBox2.Text = "Region Template";
			this.buttonSelectRegion.Font = new Font("Calibri", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.buttonSelectRegion.Location = new Point(231, 151);
			this.buttonSelectRegion.Name = "buttonSelectRegion";
			this.buttonSelectRegion.Size = new Size(156, 35);
			this.buttonSelectRegion.TabIndex = 5;
			this.buttonSelectRegion.Text = "Select Region";
			this.buttonSelectRegion.UseVisualStyleBackColor = true;
			this.comboBoxStateSelection.FormattingEnabled = true;
			this.comboBoxStateSelection.Location = new Point(172, 92);
			this.comboBoxStateSelection.Name = "comboBoxStateSelection";
			this.comboBoxStateSelection.Size = new Size(338, 32);
			this.comboBoxStateSelection.TabIndex = 4;
			this.rbSelRegion.AutoSize = true;
			this.rbSelRegion.Location = new Point(23, 155);
			this.rbSelRegion.Name = "rbSelRegion";
			this.rbSelRegion.Size = new Size(143, 28);
			this.rbSelRegion.TabIndex = 2;
			this.rbSelRegion.TabStop = true;
			this.rbSelRegion.Text = "Region based";
			this.rbSelRegion.UseVisualStyleBackColor = true;
			this.rbStateSelection.AutoSize = true;
			this.rbStateSelection.Location = new Point(23, 97);
			this.rbStateSelection.Name = "rbStateSelection";
			this.rbStateSelection.Size = new Size(129, 28);
			this.rbStateSelection.TabIndex = 1;
			this.rbStateSelection.Text = "State based";
			this.rbStateSelection.UseVisualStyleBackColor = true;
			this.rbUserDefined.AutoSize = true;
			this.rbUserDefined.Checked = true;
			this.rbUserDefined.Location = new Point(23, 42);
			this.rbUserDefined.Name = "rbUserDefined";
			this.rbUserDefined.Size = new Size(138, 28);
			this.rbUserDefined.TabIndex = 0;
			this.rbUserDefined.TabStop = true;
			this.rbUserDefined.Text = "User defined";
			this.rbUserDefined.UseVisualStyleBackColor = true;
			this.RegionComboBox.FormattingEnabled = true;
			this.RegionComboBox.Location = new Point(87, 74);
			this.RegionComboBox.Name = "RegionComboBox";
			this.RegionComboBox.Size = new Size(511, 32);
			this.RegionComboBox.TabIndex = 14;
			this.ToolTip1.Tag = "aaaa";
			this.ToolTip1.ToolTipTitle = "aaaaab";
			base.AutoScaleDimensions = new SizeF(10f, 22f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(706, 579);
			base.Controls.Add(this.RegionGroupBox);
			base.Controls.Add(this.cmdCancel);
			base.Controls.Add(this.cmdOk);
			this.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.Margin = new Padding(4, 6, 4, 6);
			base.Name = "RegSett";
			this.Text = "Customized Region Setup";
			this.RegionGroupBox.ResumeLayout(false);
			this.RegionGroupBox.PerformLayout();
			this.GroupBox2.ResumeLayout(false);
			this.GroupBox2.PerformLayout();
			base.ResumeLayout(false);
		}

		public RegSett(ref BEAData b)
		{
			base.Load += this.frmRegMethod_Load;
			this.PSelect = "Please Select ...";
			this.UserSelectedRegion = "User Supplied Data";
			this.multiStateRegion = "Multiple State/Region Selection";
			this.isStateRegionCommitted = false;
			this.isRegionCommitted = false;
			this.isStateFirstTime = true;
			this.InitializeComponent();
			this.bdat = b;
			this.bdat.worksum = b.worksum;
		}

		private void frmRegMethod_Load(object sender, EventArgs e)
		{
			this.setMainRegionSelectionDropList();
			this.buttonSelectRegion.Enabled = false;
			this.comboBoxStateSelection.Enabled = false;
			this.rbUserDefined.Checked = true;
			this.rbUserDefMain.Checked = true;
			this.rbSelRegion.Checked = false;
			this.rbStateSelection.Checked = false;
			this.setUdefStateSelection();
			RegionalData regStateData = this.bdat.RegStateData;
			string text = "AL";
			this.regiondat = new StateData("RegionX", regStateData.GetState(ref text).Employment_base.Count);
			this.regiondat.StateSetup(ref this.bdat.NTables.FTE_Ratio);
			this.tboxRegionName.Text = "";
			this.rbSelRegion.Enabled = false;
			this.rbStateSelection.Enabled = false;
			this.rbUserDefined.Enabled = false;
			this.comboBoxStateSelection.Enabled = false;
			this.buttonSelectRegion.Enabled = false;
			this.rbSelRegMain.Checked = true;
			this.rbUserDefMain.Checked = false;
		}

		private void setMainRegionSelectionDropList()
		{
			this.RegionComboBox.Items.Clear();
			this.RegionComboBox.Items.Add(this.multiStateRegion);
			StateCodeNames stateCodeNames = new StateCodeNames();
			checked
			{
				int num = stateCodeNames.StatesFullNames.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.RegionComboBox.Items.Add(RuntimeHelpers.GetObjectValue(stateCodeNames.StatesFullNames[i]));
				}
				this.RegionComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
				this.RegionComboBox.Items.Insert(0, this.PSelect);
				this.RegionComboBox.SelectedItem = this.PSelect;
			}
		}

		private void setUdefStateSelection()
		{
			StateCodeNames stateCodes = this.bdat.stateCodes;
			this.comboBoxStateSelection.Items.Clear();
			checked
			{
				int num = stateCodes.StatesFullNames.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.comboBoxStateSelection.Items.Add(RuntimeHelpers.GetObjectValue(stateCodes.StatesFullNames[i]));
				}
				this.comboBoxStateSelection.DropDownStyle = ComboBoxStyle.DropDownList;
				this.comboBoxStateSelection.Items.Insert(0, this.PSelect);
				this.comboBoxStateSelection.SelectedItem = this.PSelect;
			}
		}

		private void RegionComboBox_DropDown(object sender, EventArgs e)
		{
			this.RegionComboBox.Items.Remove(this.PSelect);
		}

		private void RegionComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.tboxRegionName.Text = Conversions.ToString(Operators.ConcatenateObject("Modified ", this.RegionComboBox.SelectedItem));
		}

		private void cmdCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void cmdOk_Click(object sender, EventArgs e)
		{
			string text = this.RegionComboBox.Text;
			checked
			{
				if (this.rbUserDefMain.Checked)
				{
					this.rbUserDefined.Checked = true;
					if (this.tboxRegionName.Text.Trim().Length > 0)
					{
						RegionalDataEntryForm regionalDataEntryForm = new RegionalDataEntryForm();
						EditRegData editRegData = new EditRegData();
						bool flag = true;
						string text2 = this.tboxRegionName.Text.Trim();
						if (text2.Length < 1)
						{
							flag = false;
						}
						if (this.rbStateSelection.Checked & !this.isStateRegionCommitted)
						{
							flag = false;
						}
						if (this.rbSelRegion.Checked & !this.isRegionCommitted)
						{
							flag = false;
						}
						if (flag)
						{
							if (this.rbUserDefined.Checked)
							{
								this.rbUserDefMain.Checked = true;
								RegionalData regStateData = this.bdat.RegStateData;
								string text3 = "XX";
								this.regiondat = new StateData("Region", regStateData.GetState(ref text3).nrec_base);
								this.regiondat.StateSetup(ref this.bdat.NTables.FTE_Ratio);
								this.regiondat.setEmptyState();
							}
							this.regiondat.Name = text2;
							int noCols = 5;
							string[] hdrsCol = new string[5]
							{
								"Employment",
								"EC",
								"GOS",
								"T",
								"Value Added"
							};
							Matrix matrix = new Matrix(this.bdat.NTables.Employment.Count, noCols);
							int num = this.bdat.NTables.Employment.Count - 1;
							for (int i = 0; i <= num; i++)
							{
								matrix[i, 0] = this.regiondat.getEmployment()[i];
								matrix[i, 1] = this.regiondat.getEC()[i];
								matrix[i, 1] = this.regiondat.getGOS()[i];
								matrix[i, 1] = this.regiondat.getT()[i];
								matrix[i, 4] = this.regiondat.getGDP()[i];
							}
							editRegData.hdrsCol = hdrsCol;
							editRegData.hdrsRow = this.bdat.NTables.hdrInd;
							editRegData.BaseTable = matrix;
							regionalDataEntryForm.dispdat = editRegData;
							regionalDataEntryForm.testData();
							if (regionalDataEntryForm.ShowDialog() == DialogResult.OK)
							{
								this.regiondat.Name = text2;
								base.DialogResult = DialogResult.OK;
								base.Close();
							}
						}
					}
					else
					{
						Interaction.MsgBox("Please Enter Region Name.", MsgBoxStyle.OkOnly, null);
						this.tboxRegionName.Focus();
					}
				}
				if (this.rbSelRegMain.Checked)
				{
					if (!text.Equals(this.PSelect))
					{
						string left = text;
						if (Operators.CompareString(left, this.multiStateRegion, false) == 0)
						{
							MultistateForm multistateForm = new MultistateForm(ref this.bdat);
							if (multistateForm.ShowDialog() == DialogResult.OK)
							{
								base.DialogResult = DialogResult.OK;
								base.Close();
							}
						}
						else if (Operators.CompareString(left, "", false) == 0)
						{
							this.RegionComboBox.Focus();
						}
						else
						{
							this.regiondat.Name = text;
							this.tboxRegionName.Text = text;
							base.DialogResult = DialogResult.OK;
							base.Close();
						}
					}
					else
					{
						this.RegionComboBox.Focus();
					}
				}
			}
		}

		private void buttonSelectRegion_Click(object sender, EventArgs e)
		{
			MultistateForm multistateForm = new MultistateForm(ref this.bdat);
			if (multistateForm.ShowDialog() == DialogResult.OK)
			{
				this.regiondat = this.bdat.RegionSelected;
				this.isRegionCommitted = true;
				this.tboxRegionName.Text = "Modified " + this.bdat.worksum.regby;
			}
		}

		private void comboBoxStateSelection_DropDown(object sender, EventArgs e)
		{
			this.comboBoxStateSelection.Items.Remove(this.PSelect);
		}

		private void rbStateSelection_CheckedChanged(object sender, EventArgs e)
		{
			this.isStateRegionCommitted = false;
			if (this.rbStateSelection.Checked)
			{
				this.comboBoxStateSelection.Enabled = true;
				this.buttonSelectRegion.Enabled = false;
				this.tboxRegionName.Text = "";
				this.isRegionCommitted = false;
				if (Conversions.ToBoolean(this.isStateFirstTime))
				{
					this.isStateFirstTime = false;
				}
				else if (Operators.ConditionalCompareObjectEqual(this.comboBoxStateSelection.SelectedItem, this.PSelect, false))
				{
					this.isStateRegionCommitted = false;
				}
				else
				{
					this.cBoxStateSelect();
					this.isStateRegionCommitted = true;
				}
			}
			else
			{
				this.comboBoxStateSelection.Enabled = false;
				this.isStateRegionCommitted = false;
			}
		}

		private void rbSelRegion_CheckedChanged(object sender, EventArgs e)
		{
			if (this.rbSelRegion.Checked)
			{
				this.buttonSelectRegion.Enabled = true;
				this.comboBoxStateSelection.Enabled = false;
				this.tboxRegionName.Text = "";
				this.isStateRegionCommitted = false;
			}
			else
			{
				this.buttonSelectRegion.Enabled = false;
				this.isRegionCommitted = false;
			}
		}

		private void comboBoxStateSelection_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.cBoxStateSelect();
		}

		private void cBoxStateSelect()
		{
			StateCodeNames stateCodes = this.bdat.stateCodes;
			double num = 1000000.0;
			string text = Conversions.ToString(this.comboBoxStateSelection.SelectedItem);
			string stateID = stateCodes.getStateID(text);
			StateData state = this.bdat.RegStateData.GetState(ref stateID);
			this.tboxRegionName.Text = "Modified " + text;
			this.regiondat = state;
			this.isStateRegionCommitted = true;
		}

		private void rbUDef_CheckedChanged(object sender, EventArgs e)
		{
			if (this.rbUserDefMain.Checked)
			{
				this.rbSelRegion.Enabled = true;
				this.rbStateSelection.Enabled = true;
				this.rbUserDefined.Enabled = true;
				this.rbUserDefined.Checked = true;
				this.setUdefStateSelection();
				this.tboxRegionName.Text = "";
			}
			else
			{
				this.rbSelRegion.Enabled = false;
				this.rbStateSelection.Enabled = false;
				this.rbUserDefined.Enabled = false;
				this.comboBoxStateSelection.Enabled = false;
				this.buttonSelectRegion.Enabled = false;
			}
		}

		private void selRegMain_CheckedChanged(object sender, EventArgs e)
		{
			if (this.rbSelRegMain.Checked)
			{
				this.RegionComboBox.Enabled = true;
				this.setMainRegionSelectionDropList();
				this.tboxRegionName.Text = "";
			}
			else
			{
				this.RegionComboBox.Enabled = false;
			}
		}
	}
}
