using Matrix_Lib;

namespace IOSNAP
{
	public class EditRegData
	{
		public Matrix BaseTable;

		public Matrix ColorTable;

		public Matrix ExtraCols;

		public Matrix ExtraRows;

		public string[] hdrsCol;

		public string[] hdrsRow;

		public string[] hdrsXCols;

		public string[] hdrsXRows;

		public int[] FormatId;

		public bool useGeneralFormatOnly;

		public int[] DataFormatTable;

		public bool isReadOnly;

		public bool isSortable;

		public bool showCellColor;

		public bool hasEmployment;

		public bool isAfterGRT;

		public bool isAfterRegionalization;

		public EditRegData()
		{
			this.useGeneralFormatOnly = true;
			this.isReadOnly = false;
			this.isSortable = false;
			this.showCellColor = false;
			this.hasEmployment = false;
			this.isAfterGRT = false;
			this.isAfterRegionalization = false;
		}

		public Vector getColSum()
		{
			Vector vector = Matrix.ColSum(this.BaseTable);
			if ((object)this.ExtraRows != null)
			{
				checked
				{
					int num = this.ExtraRows.NoRows - 1;
					for (int i = 0; i <= num; i++)
					{
						int num2 = this.ExtraRows.NoCols - 1;
						for (int j = 0; j <= num2; j++)
						{
							Vector vector2;
							int row;
							(vector2 = vector)[row = j] = unchecked(vector2[row] + this.ExtraRows[i, j]);
						}
					}
				}
			}
			return vector;
		}
	}
}
