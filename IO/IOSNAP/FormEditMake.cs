using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class FormEditMake : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("summarydgvnn")]
		private DataGridView _summarydgvnn;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("tbar2")]
		private TrackBar _tbar2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("headersListBox")]
		private ListBox _headersListBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("UndoButton")]
		private Button _UndoButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("tbar")]
		private TrackBar _tbar;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("dgvnn")]
		private DataGridView _dgvnn;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Cancel_Button")]
		private Button _Cancel_Button;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ProceedButton")]
		private Button _ProceedButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label3")]
		private Label _Label3;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("label_industryedited")]
		private Label _label_industryedited;

		private string[] headersCols;

		private ArrayList headersRows;

		public ViewOptions view_options;

		private int cellwidth;

		private BEAData bdat;

		public bool isEdited;

		private Vector modifiedColumn;

		private Color non_edit_color;

		private Color edit_color;

		private const string ed_field = ": Industry Output";

		internal virtual DataGridView summarydgvnn
		{
			[CompilerGenerated]
			get
			{
				return this._summarydgvnn;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.dgvnsummary_selectionchanged;
				DataGridView summarydgvnn = this._summarydgvnn;
				if (summarydgvnn != null)
				{
					summarydgvnn.SelectionChanged -= value2;
				}
				this._summarydgvnn = value;
				summarydgvnn = this._summarydgvnn;
				if (summarydgvnn != null)
				{
					summarydgvnn.SelectionChanged += value2;
				}
			}
		}

		internal virtual TrackBar tbar2
		{
			[CompilerGenerated]
			get
			{
				return this._tbar2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.tbar2_Changed;
				TrackBar tbar = this._tbar2;
				if (tbar != null)
				{
					tbar.ValueChanged -= value2;
				}
				this._tbar2 = value;
				tbar = this._tbar2;
				if (tbar != null)
				{
					tbar.ValueChanged += value2;
				}
			}
		}

		internal virtual Label Label2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ListBox headersListBox
		{
			[CompilerGenerated]
			get
			{
				return this._headersListBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ListBoxRegions_SelectedIndexChanged;
				ListBox headersListBox = this._headersListBox;
				if (headersListBox != null)
				{
					headersListBox.SelectedIndexChanged -= value2;
				}
				this._headersListBox = value;
				headersListBox = this._headersListBox;
				if (headersListBox != null)
				{
					headersListBox.SelectedIndexChanged += value2;
				}
			}
		}

		internal virtual Button UndoButton
		{
			[CompilerGenerated]
			get
			{
				return this._UndoButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Undo_Click;
				Button undoButton = this._UndoButton;
				if (undoButton != null)
				{
					undoButton.Click -= value2;
				}
				this._UndoButton = value;
				undoButton = this._UndoButton;
				if (undoButton != null)
				{
					undoButton.Click += value2;
				}
			}
		}

		internal virtual TrackBar tbar
		{
			[CompilerGenerated]
			get
			{
				return this._tbar;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.tbar_Changed;
				TrackBar tbar = this._tbar;
				if (tbar != null)
				{
					tbar.ValueChanged -= value2;
				}
				this._tbar = value;
				tbar = this._tbar;
				if (tbar != null)
				{
					tbar.ValueChanged += value2;
				}
			}
		}

		internal virtual DataGridView dgvnn
		{
			[CompilerGenerated]
			get
			{
				return this._dgvnn;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				DataGridViewCellEventHandler value2 = this.dgv3_CellClick;
				DataGridViewCellEventHandler value3 = this.dgv3_CellChange;
				DataGridViewCellEventHandler value4 = this.dgv3_CellValueChanged;
				DataGridViewCellMouseEventHandler value5 = this.CellHeader_Click;
				DataGridView dgvnn = this._dgvnn;
				if (dgvnn != null)
				{
					dgvnn.CellClick -= value2;
					dgvnn.CellEndEdit -= value3;
					dgvnn.CellValueChanged -= value4;
					dgvnn.RowHeaderMouseClick -= value5;
				}
				this._dgvnn = value;
				dgvnn = this._dgvnn;
				if (dgvnn != null)
				{
					dgvnn.CellClick += value2;
					dgvnn.CellEndEdit += value3;
					dgvnn.CellValueChanged += value4;
					dgvnn.RowHeaderMouseClick += value5;
				}
			}
		}

		internal virtual Button Cancel_Button
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button ProceedButton
		{
			[CompilerGenerated]
			get
			{
				return this._ProceedButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ProceedButton_Click;
				Button proceedButton = this._ProceedButton;
				if (proceedButton != null)
				{
					proceedButton.Click -= value2;
				}
				this._ProceedButton = value;
				proceedButton = this._ProceedButton;
				if (proceedButton != null)
				{
					proceedButton.Click += value2;
				}
			}
		}

		internal virtual Label Label3
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label label_industryedited
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.summarydgvnn = new DataGridView();
			this.tbar2 = new TrackBar();
			this.Label2 = new Label();
			this.headersListBox = new ListBox();
			this.UndoButton = new Button();
			this.tbar = new TrackBar();
			this.dgvnn = new DataGridView();
			this.Cancel_Button = new Button();
			this.ProceedButton = new Button();
			this.Label3 = new Label();
			this.label_industryedited = new Label();
			((ISupportInitialize)this.summarydgvnn).BeginInit();
			((ISupportInitialize)this.tbar2).BeginInit();
			((ISupportInitialize)this.tbar).BeginInit();
			((ISupportInitialize)this.dgvnn).BeginInit();
			base.SuspendLayout();
			this.summarydgvnn.AllowUserToAddRows = false;
			this.summarydgvnn.AllowUserToDeleteRows = false;
			this.summarydgvnn.AllowUserToResizeColumns = false;
			this.summarydgvnn.AllowUserToResizeRows = false;
			this.summarydgvnn.BackgroundColor = SystemColors.Control;
			this.summarydgvnn.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
			this.summarydgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.summarydgvnn.EditMode = DataGridViewEditMode.EditProgrammatically;
			this.summarydgvnn.Location = new Point(631, 9);
			this.summarydgvnn.Margin = new Padding(3, 4, 3, 4);
			this.summarydgvnn.Name = "summarydgvnn";
			this.summarydgvnn.ReadOnly = true;
			this.summarydgvnn.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.summarydgvnn.RowTemplate.Height = 28;
			this.summarydgvnn.Size = new Size(454, 133);
			this.summarydgvnn.TabIndex = 33;
			this.tbar2.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			this.tbar2.Location = new Point(435, 189);
			this.tbar2.Margin = new Padding(3, 4, 3, 4);
			this.tbar2.Name = "tbar2";
			this.tbar2.Orientation = Orientation.Vertical;
			this.tbar2.RightToLeft = RightToLeft.No;
			this.tbar2.RightToLeftLayout = true;
			this.tbar2.Size = new Size(45, 521);
			this.tbar2.TabIndex = 32;
			this.tbar2.TickStyle = TickStyle.None;
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Calibri", 12f, FontStyle.Bold | FontStyle.Italic | FontStyle.Underline, GraphicsUnit.Point, 0);
			this.Label2.ForeColor = SystemColors.HotTrack;
			this.Label2.Location = new Point(152, 78);
			this.Label2.Name = "Label2";
			this.Label2.Size = new Size(117, 19);
			this.Label2.TabIndex = 31;
			this.Label2.Text = "List of Industries";
			this.headersListBox.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			this.headersListBox.FormattingEnabled = true;
			this.headersListBox.ItemHeight = 14;
			this.headersListBox.Location = new Point(60, 111);
			this.headersListBox.Margin = new Padding(3, 4, 3, 4);
			this.headersListBox.Name = "headersListBox";
			this.headersListBox.Size = new Size(369, 662);
			this.headersListBox.TabIndex = 24;
			this.UndoButton.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.UndoButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.UndoButton.Location = new Point(555, 735);
			this.UndoButton.Margin = new Padding(3, 4, 3, 4);
			this.UndoButton.Name = "UndoButton";
			this.UndoButton.Size = new Size(140, 50);
			this.UndoButton.TabIndex = 23;
			this.UndoButton.Text = "Reset";
			this.UndoButton.UseVisualStyleBackColor = true;
			this.tbar.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			this.tbar.Location = new Point(19, 111);
			this.tbar.Margin = new Padding(3, 4, 3, 4);
			this.tbar.Name = "tbar";
			this.tbar.Orientation = Orientation.Vertical;
			this.tbar.RightToLeft = RightToLeft.No;
			this.tbar.RightToLeftLayout = true;
			this.tbar.Size = new Size(45, 665);
			this.tbar.TabIndex = 22;
			this.tbar.TickStyle = TickStyle.None;
			this.dgvnn.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvnn.BackgroundColor = SystemColors.Control;
			this.dgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnn.Location = new Point(469, 189);
			this.dgvnn.Margin = new Padding(3, 4, 3, 4);
			this.dgvnn.Name = "dgvnn";
			this.dgvnn.RowTemplate.Height = 28;
			this.dgvnn.Size = new Size(700, 521);
			this.dgvnn.TabIndex = 21;
			this.Cancel_Button.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.Cancel_Button.DialogResult = DialogResult.Cancel;
			this.Cancel_Button.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Cancel_Button.Location = new Point(788, 735);
			this.Cancel_Button.Margin = new Padding(3, 4, 3, 4);
			this.Cancel_Button.Name = "Cancel_Button";
			this.Cancel_Button.Size = new Size(140, 50);
			this.Cancel_Button.TabIndex = 20;
			this.Cancel_Button.Text = "Cancel";
			this.Cancel_Button.UseVisualStyleBackColor = true;
			this.ProceedButton.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.ProceedButton.DialogResult = DialogResult.OK;
			this.ProceedButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.ProceedButton.Location = new Point(961, 735);
			this.ProceedButton.Margin = new Padding(3, 4, 3, 4);
			this.ProceedButton.Name = "ProceedButton";
			this.ProceedButton.Size = new Size(140, 50);
			this.ProceedButton.TabIndex = 19;
			this.ProceedButton.Text = "Proceed";
			this.ProceedButton.UseVisualStyleBackColor = true;
			this.Label3.AutoSize = true;
			this.Label3.Font = new Font("Calibri", 24f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label3.ForeColor = SystemColors.HotTrack;
			this.Label3.Location = new Point(82, 17);
			this.Label3.Name = "Label3";
			this.Label3.Size = new Size(92, 39);
			this.Label3.TabIndex = 34;
			this.Label3.Text = "Make";
			this.label_industryedited.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.label_industryedited.AutoSize = true;
			this.label_industryedited.Font = new Font("Calibri", 9f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.label_industryedited.ForeColor = SystemColors.HotTrack;
			this.label_industryedited.Location = new Point(466, 156);
			this.label_industryedited.Name = "label_industryedited";
			this.label_industryedited.Size = new Size(39, 14);
			this.label_industryedited.TabIndex = 35;
			this.label_industryedited.Text = "Label4";
			base.AutoScaleDimensions = new SizeF(6f, 14f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(1192, 807);
			base.Controls.Add(this.dgvnn);
			base.Controls.Add(this.label_industryedited);
			base.Controls.Add(this.Label3);
			base.Controls.Add(this.summarydgvnn);
			base.Controls.Add(this.Label2);
			base.Controls.Add(this.headersListBox);
			base.Controls.Add(this.UndoButton);
			base.Controls.Add(this.tbar);
			base.Controls.Add(this.Cancel_Button);
			base.Controls.Add(this.ProceedButton);
			base.Controls.Add(this.tbar2);
			this.DoubleBuffered = true;
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.Margin = new Padding(3, 4, 3, 4);
			base.MinimizeBox = false;
			this.MinimumSize = new Size(1200, 845);
			base.Name = "FormEditMake";
			this.Text = "Make Table Editing Form";
			((ISupportInitialize)this.summarydgvnn).EndInit();
			((ISupportInitialize)this.tbar2).EndInit();
			((ISupportInitialize)this.tbar).EndInit();
			((ISupportInitialize)this.dgvnn).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public FormEditMake(ref BEAData b)
		{
			base.Load += this.Form_Load;
			this.headersRows = new ArrayList();
			this.cellwidth = 150;
			this.isEdited = false;
			this.non_edit_color = Color.LightGray;
			this.edit_color = SystemColors.Window;
			this.InitializeComponent();
			this.bdat = b;
			this.headersCols = this.bdat.hdrComm;
			checked
			{
				int num = this.bdat.NTables.Make().NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					this.headersRows.Add(this.bdat.NTables.hdrInd[i]);
				}
				this.headersRows.Add("Imports ");
				this.tbar2.Maximum = this.headersRows.Count - 1;
				this.tbar2.Minimum = 0;
				this.tbar2.Value = this.headersRows.Count - 1;
				this.tbar.Maximum = this.headersRows.Count - 1;
				this.tbar.Minimum = 0;
				this.tbar.Value = this.headersRows.Count - 1;
				int num2 = 0;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.headersRows.GetEnumerator();
					while (enumerator.MoveNext())
					{
						string item = Conversions.ToString(enumerator.Current);
						num2++;
						try
						{
							this.headersListBox.Items.Add(item);
						}
						catch (Exception ex)
						{
							ProjectData.SetProjectError(ex);
							Exception ex2 = ex;
							Interaction.MsgBox(ex2.Message + Conversions.ToString(num2), MsgBoxStyle.OkOnly, null);
							ProjectData.ClearProjectError();
						}
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				this.label_industryedited.Text = Conversions.ToString(this.headersRows[0]);
				this.headersListBox.SelectedIndex = 0;
				this.view_options = new ViewOptions();
			}
		}

		private void Form_Load(object sender, EventArgs e)
		{
			this.dgvnn.Columns.Clear();
			this.summarydgvnn.Columns.Clear();
			this.dgvnn.RightToLeft = RightToLeft.No;
			this.dgvnn.RowHeadersWidth = 275;
			this.dgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvnn.RowTemplate.Height = 24;
			this.summarydgvnn.RightToLeft = RightToLeft.No;
			this.summarydgvnn.RowHeadersWidth = 150;
			this.summarydgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.summarydgvnn.RowTemplate.Height = 24;
			checked
			{
				int num = this.headersCols.Length - 1;
				for (int i = 0; i <= num; i++)
				{
					string text = this.headersCols[i];
					this.dgvnn.Columns.Add(text, text);
				}
				this.summarydgvnn.Columns.Add("a", "a");
				this.summarydgvnn.ColumnHeadersVisible = false;
				int num2 = 1;
			}
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = this.dgvnn.Columns.GetEnumerator();
				while (enumerator.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
					dataGridViewColumn.MinimumWidth = 150;
					dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			this.dgvnn.RowHeadersVisible = true;
			this.dgvnn.AllowUserToAddRows = false;
			IEnumerator enumerator2 = default(IEnumerator);
			try
			{
				enumerator2 = this.summarydgvnn.Columns.GetEnumerator();
				while (enumerator2.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn2 = (DataGridViewColumn)enumerator2.Current;
					dataGridViewColumn2.MinimumWidth = 150;
					dataGridViewColumn2.SortMode = DataGridViewColumnSortMode.NotSortable;
					dataGridViewColumn2.ReadOnly = true;
					dataGridViewColumn2.DefaultCellStyle.BackColor = Color.LightSteelBlue;
				}
			}
			finally
			{
				if (enumerator2 is IDisposable)
				{
					(enumerator2 as IDisposable).Dispose();
				}
			}
			this.summarydgvnn.RowHeadersVisible = true;
			int noRows = this.bdat.NTables.Make().NoRows;
			checked
			{
				decimal num4;
				DataGridViewRow dataGridViewRow;
				for (int j = 0; j <= noRows; j++)
				{
					dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					if (j < this.bdat.NTables.Make().NoRows)
					{
						int num3 = this.bdat.NTables.Make().NoCols - 1;
						for (int k = 0; k <= num3; k++)
						{
							decimal d = new decimal(this.bdat.NTables.Make()[j, k]);
							DataGridViewCell dataGridViewCell = dataGridViewRow.Cells[k];
							num4 = decimal.Round(d, 2);
							dataGridViewCell.Value = num4.ToString("f2");
						}
					}
					else
					{
						int num5 = this.bdat.NTables.Make().NoCols - 1;
						for (int l = 0; l <= num5; l++)
						{
							dataGridViewRow.Cells[l].Value = this.bdat.Import[0, l];
						}
					}
					dataGridViewRow.HeaderCell.Value = RuntimeHelpers.GetObjectValue(this.headersRows[j]);
					this.dgvnn.Rows.Add(dataGridViewRow);
					if (j != 0)
					{
						dataGridViewRow.ReadOnly = true;
						dataGridViewRow.DefaultCellStyle.BackColor = this.non_edit_color;
					}
					else
					{
						dataGridViewRow.DefaultCellStyle.BackColor = this.edit_color;
					}
				}
				dataGridViewRow = new DataGridViewRow();
				dataGridViewRow.CreateCells(this.summarydgvnn);
				decimal d2 = new decimal(this.getRowSumMake(0));
				DataGridViewCell dataGridViewCell2 = dataGridViewRow.Cells[0];
				num4 = decimal.Round(d2, 2);
				dataGridViewCell2.Value = num4.ToString("f2");
				dataGridViewRow.HeaderCell.Value = "Edited Total";
				this.summarydgvnn.Rows.Add(dataGridViewRow);
				dataGridViewRow = new DataGridViewRow();
				dataGridViewRow.CreateCells(this.summarydgvnn);
				DataGridViewCell dataGridViewCell3 = dataGridViewRow.Cells[0];
				num4 = decimal.Round(d2, 2);
				dataGridViewCell3.Value = num4.ToString("f2");
				dataGridViewRow.HeaderCell.Value = "Pre-Edit Total";
				this.summarydgvnn.Rows.Add(dataGridViewRow);
				dataGridViewRow = new DataGridViewRow();
				dataGridViewRow.CreateCells(this.summarydgvnn);
				dataGridViewRow.Cells[0].Value = Operators.SubtractObject(this.summarydgvnn.Rows[0].Cells[0].Value, this.summarydgvnn.Rows[1].Cells[0].Value);
				dataGridViewRow.HeaderCell.Value = "Difference";
				this.summarydgvnn.Rows.Add(dataGridViewRow);
				this.summarydgvnn.AllowUserToAddRows = false;
				int count = this.dgvnn.Columns.Count;
				this.modifiedColumn = new Vector(count);
			}
		}

		private void dgvnsummary_selectionchanged(object sender, EventArgs e)
		{
			this.summarydgvnn.ClearSelection();
		}

		private double getRowSumMake(int id)
		{
			Vector vector = new Vector(this.bdat.NTables.Make().NoCols);
			checked
			{
				if (id < this.bdat.NTables.Make().NoRows)
				{
					int num = vector.Count - 1;
					for (int i = 0; i <= num; i++)
					{
						vector[i] = this.bdat.NTables.Make()[id, i];
					}
				}
				else
				{
					int num2 = vector.Count - 1;
					for (int j = 0; j <= num2; j++)
					{
						vector[j] = this.bdat.Import[0, j];
					}
				}
				return Vector.sum(vector);
			}
		}

		private void ListBoxRegions_SelectedIndexChanged(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State = 2;
			//		this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall = 0;
			//	}
			//	else if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init);
			//}
			//if (Operators.ConditionalCompareObjectGreater(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall, 0, false))
			//{
			//	string text = this.headersListBox.SelectedItem.ToString().Trim();
			//	int selectedIndex = this.headersListBox.SelectedIndex;
			//	this.updateDisp(selectedIndex);
			//}
			//this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall = 1;
			this.freezeDisp(true);
		}

		private void updateDisp(int idx)
		{
			string text = this.headersListBox.SelectedItem.ToString().Trim();
			checked
			{
				this.tbar.Value = this.headersRows.Count - idx - 1;
				this.tbar2.Value = this.headersRows.Count - idx - 1;
				this.headersListBox.SelectedIndex = idx;
				this.label_industryedited.Text = Conversions.ToString(this.headersRows[idx]);
				if (idx > 0)
				{
					int num = idx - 1;
					for (int i = 0; i <= num; i++)
					{
						this.dgvnn.Rows[i].ReadOnly = true;
						this.dgvnn.Rows[i].DefaultCellStyle.BackColor = this.non_edit_color;
					}
				}
				if (idx < this.headersRows.Count - 1)
				{
					int num2 = idx + 1;
					int num3 = this.headersRows.Count - 1;
					for (int j = num2; j <= num3; j++)
					{
						this.dgvnn.Rows[j].ReadOnly = true;
						this.dgvnn.Rows[j].DefaultCellStyle.BackColor = this.non_edit_color;
					}
				}
				this.dgvnn.Rows[idx].ReadOnly = false;
				this.dgvnn.Rows[idx].DefaultCellStyle.BackColor = this.edit_color;
				int columnIndex = this.dgvnn.CurrentCell.ColumnIndex;
				this.dgvnn.CurrentCell = this.dgvnn.Rows[idx].Cells[columnIndex];
				decimal d = new decimal(this.getRowSumMake(idx));
				this.summarydgvnn.Rows[1].Cells[0].Value = decimal.Round(d, 2).ToString("f2");
				this.updateSummary(idx);
			}
		}

		private void updateSummary(int idx2)
		{
			decimal num;
			decimal d;
			if (this.isEdited)
			{
				d = new decimal(this.getEditedRowSum());
				DataGridViewCell dataGridViewCell = this.summarydgvnn.Rows[0].Cells[0];
				num = decimal.Round(d, 2);
				dataGridViewCell.Value = num.ToString("f2");
			}
			else
			{
				d = new decimal(this.getRowSumMake(idx2));
				DataGridViewCell dataGridViewCell2 = this.summarydgvnn.Rows[0].Cells[0];
				num = decimal.Round(d, 2);
				dataGridViewCell2.Value = num.ToString("f2");
			}
			d = Conversions.ToDecimal(Operators.SubtractObject(this.summarydgvnn.Rows[0].Cells[0].Value, this.summarydgvnn.Rows[1].Cells[0].Value));
			DataGridViewCell dataGridViewCell3 = this.summarydgvnn.Rows[2].Cells[0];
			num = decimal.Round(d, 2);
			dataGridViewCell3.Value = num.ToString("f2");
		}

		private void ToNextButton_Click(object sender, EventArgs e)
		{
			int selectedIndex = this.headersListBox.SelectedIndex;
			checked
			{
				if (selectedIndex < this.headersRows.Count - 1)
				{
					this.headersListBox.SelectedIndex = selectedIndex + 1;
				}
				this.freezeDisp(true);
			}
		}

		private void ToPreviousButton_Click(object sender, EventArgs e)
		{
			int selectedIndex = this.headersListBox.SelectedIndex;
			if (selectedIndex > 0)
			{
				this.headersListBox.SelectedIndex = checked(selectedIndex - 1);
			}
			this.freezeDisp(true);
		}

		private void tbar_Changed(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init);
			//}
			//if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf > 0)
			//{
			//	int selectedIndex = checked(this.headersRows.Count - this.tbar.Value - 1);
			//	this.headersListBox.SelectedIndex = selectedIndex;
			//}
			//this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf = 1;
			this.freezeDisp(true);
		}

		private void tbar2_Changed(object sender, EventArgs e)
		{
            //TODO Later
   //         if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init);
			//}
			//if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf > 0)
			//{
			//	int selectedIndex = checked(this.headersRows.Count - this.tbar2.Value - 1);
			//	this.headersListBox.SelectedIndex = selectedIndex;
			//}
			//this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf = 1;
			this.freezeDisp(true);
		}

		private void dgv3_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView)sender;
			dataGridView.BeginEdit(true);
			int columnIndex = e.ColumnIndex;
			int rowIndex = e.RowIndex;
			if (!this.isEdited)
			{
				this.headersListBox.SelectedIndex = this.dgvnn.CurrentRow.Index;
			}
			else
			{
				this.dgvnn.Rows[this.headersListBox.SelectedIndex].Cells[columnIndex].Selected = true;
				this.dgvnn.Rows[rowIndex].Cells[columnIndex].Selected = false;
				this.dgvnn.CurrentCell = this.dgvnn.Rows[this.headersListBox.SelectedIndex].Cells[columnIndex];
			}
		}

		private void dgv3_CellChange(object sender, DataGridViewCellEventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init);
			//}
			int columnIndex = e.ColumnIndex;
			int rowIndex = e.RowIndex;
			double num = default(double);
			if (double.TryParse(Conversions.ToString(this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value), out num))
			{
				decimal d = new decimal(num);
				string strB = Conversions.ToString(this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value);
				string text = decimal.Round(d, 2).ToString("f2");
				if (string.Compare(text, strB) != 0)
				{
					this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value = text;
				}
				int selectedIndex = this.headersListBox.SelectedIndex;
				double num2 = (rowIndex >= this.bdat.NTables.Make().NoRows) ? this.bdat.Import[0, columnIndex] : this.bdat.NTables.Make()[rowIndex, columnIndex];
				if (num != num2)
				{
					this.freezeDisp(false);
					this.isEdited = true;
				}
			}
			if (this.isEdited)
			{
				this.updateSummary(0);
			}
		}

		private void dgv3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
		}

		private void freezeDisp(bool dispstate)
		{
			this.headersListBox.Enabled = dispstate;
			this.tbar2.Enabled = dispstate;
			this.tbar.Enabled = dispstate;
		}

		private void Undo_Click(object sender, EventArgs e)
		{
			this.freezeDisp(true);
			this.isEdited = false;
			int selectedIndex = this.headersListBox.SelectedIndex;
			int num = 0;
			decimal num2 = default(decimal);
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = ((IEnumerable)this.dgvnn.Rows).GetEnumerator();
				while (enumerator.MoveNext())
				{
					DataGridViewRow dataGridViewRow = (DataGridViewRow)enumerator.Current;
					checked
					{
						decimal num4;
						if (num < this.dgvnn.Rows.Count - 1)
						{
							int num3 = this.headersCols.Length - 1;
							for (int i = 0; i <= num3; i++)
							{
								num2 = new decimal(this.bdat.NTables.Make()[num, i]);
								DataGridViewCell dataGridViewCell = dataGridViewRow.Cells[i];
								num4 = decimal.Round(num2, 2);
								dataGridViewCell.Value = num4.ToString("f2");
							}
						}
						else
						{
							int num5 = this.headersCols.Length - 1;
							for (int j = 0; j <= num5; j++)
							{
								num2 = new decimal(this.bdat.Import[0, j]);
								DataGridViewCell dataGridViewCell2 = dataGridViewRow.Cells[j];
								num4 = decimal.Round(num2, 2);
								dataGridViewCell2.Value = num4.ToString("f2");
							}
						}
						num++;
					}
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			this.updateSummary(this.headersListBox.SelectedIndex);
			this.freezeDisp(true);
		}

		private double getEditedRowSum()
		{
			double num = 0.0;
			double num2 = 0.0;
			int count = this.dgvnn.Columns.Count;
			Vector vector = new Vector(count);
			int selectedIndex = this.headersListBox.SelectedIndex;
			checked
			{
				int num3 = count - 1;
				for (int i = 0; i <= num3; i++)
				{
					if (i < count - 1)
					{
						if (!double.TryParse(Conversions.ToString(this.dgvnn.Rows[selectedIndex].Cells[i].Value), out num2))
						{
							this.dgvnn.Rows[selectedIndex].Cells[i].Value = this.bdat.NTables.Make()[selectedIndex, i];
						}
					}
					else if (!double.TryParse(Conversions.ToString(this.dgvnn.Rows[selectedIndex].Cells[i].Value), out num2))
					{
						this.dgvnn.Rows[selectedIndex].Cells[i].Value = this.bdat.Import[0, i];
					}
				}
				int num4 = count - 1;
				for (int j = 0; j <= num4; j++)
				{
					vector[j] = Conversions.ToDouble(this.dgvnn.Rows[selectedIndex].Cells[j].Value);
				}
				return Vector.sum(vector);
			}
		}

		private void ProceedButton_Click(object sender, EventArgs e)
		{
			int selectedIndex = this.headersListBox.SelectedIndex;
			Matrix matrix = new Matrix(this.bdat.NTables.Make().NoRows, this.bdat.NTables.Make().NoCols);
			double num = 0.0;
			double num2 = 0.0;
			double num3 = 0.0;
			double num4 = 0.0;
			int num5 = checked(this.dgvnn.Columns.Count - 1);
			for (int i = 0; i <= num5; i = checked(i + 1))
			{
				num2 = Conversions.ToDouble(Operators.AddObject(num2, this.dgvnn.Rows[selectedIndex].Cells[i].Value));
				num3 += this.bdat.NTables.Make()[selectedIndex, i];
			}
			num4 = num2 / num3;
			checked
			{
				int num6 = this.bdat.NTables.Make().NoRows - 1;
				for (int j = 0; j <= num6; j++)
				{
					int num7 = this.bdat.NTables.Make().NoCols - 1;
					for (int k = 0; k <= num7; k++)
					{
						matrix[j, k] = Conversions.ToDouble(this.dgvnn.Rows[j].Cells[k].Value);
					}
				}
				int num8 = this.bdat.Use.NoRows - 1;
				for (int l = 0; l <= num8; l++)
				{
					this.bdat.Use[l, selectedIndex] = unchecked(num4 * this.bdat.Use[l, selectedIndex]);
				}
				int num9 = this.bdat.NTables.VA.NoRows - 1;
				for (int m = 0; m <= num9; m++)
				{
					this.bdat.NTables.VA[m, selectedIndex] = unchecked(num4 * this.bdat.NTables.VA[m, selectedIndex]);
				}
				NatTables nTables = this.bdat.NTables;
				BEAData bEAData;
				Matrix use = (bEAData = this.bdat).Use;
				nTables.SetUse(ref use);
				bEAData.Use = use;
				this.bdat.NTables.SetMake(ref matrix);
			}
			this.bdat.NTables.Employment[selectedIndex] = num4 * this.bdat.NTables.Employment[selectedIndex];
			this.bdat.NTables.Employment_JOBS[selectedIndex] = num4 * this.bdat.NTables.Employment_JOBS[selectedIndex];
			this.bdat.NTables.Employment_FTE[selectedIndex] = num4 * this.bdat.NTables.Employment_FTE[selectedIndex];
			this.bdat.ImportExportUpdate();
		}

		private void CellHeader_Click(object sender, EventArgs e)
		{
			int num = 0;
			num = this.dgvnn.CurrentRow.Index;
			int columnIndex = this.dgvnn.CurrentCell.ColumnIndex;
			this.dgvnn.ClearSelection();
			this.dgvnn.CurrentCell = this.dgvnn.Rows[num].Cells[columnIndex];
			this.dgvnn.CurrentCell.Selected = true;
			if (!this.isEdited)
			{
				this.headersListBox.SelectedIndex = num;
			}
		}
	}
}
