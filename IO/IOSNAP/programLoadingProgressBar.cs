using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class programLoadingProgressBar : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("PBar")]
		private ProgressBar _PBar;

		private string s;

		internal virtual ProgressBar PBar
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.PBar = new ProgressBar();
			base.SuspendLayout();
			this.PBar.Location = new Point(46, 42);
			this.PBar.Name = "PBar";
			this.PBar.Size = new Size(581, 27);
			this.PBar.TabIndex = 0;
			this.PBar.UseWaitCursor = true;
			base.AutoScaleDimensions = new SizeF(10f, 24f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(682, 111);
			base.Controls.Add(this.PBar);
			this.Font = new Font("Calibri", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.ForeColor = Color.White;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "programLoadingProgressBar";
			this.Text = "SNAP -  Please Wait";
			base.TopMost = true;
			base.TransparencyKey = Color.FromArgb(192, 255, 255);
			base.ResumeLayout(false);
		}

		public programLoadingProgressBar()
		{
			base.Load += this.Form_Load;
			this.InitializeComponent();
		}

		private void Form_Load(object sender, EventArgs e)
		{
		}

		public void setMax(int v)
		{
			this.PBar.Maximum = v;
		}

		public void setValue(int v)
		{
			this.PBar.Value = v;
			this.PBar.Refresh();
		}

		public int getValue()
		{
			return this.PBar.Value;
		}

		private void RichTextBox1_TextChanged(object sender, EventArgs e)
		{
		}
	}
}
