using IOSNAP.My;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class MultistateForm : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OKButton")]
		private Button _OKButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CancelButtonMF")]
		private Button _CancelButtonMF;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RegionName_TextBox")]
		private TextBox _RegionName_TextBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("LabelRegionName")]
		private Label _LabelRegionName;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ListBoxRegions")]
		private ListBox _ListBoxRegions;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("AL_cbox")]
		private CheckBox _AL_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("AZ_cbox")]
		private CheckBox _AZ_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CA_cbox")]
		private CheckBox _CA_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("AK_cbox")]
		private CheckBox _AK_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("AR_cbox")]
		private CheckBox _AR_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CO_cbox")]
		private CheckBox _CO_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("FL_cbox")]
		private CheckBox _FL_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("DE_cbox")]
		private CheckBox _DE_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GA_cbox")]
		private CheckBox _GA_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("DC_cbox")]
		private CheckBox _DC_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CT_cbox")]
		private CheckBox _CT_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("KY_cbox")]
		private CheckBox _KY_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("IA_cbox")]
		private CheckBox _IA_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ID_cbox")]
		private CheckBox _ID_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("KS_cbox")]
		private CheckBox _KS_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("IL_cbox")]
		private CheckBox _IL_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("HI_cbox")]
		private CheckBox _HI_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("MI_cbox")]
		private CheckBox _MI_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("MA_cbox")]
		private CheckBox _MA_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ME_cbox")]
		private CheckBox _ME_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("MD_cbox")]
		private CheckBox _MD_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("LA_cbox")]
		private CheckBox _LA_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("MN_cbox")]
		private CheckBox _MN_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NH_cbox")]
		private CheckBox _NH_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NV_cbox")]
		private CheckBox _NV_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NE_cbox")]
		private CheckBox _NE_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("MO_cbox")]
		private CheckBox _MO_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("MT_cbox")]
		private CheckBox _MT_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("MS_cbox")]
		private CheckBox _MS_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OH_cbox")]
		private CheckBox _OH_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ND_cbox")]
		private CheckBox _ND_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NC_cbox")]
		private CheckBox _NC_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NM_cbox")]
		private CheckBox _NM_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NY_cbox")]
		private CheckBox _NY_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NJ_cbox")]
		private CheckBox _NJ_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("SD_cbox")]
		private CheckBox _SD_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("SC_cbox")]
		private CheckBox _SC_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RI_cbox")]
		private CheckBox _RI_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OR_cbox")]
		private CheckBox _OR_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("PA_cbox")]
		private CheckBox _PA_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OK_cbox")]
		private CheckBox _OK_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("WV_cbox")]
		private CheckBox _WV_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("WA_cbox")]
		private CheckBox _WA_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("VT_cbox")]
		private CheckBox _VT_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("TX_cbox")]
		private CheckBox _TX_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("UT_cbox")]
		private CheckBox _UT_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("TN_cbox")]
		private CheckBox _TN_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("VA_cbox")]
		private CheckBox _VA_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("WI_cbox")]
		private CheckBox _WI_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("WY_cbox")]
		private CheckBox _WY_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("IN_cbox")]
		private CheckBox _IN_cbox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ResetButton")]
		private Button _ResetButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox1")]
		private GroupBox _GroupBox1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox2")]
		private GroupBox _GroupBox2;

		private string sepMR;

		private string sepSR;

		private Color scolor;

		private Color baseColor;

		public BEAData beadat;

		internal virtual Button OKButton
		{
			[CompilerGenerated]
			get
			{
				return this._OKButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OKButton_Click;
				Button oKButton = this._OKButton;
				if (oKButton != null)
				{
					oKButton.Click -= value2;
				}
				this._OKButton = value;
				oKButton = this._OKButton;
				if (oKButton != null)
				{
					oKButton.Click += value2;
				}
			}
		}

		internal virtual Button CancelButtonMF
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual TextBox RegionName_TextBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label LabelRegionName
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ListBox ListBoxRegions
		{
			[CompilerGenerated]
			get
			{
				return this._ListBoxRegions;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ListBoxRegions_SelectedIndexChanged;
				ListBox listBoxRegions = this._ListBoxRegions;
				if (listBoxRegions != null)
				{
					listBoxRegions.SelectedIndexChanged -= value2;
				}
				this._ListBoxRegions = value;
				listBoxRegions = this._ListBoxRegions;
				if (listBoxRegions != null)
				{
					listBoxRegions.SelectedIndexChanged += value2;
				}
			}
		}

		internal virtual CheckBox AL_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._AL_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.AL_cbox_CheckedChanged;
				CheckBox aL_cbox = this._AL_cbox;
				if (aL_cbox != null)
				{
					aL_cbox.CheckedChanged -= value2;
				}
				this._AL_cbox = value;
				aL_cbox = this._AL_cbox;
				if (aL_cbox != null)
				{
					aL_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox AZ_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._AZ_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.AZ_cbox_CheckedChanged;
				CheckBox aZ_cbox = this._AZ_cbox;
				if (aZ_cbox != null)
				{
					aZ_cbox.CheckedChanged -= value2;
				}
				this._AZ_cbox = value;
				aZ_cbox = this._AZ_cbox;
				if (aZ_cbox != null)
				{
					aZ_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox CA_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._CA_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.CA_cbox_CheckedChanged;
				CheckBox cA_cbox = this._CA_cbox;
				if (cA_cbox != null)
				{
					cA_cbox.CheckedChanged -= value2;
				}
				this._CA_cbox = value;
				cA_cbox = this._CA_cbox;
				if (cA_cbox != null)
				{
					cA_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox AK_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._AK_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.AK_cbox_CheckedChanged;
				CheckBox aK_cbox = this._AK_cbox;
				if (aK_cbox != null)
				{
					aK_cbox.CheckedChanged -= value2;
				}
				this._AK_cbox = value;
				aK_cbox = this._AK_cbox;
				if (aK_cbox != null)
				{
					aK_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox AR_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._AR_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.AR_cbox_CheckedChanged;
				CheckBox aR_cbox = this._AR_cbox;
				if (aR_cbox != null)
				{
					aR_cbox.CheckedChanged -= value2;
				}
				this._AR_cbox = value;
				aR_cbox = this._AR_cbox;
				if (aR_cbox != null)
				{
					aR_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox CO_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._CO_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.CO_cbox_CheckedChanged;
				CheckBox cO_cbox = this._CO_cbox;
				if (cO_cbox != null)
				{
					cO_cbox.CheckedChanged -= value2;
				}
				this._CO_cbox = value;
				cO_cbox = this._CO_cbox;
				if (cO_cbox != null)
				{
					cO_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox FL_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._FL_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.FL_cbox_CheckedChanged;
				CheckBox fL_cbox = this._FL_cbox;
				if (fL_cbox != null)
				{
					fL_cbox.CheckedChanged -= value2;
				}
				this._FL_cbox = value;
				fL_cbox = this._FL_cbox;
				if (fL_cbox != null)
				{
					fL_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox DE_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._DE_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.DE_cbox_CheckedChanged;
				CheckBox dE_cbox = this._DE_cbox;
				if (dE_cbox != null)
				{
					dE_cbox.CheckedChanged -= value2;
				}
				this._DE_cbox = value;
				dE_cbox = this._DE_cbox;
				if (dE_cbox != null)
				{
					dE_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox GA_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._GA_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.GA_cbox_CheckedChanged;
				CheckBox gA_cbox = this._GA_cbox;
				if (gA_cbox != null)
				{
					gA_cbox.CheckedChanged -= value2;
				}
				this._GA_cbox = value;
				gA_cbox = this._GA_cbox;
				if (gA_cbox != null)
				{
					gA_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox DC_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._DC_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.DC_cbox_CheckedChanged;
				CheckBox dC_cbox = this._DC_cbox;
				if (dC_cbox != null)
				{
					dC_cbox.CheckedChanged -= value2;
				}
				this._DC_cbox = value;
				dC_cbox = this._DC_cbox;
				if (dC_cbox != null)
				{
					dC_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox CT_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._CT_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.CT_cbox_CheckedChanged;
				CheckBox cT_cbox = this._CT_cbox;
				if (cT_cbox != null)
				{
					cT_cbox.CheckedChanged -= value2;
				}
				this._CT_cbox = value;
				cT_cbox = this._CT_cbox;
				if (cT_cbox != null)
				{
					cT_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox KY_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._KY_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.KY_cbox_CheckedChanged;
				CheckBox kY_cbox = this._KY_cbox;
				if (kY_cbox != null)
				{
					kY_cbox.CheckedChanged -= value2;
				}
				this._KY_cbox = value;
				kY_cbox = this._KY_cbox;
				if (kY_cbox != null)
				{
					kY_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox IA_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._IA_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.IA_cbox_CheckedChanged;
				CheckBox iA_cbox = this._IA_cbox;
				if (iA_cbox != null)
				{
					iA_cbox.CheckedChanged -= value2;
				}
				this._IA_cbox = value;
				iA_cbox = this._IA_cbox;
				if (iA_cbox != null)
				{
					iA_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox ID_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._ID_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ID_cbox_CheckedChanged;
				CheckBox iD_cbox = this._ID_cbox;
				if (iD_cbox != null)
				{
					iD_cbox.CheckedChanged -= value2;
				}
				this._ID_cbox = value;
				iD_cbox = this._ID_cbox;
				if (iD_cbox != null)
				{
					iD_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox KS_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._KS_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.KS_cbox_CheckedChanged;
				CheckBox kS_cbox = this._KS_cbox;
				if (kS_cbox != null)
				{
					kS_cbox.CheckedChanged -= value2;
				}
				this._KS_cbox = value;
				kS_cbox = this._KS_cbox;
				if (kS_cbox != null)
				{
					kS_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox IL_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._IL_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.IL_cbox_CheckedChanged;
				CheckBox iL_cbox = this._IL_cbox;
				if (iL_cbox != null)
				{
					iL_cbox.CheckedChanged -= value2;
				}
				this._IL_cbox = value;
				iL_cbox = this._IL_cbox;
				if (iL_cbox != null)
				{
					iL_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox HI_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._HI_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.HI_cbox_CheckedChanged;
				CheckBox hI_cbox = this._HI_cbox;
				if (hI_cbox != null)
				{
					hI_cbox.CheckedChanged -= value2;
				}
				this._HI_cbox = value;
				hI_cbox = this._HI_cbox;
				if (hI_cbox != null)
				{
					hI_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox MI_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._MI_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.MI_cbox_CheckedChanged;
				CheckBox mI_cbox = this._MI_cbox;
				if (mI_cbox != null)
				{
					mI_cbox.CheckedChanged -= value2;
				}
				this._MI_cbox = value;
				mI_cbox = this._MI_cbox;
				if (mI_cbox != null)
				{
					mI_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox MA_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._MA_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.MA_cbox_CheckedChanged;
				CheckBox mA_cbox = this._MA_cbox;
				if (mA_cbox != null)
				{
					mA_cbox.CheckedChanged -= value2;
				}
				this._MA_cbox = value;
				mA_cbox = this._MA_cbox;
				if (mA_cbox != null)
				{
					mA_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox ME_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._ME_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ME_cbox_CheckedChanged;
				CheckBox mE_cbox = this._ME_cbox;
				if (mE_cbox != null)
				{
					mE_cbox.CheckedChanged -= value2;
				}
				this._ME_cbox = value;
				mE_cbox = this._ME_cbox;
				if (mE_cbox != null)
				{
					mE_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox MD_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._MD_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.MD_cbox_CheckedChanged;
				CheckBox mD_cbox = this._MD_cbox;
				if (mD_cbox != null)
				{
					mD_cbox.CheckedChanged -= value2;
				}
				this._MD_cbox = value;
				mD_cbox = this._MD_cbox;
				if (mD_cbox != null)
				{
					mD_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox LA_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._LA_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.LA_cbox_CheckedChanged;
				CheckBox lA_cbox = this._LA_cbox;
				if (lA_cbox != null)
				{
					lA_cbox.CheckedChanged -= value2;
				}
				this._LA_cbox = value;
				lA_cbox = this._LA_cbox;
				if (lA_cbox != null)
				{
					lA_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox MN_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._MN_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.MN_cbox_CheckedChanged;
				CheckBox mN_cbox = this._MN_cbox;
				if (mN_cbox != null)
				{
					mN_cbox.CheckedChanged -= value2;
				}
				this._MN_cbox = value;
				mN_cbox = this._MN_cbox;
				if (mN_cbox != null)
				{
					mN_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox NH_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._NH_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NH_cbox_CheckedChanged;
				CheckBox nH_cbox = this._NH_cbox;
				if (nH_cbox != null)
				{
					nH_cbox.CheckedChanged -= value2;
				}
				this._NH_cbox = value;
				nH_cbox = this._NH_cbox;
				if (nH_cbox != null)
				{
					nH_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox NV_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._NV_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NV_cbox_CheckedChanged;
				CheckBox nV_cbox = this._NV_cbox;
				if (nV_cbox != null)
				{
					nV_cbox.CheckedChanged -= value2;
				}
				this._NV_cbox = value;
				nV_cbox = this._NV_cbox;
				if (nV_cbox != null)
				{
					nV_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox NE_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._NE_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NE_cbox_CheckedChanged;
				CheckBox nE_cbox = this._NE_cbox;
				if (nE_cbox != null)
				{
					nE_cbox.CheckedChanged -= value2;
				}
				this._NE_cbox = value;
				nE_cbox = this._NE_cbox;
				if (nE_cbox != null)
				{
					nE_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox MO_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._MO_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.MO_cbox_CheckedChanged;
				CheckBox mO_cbox = this._MO_cbox;
				if (mO_cbox != null)
				{
					mO_cbox.CheckedChanged -= value2;
				}
				this._MO_cbox = value;
				mO_cbox = this._MO_cbox;
				if (mO_cbox != null)
				{
					mO_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox MT_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._MT_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.MT_cbox_CheckedChanged;
				CheckBox mT_cbox = this._MT_cbox;
				if (mT_cbox != null)
				{
					mT_cbox.CheckedChanged -= value2;
				}
				this._MT_cbox = value;
				mT_cbox = this._MT_cbox;
				if (mT_cbox != null)
				{
					mT_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox MS_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._MS_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.MS_cbox_CheckedChanged;
				CheckBox mS_cbox = this._MS_cbox;
				if (mS_cbox != null)
				{
					mS_cbox.CheckedChanged -= value2;
				}
				this._MS_cbox = value;
				mS_cbox = this._MS_cbox;
				if (mS_cbox != null)
				{
					mS_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox OH_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._OH_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OH_cbox_CheckedChanged;
				CheckBox oH_cbox = this._OH_cbox;
				if (oH_cbox != null)
				{
					oH_cbox.CheckedChanged -= value2;
				}
				this._OH_cbox = value;
				oH_cbox = this._OH_cbox;
				if (oH_cbox != null)
				{
					oH_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox ND_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._ND_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ND_cbox_CheckedChanged;
				CheckBox nD_cbox = this._ND_cbox;
				if (nD_cbox != null)
				{
					nD_cbox.CheckedChanged -= value2;
				}
				this._ND_cbox = value;
				nD_cbox = this._ND_cbox;
				if (nD_cbox != null)
				{
					nD_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox NC_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._NC_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NC_cbox_CheckedChanged;
				CheckBox nC_cbox = this._NC_cbox;
				if (nC_cbox != null)
				{
					nC_cbox.CheckedChanged -= value2;
				}
				this._NC_cbox = value;
				nC_cbox = this._NC_cbox;
				if (nC_cbox != null)
				{
					nC_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox NM_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._NM_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NM_cbox_CheckedChanged;
				CheckBox nM_cbox = this._NM_cbox;
				if (nM_cbox != null)
				{
					nM_cbox.CheckedChanged -= value2;
				}
				this._NM_cbox = value;
				nM_cbox = this._NM_cbox;
				if (nM_cbox != null)
				{
					nM_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox NY_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._NY_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NY_cbox_CheckedChanged;
				CheckBox nY_cbox = this._NY_cbox;
				if (nY_cbox != null)
				{
					nY_cbox.CheckedChanged -= value2;
				}
				this._NY_cbox = value;
				nY_cbox = this._NY_cbox;
				if (nY_cbox != null)
				{
					nY_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox NJ_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._NJ_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NJ_cbox_CheckedChanged;
				CheckBox nJ_cbox = this._NJ_cbox;
				if (nJ_cbox != null)
				{
					nJ_cbox.CheckedChanged -= value2;
				}
				this._NJ_cbox = value;
				nJ_cbox = this._NJ_cbox;
				if (nJ_cbox != null)
				{
					nJ_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox SD_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._SD_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.SD_cbox_CheckedChanged;
				CheckBox sD_cbox = this._SD_cbox;
				if (sD_cbox != null)
				{
					sD_cbox.CheckedChanged -= value2;
				}
				this._SD_cbox = value;
				sD_cbox = this._SD_cbox;
				if (sD_cbox != null)
				{
					sD_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox SC_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._SC_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.SC_cbox_CheckedChanged;
				CheckBox sC_cbox = this._SC_cbox;
				if (sC_cbox != null)
				{
					sC_cbox.CheckedChanged -= value2;
				}
				this._SC_cbox = value;
				sC_cbox = this._SC_cbox;
				if (sC_cbox != null)
				{
					sC_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox RI_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._RI_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.RI_cbox_CheckedChanged;
				CheckBox rI_cbox = this._RI_cbox;
				if (rI_cbox != null)
				{
					rI_cbox.CheckedChanged -= value2;
				}
				this._RI_cbox = value;
				rI_cbox = this._RI_cbox;
				if (rI_cbox != null)
				{
					rI_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox OR_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._OR_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OR_cbox_CheckedChanged;
				CheckBox oR_cbox = this._OR_cbox;
				if (oR_cbox != null)
				{
					oR_cbox.CheckedChanged -= value2;
				}
				this._OR_cbox = value;
				oR_cbox = this._OR_cbox;
				if (oR_cbox != null)
				{
					oR_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox PA_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._PA_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.PA_cbox_CheckedChanged;
				CheckBox pA_cbox = this._PA_cbox;
				if (pA_cbox != null)
				{
					pA_cbox.CheckedChanged -= value2;
				}
				this._PA_cbox = value;
				pA_cbox = this._PA_cbox;
				if (pA_cbox != null)
				{
					pA_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox OK_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._OK_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OK_cbox_CheckedChanged;
				CheckBox oK_cbox = this._OK_cbox;
				if (oK_cbox != null)
				{
					oK_cbox.CheckedChanged -= value2;
				}
				this._OK_cbox = value;
				oK_cbox = this._OK_cbox;
				if (oK_cbox != null)
				{
					oK_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox WV_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._WV_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.WV_cbox_CheckedChanged;
				CheckBox wV_cbox = this._WV_cbox;
				if (wV_cbox != null)
				{
					wV_cbox.CheckedChanged -= value2;
				}
				this._WV_cbox = value;
				wV_cbox = this._WV_cbox;
				if (wV_cbox != null)
				{
					wV_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox WA_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._WA_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.WA_cbox_CheckedChanged;
				CheckBox wA_cbox = this._WA_cbox;
				if (wA_cbox != null)
				{
					wA_cbox.CheckedChanged -= value2;
				}
				this._WA_cbox = value;
				wA_cbox = this._WA_cbox;
				if (wA_cbox != null)
				{
					wA_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox VT_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._VT_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.VT_cbox_CheckedChanged;
				CheckBox vT_cbox = this._VT_cbox;
				if (vT_cbox != null)
				{
					vT_cbox.CheckedChanged -= value2;
				}
				this._VT_cbox = value;
				vT_cbox = this._VT_cbox;
				if (vT_cbox != null)
				{
					vT_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox TX_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._TX_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.TX_cbox_CheckedChanged;
				CheckBox tX_cbox = this._TX_cbox;
				if (tX_cbox != null)
				{
					tX_cbox.CheckedChanged -= value2;
				}
				this._TX_cbox = value;
				tX_cbox = this._TX_cbox;
				if (tX_cbox != null)
				{
					tX_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox UT_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._UT_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.UT_cbox_CheckedChanged;
				CheckBox uT_cbox = this._UT_cbox;
				if (uT_cbox != null)
				{
					uT_cbox.CheckedChanged -= value2;
				}
				this._UT_cbox = value;
				uT_cbox = this._UT_cbox;
				if (uT_cbox != null)
				{
					uT_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox TN_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._TN_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.TN_cbox_CheckedChanged;
				CheckBox tN_cbox = this._TN_cbox;
				if (tN_cbox != null)
				{
					tN_cbox.CheckedChanged -= value2;
				}
				this._TN_cbox = value;
				tN_cbox = this._TN_cbox;
				if (tN_cbox != null)
				{
					tN_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox VA_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._VA_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.VA_cbox_CheckedChanged;
				CheckBox vA_cbox = this._VA_cbox;
				if (vA_cbox != null)
				{
					vA_cbox.CheckedChanged -= value2;
				}
				this._VA_cbox = value;
				vA_cbox = this._VA_cbox;
				if (vA_cbox != null)
				{
					vA_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox WI_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._WI_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.WI_cbox_CheckedChanged;
				CheckBox wI_cbox = this._WI_cbox;
				if (wI_cbox != null)
				{
					wI_cbox.CheckedChanged -= value2;
				}
				this._WI_cbox = value;
				wI_cbox = this._WI_cbox;
				if (wI_cbox != null)
				{
					wI_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox WY_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._WY_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.WY_cbox_CheckedChanged;
				CheckBox wY_cbox = this._WY_cbox;
				if (wY_cbox != null)
				{
					wY_cbox.CheckedChanged -= value2;
				}
				this._WY_cbox = value;
				wY_cbox = this._WY_cbox;
				if (wY_cbox != null)
				{
					wY_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual CheckBox IN_cbox
		{
			[CompilerGenerated]
			get
			{
				return this._IN_cbox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.IN_cbox_CheckedChanged;
				CheckBox iN_cbox = this._IN_cbox;
				if (iN_cbox != null)
				{
					iN_cbox.CheckedChanged -= value2;
				}
				this._IN_cbox = value;
				iN_cbox = this._IN_cbox;
				if (iN_cbox != null)
				{
					iN_cbox.CheckedChanged += value2;
				}
			}
		}

		internal virtual Button ResetButton
		{
			[CompilerGenerated]
			get
			{
				return this._ResetButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ResetButton_Click;
				Button resetButton = this._ResetButton;
				if (resetButton != null)
				{
					resetButton.Click -= value2;
				}
				this._ResetButton = value;
				resetButton = this._ResetButton;
				if (resetButton != null)
				{
					resetButton.Click += value2;
				}
			}
		}

		internal virtual GroupBox GroupBox1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual GroupBox GroupBox2
		{
			[CompilerGenerated]
			get
			{
				return this._GroupBox2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.GroupBox2_Enter;
				GroupBox groupBox = this._GroupBox2;
				if (groupBox != null)
				{
					groupBox.Enter -= value2;
				}
				this._GroupBox2 = value;
				groupBox = this._GroupBox2;
				if (groupBox != null)
				{
					groupBox.Enter += value2;
				}
			}
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.OKButton = new Button();
			this.CancelButtonMF = new Button();
			this.RegionName_TextBox = new TextBox();
			this.LabelRegionName = new Label();
			this.ListBoxRegions = new ListBox();
			this.AL_cbox = new CheckBox();
			this.AZ_cbox = new CheckBox();
			this.CA_cbox = new CheckBox();
			this.AK_cbox = new CheckBox();
			this.AR_cbox = new CheckBox();
			this.CO_cbox = new CheckBox();
			this.FL_cbox = new CheckBox();
			this.DE_cbox = new CheckBox();
			this.GA_cbox = new CheckBox();
			this.DC_cbox = new CheckBox();
			this.CT_cbox = new CheckBox();
			this.KY_cbox = new CheckBox();
			this.IA_cbox = new CheckBox();
			this.ID_cbox = new CheckBox();
			this.KS_cbox = new CheckBox();
			this.IL_cbox = new CheckBox();
			this.HI_cbox = new CheckBox();
			this.MI_cbox = new CheckBox();
			this.MA_cbox = new CheckBox();
			this.ME_cbox = new CheckBox();
			this.MD_cbox = new CheckBox();
			this.LA_cbox = new CheckBox();
			this.MN_cbox = new CheckBox();
			this.NH_cbox = new CheckBox();
			this.NV_cbox = new CheckBox();
			this.NE_cbox = new CheckBox();
			this.MO_cbox = new CheckBox();
			this.MT_cbox = new CheckBox();
			this.MS_cbox = new CheckBox();
			this.OH_cbox = new CheckBox();
			this.ND_cbox = new CheckBox();
			this.NC_cbox = new CheckBox();
			this.NM_cbox = new CheckBox();
			this.NY_cbox = new CheckBox();
			this.NJ_cbox = new CheckBox();
			this.SD_cbox = new CheckBox();
			this.SC_cbox = new CheckBox();
			this.RI_cbox = new CheckBox();
			this.OR_cbox = new CheckBox();
			this.PA_cbox = new CheckBox();
			this.OK_cbox = new CheckBox();
			this.WV_cbox = new CheckBox();
			this.WA_cbox = new CheckBox();
			this.VT_cbox = new CheckBox();
			this.TX_cbox = new CheckBox();
			this.UT_cbox = new CheckBox();
			this.TN_cbox = new CheckBox();
			this.VA_cbox = new CheckBox();
			this.WI_cbox = new CheckBox();
			this.WY_cbox = new CheckBox();
			this.IN_cbox = new CheckBox();
			this.ResetButton = new Button();
			this.GroupBox1 = new GroupBox();
			this.GroupBox2 = new GroupBox();
			this.GroupBox1.SuspendLayout();
			this.GroupBox2.SuspendLayout();
			base.SuspendLayout();
			this.OKButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.OKButton.Location = new Point(1047, 568);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new Size(136, 42);
			this.OKButton.TabIndex = 0;
			this.OKButton.Text = "OK";
			this.OKButton.UseVisualStyleBackColor = true;
			this.CancelButtonMF.DialogResult = DialogResult.Cancel;
			this.CancelButtonMF.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.CancelButtonMF.Location = new Point(865, 568);
			this.CancelButtonMF.Name = "CancelButtonMF";
			this.CancelButtonMF.Size = new Size(136, 42);
			this.CancelButtonMF.TabIndex = 1;
			this.CancelButtonMF.Text = "Cancel";
			this.CancelButtonMF.UseVisualStyleBackColor = true;
			this.RegionName_TextBox.BorderStyle = BorderStyle.None;
			this.RegionName_TextBox.Font = new Font("Calibri", 11f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.RegionName_TextBox.Location = new Point(179, 29);
			this.RegionName_TextBox.Name = "RegionName_TextBox";
			this.RegionName_TextBox.Size = new Size(502, 18);
			this.RegionName_TextBox.TabIndex = 2;
			this.LabelRegionName.AutoSize = true;
			this.LabelRegionName.Font = new Font("Calibri", 12f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.LabelRegionName.Location = new Point(6, 23);
			this.LabelRegionName.Name = "LabelRegionName";
			this.LabelRegionName.Size = new Size(95, 19);
			this.LabelRegionName.TabIndex = 3;
			this.LabelRegionName.Text = "Region name";
			this.ListBoxRegions.FormattingEnabled = true;
			this.ListBoxRegions.ItemHeight = 14;
			this.ListBoxRegions.Location = new Point(26, 28);
			this.ListBoxRegions.Name = "ListBoxRegions";
			this.ListBoxRegions.Size = new Size(361, 550);
			this.ListBoxRegions.TabIndex = 4;
			this.AL_cbox.AutoSize = true;
			this.AL_cbox.Location = new Point(6, 39);
			this.AL_cbox.Name = "AL_cbox";
			this.AL_cbox.Size = new Size(70, 18);
			this.AL_cbox.TabIndex = 6;
			this.AL_cbox.Text = "Alabama";
			this.AL_cbox.UseVisualStyleBackColor = true;
			this.AZ_cbox.AutoSize = true;
			this.AZ_cbox.Location = new Point(304, 39);
			this.AZ_cbox.Name = "AZ_cbox";
			this.AZ_cbox.Size = new Size(63, 18);
			this.AZ_cbox.TabIndex = 7;
			this.AZ_cbox.Text = "Arizona";
			this.AZ_cbox.UseVisualStyleBackColor = true;
			this.CA_cbox.AutoSize = true;
			this.CA_cbox.Location = new Point(584, 39);
			this.CA_cbox.Name = "CA_cbox";
			this.CA_cbox.Size = new Size(73, 18);
			this.CA_cbox.TabIndex = 8;
			this.CA_cbox.Text = "California";
			this.CA_cbox.UseVisualStyleBackColor = true;
			this.AK_cbox.AutoSize = true;
			this.AK_cbox.Location = new Point(171, 39);
			this.AK_cbox.Name = "AK_cbox";
			this.AK_cbox.Size = new Size(59, 18);
			this.AK_cbox.TabIndex = 10;
			this.AK_cbox.Text = "Alaska";
			this.AK_cbox.UseVisualStyleBackColor = true;
			this.AR_cbox.AutoSize = true;
			this.AR_cbox.Location = new Point(446, 39);
			this.AR_cbox.Name = "AR_cbox";
			this.AR_cbox.Size = new Size(71, 18);
			this.AR_cbox.TabIndex = 11;
			this.AR_cbox.Text = "Arkansas";
			this.AR_cbox.UseVisualStyleBackColor = true;
			this.CO_cbox.AutoSize = true;
			this.CO_cbox.Location = new Point(746, 39);
			this.CO_cbox.Name = "CO_cbox";
			this.CO_cbox.Size = new Size(69, 18);
			this.CO_cbox.TabIndex = 12;
			this.CO_cbox.Text = "Colorado";
			this.CO_cbox.UseVisualStyleBackColor = true;
			this.FL_cbox.AutoSize = true;
			this.FL_cbox.Location = new Point(584, 74);
			this.FL_cbox.Name = "FL_cbox";
			this.FL_cbox.Size = new Size(60, 18);
			this.FL_cbox.TabIndex = 17;
			this.FL_cbox.Text = "Florida";
			this.FL_cbox.UseVisualStyleBackColor = true;
			this.DE_cbox.AutoSize = true;
			this.DE_cbox.Location = new Point(171, 74);
			this.DE_cbox.Name = "DE_cbox";
			this.DE_cbox.Size = new Size(74, 18);
			this.DE_cbox.TabIndex = 16;
			this.DE_cbox.Text = "Delaware";
			this.DE_cbox.UseVisualStyleBackColor = true;
			this.GA_cbox.AutoSize = true;
			this.GA_cbox.Location = new Point(746, 74);
			this.GA_cbox.Name = "GA_cbox";
			this.GA_cbox.Size = new Size(65, 18);
			this.GA_cbox.TabIndex = 15;
			this.GA_cbox.Text = "Georgia";
			this.GA_cbox.UseVisualStyleBackColor = true;
			this.DC_cbox.AutoSize = true;
			this.DC_cbox.Location = new Point(304, 74);
			this.DC_cbox.Name = "DC_cbox";
			this.DC_cbox.Size = new Size(124, 18);
			this.DC_cbox.TabIndex = 14;
			this.DC_cbox.Text = "District of Columbia";
			this.DC_cbox.UseVisualStyleBackColor = true;
			this.CT_cbox.AutoSize = true;
			this.CT_cbox.Location = new Point(6, 74);
			this.CT_cbox.Name = "CT_cbox";
			this.CT_cbox.Size = new Size(83, 18);
			this.CT_cbox.TabIndex = 13;
			this.CT_cbox.Text = "Connecticut";
			this.CT_cbox.UseVisualStyleBackColor = true;
			this.KY_cbox.AutoSize = true;
			this.KY_cbox.Location = new Point(6, 144);
			this.KY_cbox.Name = "KY_cbox";
			this.KY_cbox.Size = new Size(72, 18);
			this.KY_cbox.TabIndex = 23;
			this.KY_cbox.Text = "Kentucky";
			this.KY_cbox.UseVisualStyleBackColor = true;
			this.IA_cbox.AutoSize = true;
			this.IA_cbox.Location = new Point(584, 109);
			this.IA_cbox.Name = "IA_cbox";
			this.IA_cbox.Size = new Size(50, 18);
			this.IA_cbox.TabIndex = 22;
			this.IA_cbox.Text = "Iowa";
			this.IA_cbox.UseVisualStyleBackColor = true;
			this.ID_cbox.AutoSize = true;
			this.ID_cbox.Location = new Point(171, 109);
			this.ID_cbox.Name = "ID_cbox";
			this.ID_cbox.Size = new Size(53, 18);
			this.ID_cbox.TabIndex = 21;
			this.ID_cbox.Text = "Idaho";
			this.ID_cbox.UseVisualStyleBackColor = true;
			this.KS_cbox.AutoSize = true;
			this.KS_cbox.Location = new Point(746, 109);
			this.KS_cbox.Name = "KS_cbox";
			this.KS_cbox.Size = new Size(61, 18);
			this.KS_cbox.TabIndex = 20;
			this.KS_cbox.Text = "Kansas";
			this.KS_cbox.UseVisualStyleBackColor = true;
			this.IL_cbox.AutoSize = true;
			this.IL_cbox.Location = new Point(304, 109);
			this.IL_cbox.Name = "IL_cbox";
			this.IL_cbox.Size = new Size(58, 18);
			this.IL_cbox.TabIndex = 19;
			this.IL_cbox.Text = "Illinois";
			this.IL_cbox.UseVisualStyleBackColor = true;
			this.HI_cbox.AutoSize = true;
			this.HI_cbox.Location = new Point(6, 109);
			this.HI_cbox.Name = "HI_cbox";
			this.HI_cbox.Size = new Size(61, 18);
			this.HI_cbox.TabIndex = 18;
			this.HI_cbox.Text = "Hawaii";
			this.HI_cbox.UseVisualStyleBackColor = true;
			this.MI_cbox.AutoSize = true;
			this.MI_cbox.Location = new Point(746, 144);
			this.MI_cbox.Name = "MI_cbox";
			this.MI_cbox.Size = new Size(71, 18);
			this.MI_cbox.TabIndex = 29;
			this.MI_cbox.Text = "Michigan";
			this.MI_cbox.UseVisualStyleBackColor = true;
			this.MA_cbox.AutoSize = true;
			this.MA_cbox.Location = new Point(584, 144);
			this.MA_cbox.Name = "MA_cbox";
			this.MA_cbox.Size = new Size(99, 18);
			this.MA_cbox.TabIndex = 28;
			this.MA_cbox.Text = "Massachusetts";
			this.MA_cbox.UseVisualStyleBackColor = true;
			this.ME_cbox.AutoSize = true;
			this.ME_cbox.Location = new Point(304, 144);
			this.ME_cbox.Name = "ME_cbox";
			this.ME_cbox.Size = new Size(57, 18);
			this.ME_cbox.TabIndex = 27;
			this.ME_cbox.Text = "Maine";
			this.ME_cbox.UseVisualStyleBackColor = true;
			this.MD_cbox.AutoSize = true;
			this.MD_cbox.Location = new Point(446, 144);
			this.MD_cbox.Name = "MD_cbox";
			this.MD_cbox.Size = new Size(73, 18);
			this.MD_cbox.TabIndex = 25;
			this.MD_cbox.Text = "Maryland";
			this.MD_cbox.UseVisualStyleBackColor = true;
			this.LA_cbox.AutoSize = true;
			this.LA_cbox.Location = new Point(171, 144);
			this.LA_cbox.Name = "LA_cbox";
			this.LA_cbox.Size = new Size(72, 18);
			this.LA_cbox.TabIndex = 24;
			this.LA_cbox.Text = "Louisiana";
			this.LA_cbox.UseVisualStyleBackColor = true;
			this.MN_cbox.AutoSize = true;
			this.MN_cbox.Location = new Point(6, 187);
			this.MN_cbox.Name = "MN_cbox";
			this.MN_cbox.Size = new Size(78, 18);
			this.MN_cbox.TabIndex = 30;
			this.MN_cbox.Text = "Minnesota";
			this.MN_cbox.UseVisualStyleBackColor = true;
			this.NH_cbox.AutoSize = true;
			this.NH_cbox.Location = new Point(6, 231);
			this.NH_cbox.Name = "NH_cbox";
			this.NH_cbox.Size = new Size(106, 18);
			this.NH_cbox.TabIndex = 36;
			this.NH_cbox.Text = "New Hampshire";
			this.NH_cbox.UseVisualStyleBackColor = true;
			this.NV_cbox.AutoSize = true;
			this.NV_cbox.Location = new Point(746, 187);
			this.NV_cbox.Name = "NV_cbox";
			this.NV_cbox.Size = new Size(64, 18);
			this.NV_cbox.TabIndex = 35;
			this.NV_cbox.Text = "Nevada";
			this.NV_cbox.UseVisualStyleBackColor = true;
			this.NE_cbox.AutoSize = true;
			this.NE_cbox.Location = new Point(584, 187);
			this.NE_cbox.Name = "NE_cbox";
			this.NE_cbox.Size = new Size(73, 18);
			this.NE_cbox.TabIndex = 34;
			this.NE_cbox.Text = "Nebraska";
			this.NE_cbox.UseVisualStyleBackColor = true;
			this.MO_cbox.AutoSize = true;
			this.MO_cbox.Location = new Point(304, 187);
			this.MO_cbox.Name = "MO_cbox";
			this.MO_cbox.Size = new Size(68, 18);
			this.MO_cbox.TabIndex = 33;
			this.MO_cbox.Text = "Missouri";
			this.MO_cbox.UseVisualStyleBackColor = true;
			this.MT_cbox.AutoSize = true;
			this.MT_cbox.Location = new Point(446, 187);
			this.MT_cbox.Name = "MT_cbox";
			this.MT_cbox.Size = new Size(70, 18);
			this.MT_cbox.TabIndex = 32;
			this.MT_cbox.Text = "Montana";
			this.MT_cbox.UseVisualStyleBackColor = true;
			this.MS_cbox.AutoSize = true;
			this.MS_cbox.Location = new Point(171, 187);
			this.MS_cbox.Name = "MS_cbox";
			this.MS_cbox.Size = new Size(74, 18);
			this.MS_cbox.TabIndex = 31;
			this.MS_cbox.Text = "Mississipi";
			this.MS_cbox.UseVisualStyleBackColor = true;
			this.OH_cbox.AutoSize = true;
			this.OH_cbox.Location = new Point(6, 274);
			this.OH_cbox.Name = "OH_cbox";
			this.OH_cbox.Size = new Size(49, 18);
			this.OH_cbox.TabIndex = 42;
			this.OH_cbox.Text = "Ohio";
			this.OH_cbox.UseVisualStyleBackColor = true;
			this.ND_cbox.AutoSize = true;
			this.ND_cbox.Location = new Point(746, 231);
			this.ND_cbox.Name = "ND_cbox";
			this.ND_cbox.Size = new Size(93, 18);
			this.ND_cbox.TabIndex = 41;
			this.ND_cbox.Text = "North Dakota";
			this.ND_cbox.UseVisualStyleBackColor = true;
			this.NC_cbox.AutoSize = true;
			this.NC_cbox.Location = new Point(584, 231);
			this.NC_cbox.Name = "NC_cbox";
			this.NC_cbox.Size = new Size(97, 18);
			this.NC_cbox.TabIndex = 40;
			this.NC_cbox.Text = "North Carolina";
			this.NC_cbox.UseVisualStyleBackColor = true;
			this.NM_cbox.AutoSize = true;
			this.NM_cbox.Location = new Point(304, 231);
			this.NM_cbox.Name = "NM_cbox";
			this.NM_cbox.Size = new Size(88, 18);
			this.NM_cbox.TabIndex = 39;
			this.NM_cbox.Text = "New Mexico";
			this.NM_cbox.UseVisualStyleBackColor = true;
			this.NY_cbox.AutoSize = true;
			this.NY_cbox.Location = new Point(446, 231);
			this.NY_cbox.Name = "NY_cbox";
			this.NY_cbox.Size = new Size(73, 18);
			this.NY_cbox.TabIndex = 38;
			this.NY_cbox.Text = "New York";
			this.NY_cbox.UseVisualStyleBackColor = true;
			this.NJ_cbox.AutoSize = true;
			this.NJ_cbox.Location = new Point(171, 231);
			this.NJ_cbox.Name = "NJ_cbox";
			this.NJ_cbox.Size = new Size(83, 18);
			this.NJ_cbox.TabIndex = 37;
			this.NJ_cbox.Text = "New Jersey";
			this.NJ_cbox.UseVisualStyleBackColor = true;
			this.SD_cbox.AutoSize = true;
			this.SD_cbox.Location = new Point(6, 318);
			this.SD_cbox.Name = "SD_cbox";
			this.SD_cbox.Size = new Size(93, 18);
			this.SD_cbox.TabIndex = 48;
			this.SD_cbox.Text = "South Dakota";
			this.SD_cbox.UseVisualStyleBackColor = true;
			this.SC_cbox.AutoSize = true;
			this.SC_cbox.Location = new Point(746, 274);
			this.SC_cbox.Name = "SC_cbox";
			this.SC_cbox.Size = new Size(97, 18);
			this.SC_cbox.TabIndex = 47;
			this.SC_cbox.Text = "South Carolina";
			this.SC_cbox.UseVisualStyleBackColor = true;
			this.RI_cbox.AutoSize = true;
			this.RI_cbox.Location = new Point(584, 274);
			this.RI_cbox.Name = "RI_cbox";
			this.RI_cbox.Size = new Size(89, 18);
			this.RI_cbox.TabIndex = 46;
			this.RI_cbox.Text = "Rhode Island";
			this.RI_cbox.UseVisualStyleBackColor = true;
			this.OR_cbox.AutoSize = true;
			this.OR_cbox.Location = new Point(304, 274);
			this.OR_cbox.Name = "OR_cbox";
			this.OR_cbox.Size = new Size(62, 18);
			this.OR_cbox.TabIndex = 45;
			this.OR_cbox.Text = "Oregon";
			this.OR_cbox.UseVisualStyleBackColor = true;
			this.PA_cbox.AutoSize = true;
			this.PA_cbox.Location = new Point(446, 274);
			this.PA_cbox.Name = "PA_cbox";
			this.PA_cbox.Size = new Size(91, 18);
			this.PA_cbox.TabIndex = 44;
			this.PA_cbox.Text = "Pennsylvania";
			this.PA_cbox.UseVisualStyleBackColor = true;
			this.OK_cbox.AutoSize = true;
			this.OK_cbox.Location = new Point(171, 274);
			this.OK_cbox.Name = "OK_cbox";
			this.OK_cbox.Size = new Size(77, 18);
			this.OK_cbox.TabIndex = 43;
			this.OK_cbox.Text = "Oklahoma";
			this.OK_cbox.UseVisualStyleBackColor = true;
			this.WV_cbox.AutoSize = true;
			this.WV_cbox.Location = new Point(304, 363);
			this.WV_cbox.Name = "WV_cbox";
			this.WV_cbox.Size = new Size(93, 18);
			this.WV_cbox.TabIndex = 54;
			this.WV_cbox.Text = "West Virginia";
			this.WV_cbox.UseVisualStyleBackColor = true;
			this.WA_cbox.AutoSize = true;
			this.WA_cbox.Location = new Point(171, 363);
			this.WA_cbox.Name = "WA_cbox";
			this.WA_cbox.Size = new Size(85, 18);
			this.WA_cbox.TabIndex = 53;
			this.WA_cbox.Text = "Washington";
			this.WA_cbox.UseVisualStyleBackColor = true;
			this.VT_cbox.AutoSize = true;
			this.VT_cbox.Location = new Point(584, 318);
			this.VT_cbox.Name = "VT_cbox";
			this.VT_cbox.Size = new Size(68, 18);
			this.VT_cbox.TabIndex = 52;
			this.VT_cbox.Text = "Vermont";
			this.VT_cbox.UseVisualStyleBackColor = true;
			this.TX_cbox.AutoSize = true;
			this.TX_cbox.Location = new Point(304, 318);
			this.TX_cbox.Name = "TX_cbox";
			this.TX_cbox.Size = new Size(54, 18);
			this.TX_cbox.TabIndex = 51;
			this.TX_cbox.Text = "Texas";
			this.TX_cbox.UseVisualStyleBackColor = true;
			this.UT_cbox.AutoSize = true;
			this.UT_cbox.Location = new Point(446, 318);
			this.UT_cbox.Name = "UT_cbox";
			this.UT_cbox.Size = new Size(50, 18);
			this.UT_cbox.TabIndex = 50;
			this.UT_cbox.Text = "Utah";
			this.UT_cbox.UseVisualStyleBackColor = true;
			this.TN_cbox.AutoSize = true;
			this.TN_cbox.Location = new Point(171, 318);
			this.TN_cbox.Name = "TN_cbox";
			this.TN_cbox.Size = new Size(77, 18);
			this.TN_cbox.TabIndex = 49;
			this.TN_cbox.Text = "Tennessee";
			this.TN_cbox.UseVisualStyleBackColor = true;
			this.VA_cbox.AutoSize = true;
			this.VA_cbox.Location = new Point(746, 318);
			this.VA_cbox.Name = "VA_cbox";
			this.VA_cbox.Size = new Size(64, 18);
			this.VA_cbox.TabIndex = 55;
			this.VA_cbox.Text = "Virginia";
			this.VA_cbox.UseVisualStyleBackColor = true;
			this.WI_cbox.AutoSize = true;
			this.WI_cbox.Location = new Point(446, 363);
			this.WI_cbox.Name = "WI_cbox";
			this.WI_cbox.Size = new Size(76, 18);
			this.WI_cbox.TabIndex = 56;
			this.WI_cbox.Text = "Wisconsin";
			this.WI_cbox.UseVisualStyleBackColor = true;
			this.WY_cbox.AutoSize = true;
			this.WY_cbox.Location = new Point(584, 363);
			this.WY_cbox.Name = "WY_cbox";
			this.WY_cbox.Size = new Size(74, 18);
			this.WY_cbox.TabIndex = 57;
			this.WY_cbox.Text = "Wyoming";
			this.WY_cbox.UseVisualStyleBackColor = true;
			this.IN_cbox.AutoSize = true;
			this.IN_cbox.Location = new Point(446, 109);
			this.IN_cbox.Name = "IN_cbox";
			this.IN_cbox.Size = new Size(62, 18);
			this.IN_cbox.TabIndex = 58;
			this.IN_cbox.Text = "Indiana";
			this.IN_cbox.UseVisualStyleBackColor = true;
			this.ResetButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.ResetButton.Location = new Point(686, 568);
			this.ResetButton.Name = "ResetButton";
			this.ResetButton.Size = new Size(136, 42);
			this.ResetButton.TabIndex = 59;
			this.ResetButton.Text = "Reset";
			this.ResetButton.UseVisualStyleBackColor = true;
			this.GroupBox1.Controls.Add(this.LabelRegionName);
			this.GroupBox1.Controls.Add(this.RegionName_TextBox);
			this.GroupBox1.FlatStyle = FlatStyle.Popup;
			this.GroupBox1.Location = new Point(543, 461);
			this.GroupBox1.Name = "GroupBox1";
			this.GroupBox1.Size = new Size(699, 68);
			this.GroupBox1.TabIndex = 60;
			this.GroupBox1.TabStop = false;
			this.GroupBox2.Controls.Add(this.SD_cbox);
			this.GroupBox2.Controls.Add(this.TN_cbox);
			this.GroupBox2.Controls.Add(this.UT_cbox);
			this.GroupBox2.Controls.Add(this.CO_cbox);
			this.GroupBox2.Controls.Add(this.IN_cbox);
			this.GroupBox2.Controls.Add(this.AR_cbox);
			this.GroupBox2.Controls.Add(this.TX_cbox);
			this.GroupBox2.Controls.Add(this.AK_cbox);
			this.GroupBox2.Controls.Add(this.MI_cbox);
			this.GroupBox2.Controls.Add(this.CA_cbox);
			this.GroupBox2.Controls.Add(this.SC_cbox);
			this.GroupBox2.Controls.Add(this.AZ_cbox);
			this.GroupBox2.Controls.Add(this.MA_cbox);
			this.GroupBox2.Controls.Add(this.AL_cbox);
			this.GroupBox2.Controls.Add(this.WY_cbox);
			this.GroupBox2.Controls.Add(this.ME_cbox);
			this.GroupBox2.Controls.Add(this.RI_cbox);
			this.GroupBox2.Controls.Add(this.MD_cbox);
			this.GroupBox2.Controls.Add(this.VT_cbox);
			this.GroupBox2.Controls.Add(this.LA_cbox);
			this.GroupBox2.Controls.Add(this.OR_cbox);
			this.GroupBox2.Controls.Add(this.KY_cbox);
			this.GroupBox2.Controls.Add(this.WI_cbox);
			this.GroupBox2.Controls.Add(this.IA_cbox);
			this.GroupBox2.Controls.Add(this.PA_cbox);
			this.GroupBox2.Controls.Add(this.ID_cbox);
			this.GroupBox2.Controls.Add(this.WA_cbox);
			this.GroupBox2.Controls.Add(this.KS_cbox);
			this.GroupBox2.Controls.Add(this.OK_cbox);
			this.GroupBox2.Controls.Add(this.IL_cbox);
			this.GroupBox2.Controls.Add(this.VA_cbox);
			this.GroupBox2.Controls.Add(this.HI_cbox);
			this.GroupBox2.Controls.Add(this.OH_cbox);
			this.GroupBox2.Controls.Add(this.FL_cbox);
			this.GroupBox2.Controls.Add(this.WV_cbox);
			this.GroupBox2.Controls.Add(this.DE_cbox);
			this.GroupBox2.Controls.Add(this.ND_cbox);
			this.GroupBox2.Controls.Add(this.GA_cbox);
			this.GroupBox2.Controls.Add(this.MN_cbox);
			this.GroupBox2.Controls.Add(this.DC_cbox);
			this.GroupBox2.Controls.Add(this.NC_cbox);
			this.GroupBox2.Controls.Add(this.CT_cbox);
			this.GroupBox2.Controls.Add(this.MS_cbox);
			this.GroupBox2.Controls.Add(this.NM_cbox);
			this.GroupBox2.Controls.Add(this.MT_cbox);
			this.GroupBox2.Controls.Add(this.NY_cbox);
			this.GroupBox2.Controls.Add(this.MO_cbox);
			this.GroupBox2.Controls.Add(this.NJ_cbox);
			this.GroupBox2.Controls.Add(this.NE_cbox);
			this.GroupBox2.Controls.Add(this.NH_cbox);
			this.GroupBox2.Controls.Add(this.NV_cbox);
			this.GroupBox2.Font = new Font("Calibri", 9f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.GroupBox2.Location = new Point(393, 28);
			this.GroupBox2.Name = "GroupBox2";
			this.GroupBox2.Size = new Size(901, 427);
			this.GroupBox2.TabIndex = 61;
			this.GroupBox2.TabStop = false;
			this.GroupBox2.Text = "States";
			base.AutoScaleDimensions = new SizeF(6f, 14f);
			base.AutoScaleMode = AutoScaleMode.Font;
			this.AutoScroll = true;
			base.ClientSize = new Size(1308, 676);
			base.Controls.Add(this.GroupBox2);
			base.Controls.Add(this.GroupBox1);
			base.Controls.Add(this.ResetButton);
			base.Controls.Add(this.ListBoxRegions);
			base.Controls.Add(this.CancelButtonMF);
			base.Controls.Add(this.OKButton);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.MaximizeBox = false;
			this.MaximumSize = new Size(1314, 704);
			base.MinimizeBox = false;
			this.MinimumSize = new Size(1314, 704);
			base.Name = "MultistateForm";
			this.Text = "Multistate Region Construction Form";
			this.GroupBox1.ResumeLayout(false);
			this.GroupBox1.PerformLayout();
			this.GroupBox2.ResumeLayout(false);
			this.GroupBox2.PerformLayout();
			base.ResumeLayout(false);
		}

		public MultistateForm(ref BEAData b)
		{
			base.Load += this.MultistateForm_Load;
			this.sepMR = "    ";
			this.sepSR = "        ";
			this.scolor = Color.LemonChiffon;
			this.baseColor = Color.White;
			this.InitializeComponent();
			this.beadat = b;
			this.baseColor = this.LabelRegionName.BackColor;
		}

		private void ListBoxRegions_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = this.ListBoxRegions.SelectedItem.ToString().Trim();
			bool flag = false;
			string[] array;
			switch (BEAData.ComputeStringHash(text))
			{
			case 2188923377u:
				if (Operators.CompareString(text, "Northeast", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[9]
					{
						"ME",
						"VT",
						"NH",
						"MA",
						"CT",
						"RI",
						"NY",
						"PA",
						"NJ"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 991703048u:
				if (Operators.CompareString(text, "New England", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[6]
					{
						"ME",
						"VT",
						"NH",
						"MA",
						"CT",
						"RI"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 3421416748u:
				if (Operators.CompareString(text, "Middle Atlantic", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[3]
					{
						"NY",
						"PA",
						"NJ"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 3017973530u:
				if (Operators.CompareString(text, "South", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[17]
					{
						"FL",
						"GA",
						"SC",
						"NC",
						"VA",
						"WV",
						"DC",
						"MD",
						"DE",
						"AR",
						"LA",
						"OK",
						"TX",
						"KY",
						"TN",
						"MS",
						"AL"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 2667202026u:
				if (Operators.CompareString(text, "South Atlantic", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[9]
					{
						"FL",
						"GA",
						"SC",
						"NC",
						"VA",
						"WV",
						"DC",
						"MD",
						"DE"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 775015666u:
				if (Operators.CompareString(text, "West South Central", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[4]
					{
						"AR",
						"LA",
						"OK",
						"TX"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 621995524u:
				if (Operators.CompareString(text, "East South Central", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[4]
					{
						"KY",
						"TN",
						"MS",
						"AL"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 3832805742u:
				if (Operators.CompareString(text, "Midwest", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[12]
					{
						"MO",
						"KS",
						"NE",
						"IA",
						"MN",
						"SD",
						"ND",
						"IL",
						"IN",
						"OH",
						"MI",
						"WI"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 3726075082u:
				if (Operators.CompareString(text, "East North Central", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[5]
					{
						"IL",
						"IN",
						"OH",
						"MI",
						"WI"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 2899121220u:
				if (Operators.CompareString(text, "West North Central", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[7]
					{
						"MO",
						"KS",
						"NE",
						"IA",
						"MN",
						"SD",
						"ND"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 1796576718u:
				if (Operators.CompareString(text, "West", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[13]
					{
						"AZ",
						"NM",
						"CO",
						"UT",
						"NV",
						"ID",
						"WY",
						"MT",
						"AK",
						"CA",
						"HI",
						"OR",
						"WA"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 476163512u:
				if (Operators.CompareString(text, "Pacific", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[5]
					{
						"AK",
						"CA",
						"HI",
						"OR",
						"WA"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 3308967874u:
				if (Operators.CompareString(text, "Mountain", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[8]
					{
						"AZ",
						"NM",
						"CO",
						"UT",
						"NV",
						"ID",
						"WY",
						"MT"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 1274130316u:
				if (Operators.CompareString(text, "BEA New England", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[6]
					{
						"CT",
						"ME",
						"MA",
						"NH",
						"RI",
						"VT"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 3767627004u:
				if (Operators.CompareString(text, "Mideast", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[6]
					{
						"DE",
						"DC",
						"MD",
						"NJ",
						"NY",
						"PA"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 3359727902u:
				if (Operators.CompareString(text, "Great Lakes", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[5]
					{
						"IL",
						"IN",
						"MI",
						"OH",
						"WI"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 1440357532u:
				if (Operators.CompareString(text, "Plains", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[7]
					{
						"IA",
						"KS",
						"MN",
						"MO",
						"NE",
						"ND",
						"SD"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 2566400595u:
				if (Operators.CompareString(text, "Southeast", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[12]
					{
						"AL",
						"AR",
						"FL",
						"GA",
						"KY",
						"LA",
						"MS",
						"NC",
						"SC",
						"TN",
						"VA",
						"WV"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 2397948593u:
				if (Operators.CompareString(text, "Southwest", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[4]
					{
						"AZ",
						"NM",
						"OK",
						"TX"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 2091425618u:
				if (Operators.CompareString(text, "Rocky Mountain", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[5]
					{
						"CO",
						"ID",
						"MT",
						"UT",
						"WY"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 2086572801u:
				if (Operators.CompareString(text, "Far West", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[6]
					{
						"AK",
						"CA",
						"HI",
						"NV",
						"OR",
						"WA"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 472933804u:
				if (Operators.CompareString(text, "United States", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[51]
					{
						"HI",
						"AK",
						"AL",
						"AZ",
						"AR",
						"FL",
						"CO",
						"CT",
						"CA",
						"DE",
						"DC",
						"GA",
						"ID",
						"IL",
						"IN",
						"IA",
						"KS",
						"KY",
						"LA",
						"ME",
						"MD",
						"MA",
						"MI",
						"MN",
						"MS",
						"MO",
						"MT",
						"NE",
						"NH",
						"NV",
						"NJ",
						"NM",
						"ND",
						"NY",
						"NC",
						"OH",
						"OK",
						"OR",
						"PA",
						"RI",
						"SC",
						"SD",
						"TN",
						"TX",
						"UT",
						"VT",
						"VA",
						"WA",
						"WV",
						"WY",
						"WI"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 1868614360u:
				if (Operators.CompareString(text, "Lower 48 States", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[49]
					{
						"AL",
						"AZ",
						"AR",
						"FL",
						"CO",
						"CT",
						"CA",
						"DE",
						"DC",
						"GA",
						"ID",
						"IL",
						"IN",
						"IA",
						"KS",
						"KY",
						"LA",
						"ME",
						"MD",
						"MA",
						"MI",
						"MN",
						"MS",
						"MO",
						"MT",
						"NE",
						"NH",
						"NV",
						"NJ",
						"NM",
						"ND",
						"NY",
						"NC",
						"OH",
						"OK",
						"OR",
						"PA",
						"RI",
						"SC",
						"SD",
						"TN",
						"TX",
						"UT",
						"VT",
						"VA",
						"WA",
						"WV",
						"WY",
						"WI"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 1170436531u:
				if (Operators.CompareString(text, "Gulf States", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[5]
					{
						"TX",
						"LA",
						"MS",
						"AL",
						"FL"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			case 641632717u:
				if (Operators.CompareString(text, "Continental Pacific", false) == 0)
				{
					this.resetStateSelection();
					flag = true;
					array = new string[3]
					{
						"CA",
						"OR",
						"WA"
					};
					this.selectStates(ref array);
					break;
				}
				goto default;
			default:
				flag = false;
				break;
			}
			if (flag)
			{
				this.RegionName_TextBox.Text = text;
			}
		}

		private void MultistateForm_Load(object sender, EventArgs e)
		{
			this.ListBoxRegions.Items.Add("Census Bureau Regions and Divisions");
			this.ListBoxRegions.Items.Add("___________________________________");
			this.ListBoxRegions.Items.Add(this.sepMR + "Northeast");
			this.ListBoxRegions.Items.Add(this.sepSR + "New England");
			this.ListBoxRegions.Items.Add(this.sepSR + "Middle Atlantic");
			this.ListBoxRegions.Items.Add(this.sepMR + "Midwest");
			this.ListBoxRegions.Items.Add(this.sepSR + "East North Central");
			this.ListBoxRegions.Items.Add(this.sepSR + "West North Central");
			this.ListBoxRegions.Items.Add(this.sepMR + "South");
			this.ListBoxRegions.Items.Add(this.sepSR + "South Atlantic");
			this.ListBoxRegions.Items.Add(this.sepSR + "East South Central");
			this.ListBoxRegions.Items.Add(this.sepSR + "West South Central");
			this.ListBoxRegions.Items.Add(this.sepMR + "West");
			this.ListBoxRegions.Items.Add(this.sepSR + "Mountain");
			this.ListBoxRegions.Items.Add(this.sepSR + "Pacific");
			this.ListBoxRegions.Items.Add("___________________________________");
			this.ListBoxRegions.Items.Add("");
			this.ListBoxRegions.Items.Add("BEA Regions");
			this.ListBoxRegions.Items.Add(this.sepMR + "BEA New England");
			this.ListBoxRegions.Items.Add(this.sepMR + "Mideast");
			this.ListBoxRegions.Items.Add(this.sepMR + "Great Lakes");
			this.ListBoxRegions.Items.Add(this.sepMR + "Plains");
			this.ListBoxRegions.Items.Add(this.sepMR + "Southeast");
			this.ListBoxRegions.Items.Add(this.sepMR + "Southwest");
			this.ListBoxRegions.Items.Add(this.sepMR + "Rocky Mountain");
			this.ListBoxRegions.Items.Add(this.sepMR + "Far West");
			this.ListBoxRegions.Items.Add("___________________________________");
			this.ListBoxRegions.Items.Add("");
			this.ListBoxRegions.Items.Add("Other Regions");
			this.ListBoxRegions.Items.Add(this.sepMR + "United States");
			this.ListBoxRegions.Items.Add(this.sepMR + "Lower 48 States");
			this.ListBoxRegions.Items.Add(this.sepMR + "Gulf States");
			this.ListBoxRegions.Items.Add(this.sepMR + "Continental Pacific");
		}

		private void resetStateSelection()
		{
			this.RegionName_TextBox.Text = "";
			this.AK_cbox.Checked = false;
			this.AL_cbox.Checked = false;
			this.AR_cbox.Checked = false;
			this.AZ_cbox.Checked = false;
			this.CA_cbox.Checked = false;
			this.CO_cbox.Checked = false;
			this.CT_cbox.Checked = false;
			this.DE_cbox.Checked = false;
			this.DC_cbox.Checked = false;
			this.FL_cbox.Checked = false;
			this.GA_cbox.Checked = false;
			this.HI_cbox.Checked = false;
			this.IN_cbox.Checked = false;
			this.ID_cbox.Checked = false;
			this.IA_cbox.Checked = false;
			this.IL_cbox.Checked = false;
			this.KS_cbox.Checked = false;
			this.KY_cbox.Checked = false;
			this.LA_cbox.Checked = false;
			this.ME_cbox.Checked = false;
			this.MD_cbox.Checked = false;
			this.MA_cbox.Checked = false;
			this.MI_cbox.Checked = false;
			this.MN_cbox.Checked = false;
			this.MS_cbox.Checked = false;
			this.MO_cbox.Checked = false;
			this.MT_cbox.Checked = false;
			this.NY_cbox.Checked = false;
			this.NE_cbox.Checked = false;
			this.NV_cbox.Checked = false;
			this.NH_cbox.Checked = false;
			this.NJ_cbox.Checked = false;
			this.NM_cbox.Checked = false;
			this.NC_cbox.Checked = false;
			this.ND_cbox.Checked = false;
			this.OH_cbox.Checked = false;
			this.OK_cbox.Checked = false;
			this.OR_cbox.Checked = false;
			this.PA_cbox.Checked = false;
			this.RI_cbox.Checked = false;
			this.SC_cbox.Checked = false;
			this.SD_cbox.Checked = false;
			this.TN_cbox.Checked = false;
			this.TX_cbox.Checked = false;
			this.UT_cbox.Checked = false;
			this.VT_cbox.Checked = false;
			this.VA_cbox.Checked = false;
			this.WA_cbox.Checked = false;
			this.WV_cbox.Checked = false;
			this.WI_cbox.Checked = false;
			this.WY_cbox.Checked = false;
			this.AK_cbox.BackColor = this.baseColor;
			this.AL_cbox.BackColor = this.baseColor;
			this.AR_cbox.BackColor = this.baseColor;
			this.AZ_cbox.BackColor = this.baseColor;
			this.CA_cbox.BackColor = this.baseColor;
			this.CO_cbox.BackColor = this.baseColor;
			this.CT_cbox.BackColor = this.baseColor;
			this.DE_cbox.BackColor = this.baseColor;
			this.DC_cbox.BackColor = this.baseColor;
			this.FL_cbox.BackColor = this.baseColor;
			this.GA_cbox.BackColor = this.baseColor;
			this.HI_cbox.BackColor = this.baseColor;
			this.IN_cbox.BackColor = this.baseColor;
			this.ID_cbox.BackColor = this.baseColor;
			this.IA_cbox.BackColor = this.baseColor;
			this.IL_cbox.BackColor = this.baseColor;
			this.KS_cbox.BackColor = this.baseColor;
			this.KY_cbox.BackColor = this.baseColor;
			this.LA_cbox.BackColor = this.baseColor;
			this.ME_cbox.BackColor = this.baseColor;
			this.MD_cbox.BackColor = this.baseColor;
			this.MA_cbox.BackColor = this.baseColor;
			this.MI_cbox.BackColor = this.baseColor;
			this.MN_cbox.BackColor = this.baseColor;
			this.MS_cbox.BackColor = this.baseColor;
			this.MO_cbox.BackColor = this.baseColor;
			this.MT_cbox.BackColor = this.baseColor;
			this.NY_cbox.BackColor = this.baseColor;
			this.NE_cbox.BackColor = this.baseColor;
			this.NV_cbox.BackColor = this.baseColor;
			this.NH_cbox.BackColor = this.baseColor;
			this.NJ_cbox.BackColor = this.baseColor;
			this.NM_cbox.BackColor = this.baseColor;
			this.NC_cbox.BackColor = this.baseColor;
			this.ND_cbox.BackColor = this.baseColor;
			this.OH_cbox.BackColor = this.baseColor;
			this.OK_cbox.BackColor = this.baseColor;
			this.OR_cbox.BackColor = this.baseColor;
			this.PA_cbox.BackColor = this.baseColor;
			this.RI_cbox.BackColor = this.baseColor;
			this.SC_cbox.BackColor = this.baseColor;
			this.SD_cbox.BackColor = this.baseColor;
			this.TN_cbox.BackColor = this.baseColor;
			this.TX_cbox.BackColor = this.baseColor;
			this.UT_cbox.BackColor = this.baseColor;
			this.VT_cbox.BackColor = this.baseColor;
			this.VA_cbox.BackColor = this.baseColor;
			this.WA_cbox.BackColor = this.baseColor;
			this.WV_cbox.BackColor = this.baseColor;
			this.WI_cbox.BackColor = this.baseColor;
			this.WY_cbox.BackColor = this.baseColor;
		}

		private void selectStates(ref string[] stname)
		{
			string[] array = stname;
			foreach (string text in array)
			{
				switch (BEAData.ComputeStringHash(text))
				{
				case 736520472u:
					if (Operators.CompareString(text, "NY", false) == 0)
					{
						this.NY_cbox.Checked = true;
						this.NY_cbox.BackColor = this.scolor;
					}
					break;
				case 805308234u:
					if (Operators.CompareString(text, "PA", false) == 0)
					{
						this.PA_cbox.Checked = true;
						this.PA_cbox.BackColor = this.scolor;
					}
					break;
				case 1055295233u:
					if (Operators.CompareString(text, "NJ", false) == 0)
					{
						this.NJ_cbox.Checked = true;
						this.NJ_cbox.BackColor = this.scolor;
					}
					break;
				case 1205852519u:
					if (Operators.CompareString(text, "ME", false) == 0)
					{
						this.ME_cbox.Checked = true;
						this.ME_cbox.BackColor = this.scolor;
					}
					break;
				case 1021739995u:
					if (Operators.CompareString(text, "NH", false) == 0)
					{
						this.NH_cbox.Checked = true;
						this.NH_cbox.BackColor = this.scolor;
					}
					break;
				case 2029677063u:
					if (Operators.CompareString(text, "VT", false) == 0)
					{
						this.VT_cbox.Checked = true;
						this.VT_cbox.BackColor = this.scolor;
					}
					break;
				case 1138742043u:
					if (Operators.CompareString(text, "MA", false) == 0)
					{
						this.MA_cbox.Checked = true;
						this.MA_cbox.BackColor = this.scolor;
					}
					break;
				case 2214922420u:
					if (Operators.CompareString(text, "RI", false) == 0)
					{
						this.RI_cbox.Checked = true;
						this.RI_cbox.BackColor = this.scolor;
					}
					break;
				case 1792671826u:
					if (Operators.CompareString(text, "CT", false) == 0)
					{
						this.CT_cbox.Checked = true;
						this.CT_cbox.BackColor = this.scolor;
					}
					break;
				case 634721925u:
					if (Operators.CompareString(text, "AK", false) == 0)
					{
						this.AK_cbox.Checked = true;
						this.AK_cbox.BackColor = this.scolor;
					}
					break;
				case 2010780873u:
					if (Operators.CompareString(text, "CA", false) == 0)
					{
						this.CA_cbox.Checked = true;
						this.CA_cbox.BackColor = this.scolor;
					}
					break;
				case 2280355610u:
					if (Operators.CompareString(text, "HI", false) == 0)
					{
						this.HI_cbox.Checked = true;
						this.HI_cbox.BackColor = this.scolor;
					}
					break;
				case 719742853u:
					if (Operators.CompareString(text, "NV", false) == 0)
					{
						this.NV_cbox.Checked = true;
						this.NV_cbox.BackColor = this.scolor;
					}
					break;
				case 2095360516u:
					if (Operators.CompareString(text, "OR", false) == 0)
					{
						this.OR_cbox.Checked = true;
						this.OR_cbox.BackColor = this.scolor;
					}
					break;
				case 1073205685u:
					if (Operators.CompareString(text, "WA", false) == 0)
					{
						this.WA_cbox.Checked = true;
						this.WA_cbox.BackColor = this.scolor;
					}
					break;
				case 821394305u:
					if (Operators.CompareString(text, "TX", false) == 0)
					{
						this.TX_cbox.Checked = true;
						this.TX_cbox.BackColor = this.scolor;
					}
					break;
				case 1742883422u:
					if (Operators.CompareString(text, "LA", false) == 0)
					{
						this.LA_cbox.Checked = true;
						this.LA_cbox.BackColor = this.scolor;
					}
					break;
				case 903855377u:
					if (Operators.CompareString(text, "MS", false) == 0)
					{
						this.MS_cbox.Checked = true;
						this.MS_cbox.BackColor = this.scolor;
					}
					break;
				case 517278592u:
					if (Operators.CompareString(text, "AL", false) == 0)
					{
						this.AL_cbox.Checked = true;
						this.AL_cbox.BackColor = this.scolor;
					}
					break;
				case 2161338159u:
					if (Operators.CompareString(text, "FL", false) == 0)
					{
						this.FL_cbox.Checked = true;
						this.FL_cbox.BackColor = this.scolor;
					}
					break;
				case 1020607162u:
					if (Operators.CompareString(text, "AR", false) == 0)
					{
						this.AR_cbox.Checked = true;
						this.AR_cbox.BackColor = this.scolor;
					}
					break;
				case 2246359087u:
					if (Operators.CompareString(text, "OK", false) == 0)
					{
						this.OK_cbox.Checked = true;
						this.OK_cbox.BackColor = this.scolor;
					}
					break;
				case 804175401u:
					if (Operators.CompareString(text, "KY", false) == 0)
					{
						this.KY_cbox.Checked = true;
						this.KY_cbox.BackColor = this.scolor;
					}
					break;
				case 586507639u:
					if (Operators.CompareString(text, "TN", false) == 0)
					{
						this.TN_cbox.Checked = true;
						this.TN_cbox.BackColor = this.scolor;
					}
					break;
				case 1607529637u:
					if (Operators.CompareString(text, "GA", false) == 0)
					{
						this.GA_cbox.Checked = true;
						this.GA_cbox.BackColor = this.scolor;
					}
					break;
				case 1443004851u:
					if (Operators.CompareString(text, "SC", false) == 0)
					{
						this.SC_cbox.Checked = true;
						this.SC_cbox.BackColor = this.scolor;
					}
					break;
				case 904296662u:
					if (Operators.CompareString(text, "NC", false) == 0)
					{
						this.NC_cbox.Checked = true;
						this.NC_cbox.BackColor = this.scolor;
					}
					break;
				case 1677347064u:
					if (Operators.CompareString(text, "VA", false) == 0)
					{
						this.VA_cbox.Checked = true;
						this.VA_cbox.BackColor = this.scolor;
					}
					break;
				case 687320448u:
					if (Operators.CompareString(text, "WV", false) == 0)
					{
						this.WV_cbox.Checked = true;
						this.WV_cbox.BackColor = this.scolor;
					}
					break;
				case 1189074900u:
					if (Operators.CompareString(text, "MD", false) == 0)
					{
						this.MD_cbox.Checked = true;
						this.MD_cbox.BackColor = this.scolor;
					}
					break;
				case 902722544u:
					if (Operators.CompareString(text, "DC", false) == 0)
					{
						this.DC_cbox.Checked = true;
						this.DC_cbox.BackColor = this.scolor;
					}
					break;
				case 1003388258u:
					if (Operators.CompareString(text, "DE", false) == 0)
					{
						this.DE_cbox.Checked = true;
						this.DE_cbox.BackColor = this.scolor;
					}
					break;
				case 886386210u:
					if (Operators.CompareString(text, "AZ", false) == 0)
					{
						this.AZ_cbox.Checked = true;
						this.AZ_cbox.BackColor = this.scolor;
					}
					break;
				case 1072072852u:
					if (Operators.CompareString(text, "NM", false) == 0)
					{
						this.NM_cbox.Checked = true;
						this.NM_cbox.BackColor = this.scolor;
					}
					break;
				case 1727238636u:
					if (Operators.CompareString(text, "UT", false) == 0)
					{
						this.UT_cbox.Checked = true;
						this.UT_cbox.BackColor = this.scolor;
					}
					break;
				case 2178557063u:
					if (Operators.CompareString(text, "CO", false) == 0)
					{
						this.CO_cbox.Checked = true;
						this.CO_cbox.BackColor = this.scolor;
					}
					break;
				case 938984733u:
					if (Operators.CompareString(text, "WY", false) == 0)
					{
						this.WY_cbox.Checked = true;
						this.WY_cbox.BackColor = this.scolor;
					}
					break;
				case 1458105184u:
					if (Operators.CompareString(text, "ID", false) == 0)
					{
						this.ID_cbox.Checked = true;
						this.ID_cbox.BackColor = this.scolor;
					}
					break;
				case 920632996u:
					if (Operators.CompareString(text, "MT", false) == 0)
					{
						this.MT_cbox.Checked = true;
						this.MT_cbox.BackColor = this.scolor;
					}
					break;
				case 1592326136u:
					if (Operators.CompareString(text, "IL", false) == 0)
					{
						this.IL_cbox.Checked = true;
						this.IL_cbox.BackColor = this.scolor;
					}
					break;
				case 1625881374u:
					if (Operators.CompareString(text, "IN", false) == 0)
					{
						this.IN_cbox.Checked = true;
						this.IN_cbox.BackColor = this.scolor;
					}
					break;
				case 2263136706u:
					if (Operators.CompareString(text, "OH", false) == 0)
					{
						this.OH_cbox.Checked = true;
						this.OH_cbox.BackColor = this.scolor;
					}
					break;
				case 1207426637u:
					if (Operators.CompareString(text, "WI", false) == 0)
					{
						this.WI_cbox.Checked = true;
						this.WI_cbox.BackColor = this.scolor;
					}
					break;
				case 1004521091u:
					if (Operators.CompareString(text, "MI", false) == 0)
					{
						this.MI_cbox.Checked = true;
						this.MI_cbox.BackColor = this.scolor;
					}
					break;
				case 954629519u:
					if (Operators.CompareString(text, "ND", false) == 0)
					{
						this.ND_cbox.Checked = true;
						this.ND_cbox.BackColor = this.scolor;
					}
					break;
				case 1088409186u:
					if (Operators.CompareString(text, "MN", false) == 0)
					{
						this.MN_cbox.Checked = true;
						this.MN_cbox.BackColor = this.scolor;
					}
					break;
				case 1526892946u:
					if (Operators.CompareString(text, "SD", false) == 0)
					{
						this.SD_cbox.Checked = true;
						this.SD_cbox.BackColor = this.scolor;
					}
					break;
				case 937851900u:
					if (Operators.CompareString(text, "NE", false) == 0)
					{
						this.NE_cbox.Checked = true;
						this.NE_cbox.BackColor = this.scolor;
					}
					break;
				case 1541993279u:
					if (Operators.CompareString(text, "IA", false) == 0)
					{
						this.IA_cbox.Checked = true;
						this.IA_cbox.BackColor = this.scolor;
					}
					break;
				case 904841115u:
					if (Operators.CompareString(text, "KS", false) == 0)
					{
						this.KS_cbox.Checked = true;
						this.KS_cbox.BackColor = this.scolor;
					}
					break;
				case 1105186805u:
					if (Operators.CompareString(text, "MO", false) == 0)
					{
						this.MO_cbox.Checked = true;
						this.MO_cbox.BackColor = this.scolor;
					}
					break;
				}
			}
		}

		private void ResetButton_Click(object sender, EventArgs e)
		{
			this.resetStateSelection();
		}

		private void AK_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.AK_cbox.Checked)
			{
				this.AK_cbox.BackColor = this.scolor;
			}
			else
			{
				this.AK_cbox.BackColor = this.baseColor;
			}
		}

		private void AL_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.AL_cbox.Checked)
			{
				this.AL_cbox.BackColor = this.scolor;
			}
			else
			{
				this.AL_cbox.BackColor = this.baseColor;
			}
		}

		private void AZ_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.AZ_cbox.Checked)
			{
				this.AZ_cbox.BackColor = this.scolor;
			}
			else
			{
				this.AZ_cbox.BackColor = this.baseColor;
			}
		}

		private void AR_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.AR_cbox.Checked)
			{
				this.AR_cbox.BackColor = this.scolor;
			}
			else
			{
				this.AR_cbox.BackColor = this.baseColor;
			}
		}

		private void CA_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.CA_cbox.Checked)
			{
				this.CA_cbox.BackColor = this.scolor;
			}
			else
			{
				this.CA_cbox.BackColor = this.baseColor;
			}
		}

		private void CO_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.CO_cbox.Checked)
			{
				this.CO_cbox.BackColor = this.scolor;
			}
			else
			{
				this.CO_cbox.BackColor = this.baseColor;
			}
		}

		private void CT_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.CT_cbox.Checked)
			{
				this.CT_cbox.BackColor = this.scolor;
			}
			else
			{
				this.CT_cbox.BackColor = this.baseColor;
			}
		}

		private void DE_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.DE_cbox.Checked)
			{
				this.DE_cbox.BackColor = this.scolor;
			}
			else
			{
				this.DE_cbox.BackColor = this.baseColor;
			}
		}

		private void DC_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.DC_cbox.Checked)
			{
				this.DC_cbox.BackColor = this.scolor;
			}
			else
			{
				this.DC_cbox.BackColor = this.baseColor;
			}
		}

		private void FL_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.FL_cbox.Checked)
			{
				this.FL_cbox.BackColor = this.scolor;
			}
			else
			{
				this.FL_cbox.BackColor = this.baseColor;
			}
		}

		private void GA_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.GA_cbox.Checked)
			{
				this.GA_cbox.BackColor = this.scolor;
			}
			else
			{
				this.GA_cbox.BackColor = this.baseColor;
			}
		}

		private void HI_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.HI_cbox.Checked)
			{
				this.HI_cbox.BackColor = this.scolor;
			}
			else
			{
				this.HI_cbox.BackColor = this.baseColor;
			}
		}

		private void IA_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.IA_cbox.Checked)
			{
				this.IA_cbox.BackColor = this.scolor;
			}
			else
			{
				this.IA_cbox.BackColor = this.baseColor;
			}
		}

		private void ID_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.ID_cbox.Checked)
			{
				this.ID_cbox.BackColor = this.scolor;
			}
			else
			{
				this.ID_cbox.BackColor = this.baseColor;
			}
		}

		private void IL_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.IL_cbox.Checked)
			{
				this.IL_cbox.BackColor = this.scolor;
			}
			else
			{
				this.IL_cbox.BackColor = this.baseColor;
			}
		}

		private void IN_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.IN_cbox.Checked)
			{
				this.IN_cbox.BackColor = this.scolor;
			}
			else
			{
				this.IN_cbox.BackColor = this.baseColor;
			}
		}

		private void KS_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.KS_cbox.Checked)
			{
				this.KS_cbox.BackColor = this.scolor;
			}
			else
			{
				this.KS_cbox.BackColor = this.baseColor;
			}
		}

		private void KY_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.KY_cbox.Checked)
			{
				this.KY_cbox.BackColor = this.scolor;
			}
			else
			{
				this.KY_cbox.BackColor = this.baseColor;
			}
		}

		private void LA_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.LA_cbox.Checked)
			{
				this.LA_cbox.BackColor = this.scolor;
			}
			else
			{
				this.LA_cbox.BackColor = this.baseColor;
			}
		}

		private void MA_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.MA_cbox.Checked)
			{
				this.MA_cbox.BackColor = this.scolor;
			}
			else
			{
				this.MA_cbox.BackColor = this.baseColor;
			}
		}

		private void MI_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.MI_cbox.Checked)
			{
				this.MI_cbox.BackColor = this.scolor;
			}
			else
			{
				this.MI_cbox.BackColor = this.baseColor;
			}
		}

		private void ME_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.ME_cbox.Checked)
			{
				this.ME_cbox.BackColor = this.scolor;
			}
			else
			{
				this.ME_cbox.BackColor = this.baseColor;
			}
		}

		private void MD_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.MD_cbox.Checked)
			{
				this.MD_cbox.BackColor = this.scolor;
			}
			else
			{
				this.MD_cbox.BackColor = this.baseColor;
			}
		}

		private void MS_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.MS_cbox.Checked)
			{
				this.MS_cbox.BackColor = this.scolor;
			}
			else
			{
				this.MS_cbox.BackColor = this.baseColor;
			}
		}

		private void MO_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.MO_cbox.Checked)
			{
				this.MO_cbox.BackColor = this.scolor;
			}
			else
			{
				this.MO_cbox.BackColor = this.baseColor;
			}
		}

		private void MN_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.MN_cbox.Checked)
			{
				this.MN_cbox.BackColor = this.scolor;
			}
			else
			{
				this.MN_cbox.BackColor = this.baseColor;
			}
		}

		private void MT_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.MT_cbox.Checked)
			{
				this.MT_cbox.BackColor = this.scolor;
			}
			else
			{
				this.MT_cbox.BackColor = this.baseColor;
			}
		}

		private void NC_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.NC_cbox.Checked)
			{
				this.NC_cbox.BackColor = this.scolor;
			}
			else
			{
				this.NC_cbox.BackColor = this.baseColor;
			}
		}

		private void ND_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.ND_cbox.Checked)
			{
				this.ND_cbox.BackColor = this.scolor;
			}
			else
			{
				this.ND_cbox.BackColor = this.baseColor;
			}
		}

		private void NE_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.NE_cbox.Checked)
			{
				this.NE_cbox.BackColor = this.scolor;
			}
			else
			{
				this.NE_cbox.BackColor = this.baseColor;
			}
		}

		private void NJ_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.NJ_cbox.Checked)
			{
				this.NJ_cbox.BackColor = this.scolor;
			}
			else
			{
				this.NJ_cbox.BackColor = this.baseColor;
			}
		}

		private void NH_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.NH_cbox.Checked)
			{
				this.NH_cbox.BackColor = this.scolor;
			}
			else
			{
				this.NH_cbox.BackColor = this.baseColor;
			}
		}

		private void NY_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.NY_cbox.Checked)
			{
				this.NY_cbox.BackColor = this.scolor;
			}
			else
			{
				this.NY_cbox.BackColor = this.baseColor;
			}
		}

		private void NV_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.NV_cbox.Checked)
			{
				this.NV_cbox.BackColor = this.scolor;
			}
			else
			{
				this.NV_cbox.BackColor = this.baseColor;
			}
		}

		private void NM_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.NM_cbox.Checked)
			{
				this.NM_cbox.BackColor = this.scolor;
			}
			else
			{
				this.NM_cbox.BackColor = this.baseColor;
			}
		}

		private void OH_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.OH_cbox.Checked)
			{
				this.OH_cbox.BackColor = this.scolor;
			}
			else
			{
				this.OH_cbox.BackColor = this.baseColor;
			}
		}

		private void OK_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.OK_cbox.Checked)
			{
				this.OK_cbox.BackColor = this.scolor;
			}
			else
			{
				this.OK_cbox.BackColor = this.baseColor;
			}
		}

		private void OR_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.OR_cbox.Checked)
			{
				this.OR_cbox.BackColor = this.scolor;
			}
			else
			{
				this.OR_cbox.BackColor = this.baseColor;
			}
		}

		private void PA_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.PA_cbox.Checked)
			{
				this.PA_cbox.BackColor = this.scolor;
			}
			else
			{
				this.PA_cbox.BackColor = this.baseColor;
			}
		}

		private void RI_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.RI_cbox.Checked)
			{
				this.RI_cbox.BackColor = this.scolor;
			}
			else
			{
				this.RI_cbox.BackColor = this.baseColor;
			}
		}

		private void SC_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.SC_cbox.Checked)
			{
				this.SC_cbox.BackColor = this.scolor;
			}
			else
			{
				this.SC_cbox.BackColor = this.baseColor;
			}
		}

		private void SD_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.SD_cbox.Checked)
			{
				this.SD_cbox.BackColor = this.scolor;
			}
			else
			{
				this.SD_cbox.BackColor = this.baseColor;
			}
		}

		private void TN_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.TN_cbox.Checked)
			{
				this.TN_cbox.BackColor = this.scolor;
			}
			else
			{
				this.TN_cbox.BackColor = this.baseColor;
			}
		}

		private void TX_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.TX_cbox.Checked)
			{
				this.TX_cbox.BackColor = this.scolor;
			}
			else
			{
				this.TX_cbox.BackColor = this.baseColor;
			}
		}

		private void UT_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.UT_cbox.Checked)
			{
				this.UT_cbox.BackColor = this.scolor;
			}
			else
			{
				this.UT_cbox.BackColor = this.baseColor;
			}
		}

		private void VA_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.VA_cbox.Checked)
			{
				this.VA_cbox.BackColor = this.scolor;
			}
			else
			{
				this.VA_cbox.BackColor = this.baseColor;
			}
		}

		private void VT_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.VT_cbox.Checked)
			{
				this.VT_cbox.BackColor = this.scolor;
			}
			else
			{
				this.VT_cbox.BackColor = this.baseColor;
			}
		}

		private void WA_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.WA_cbox.Checked)
			{
				this.WA_cbox.BackColor = this.scolor;
			}
			else
			{
				this.WA_cbox.BackColor = this.baseColor;
			}
		}

		private void WI_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.WI_cbox.Checked)
			{
				this.WI_cbox.BackColor = this.scolor;
			}
			else
			{
				this.WI_cbox.BackColor = this.baseColor;
			}
		}

		private void WV_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.WV_cbox.Checked)
			{
				this.WV_cbox.BackColor = this.scolor;
			}
			else
			{
				this.WV_cbox.BackColor = this.baseColor;
			}
		}

		private void WY_cbox_CheckedChanged(object sender, EventArgs e)
		{
			if (this.WY_cbox.Checked)
			{
				this.WY_cbox.BackColor = this.scolor;
			}
			else
			{
				this.WY_cbox.BackColor = this.baseColor;
			}
		}

		private void OKButton_Click(object sender, EventArgs e)
		{
			if (this.RegionName_TextBox.TextLength > 0)
			{
				bool flag = false;
				RegionalData regStateData = this.beadat.RegStateData;
				string text = "AL";
				StateData stateData = new StateData("Multistate Region", regStateData.GetState(ref text).nrec_base);
				stateData.StateSetup(ref this.beadat.NTables.FTE_Ratio);
				stateData.setEmptyState();
				MyProject.Forms.frmMain.AL_cboxChecked = false;
				MyProject.Forms.frmMain.AK_cboxChecked = false;
				MyProject.Forms.frmMain.AR_cboxChecked = false;
				MyProject.Forms.frmMain.AZ_cboxChecked = false;
				MyProject.Forms.frmMain.CA_cboxChecked = false;
				MyProject.Forms.frmMain.CO_cboxChecked = false;
				MyProject.Forms.frmMain.CT_cboxChecked = false;
				MyProject.Forms.frmMain.DC_cboxChecked = false;
				MyProject.Forms.frmMain.DE_cboxChecked = false;
				MyProject.Forms.frmMain.FL_cboxChecked = false;
				MyProject.Forms.frmMain.GA_cboxChecked = false;
				MyProject.Forms.frmMain.IA_cboxChecked = false;
				MyProject.Forms.frmMain.ID_cboxChecked = false;
				MyProject.Forms.frmMain.IL_cboxChecked = false;
				MyProject.Forms.frmMain.IN_cboxChecked = false;
				MyProject.Forms.frmMain.KS_cboxChecked = false;
				MyProject.Forms.frmMain.KY_cboxChecked = false;
				MyProject.Forms.frmMain.LA_cboxChecked = false;
				MyProject.Forms.frmMain.MA_cboxChecked = false;
				MyProject.Forms.frmMain.ME_cboxChecked = false;
				MyProject.Forms.frmMain.MI_cboxChecked = false;
				MyProject.Forms.frmMain.MN_cboxChecked = false;
				MyProject.Forms.frmMain.MD_cboxChecked = false;
				MyProject.Forms.frmMain.MO_cboxChecked = false;
				MyProject.Forms.frmMain.MS_cboxChecked = false;
				MyProject.Forms.frmMain.MT_cboxChecked = false;
				MyProject.Forms.frmMain.NE_cboxChecked = false;
				MyProject.Forms.frmMain.ND_cboxChecked = false;
				MyProject.Forms.frmMain.NC_cboxChecked = false;
				MyProject.Forms.frmMain.NH_cboxChecked = false;
				MyProject.Forms.frmMain.NJ_cboxChecked = false;
				MyProject.Forms.frmMain.NM_cboxChecked = false;
				MyProject.Forms.frmMain.NY_cboxChecked = false;
				MyProject.Forms.frmMain.NV_cboxChecked = false;
				MyProject.Forms.frmMain.HI_cboxChecked = false;
				MyProject.Forms.frmMain.OH_cboxChecked = false;
				MyProject.Forms.frmMain.OK_cboxChecked = false;
				MyProject.Forms.frmMain.OR_cboxChecked = false;
				MyProject.Forms.frmMain.PA_cboxChecked = false;
				MyProject.Forms.frmMain.RI_cboxChecked = false;
				MyProject.Forms.frmMain.SC_cboxChecked = false;
				MyProject.Forms.frmMain.SD_cboxChecked = false;
				MyProject.Forms.frmMain.TN_cboxChecked = false;
				MyProject.Forms.frmMain.TX_cboxChecked = false;
				MyProject.Forms.frmMain.UT_cboxChecked = false;
				MyProject.Forms.frmMain.VA_cboxChecked = false;
				MyProject.Forms.frmMain.VT_cboxChecked = false;
				MyProject.Forms.frmMain.WA_cboxChecked = false;
				MyProject.Forms.frmMain.WI_cboxChecked = false;
				MyProject.Forms.frmMain.WV_cboxChecked = false;
				MyProject.Forms.frmMain.WY_cboxChecked = false;
				if (this.AL_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.AL_cboxChecked = true;
					RegionalData regStateData2 = this.beadat.RegStateData;
					text = "AL";
					StateData state = regStateData2.GetState(ref text);
					stateData.addState(ref state);
				}
				if (this.AK_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.AK_cboxChecked = true;
					RegionalData regStateData3 = this.beadat.RegStateData;
					text = "AK";
					StateData state2 = regStateData3.GetState(ref text);
					stateData.addState(ref state2);
				}
				if (this.AR_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.AR_cboxChecked = true;
					RegionalData regStateData4 = this.beadat.RegStateData;
					text = "AR";
					StateData state3 = regStateData4.GetState(ref text);
					stateData.addState(ref state3);
				}
				if (this.AZ_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.AZ_cboxChecked = true;
					RegionalData regStateData5 = this.beadat.RegStateData;
					text = "AZ";
					StateData state4 = regStateData5.GetState(ref text);
					stateData.addState(ref state4);
				}
				if (this.CA_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.CA_cboxChecked = true;
					RegionalData regStateData6 = this.beadat.RegStateData;
					text = "CA";
					StateData state5 = regStateData6.GetState(ref text);
					stateData.addState(ref state5);
				}
				if (this.CO_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.CO_cboxChecked = true;
					RegionalData regStateData7 = this.beadat.RegStateData;
					text = "CO";
					StateData state6 = regStateData7.GetState(ref text);
					stateData.addState(ref state6);
				}
				if (this.CT_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.CT_cboxChecked = true;
					RegionalData regStateData8 = this.beadat.RegStateData;
					text = "CT";
					StateData state7 = regStateData8.GetState(ref text);
					stateData.addState(ref state7);
				}
				if (this.DC_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.DC_cboxChecked = true;
					RegionalData regStateData9 = this.beadat.RegStateData;
					text = "DC";
					StateData state8 = regStateData9.GetState(ref text);
					stateData.addState(ref state8);
				}
				if (this.DE_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.DE_cboxChecked = true;
					RegionalData regStateData10 = this.beadat.RegStateData;
					text = "DE";
					StateData state9 = regStateData10.GetState(ref text);
					stateData.addState(ref state9);
				}
				if (this.FL_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.FL_cboxChecked = true;
					RegionalData regStateData11 = this.beadat.RegStateData;
					text = "FL";
					StateData state10 = regStateData11.GetState(ref text);
					stateData.addState(ref state10);
				}
				if (this.GA_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.GA_cboxChecked = true;
					RegionalData regStateData12 = this.beadat.RegStateData;
					text = "GA";
					StateData state11 = regStateData12.GetState(ref text);
					stateData.addState(ref state11);
				}
				if (this.IA_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.IA_cboxChecked = true;
					RegionalData regStateData13 = this.beadat.RegStateData;
					text = "IA";
					StateData state12 = regStateData13.GetState(ref text);
					stateData.addState(ref state12);
				}
				if (this.ID_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.ID_cboxChecked = true;
					RegionalData regStateData14 = this.beadat.RegStateData;
					text = "ID";
					StateData state13 = regStateData14.GetState(ref text);
					stateData.addState(ref state13);
				}
				if (this.IL_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.IL_cboxChecked = true;
					RegionalData regStateData15 = this.beadat.RegStateData;
					text = "IL";
					StateData state14 = regStateData15.GetState(ref text);
					stateData.addState(ref state14);
				}
				if (this.IN_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.IN_cboxChecked = true;
					RegionalData regStateData16 = this.beadat.RegStateData;
					text = "IN";
					StateData state15 = regStateData16.GetState(ref text);
					stateData.addState(ref state15);
				}
				if (this.KS_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.KS_cboxChecked = true;
					RegionalData regStateData17 = this.beadat.RegStateData;
					text = "KS";
					StateData state16 = regStateData17.GetState(ref text);
					stateData.addState(ref state16);
				}
				if (this.KY_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.KY_cboxChecked = true;
					RegionalData regStateData18 = this.beadat.RegStateData;
					text = "KY";
					StateData state17 = regStateData18.GetState(ref text);
					stateData.addState(ref state17);
				}
				if (this.LA_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.LA_cboxChecked = true;
					RegionalData regStateData19 = this.beadat.RegStateData;
					text = "LA";
					StateData state18 = regStateData19.GetState(ref text);
					stateData.addState(ref state18);
				}
				if (this.MA_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.MA_cboxChecked = true;
					RegionalData regStateData20 = this.beadat.RegStateData;
					text = "MA";
					StateData state19 = regStateData20.GetState(ref text);
					stateData.addState(ref state19);
				}
				if (this.ME_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.ME_cboxChecked = true;
					RegionalData regStateData21 = this.beadat.RegStateData;
					text = "ME";
					StateData state20 = regStateData21.GetState(ref text);
					stateData.addState(ref state20);
				}
				if (this.MI_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.MI_cboxChecked = true;
					RegionalData regStateData22 = this.beadat.RegStateData;
					text = "MI";
					StateData state21 = regStateData22.GetState(ref text);
					stateData.addState(ref state21);
				}
				if (this.MN_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.MN_cboxChecked = true;
					RegionalData regStateData23 = this.beadat.RegStateData;
					text = "MN";
					StateData state22 = regStateData23.GetState(ref text);
					stateData.addState(ref state22);
				}
				if (this.MD_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.MD_cboxChecked = true;
					RegionalData regStateData24 = this.beadat.RegStateData;
					text = "MD";
					StateData state23 = regStateData24.GetState(ref text);
					stateData.addState(ref state23);
				}
				if (this.MO_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.MO_cboxChecked = true;
					RegionalData regStateData25 = this.beadat.RegStateData;
					text = "MO";
					StateData state24 = regStateData25.GetState(ref text);
					stateData.addState(ref state24);
				}
				if (this.MS_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.MS_cboxChecked = true;
					RegionalData regStateData26 = this.beadat.RegStateData;
					text = "MS";
					StateData state25 = regStateData26.GetState(ref text);
					stateData.addState(ref state25);
				}
				if (this.MT_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.MT_cboxChecked = true;
					RegionalData regStateData27 = this.beadat.RegStateData;
					text = "MT";
					StateData state26 = regStateData27.GetState(ref text);
					stateData.addState(ref state26);
				}
				if (this.NE_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.NE_cboxChecked = true;
					RegionalData regStateData28 = this.beadat.RegStateData;
					text = "NE";
					StateData state27 = regStateData28.GetState(ref text);
					stateData.addState(ref state27);
				}
				if (this.ND_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.ND_cboxChecked = true;
					RegionalData regStateData29 = this.beadat.RegStateData;
					text = "ND";
					StateData state28 = regStateData29.GetState(ref text);
					stateData.addState(ref state28);
				}
				if (this.NC_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.NC_cboxChecked = true;
					RegionalData regStateData30 = this.beadat.RegStateData;
					text = "NC";
					StateData state29 = regStateData30.GetState(ref text);
					stateData.addState(ref state29);
				}
				if (this.NH_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.NH_cboxChecked = true;
					RegionalData regStateData31 = this.beadat.RegStateData;
					text = "NH";
					StateData state30 = regStateData31.GetState(ref text);
					stateData.addState(ref state30);
				}
				if (this.NJ_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.NJ_cboxChecked = true;
					RegionalData regStateData32 = this.beadat.RegStateData;
					text = "NJ";
					StateData state31 = regStateData32.GetState(ref text);
					stateData.addState(ref state31);
				}
				if (this.NM_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.NM_cboxChecked = true;
					RegionalData regStateData33 = this.beadat.RegStateData;
					text = "NM";
					StateData state32 = regStateData33.GetState(ref text);
					stateData.addState(ref state32);
				}
				if (this.NY_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.NY_cboxChecked = true;
					RegionalData regStateData34 = this.beadat.RegStateData;
					text = "NY";
					StateData state33 = regStateData34.GetState(ref text);
					stateData.addState(ref state33);
				}
				if (this.NV_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.NV_cboxChecked = true;
					RegionalData regStateData35 = this.beadat.RegStateData;
					text = "NV";
					StateData state34 = regStateData35.GetState(ref text);
					stateData.addState(ref state34);
				}
				if (this.HI_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.HI_cboxChecked = true;
					RegionalData regStateData36 = this.beadat.RegStateData;
					text = "HI";
					StateData state35 = regStateData36.GetState(ref text);
					stateData.addState(ref state35);
				}
				if (this.OH_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.OH_cboxChecked = true;
					RegionalData regStateData37 = this.beadat.RegStateData;
					text = "OH";
					StateData state36 = regStateData37.GetState(ref text);
					stateData.addState(ref state36);
				}
				if (this.OK_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.OK_cboxChecked = true;
					RegionalData regStateData38 = this.beadat.RegStateData;
					text = "OK";
					StateData state37 = regStateData38.GetState(ref text);
					stateData.addState(ref state37);
				}
				if (this.OR_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.OR_cboxChecked = true;
					RegionalData regStateData39 = this.beadat.RegStateData;
					text = "OR";
					StateData state38 = regStateData39.GetState(ref text);
					stateData.addState(ref state38);
				}
				if (this.PA_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.PA_cboxChecked = true;
					RegionalData regStateData40 = this.beadat.RegStateData;
					text = "PA";
					StateData state39 = regStateData40.GetState(ref text);
					stateData.addState(ref state39);
				}
				if (this.RI_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.RI_cboxChecked = true;
					RegionalData regStateData41 = this.beadat.RegStateData;
					text = "RI";
					StateData state40 = regStateData41.GetState(ref text);
					stateData.addState(ref state40);
				}
				if (this.SC_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.SC_cboxChecked = true;
					RegionalData regStateData42 = this.beadat.RegStateData;
					text = "SC";
					StateData state41 = regStateData42.GetState(ref text);
					stateData.addState(ref state41);
				}
				if (this.SD_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.SD_cboxChecked = true;
					RegionalData regStateData43 = this.beadat.RegStateData;
					text = "SD";
					StateData state42 = regStateData43.GetState(ref text);
					stateData.addState(ref state42);
				}
				if (this.TN_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.TN_cboxChecked = true;
					RegionalData regStateData44 = this.beadat.RegStateData;
					text = "TN";
					StateData state43 = regStateData44.GetState(ref text);
					stateData.addState(ref state43);
				}
				if (this.TX_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.TX_cboxChecked = true;
					RegionalData regStateData45 = this.beadat.RegStateData;
					text = "TX";
					StateData state44 = regStateData45.GetState(ref text);
					stateData.addState(ref state44);
				}
				if (this.UT_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.UT_cboxChecked = true;
					RegionalData regStateData46 = this.beadat.RegStateData;
					text = "UT";
					StateData state45 = regStateData46.GetState(ref text);
					stateData.addState(ref state45);
				}
				if (this.VA_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.VA_cboxChecked = true;
					RegionalData regStateData47 = this.beadat.RegStateData;
					text = "VA";
					StateData state46 = regStateData47.GetState(ref text);
					stateData.addState(ref state46);
				}
				if (this.VT_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.VT_cboxChecked = true;
					RegionalData regStateData48 = this.beadat.RegStateData;
					text = "VT";
					StateData state47 = regStateData48.GetState(ref text);
					stateData.addState(ref state47);
				}
				if (this.WA_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.WA_cboxChecked = true;
					RegionalData regStateData49 = this.beadat.RegStateData;
					text = "WA";
					StateData state48 = regStateData49.GetState(ref text);
					stateData.addState(ref state48);
				}
				if (this.WI_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.WI_cboxChecked = true;
					RegionalData regStateData50 = this.beadat.RegStateData;
					text = "WI";
					StateData state49 = regStateData50.GetState(ref text);
					stateData.addState(ref state49);
				}
				if (this.WV_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.WV_cboxChecked = true;
					RegionalData regStateData51 = this.beadat.RegStateData;
					text = "WV";
					StateData state50 = regStateData51.GetState(ref text);
					stateData.addState(ref state50);
				}
				if (this.WY_cbox.Checked)
				{
					flag = true;
					MyProject.Forms.frmMain.WY_cboxChecked = true;
					RegionalData regStateData52 = this.beadat.RegStateData;
					text = "WY";
					StateData state51 = regStateData52.GetState(ref text);
					stateData.addState(ref state51);
				}
				if (flag)
				{
					this.beadat.worksum.regby = this.RegionName_TextBox.Text;
					if (Operators.CompareString(this.beadat.worksum.EmploymentStatus, "FTE", false) == 0)
					{
						stateData.scaleEmploymentByFTE(this.beadat.NTables.FTE_Ratio);
					}
					this.beadat.RegionSelected = stateData;
					base.DialogResult = DialogResult.OK;
					base.Close();
				}
			}
			else
			{
				this.RegionName_TextBox.Focus();
			}
		}

		private void GroupBox2_Enter(object sender, EventArgs e)
		{
		}
	}
}
