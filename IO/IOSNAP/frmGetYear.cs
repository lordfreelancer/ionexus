using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class frmGetYear : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Cancel_Button")]
		private Button _Cancel_Button;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cbYear")]
		private ComboBox _cbYear;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		private string PSelect;

		internal virtual Button Cancel_Button
		{
			[CompilerGenerated]
			get
			{
				return this._Cancel_Button;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Cancel_Button_Click;
				Button cancel_Button = this._Cancel_Button;
				if (cancel_Button != null)
				{
					cancel_Button.Click -= value2;
				}
				this._Cancel_Button = value;
				cancel_Button = this._Cancel_Button;
				if (cancel_Button != null)
				{
					cancel_Button.Click += value2;
				}
			}
		}

		internal virtual ComboBox cbYear
		{
			[CompilerGenerated]
			get
			{
				return this._cbYear;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.cbYear_SelectedValueChanged;
				ComboBox cbYear = this._cbYear;
				if (cbYear != null)
				{
					cbYear.SelectedValueChanged -= value2;
				}
				this._cbYear = value;
				cbYear = this._cbYear;
				if (cbYear != null)
				{
					cbYear.SelectedValueChanged += value2;
				}
			}
		}

		internal virtual Label Label1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		public frmGetYear()
		{
			base.Load += this.GetYear_Load;
			this.PSelect = "Please Select ...";
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.Cancel_Button = new Button();
			this.cbYear = new ComboBox();
			this.Label1 = new Label();
			base.SuspendLayout();
			this.Cancel_Button.DialogResult = DialogResult.Cancel;
			this.Cancel_Button.Font = new Font("Microsoft Sans Serif", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Cancel_Button.Location = new Point(254, 86);
			this.Cancel_Button.Margin = new Padding(4, 5, 4, 5);
			this.Cancel_Button.Name = "Cancel_Button";
			this.Cancel_Button.Size = new Size(100, 40);
			this.Cancel_Button.TabIndex = 1;
			this.Cancel_Button.Text = "Cancel";
			this.cbYear.Font = new Font("Microsoft Sans Serif", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.cbYear.FormattingEnabled = true;
			this.cbYear.Location = new Point(52, 86);
			this.cbYear.Margin = new Padding(4, 5, 4, 5);
			this.cbYear.Name = "cbYear";
			this.cbYear.Size = new Size(180, 37);
			this.cbYear.TabIndex = 1;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Microsoft Sans Serif", 11.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Label1.Location = new Point(34, 34);
			this.Label1.Margin = new Padding(4, 0, 4, 0);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(290, 29);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "Select year of data to load";
			base.AutoScaleDimensions = new SizeF(9f, 20f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.CancelButton = this.Cancel_Button;
			base.ClientSize = new Size(386, 152);
			base.Controls.Add(this.Label1);
			base.Controls.Add(this.cbYear);
			base.Controls.Add(this.Cancel_Button);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Margin = new Padding(4, 5, 4, 5);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "frmGetYear";
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			this.Text = "Select Year";
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private void Cancel_Button_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void cbYear_SelectedValueChanged(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void GetYear_Load(object sender, EventArgs e)
		{
			SummaryDat summaryDat = null;
			DataHandler_OLD dataHandler_OLD = new DataHandler_OLD(null, ref summaryDat);
			int[] years = dataHandler_OLD.getYears();
			foreach (int num in years)
			{
				this.cbYear.Items.Add(num);
			}
		}

		public object GetYear()
		{
			if (base.ShowDialog() == DialogResult.Cancel)
			{
				return 0;
			}
			return this.cbYear.SelectedItem;
		}
	}
}
