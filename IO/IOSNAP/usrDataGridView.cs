using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace IOSNAP
{
	public class usrDataGridView : DataGridView
	{
		private Image _backgroundPic;

		public override Image BackgroundImage
		{
			get
			{
				return this._backgroundPic;
			}
			set
			{
				this._backgroundPic = value;
			}
		}

		public usrDataGridView()
		{
			base.SetStyle(ControlStyles.UserPaint, true);
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			base.SetStyle(ControlStyles.DoubleBuffer, true);
		}

		protected override void PaintBackground(Graphics graphics, Rectangle clipBounds, Rectangle gridBounds)
		{
			base.PaintBackground(graphics, clipBounds, gridBounds);
			graphics.DrawImage(this.BackgroundImage, gridBounds);
		}

		public void SetCellsTransparent()
		{
			base.EnableHeadersVisualStyles = false;
			base.ColumnHeadersDefaultCellStyle.BackColor = Color.Transparent;
			base.RowHeadersDefaultCellStyle.BackColor = Color.Transparent;
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = base.Columns.GetEnumerator();
				while (enumerator.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
					dataGridViewColumn.DefaultCellStyle.BackColor = Color.Transparent;
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
		}
	}
}
