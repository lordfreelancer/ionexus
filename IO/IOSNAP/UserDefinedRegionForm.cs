using Microsoft.VisualBasic.CompilerServices;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class UserDefinedRegionForm : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CancelButtonUDR")]
		private Button _CancelButtonUDR;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OKButton")]
		private Button _OKButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("tboxRegionName")]
		private TextBox _tboxRegionName;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox1")]
		private GroupBox _GroupBox1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cbTaxes")]
		private CheckBox _cbTaxes;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cbGOS")]
		private CheckBox _cbGOS;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cbTVA")]
		private CheckBox _cbTVA;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cbEC")]
		private CheckBox _cbEC;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cbEmployment")]
		private CheckBox _cbEmployment;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox2")]
		private GroupBox _GroupBox2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbSelRegion")]
		private RadioButton _rbSelRegion;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbStateSelection")]
		private RadioButton _rbStateSelection;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("rbUserDefined")]
		private RadioButton _rbUserDefined;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("buttonSelectRegion")]
		private Button _buttonSelectRegion;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("comboBoxStateSelection")]
		private ComboBox _comboBoxStateSelection;

		internal virtual Button CancelButtonUDR
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button OKButton
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual TextBox tboxRegionName
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label Label1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual GroupBox GroupBox1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox cbTaxes
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox cbGOS
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox cbTVA
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox cbEC
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox cbEmployment
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual GroupBox GroupBox2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rbSelRegion
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rbStateSelection
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton rbUserDefined
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button buttonSelectRegion
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ComboBox comboBoxStateSelection
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		public UserDefinedRegionForm()
		{
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.CancelButtonUDR = new Button();
			this.OKButton = new Button();
			this.tboxRegionName = new TextBox();
			this.Label1 = new Label();
			this.GroupBox1 = new GroupBox();
			this.cbTaxes = new CheckBox();
			this.cbGOS = new CheckBox();
			this.cbTVA = new CheckBox();
			this.cbEC = new CheckBox();
			this.cbEmployment = new CheckBox();
			this.GroupBox2 = new GroupBox();
			this.buttonSelectRegion = new Button();
			this.comboBoxStateSelection = new ComboBox();
			this.rbSelRegion = new RadioButton();
			this.rbStateSelection = new RadioButton();
			this.rbUserDefined = new RadioButton();
			this.GroupBox1.SuspendLayout();
			this.GroupBox2.SuspendLayout();
			base.SuspendLayout();
			this.CancelButtonUDR.DialogResult = DialogResult.Cancel;
			this.CancelButtonUDR.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.CancelButtonUDR.Location = new Point(616, 334);
			this.CancelButtonUDR.Name = "CancelButtonUDR";
			this.CancelButtonUDR.Size = new Size(120, 43);
			this.CancelButtonUDR.TabIndex = 0;
			this.CancelButtonUDR.Text = "Cancel";
			this.CancelButtonUDR.UseVisualStyleBackColor = true;
			this.OKButton.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.OKButton.Location = new Point(757, 334);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new Size(120, 43);
			this.OKButton.TabIndex = 1;
			this.OKButton.Text = "OK";
			this.OKButton.UseVisualStyleBackColor = true;
			this.tboxRegionName.Location = new Point(516, 280);
			this.tboxRegionName.Name = "tboxRegionName";
			this.tboxRegionName.Size = new Size(361, 26);
			this.tboxRegionName.TabIndex = 2;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label1.Location = new Point(372, 279);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(138, 25);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "Region name";
			this.GroupBox1.Controls.Add(this.cbTaxes);
			this.GroupBox1.Controls.Add(this.cbGOS);
			this.GroupBox1.Controls.Add(this.cbTVA);
			this.GroupBox1.Controls.Add(this.cbEC);
			this.GroupBox1.Controls.Add(this.cbEmployment);
			this.GroupBox1.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.GroupBox1.Location = new Point(26, 41);
			this.GroupBox1.Name = "GroupBox1";
			this.GroupBox1.Size = new Size(340, 276);
			this.GroupBox1.TabIndex = 4;
			this.GroupBox1.TabStop = false;
			this.GroupBox1.Text = "Parameters";
			this.cbTaxes.AutoSize = true;
			this.cbTaxes.Checked = true;
			this.cbTaxes.CheckState = CheckState.Checked;
			this.cbTaxes.Location = new Point(27, 161);
			this.cbTaxes.Name = "cbTaxes";
			this.cbTaxes.Size = new Size(89, 29);
			this.cbTaxes.TabIndex = 4;
			this.cbTaxes.Text = "Taxes";
			this.cbTaxes.UseVisualStyleBackColor = true;
			this.cbGOS.AutoSize = true;
			this.cbGOS.Checked = true;
			this.cbGOS.CheckState = CheckState.Checked;
			this.cbGOS.Location = new Point(27, 122);
			this.cbGOS.Name = "cbGOS";
			this.cbGOS.Size = new Size(79, 29);
			this.cbGOS.TabIndex = 3;
			this.cbGOS.Text = "GOS";
			this.cbGOS.UseVisualStyleBackColor = true;
			this.cbTVA.AutoSize = true;
			this.cbTVA.Checked = true;
			this.cbTVA.CheckState = CheckState.Checked;
			this.cbTVA.Location = new Point(27, 204);
			this.cbTVA.Name = "cbTVA";
			this.cbTVA.Size = new Size(197, 29);
			this.cbTVA.TabIndex = 2;
			this.cbTVA.Text = "Total Value Added";
			this.cbTVA.UseVisualStyleBackColor = true;
			this.cbEC.AutoSize = true;
			this.cbEC.Checked = true;
			this.cbEC.CheckState = CheckState.Checked;
			this.cbEC.Location = new Point(27, 82);
			this.cbEC.Name = "cbEC";
			this.cbEC.Size = new Size(253, 29);
			this.cbEC.TabIndex = 1;
			this.cbEC.Text = "Employee Compensation";
			this.cbEC.UseVisualStyleBackColor = true;
			this.cbEmployment.AutoSize = true;
			this.cbEmployment.Checked = true;
			this.cbEmployment.CheckState = CheckState.Checked;
			this.cbEmployment.Location = new Point(27, 39);
			this.cbEmployment.Name = "cbEmployment";
			this.cbEmployment.Size = new Size(142, 29);
			this.cbEmployment.TabIndex = 0;
			this.cbEmployment.Text = "Employment";
			this.cbEmployment.UseVisualStyleBackColor = true;
			this.GroupBox2.Controls.Add(this.buttonSelectRegion);
			this.GroupBox2.Controls.Add(this.comboBoxStateSelection);
			this.GroupBox2.Controls.Add(this.rbSelRegion);
			this.GroupBox2.Controls.Add(this.rbStateSelection);
			this.GroupBox2.Controls.Add(this.rbUserDefined);
			this.GroupBox2.Font = new Font("Microsoft Sans Serif", 10f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.GroupBox2.Location = new Point(456, 41);
			this.GroupBox2.Name = "GroupBox2";
			this.GroupBox2.Size = new Size(421, 208);
			this.GroupBox2.TabIndex = 5;
			this.GroupBox2.TabStop = false;
			this.GroupBox2.Text = "Region template";
			this.buttonSelectRegion.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.buttonSelectRegion.Location = new Point(216, 138);
			this.buttonSelectRegion.Name = "buttonSelectRegion";
			this.buttonSelectRegion.Size = new Size(140, 32);
			this.buttonSelectRegion.TabIndex = 5;
			this.buttonSelectRegion.Text = "Select Region";
			this.buttonSelectRegion.UseVisualStyleBackColor = true;
			this.comboBoxStateSelection.FormattingEnabled = true;
			this.comboBoxStateSelection.Location = new Point(216, 87);
			this.comboBoxStateSelection.Name = "comboBoxStateSelection";
			this.comboBoxStateSelection.Size = new Size(140, 33);
			this.comboBoxStateSelection.TabIndex = 4;
			this.rbSelRegion.AutoSize = true;
			this.rbSelRegion.Location = new Point(21, 141);
			this.rbSelRegion.Name = "rbSelRegion";
			this.rbSelRegion.Size = new Size(153, 29);
			this.rbSelRegion.TabIndex = 2;
			this.rbSelRegion.TabStop = true;
			this.rbSelRegion.Text = "Region based";
			this.rbSelRegion.UseVisualStyleBackColor = true;
			this.rbStateSelection.AutoSize = true;
			this.rbStateSelection.Location = new Point(21, 88);
			this.rbStateSelection.Name = "rbStateSelection";
			this.rbStateSelection.Size = new Size(138, 29);
			this.rbStateSelection.TabIndex = 1;
			this.rbStateSelection.TabStop = true;
			this.rbStateSelection.Text = "State based";
			this.rbStateSelection.UseVisualStyleBackColor = true;
			this.rbUserDefined.AutoSize = true;
			this.rbUserDefined.Checked = true;
			this.rbUserDefined.Location = new Point(21, 38);
			this.rbUserDefined.Name = "rbUserDefined";
			this.rbUserDefined.Size = new Size(143, 29);
			this.rbUserDefined.TabIndex = 0;
			this.rbUserDefined.TabStop = true;
			this.rbUserDefined.Text = "User defined";
			this.rbUserDefined.UseVisualStyleBackColor = true;
			base.AutoScaleDimensions = new SizeF(9f, 20f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(904, 399);
			base.Controls.Add(this.GroupBox2);
			base.Controls.Add(this.GroupBox1);
			base.Controls.Add(this.Label1);
			base.Controls.Add(this.tboxRegionName);
			base.Controls.Add(this.OKButton);
			base.Controls.Add(this.CancelButtonUDR);
			base.Name = "UserDefinedRegionForm";
			this.Text = "User Defined Region Template";
			this.GroupBox1.ResumeLayout(false);
			this.GroupBox1.PerformLayout();
			this.GroupBox2.ResumeLayout(false);
			this.GroupBox2.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
