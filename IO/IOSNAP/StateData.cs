using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;

namespace IOSNAP
{
	[Serializable]
	public class StateData : ICloneable
	{
		public int nrec_base;

		public int nrec_imputed;

		public int nrec_totgov;

		public int nrec_fedstate;

		private const int N_IMPUTED = 4;

		private const int N_FEDSTATE = 2;

		public string Name;

		public GenDataStatus GovAggLevel;

		public FTE_RATIO_CLASS fte;

		public double PersonalIncome;

		public double DisposableIncome;

		public double TotFedExp;

		public double TotSLEExp;

		public Vector EC_base;

		public Vector GDP_base;

		public Vector GOS_base;

		public Vector T_base;

		public Vector Employment_base;

		public Vector PCE_Table;

		public Vector ValueAdded_base;

		private Vector CompensationRate_base;

		public Vector EC_base_flag;

		public Vector GDP_base_flag;

		public Vector GOS_base_flag;

		public Vector T_base_flag;

		public Vector Employment_base_flag;

		private Vector CompensationRate_base_flag;

		public Vector EC_Imputed;

		public Vector GDP_imputed;

		public Vector GOS_imputed;

		public Vector T_imputed;

		public Vector Employment_Imputed;

		public Vector ValueAdded_imputed;

		private Vector CompensationRate_Imputed;

		public Vector EC_Imputed_flag;

		public Vector GDP_imputed_flag;

		public Vector GOS_imputed_flag;

		public Vector T_imputed_flag;

		public Vector Employment_Imputed_flag;

		private Vector CompensationRate_Imputed_flag;

		public Vector EC_TotalGov;

		public Vector GDP_TotalGov;

		public Vector GOS_TotalGov;

		public Vector T_TotalGov;

		public Vector Employment_TotalGov;

		public Vector Employment_TotalGov_jobs;

		public Vector Employment_TotalGov_fte;

		public Vector ValueAdded_TotalGov;

		private Vector CompensationRate_TotalGov;

		public Vector EC_TotalGov_flag;

		public Vector GDP_TotalGov_flag;

		public Vector GOS_TotalGov_flag;

		public Vector T_TotalGov_flag;

		public Vector Employment_TotalGov_flag;

		private Vector CompensationRate_TotalGov_flag;

		public double EC_TotGovSUM;

		public double GDP_TotGovSUM;

		public double GOS_TotGovSUM;

		public double T_TotGovSUM;

		public double Employment_TotGovSUM;

		public double ValueAdded_TotGovSUM;

		private double CompensationRate_TotGovSUM;

		public double EC_TotGovSUM_flag;

		public double GDP_TotGovSUM_flag;

		public double GOS_TotGovSUM_flag;

		public double T_TotGovSUM_flag;

		public double Employment_TotGovSUM_flag;

		private double CompensationRate_TotGovSUM_flag;

		public Vector EC_ImpGov;

		public Vector GDP_ImpGov;

		public Vector GOS_ImpGov;

		public Vector T_ImpGov;

		public Vector Employment_ImpGov;

		public Vector ValueAdded_ImpGov;

		private Vector CompensationRate_ImpGov;

		public Vector EC_ImpGov_flag;

		public Vector GDP_ImpGov_flag;

		public Vector GOS_ImpGov_flag;

		public Vector T_ImpGov_flag;

		public Vector Employment_ImpGov_flag;

		private Vector CompensationRate_ImpGov_flag;

		public Vector EC_FedState;

		public Vector GDP_FedState;

		public Vector GOS_FedState;

		public Vector T_FedState;

		public Vector Employment_FedState;

		public Vector Employment_FedState_jobs;

		public Vector Employment_FedState_fte;

		public Vector ValueAdded_FedState;

		private Vector CompensationRate_FedState;

		public Vector EC_FedState_flag;

		public Vector GDP_FedState_flag;

		public Vector GOS_FedState_flag;

		public Vector T_FedState_flag;

		public Vector Employment_FedState_flag;

		private Vector CompensationRate_FedState_flag;

		public Vector EC_FedState_Gov;

		public Vector GDP_FedState_Gov;

		public Vector GOS_FedState_Gov;

		public Vector T_FedState_Gov;

		public Vector Employment_FedState_Gov;

		public Vector Employment_FedState_Gov_jobs;

		public Vector Employment_FedState_Gov_fte;

		public Vector ValueAdded_FedState_Gov;

		private Vector CompensationRate_FedState_Gov;

		public Vector EC_FedState_Gov_flag;

		public Vector GDP_FedState_Gov_flag;

		public Vector GOS_FedState_Gov_flag;

		public Vector T_FedState_Gov_flag;

		public Vector Employment_FedState_Gov_flag;

		private Vector CompensationRate_FedState_Gov_flag;

		public StateData()
		{
			this.nrec_base = 0;
			this.nrec_imputed = 0;
			this.nrec_totgov = 0;
			this.nrec_fedstate = 0;
			this.Name = "";
			this.GovAggLevel = new GenDataStatus();
			this.PersonalIncome = 0.0;
			this.DisposableIncome = 0.0;
			this.TotFedExp = 0.0;
			this.TotSLEExp = 0.0;
			this.EC_base = new Vector(1);
			this.GDP_base = new Vector(1);
			this.GOS_base = new Vector(1);
			this.T_base = new Vector(1);
			this.Employment_base = new Vector(1);
			this.PCE_Table = new Vector(1);
			this.ValueAdded_base = new Vector(1);
			this.CompensationRate_base = new Vector(1);
			this.EC_base_flag = new Vector(1);
			this.GDP_base_flag = new Vector(1);
			this.GOS_base_flag = new Vector(1);
			this.T_base_flag = new Vector(1);
			this.Employment_base_flag = new Vector(1);
			this.CompensationRate_base_flag = new Vector(1);
			this.EC_Imputed = new Vector(1);
			this.GDP_imputed = new Vector(1);
			this.GOS_imputed = new Vector(1);
			this.T_imputed = new Vector(1);
			this.Employment_Imputed = new Vector(1);
			this.ValueAdded_imputed = new Vector(1);
			this.CompensationRate_Imputed = new Vector(1);
			this.EC_Imputed_flag = new Vector(1);
			this.GDP_imputed_flag = new Vector(1);
			this.GOS_imputed_flag = new Vector(1);
			this.T_imputed_flag = new Vector(1);
			this.Employment_Imputed_flag = new Vector(1);
			this.CompensationRate_Imputed_flag = new Vector(1);
			this.EC_TotalGov = new Vector(1);
			this.GDP_TotalGov = new Vector(1);
			this.GOS_TotalGov = new Vector(1);
			this.T_TotalGov = new Vector(1);
			this.Employment_TotalGov = new Vector(1);
			this.Employment_TotalGov_jobs = new Vector(1);
			this.Employment_TotalGov_fte = new Vector(1);
			this.ValueAdded_TotalGov = new Vector(1);
			this.CompensationRate_TotalGov = new Vector(1);
			this.EC_TotalGov_flag = new Vector(1);
			this.GDP_TotalGov_flag = new Vector(1);
			this.GOS_TotalGov_flag = new Vector(1);
			this.T_TotalGov_flag = new Vector(1);
			this.Employment_TotalGov_flag = new Vector(1);
			this.CompensationRate_TotalGov_flag = new Vector(1);
			this.EC_TotGovSUM = 0.0;
			this.GDP_TotGovSUM = 0.0;
			this.GOS_TotGovSUM = 0.0;
			this.T_TotGovSUM = 0.0;
			this.Employment_TotGovSUM = 0.0;
			this.ValueAdded_TotGovSUM = 0.0;
			this.CompensationRate_TotGovSUM = 0.0;
			this.EC_TotGovSUM_flag = 0.0;
			this.GDP_TotGovSUM_flag = 0.0;
			this.GOS_TotGovSUM_flag = 0.0;
			this.T_TotGovSUM_flag = 0.0;
			this.Employment_TotGovSUM_flag = 0.0;
			this.CompensationRate_TotGovSUM_flag = 0.0;
			this.EC_ImpGov = new Vector(4);
			this.GDP_ImpGov = new Vector(4);
			this.GOS_ImpGov = new Vector(4);
			this.T_ImpGov = new Vector(4);
			this.Employment_ImpGov = new Vector(4);
			this.ValueAdded_ImpGov = new Vector(4);
			this.CompensationRate_ImpGov = new Vector(4);
			this.EC_ImpGov_flag = new Vector(4);
			this.GDP_ImpGov_flag = new Vector(4);
			this.GOS_ImpGov_flag = new Vector(4);
			this.T_ImpGov_flag = new Vector(4);
			this.Employment_ImpGov_flag = new Vector(4);
			this.CompensationRate_ImpGov_flag = new Vector(4);
			this.EC_FedState = new Vector(1);
			this.GDP_FedState = new Vector(1);
			this.GOS_FedState = new Vector(1);
			this.T_FedState = new Vector(1);
			this.Employment_FedState = new Vector(1);
			this.Employment_FedState_jobs = new Vector(1);
			this.Employment_FedState_fte = new Vector(1);
			this.ValueAdded_FedState = new Vector(1);
			this.CompensationRate_FedState = new Vector(1);
			this.EC_FedState_flag = new Vector(1);
			this.GDP_FedState_flag = new Vector(1);
			this.GOS_FedState_flag = new Vector(1);
			this.T_FedState_flag = new Vector(1);
			this.Employment_FedState_flag = new Vector(1);
			this.CompensationRate_FedState_flag = new Vector(1);
			this.EC_FedState_Gov = new Vector(2);
			this.GDP_FedState_Gov = new Vector(2);
			this.GOS_FedState_Gov = new Vector(2);
			this.T_FedState_Gov = new Vector(2);
			this.Employment_FedState_Gov = new Vector(2);
			this.Employment_FedState_Gov_jobs = new Vector(2);
			this.Employment_FedState_Gov_fte = new Vector(2);
			this.ValueAdded_FedState_Gov = new Vector(2);
			this.CompensationRate_FedState_Gov = new Vector(2);
			this.EC_FedState_Gov_flag = new Vector(2);
			this.GDP_FedState_Gov_flag = new Vector(2);
			this.GOS_FedState_Gov_flag = new Vector(2);
			this.T_FedState_Gov_flag = new Vector(2);
			this.Employment_FedState_Gov_flag = new Vector(2);
			this.CompensationRate_FedState_Gov_flag = new Vector(2);
		}

		public StateData(string st)
		{
			this.nrec_base = 0;
			this.nrec_imputed = 0;
			this.nrec_totgov = 0;
			this.nrec_fedstate = 0;
			this.Name = "";
			this.GovAggLevel = new GenDataStatus();
			this.PersonalIncome = 0.0;
			this.DisposableIncome = 0.0;
			this.TotFedExp = 0.0;
			this.TotSLEExp = 0.0;
			this.EC_base = new Vector(1);
			this.GDP_base = new Vector(1);
			this.GOS_base = new Vector(1);
			this.T_base = new Vector(1);
			this.Employment_base = new Vector(1);
			this.PCE_Table = new Vector(1);
			this.ValueAdded_base = new Vector(1);
			this.CompensationRate_base = new Vector(1);
			this.EC_base_flag = new Vector(1);
			this.GDP_base_flag = new Vector(1);
			this.GOS_base_flag = new Vector(1);
			this.T_base_flag = new Vector(1);
			this.Employment_base_flag = new Vector(1);
			this.CompensationRate_base_flag = new Vector(1);
			this.EC_Imputed = new Vector(1);
			this.GDP_imputed = new Vector(1);
			this.GOS_imputed = new Vector(1);
			this.T_imputed = new Vector(1);
			this.Employment_Imputed = new Vector(1);
			this.ValueAdded_imputed = new Vector(1);
			this.CompensationRate_Imputed = new Vector(1);
			this.EC_Imputed_flag = new Vector(1);
			this.GDP_imputed_flag = new Vector(1);
			this.GOS_imputed_flag = new Vector(1);
			this.T_imputed_flag = new Vector(1);
			this.Employment_Imputed_flag = new Vector(1);
			this.CompensationRate_Imputed_flag = new Vector(1);
			this.EC_TotalGov = new Vector(1);
			this.GDP_TotalGov = new Vector(1);
			this.GOS_TotalGov = new Vector(1);
			this.T_TotalGov = new Vector(1);
			this.Employment_TotalGov = new Vector(1);
			this.Employment_TotalGov_jobs = new Vector(1);
			this.Employment_TotalGov_fte = new Vector(1);
			this.ValueAdded_TotalGov = new Vector(1);
			this.CompensationRate_TotalGov = new Vector(1);
			this.EC_TotalGov_flag = new Vector(1);
			this.GDP_TotalGov_flag = new Vector(1);
			this.GOS_TotalGov_flag = new Vector(1);
			this.T_TotalGov_flag = new Vector(1);
			this.Employment_TotalGov_flag = new Vector(1);
			this.CompensationRate_TotalGov_flag = new Vector(1);
			this.EC_TotGovSUM = 0.0;
			this.GDP_TotGovSUM = 0.0;
			this.GOS_TotGovSUM = 0.0;
			this.T_TotGovSUM = 0.0;
			this.Employment_TotGovSUM = 0.0;
			this.ValueAdded_TotGovSUM = 0.0;
			this.CompensationRate_TotGovSUM = 0.0;
			this.EC_TotGovSUM_flag = 0.0;
			this.GDP_TotGovSUM_flag = 0.0;
			this.GOS_TotGovSUM_flag = 0.0;
			this.T_TotGovSUM_flag = 0.0;
			this.Employment_TotGovSUM_flag = 0.0;
			this.CompensationRate_TotGovSUM_flag = 0.0;
			this.EC_ImpGov = new Vector(4);
			this.GDP_ImpGov = new Vector(4);
			this.GOS_ImpGov = new Vector(4);
			this.T_ImpGov = new Vector(4);
			this.Employment_ImpGov = new Vector(4);
			this.ValueAdded_ImpGov = new Vector(4);
			this.CompensationRate_ImpGov = new Vector(4);
			this.EC_ImpGov_flag = new Vector(4);
			this.GDP_ImpGov_flag = new Vector(4);
			this.GOS_ImpGov_flag = new Vector(4);
			this.T_ImpGov_flag = new Vector(4);
			this.Employment_ImpGov_flag = new Vector(4);
			this.CompensationRate_ImpGov_flag = new Vector(4);
			this.EC_FedState = new Vector(1);
			this.GDP_FedState = new Vector(1);
			this.GOS_FedState = new Vector(1);
			this.T_FedState = new Vector(1);
			this.Employment_FedState = new Vector(1);
			this.Employment_FedState_jobs = new Vector(1);
			this.Employment_FedState_fte = new Vector(1);
			this.ValueAdded_FedState = new Vector(1);
			this.CompensationRate_FedState = new Vector(1);
			this.EC_FedState_flag = new Vector(1);
			this.GDP_FedState_flag = new Vector(1);
			this.GOS_FedState_flag = new Vector(1);
			this.T_FedState_flag = new Vector(1);
			this.Employment_FedState_flag = new Vector(1);
			this.CompensationRate_FedState_flag = new Vector(1);
			this.EC_FedState_Gov = new Vector(2);
			this.GDP_FedState_Gov = new Vector(2);
			this.GOS_FedState_Gov = new Vector(2);
			this.T_FedState_Gov = new Vector(2);
			this.Employment_FedState_Gov = new Vector(2);
			this.Employment_FedState_Gov_jobs = new Vector(2);
			this.Employment_FedState_Gov_fte = new Vector(2);
			this.ValueAdded_FedState_Gov = new Vector(2);
			this.CompensationRate_FedState_Gov = new Vector(2);
			this.EC_FedState_Gov_flag = new Vector(2);
			this.GDP_FedState_Gov_flag = new Vector(2);
			this.GOS_FedState_Gov_flag = new Vector(2);
			this.T_FedState_Gov_flag = new Vector(2);
			this.Employment_FedState_Gov_flag = new Vector(2);
			this.CompensationRate_FedState_Gov_flag = new Vector(2);
			this.Name = st;
		}

		public StateData(string st, int _nrec)
		{
			this.nrec_base = 0;
			this.nrec_imputed = 0;
			this.nrec_totgov = 0;
			this.nrec_fedstate = 0;
			this.Name = "";
			this.GovAggLevel = new GenDataStatus();
			this.PersonalIncome = 0.0;
			this.DisposableIncome = 0.0;
			this.TotFedExp = 0.0;
			this.TotSLEExp = 0.0;
			this.EC_base = new Vector(1);
			this.GDP_base = new Vector(1);
			this.GOS_base = new Vector(1);
			this.T_base = new Vector(1);
			this.Employment_base = new Vector(1);
			this.PCE_Table = new Vector(1);
			this.ValueAdded_base = new Vector(1);
			this.CompensationRate_base = new Vector(1);
			this.EC_base_flag = new Vector(1);
			this.GDP_base_flag = new Vector(1);
			this.GOS_base_flag = new Vector(1);
			this.T_base_flag = new Vector(1);
			this.Employment_base_flag = new Vector(1);
			this.CompensationRate_base_flag = new Vector(1);
			this.EC_Imputed = new Vector(1);
			this.GDP_imputed = new Vector(1);
			this.GOS_imputed = new Vector(1);
			this.T_imputed = new Vector(1);
			this.Employment_Imputed = new Vector(1);
			this.ValueAdded_imputed = new Vector(1);
			this.CompensationRate_Imputed = new Vector(1);
			this.EC_Imputed_flag = new Vector(1);
			this.GDP_imputed_flag = new Vector(1);
			this.GOS_imputed_flag = new Vector(1);
			this.T_imputed_flag = new Vector(1);
			this.Employment_Imputed_flag = new Vector(1);
			this.CompensationRate_Imputed_flag = new Vector(1);
			this.EC_TotalGov = new Vector(1);
			this.GDP_TotalGov = new Vector(1);
			this.GOS_TotalGov = new Vector(1);
			this.T_TotalGov = new Vector(1);
			this.Employment_TotalGov = new Vector(1);
			this.Employment_TotalGov_jobs = new Vector(1);
			this.Employment_TotalGov_fte = new Vector(1);
			this.ValueAdded_TotalGov = new Vector(1);
			this.CompensationRate_TotalGov = new Vector(1);
			this.EC_TotalGov_flag = new Vector(1);
			this.GDP_TotalGov_flag = new Vector(1);
			this.GOS_TotalGov_flag = new Vector(1);
			this.T_TotalGov_flag = new Vector(1);
			this.Employment_TotalGov_flag = new Vector(1);
			this.CompensationRate_TotalGov_flag = new Vector(1);
			this.EC_TotGovSUM = 0.0;
			this.GDP_TotGovSUM = 0.0;
			this.GOS_TotGovSUM = 0.0;
			this.T_TotGovSUM = 0.0;
			this.Employment_TotGovSUM = 0.0;
			this.ValueAdded_TotGovSUM = 0.0;
			this.CompensationRate_TotGovSUM = 0.0;
			this.EC_TotGovSUM_flag = 0.0;
			this.GDP_TotGovSUM_flag = 0.0;
			this.GOS_TotGovSUM_flag = 0.0;
			this.T_TotGovSUM_flag = 0.0;
			this.Employment_TotGovSUM_flag = 0.0;
			this.CompensationRate_TotGovSUM_flag = 0.0;
			this.EC_ImpGov = new Vector(4);
			this.GDP_ImpGov = new Vector(4);
			this.GOS_ImpGov = new Vector(4);
			this.T_ImpGov = new Vector(4);
			this.Employment_ImpGov = new Vector(4);
			this.ValueAdded_ImpGov = new Vector(4);
			this.CompensationRate_ImpGov = new Vector(4);
			this.EC_ImpGov_flag = new Vector(4);
			this.GDP_ImpGov_flag = new Vector(4);
			this.GOS_ImpGov_flag = new Vector(4);
			this.T_ImpGov_flag = new Vector(4);
			this.Employment_ImpGov_flag = new Vector(4);
			this.CompensationRate_ImpGov_flag = new Vector(4);
			this.EC_FedState = new Vector(1);
			this.GDP_FedState = new Vector(1);
			this.GOS_FedState = new Vector(1);
			this.T_FedState = new Vector(1);
			this.Employment_FedState = new Vector(1);
			this.Employment_FedState_jobs = new Vector(1);
			this.Employment_FedState_fte = new Vector(1);
			this.ValueAdded_FedState = new Vector(1);
			this.CompensationRate_FedState = new Vector(1);
			this.EC_FedState_flag = new Vector(1);
			this.GDP_FedState_flag = new Vector(1);
			this.GOS_FedState_flag = new Vector(1);
			this.T_FedState_flag = new Vector(1);
			this.Employment_FedState_flag = new Vector(1);
			this.CompensationRate_FedState_flag = new Vector(1);
			this.EC_FedState_Gov = new Vector(2);
			this.GDP_FedState_Gov = new Vector(2);
			this.GOS_FedState_Gov = new Vector(2);
			this.T_FedState_Gov = new Vector(2);
			this.Employment_FedState_Gov = new Vector(2);
			this.Employment_FedState_Gov_jobs = new Vector(2);
			this.Employment_FedState_Gov_fte = new Vector(2);
			this.ValueAdded_FedState_Gov = new Vector(2);
			this.CompensationRate_FedState_Gov = new Vector(2);
			this.EC_FedState_Gov_flag = new Vector(2);
			this.GDP_FedState_Gov_flag = new Vector(2);
			this.GOS_FedState_Gov_flag = new Vector(2);
			this.T_FedState_Gov_flag = new Vector(2);
			this.Employment_FedState_Gov_flag = new Vector(2);
			this.CompensationRate_FedState_Gov_flag = new Vector(2);
			this.Name = st;
			this.nrec_base = _nrec;
			checked
			{
				this.nrec_imputed = _nrec + 4;
				this.nrec_fedstate = _nrec + 2;
				this.nrec_totgov = _nrec + 1;
				this.EC_base = new Vector(this.nrec_base);
				this.GDP_base = new Vector(this.nrec_base);
				this.GOS_base = new Vector(this.nrec_base);
				this.T_base = new Vector(this.nrec_base);
				this.Employment_base = new Vector(this.nrec_base);
				this.ValueAdded_base = new Vector(this.nrec_base);
				this.CompensationRate_base = new Vector(this.nrec_base);
				this.EC_base_flag = new Vector(this.nrec_base);
				this.GDP_base_flag = new Vector(this.nrec_base);
				this.GOS_base_flag = new Vector(this.nrec_base);
				this.T_base_flag = new Vector(this.nrec_base);
				this.Employment_base_flag = new Vector(this.nrec_base);
				this.CompensationRate_base_flag = new Vector(this.nrec_base);
				this.PCE_Table = new Vector(this.nrec_base + 6);
			}
		}

		private void calcStateGov()
		{
		}

		public void StateSetup(ref FTE_RATIO_CLASS v_fte)
		{
			this.fte = v_fte;
			this.calcStateGov();
			this.ValueAdded_base = this.EC_base + this.GOS_base + this.T_base;
			this.ValueAdded_ImpGov = this.EC_ImpGov + this.GOS_ImpGov + this.T_ImpGov;
			this.ValueAdded_TotGovSUM = this.EC_TotGovSUM + this.GOS_TotGovSUM + this.T_TotGovSUM;
			this.ValueAdded_FedState_Gov = this.EC_FedState_Gov + this.GOS_FedState_Gov + this.T_FedState_Gov;
			this.CompensationRate_TotGovSUM = this.EC_TotGovSUM / this.Employment_TotGovSUM;
			this.CompensationRate_TotGovSUM_flag = this.EC_TotGovSUM_flag + this.Employment_TotGovSUM_flag;
			this.ValueAdded_imputed = new Vector(this.nrec_imputed);
			this.CompensationRate_Imputed = new Vector(this.nrec_imputed);
			this.EC_Imputed = this.Append2Vectors(ref this.EC_base, ref this.EC_ImpGov);
			this.GOS_imputed = this.Append2Vectors(ref this.GOS_base, ref this.GOS_ImpGov);
			this.GDP_imputed = this.Append2Vectors(ref this.GDP_base, ref this.GDP_ImpGov);
			this.T_imputed = this.Append2Vectors(ref this.T_base, ref this.T_ImpGov);
			this.Employment_Imputed = this.Append2Vectors(ref this.Employment_base, ref this.Employment_ImpGov);
			this.EC_Imputed_flag = this.Append2Vectors(ref this.EC_base_flag, ref this.EC_ImpGov_flag);
			this.GOS_imputed_flag = this.Append2Vectors(ref this.GOS_base_flag, ref this.GOS_ImpGov_flag);
			this.GDP_imputed_flag = this.Append2Vectors(ref this.GDP_base_flag, ref this.GDP_ImpGov_flag);
			this.T_imputed_flag = this.Append2Vectors(ref this.T_base_flag, ref this.T_ImpGov_flag);
			this.Employment_Imputed_flag = this.Append2Vectors(ref this.Employment_base_flag, ref this.Employment_ImpGov_flag);
			this.ValueAdded_imputed = this.EC_Imputed + this.GOS_imputed + this.T_imputed;
			this.CompensationRate_ImpGov_flag = this.EC_ImpGov_flag + this.Employment_ImpGov_flag;
			this.CompensationRate_ImpGov = this.EC_ImpGov / this.Employment_ImpGov;
			this.CompensationRate_base_flag = this.EC_base_flag + this.Employment_base_flag;
			this.CompensationRate_base = this.EC_base / this.Employment_base;
			this.CompensationRate_Imputed_flag = this.EC_Imputed_flag + this.Employment_Imputed_flag;
			this.CompensationRate_Imputed = this.EC_Imputed / this.Employment_Imputed;
			this.EC_FedState = this.Append2Vectors(ref this.EC_base, ref this.EC_FedState_Gov);
			this.GOS_FedState = this.Append2Vectors(ref this.GOS_base, ref this.GOS_FedState_Gov);
			this.GDP_FedState = this.Append2Vectors(ref this.GDP_base, ref this.GDP_FedState_Gov);
			this.T_FedState = this.Append2Vectors(ref this.T_base, ref this.T_FedState_Gov);
			this.Employment_FedState = this.Append2Vectors(ref this.Employment_base, ref this.Employment_FedState_Gov);
			this.Employment_FedState_fte = new Vector(this.Employment_FedState.Count);
			this.Employment_FedState_jobs = new Vector(this.Employment_FedState.Count);
			this.EC_FedState_flag = this.Append2Vectors(ref this.EC_base_flag, ref this.EC_FedState_Gov_flag);
			this.GOS_FedState_flag = this.Append2Vectors(ref this.GOS_base_flag, ref this.GOS_FedState_Gov_flag);
			this.GDP_FedState_flag = this.Append2Vectors(ref this.GDP_base_flag, ref this.GDP_FedState_Gov_flag);
			this.T_FedState_flag = this.Append2Vectors(ref this.T_base_flag, ref this.T_FedState_Gov_flag);
			this.Employment_FedState_flag = this.Append2Vectors(ref this.Employment_base_flag, ref this.Employment_FedState_Gov_flag);
			this.ValueAdded_FedState = this.EC_FedState + this.GOS_FedState + this.T_FedState;
			this.CompensationRate_FedState_flag = this.EC_FedState_flag + this.Employment_FedState_flag;
			this.CompensationRate_FedState = this.EC_FedState / this.Employment_FedState;
			this.EC_TotalGov = this.Append2Vectors(ref this.EC_base, ref this.EC_TotGovSUM);
			this.GOS_TotalGov = this.Append2Vectors(ref this.GOS_base, ref this.GOS_TotGovSUM);
			this.GDP_TotalGov = this.Append2Vectors(ref this.GDP_base, ref this.GDP_TotGovSUM);
			this.T_TotalGov = this.Append2Vectors(ref this.T_base, ref this.T_TotGovSUM);
			this.Employment_TotalGov = this.Append2Vectors(ref this.Employment_base, ref this.Employment_TotGovSUM);
			this.Employment_TotalGov_fte = new Vector(this.Employment_TotalGov.Count);
			this.Employment_TotalGov_jobs = new Vector(this.Employment_TotalGov.Count);
			this.EC_TotalGov_flag = this.Append2Vectors(ref this.EC_base_flag, ref this.EC_TotGovSUM_flag);
			this.GOS_TotalGov_flag = this.Append2Vectors(ref this.GOS_base_flag, ref this.GOS_TotGovSUM_flag);
			this.GDP_TotalGov_flag = this.Append2Vectors(ref this.GDP_base_flag, ref this.GDP_TotGovSUM_flag);
			this.T_TotalGov_flag = this.Append2Vectors(ref this.T_base_flag, ref this.T_TotGovSUM_flag);
			this.Employment_TotalGov_flag = this.Append2Vectors(ref this.Employment_base_flag, ref this.Employment_TotGovSUM_flag);
			this.ValueAdded_TotalGov = this.EC_TotalGov + this.GOS_TotalGov + this.T_TotalGov;
			this.scaleEmploymentByFTE(this.fte);
			this.CompensationRate_TotalGov_flag = this.EC_TotalGov_flag + this.Employment_TotalGov_flag;
			this.CompensationRate_TotalGov = this.EC_TotalGov / this.Employment_TotalGov;
		}

		public void setEmptyState()
		{
		}

		public void addState(ref StateData stateAdded)
		{
			int num = checked(this.Employment_base.Count - 1);
			for (int i = 0; i <= num; i = checked(i + 1))
			{
				this.EC_base[i] = this.EC_base[i] + stateAdded.EC_base[i];
				this.EC_base_flag[i] = this.EC_base_flag[i] + stateAdded.EC_base_flag[i];
				this.Employment_base[i] = this.Employment_base[i] + stateAdded.Employment_base[i];
				this.Employment_base_flag[i] = this.Employment_base_flag[i] + stateAdded.Employment_base_flag[i];
				this.T_base[i] = this.T_base[i] + stateAdded.T_base[i];
				this.T_base_flag[i] = this.T_base_flag[i] + stateAdded.T_base_flag[i];
				this.GOS_base[i] = this.GOS_base[i] + stateAdded.GOS_base[i];
				this.GOS_base_flag[i] = this.GOS_base_flag[i] + stateAdded.GOS_base_flag[i];
				this.GDP_base[i] = this.GDP_base[i] + stateAdded.GDP_base[i];
				this.GDP_base_flag[i] = this.GDP_base_flag[i] + stateAdded.GDP_base_flag[i];
			}
			int num2 = 0;
			do
			{
				this.EC_ImpGov[num2] = this.EC_ImpGov[num2] + stateAdded.EC_ImpGov[num2];
				this.EC_ImpGov_flag[num2] = this.EC_ImpGov_flag[num2] + stateAdded.EC_ImpGov_flag[num2];
				this.Employment_ImpGov[num2] = this.Employment_ImpGov[num2] + stateAdded.Employment_ImpGov[num2];
				this.Employment_ImpGov_flag[num2] = this.Employment_ImpGov_flag[num2] + stateAdded.Employment_ImpGov_flag[num2];
				this.T_ImpGov[num2] = this.T_ImpGov[num2] + stateAdded.T_ImpGov[num2];
				this.T_ImpGov_flag[num2] = this.T_ImpGov_flag[num2] + stateAdded.T_ImpGov_flag[num2];
				this.GOS_ImpGov[num2] = this.GOS_ImpGov[num2] + stateAdded.GOS_ImpGov[num2];
				this.GOS_ImpGov_flag[num2] = this.GOS_ImpGov_flag[num2] + stateAdded.GOS_ImpGov_flag[num2];
				this.GDP_ImpGov[num2] = this.GDP_ImpGov[num2] + stateAdded.GDP_ImpGov[num2];
				this.GDP_ImpGov_flag[num2] = this.GDP_ImpGov_flag[num2] + stateAdded.GDP_ImpGov_flag[num2];
				num2 = checked(num2 + 1);
			}
			while (num2 <= 3);
			int num3 = 0;
			do
			{
				this.EC_FedState_Gov[num3] = this.EC_FedState_Gov[num3] + stateAdded.EC_FedState_Gov[num3];
				this.EC_FedState_Gov_flag[num3] = this.EC_FedState_Gov_flag[num3] + stateAdded.EC_FedState_Gov_flag[num3];
				this.Employment_FedState_Gov[num3] = this.Employment_FedState_Gov[num3] + stateAdded.Employment_FedState_Gov[num3];
				this.Employment_FedState_Gov_flag[num3] = this.Employment_FedState_Gov_flag[num3] + stateAdded.Employment_FedState_Gov_flag[num3];
				this.T_FedState_Gov[num3] = this.T_FedState_Gov[num3] + stateAdded.T_FedState_Gov[num3];
				this.T_FedState_Gov_flag[num3] = this.T_FedState_Gov_flag[num3] + stateAdded.T_FedState_Gov_flag[num3];
				this.GOS_FedState_Gov[num3] = this.GOS_FedState_Gov[num3] + stateAdded.GOS_FedState_Gov[num3];
				this.GOS_FedState_Gov_flag[num3] = this.GOS_FedState_Gov_flag[num3] + stateAdded.GOS_FedState_Gov_flag[num3];
				this.GDP_FedState_Gov[num3] = this.GDP_FedState_Gov[num3] + stateAdded.GDP_FedState_Gov[num3];
				this.GDP_FedState_Gov_flag[num3] = this.GDP_FedState_Gov_flag[num3] + stateAdded.GDP_FedState_Gov_flag[num3];
				num3 = checked(num3 + 1);
			}
			while (num3 <= 1);
			this.EC_TotGovSUM += stateAdded.EC_TotGovSUM;
			this.EC_TotGovSUM_flag += stateAdded.EC_TotGovSUM_flag;
			this.Employment_TotGovSUM += stateAdded.Employment_TotGovSUM;
			this.Employment_TotGovSUM_flag += stateAdded.Employment_TotGovSUM_flag;
			this.T_TotGovSUM += stateAdded.T_TotGovSUM;
			this.T_TotGovSUM_flag += stateAdded.T_TotGovSUM_flag;
			this.GOS_TotGovSUM += stateAdded.GOS_TotGovSUM;
			this.GOS_TotGovSUM_flag += stateAdded.GOS_TotGovSUM_flag;
			this.GDP_TotGovSUM += stateAdded.GDP_TotGovSUM;
			this.GDP_TotGovSUM_flag += stateAdded.GDP_TotGovSUM_flag;
			this.PersonalIncome += stateAdded.PersonalIncome;
			this.DisposableIncome += stateAdded.DisposableIncome;
			this.TotSLEExp += stateAdded.TotSLEExp;
			this.TotFedExp += stateAdded.TotFedExp;
			this.PCE_Table += stateAdded.PCE_Table;
			this.StateSetup(ref this.fte);
		}

		public void scaleEmploymentByFTE(FTE_RATIO_CLASS fte_v2)
		{
			checked
			{
				int num = this.Employment_FedState.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.Employment_FedState_jobs[i] = this.Employment_FedState[i];
				}
				if (fte_v2.jobs2fte_ratio_fedstate.Count != this.Employment_FedState.Count)
				{
					Interaction.MsgBox("code 785721h \t" + Conversions.ToString(fte_v2.jobs2fte_ratio_fedstate.Count) + "\t" + Conversions.ToString(this.Employment_FedState.Count), MsgBoxStyle.OkOnly, null);
					Interaction.MsgBox(this.Name, MsgBoxStyle.OkOnly, null);
					Interaction.MsgBox(Conversions.ToString(this.GDP_base.Count) + "\t" + Conversions.ToString(this.GDP_FedState.Count) + "\t" + Conversions.ToString(this.GDP_imputed.Count) + "\t" + Conversions.ToString(this.GDP_TotalGov.Count), MsgBoxStyle.OkOnly, null);
				}
				this.Employment_FedState_fte = fte_v2.jobs2fte_ratio_fedstate * this.Employment_FedState;
				int num2 = this.Employment_TotalGov.Count - 1;
				for (int j = 0; j <= num2; j++)
				{
					this.Employment_TotalGov_jobs[j] = this.Employment_TotalGov[j];
				}
				this.Employment_TotalGov_fte = fte_v2.jobs2fte_ratio_totalgov * this.Employment_TotalGov;
			}
		}

		public void changeEmployment2FTE()
		{
		}

		public void changeEmployment2Jobs()
		{
		}

		public void setEmployment(ref Vector vv)
		{
			Vector vector = new Vector(vv.Count);
			switch (this.GovAggLevel.getFTE_Type())
			{
			case GenDataStatus.dataEmpStatus.FTE_BASED:
				vector = this.fte.fte2jobs_ratio_imputed * vv;
				break;
			case GenDataStatus.dataEmpStatus.JOB_BASED:
				vector = vv;
				break;
			}
			checked
			{
				int num = this.Employment_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.Employment_base[i] = vector[i];
				}
			}
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
			{
				checked
				{
					this.Employment_FedState_Gov[0] = vector[vector.Count - 2];
					this.Employment_FedState_Gov[1] = vector[vector.Count - 1];
				}
				this.Employment_TotGovSUM = this.Employment_FedState_Gov[0] + this.Employment_FedState_Gov[1];
				double num2 = this.Employment_ImpGov[1] + this.Employment_ImpGov[0];
				double num3 = this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
				this.Employment_ImpGov[0] = this.Employment_FedState_Gov[0] * this.Employment_ImpGov[0] / num2;
				this.Employment_ImpGov[1] = this.Employment_FedState_Gov[0] * this.Employment_ImpGov[1] / num2;
				this.Employment_ImpGov[2] = this.Employment_FedState_Gov[1] * this.Employment_ImpGov[2] / num3;
				this.Employment_ImpGov[3] = this.Employment_FedState_Gov[1] * this.Employment_ImpGov[3] / num3;
				break;
			}
			case GenDataStatus.dataStatusType.TotalGovernment:
			{
				double employment_TotGovSUM = this.Employment_TotGovSUM;
				this.Employment_TotGovSUM = vector[checked(vector.Count - 1)];
				this.Employment_ImpGov[0] = this.Employment_TotGovSUM * this.Employment_ImpGov[0] / employment_TotGovSUM;
				this.Employment_ImpGov[1] = this.Employment_TotGovSUM * this.Employment_ImpGov[1] / employment_TotGovSUM;
				this.Employment_ImpGov[2] = this.Employment_TotGovSUM * this.Employment_ImpGov[2] / employment_TotGovSUM;
				this.Employment_ImpGov[3] = this.Employment_TotGovSUM * this.Employment_ImpGov[3] / employment_TotGovSUM;
				this.Employment_FedState_Gov[0] = this.Employment_ImpGov[0] + this.Employment_ImpGov[1];
				this.Employment_FedState_Gov[1] = this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
				break;
			}
			case GenDataStatus.dataStatusType.ImputedGov:
				checked
				{
					this.Employment_ImpGov[3] = vector[vector.Count - 1];
					this.Employment_ImpGov[2] = vector[vector.Count - 2];
					this.Employment_ImpGov[1] = vector[vector.Count - 3];
					this.Employment_ImpGov[0] = vector[vector.Count - 4];
				}
				this.Employment_TotGovSUM = this.Employment_ImpGov[0] + this.Employment_ImpGov[1] + this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
				this.Employment_FedState_Gov[0] = this.Employment_ImpGov[0] + this.Employment_ImpGov[1];
				this.Employment_FedState_Gov[1] = this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
				break;
			default:
				checked
				{
					this.Employment_ImpGov[3] = vector[vector.Count - 1];
					this.Employment_ImpGov[2] = vector[vector.Count - 2];
					this.Employment_ImpGov[1] = vector[vector.Count - 3];
					this.Employment_ImpGov[0] = vector[vector.Count - 4];
				}
				this.Employment_TotGovSUM = this.Employment_ImpGov[0] + this.Employment_ImpGov[1] + this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
				this.Employment_FedState_Gov[0] = this.Employment_ImpGov[0] + this.Employment_ImpGov[1];
				this.Employment_FedState_Gov[1] = this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
				break;
			}
		}

		public Vector getEmployment()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				if (this.GovAggLevel.getFTE_Type() == GenDataStatus.dataEmpStatus.FTE_BASED)
				{
					return this.fte.jobs2fte_ratio_fedstate * this.Append2Vectors(ref this.Employment_base, ref this.Employment_FedState_Gov);
				}
				return this.Append2Vectors(ref this.Employment_base, ref this.Employment_FedState_Gov);
			case GenDataStatus.dataStatusType.TotalGovernment:
				if (this.GovAggLevel.getFTE_Type() == GenDataStatus.dataEmpStatus.FTE_BASED)
				{
					return this.fte.jobs2fte_ratio_totalgov * this.Append2Vectors(ref this.Employment_base, ref this.Employment_TotGovSUM);
				}
				return this.Append2Vectors(ref this.Employment_base, ref this.Employment_TotGovSUM);
			case GenDataStatus.dataStatusType.ImputedGov:
				if (this.GovAggLevel.getFTE_Type() == GenDataStatus.dataEmpStatus.FTE_BASED)
				{
					return this.fte.jobs2fte_ratio_imputed * this.Append2Vectors(ref this.Employment_base, ref this.Employment_ImpGov);
				}
				return this.Append2Vectors(ref this.Employment_base, ref this.Employment_ImpGov);
			default:
				if (this.GovAggLevel.getFTE_Type() == GenDataStatus.dataEmpStatus.FTE_BASED)
				{
					return this.fte.jobs2fte_ratio_imputed * this.Append2Vectors(ref this.Employment_base, ref this.Employment_ImpGov);
				}
				return this.Append2Vectors(ref this.Employment_base, ref this.Employment_ImpGov);
			}
		}

		public void setEmployment_AT(int ida, double vv)
		{
			double num = 0.0;
			switch (this.GovAggLevel.getFTE_Type())
			{
			case GenDataStatus.dataEmpStatus.FTE_BASED:
				num = this.fte.fte2jobs_ratio_imputed[ida] * vv;
				break;
			case GenDataStatus.dataEmpStatus.JOB_BASED:
				num = vv;
				break;
			}
			if (ida < this.Employment_base.Count)
			{
				this.Employment_base[ida] = num;
			}
			else
			{
				int num2 = checked(ida - this.Employment_base.Count);
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.FedStateLocal:
				{
					double num3 = this.Employment_FedState_Gov[num2];
					this.Employment_FedState_Gov[num2] = num;
					if (num2 == 0)
					{
						this.Employment_ImpGov[0] = this.Employment_FedState_Gov[0] * this.Employment_ImpGov[0] / num3;
						this.Employment_ImpGov[1] = this.Employment_FedState_Gov[0] * this.Employment_ImpGov[1] / num3;
					}
					if (num2 == 1)
					{
						this.Employment_ImpGov[2] = this.Employment_FedState_Gov[0] * this.Employment_ImpGov[2] / num3;
						this.Employment_ImpGov[3] = this.Employment_FedState_Gov[0] * this.Employment_ImpGov[3] / num3;
					}
					this.Employment_TotGovSUM = this.Employment_ImpGov[0] + this.Employment_ImpGov[1] + this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.TotalGovernment:
				{
					double employment_TotGovSUM = this.Employment_TotGovSUM;
					this.Employment_TotGovSUM = num;
					this.Employment_ImpGov[0] = this.Employment_TotGovSUM * this.Employment_ImpGov[0] / employment_TotGovSUM;
					this.Employment_ImpGov[1] = this.Employment_TotGovSUM * this.Employment_ImpGov[1] / employment_TotGovSUM;
					this.Employment_ImpGov[2] = this.Employment_TotGovSUM * this.Employment_ImpGov[2] / employment_TotGovSUM;
					this.Employment_ImpGov[3] = this.Employment_TotGovSUM * this.Employment_ImpGov[3] / employment_TotGovSUM;
					this.Employment_FedState_Gov[0] = this.Employment_ImpGov[0] + this.Employment_ImpGov[1];
					this.Employment_FedState_Gov[1] = this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.ImputedGov:
					this.Employment_ImpGov[num2] = num;
					this.Employment_TotGovSUM = this.Employment_ImpGov[0] + this.Employment_ImpGov[1] + this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
					this.Employment_FedState_Gov[0] = this.Employment_ImpGov[0] + this.Employment_ImpGov[1];
					this.Employment_FedState_Gov[1] = this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
					break;
				default:
					this.Employment_ImpGov[num2] = num;
					this.Employment_TotGovSUM = this.Employment_ImpGov[0] + this.Employment_ImpGov[1] + this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
					this.Employment_FedState_Gov[0] = this.Employment_ImpGov[0] + this.Employment_ImpGov[1];
					this.Employment_FedState_Gov[1] = this.Employment_ImpGov[2] + this.Employment_ImpGov[3];
					break;
				}
			}
		}

		public Vector getEmployment_Flag()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.Employment_base_flag, ref this.Employment_FedState_Gov_flag);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.Employment_base_flag, ref this.Employment_TotGovSUM_flag);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.Employment_base_flag, ref this.Employment_ImpGov_flag);
			default:
				return this.Append2Vectors(ref this.Employment_base_flag, ref this.Employment_ImpGov_flag);
			}
		}

		public Vector getEC()
		{
			Vector vector = this.Append2Vectors(ref this.EC_base, ref this.EC_ImpGov);
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.EC_base, ref this.EC_FedState_Gov);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.EC_base, ref this.EC_TotGovSUM);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.EC_base, ref this.EC_ImpGov);
			default:
				return this.Append2Vectors(ref this.EC_base, ref this.EC_ImpGov);
			}
		}

		public void setEC(ref Vector v1)
		{
			checked
			{
				int num = this.EC_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.EC_base[i] = v1[i];
				}
			}
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
			{
				checked
				{
					this.EC_FedState_Gov[0] = v1[v1.Count - 2];
					this.EC_FedState_Gov[1] = v1[v1.Count - 1];
				}
				this.EC_TotGovSUM = this.EC_FedState_Gov[0] + this.EC_FedState_Gov[1];
				double num2 = this.EC_ImpGov[1] + this.EC_ImpGov[0];
				double num3 = this.EC_ImpGov[2] + this.EC_ImpGov[3];
				this.EC_ImpGov[0] = this.EC_FedState_Gov[0] * this.EC_ImpGov[0] / num2;
				this.EC_ImpGov[1] = this.EC_FedState_Gov[0] * this.EC_ImpGov[1] / num2;
				this.EC_ImpGov[2] = this.EC_FedState_Gov[1] * this.EC_ImpGov[2] / num3;
				this.EC_ImpGov[3] = this.EC_FedState_Gov[1] * this.EC_ImpGov[3] / num3;
				break;
			}
			case GenDataStatus.dataStatusType.TotalGovernment:
			{
				double eC_TotGovSUM = this.EC_TotGovSUM;
				this.EC_TotGovSUM = v1[checked(v1.Count - 1)];
				this.EC_ImpGov[0] = this.EC_TotGovSUM * this.EC_ImpGov[0] / eC_TotGovSUM;
				this.EC_ImpGov[1] = this.EC_TotGovSUM * this.EC_ImpGov[1] / eC_TotGovSUM;
				this.EC_ImpGov[2] = this.EC_TotGovSUM * this.EC_ImpGov[2] / eC_TotGovSUM;
				this.EC_ImpGov[3] = this.EC_TotGovSUM * this.EC_ImpGov[3] / eC_TotGovSUM;
				this.EC_FedState_Gov[0] = this.EC_ImpGov[0] + this.EC_ImpGov[1];
				this.EC_FedState_Gov[1] = this.EC_ImpGov[2] + this.EC_ImpGov[3];
				break;
			}
			case GenDataStatus.dataStatusType.ImputedGov:
				checked
				{
					this.EC_ImpGov[3] = v1[v1.Count - 1];
					this.EC_ImpGov[2] = v1[v1.Count - 2];
					this.EC_ImpGov[1] = v1[v1.Count - 3];
					this.EC_ImpGov[0] = v1[v1.Count - 4];
				}
				this.EC_TotGovSUM = this.EC_ImpGov[0] + this.EC_ImpGov[1] + this.EC_ImpGov[2] + this.EC_ImpGov[3];
				this.EC_FedState_Gov[0] = this.EC_ImpGov[0] + this.EC_ImpGov[1];
				this.EC_FedState_Gov[1] = this.EC_ImpGov[2] + this.EC_ImpGov[3];
				break;
			default:
				checked
				{
					this.EC_ImpGov[3] = v1[v1.Count - 1];
					this.EC_ImpGov[2] = v1[v1.Count - 2];
					this.EC_ImpGov[1] = v1[v1.Count - 3];
					this.EC_ImpGov[0] = v1[v1.Count - 4];
				}
				this.EC_TotGovSUM = this.EC_ImpGov[0] + this.EC_ImpGov[1] + this.EC_ImpGov[2] + this.EC_ImpGov[3];
				this.EC_FedState_Gov[0] = this.EC_ImpGov[0] + this.EC_ImpGov[1];
				this.EC_FedState_Gov[1] = this.EC_ImpGov[2] + this.EC_ImpGov[3];
				break;
			}
		}

		public void setEC_AT(int ida, double va)
		{
			if (ida < this.EC_base.Count)
			{
				this.EC_base[ida] = va;
			}
			else
			{
				int num = checked(ida - this.EC_base.Count);
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.FedStateLocal:
				{
					double num2 = this.EC_FedState_Gov[num];
					this.EC_FedState_Gov[num] = va;
					if (num == 0)
					{
						this.EC_ImpGov[0] = this.EC_FedState_Gov[0] * this.EC_ImpGov[0] / num2;
						this.EC_ImpGov[1] = this.EC_FedState_Gov[0] * this.EC_ImpGov[1] / num2;
					}
					if (num == 1)
					{
						this.EC_ImpGov[2] = this.EC_FedState_Gov[0] * this.EC_ImpGov[2] / num2;
						this.EC_ImpGov[3] = this.EC_FedState_Gov[0] * this.EC_ImpGov[3] / num2;
					}
					this.EC_TotGovSUM = this.EC_ImpGov[0] + this.EC_ImpGov[1] + this.EC_ImpGov[2] + this.EC_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.TotalGovernment:
				{
					double eC_TotGovSUM = this.EC_TotGovSUM;
					this.EC_TotGovSUM = va;
					this.EC_ImpGov[0] = this.EC_TotGovSUM * this.EC_ImpGov[0] / eC_TotGovSUM;
					this.EC_ImpGov[1] = this.EC_TotGovSUM * this.EC_ImpGov[1] / eC_TotGovSUM;
					this.EC_ImpGov[2] = this.EC_TotGovSUM * this.EC_ImpGov[2] / eC_TotGovSUM;
					this.EC_ImpGov[3] = this.EC_TotGovSUM * this.EC_ImpGov[3] / eC_TotGovSUM;
					this.EC_FedState_Gov[0] = this.EC_ImpGov[0] + this.EC_ImpGov[1];
					this.EC_FedState_Gov[1] = this.EC_ImpGov[2] + this.EC_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.ImputedGov:
					this.EC_ImpGov[num] = va;
					this.EC_TotGovSUM = this.EC_ImpGov[0] + this.EC_ImpGov[1] + this.EC_ImpGov[2] + this.EC_ImpGov[3];
					this.EC_FedState_Gov[0] = this.EC_ImpGov[0] + this.EC_ImpGov[1];
					this.EC_FedState_Gov[1] = this.EC_ImpGov[2] + this.EC_ImpGov[3];
					break;
				default:
					this.EC_ImpGov[num] = va;
					this.EC_TotGovSUM = this.EC_ImpGov[0] + this.EC_ImpGov[1] + this.EC_ImpGov[2] + this.EC_ImpGov[3];
					this.EC_FedState_Gov[0] = this.EC_ImpGov[0] + this.EC_ImpGov[1];
					this.EC_FedState_Gov[1] = this.EC_ImpGov[2] + this.EC_ImpGov[3];
					break;
				}
			}
		}

		public Vector getEC_Flag()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.EC_base_flag, ref this.EC_FedState_Gov_flag);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.EC_base_flag, ref this.EC_TotGovSUM_flag);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.EC_base_flag, ref this.EC_ImpGov_flag);
			default:
				return this.Append2Vectors(ref this.EC_base_flag, ref this.EC_ImpGov_flag);
			}
		}

		public Vector getCompensationRate()
		{
			Vector vector;
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				vector = this.getEC() * (Vector)this.GovAggLevel.getCompRateScaleFactorVECTOR(this.getEC().Count) / this.getEmployment();
				checked
				{
					int num4 = vector.Count - 1;
					for (int l = 0; l <= num4; l++)
					{
						if (double.IsNaN(vector[l]))
						{
							vector[l] = 0.0;
						}
					}
					break;
				}
			case GenDataStatus.dataStatusType.TotalGovernment:
				vector = this.getEC() * (Vector)this.GovAggLevel.getCompRateScaleFactorVECTOR(this.getEC().Count) / this.getEmployment();
				checked
				{
					int num2 = vector.Count - 1;
					for (int j = 0; j <= num2; j++)
					{
						if (double.IsNaN(vector[j]))
						{
							vector[j] = 0.0;
						}
					}
					break;
				}
			case GenDataStatus.dataStatusType.ImputedGov:
				vector = this.getEC() * (Vector)this.GovAggLevel.getCompRateScaleFactorVECTOR(this.getEC().Count) / this.getEmployment();
				checked
				{
					int num3 = vector.Count - 1;
					for (int k = 0; k <= num3; k++)
					{
						if (double.IsNaN(vector[k]))
						{
							vector[k] = 0.0;
						}
					}
					break;
				}
			default:
				vector = this.getEC() * (Vector)this.GovAggLevel.getCompRateScaleFactorVECTOR(this.getEC().Count) / this.getEmployment();
				checked
				{
					int num = vector.Count - 1;
					for (int i = 0; i <= num; i++)
					{
						if (double.IsNaN(vector[i]))
						{
							vector[i] = 0.0;
						}
					}
					break;
				}
			}
			return vector;
		}

		public Vector getCompensationRate_Flag()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.EC_FedState_flag + this.Employment_FedState_flag;
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.EC_TotalGov_flag + this.Employment_TotalGov_flag;
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.EC_Imputed_flag + this.Employment_Imputed_flag;
			default:
				return this.EC_Imputed_flag + this.Employment_Imputed_flag;
			}
		}

		public Vector getT()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.T_base, ref this.T_FedState_Gov);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.T_base, ref this.T_TotGovSUM);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.T_base, ref this.T_ImpGov);
			default:
				return this.Append2Vectors(ref this.T_base, ref this.T_ImpGov);
			}
		}

		public void setT(Vector v1)
		{
			checked
			{
				int num = this.T_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.T_base[i] = v1[i];
				}
			}
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
			{
				checked
				{
					this.T_FedState_Gov[0] = v1[v1.Count - 2];
					this.T_FedState_Gov[1] = v1[v1.Count - 1];
				}
				this.T_TotGovSUM = this.T_FedState_Gov[0] + this.T_FedState_Gov[1];
				double num2 = this.T_ImpGov[1] + this.T_ImpGov[0];
				double num3 = this.T_ImpGov[2] + this.T_ImpGov[3];
				this.T_ImpGov[0] = this.T_FedState_Gov[0] * this.T_ImpGov[0] / num2;
				this.T_ImpGov[1] = this.T_FedState_Gov[0] * this.T_ImpGov[1] / num2;
				this.T_ImpGov[2] = this.T_FedState_Gov[1] * this.T_ImpGov[2] / num3;
				this.T_ImpGov[3] = this.T_FedState_Gov[1] * this.T_ImpGov[3] / num3;
				break;
			}
			case GenDataStatus.dataStatusType.TotalGovernment:
			{
				double t_TotGovSUM = this.T_TotGovSUM;
				this.T_TotGovSUM = v1[checked(v1.Count - 1)];
				this.T_ImpGov[0] = this.T_TotGovSUM * this.T_ImpGov[0] / t_TotGovSUM;
				this.T_ImpGov[1] = this.T_TotGovSUM * this.T_ImpGov[1] / t_TotGovSUM;
				this.T_ImpGov[2] = this.T_TotGovSUM * this.T_ImpGov[2] / t_TotGovSUM;
				this.T_ImpGov[3] = this.T_TotGovSUM * this.T_ImpGov[3] / t_TotGovSUM;
				this.T_FedState_Gov[0] = this.T_ImpGov[0] + this.T_ImpGov[1];
				this.T_FedState_Gov[1] = this.T_ImpGov[2] + this.T_ImpGov[3];
				break;
			}
			case GenDataStatus.dataStatusType.ImputedGov:
				checked
				{
					this.T_ImpGov[3] = v1[v1.Count - 1];
					this.T_ImpGov[2] = v1[v1.Count - 2];
					this.T_ImpGov[1] = v1[v1.Count - 3];
					this.T_ImpGov[0] = v1[v1.Count - 4];
				}
				this.T_TotGovSUM = this.T_ImpGov[0] + this.T_ImpGov[1] + this.T_ImpGov[2] + this.T_ImpGov[3];
				this.T_FedState_Gov[0] = this.T_ImpGov[0] + this.T_ImpGov[1];
				this.T_FedState_Gov[1] = this.T_ImpGov[2] + this.T_ImpGov[3];
				break;
			default:
				checked
				{
					this.T_ImpGov[3] = v1[v1.Count - 1];
					this.T_ImpGov[2] = v1[v1.Count - 2];
					this.T_ImpGov[1] = v1[v1.Count - 3];
					this.T_ImpGov[0] = v1[v1.Count - 4];
				}
				this.T_TotGovSUM = this.T_ImpGov[0] + this.T_ImpGov[1] + this.T_ImpGov[2] + this.T_ImpGov[3];
				this.T_FedState_Gov[0] = this.T_ImpGov[0] + this.T_ImpGov[1];
				this.T_FedState_Gov[1] = this.T_ImpGov[2] + this.T_ImpGov[3];
				break;
			}
		}

		public void setT_at(int ida, double va)
		{
			if (ida < this.T_base.Count)
			{
				this.T_base[ida] = va;
			}
			else
			{
				int num = checked(ida - this.T_base.Count);
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.FedStateLocal:
				{
					double num2 = this.T_FedState_Gov[num];
					this.T_FedState_Gov[num] = va;
					if (num == 0)
					{
						this.T_ImpGov[0] = this.T_FedState_Gov[0] * this.T_ImpGov[0] / num2;
						this.T_ImpGov[1] = this.T_FedState_Gov[0] * this.T_ImpGov[1] / num2;
					}
					if (num == 1)
					{
						this.T_ImpGov[2] = this.T_FedState_Gov[0] * this.T_ImpGov[2] / num2;
						this.T_ImpGov[3] = this.T_FedState_Gov[0] * this.T_ImpGov[3] / num2;
					}
					this.T_TotGovSUM = this.T_ImpGov[0] + this.T_ImpGov[1] + this.T_ImpGov[2] + this.T_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.TotalGovernment:
				{
					double t_TotGovSUM = this.T_TotGovSUM;
					this.T_TotGovSUM = va;
					this.T_ImpGov[0] = this.T_TotGovSUM * this.T_ImpGov[0] / t_TotGovSUM;
					this.T_ImpGov[1] = this.T_TotGovSUM * this.T_ImpGov[1] / t_TotGovSUM;
					this.T_ImpGov[2] = this.T_TotGovSUM * this.T_ImpGov[2] / t_TotGovSUM;
					this.T_ImpGov[3] = this.T_TotGovSUM * this.T_ImpGov[3] / t_TotGovSUM;
					this.T_FedState_Gov[0] = this.T_ImpGov[0] + this.T_ImpGov[1];
					this.T_FedState_Gov[1] = this.T_ImpGov[2] + this.T_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.ImputedGov:
					this.T_ImpGov[num] = va;
					this.T_TotGovSUM = this.T_ImpGov[0] + this.T_ImpGov[1] + this.T_ImpGov[2] + this.T_ImpGov[3];
					this.T_FedState_Gov[0] = this.T_ImpGov[0] + this.T_ImpGov[1];
					this.T_FedState_Gov[1] = this.T_ImpGov[2] + this.T_ImpGov[3];
					break;
				}
			}
		}

		public Vector getT_Flag()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.T_base_flag, ref this.T_FedState_Gov_flag);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.T_base_flag, ref this.T_TotGovSUM_flag);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.T_base_flag, ref this.T_ImpGov_flag);
			default:
				return this.Append2Vectors(ref this.T_base_flag, ref this.T_ImpGov_flag);
			}
		}

		public Vector getGOS()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.GOS_base, ref this.GOS_FedState_Gov);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.GOS_base, ref this.GOS_TotGovSUM);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.GOS_base, ref this.GOS_ImpGov);
			default:
				return this.Append2Vectors(ref this.GOS_base, ref this.GOS_ImpGov);
			}
		}

		public void setGOS(Vector v1)
		{
			checked
			{
				int num = this.GOS_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.GOS_base[i] = v1[i];
				}
			}
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
			{
				checked
				{
					this.GOS_FedState_Gov[0] = v1[v1.Count - 2];
					this.GOS_FedState_Gov[1] = v1[v1.Count - 1];
				}
				this.GOS_TotGovSUM = this.GOS_FedState_Gov[0] + this.GOS_FedState_Gov[1];
				double num2 = this.GOS_ImpGov[1] + this.GOS_ImpGov[0];
				double num3 = this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
				this.GOS_ImpGov[0] = this.GOS_FedState_Gov[0] * this.GOS_ImpGov[0] / num2;
				this.GOS_ImpGov[1] = this.GOS_FedState_Gov[0] * this.GOS_ImpGov[1] / num2;
				this.GOS_ImpGov[2] = this.GOS_FedState_Gov[1] * this.GOS_ImpGov[2] / num3;
				this.GOS_ImpGov[3] = this.GOS_FedState_Gov[1] * this.GOS_ImpGov[3] / num3;
				break;
			}
			case GenDataStatus.dataStatusType.TotalGovernment:
			{
				double gOS_TotGovSUM = this.GOS_TotGovSUM;
				this.GOS_TotGovSUM = v1[checked(v1.Count - 1)];
				this.GOS_ImpGov[0] = this.GOS_TotGovSUM * this.GOS_ImpGov[0] / gOS_TotGovSUM;
				this.GOS_ImpGov[1] = this.GOS_TotGovSUM * this.GOS_ImpGov[1] / gOS_TotGovSUM;
				this.GOS_ImpGov[2] = this.GOS_TotGovSUM * this.GOS_ImpGov[2] / gOS_TotGovSUM;
				this.GOS_ImpGov[3] = this.GOS_TotGovSUM * this.GOS_ImpGov[3] / gOS_TotGovSUM;
				this.GOS_FedState_Gov[0] = this.GOS_ImpGov[0] + this.GOS_ImpGov[1];
				this.GOS_FedState_Gov[1] = this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
				break;
			}
			case GenDataStatus.dataStatusType.ImputedGov:
				checked
				{
					this.GOS_ImpGov[3] = v1[v1.Count - 1];
					this.GOS_ImpGov[2] = v1[v1.Count - 2];
					this.GOS_ImpGov[1] = v1[v1.Count - 3];
					this.GOS_ImpGov[0] = v1[v1.Count - 4];
				}
				this.GOS_TotGovSUM = this.GOS_ImpGov[0] + this.GOS_ImpGov[1] + this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
				this.GOS_FedState_Gov[0] = this.GOS_ImpGov[0] + this.GOS_ImpGov[1];
				this.GOS_FedState_Gov[1] = this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
				break;
			default:
				checked
				{
					this.GOS_ImpGov[3] = v1[v1.Count - 1];
					this.GOS_ImpGov[2] = v1[v1.Count - 2];
					this.GOS_ImpGov[1] = v1[v1.Count - 3];
					this.GOS_ImpGov[0] = v1[v1.Count - 4];
				}
				this.GOS_TotGovSUM = this.GOS_ImpGov[0] + this.GOS_ImpGov[1] + this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
				this.GOS_FedState_Gov[0] = this.GOS_ImpGov[0] + this.GOS_ImpGov[1];
				this.GOS_FedState_Gov[1] = this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
				break;
			}
		}

		public void setGOS_at(int ida, double va)
		{
			if (ida < this.GOS_base.Count)
			{
				this.GOS_base[ida] = va;
			}
			else
			{
				int num = checked(ida - this.GOS_base.Count);
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.FedStateLocal:
				{
					double num2 = this.GOS_FedState_Gov[num];
					this.GOS_FedState_Gov[num] = va;
					if (num == 0)
					{
						this.GOS_ImpGov[0] = this.GOS_FedState_Gov[0] * this.GOS_ImpGov[0] / num2;
						this.GOS_ImpGov[1] = this.GOS_FedState_Gov[0] * this.GOS_ImpGov[1] / num2;
					}
					if (num == 1)
					{
						this.GOS_ImpGov[2] = this.GOS_FedState_Gov[0] * this.GOS_ImpGov[2] / num2;
						this.GOS_ImpGov[3] = this.GOS_FedState_Gov[0] * this.GOS_ImpGov[3] / num2;
					}
					this.GOS_TotGovSUM = this.GOS_ImpGov[0] + this.GOS_ImpGov[1] + this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.TotalGovernment:
				{
					double gOS_TotGovSUM = this.GOS_TotGovSUM;
					this.GOS_TotGovSUM = va;
					this.GOS_ImpGov[0] = this.GOS_TotGovSUM * this.GOS_ImpGov[0] / gOS_TotGovSUM;
					this.GOS_ImpGov[1] = this.GOS_TotGovSUM * this.GOS_ImpGov[1] / gOS_TotGovSUM;
					this.GOS_ImpGov[2] = this.GOS_TotGovSUM * this.GOS_ImpGov[2] / gOS_TotGovSUM;
					this.GOS_ImpGov[3] = this.GOS_TotGovSUM * this.GOS_ImpGov[3] / gOS_TotGovSUM;
					this.GOS_FedState_Gov[0] = this.GOS_ImpGov[0] + this.GOS_ImpGov[1];
					this.GOS_FedState_Gov[1] = this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.ImputedGov:
					this.GOS_ImpGov[num] = va;
					this.GOS_TotGovSUM = this.GOS_ImpGov[0] + this.GOS_ImpGov[1] + this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
					this.GOS_FedState_Gov[0] = this.GOS_ImpGov[0] + this.GOS_ImpGov[1];
					this.GOS_FedState_Gov[1] = this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
					break;
				default:
					this.GOS_ImpGov[num] = va;
					this.GOS_TotGovSUM = this.GOS_ImpGov[0] + this.GOS_ImpGov[1] + this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
					this.GOS_FedState_Gov[0] = this.GOS_ImpGov[0] + this.GOS_ImpGov[1];
					this.GOS_FedState_Gov[1] = this.GOS_ImpGov[2] + this.GOS_ImpGov[3];
					break;
				}
			}
		}

		public Vector getGOS_Flag()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.GOS_base_flag, ref this.GOS_FedState_Gov_flag);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.GOS_base_flag, ref this.GOS_TotGovSUM_flag);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.GOS_base_flag, ref this.GOS_ImpGov_flag);
			default:
				return this.Append2Vectors(ref this.GOS_base_flag, ref this.GOS_ImpGov_flag);
			}
		}

		public Vector getGDP()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.GDP_base, ref this.GDP_FedState_Gov);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.GDP_base, ref this.GDP_TotGovSUM);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.GDP_base, ref this.GDP_ImpGov);
			default:
				return this.Append2Vectors(ref this.GDP_base, ref this.GDP_ImpGov);
			}
		}

		public void setGDP(Vector v1)
		{
			checked
			{
				int num = this.GDP_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.GDP_base[i] = v1[i];
				}
			}
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
			{
				checked
				{
					this.GDP_FedState_Gov[0] = v1[v1.Count - 2];
					this.GDP_FedState_Gov[1] = v1[v1.Count - 1];
				}
				this.GDP_TotGovSUM = this.GDP_FedState_Gov[0] + this.GDP_FedState_Gov[1];
				double num2 = this.GDP_ImpGov[1] + this.GDP_ImpGov[0];
				double num3 = this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
				this.GDP_ImpGov[0] = this.GDP_FedState_Gov[0] * this.GDP_ImpGov[0] / num2;
				this.GDP_ImpGov[1] = this.GDP_FedState_Gov[0] * this.GDP_ImpGov[1] / num2;
				this.GDP_ImpGov[2] = this.GDP_FedState_Gov[1] * this.GDP_ImpGov[2] / num3;
				this.GDP_ImpGov[3] = this.GDP_FedState_Gov[1] * this.GDP_ImpGov[3] / num3;
				break;
			}
			case GenDataStatus.dataStatusType.TotalGovernment:
			{
				double gDP_TotGovSUM = this.GDP_TotGovSUM;
				this.GDP_TotGovSUM = v1[checked(v1.Count - 1)];
				this.GDP_ImpGov[0] = this.GDP_TotGovSUM * this.GDP_ImpGov[0] / gDP_TotGovSUM;
				this.GDP_ImpGov[1] = this.GDP_TotGovSUM * this.GDP_ImpGov[1] / gDP_TotGovSUM;
				this.GDP_ImpGov[2] = this.GDP_TotGovSUM * this.GDP_ImpGov[2] / gDP_TotGovSUM;
				this.GDP_ImpGov[3] = this.GDP_TotGovSUM * this.GDP_ImpGov[3] / gDP_TotGovSUM;
				this.GDP_FedState_Gov[0] = this.GDP_ImpGov[0] + this.GDP_ImpGov[1];
				this.GDP_FedState_Gov[1] = this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
				break;
			}
			case GenDataStatus.dataStatusType.ImputedGov:
				checked
				{
					this.GDP_ImpGov[3] = v1[v1.Count - 1];
					this.GDP_ImpGov[2] = v1[v1.Count - 2];
					this.GDP_ImpGov[1] = v1[v1.Count - 3];
					this.GDP_ImpGov[0] = v1[v1.Count - 4];
				}
				this.GDP_TotGovSUM = this.GDP_ImpGov[0] + this.GDP_ImpGov[1] + this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
				this.GDP_FedState_Gov[0] = this.GDP_ImpGov[0] + this.GDP_ImpGov[1];
				this.GDP_FedState_Gov[1] = this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
				break;
			default:
				checked
				{
					this.GDP_ImpGov[3] = v1[v1.Count - 1];
					this.GDP_ImpGov[2] = v1[v1.Count - 2];
					this.GDP_ImpGov[1] = v1[v1.Count - 3];
					this.GDP_ImpGov[0] = v1[v1.Count - 4];
				}
				this.GDP_TotGovSUM = this.GDP_ImpGov[0] + this.GDP_ImpGov[1] + this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
				this.GDP_FedState_Gov[0] = this.GDP_ImpGov[0] + this.GDP_ImpGov[1];
				this.GDP_FedState_Gov[1] = this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
				break;
			}
		}

		public void setGDP_at(int ida, double va)
		{
			if (ida < this.GDP_base.Count)
			{
				this.GDP_base[ida] = va;
			}
			else
			{
				int num = checked(ida - this.GDP_base.Count);
				switch (this.GovAggLevel.getAggLevel())
				{
				case GenDataStatus.dataStatusType.FedStateLocal:
				{
					double num2 = this.GDP_FedState_Gov[num];
					this.GDP_FedState_Gov[num] = va;
					if (num == 0)
					{
						this.GDP_ImpGov[0] = this.GDP_FedState_Gov[0] * this.GDP_ImpGov[0] / num2;
						this.GDP_ImpGov[1] = this.GDP_FedState_Gov[0] * this.GDP_ImpGov[1] / num2;
					}
					if (num == 1)
					{
						this.GDP_ImpGov[2] = this.GDP_FedState_Gov[0] * this.GDP_ImpGov[2] / num2;
						this.GDP_ImpGov[3] = this.GDP_FedState_Gov[0] * this.GDP_ImpGov[3] / num2;
					}
					this.GDP_TotGovSUM = this.GDP_ImpGov[0] + this.GDP_ImpGov[1] + this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.TotalGovernment:
				{
					double gDP_TotGovSUM = this.GDP_TotGovSUM;
					this.GDP_TotGovSUM = va;
					this.GDP_ImpGov[0] = this.GDP_TotGovSUM * this.GDP_ImpGov[0] / gDP_TotGovSUM;
					this.GDP_ImpGov[1] = this.GDP_TotGovSUM * this.GDP_ImpGov[1] / gDP_TotGovSUM;
					this.GDP_ImpGov[2] = this.GDP_TotGovSUM * this.GDP_ImpGov[2] / gDP_TotGovSUM;
					this.GDP_ImpGov[3] = this.GDP_TotGovSUM * this.GDP_ImpGov[3] / gDP_TotGovSUM;
					this.GDP_FedState_Gov[0] = this.GDP_ImpGov[0] + this.GDP_ImpGov[1];
					this.GDP_FedState_Gov[1] = this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
					break;
				}
				case GenDataStatus.dataStatusType.ImputedGov:
					this.GDP_ImpGov[num] = va;
					this.GDP_TotGovSUM = this.GDP_ImpGov[0] + this.GDP_ImpGov[1] + this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
					this.GDP_FedState_Gov[0] = this.GDP_ImpGov[0] + this.GDP_ImpGov[1];
					this.GDP_FedState_Gov[1] = this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
					break;
				default:
					this.GDP_ImpGov[num] = va;
					this.GDP_TotGovSUM = this.GDP_ImpGov[0] + this.GDP_ImpGov[1] + this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
					this.GDP_FedState_Gov[0] = this.GDP_ImpGov[0] + this.GDP_ImpGov[1];
					this.GDP_FedState_Gov[1] = this.GDP_ImpGov[2] + this.GDP_ImpGov[3];
					break;
				}
			}
		}

		public Vector getGDP_Flag()
		{
			switch (this.GovAggLevel.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				return this.Append2Vectors(ref this.GDP_base_flag, ref this.GDP_FedState_Gov_flag);
			case GenDataStatus.dataStatusType.TotalGovernment:
				return this.Append2Vectors(ref this.GDP_base_flag, ref this.GDP_TotGovSUM_flag);
			case GenDataStatus.dataStatusType.ImputedGov:
				return this.Append2Vectors(ref this.GDP_base_flag, ref this.GDP_ImpGov_flag);
			default:
				return this.Append2Vectors(ref this.GDP_base_flag, ref this.GDP_ImpGov_flag);
			}
		}

		public object Clone()
		{
			StateData stateData = new StateData(this.Name, this.nrec_base);
			StateData stateData2 = stateData;
			checked
			{
				stateData2.nrec_base = this.nrec_base + 0;
				stateData2.nrec_imputed = this.nrec_imputed + 0;
				stateData2.nrec_totgov = this.nrec_totgov + 0;
				stateData2.nrec_fedstate = this.nrec_fedstate + 0;
			}
			stateData2.fte = (FTE_RATIO_CLASS)this.fte.Clone();
			stateData2.Name = this.Name;
			stateData2.GovAggLevel = (GenDataStatus)this.GovAggLevel.Clone();
			stateData2.PersonalIncome = this.PersonalIncome + 0.0;
			stateData2.DisposableIncome = this.DisposableIncome + 0.0;
			stateData2.TotFedExp = this.TotFedExp + 0.0;
			stateData2.TotSLEExp = this.TotSLEExp + 0.0;
			stateData2.GDP_ImpGov = this.GDP_ImpGov.Clone();
			stateData2.GDP_ImpGov_flag = this.GDP_ImpGov_flag.Clone();
			stateData2.GDP_base = this.GDP_base.Clone();
			stateData2.GDP_base_flag = this.GDP_base_flag.Clone();
			stateData2.GDP_imputed = this.GDP_imputed.Clone();
			stateData2.GDP_imputed_flag = this.GDP_imputed_flag.Clone();
			stateData2.GDP_TotGovSUM = this.GDP_TotGovSUM + 0.0;
			stateData2.GDP_TotGovSUM_flag = this.GDP_TotGovSUM_flag + 0.0;
			stateData2.GDP_FedState = this.GDP_FedState.Clone();
			stateData2.GDP_FedState_flag = this.GDP_FedState_flag.Clone();
			stateData2.GDP_FedState_Gov = this.GDP_FedState_Gov.Clone();
			stateData2.GDP_FedState_Gov_flag = this.GDP_FedState_Gov_flag.Clone();
			stateData2.GDP_TotalGov = this.GDP_TotalGov.Clone();
			stateData2.GDP_TotalGov_flag = this.GDP_TotalGov_flag.Clone();
			stateData2.GOS_ImpGov = this.GOS_ImpGov.Clone();
			stateData2.GOS_ImpGov_flag = this.GOS_ImpGov_flag.Clone();
			stateData2.GOS_base = this.GOS_base.Clone();
			stateData2.GOS_base_flag = this.GOS_base_flag.Clone();
			stateData2.GOS_imputed = this.GOS_imputed.Clone();
			stateData2.GOS_imputed_flag = this.GOS_imputed_flag.Clone();
			stateData2.GOS_TotGovSUM = this.GOS_TotGovSUM + 0.0;
			stateData2.GOS_TotGovSUM_flag = this.GOS_TotGovSUM_flag + 0.0;
			stateData2.GOS_FedState = this.GOS_FedState.Clone();
			stateData2.GOS_FedState_flag = this.GOS_FedState_flag.Clone();
			stateData2.GOS_FedState_Gov = this.GOS_FedState_Gov.Clone();
			stateData2.GOS_FedState_Gov_flag = this.GOS_FedState_Gov_flag.Clone();
			stateData2.GOS_TotalGov = this.GOS_TotalGov.Clone();
			stateData2.GOS_TotalGov_flag = this.GOS_TotalGov_flag.Clone();
			stateData2.CompensationRate_ImpGov = this.CompensationRate_ImpGov.Clone();
			stateData2.CompensationRate_ImpGov_flag = this.CompensationRate_ImpGov_flag.Clone();
			stateData2.CompensationRate_base = this.CompensationRate_base.Clone();
			stateData2.CompensationRate_base_flag = this.CompensationRate_base_flag.Clone();
			stateData2.CompensationRate_Imputed = this.CompensationRate_Imputed.Clone();
			stateData2.CompensationRate_Imputed_flag = this.CompensationRate_Imputed_flag.Clone();
			stateData2.CompensationRate_TotGovSUM = this.CompensationRate_TotGovSUM + 0.0;
			stateData2.CompensationRate_TotGovSUM_flag = this.CompensationRate_TotGovSUM_flag + 0.0;
			stateData2.CompensationRate_FedState = this.CompensationRate_FedState.Clone();
			stateData2.CompensationRate_FedState_flag = this.CompensationRate_FedState_flag.Clone();
			stateData2.CompensationRate_FedState_Gov = this.CompensationRate_FedState_Gov.Clone();
			stateData2.CompensationRate_FedState_Gov_flag = this.CompensationRate_FedState_Gov_flag.Clone();
			stateData2.CompensationRate_TotalGov = this.CompensationRate_TotalGov.Clone();
			stateData2.CompensationRate_TotalGov_flag = this.CompensationRate_TotalGov_flag.Clone();
			stateData2.EC_ImpGov = this.EC_ImpGov.Clone();
			stateData2.EC_ImpGov_flag = this.EC_ImpGov_flag.Clone();
			stateData2.EC_base = this.EC_base.Clone();
			stateData2.EC_base_flag = this.EC_base_flag.Clone();
			stateData2.EC_Imputed = this.EC_Imputed.Clone();
			stateData2.EC_Imputed_flag = this.EC_Imputed_flag.Clone();
			stateData2.EC_TotGovSUM = this.EC_TotGovSUM + 0.0;
			stateData2.EC_TotGovSUM_flag = this.EC_TotGovSUM_flag + 0.0;
			stateData2.EC_FedState = this.EC_FedState.Clone();
			stateData2.EC_FedState_flag = this.EC_FedState_flag.Clone();
			stateData2.EC_FedState_Gov = this.EC_FedState_Gov.Clone();
			stateData2.EC_FedState_Gov_flag = this.EC_FedState_Gov_flag.Clone();
			stateData2.EC_TotalGov = this.EC_TotalGov.Clone();
			stateData2.EC_TotalGov_flag = this.EC_TotalGov_flag.Clone();
			stateData2.Employment_ImpGov = this.Employment_ImpGov.Clone();
			stateData2.Employment_ImpGov_flag = this.Employment_ImpGov_flag.Clone();
			stateData2.Employment_base = this.Employment_base.Clone();
			stateData2.Employment_base_flag = this.Employment_base_flag.Clone();
			stateData2.Employment_Imputed = this.Employment_Imputed.Clone();
			stateData2.Employment_Imputed_flag = this.Employment_Imputed_flag.Clone();
			stateData2.Employment_TotGovSUM = this.Employment_TotGovSUM + 0.0;
			stateData2.Employment_TotGovSUM_flag = this.Employment_TotGovSUM_flag + 0.0;
			stateData2.Employment_FedState = this.Employment_FedState.Clone();
			stateData2.Employment_FedState_flag = this.Employment_FedState_flag.Clone();
			stateData2.Employment_FedState_Gov = this.Employment_FedState_Gov.Clone();
			stateData2.Employment_FedState_Gov_flag = this.Employment_FedState_Gov_flag.Clone();
			stateData2.Employment_TotalGov = this.Employment_TotalGov.Clone();
			stateData2.Employment_TotalGov_flag = this.Employment_TotalGov_flag.Clone();
			stateData2.Employment_FedState_fte = this.Employment_FedState_fte.Clone();
			stateData2.Employment_FedState_jobs = this.Employment_FedState_jobs.Clone();
			stateData2.Employment_TotalGov_fte = this.Employment_TotalGov_fte.Clone();
			stateData2.Employment_TotalGov_jobs = this.Employment_TotalGov_jobs.Clone();
			stateData2.T_ImpGov = this.T_ImpGov.Clone();
			stateData2.T_ImpGov_flag = this.T_ImpGov_flag.Clone();
			stateData2.T_base = this.T_base.Clone();
			stateData2.T_base_flag = this.T_base_flag.Clone();
			stateData2.T_imputed = this.T_imputed.Clone();
			stateData2.T_imputed_flag = this.T_imputed_flag.Clone();
			stateData2.T_TotGovSUM = this.T_TotGovSUM + 0.0;
			stateData2.T_TotGovSUM_flag = this.T_TotGovSUM_flag + 0.0;
			stateData2.T_FedState = this.T_FedState.Clone();
			stateData2.T_FedState_flag = this.T_FedState_flag.Clone();
			stateData2.T_FedState_Gov = this.T_FedState_Gov.Clone();
			stateData2.T_FedState_Gov_flag = this.T_FedState_Gov_flag.Clone();
			stateData2.T_TotalGov = this.T_TotalGov.Clone();
			stateData2.T_TotalGov_flag = this.T_TotalGov_flag.Clone();
			stateData2.ValueAdded_ImpGov = this.ValueAdded_ImpGov.Clone();
			stateData2.ValueAdded_base = this.ValueAdded_base.Clone();
			stateData2.ValueAdded_imputed = this.ValueAdded_imputed.Clone();
			stateData2.ValueAdded_TotGovSUM = this.ValueAdded_TotGovSUM + 0.0;
			stateData2.ValueAdded_FedState = this.ValueAdded_FedState.Clone();
			stateData2.ValueAdded_FedState_Gov = this.ValueAdded_FedState_Gov.Clone();
			stateData2.ValueAdded_TotalGov = this.ValueAdded_TotalGov.Clone();
			stateData2.PCE_Table = this.PCE_Table.Clone();
			stateData2 = null;
			stateData.StateSetup(ref this.fte);
			return stateData;
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Clone
			return this.Clone();
		}

		private Vector Append2Vectors(ref Vector v1, ref Vector v2)
		{
			checked
			{
				Vector vector = new Vector(v1.Count + v2.Count);
				int num = v1.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					vector[i] = v1[i];
				}
				int num2 = v2.Count - 1;
				for (int j = 0; j <= num2; j++)
				{
					vector[j + v1.Count] = v2[j];
				}
				return vector;
			}
		}

		private Vector Append2Vectors(ref Vector v1, ref double v2)
		{
			checked
			{
				Vector vector = new Vector(v1.Count + 1);
				int num = v1.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					vector[i] = v1[i];
				}
				vector[v1.Count] = v2;
				return vector;
			}
		}

		public void Aggregate(int[] AggInd, int[] AggComm, int N_IND_BASE, FTE_RATIO_CLASS fte_bis)
		{
			this.fte = (FTE_RATIO_CLASS)fte_bis.Clone();
			Vector vector = new Vector(N_IND_BASE);
			Vector vector2 = new Vector(N_IND_BASE);
			Vector vector3 = new Vector(N_IND_BASE);
			Vector vector4 = new Vector(N_IND_BASE);
			Vector vector5 = new Vector(N_IND_BASE);
			Vector vector6 = new Vector(N_IND_BASE);
			Vector vector7 = new Vector(N_IND_BASE);
			Vector vector8 = new Vector(N_IND_BASE);
			Vector vector9 = new Vector(N_IND_BASE);
			Vector vector10 = new Vector(N_IND_BASE);
			checked
			{
				Vector vector11 = new Vector(N_IND_BASE + 5);
				int num = this.EC_base.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					unchecked
					{
						Vector vector12;
						int row;
						(vector12 = vector)[row = AggInd[i]] = vector12[row] + this.EC_base[i];
						(vector12 = vector3)[row = AggInd[i]] = vector12[row] + this.GOS_base[i];
						(vector12 = vector2)[row = AggInd[i]] = vector12[row] + this.T_base[i];
						(vector12 = vector4)[row = AggInd[i]] = vector12[row] + (this.GOS_base[i] + this.T_base[i] + this.EC_base[i]);
						(vector12 = vector5)[row = AggInd[i]] = vector12[row] + this.Employment_base[i];
						(vector12 = vector6)[row = AggInd[i]] = vector12[row] + this.EC_base_flag[i];
						(vector12 = vector8)[row = AggInd[i]] = vector12[row] + this.GOS_base_flag[i];
						(vector12 = vector7)[row = AggInd[i]] = vector12[row] + this.T_base_flag[i];
						(vector12 = vector9)[row = AggInd[i]] = vector12[row] + (this.GOS_base_flag[i] + this.T_base_flag[i] + this.EC_base_flag[i]);
						(vector12 = vector10)[row = AggInd[i]] = vector12[row] + this.Employment_base_flag[i];
						(vector12 = vector11)[row = AggInd[i]] = vector12[row] + this.PCE_Table[i];
					}
				}
				this.nrec_base = N_IND_BASE;
				this.nrec_imputed = N_IND_BASE + 4;
				this.nrec_fedstate = N_IND_BASE + 2;
				this.nrec_totgov = N_IND_BASE + 1;
				this.EC_base = vector;
				this.GOS_base = vector3;
				this.T_base = vector2;
				this.GDP_base = vector4;
				this.Employment_base = vector5;
				this.EC_base_flag = vector6;
				this.GOS_base_flag = vector8;
				this.T_base_flag = vector7;
				this.GDP_base_flag = vector9;
				this.Employment_base_flag = vector10;
				int count = vector11.Count;
				int count2 = this.PCE_Table.Count;
				int num2 = 1;
				do
				{
					vector11[count - num2] = this.PCE_Table[count2 - num2];
					num2++;
				}
				while (num2 <= 5);
				this.PCE_Table = vector11;
			}
		}
	}
}
