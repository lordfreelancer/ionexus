using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class SelectStateForm : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Cancel_Button")]
		private Button _Cancel_Button;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("SelectStateLabel")]
		private Label _SelectStateLabel;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("SelectStateComboBox")]
		private ComboBox _SelectStateComboBox;

		private string PSelect;

		public string EmptyString;

		internal virtual Button Cancel_Button
		{
			[CompilerGenerated]
			get
			{
				return this._Cancel_Button;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Cancel_Button_Click;
				Button cancel_Button = this._Cancel_Button;
				if (cancel_Button != null)
				{
					cancel_Button.Click -= value2;
				}
				this._Cancel_Button = value;
				cancel_Button = this._Cancel_Button;
				if (cancel_Button != null)
				{
					cancel_Button.Click += value2;
				}
			}
		}

		internal virtual Label SelectStateLabel
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ComboBox SelectStateComboBox
		{
			[CompilerGenerated]
			get
			{
				return this._SelectStateComboBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.SelectStateComboBox_DropDown;
				EventHandler value3 = this.SelectStateComboBox_SelectedValueChanged;
				ComboBox selectStateComboBox = this._SelectStateComboBox;
				if (selectStateComboBox != null)
				{
					selectStateComboBox.DropDown -= value2;
					selectStateComboBox.SelectedValueChanged -= value3;
				}
				this._SelectStateComboBox = value;
				selectStateComboBox = this._SelectStateComboBox;
				if (selectStateComboBox != null)
				{
					selectStateComboBox.DropDown += value2;
					selectStateComboBox.SelectedValueChanged += value3;
				}
			}
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.Cancel_Button = new Button();
			this.SelectStateLabel = new Label();
			this.SelectStateComboBox = new ComboBox();
			base.SuspendLayout();
			this.Cancel_Button.Anchor = AnchorStyles.None;
			this.Cancel_Button.DialogResult = DialogResult.Cancel;
			this.Cancel_Button.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Cancel_Button.Location = new Point(331, 194);
			this.Cancel_Button.Margin = new Padding(4, 5, 4, 5);
			this.Cancel_Button.Name = "Cancel_Button";
			this.Cancel_Button.Size = new Size(100, 35);
			this.Cancel_Button.TabIndex = 1;
			this.Cancel_Button.Text = "Cancel";
			this.SelectStateLabel.AutoSize = true;
			this.SelectStateLabel.Font = new Font("Calibri", 14f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.SelectStateLabel.Location = new Point(154, 55);
			this.SelectStateLabel.Name = "SelectStateLabel";
			this.SelectStateLabel.Size = new Size(152, 35);
			this.SelectStateLabel.TabIndex = 1;
			this.SelectStateLabel.Text = "Select State";
			this.SelectStateComboBox.Font = new Font("Calibri", 12f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.SelectStateComboBox.FormattingEnabled = true;
			this.SelectStateComboBox.Location = new Point(106, 101);
			this.SelectStateComboBox.Name = "SelectStateComboBox";
			this.SelectStateComboBox.Size = new Size(265, 37);
			this.SelectStateComboBox.TabIndex = 2;
			base.AutoScaleDimensions = new SizeF(9f, 20f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.CancelButton = this.Cancel_Button;
			base.ClientSize = new Size(469, 243);
			base.Controls.Add(this.Cancel_Button);
			base.Controls.Add(this.SelectStateComboBox);
			base.Controls.Add(this.SelectStateLabel);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Margin = new Padding(4, 5, 4, 5);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "SelectStateForm";
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			this.Text = "Select State";
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private void OK_Button_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void Cancel_Button_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		public SelectStateForm(ref StateCodeNames stats)
		{
			this.PSelect = "Please Select ...";
			this.EmptyString = "Empty";
			this.InitializeComponent();
			StateCodeNames stateCodeNames = stats;
			this.SelectStateComboBox.Items.Clear();
			checked
			{
				int num = stateCodeNames.StatesFullNames.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					this.SelectStateComboBox.Items.Add(RuntimeHelpers.GetObjectValue(stateCodeNames.StatesFullNames[i]));
				}
				this.SelectStateComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
				this.SelectStateComboBox.Items.Insert(0, this.PSelect);
				this.SelectStateComboBox.SelectedItem = this.PSelect;
			}
		}

		private void SelectStateComboBox_DropDown(object sender, EventArgs e)
		{
			this.SelectStateComboBox.Items.Remove(this.PSelect);
		}

		public object GetState()
		{
			if (base.ShowDialog() == DialogResult.Cancel)
			{
				return this.EmptyString;
			}
			return this.SelectStateComboBox.SelectedItem;
		}

		private void SelectStateComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.OK;
		}
	}
}
