using IOSNAP.My;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class frmNotify : Form
	{
		private IContainer components;

		private const int InitTime = 1000;

		private const int FadeTime = 500;

		private const int FadeStages = 10;

		private int iStage;

        public Timer Timer1;
        public Label Label1;
        public ToolStripContainer ToolStripContainer1;
        public MenuStrip mnu;
		public frmNotify()
		{
			base.Click += this.frmNotify_Click;
			base.Shown += this.frmNotify_Shown;
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.Label1 = new System.Windows.Forms.Label();
            this.ToolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.mnu = new System.Windows.Forms.MenuStrip();
            this.ToolStripContainer1.ContentPanel.SuspendLayout();
            this.ToolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Timer1
            // 
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick_1);
            // 
            // Label1
            // 
            this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(12, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(372, 178);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Label1";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ToolStripContainer1
            // 
            this.ToolStripContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // ToolStripContainer1.ContentPanel
            // 
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.mnu);
            this.ToolStripContainer1.ContentPanel.Size = new System.Drawing.Size(396, 0);
            this.ToolStripContainer1.Location = new System.Drawing.Point(0, 190);
            this.ToolStripContainer1.Name = "ToolStripContainer1";
            this.ToolStripContainer1.Size = new System.Drawing.Size(396, 9);
            this.ToolStripContainer1.TabIndex = 2;
            this.ToolStripContainer1.Text = "ToolStripContainer1";
            // 
            // mnu
            // 
            this.mnu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mnu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.mnu.Location = new System.Drawing.Point(0, 0);
            this.mnu.Name = "mnu";
            this.mnu.Size = new System.Drawing.Size(396, 0);
            this.mnu.TabIndex = 0;
            this.mnu.Text = "MenuStrip1";
            // 
            // frmNotify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 201);
            this.Controls.Add(this.ToolStripContainer1);
            this.Controls.Add(this.Label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmNotify";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmNotify";
            this.TopMost = true;
            this.ToolStripContainer1.ContentPanel.ResumeLayout(false);
            this.ToolStripContainer1.ContentPanel.PerformLayout();
            this.ToolStripContainer1.ResumeLayout(false);
            this.ToolStripContainer1.PerformLayout();
            this.ResumeLayout(false);

		}

		private void frmNotify_Click(object sender, EventArgs e)
		{
			base.Dispose();
		}

		private void Timer1_Tick(object sender, EventArgs e)
		{
			if (this.Timer1.Interval == 1000)
			{
				this.Timer1.Stop();
				this.Timer1.Interval = 50;
				this.Timer1.Start();
			}
			else
			{
				base.Opacity -= 0.1;
                //ref int reference;
                //*(ref reference = ref this.iStage) = checked(reference + 1);

                iStage = checked(iStage + 1);

                if (this.iStage == 10)
				{
					base.Dispose();
				}
			}
		}

		public void Show(string Msg, List<MenuItem> lstMi = null)
		{
			base.Opacity = 1.0;
			this.Label1.Text = Msg;
			if (lstMi != null)
			{
				this.mnu.Items.Clear();
				this.mnu.Items.AddRange(MyProject.Forms.frmMain.mnu1.Items);
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.mnu.Items.GetEnumerator();
					while (enumerator.MoveNext())
					{
						ToolStripMenuItem toolStripMenuItem = (ToolStripMenuItem)enumerator.Current;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
			}
			base.ShowDialog();
		}

		private void frmNotify_Shown(object sender, EventArgs e)
		{
			this.Timer1.Interval = 1000;
			this.Timer1.Start();
			this.iStage = 0;
		}

		private void mnu_Click(object sender, EventArgs e)
		{
			this.Timer1.Stop();
		}

        private void Timer1_Tick_1(object sender, EventArgs e)
        {
            Timer1_Tick(sender, e);
        }
    }
}
