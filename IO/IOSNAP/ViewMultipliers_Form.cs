using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class ViewMultipliers_Form : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("UserRegisteredRadioButton")]
		private RadioButton _UserRegisteredRadioButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NationalRadioButton")]
		private RadioButton _NationalRadioButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Panel1")]
		private Panel _Panel1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox2")]
		private GroupBox _GroupBox2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("GroupBox1")]
		private GroupBox _GroupBox1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Output_RButton")]
		private RadioButton _Output_RButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Income_RButton")]
		private RadioButton _Income_RButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Employment_RButton")]
		private RadioButton _Employment_RButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("RegisteredDataCBox")]
		private ComboBox _RegisteredDataCBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("NatYearCBox")]
		private ComboBox _NatYearCBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("DisplayButton")]
		private Button _DisplayButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CxC_CheckBox")]
		private CheckBox _CxC_CheckBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("IxC_CheckBox")]
		private CheckBox _IxC_CheckBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("IxI_CheckBox")]
		private CheckBox _IxI_CheckBox;

		private DataWrapREADER mainData;

		private DataWrapREADER yearlyData;

		private DataWrapREADER userData;

		private string PSelect;

		public BEAData bdat;

		public bool isCxC;

		public bool isIxC;

		public bool isIxI;

		public bool isOutput;

		public bool isIncome;

		public bool isEmployment;

		public bool isnat;

		public int idxnat;

		internal virtual RadioButton UserRegisteredRadioButton
		{
			[CompilerGenerated]
			get
			{
				return this._UserRegisteredRadioButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.UserRegisteredRadioButton_CheckedChanged;
				RadioButton userRegisteredRadioButton = this._UserRegisteredRadioButton;
				if (userRegisteredRadioButton != null)
				{
					userRegisteredRadioButton.CheckedChanged -= value2;
				}
				this._UserRegisteredRadioButton = value;
				userRegisteredRadioButton = this._UserRegisteredRadioButton;
				if (userRegisteredRadioButton != null)
				{
					userRegisteredRadioButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton NationalRadioButton
		{
			[CompilerGenerated]
			get
			{
				return this._NationalRadioButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.NationalRadioButton_CheckedChanged;
				RadioButton nationalRadioButton = this._NationalRadioButton;
				if (nationalRadioButton != null)
				{
					nationalRadioButton.CheckedChanged -= value2;
				}
				this._NationalRadioButton = value;
				nationalRadioButton = this._NationalRadioButton;
				if (nationalRadioButton != null)
				{
					nationalRadioButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual Panel Panel1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual GroupBox GroupBox2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual GroupBox GroupBox1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual RadioButton Output_RButton
		{
			[CompilerGenerated]
			get
			{
				return this._Output_RButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = delegate
				{
					this.OutputChecked();
				};
				RadioButton output_RButton = this._Output_RButton;
				if (output_RButton != null)
				{
					output_RButton.CheckedChanged -= value2;
				}
				this._Output_RButton = value;
				output_RButton = this._Output_RButton;
				if (output_RButton != null)
				{
					output_RButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton Income_RButton
		{
			[CompilerGenerated]
			get
			{
				return this._Income_RButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = delegate
				{
					this.IncomeChecked();
				};
				RadioButton income_RButton = this._Income_RButton;
				if (income_RButton != null)
				{
					income_RButton.CheckedChanged -= value2;
				}
				this._Income_RButton = value;
				income_RButton = this._Income_RButton;
				if (income_RButton != null)
				{
					income_RButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual RadioButton Employment_RButton
		{
			[CompilerGenerated]
			get
			{
				return this._Employment_RButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = delegate
				{
					this.EmploymentChecked();
				};
				RadioButton employment_RButton = this._Employment_RButton;
				if (employment_RButton != null)
				{
					employment_RButton.CheckedChanged -= value2;
				}
				this._Employment_RButton = value;
				employment_RButton = this._Employment_RButton;
				if (employment_RButton != null)
				{
					employment_RButton.CheckedChanged += value2;
				}
			}
		}

		internal virtual ComboBox RegisteredDataCBox
		{
			[CompilerGenerated]
			get
			{
				return this._RegisteredDataCBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.RegisteredDataCbox_SelectedIndexChanged;
				ComboBox registeredDataCBox = this._RegisteredDataCBox;
				if (registeredDataCBox != null)
				{
					registeredDataCBox.SelectedIndexChanged -= value2;
				}
				this._RegisteredDataCBox = value;
				registeredDataCBox = this._RegisteredDataCBox;
				if (registeredDataCBox != null)
				{
					registeredDataCBox.SelectedIndexChanged += value2;
				}
			}
		}

		internal virtual ComboBox NatYearCBox
		{
			[CompilerGenerated]
			get
			{
				return this._NatYearCBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.natYearCbox_SelectedIndexChanged;
				ComboBox natYearCBox = this._NatYearCBox;
				if (natYearCBox != null)
				{
					natYearCBox.SelectedIndexChanged -= value2;
				}
				this._NatYearCBox = value;
				natYearCBox = this._NatYearCBox;
				if (natYearCBox != null)
				{
					natYearCBox.SelectedIndexChanged += value2;
				}
			}
		}

		internal virtual Label Label1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button Button2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button DisplayButton
		{
			[CompilerGenerated]
			get
			{
				return this._DisplayButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.DisplayButton_Click;
				Button displayButton = this._DisplayButton;
				if (displayButton != null)
				{
					displayButton.Click -= value2;
				}
				this._DisplayButton = value;
				displayButton = this._DisplayButton;
				if (displayButton != null)
				{
					displayButton.Click += value2;
				}
			}
		}

		internal virtual CheckBox CxC_CheckBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox IxC_CheckBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual CheckBox IxI_CheckBox
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.UserRegisteredRadioButton = new RadioButton();
			this.NationalRadioButton = new RadioButton();
			this.Panel1 = new Panel();
			this.GroupBox2 = new GroupBox();
			this.CxC_CheckBox = new CheckBox();
			this.IxC_CheckBox = new CheckBox();
			this.IxI_CheckBox = new CheckBox();
			this.GroupBox1 = new GroupBox();
			this.Output_RButton = new RadioButton();
			this.Income_RButton = new RadioButton();
			this.Employment_RButton = new RadioButton();
			this.RegisteredDataCBox = new ComboBox();
			this.NatYearCBox = new ComboBox();
			this.Label1 = new Label();
			this.Button2 = new Button();
			this.DisplayButton = new Button();
			this.Panel1.SuspendLayout();
			this.GroupBox2.SuspendLayout();
			this.GroupBox1.SuspendLayout();
			base.SuspendLayout();
			this.UserRegisteredRadioButton.AutoSize = true;
			this.UserRegisteredRadioButton.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.UserRegisteredRadioButton.Location = new Point(58, 155);
			this.UserRegisteredRadioButton.Name = "UserRegisteredRadioButton";
			this.UserRegisteredRadioButton.Size = new Size(187, 33);
			this.UserRegisteredRadioButton.TabIndex = 32;
			this.UserRegisteredRadioButton.TabStop = true;
			this.UserRegisteredRadioButton.Text = "User Registered";
			this.UserRegisteredRadioButton.UseVisualStyleBackColor = true;
			this.NationalRadioButton.AutoSize = true;
			this.NationalRadioButton.Checked = true;
			this.NationalRadioButton.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.NationalRadioButton.Location = new Point(58, 97);
			this.NationalRadioButton.Name = "NationalRadioButton";
			this.NationalRadioButton.Size = new Size(120, 33);
			this.NationalRadioButton.TabIndex = 30;
			this.NationalRadioButton.TabStop = true;
			this.NationalRadioButton.Text = "National";
			this.NationalRadioButton.UseVisualStyleBackColor = true;
			this.Panel1.BorderStyle = BorderStyle.FixedSingle;
			this.Panel1.Controls.Add(this.GroupBox2);
			this.Panel1.Controls.Add(this.GroupBox1);
			this.Panel1.Location = new Point(58, 223);
			this.Panel1.Name = "Panel1";
			this.Panel1.Size = new Size(575, 195);
			this.Panel1.TabIndex = 29;
			this.GroupBox2.Controls.Add(this.CxC_CheckBox);
			this.GroupBox2.Controls.Add(this.IxC_CheckBox);
			this.GroupBox2.Controls.Add(this.IxI_CheckBox);
			this.GroupBox2.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.GroupBox2.Location = new Point(321, 14);
			this.GroupBox2.Name = "GroupBox2";
			this.GroupBox2.Size = new Size(219, 144);
			this.GroupBox2.TabIndex = 19;
			this.GroupBox2.TabStop = false;
			this.GroupBox2.Text = "Select Format";
			this.CxC_CheckBox.AutoSize = true;
			this.CxC_CheckBox.Checked = true;
			this.CxC_CheckBox.CheckState = CheckState.Checked;
			this.CxC_CheckBox.Location = new Point(80, 101);
			this.CxC_CheckBox.Name = "CxC_CheckBox";
			this.CxC_CheckBox.Size = new Size(72, 33);
			this.CxC_CheckBox.TabIndex = 20;
			this.CxC_CheckBox.Text = "CxC";
			this.CxC_CheckBox.UseVisualStyleBackColor = true;
			this.IxC_CheckBox.AutoSize = true;
			this.IxC_CheckBox.Checked = true;
			this.IxC_CheckBox.CheckState = CheckState.Checked;
			this.IxC_CheckBox.Location = new Point(80, 66);
			this.IxC_CheckBox.Name = "IxC_CheckBox";
			this.IxC_CheckBox.Size = new Size(65, 33);
			this.IxC_CheckBox.TabIndex = 19;
			this.IxC_CheckBox.Text = "IxC";
			this.IxC_CheckBox.UseVisualStyleBackColor = true;
			this.IxI_CheckBox.AutoSize = true;
			this.IxI_CheckBox.Checked = true;
			this.IxI_CheckBox.CheckState = CheckState.Checked;
			this.IxI_CheckBox.Location = new Point(80, 34);
			this.IxI_CheckBox.Name = "IxI_CheckBox";
			this.IxI_CheckBox.Size = new Size(58, 33);
			this.IxI_CheckBox.TabIndex = 18;
			this.IxI_CheckBox.Text = "IxI";
			this.IxI_CheckBox.UseVisualStyleBackColor = true;
			this.GroupBox1.Controls.Add(this.Output_RButton);
			this.GroupBox1.Controls.Add(this.Income_RButton);
			this.GroupBox1.Controls.Add(this.Employment_RButton);
			this.GroupBox1.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.GroupBox1.Location = new Point(33, 14);
			this.GroupBox1.Name = "GroupBox1";
			this.GroupBox1.Size = new Size(271, 144);
			this.GroupBox1.TabIndex = 18;
			this.GroupBox1.TabStop = false;
			this.GroupBox1.Text = "Select Table";
			this.Output_RButton.AutoSize = true;
			this.Output_RButton.Checked = true;
			this.Output_RButton.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Output_RButton.Location = new Point(78, 28);
			this.Output_RButton.Name = "Output_RButton";
			this.Output_RButton.Size = new Size(105, 33);
			this.Output_RButton.TabIndex = 6;
			this.Output_RButton.TabStop = true;
			this.Output_RButton.Text = "Output";
			this.Output_RButton.UseVisualStyleBackColor = true;
			this.Income_RButton.AutoSize = true;
			this.Income_RButton.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Income_RButton.Location = new Point(78, 61);
			this.Income_RButton.Name = "Income_RButton";
			this.Income_RButton.Size = new Size(107, 33);
			this.Income_RButton.TabIndex = 7;
			this.Income_RButton.Text = "Income";
			this.Income_RButton.UseVisualStyleBackColor = true;
			this.Employment_RButton.AutoSize = true;
			this.Employment_RButton.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Employment_RButton.Location = new Point(78, 94);
			this.Employment_RButton.Name = "Employment_RButton";
			this.Employment_RButton.Size = new Size(160, 33);
			this.Employment_RButton.TabIndex = 8;
			this.Employment_RButton.Text = "Employment";
			this.Employment_RButton.UseVisualStyleBackColor = true;
			this.RegisteredDataCBox.FormattingEnabled = true;
			this.RegisteredDataCBox.Location = new Point(251, 155);
			this.RegisteredDataCBox.Name = "RegisteredDataCBox";
			this.RegisteredDataCBox.Size = new Size(363, 30);
			this.RegisteredDataCBox.TabIndex = 28;
			this.NatYearCBox.FormattingEnabled = true;
			this.NatYearCBox.Location = new Point(251, 97);
			this.NatYearCBox.Name = "NatYearCBox";
			this.NatYearCBox.Size = new Size(363, 30);
			this.NatYearCBox.TabIndex = 26;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Calibri", 14f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Label1.Location = new Point(53, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(230, 35);
			this.Label1.TabIndex = 25;
			this.Label1.Text = "Select IO Accounts";
			this.Button2.DialogResult = DialogResult.Cancel;
			this.Button2.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button2.Location = new Point(380, 441);
			this.Button2.Name = "Button2";
			this.Button2.Size = new Size(110, 44);
			this.Button2.TabIndex = 24;
			this.Button2.Text = "Cancel";
			this.Button2.UseVisualStyleBackColor = true;
			this.DisplayButton.DialogResult = DialogResult.OK;
			this.DisplayButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.DisplayButton.Location = new Point(523, 441);
			this.DisplayButton.Name = "DisplayButton";
			this.DisplayButton.Size = new Size(110, 44);
			this.DisplayButton.TabIndex = 23;
			this.DisplayButton.Text = "Display";
			this.DisplayButton.UseVisualStyleBackColor = true;
			base.AutoScaleDimensions = new SizeF(9f, 22f);
			base.AutoScaleMode = AutoScaleMode.Font;
			this.AutoScroll = true;
			base.ClientSize = new Size(678, 517);
			base.Controls.Add(this.UserRegisteredRadioButton);
			base.Controls.Add(this.NationalRadioButton);
			base.Controls.Add(this.Panel1);
			base.Controls.Add(this.RegisteredDataCBox);
			base.Controls.Add(this.NatYearCBox);
			base.Controls.Add(this.Label1);
			base.Controls.Add(this.Button2);
			base.Controls.Add(this.DisplayButton);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "ViewMultipliers_Form";
			this.Text = "View Multipliers";
			this.Panel1.ResumeLayout(false);
			this.GroupBox2.ResumeLayout(false);
			this.GroupBox2.PerformLayout();
			this.GroupBox1.ResumeLayout(false);
			this.GroupBox1.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public ViewMultipliers_Form(ref DataWrapREADER d, ref int idx)
		{
			this.PSelect = "Please Select ...";
			this.isCxC = false;
			this.isIxC = false;
			this.isIxI = false;
			this.isOutput = true;
			this.isIncome = false;
			this.isEmployment = false;
			this.isnat = true;
			this.idxnat = 0;
			this.InitializeComponent();
			this.mainData = d;
			this.yearlyData = new DataWrapREADER();
			this.yearlyData.setEmpty();
			this.userData = new DataWrapREADER();
			this.userData.setEmpty();
			checked
			{
				int num = this.mainData.getNrec() - 1;
				for (int i = 0; i <= num; i++)
				{
					BEAData bEAData = unchecked((BEAData)this.mainData.bealist[i]);
					if (Conversions.ToBoolean(Operators.OrObject(Operators.CompareObjectEqual(bEAData.worksum.getDataType(), "Base Data", false), Operators.CompareObjectEqual(bEAData.worksum.getDataType(), "User Defined", false))))
					{
						this.yearlyData.addData(ref bEAData);
						if (i == idx)
						{
							this.isnat = true;
							this.idxnat = this.yearlyData.getNrec();
						}
					}
					else
					{
						this.userData.addData(ref bEAData);
						if (i == idx)
						{
							this.isnat = false;
							this.idxnat = this.userData.getNrec();
						}
					}
				}
				this.NatYearCBox.Items.Clear();
				int num2 = this.yearlyData.getNrec() - 1;
				for (int j = 0; j <= num2; j++)
				{
					this.NatYearCBox.Items.Add(this.yearlyData.getData(j).worksum.DataLabel);
				}
				this.NatYearCBox.DropDownStyle = ComboBoxStyle.DropDownList;
				if (this.userData.getNrec() > 0)
				{
					this.UserRegisteredRadioButton.Enabled = true;
					this.RegisteredDataCBox.Items.Clear();
					int num3 = this.userData.getNrec() - 1;
					for (int k = 0; k <= num3; k++)
					{
						this.RegisteredDataCBox.Items.Add(this.userData.getData(k).worksum.DataLabel);
					}
					this.RegisteredDataCBox.DropDownStyle = ComboBoxStyle.DropDownList;
				}
				else
				{
					this.UserRegisteredRadioButton.Enabled = false;
				}
				if (this.isnat)
				{
					this.NatYearCBox.Enabled = true;
					this.RegisteredDataCBox.Enabled = false;
					this.NationalRadioButton.Checked = true;
					this.UserRegisteredRadioButton.Checked = false;
					if (this.userData.getNrec() == 0)
					{
						this.UserRegisteredRadioButton.Enabled = false;
					}
					this.NatYearCBox.SelectedItem = this.yearlyData.getData(this.idxnat - 1).worksum.DataLabel;
					this.bdat = this.yearlyData.getData(this.idxnat - 1);
				}
				else
				{
					this.NatYearCBox.Enabled = false;
					this.RegisteredDataCBox.Enabled = true;
					this.NationalRadioButton.Checked = false;
					this.UserRegisteredRadioButton.Checked = true;
					this.RegisteredDataCBox.SelectedItem = this.userData.getData(this.idxnat - 1).worksum.DataLabel;
					this.bdat = this.userData.getData(this.idxnat - 1);
				}
			}
		}

		private void IncomeChecked()
		{
			if (this.Income_RButton.Checked)
			{
				this.CxC_CheckBox.Checked = false;
				this.CxC_CheckBox.Enabled = false;
			}
		}

		private void EmploymentChecked()
		{
			if (this.Employment_RButton.Checked)
			{
				this.CxC_CheckBox.Checked = false;
				this.CxC_CheckBox.Enabled = false;
			}
		}

		private void OutputChecked()
		{
			if (this.Output_RButton.Checked)
			{
				this.CxC_CheckBox.Enabled = true;
			}
		}

		private void natYearCbox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = Conversions.ToString(this.NatYearCBox.SelectedItem);
            //TODO Later
   //         if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 2;
			//		this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = false;
			//	}
			//	else if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//}
			//if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread)
			//{
			//	int selectedIndex = this.NatYearCBox.SelectedIndex;
			//	if (selectedIndex >= 0)
			//	{
			//		this.bdat = this.yearlyData.getData(selectedIndex);
			//	}
			//}
			//else
			//{
			//	this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = true;
			//}
		}

		private void RegisteredDataCbox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = Conversions.ToString(this.RegisteredDataCBox.SelectedItem);
            //TODO Later
   //         if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 2;
			//		this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = false;
			//	}
			//	else if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//}
			//if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread)
			//{
			//	int selectedIndex = this.RegisteredDataCBox.SelectedIndex;
			//	if (selectedIndex >= 0)
			//	{
			//		this.bdat = this.userData.getData(selectedIndex);
			//	}
			//}
			//else
			//{
			//	this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = true;
			//}
		}

		private void DisplayButton_Click(object sender, EventArgs e)
		{
			this.isIxI = Conversions.ToBoolean(Interaction.IIf(this.IxI_CheckBox.Checked, true, false));
			this.isIxC = Conversions.ToBoolean(Interaction.IIf(this.IxC_CheckBox.Checked, true, false));
			this.isCxC = Conversions.ToBoolean(Interaction.IIf(this.CxC_CheckBox.Checked, true, false));
			this.isEmployment = Conversions.ToBoolean(Interaction.IIf(this.Employment_RButton.Checked, true, false));
			this.isOutput = Conversions.ToBoolean(Interaction.IIf(this.Output_RButton.Checked, true, false));
			this.isIncome = Conversions.ToBoolean(Interaction.IIf(this.Income_RButton.Checked, true, false));
		}

		private void UserRegisteredRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			if (this.UserRegisteredRadioButton.Checked)
			{
				this.NatYearCBox.Enabled = false;
				this.RegisteredDataCBox.Enabled = true;
				this.RegisteredDataCBox.SelectedItem = this.userData.getData(0).worksum.DataLabel;
				this.bdat = this.userData.getData(0);
			}
		}

		private void NationalRadioButton_CheckedChanged(object sender, EventArgs e)
		{
            //TODO Later
            //if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init == null)
            //{
            //	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
            //}
            //Monitor.Enter(this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init);
            //try
            //{
            //	if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State == 0)
            //	{
            //		this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State = 2;
            //		this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun = true;
            //	}
            //	else if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State == 2)
            //	{
            //		throw new IncompleteInitialization();
            //	}
            //}
            //finally
            //{
            //	this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State = 1;
            //	Monitor.Exit(this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init);
            //}
            //if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun)
            //{
            //	this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun = false;
            //}
            //else
            //{
            //	this.NatYearCBox.Enabled = true;
            //	this.RegisteredDataCBox.Enabled = false;
            //	this.NatYearCBox.SelectedItem = this.yearlyData.getData(0).worksum.DataLabel;
            //	this.bdat = this.yearlyData.getData(0);
            //}
        }

        public string getLabel()
		{
			return this.bdat.worksum.DataLabel;
		}

		[CompilerGenerated]
		[DebuggerHidden]
		private void _Lambda_0024__R25_002D1(object a0, EventArgs a1)
		{
			this.OutputChecked();
		}

		[CompilerGenerated]
		[DebuggerHidden]
		private void _Lambda_0024__R29_002D2(object a0, EventArgs a1)
		{
			this.IncomeChecked();
		}

		[CompilerGenerated]
		[DebuggerHidden]
		private void _Lambda_0024__R33_002D3(object a0, EventArgs a1)
		{
			this.EmploymentChecked();
		}
	}
}
