using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class frmReg : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("pgComm")]
		private TabPage _pgComm;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("dgvComm")]
		private DataGridView _dgvComm;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("TabControl1")]
		private TabControl _TabControl1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("pgInd")]
		private TabPage _pgInd;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("dgvInd")]
		private DataGridView _dgvInd;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("cmdSave")]
		private Button _cmdSave;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("ToolStrip1")]
		private ToolStrip _ToolStrip1;

		public BEAData beaDat;

		public Vector TVA;

		public Vector GOSr;

		public Vector Tr;

		public Vector ECr;

		public SummaryDat sumdat;

		internal virtual TabPage pgComm
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual DataGridView dgvComm
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual TabControl TabControl1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual TabPage pgInd
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual DataGridView dgvInd
		{
			[CompilerGenerated]
			get
			{
				return this._dgvInd;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				DataGridViewCellEventHandler value2 = this.dgvInd_CellContentClick;
				DataGridView dgvInd = this._dgvInd;
				if (dgvInd != null)
				{
					dgvInd.CellContentClick -= value2;
				}
				this._dgvInd = value;
				dgvInd = this._dgvInd;
				if (dgvInd != null)
				{
					dgvInd.CellContentClick += value2;
				}
			}
		}

		internal virtual Button cmdSave
		{
			[CompilerGenerated]
			get
			{
				return this._cmdSave;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.cmdSave_Click;
				Button cmdSave = this._cmdSave;
				if (cmdSave != null)
				{
					cmdSave.Click -= value2;
				}
				this._cmdSave = value;
				cmdSave = this._cmdSave;
				if (cmdSave != null)
				{
					cmdSave.Click += value2;
				}
			}
		}

		internal virtual ToolStrip ToolStrip1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		public frmReg()
		{
			base.Load += this.frmReg_Load;
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			this.pgComm = new TabPage();
			this.dgvComm = new DataGridView();
			this.TabControl1 = new TabControl();
			this.pgInd = new TabPage();
			this.dgvInd = new DataGridView();
			this.cmdSave = new Button();
			this.ToolStrip1 = new ToolStrip();
			this.pgComm.SuspendLayout();
			((ISupportInitialize)this.dgvComm).BeginInit();
			this.TabControl1.SuspendLayout();
			this.pgInd.SuspendLayout();
			((ISupportInitialize)this.dgvInd).BeginInit();
			base.SuspendLayout();
			this.pgComm.Controls.Add(this.dgvComm);
			this.pgComm.Location = new Point(4, 29);
			this.pgComm.Margin = new Padding(4, 6, 4, 6);
			this.pgComm.Name = "pgComm";
			this.pgComm.Padding = new Padding(4, 6, 4, 6);
			this.pgComm.Size = new Size(992, 455);
			this.pgComm.TabIndex = 1;
			this.pgComm.Text = "Commodity Data";
			this.pgComm.UseVisualStyleBackColor = true;
			this.dgvComm.AllowUserToAddRows = false;
			this.dgvComm.AllowUserToDeleteRows = false;
			this.dgvComm.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvComm.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvComm.Location = new Point(0, 0);
			this.dgvComm.Margin = new Padding(0);
			this.dgvComm.Name = "dgvComm";
			this.dgvComm.RowHeadersWidth = 125;
			this.dgvComm.RowTemplate.Height = 28;
			this.dgvComm.SelectionMode = DataGridViewSelectionMode.CellSelect;
			this.dgvComm.Size = new Size(996, 658);
			this.dgvComm.TabIndex = 5;
			this.TabControl1.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.TabControl1.Controls.Add(this.pgInd);
			this.TabControl1.Controls.Add(this.pgComm);
			this.TabControl1.Location = new Point(3, 28);
			this.TabControl1.Margin = new Padding(0);
			this.TabControl1.Name = "TabControl1";
			this.TabControl1.SelectedIndex = 0;
			this.TabControl1.Size = new Size(1000, 488);
			this.TabControl1.SizeMode = TabSizeMode.FillToRight;
			this.TabControl1.TabIndex = 1;
			this.pgInd.Controls.Add(this.dgvInd);
			this.pgInd.Location = new Point(4, 31);
			this.pgInd.Margin = new Padding(4, 6, 4, 6);
			this.pgInd.Name = "pgInd";
			this.pgInd.Padding = new Padding(4, 6, 4, 6);
			this.pgInd.Size = new Size(992, 453);
			this.pgInd.TabIndex = 0;
			this.pgInd.Text = "Industry Data";
			this.pgInd.UseVisualStyleBackColor = true;
			this.dgvInd.AllowDrop = true;
			this.dgvInd.AllowUserToAddRows = false;
			this.dgvInd.AllowUserToDeleteRows = false;
			this.dgvInd.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle.BackColor = SystemColors.Control;
			dataGridViewCellStyle.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvInd.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle;
			this.dgvInd.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle2.BackColor = SystemColors.Window;
			dataGridViewCellStyle2.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle2.Format = "#,##0.0#";
			dataGridViewCellStyle2.NullValue = "####";
			dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
			this.dgvInd.DefaultCellStyle = dataGridViewCellStyle2;
			this.dgvInd.Location = new Point(0, 0);
			this.dgvInd.Margin = new Padding(0);
			this.dgvInd.Name = "dgvInd";
			this.dgvInd.RowHeadersWidth = 175;
			this.dgvInd.RowTemplate.Height = 28;
			this.dgvInd.SelectionMode = DataGridViewSelectionMode.CellSelect;
			this.dgvInd.Size = new Size(988, 445);
			this.dgvInd.TabIndex = 4;
			this.cmdSave.Location = new Point(233, -2);
			this.cmdSave.Margin = new Padding(4, 6, 4, 6);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new Size(112, 30);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Regionalize";
			this.cmdSave.UseVisualStyleBackColor = true;
			this.ToolStrip1.Location = new Point(0, 0);
			this.ToolStrip1.Name = "ToolStrip1";
			this.ToolStrip1.Size = new Size(1004, 25);
			this.ToolStrip1.TabIndex = 3;
			this.ToolStrip1.Text = "ToolStrip1";
			base.AutoScaleDimensions = new SizeF(9f, 22f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(1004, 550);
			base.Controls.Add(this.cmdSave);
			base.Controls.Add(this.ToolStrip1);
			base.Controls.Add(this.TabControl1);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.Margin = new Padding(4, 6, 4, 6);
			base.Name = "frmReg";
			this.Text = "Regionalization: User Input Table";
			this.pgComm.ResumeLayout(false);
			((ISupportInitialize)this.dgvComm).EndInit();
			this.TabControl1.ResumeLayout(false);
			this.pgInd.ResumeLayout(false);
			((ISupportInitialize)this.dgvInd).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private void Save()
		{
			int upperBound = this.beaDat.VAProvided.GetUpperBound(0);
			checked
			{
				for (int i = 0; i <= upperBound; i++)
				{
					this.beaDat.VAProvided[0] = false;
				}
				Vector vector = new Vector(this.beaDat.N_INDUSTRIES + 1);
				int num = this.dgvInd.Columns.Count - 1;
				for (int j = 0; j <= num; j++)
				{
					int n_INDUSTRIES = this.beaDat.N_INDUSTRIES;
					for (int i = 0; i <= n_INDUSTRIES; i++)
					{
						vector[i] = Conversions.ToDouble(this.dgvInd.Rows[i].Cells[j].Value);
					}
					string headerText = this.dgvInd.Columns[j].HeaderText;
					if (Operators.CompareString(headerText, "Gross Operating Surplus", false) != 0)
					{
						if (Operators.CompareString(headerText, "Taxes", false) != 0)
						{
							if (Operators.CompareString(headerText, "Employee Compensation", false) != 0)
							{
								if (Operators.CompareString(headerText, "Total Value Added", false) == 0)
								{
									this.beaDat.VAProvided[0] = true;
									this.TVA = vector.Clone();
								}
							}
							else
							{
								this.beaDat.VAProvided[3] = true;
								this.ECr = vector.Clone();
							}
						}
						else
						{
							this.beaDat.VAProvided[2] = true;
							this.Tr = vector.Clone();
						}
					}
					else
					{
						this.beaDat.VAProvided[1] = true;
						this.GOSr = vector.Clone();
					}
				}
				switch (this.beaDat.RegMethod)
				{
				case BEAData.eMethod.ValueAdded:
				{
					int j = this.dgvInd.Columns["Value Added"].Index;
					int n_INDUSTRIES3 = this.beaDat.N_INDUSTRIES;
					for (int i = 0; i <= n_INDUSTRIES3; i++)
					{
						this.beaDat.Regionalizer[i] = Conversions.ToDouble(this.dgvInd.Rows[i].Cells[j].Value);
					}
					break;
				}
				case BEAData.eMethod.Income:
				{
					this.beaDat.Regionalizer = new Vector(this.beaDat.N_INDUSTRIES);
					int j = this.dgvInd.Columns["Employee Compensation"].Index;
					int n_INDUSTRIES4 = this.beaDat.N_INDUSTRIES;
					for (int i = 0; i <= n_INDUSTRIES4; i++)
					{
						this.beaDat.Regionalizer[i] = Conversions.ToDouble(Operators.MultiplyObject(Operators.DivideObject(this.dgvInd.Rows[i].Cells[j].Value, this.beaDat.get_ECDouble(i)), this.beaDat.g[i]));
					}
					break;
				}
				case BEAData.eMethod.Employment:
				{
					this.beaDat.Regionalizer = new Vector(this.beaDat.N_INDUSTRIES);
					int j = this.dgvInd.Columns["Employment"].Index;
					int n_INDUSTRIES2 = this.beaDat.N_INDUSTRIES;
					for (int i = 0; i <= n_INDUSTRIES2; i++)
					{
						this.beaDat.Regionalizer[i] = Conversions.ToDouble(Operators.MultiplyObject(Operators.DivideObject(this.dgvInd.Rows[i].Cells[j].Value, this.beaDat.EmploymentReg[i]), this.beaDat.g[i]));
					}
					break;
				}
				}
				if (this.dgvComm.ColumnCount > 1)
				{
					this.beaDat.FinDem = new Matrix(this.beaDat.N_COMMODITIES + 1, 5);
					this.beaDat.hdrFinDem = new string[5]
					{
						"Personal Consumption Expenditures",
						"Investments",
						"Exports",
						"Government",
						"Other Final Demand"
					};
				}
				int num2 = this.dgvComm.RowCount - 1;
				for (int i = 0; i <= num2; i++)
				{
					int num3 = this.dgvComm.ColumnCount - 1;
					for (int j = 0; j <= num3; j++)
					{
						string name = this.dgvComm.Columns[j].Name;
						if (Operators.CompareString(name, "Emp", false) != 0)
						{
							if (Operators.CompareString(name, "PCE", false) != 0)
							{
								if (Operators.CompareString(name, "INV", false) != 0)
								{
									if (Operators.CompareString(name, "X", false) != 0)
									{
										if (Operators.CompareString(name, "GOV", false) != 0)
										{
											if (Operators.CompareString(name, "OFD", false) == 0)
											{
												this.beaDat.set_FinDemDouble(i, 4, Conversions.ToDouble(this.dgvComm.Rows[i].Cells[j].Value));
											}
										}
										else
										{
											this.beaDat.set_FinDemDouble(i, 3, Conversions.ToDouble(this.dgvComm.Rows[i].Cells[j].Value));
										}
									}
									else
									{
										this.beaDat.set_FinDemDouble(i, 2, Conversions.ToDouble(this.dgvComm.Rows[i].Cells[j].Value));
									}
								}
								else
								{
									this.beaDat.set_FinDemDouble(i, 1, Conversions.ToDouble(this.dgvComm.Rows[i].Cells[j].Value));
								}
							}
							else
							{
								this.beaDat.set_FinDemDouble(i, 0, Conversions.ToDouble(this.dgvComm.Rows[i].Cells[j].Value));
							}
						}
						else
						{
							this.beaDat.NTables.Employment[i] = Conversions.ToDouble(this.dgvComm.Rows[i].Cells[j].Value);
						}
					}
				}
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			if (this.ValidateEntries())
			{
				this.StoreData();
				this.beaDat.Regionalize();
				base.Close();
			}
			else
			{
				Interaction.MsgBox("Regionalization Un-succesful", MsgBoxStyle.OkOnly, null);
			}
		}

		private void StoreData()
		{
			checked
			{
				Vector vector = new Vector(this.beaDat.N_INDUSTRIES + 1);
				object obj = 0;
				int n_INDUSTRIES = this.beaDat.N_INDUSTRIES;
				for (int i = 0; i <= n_INDUSTRIES; i++)
				{
					Vector vector2 = vector;
					int row = i;
					DataGridViewCellCollection cells = this.dgvInd.Rows[i].Cells;
					object[] obj2 = new object[1]
					{
						obj
					};
					object[] array = obj2;
					bool[] obj3 = new bool[1]
					{
						true
					};
					bool[] array2 = obj3;
					object instance = NewLateBinding.LateGet(cells, null, "Item", obj2, null, null, obj3);
					if (array2[0])
					{
						obj = RuntimeHelpers.GetObjectValue(array[0]);
					}
					vector2[row] = Conversions.ToDouble(NewLateBinding.LateGet(instance, null, "Value", new object[0], null, null, null));
				}
				this.beaDat.regdatinp = vector;
			}
		}

		private bool ValidateEntries()
		{
			checked
			{
				int num = this.dgvInd.Rows.Count - 1;
				int num2 = num - 1;
				int num3 = num;
				for (int i = num2; i <= num3; i++)
				{
					object value = this.dgvInd.Rows[num].HeaderCell.Value;
					if (Operators.ConditionalCompareObjectEqual(value, "Scrap", false) || Operators.ConditionalCompareObjectEqual(value, "NCI", false))
					{
						int num4 = this.dgvInd.Columns.Count - 1;
						for (int j = 0; j <= num4; j++)
						{
							if (Operators.ConditionalCompareObjectEqual(this.dgvInd.Rows[num].Cells[j].Value, "", false))
							{
								Interaction.MsgBox("All values for Scrap and NCI must be provided", MsgBoxStyle.OkOnly, null);
								return false;
							}
						}
					}
				}
				return true;
			}
		}

		public void dgvComm_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			int rowIndex = e.RowIndex;
			double num = Conversions.ToDouble(this.dgvComm.Rows[rowIndex].Cells["RTC"].Value);
			this.dgvComm.CellValueChanged -= this.dgvComm_CellValueChanged;
			num = Conversions.ToDouble(Operators.SubtractObject(num, this.dgvComm.Rows[rowIndex].Cells["INV"].Value));
			num = Conversions.ToDouble(Operators.SubtractObject(num, this.dgvComm.Rows[rowIndex].Cells["GOV"].Value));
			num = Conversions.ToDouble(Operators.SubtractObject(num, this.dgvComm.Rows[rowIndex].Cells["X"].Value));
			num = Conversions.ToDouble(Operators.SubtractObject(num, this.dgvComm.Rows[rowIndex].Cells["PCE"].Value));
			this.dgvComm.Rows[rowIndex].Cells["OFD"].Value = Math.Round(num, 7);
			this.dgvComm.CellValueChanged += this.dgvComm_CellValueChanged;
		}

		private void ToolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
		}

		private void frmReg_Load(object sender, EventArgs e)
		{
		}

		private void dgvInd_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
		}
	}
}
