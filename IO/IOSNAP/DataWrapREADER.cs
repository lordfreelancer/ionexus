using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IOSNAP
{
	public class DataWrapREADER
	{
		public ArrayList names;

		private ArrayList lab_names;

		public ArrayList bealist;

		private ArrayList tablist;

		public DataWrapREADER()
		{
			this.names = new ArrayList();
			this.lab_names = new ArrayList();
			this.bealist = new ArrayList();
			this.tablist = new ArrayList();
			this.names.Add("1998");
		}

		public void setEmpty()
		{
			this.names = new ArrayList();
			this.bealist = new ArrayList();
			this.tablist = new ArrayList();
		}

		public void loadData2(ref string LicenseT)
		{
			string text = "data0908_demo.dat";
			text = ((Operators.CompareString(LicenseT, "Full", false) != 0) ? "data0908_demo.dat" : "data0908_propv.dat");
			StreamReader streamReader = new StreamReader(FileSystem.CurDir() + "\\" + text);
			string text2 = "";
			List<string> list = new List<string>();
			string dataVersion = "";
			string dataType = "";
			string text3 = "";
			int num = -1;
			int num2 = -1;
			int num3 = -1;
			checked
			{
				try
				{
					object value = streamReader.ReadToEnd();
					streamReader.Close();
					byte[] bytes = Convert.FromBase64String(Conversions.ToString(value));
					text2 = Encoding.ASCII.GetString(bytes);
					list = this.CleanDataString(text2);
					for (int i = 0; i < list.Count; i++)
					{
						text3 = list[i];
						if (Operators.CompareString(text3, "<version>", false) != 0)
						{
							if (Operators.CompareString(text3, "<parameters>", false) != 0)
							{
								string[] hdrVA = default(string[]);
								string[] hdrFinDem = default(string[]);
								string[] hdrIndBase = default(string[]);
								string[] hdrIndAdjGov = default(string[]);
								string[] hdrIndFedStateLocGov = default(string[]);
								string[] hdrIndTotGov = default(string[]);
								string[] hdrIndExpandedGov = default(string[]);
								string[] indBaseClass = default(string[]);
								string[] indAdjGovClass = default(string[]);
								string[] indFedStateLocGovClass = default(string[]);
								string[] indTotGovClass = default(string[]);
								string[] indExpandedGovClass = default(string[]);
								string[] hdrCommBase = default(string[]);
								string[] hdrCommAdjGov = default(string[]);
								string[] hdrCommFedStateLocGov = default(string[]);
								string[] hdrCommTotGov = default(string[]);
								string[] hdrNoncompImp = default(string[]);
								string[] hdrScrap = default(string[]);
								string[] hdrCommExpandedGov = default(string[]);
								if (Operators.CompareString(text3, "<headers>", false) != 0)
								{
									if (Operators.CompareString(text3, "<data>", false) == 0)
									{
										while (!text3.Contains("</data>"))
										{
											i++;
											text3 = list[i];
											if (text3.Contains("<year>"))
											{
												string text4 = "";
												Matrix matrix = new Matrix(3, num);
												Matrix matrix2 = new Matrix(num, num2);
												Matrix matrix3 = new Matrix(num2, num);
												Matrix matrix4 = new Matrix(num2, 10);
												Matrix matrix5 = new Matrix(1, num2);
												Vector vector = new Vector(num);
												Vector vector2 = new Vector(num);
												Vector vector3 = new Vector(num2);
												Vector vector4 = new Vector(1);
												Vector vector5 = new Vector(1);
												Vector vector6 = new Vector(1);
												Vector vector7 = new Vector(1);
												i++;
												text3 = list[i];
												if (text3.Contains("<value"))
												{
													text4 = this.ExtractString(text3);
												}
												BEAData bEAData = new BEAData();
												this.lab_names.Add("Base " + text4);
												SummaryDat summaryDat = new SummaryDat();
												summaryDat.InitSetup();
												bEAData.worksum = summaryDat;
												bEAData.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
												bEAData.worksum.notFirstRead = true;
												while (!text3.Contains("</year>"))
												{
													i++;
													text3 = list[i];
													if (text3.Contains("<national>"))
													{
														while (!text3.Contains("</national>"))
														{
															i++;
															text3 = list[i];
															switch (BEAData.ComputeStringHash(text3))
															{
															case 1293265258u:
																if (Operators.CompareString(text3, "<tableVA>", false) == 0)
																{
																	int num5 = 0;
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableVA>"))
																	{
																		double[] array8 = this.StrArr_2_DblArr(text3.Split('\t'));
																		int num6 = array8.Length - 1;
																		for (int n = 0; n <= num6; n++)
																		{
																			matrix[num5, n] = array8[n];
																		}
																		num5++;
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 3136475398u:
																if (Operators.CompareString(text3, "<tableUse>", false) == 0)
																{
																	int num4 = 0;
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableUse>"))
																	{
																		double[] array3 = this.StrArr_2_DblArr(text3.Split('\t'));
																		int upperBound2 = array3.GetUpperBound(0);
																		for (int k = 0; k <= upperBound2; k++)
																		{
																			matrix3[num4, k] = array3[k];
																		}
																		num4++;
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 4003730491u:
																if (Operators.CompareString(text3, "<tableMake>", false) == 0)
																{
																	int num7 = 0;
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableMake>"))
																	{
																		double[] array9 = this.StrArr_2_DblArr(text3.Split('\t'));
																		int num8 = array9.Length - 1;
																		for (int num9 = 0; num9 <= num8; num9++)
																		{
																			matrix2[num7, num9] = array9[num9];
																		}
																		num7++;
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 2449239234u:
																if (Operators.CompareString(text3, "<tableFinDem>", false) == 0)
																{
																	int num11 = 0;
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableFinDem>"))
																	{
																		double[] array12 = this.StrArr_2_DblArr(text3.Split('\t'));
																		int num12 = array12.Length - 1;
																		for (int num13 = 0; num13 <= num12; num13++)
																		{
																			matrix4[num11, num13] = array12[num13];
																		}
																		num11++;
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 147079669u:
																if (Operators.CompareString(text3, "<tableImp>", false) == 0)
																{
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableImp>"))
																	{
																		double[] array5 = this.StrArr_2_DblArr(text3.Split('\t'));
																		int upperBound3 = array5.GetUpperBound(0);
																		for (int l = 0; l <= upperBound3; l++)
																		{
																			matrix5[0, l] = unchecked(array5[l] * -1.0);
																		}
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 671048239u:
																if (Operators.CompareString(text3, "<tableEmployment>", false) == 0)
																{
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableEmployment>"))
																	{
																		double[] array11 = this.StrArr_2_DblArr(text3.Split('\t'));
																		int upperBound5 = array11.GetUpperBound(0);
																		for (int num10 = 0; num10 <= upperBound5; num10++)
																		{
																			vector[num10] = array11[num10];
																		}
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 3175804063u:
																if (Operators.CompareString(text3, "<tablePCE>", false) == 0)
																{
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tablePCE>"))
																	{
																		double[] array6 = this.StrArr_2_DblArr(text3.Split('\t'));
																		int upperBound4 = array6.GetUpperBound(0);
																		for (int m = 0; m <= upperBound4; m++)
																		{
																			vector3[m] = array6[m];
																		}
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 2894797272u:
																if (Operators.CompareString(text3, "<tableFTE_Ratio>", false) == 0)
																{
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableFTE_Ratio>"))
																	{
																		double[] array2 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																		int upperBound = array2.GetUpperBound(0);
																		for (int j = 0; j <= upperBound; j++)
																		{
																			vector2[j] = array2[j];
																		}
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 1064776432u:
																if (Operators.CompareString(text3, "<tablePersonalIncome>", false) == 0)
																{
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tablePersonalIncome>"))
																	{
																		double[] array10 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																		vector4[0] = array10[0];
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 3686194394u:
																if (Operators.CompareString(text3, "<tableDisposableIncome>", false) == 0)
																{
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableDisposableIncome>"))
																	{
																		double[] array7 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																		vector5[0] = array7[0];
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 2199806808u:
																if (Operators.CompareString(text3, "<tableTotFedExp_2005>", false) == 0)
																{
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableTotFedExp_2005>"))
																	{
																		double[] array4 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																		vector6[0] = array4[0];
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															case 1093051045u:
																if (Operators.CompareString(text3, "<tableTotSLExp_2008>", false) == 0)
																{
																	i++;
																	text3 = list[i];
																	while (!text3.Contains("</tableTotSLExp_2008>"))
																	{
																		double[] array = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																		vector7[0] = array[0];
																		i++;
																		text3 = list[i];
																	}
																}
																break;
															}
														}
														bEAData.NTables.Employment_JOBS = vector;
														bEAData.NTables.FTE_Ratio.setupFTE(ref vector2, ref vector);
													}
													if (text3.Contains("<statedata>"))
													{
														while (!text3.Contains("</statedata>"))
														{
															i++;
															text3 = list[i];
															if (text3.Contains("<state>"))
															{
																string text5 = "";
																Vector vector8 = new Vector(1);
																Vector vector9 = new Vector(1);
																Vector vector10 = new Vector(1);
																Vector vector11 = new Vector(1);
																Vector vector12 = new Vector(num3);
																Vector vector13 = new Vector(num3);
																Vector vector14 = new Vector(2);
																Vector vector15 = new Vector(2);
																Vector vector16 = new Vector(4);
																Vector vector17 = new Vector(4);
																Vector vector18 = new Vector(1);
																Vector vector19 = new Vector(1);
																Vector vector20 = new Vector(num3);
																Vector vector21 = new Vector(num3);
																Vector vector22 = new Vector(2);
																Vector vector23 = new Vector(2);
																Vector vector24 = new Vector(4);
																Vector vector25 = new Vector(4);
																Vector vector26 = new Vector(1);
																Vector vector27 = new Vector(1);
																Vector vector28 = new Vector(num3);
																Vector vector29 = new Vector(num3);
																Vector vector30 = new Vector(2);
																Vector vector31 = new Vector(2);
																Vector vector32 = new Vector(4);
																Vector vector33 = new Vector(4);
																Vector vector34 = new Vector(1);
																Vector vector35 = new Vector(1);
																Vector vector36 = new Vector(num3);
																Vector vector37 = new Vector(num3);
																Vector vector38 = new Vector(2);
																Vector vector39 = new Vector(2);
																Vector vector40 = new Vector(4);
																Vector vector41 = new Vector(4);
																Vector vector42 = new Vector(1);
																Vector vector43 = new Vector(1);
																Vector vector44 = new Vector(num3);
																Vector vector45 = new Vector(num3);
																Vector vector46 = new Vector(2);
																Vector vector47 = new Vector(2);
																Vector vector48 = new Vector(4);
																Vector vector49 = new Vector(4);
																Vector vector50 = new Vector(1);
																Vector vector51 = new Vector(1);
																Vector vector52 = new Vector(num2);
																i++;
																text3 = list[i];
																if (text3.Contains("<value"))
																{
																	text5 = this.ExtractString(text3);
																}
																while (!text3.Contains("</state>"))
																{
																	i++;
																	text3 = list[i];
																	switch (BEAData.ComputeStringHash(text3))
																	{
																	case 2985580531u:
																		if (Operators.CompareString(text3, "<statePersonalIncome>", false) == 0)
																		{
																			int num25 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</statePersonalIncome>"))
																			{
																				double[] array24 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector8[0] = array24[0];
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 2108530769u:
																		if (Operators.CompareString(text3, "<stateDisposableIncome>", false) == 0)
																		{
																			int num15 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateDisposableIncome>"))
																			{
																				double[] array14 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector9[0] = array14[0];
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 1259441645u:
																		if (Operators.CompareString(text3, "<stateTotFedExp>", false) == 0)
																		{
																			int num28 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateTotFedExp>"))
																			{
																				double[] array27 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector11[0] = array27[0];
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 1561574220u:
																		if (Operators.CompareString(text3, "<stateTotSLEExp>", false) == 0)
																		{
																			int num34 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateTotSLEExp>"))
																			{
																				double[] array33 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector10[0] = array33[0];
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 1201911847u:
																		if (Operators.CompareString(text3, "<stateEmploymentBase>", false) == 0)
																		{
																			int num19 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateEmploymentBase>"))
																			{
																				double[] array18 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector12[num19] = array18[0];
																				vector13[num19] = array18[1];
																				num19++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 488386426u:
																		if (Operators.CompareString(text3, "<statePCE>", false) == 0)
																		{
																			int num33 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</statePCE>"))
																			{
																				double[] array32 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector52[num33] = array32[0];
																				num33++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 2779262348u:
																		if (Operators.CompareString(text3, "<stateEmploymentFedStateGov>", false) == 0)
																		{
																			int num21 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateEmploymentFedStateGov>"))
																			{
																				double[] array20 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector14[num21] = array20[0];
																				vector15[num21] = array20[1];
																				num21++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 2377842226u:
																		if (Operators.CompareString(text3, "<stateEmploymentImputedGov>", false) == 0)
																		{
																			int num37 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateEmploymentImputedGov>"))
																			{
																				double[] array36 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector16[num37] = array36[0];
																				vector17[num37] = array36[1];
																				num37++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 1628390466u:
																		if (Operators.CompareString(text3, "<stateEmploymentTotalGov>", false) == 0)
																		{
																			int num30 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateEmploymentTotalGov>"))
																			{
																				double[] array29 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector18[num30] = array29[0];
																				vector19[num30] = array29[1];
																				num30++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 3130247017u:
																		if (Operators.CompareString(text3, "<stateECBase>", false) == 0)
																		{
																			int num24 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateECBase>"))
																			{
																				double[] array23 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector36[num24] = array23[0];
																				vector37[num24] = array23[1];
																				num24++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 4052594078u:
																		if (Operators.CompareString(text3, "<stateECFedStateGov>", false) == 0)
																		{
																			int num16 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateECFedStateGov>"))
																			{
																				double[] array15 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector38[num16] = array15[0];
																				vector39[num16] = array15[1];
																				num16++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 3769605024u:
																		if (Operators.CompareString(text3, "<stateECImputedGov>", false) == 0)
																		{
																			int num36 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateECImputedGov>"))
																			{
																				double[] array35 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector40[num36] = array35[0];
																				vector41[num36] = array35[1];
																				num36++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 1200158260u:
																		if (Operators.CompareString(text3, "<stateECTotalGov>", false) == 0)
																		{
																			int num31 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateECTotalGov>"))
																			{
																				double[] array30 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector42[num31] = array30[0];
																				vector43[num31] = array30[1];
																				num31++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 1543141785u:
																		if (Operators.CompareString(text3, "<stateTBase>", false) == 0)
																		{
																			int num27 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateTBase>"))
																			{
																				double[] array26 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector44[num27] = array26[0];
																				vector45[num27] = array26[1];
																				num27++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 3052493390u:
																		if (Operators.CompareString(text3, "<stateTFedStateGov>", false) == 0)
																		{
																			int num22 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateTFedStateGov>"))
																			{
																				double[] array21 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector46[num22] = array21[0];
																				vector47[num22] = array21[1];
																				num22++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 2824740784u:
																		if (Operators.CompareString(text3, "<stateTImputedGov>", false) == 0)
																		{
																			int num18 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateTImputedGov>"))
																			{
																				double[] array17 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector48[num18] = array17[0];
																				vector49[num18] = array17[1];
																				num18++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 4092808356u:
																		if (Operators.CompareString(text3, "<stateTTotalGov>", false) == 0)
																		{
																			int num38 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateTTotalGov>"))
																			{
																				double[] array37 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector50[num38] = array37[0];
																				vector51[num38] = array37[1];
																				num38++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 352327400u:
																		if (Operators.CompareString(text3, "<stateGOSBase>", false) == 0)
																		{
																			int num35 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateGOSBase>"))
																			{
																				double[] array34 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector28[num35] = array34[0];
																				vector29[num35] = array34[1];
																				num35++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 3113025809u:
																		if (Operators.CompareString(text3, "<stateGOSFedStateGov>", false) == 0)
																		{
																			int num32 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateGOSFedStateGov>"))
																			{
																				double[] array31 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector30[num32] = array31[0];
																				vector31[num32] = array31[1];
																				num32++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 2519650377u:
																		if (Operators.CompareString(text3, "<stateGOSImputedGov>", false) == 0)
																		{
																			int num29 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateGOSImputedGov>"))
																			{
																				double[] array28 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector32[num29] = array28[0];
																				vector33[num29] = array28[1];
																				num29++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 1414549377u:
																		if (Operators.CompareString(text3, "<stateGOSTotalGov>", false) == 0)
																		{
																			int num26 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateGOSTotalGov>"))
																			{
																				double[] array25 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector34[num26] = array25[0];
																				vector35[num26] = array25[1];
																				num26++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 3704263722u:
																		if (Operators.CompareString(text3, "<stateGDPBase>", false) == 0)
																		{
																			int num23 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateGDPBase>"))
																			{
																				double[] array22 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector20[num23] = array22[0];
																				vector21[num23] = array22[1];
																				num23++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 560814643u:
																		if (Operators.CompareString(text3, "<stateGDPFedStateGov>", false) == 0)
																		{
																			int num20 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateGDPFedStateGov>"))
																			{
																				double[] array19 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector22[num20] = array19[0];
																				vector23[num20] = array19[1];
																				num20++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 2842452935u:
																		if (Operators.CompareString(text3, "<stateGDPImputedGov>", false) == 0)
																		{
																			int num17 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateGDPImputedGov>"))
																			{
																				double[] array16 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector24[num17] = array16[0];
																				vector25[num17] = array16[1];
																				num17++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	case 985267547u:
																		if (Operators.CompareString(text3, "<stateGDPTotalGov>", false) == 0)
																		{
																			int num14 = 0;
																			i++;
																			text3 = list[i];
																			while (!text3.Contains("</stateGDPTotalGov>"))
																			{
																				double[] array13 = this.StrArr_2_DblArr(text3.Split('\t', ' '));
																				vector26[num14] = array13[0];
																				vector27[num14] = array13[1];
																				num14++;
																				i++;
																				text3 = list[i];
																			}
																		}
																		break;
																	}
																}
																StateData stateData = new StateData(text5, num3);
																stateData.PersonalIncome = vector8[0];
																stateData.DisposableIncome = vector9[0];
																stateData.TotFedExp = vector11[0];
																stateData.TotSLEExp = vector10[0];
																stateData.EC_TotGovSUM = vector42[0];
																stateData.EC_TotGovSUM_flag = vector43[0];
																stateData.Employment_TotGovSUM = vector18[0];
																stateData.Employment_TotGovSUM_flag = vector19[0];
																stateData.T_TotGovSUM = vector50[0];
																stateData.T_TotGovSUM_flag = vector51[0];
																stateData.GDP_TotGovSUM = vector26[0];
																stateData.GDP_TotGovSUM_flag = vector27[0];
																stateData.GOS_TotGovSUM = vector34[0];
																stateData.GOS_TotGovSUM_flag = vector35[0];
																stateData.EC_ImpGov = vector40;
																stateData.EC_ImpGov_flag = vector41;
																stateData.Employment_ImpGov = vector16;
																stateData.Employment_ImpGov_flag = vector17;
																stateData.T_ImpGov = vector48;
																stateData.T_ImpGov_flag = vector49;
																stateData.GDP_ImpGov = vector24;
																stateData.GDP_ImpGov_flag = vector25;
																stateData.GOS_ImpGov = vector32;
																stateData.GOS_ImpGov_flag = vector33;
																stateData.EC_FedState_Gov = vector38;
																stateData.EC_FedState_Gov_flag = vector39;
																stateData.Employment_FedState_Gov = vector14;
																stateData.Employment_FedState_Gov_flag = vector15;
																stateData.T_FedState_Gov = vector46;
																stateData.T_FedState_Gov_flag = vector47;
																stateData.GDP_FedState_Gov = vector22;
																stateData.GDP_FedState_Gov_flag = vector23;
																stateData.GOS_FedState_Gov = vector30;
																stateData.GOS_FedState_Gov_flag = vector31;
																stateData.EC_base = vector36;
																stateData.EC_base_flag = vector37;
																stateData.Employment_base = vector12;
																stateData.Employment_base_flag = vector13;
																stateData.T_base = vector44;
																stateData.T_base_flag = vector45;
																stateData.GDP_base = vector20;
																stateData.GDP_base_flag = vector21;
																stateData.GOS_base = vector28;
																stateData.GOS_base_flag = vector29;
																stateData.PCE_Table = vector52;
																stateData.StateSetup(ref bEAData.NTables.FTE_Ratio);
																bEAData.RegStateData.addState(text5, ref stateData);
															}
														}
													}
												}
												bEAData.hdrVA = hdrVA;
												bEAData.hdrFinDem = hdrFinDem;
												bEAData.NTables.hdrIndBase = hdrIndBase;
												bEAData.NTables.hdrIndAdjGov = hdrIndAdjGov;
												bEAData.NTables.hdrIndFedStateLocGov = hdrIndFedStateLocGov;
												bEAData.NTables.hdrIndTotGov = hdrIndTotGov;
												bEAData.NTables.hdrIndExpandedGov = hdrIndExpandedGov;
												bEAData.IndBaseClass = indBaseClass;
												bEAData.IndAdjGovClass = indAdjGovClass;
												bEAData.IndFedStateLocGovClass = indFedStateLocGovClass;
												bEAData.IndTotGovClass = indTotGovClass;
												bEAData.IndExpandedGovClass = indExpandedGovClass;
												bEAData.hdrCommBase = hdrCommBase;
												bEAData.hdrCommAdjGov = hdrCommAdjGov;
												bEAData.hdrCommFedStateLocGov = hdrCommFedStateLocGov;
												bEAData.hdrCommTotGov = hdrCommTotGov;
												bEAData.hdrNoncompImp = hdrNoncompImp;
												bEAData.hdrScrap = hdrScrap;
												bEAData.hdrCommExpandedGov = hdrCommExpandedGov;
												bEAData.SetupHeaders();
												bEAData.N_INDUSTRIES = num;
												bEAData.N_COMMODITIES = num2;
												bEAData.N_INDUSTRIES_BASE = num3;
												bEAData.PersonalIncome = vector4[0];
												bEAData.DisposableIncome = vector5[0];
												bEAData.TotFedExp_2005 = vector6[0];
												bEAData.TotSLExp_2008 = vector7[0];
												bEAData.NTables.VA = matrix;
												bEAData.NTables.SetMake(ref matrix2);
												bEAData.Use = matrix3;
												bEAData.FinDem = matrix4;
												bEAData.Import = matrix5;
												bEAData.NTables.Employment_JOBS = vector;
												bEAData.NTables.PCE = vector3;
												bEAData.NTables.FTE_Ratio.setupFTE(ref vector2, ref vector);
												bEAData.RegReset();
												bEAData.worksum.InitSetup();
												bEAData.worksum.datayear = text4;
												bEAData.worksum.DataTypeCode = SummaryDat.bcode.BASE_DATA;
												bEAData.worksum.DataLabel = "Base " + bEAData.worksum.datayear;
												bEAData.worksum.DataVersion = dataVersion;
												bEAData.worksum.DataType = dataType;
												bEAData.NTables.Employment_FTE = bEAData.NTables.FTE_Ratio.getFTE_RATIO() * bEAData.NTables.Employment_JOBS;
												bEAData.NTables.Employment = bEAData.NTables.Employment_FTE;
												bEAData.ConsolidateData();
												bEAData.ExtractScrapFromMake();
												bEAData.ExtractNCIFromMake();
												bEAData.eDataState = BEAData.DataState.Loaded;
												this.bealist.Add(bEAData);
												TabStorage value2 = new TabStorage();
												this.tablist.Add(value2);
											}
										}
									}
								}
								else
								{
									while (!text3.Contains("</headers>"))
									{
										i++;
										text3 = list[i];
										switch (BEAData.ComputeStringHash(text3))
										{
										case 4264053804u:
											if (Operators.CompareString(text3, "<hdrFinDem>", false) == 0)
											{
												ArrayList arrayList10 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrFinDem>"))
												{
													arrayList10.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrFinDem = unchecked((string[])arrayList10.ToArray(typeof(string)));
											}
											break;
										case 119086938u:
											if (Operators.CompareString(text3, "<hdrInd>", false) == 0)
											{
												ArrayList arrayList11 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrInd>"))
												{
													arrayList11.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrIndBase = unchecked((string[])arrayList11.ToArray(typeof(string)));
											}
											break;
										case 1650851454u:
											if (Operators.CompareString(text3, "<IndClassification>", false) == 0)
											{
												ArrayList arrayList2 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</IndClassification>"))
												{
													arrayList2.Add(text3);
													i++;
													text3 = list[i];
												}
												indBaseClass = unchecked((string[])arrayList2.ToArray(typeof(string)));
											}
											break;
										case 2979086691u:
											if (Operators.CompareString(text3, "<hdrIndAdjGov>", false) == 0)
											{
												ArrayList arrayList16 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrIndAdjGov>"))
												{
													arrayList16.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrIndAdjGov = unchecked((string[])arrayList16.ToArray(typeof(string)));
											}
											break;
										case 3246289985u:
											if (Operators.CompareString(text3, "<IndAdjGovClass>", false) == 0)
											{
												ArrayList arrayList18 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</IndAdjGovClass>"))
												{
													arrayList18.Add(text3);
													i++;
													text3 = list[i];
												}
												indAdjGovClass = unchecked((string[])arrayList18.ToArray(typeof(string)));
											}
											break;
										case 3624050580u:
											if (Operators.CompareString(text3, "<hdrIndFedStateLocGov>", false) == 0)
											{
												ArrayList arrayList6 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrIndFedStateLocGov>"))
												{
													arrayList6.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrIndFedStateLocGov = unchecked((string[])arrayList6.ToArray(typeof(string)));
											}
											break;
										case 10310320u:
											if (Operators.CompareString(text3, "<IndFedStateLocGovClass>", false) == 0)
											{
												ArrayList arrayList12 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</IndFedStateLocGovClass>"))
												{
													arrayList12.Add(text3);
													i++;
													text3 = list[i];
												}
												indFedStateLocGovClass = unchecked((string[])arrayList12.ToArray(typeof(string)));
											}
											break;
										case 4245667455u:
											if (Operators.CompareString(text3, "<hdrIndExpandedGov>", false) == 0)
											{
												ArrayList arrayList4 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrIndExpandedGov>"))
												{
													arrayList4.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrIndExpandedGov = unchecked((string[])arrayList4.ToArray(typeof(string)));
											}
											break;
										case 3735794213u:
											if (Operators.CompareString(text3, "<IndExpandedGovClass>", false) == 0)
											{
												ArrayList arrayList15 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</IndExpandedGovClass>"))
												{
													arrayList15.Add(text3);
													i++;
													text3 = list[i];
												}
												indExpandedGovClass = unchecked((string[])arrayList15.ToArray(typeof(string)));
											}
											break;
										case 3641026845u:
											if (Operators.CompareString(text3, "<hdrIndTotGov>", false) == 0)
											{
												ArrayList arrayList7 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrIndTotGov>"))
												{
													arrayList7.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrIndTotGov = unchecked((string[])arrayList7.ToArray(typeof(string)));
											}
											break;
										case 3183803363u:
											if (Operators.CompareString(text3, "<IndTotGovClass>", false) == 0)
											{
												ArrayList arrayList19 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</IndTotGovClass>"))
												{
													arrayList19.Add(text3);
													i++;
													text3 = list[i];
												}
												indTotGovClass = unchecked((string[])arrayList19.ToArray(typeof(string)));
											}
											break;
										case 743013071u:
											if (Operators.CompareString(text3, "<hdrComm>", false) == 0)
											{
												ArrayList arrayList14 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrComm>"))
												{
													arrayList14.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrCommBase = (string[])arrayList14.ToArray(typeof(string));
											}
											break;
										case 2024972078u:
											if (Operators.CompareString(text3, "<hdrCommAdjGov>", false) == 0)
											{
												ArrayList arrayList8 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrCommAdjGov>"))
												{
													arrayList8.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrCommAdjGov = unchecked((string[])arrayList8.ToArray(typeof(string)));
											}
											break;
										case 223664573u:
											if (Operators.CompareString(text3, "<hdrCommFedStateLocGov>", false) == 0)
											{
												ArrayList arrayList3 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrCommFedStateLocGov>"))
												{
													arrayList3.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrCommFedStateLocGov = unchecked((string[])arrayList3.ToArray(typeof(string)));
											}
											break;
										case 3475782100u:
											if (Operators.CompareString(text3, "<hdrCommExpandedGov>", false) == 0)
											{
												ArrayList arrayList17 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrCommExpandedGov>"))
												{
													arrayList17.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrCommExpandedGov = unchecked((string[])arrayList17.ToArray(typeof(string)));
											}
											break;
										case 1806760728u:
											if (Operators.CompareString(text3, "<hdrCommTotGov>", false) == 0)
											{
												ArrayList arrayList13 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrCommTotGov>"))
												{
													arrayList13.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrCommTotGov = unchecked((string[])arrayList13.ToArray(typeof(string)));
											}
											break;
										case 3984762213u:
											if (Operators.CompareString(text3, "<hdrNoncompImp>", false) == 0)
											{
												ArrayList arrayList9 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrNoncompImp>"))
												{
													arrayList9.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrNoncompImp = unchecked((string[])arrayList9.ToArray(typeof(string)));
											}
											break;
										case 204095630u:
											if (Operators.CompareString(text3, "<hdrScrap>", false) == 0)
											{
												ArrayList arrayList5 = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrScrap>"))
												{
													arrayList5.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrScrap = unchecked((string[])arrayList5.ToArray(typeof(string)));
											}
											break;
										case 2279516704u:
											if (Operators.CompareString(text3, "<hdrVA>", false) == 0)
											{
												ArrayList arrayList = new ArrayList();
												i++;
												text3 = list[i];
												while (!text3.Contains("</hdrVA>"))
												{
													arrayList.Add(text3);
													i++;
													text3 = list[i];
												}
												hdrVA = unchecked((string[])arrayList.ToArray(typeof(string)));
											}
											break;
										}
									}
								}
							}
							else
							{
								while (!text3.Contains("</parameters>"))
								{
									i++;
									text3 = list[i];
									if (text3.Contains("<n_industries>"))
									{
										i++;
										text3 = list[i];
										if (text3.Contains("<value"))
										{
											num = this.ExtractInt(text3);
										}
									}
									if (text3.Contains("<n_commodities>"))
									{
										i++;
										text3 = list[i];
										if (text3.Contains("<value"))
										{
											num2 = this.ExtractInt(text3);
										}
									}
									if (text3.Contains("<n_base>"))
									{
										i++;
										text3 = list[i];
										if (text3.Contains("<value"))
										{
											num3 = this.ExtractInt(text3);
										}
									}
									if (text3.Contains("<data_type>"))
									{
										i++;
										text3 = list[i];
										if (text3.Contains("<value"))
										{
											dataType = this.ExtractString(text3).ToUpper();
										}
									}
								}
							}
						}
						else
						{
							while (!text3.Contains("</version>"))
							{
								i++;
								text3 = list[i];
								if (text3.Contains("<value"))
								{
									dataVersion = this.ExtractString(text3);
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					Interaction.MsgBox(ex2.ToString(), MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
		}

		private string ExtractString(object s)
		{
			return Conversions.ToString(NewLateBinding.LateGet(NewLateBinding.LateGet(s, null, "Substring", new object[1]
			{
				6
			}, null, null, null), null, "Trim", new object[1]
			{
				new char[2]
				{
					' ',
					'>'
				}
			}, null, null, null));
		}

		private int ExtractInt(object s)
		{
			int result = 0;
			int.TryParse(Conversions.ToString(NewLateBinding.LateGet(NewLateBinding.LateGet(s, null, "Substring", new object[1]
			{
				6
			}, null, null, null), null, "Trim", new object[1]
			{
				new char[2]
				{
					' ',
					'>'
				}
			}, null, null, null)), out result);
			return result;
		}

		private List<string> CleanDataString(string s)
		{
			List<string> list = new List<string>();
			string text = "";
			string str = "";
			int i = 0;
			for (string[] array = s.ToString().Split('\r'); i < array.Length; i = checked(i + 1))
			{
				text = array[i].Trim('\t', ' ', '\r', '\n', '"');
				if (!(text.StartsWith(new string(new char[1]
				{
					'#'
				})) | text.Length == 0))
				{
					list.Add(str + text);
				}
			}
			return list;
		}

		private string dcode(string s)
		{
			byte[] bytes = Convert.FromBase64String(s);
			string @string = Encoding.Unicode.GetString(bytes);
			return s;
		}

		public void loadData(ref programLoadingProgressBar pbar, ref string SelLicense)
		{
            //TODO Just test license.
            //this.loadData2(ref SelLicense);
            string s = "Full";
            this.loadData2(ref s);
            checked
			{
				int num = pbar.PBar.Maximum - pbar.PBar.Value - 10;
				num = (int)Math.Round(unchecked((double)num / (double)this.names.Count));
			}
		}

		public int getNrec()
		{
			return this.bealist.Count;
		}

		public ArrayList getNames()
		{
			return this.lab_names;
		}

		public BEAData getData(int id)
		{
			return (BEAData)this.bealist[id];
		}

		public TabStorage getTabs(int id)
		{
			return (TabStorage)this.tablist[id];
		}

		public void addData(ref BEAData bd)
		{
			this.bealist.Add(bd);
			this.names.Add(bd.worksum.DataLabel);
			TabStorage value = new TabStorage();
			this.tablist.Add(value);
		}

		public string lastName()
		{
			return Conversions.ToString(this.lab_names[checked(this.lab_names.Count - 1)]);
		}

		private double[] StrArr_2_DblArr(string[] arr)
		{
			ArrayList arrayList = new ArrayList();
			double num = 0.0;
			checked
			{
				int num2 = arr.Length - 1;
				for (int i = 0; i <= num2; i++)
				{
					if (double.TryParse(arr[i], out num))
					{
						arrayList.Add(num);
					}
				}
			}
			return (double[])arrayList.ToArray(typeof(double));
		}

		public string Encrypt(string strInput)
		{
			string text = strInput;
			int num = 7;
			int num2 = Strings.Len(strInput);
			checked
			{
				for (int i = 1; i <= num2; i++)
				{
					StringType.MidStmtStr(ref text, i, 1, Conversions.ToString(Strings.Chr(Strings.Asc(Strings.Mid(text, i, 1)) + num)));
				}
				return text;
			}
		}

		public string Decrypt(string strInput)
		{
			string text = strInput;
			int num = 7;
			int num2 = Strings.Len(strInput);
			checked
			{
				for (int i = 1; i <= num2; i++)
				{
					StringType.MidStmtStr(ref text, i, 1, Conversions.ToString(Strings.Chr(Strings.Asc(Strings.Mid(text, i, 1)) - num)));
				}
				return text;
			}
		}
	}
}
