using Microsoft.VisualBasic.CompilerServices;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class frmImpactEntry : Form
	{
		private IContainer components;

		public frmImpactEntry()
		{
			this.InitializeComponent();
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			base.SuspendLayout();
			base.AutoScaleDimensions = new SizeF(9f, 22f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(292, 278);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.Name = "frmImpactEntry";
			this.Text = "frmImpactEntry";
			base.ResumeLayout(false);
		}
	}
}
