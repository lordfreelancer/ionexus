using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Runtime.CompilerServices;

namespace IOSNAP
{
	[StandardModule]
	internal sealed class LoadRegionalData
	{
		public static void OutputVar2XLS(ref Matrix arr, ref string filepath)
		{
            filepath = "VarValue.csv";

            string text = "";
			checked
			{
				int num = arr.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					text += "\r\n";
					int num2 = arr.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						text = text + "," + Conversions.ToString(arr[i, j]);
					}
				}
				StreamWriter streamWriter = new StreamWriter("debugfiles\\" + filepath);
				streamWriter.Write(text);
				streamWriter.Close();
				streamWriter.Dispose();
			}
		}

		public static void OutputVar2XLS(ref Vector arr, ref string filepath)
		{
            filepath = "VarValue.csv";

            checked
			{
				double[,] array = new double[1, arr.Count - 1 + 1];
				int num = arr.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					double[,] array2 = array;
					int num2 = i;
					double num3 = arr[i];
					array2[0, num2] = num3;
				}
				Matrix matrix = new Matrix(array);
				LoadRegionalData.OutputVar2XLS(ref matrix, ref filepath);
			}
		}

		public static void LoadDefaults(ref BEAData bedat)
		{
			OleDbConnection oleDbConnection = null;
			OleDbDataAdapter oleDbDataAdapter = null;
			DataTable dataTable = new DataTable();
			string datayear = bedat.worksum.datayear;
			string[] stateCodes = bedat.StateIDs.StateCodes;
			for (int i = 0; i < stateCodes.Length; i = checked(i + 1))
			{
				string str = stateCodes[i];
				StateData state = bedat.RegStateData.GetState(ref str);
				try
				{
					string str2 = FileSystem.CurDir() + "\\RegionalData" + datayear + ".xls";
					oleDbConnection = new OleDbConnection(string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + str2 + "';Extended Properties=\"Excel 8.0;HDR=NO\""));
					oleDbConnection.Open();
					oleDbDataAdapter = new OleDbDataAdapter("SELECT  * FROM [" + str + "$C3:C65]", oleDbConnection);
					dataTable = new DataTable();
					oleDbDataAdapter.Fill(dataTable);
					state.EC_base = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$C70:C73]";
					oleDbDataAdapter.Fill(dataTable);
					state.EC_ImpGov = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$C67:C67]";
					oleDbDataAdapter.Fill(dataTable);
					Vector vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.EC_TotGovSUM = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$D3:D65]";
					oleDbDataAdapter.Fill(dataTable);
					state.GDP_base = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$D70:D73]";
					oleDbDataAdapter.Fill(dataTable);
					state.GDP_ImpGov = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$D67:D67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.GDP_TotGovSUM = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$E3:E65]";
					oleDbDataAdapter.Fill(dataTable);
					state.GOS_base = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$E70:E73]";
					oleDbDataAdapter.Fill(dataTable);
					state.GOS_ImpGov = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$E67:E67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.GOS_TotGovSUM = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$F3:F65]";
					oleDbDataAdapter.Fill(dataTable);
					state.T_base = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$F70:F73]";
					oleDbDataAdapter.Fill(dataTable);
					state.T_ImpGov = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$F67:F67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.T_TotGovSUM = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$G3:G65]";
					oleDbDataAdapter.Fill(dataTable);
					state.Employment_base = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$G70:G73]";
					oleDbDataAdapter.Fill(dataTable);
					state.Employment_ImpGov = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$G67:G67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.Employment_TotGovSUM = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$M3:M65]";
					oleDbDataAdapter.Fill(dataTable);
					state.Employment_base_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$M70:M73]";
					oleDbDataAdapter.Fill(dataTable);
					state.Employment_ImpGov_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$M67:M67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.Employment_TotGovSUM_flag = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$L3:L65]";
					oleDbDataAdapter.Fill(dataTable);
					state.T_base_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$L70:L73]";
					oleDbDataAdapter.Fill(dataTable);
					state.T_ImpGov_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$L67:L67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.T_TotGovSUM_flag = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$K3:K65]";
					oleDbDataAdapter.Fill(dataTable);
					state.GOS_base_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$K70:K73]";
					oleDbDataAdapter.Fill(dataTable);
					state.GOS_ImpGov_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$K67:K67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.GOS_TotGovSUM_flag = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$J3:J65]";
					oleDbDataAdapter.Fill(dataTable);
					state.GDP_base_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$J70:J73]";
					oleDbDataAdapter.Fill(dataTable);
					state.GDP_ImpGov_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$J67:J67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.GDP_TotGovSUM_flag = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$I3:I65]";
					oleDbDataAdapter.Fill(dataTable);
					state.EC_base_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$I70:I73]";
					oleDbDataAdapter.Fill(dataTable);
					state.EC_ImpGov_flag = LoadRegionalData.DT1Dbl(ref dataTable);
					dataTable.Dispose();
					dataTable = new DataTable();
					oleDbDataAdapter.SelectCommand.CommandText = "SELECT  * FROM [" + str + "$I67:I67]";
					oleDbDataAdapter.Fill(dataTable);
					vector = LoadRegionalData.DT1Dbl(ref dataTable);
					state.EC_TotGovSUM_flag = vector[0];
					dataTable.Dispose();
					dataTable = new DataTable();
					state.StateSetup(ref bedat.NTables.FTE_Ratio);
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
				finally
				{
					oleDbConnection.Close();
					oleDbDataAdapter.Dispose();
					dataTable.Dispose();
				}
			}
		}

		public static Vector DT1Dbl(ref DataTable dt)
		{
			checked
			{
				double[] array;
				if (dt.Rows.Count > 1)
				{
					array = new double[dt.Rows.Count - 1 + 1];
					int num = dt.Rows.Count - 1;
					for (int i = 0; i <= num; i++)
					{
						if (!Convert.IsDBNull(RuntimeHelpers.GetObjectValue(dt.Rows[i][0])))
						{
							array[i] = double.Parse(Conversions.ToString(dt.Rows[i][0]));
						}
					}
				}
				else
				{
					array = new double[dt.Columns.Count - 1 + 1];
					int num2 = dt.Columns.Count - 1;
					for (int i = 0; i <= num2; i++)
					{
						if (!Convert.IsDBNull(RuntimeHelpers.GetObjectValue(dt.Rows[0][i])))
						{
							array[i] = double.Parse(Conversions.ToString(dt.Rows[0][i]));
						}
					}
				}
				return new Vector(array);
			}
		}

		public static Matrix DT2Matrix(ref DataTable dt)
		{
			Matrix matrix = new Matrix(dt.Rows.Count, dt.Columns.Count);
			checked
			{
				int num = matrix.NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					DataRow dataRow = dt.Rows[i];
					int num2 = matrix.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						if (!Convert.IsDBNull(RuntimeHelpers.GetObjectValue(dataRow[j])))
						{
							matrix[i, j] = double.Parse(Conversions.ToString(dataRow[j]));
						}
					}
				}
				return matrix;
			}
		}

		public static double[] strArr2Dbl(string[] arr)
		{
			checked
			{
				double[] array = new double[arr.GetUpperBound(0) + 1];
				int upperBound = arr.GetUpperBound(0);
				for (int i = 0; i <= upperBound; i++)
				{
					double.TryParse(arr[i], out array[i]);
				}
				return array;
			}
		}
	}
}
