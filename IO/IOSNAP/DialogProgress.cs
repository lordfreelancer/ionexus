using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class DialogProgress : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("TableLayoutPanel1")]
		private TableLayoutPanel _TableLayoutPanel1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OK_Button")]
		private Button _OK_Button;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Cancel_Button")]
		private Button _Cancel_Button;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("PBar")]
		private ProgressBar _PBar;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label1")]
		private Label _Label1;

		private string s;

		internal virtual TableLayoutPanel TableLayoutPanel1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button OK_Button
		{
			[CompilerGenerated]
			get
			{
				return this._OK_Button;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OK_Button_Click;
				Button oK_Button = this._OK_Button;
				if (oK_Button != null)
				{
					oK_Button.Click -= value2;
				}
				this._OK_Button = value;
				oK_Button = this._OK_Button;
				if (oK_Button != null)
				{
					oK_Button.Click += value2;
				}
			}
		}

		internal virtual Button Cancel_Button
		{
			[CompilerGenerated]
			get
			{
				return this._Cancel_Button;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Cancel_Button_Click;
				Button cancel_Button = this._Cancel_Button;
				if (cancel_Button != null)
				{
					cancel_Button.Click -= value2;
				}
				this._Cancel_Button = value;
				cancel_Button = this._Cancel_Button;
				if (cancel_Button != null)
				{
					cancel_Button.Click += value2;
				}
			}
		}

		internal virtual ProgressBar PBar
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Label Label1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.TableLayoutPanel1 = new TableLayoutPanel();
			this.Cancel_Button = new Button();
			this.OK_Button = new Button();
			this.PBar = new ProgressBar();
			this.Label1 = new Label();
			this.TableLayoutPanel1.SuspendLayout();
			base.SuspendLayout();
			this.TableLayoutPanel1.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.TableLayoutPanel1.ColumnCount = 2;
			this.TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
			this.TableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
			this.TableLayoutPanel1.Controls.Add(this.Cancel_Button, 1, 0);
			this.TableLayoutPanel1.Controls.Add(this.OK_Button, 0, 0);
			this.TableLayoutPanel1.Location = new Point(416, 456);
			this.TableLayoutPanel1.Margin = new Padding(4, 6, 4, 6);
			this.TableLayoutPanel1.Name = "TableLayoutPanel1";
			this.TableLayoutPanel1.RowCount = 1;
			this.TableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
			this.TableLayoutPanel1.Size = new Size(219, 58);
			this.TableLayoutPanel1.TabIndex = 0;
			this.Cancel_Button.Anchor = AnchorStyles.None;
			this.Cancel_Button.DialogResult = DialogResult.Cancel;
			this.Cancel_Button.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Cancel_Button.Location = new Point(114, 6);
			this.Cancel_Button.Margin = new Padding(4, 6, 4, 6);
			this.Cancel_Button.Name = "Cancel_Button";
			this.Cancel_Button.Size = new Size(100, 46);
			this.Cancel_Button.TabIndex = 1;
			this.Cancel_Button.Text = "Cancel";
			this.OK_Button.Anchor = AnchorStyles.None;
			this.OK_Button.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.OK_Button.Location = new Point(4, 6);
			this.OK_Button.Margin = new Padding(4, 6, 4, 6);
			this.OK_Button.Name = "OK_Button";
			this.OK_Button.Size = new Size(100, 46);
			this.OK_Button.TabIndex = 0;
			this.OK_Button.Text = "OK";
			this.PBar.Location = new Point(61, 123);
			this.PBar.Name = "PBar";
			this.PBar.Size = new Size(497, 25);
			this.PBar.TabIndex = 1;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Calibri", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label1.Location = new Point(176, 236);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(111, 24);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "Please Wait";
			base.AcceptButton = this.OK_Button;
			base.AutoScaleDimensions = new SizeF(9f, 22f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.CancelButton = this.Cancel_Button;
			base.ClientSize = new Size(652, 534);
			base.Controls.Add(this.Label1);
			base.Controls.Add(this.PBar);
			base.Controls.Add(this.TableLayoutPanel1);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.Margin = new Padding(4, 6, 4, 6);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "DialogProgress";
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterParent;
			this.Text = "DialogProgress";
			this.TableLayoutPanel1.ResumeLayout(false);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public DialogProgress()
		{
			base.Load += this.Form_Load;
			this.InitializeComponent();
		}

		private void Form_Load(object sender, EventArgs e)
		{
			this.Refresh();
		}

		public void setMax(int v)
		{
			this.PBar.Maximum = v;
		}

		public void setValue(int v)
		{
			this.PBar.Value = v;
			this.PBar.Refresh();
		}

		public int getValue()
		{
			return this.PBar.Value;
		}

		private void OK_Button_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void Cancel_Button_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}
	}
}
