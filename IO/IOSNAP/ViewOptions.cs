using System;

namespace IOSNAP
{
	public class ViewOptions : ICloneable
	{
		public int decimalsGenForm;

		public bool @fixed;

		public bool Hide0s;

		public string GenFormBase;

		public string CoeffFormatBase;

		public int decimalsCoeffForm;

		public string RoundFormatBase;

		public string getGeneralFormat
		{
			get
			{
				string str = new string('#', this.decimalsGenForm);
				return this.GenFormBase + str;
			}
		}

		public string getCoefficientFormat
		{
			get
			{
				string str = new string('#', this.decimalsCoeffForm);
				return this.CoeffFormatBase + str;
			}
		}

		public string getRoundFormat
		{
			get
			{
				return this.RoundFormatBase;
			}
		}

		public ViewOptions()
		{
			this.decimalsGenForm = 0;
			this.@fixed = false;
			this.Hide0s = false;
			this.GenFormBase = "#,##0.00";
			this.CoeffFormatBase = "#,##0.000";
			this.decimalsCoeffForm = 0;
			this.RoundFormatBase = "#,##0";
		}

		public object Clone()
		{
			ViewOptions viewOptions = new ViewOptions();
			viewOptions.decimalsGenForm = this.decimalsGenForm;
			viewOptions.@fixed = this.@fixed;
			viewOptions.Hide0s = this.Hide0s;
			return viewOptions;
		}

		object ICloneable.Clone()
		{
			//ILSpy generated this explicit interface implementation from .override directive in Clone
			return this.Clone();
		}
	}
}
