using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class ViewIO_Accounts_Form : Form
	{
		private IContainer components;

		private Button DisplayButton;

		private Button Button2;

		private Label Label1;

		private ComboBox natYearCbox;

		private ComboBox RegisteredDataCbox;

		private Panel Panel1;

		private Label Label5;

		private RadioButton rbutt_UserRegistered;

		private RadioButton rbutt_National;

		private CheckBox cbox_RegionalOnly;

		private CheckBox cbox_Use;

		private CheckBox cbox_VA;

		private CheckBox cbox_FD;

		private CheckBox cbox_Make;

		private CheckBox cboxRegNat;

		private DataWrapREADER mainData;

		private DataWrapREADER yearlyData;

		private DataWrapREADER userData;

		private string PSelect;

		public BEAData bdat;

		public bool isRegionDataOnly;

		public bool isBothRegNatData;

		public bool isMake;

		public bool isUse;

		public bool isFD;

		public bool isVA;

		public bool isUserRegistered;

		public bool isNational;

		public bool isnat;

		public int idxnat;

		//internal virtual Button DisplayButton
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._DisplayButton;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.OK_Click;
		//		Button displayButton = this._DisplayButton;
		//		if (displayButton != null)
		//		{
		//			displayButton.Click -= value2;
		//		}
		//		this._DisplayButton = value;
		//		displayButton = this._DisplayButton;
		//		if (displayButton != null)
		//		{
		//			displayButton.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button Button2
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual Label Label1
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual ComboBox natYearCbox
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._natYearCbox;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.natYearCbox_SelectedIndexChanged;
		//		ComboBox natYearCbox = this._natYearCbox;
		//		if (natYearCbox != null)
		//		{
		//			natYearCbox.SelectedIndexChanged -= value2;
		//		}
		//		this._natYearCbox = value;
		//		natYearCbox = this._natYearCbox;
		//		if (natYearCbox != null)
		//		{
		//			natYearCbox.SelectedIndexChanged += value2;
		//		}
		//	}
		//}

		//internal virtual ComboBox RegisteredDataCbox
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._RegisteredDataCbox;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.RegisteredDataCbox_SelectedIndexChanged;
		//		ComboBox registeredDataCbox = this._RegisteredDataCbox;
		//		if (registeredDataCbox != null)
		//		{
		//			registeredDataCbox.SelectedIndexChanged -= value2;
		//		}
		//		this._RegisteredDataCbox = value;
		//		registeredDataCbox = this._RegisteredDataCbox;
		//		if (registeredDataCbox != null)
		//		{
		//			registeredDataCbox.SelectedIndexChanged += value2;
		//		}
		//	}
		//}

		//internal virtual Panel Panel1
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual Label Label5
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual RadioButton rbutt_UserRegistered
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._rbutt_UserRegistered;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.UserRadioButton_CheckedChanged;
		//		RadioButton rbutt_UserRegistered = this._rbutt_UserRegistered;
		//		if (rbutt_UserRegistered != null)
		//		{
		//			rbutt_UserRegistered.CheckedChanged -= value2;
		//		}
		//		this._rbutt_UserRegistered = value;
		//		rbutt_UserRegistered = this._rbutt_UserRegistered;
		//		if (rbutt_UserRegistered != null)
		//		{
		//			rbutt_UserRegistered.CheckedChanged += value2;
		//		}
		//	}
		//}

		//internal virtual RadioButton rbutt_National
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._rbutt_National;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.NationalRadioButton_CheckedChanged;
		//		RadioButton rbutt_National = this._rbutt_National;
		//		if (rbutt_National != null)
		//		{
		//			rbutt_National.CheckedChanged -= value2;
		//		}
		//		this._rbutt_National = value;
		//		rbutt_National = this._rbutt_National;
		//		if (rbutt_National != null)
		//		{
		//			rbutt_National.CheckedChanged += value2;
		//		}
		//	}
		//}

		//internal virtual CheckBox cbox_RegionalOnly
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cbox_RegionalOnly;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cbox_Regional_CheckedChanged;
		//		CheckBox cbox_RegionalOnly = this._cbox_RegionalOnly;
		//		if (cbox_RegionalOnly != null)
		//		{
		//			cbox_RegionalOnly.CheckedChanged -= value2;
		//		}
		//		this._cbox_RegionalOnly = value;
		//		cbox_RegionalOnly = this._cbox_RegionalOnly;
		//		if (cbox_RegionalOnly != null)
		//		{
		//			cbox_RegionalOnly.CheckedChanged += value2;
		//		}
		//	}
		//}

		//internal virtual CheckBox cbox_Use
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cbox_Use;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cbox_Use_CheckedChanged;
		//		CheckBox cbox_Use = this._cbox_Use;
		//		if (cbox_Use != null)
		//		{
		//			cbox_Use.CheckedChanged -= value2;
		//		}
		//		this._cbox_Use = value;
		//		cbox_Use = this._cbox_Use;
		//		if (cbox_Use != null)
		//		{
		//			cbox_Use.CheckedChanged += value2;
		//		}
		//	}
		//}

		//internal virtual CheckBox cbox_VA
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cbox_VA;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cbox_VA_CheckedChanged;
		//		CheckBox cbox_VA = this._cbox_VA;
		//		if (cbox_VA != null)
		//		{
		//			cbox_VA.CheckedChanged -= value2;
		//		}
		//		this._cbox_VA = value;
		//		cbox_VA = this._cbox_VA;
		//		if (cbox_VA != null)
		//		{
		//			cbox_VA.CheckedChanged += value2;
		//		}
		//	}
		//}

		//internal virtual CheckBox cbox_FD
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cbox_FD;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cbox_FD_CheckedChanged;
		//		CheckBox cbox_FD = this._cbox_FD;
		//		if (cbox_FD != null)
		//		{
		//			cbox_FD.CheckedChanged -= value2;
		//		}
		//		this._cbox_FD = value;
		//		cbox_FD = this._cbox_FD;
		//		if (cbox_FD != null)
		//		{
		//			cbox_FD.CheckedChanged += value2;
		//		}
		//	}
		//}

		//internal virtual CheckBox cbox_Make
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cbox_Make;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.cbox_Make_CheckedChanged;
		//		CheckBox cbox_Make = this._cbox_Make;
		//		if (cbox_Make != null)
		//		{
		//			cbox_Make.CheckedChanged -= value2;
		//		}
		//		this._cbox_Make = value;
		//		cbox_Make = this._cbox_Make;
		//		if (cbox_Make != null)
		//		{
		//			cbox_Make.CheckedChanged += value2;
		//		}
		//	}
		//}

		//internal virtual CheckBox cboxRegNat
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._cboxRegNat;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.RegNat_CheckedChanged;
		//		CheckBox cboxRegNat = this._cboxRegNat;
		//		if (cboxRegNat != null)
		//		{
		//			cboxRegNat.CheckedChanged -= value2;
		//		}
		//		this._cboxRegNat = value;
		//		cboxRegNat = this._cboxRegNat;
		//		if (cboxRegNat != null)
		//		{
		//			cboxRegNat.CheckedChanged += value2;
		//		}
		//	}
		//}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.DisplayButton = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.natYearCbox = new System.Windows.Forms.ComboBox();
            this.RegisteredDataCbox = new System.Windows.Forms.ComboBox();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.cbox_Use = new System.Windows.Forms.CheckBox();
            this.cbox_VA = new System.Windows.Forms.CheckBox();
            this.cbox_FD = new System.Windows.Forms.CheckBox();
            this.cbox_Make = new System.Windows.Forms.CheckBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.rbutt_UserRegistered = new System.Windows.Forms.RadioButton();
            this.rbutt_National = new System.Windows.Forms.RadioButton();
            this.cbox_RegionalOnly = new System.Windows.Forms.CheckBox();
            this.cboxRegNat = new System.Windows.Forms.CheckBox();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DisplayButton
            // 
            this.DisplayButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.DisplayButton.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DisplayButton.Location = new System.Drawing.Point(530, 415);
            this.DisplayButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DisplayButton.Name = "DisplayButton";
            this.DisplayButton.Size = new System.Drawing.Size(122, 48);
            this.DisplayButton.TabIndex = 0;
            this.DisplayButton.Text = "Display";
            this.DisplayButton.UseVisualStyleBackColor = true;
            this.DisplayButton.Click += new System.EventHandler(this.OK_Click);
            // 
            // Button2
            // 
            this.Button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button2.Location = new System.Drawing.Point(396, 415);
            this.Button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(122, 48);
            this.Button2.TabIndex = 1;
            this.Button2.Text = "Cancel";
            this.Button2.UseVisualStyleBackColor = true;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(208, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(388, 58);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "Select IO Accounts";
            // 
            // natYearCbox
            // 
            this.natYearCbox.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.natYearCbox.FormattingEnabled = true;
            this.natYearCbox.Location = new System.Drawing.Point(214, 73);
            this.natYearCbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.natYearCbox.Name = "natYearCbox";
            this.natYearCbox.Size = new System.Drawing.Size(438, 44);
            this.natYearCbox.TabIndex = 6;
            this.natYearCbox.SelectedIndexChanged += new System.EventHandler(this.natYearCbox_SelectedIndexChanged);
            // 
            // RegisteredDataCbox
            // 
            this.RegisteredDataCbox.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegisteredDataCbox.FormattingEnabled = true;
            this.RegisteredDataCbox.Location = new System.Drawing.Point(214, 176);
            this.RegisteredDataCbox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RegisteredDataCbox.Name = "RegisteredDataCbox";
            this.RegisteredDataCbox.Size = new System.Drawing.Size(438, 44);
            this.RegisteredDataCbox.TabIndex = 8;
            this.RegisteredDataCbox.SelectedIndexChanged += new System.EventHandler(this.RegisteredDataCbox_SelectedIndexChanged);
            // 
            // Panel1
            // 
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Controls.Add(this.cbox_Use);
            this.Panel1.Controls.Add(this.cbox_VA);
            this.Panel1.Controls.Add(this.cbox_FD);
            this.Panel1.Controls.Add(this.cbox_Make);
            this.Panel1.Controls.Add(this.Label5);
            this.Panel1.Location = new System.Drawing.Point(30, 246);
            this.Panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(303, 217);
            this.Panel1.TabIndex = 9;
            // 
            // cbox_Use
            // 
            this.cbox_Use.AutoSize = true;
            this.cbox_Use.Checked = true;
            this.cbox_Use.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbox_Use.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_Use.Location = new System.Drawing.Point(40, 58);
            this.cbox_Use.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbox_Use.Name = "cbox_Use";
            this.cbox_Use.Size = new System.Drawing.Size(114, 49);
            this.cbox_Use.TabIndex = 17;
            this.cbox_Use.Text = "Use";
            this.cbox_Use.UseVisualStyleBackColor = true;
            this.cbox_Use.CheckedChanged += new System.EventHandler(this.cbox_Use_CheckedChanged);
            // 
            // cbox_VA
            // 
            this.cbox_VA.AutoSize = true;
            this.cbox_VA.Checked = true;
            this.cbox_VA.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbox_VA.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_VA.Location = new System.Drawing.Point(40, 166);
            this.cbox_VA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbox_VA.Name = "cbox_VA";
            this.cbox_VA.Size = new System.Drawing.Size(244, 49);
            this.cbox_VA.TabIndex = 16;
            this.cbox_VA.Text = "Value Added";
            this.cbox_VA.UseVisualStyleBackColor = true;
            this.cbox_VA.CheckedChanged += new System.EventHandler(this.cbox_VA_CheckedChanged);
            // 
            // cbox_FD
            // 
            this.cbox_FD.AutoSize = true;
            this.cbox_FD.Checked = true;
            this.cbox_FD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbox_FD.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_FD.Location = new System.Drawing.Point(40, 130);
            this.cbox_FD.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbox_FD.Name = "cbox_FD";
            this.cbox_FD.Size = new System.Drawing.Size(263, 49);
            this.cbox_FD.TabIndex = 15;
            this.cbox_FD.Text = "Final Demand";
            this.cbox_FD.UseVisualStyleBackColor = true;
            this.cbox_FD.CheckedChanged += new System.EventHandler(this.cbox_FD_CheckedChanged);
            // 
            // cbox_Make
            // 
            this.cbox_Make.AutoSize = true;
            this.cbox_Make.Checked = true;
            this.cbox_Make.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbox_Make.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_Make.Location = new System.Drawing.Point(40, 94);
            this.cbox_Make.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbox_Make.Name = "cbox_Make";
            this.cbox_Make.Size = new System.Drawing.Size(142, 49);
            this.cbox_Make.TabIndex = 14;
            this.cbox_Make.Text = "Make";
            this.cbox_Make.UseVisualStyleBackColor = true;
            this.cbox_Make.CheckedChanged += new System.EventHandler(this.cbox_Make_CheckedChanged);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(74, 12);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(219, 49);
            this.Label5.TabIndex = 4;
            this.Label5.Text = "Select Table";
            // 
            // rbutt_UserRegistered
            // 
            this.rbutt_UserRegistered.AutoSize = true;
            this.rbutt_UserRegistered.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbutt_UserRegistered.Location = new System.Drawing.Point(30, 176);
            this.rbutt_UserRegistered.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rbutt_UserRegistered.Name = "rbutt_UserRegistered";
            this.rbutt_UserRegistered.Size = new System.Drawing.Size(263, 40);
            this.rbutt_UserRegistered.TabIndex = 12;
            this.rbutt_UserRegistered.Text = "User Registered";
            this.rbutt_UserRegistered.UseVisualStyleBackColor = true;
            this.rbutt_UserRegistered.CheckedChanged += new System.EventHandler(this.UserRadioButton_CheckedChanged);
            // 
            // rbutt_National
            // 
            this.rbutt_National.AutoSize = true;
            this.rbutt_National.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbutt_National.Location = new System.Drawing.Point(30, 73);
            this.rbutt_National.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rbutt_National.Name = "rbutt_National";
            this.rbutt_National.Size = new System.Drawing.Size(232, 40);
            this.rbutt_National.TabIndex = 10;
            this.rbutt_National.Text = "National Data";
            this.rbutt_National.UseVisualStyleBackColor = true;
            this.rbutt_National.CheckedChanged += new System.EventHandler(this.NationalRadioButton_CheckedChanged);
            // 
            // cbox_RegionalOnly
            // 
            this.cbox_RegionalOnly.AutoSize = true;
            this.cbox_RegionalOnly.Enabled = false;
            this.cbox_RegionalOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_RegionalOnly.Location = new System.Drawing.Point(88, 122);
            this.cbox_RegionalOnly.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbox_RegionalOnly.Name = "cbox_RegionalOnly";
            this.cbox_RegionalOnly.Size = new System.Drawing.Size(237, 40);
            this.cbox_RegionalOnly.TabIndex = 13;
            this.cbox_RegionalOnly.Text = "Regional Only";
            this.cbox_RegionalOnly.UseVisualStyleBackColor = true;
            this.cbox_RegionalOnly.CheckedChanged += new System.EventHandler(this.cbox_Regional_CheckedChanged);
            // 
            // cboxRegNat
            // 
            this.cboxRegNat.AutoSize = true;
            this.cboxRegNat.Enabled = false;
            this.cboxRegNat.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxRegNat.Location = new System.Drawing.Point(276, 122);
            this.cboxRegNat.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cboxRegNat.Name = "cboxRegNat";
            this.cboxRegNat.Size = new System.Drawing.Size(511, 40);
            this.cboxRegNat.TabIndex = 14;
            this.cboxRegNat.Text = "Both Regional and National Tables";
            this.cboxRegNat.UseVisualStyleBackColor = true;
            this.cboxRegNat.CheckedChanged += new System.EventHandler(this.RegNat_CheckedChanged);
            // 
            // ViewIO_Accounts_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(17F, 40F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 505);
            this.Controls.Add(this.cboxRegNat);
            this.Controls.Add(this.cbox_RegionalOnly);
            this.Controls.Add(this.rbutt_UserRegistered);
            this.Controls.Add(this.rbutt_National);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.RegisteredDataCbox);
            this.Controls.Add(this.natYearCbox);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.DisplayButton);
            this.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewIO_Accounts_Form";
            this.Text = "View IO Accounts";
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		public ViewIO_Accounts_Form(ref DataWrapREADER d, ref int idx)
		{
			this.PSelect = "Please Select ...";
			this.isRegionDataOnly = false;
			this.isBothRegNatData = false;
			this.isMake = true;
			this.isUse = true;
			this.isFD = true;
			this.isVA = true;
			this.isUserRegistered = false;
			this.isNational = false;
			this.isnat = true;
			this.idxnat = 0;
			this.InitializeComponent();
			this.mainData = d;
			this.yearlyData = new DataWrapREADER();
			this.yearlyData.setEmpty();
			this.userData = new DataWrapREADER();
			this.userData.setEmpty();
			checked
			{
				int num = this.mainData.getNrec() - 1;
				for (int i = 0; i <= num; i++)
				{
					BEAData bEAData = unchecked((BEAData)this.mainData.bealist[i]);
					if (Conversions.ToBoolean(Operators.OrObject(Operators.CompareObjectEqual(bEAData.worksum.getDataType(), "Base Data", false), Operators.CompareObjectEqual(bEAData.worksum.getDataType(), "User Defined", false))))
					{
						this.yearlyData.addData(ref bEAData);
						if (i == idx)
						{
							this.isnat = true;
							this.idxnat = this.yearlyData.getNrec();
						}
					}
					else
					{
						this.userData.addData(ref bEAData);
						if (i == idx)
						{
							this.isnat = false;
							this.idxnat = this.userData.getNrec();
						}
					}
				}
				if (this.userData.getNrec() > 0)
				{
					this.rbutt_UserRegistered.Enabled = true;
					this.RegisteredDataCbox.Items.Clear();
					int num2 = this.userData.getNrec() - 1;
					for (int j = 0; j <= num2; j++)
					{
						this.RegisteredDataCbox.Items.Add(this.userData.getData(j).worksum.DataLabel);
					}
					this.RegisteredDataCbox.DropDownStyle = ComboBoxStyle.DropDownList;
					this.RegisteredDataCbox.SelectedItem = this.userData.getData(0).worksum.DataLabel;
				}
				else
				{
					this.rbutt_UserRegistered.Enabled = false;
				}
				this.natYearCbox.Items.Clear();
				int num3 = this.yearlyData.getNrec() - 1;
				for (int k = 0; k <= num3; k++)
				{
					this.natYearCbox.Items.Add(this.yearlyData.getData(k).worksum.DataLabel);
				}
				this.natYearCbox.DropDownStyle = ComboBoxStyle.DropDownList;
				this.RegisteredDataCbox.Enabled = false;
				if (this.isnat)
				{
					this.rbutt_National.Checked = true;
					this.natYearCbox.SelectedItem = this.yearlyData.getData(this.idxnat - 1).worksum.DataLabel;
					this.natYearCbox.SelectedIndex = this.idxnat - 1;
					this.bdat = this.yearlyData.getData(this.idxnat - 1);
				}
				else
				{
					this.rbutt_UserRegistered.Checked = true;
					this.RegisteredDataCbox.SelectedItem = this.userData.getData(this.idxnat - 1).worksum.DataLabel;
					this.RegisteredDataCbox.SelectedIndex = this.idxnat - 1;
					this.bdat = this.userData.getData(this.idxnat - 1);
				}
				if (this.bdat.isDataRegionalized)
				{
					this.cbox_RegionalOnly.Enabled = true;
					this.cboxRegNat.Enabled = true;
				}
				else
				{
					this.cbox_RegionalOnly.Enabled = false;
					this.cbox_RegionalOnly.Checked = false;
					this.cboxRegNat.Enabled = false;
					this.cboxRegNat.Checked = false;
				}
				this.DisplayButton.Enabled = true;
			}
		}

		private void natYearCbox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = Conversions.ToString(this.natYearCbox.SelectedItem);
            //TODO LAter
			//if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 2;
			//		this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = false;
			//	}
			//	else if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//}
			//if (this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread)
			//{
			//	int selectedIndex = this.natYearCbox.SelectedIndex;
			//	if (selectedIndex >= 0)
			//	{
			//		this.bdat = this.yearlyData.getData(selectedIndex);
			//		if (this.bdat.isDataRegionalized)
			//		{
			//			this.cbox_RegionalOnly.Enabled = true;
			//			this.cboxRegNat.Enabled = true;
			//		}
			//		else
			//		{
			//			this.cbox_RegionalOnly.Enabled = false;
			//			this.cbox_RegionalOnly.Checked = false;
			//			this.cboxRegNat.Checked = false;
			//			this.cboxRegNat.Enabled = false;
			//		}
			//		this.DisplayButton.Enabled = true;
			//	}
			//}
			//else
			//{
			//	this._0024STATIC_0024natYearCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = true;
			//}
		}

		private void cbox_Regional_CheckedChanged(object sender, EventArgs e)
		{
			if (this.cbox_RegionalOnly.Checked)
			{
				this.isRegionDataOnly = true;
				this.isBothRegNatData = false;
				this.cboxRegNat.Checked = false;
			}
			else
			{
				this.isRegionDataOnly = false;
			}
		}

		private void RegNat_CheckedChanged(object sender, EventArgs e)
		{
			if (this.cboxRegNat.Checked)
			{
				this.isRegionDataOnly = false;
				this.isBothRegNatData = true;
				this.cbox_RegionalOnly.Checked = false;
			}
			else
			{
				this.isBothRegNatData = false;
			}
		}

		private void RegisteredDataCbox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = Conversions.ToString(this.RegisteredDataCbox.SelectedItem);
            //TODO Later
			//if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 2;
			//		this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = false;
			//	}
			//	else if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//}
			//if (this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread)
			//{
			//	int selectedIndex = this.RegisteredDataCbox.SelectedIndex;
			//	if (selectedIndex >= 0)
			//	{
			//		this.bdat = this.userData.getData(selectedIndex);
			//		this.DisplayButton.Enabled = true;
			//	}
			//}
			//else
			//{
			//	this._0024STATIC_0024RegisteredDataCbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = true;
			//}
		}

		private void cbox_Use_CheckedChanged(object sender, EventArgs e)
		{
			if (this.cbox_Use.Checked)
			{
				this.isUse = true;
			}
			else
			{
				this.isUse = false;
			}
		}

		private void cbox_Make_CheckedChanged(object sender, EventArgs e)
		{
			if (this.cbox_Make.Checked)
			{
				this.isMake = true;
			}
			else
			{
				this.isMake = false;
			}
		}

		private void cbox_FD_CheckedChanged(object sender, EventArgs e)
		{
			if (this.cbox_FD.Checked)
			{
				this.isFD = true;
			}
			else
			{
				this.isFD = false;
			}
		}

		private void cbox_VA_CheckedChanged(object sender, EventArgs e)
		{
			if (this.cbox_VA.Checked)
			{
				this.isVA = true;
			}
			else
			{
				this.isVA = false;
			}
		}

		private void OK_Click(object sender, EventArgs e)
		{
			this.isMake = Conversions.ToBoolean(Interaction.IIf(this.cbox_Make.Checked, true, false));
			this.isUse = Conversions.ToBoolean(Interaction.IIf(this.cbox_Use.Checked, true, false));
			this.isFD = Conversions.ToBoolean(Interaction.IIf(this.cbox_FD.Checked, true, false));
			this.isVA = Conversions.ToBoolean(Interaction.IIf(this.cbox_VA.Checked, true, false));
			this.isNational = Conversions.ToBoolean(Interaction.IIf(this.rbutt_National.Checked, true, false));
			this.isUserRegistered = Conversions.ToBoolean(Interaction.IIf(this.rbutt_UserRegistered.Checked, true, false));
			this.isRegionDataOnly = Conversions.ToBoolean(Interaction.IIf(this.cbox_RegionalOnly.Checked, true, false));
			this.isBothRegNatData = Conversions.ToBoolean(Interaction.IIf(this.cboxRegNat.Checked, true, false));
		}

		public string getLabel()
		{
			return this.bdat.worksum.DataLabel;
		}

		private void NationalRadioButton_CheckedChanged(object sender, EventArgs e)
		{
            //TODO Later
            //if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init == null)
            //{
            //	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
            //}
            //Monitor.Enter(this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init);
            //try
            //{
            //	if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State == 0)
            //	{
            //		this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State = 2;
            //		this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun = false;
            //	}
            //	else if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State == 2)
            //	{
            //		throw new IncompleteInitialization();
            //	}
            //}
            //finally
            //{
            //	this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State = 1;
            //	Monitor.Exit(this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init);
            //}
            //if (this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun)
            //{
            //	this._0024STATIC_0024NationalRadioButton_CheckedChanged_002420211C1280B1_0024frun = false;
            //}
            //else
            //{
            //	this.natYearCbox.Enabled = true;
            //	this.RegisteredDataCbox.Enabled = false;
            //	this.natYearCbox.SelectedItem = this.yearlyData.getData(0).worksum.DataLabel;
            //	this.bdat = this.yearlyData.getData(0);
            //	if (this.bdat.isDataRegionalized)
            //	{
            //		this.cbox_RegionalOnly.Enabled = true;
            //		this.cboxRegNat.Enabled = true;
            //	}
            //	else
            //	{
            //		this.cbox_RegionalOnly.Enabled = false;
            //		this.cbox_RegionalOnly.Checked = false;
            //		this.cboxRegNat.Enabled = false;
            //		this.cboxRegNat.Checked = false;
            //	}
            //}
        }

        private void UserRadioButton_CheckedChanged(object sender, EventArgs e)
		{
            //TODO Later
            //if (this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init == null)
            //{
            //	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
            //}
            //Monitor.Enter(this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init);
            //try
            //{
            //	if (this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State == 0)
            //	{
            //		this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State = 2;
            //		this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun = false;
            //	}
            //	else if (this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State == 2)
            //	{
            //		throw new IncompleteInitialization();
            //	}
            //}
            //finally
            //{
            //	this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init.State = 1;
            //	Monitor.Exit(this._0024STATIC_0024UserRadioButton_CheckedChanged_002420211C1280B1_0024frun_0024Init);
            //}
            //if (this.rbutt_UserRegistered.Checked)
            //{
            //	this.cbox_RegionalOnly.Enabled = false;
            //	this.cbox_RegionalOnly.Checked = false;
            //	this.cboxRegNat.Enabled = false;
            //	this.cboxRegNat.Checked = false;
            //	this.natYearCbox.Enabled = false;
            //	this.RegisteredDataCbox.Enabled = true;
            //	this.RegisteredDataCbox.SelectedItem = this.userData.getData(0).worksum.DataLabel;
            //	this.bdat = this.userData.getData(0);
            //}
        }
    }
}
