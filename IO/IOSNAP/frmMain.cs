using IOSNAP.My;
using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class frmMain : Form
	{
		private enum DispTotal : byte
		{
			Yes,
			No,
			Unchanged
		}

		private enum LicenseType : byte
		{
			DEMO,
			STUDENT,
			FULL
		}

		private class UndoData
		{
			public Matrix[] CopyData;

			public Matrix[] CloneData;

			private ToolStripMenuItem miUndo;

			private DataGridView dgv;

			public bool Enabled
			{
				get
				{
					return this.miUndo.Enabled;
				}
				set
				{
					this.miUndo.Enabled = value;
				}
			}

			public void SetData(Matrix Data)
			{
				this.CopyData[0] = Data;
				this.CloneData[0] = Data.Clone();
				this.CopyData[1] = null;
				this.CopyData[2] = null;
				this.miUndo.Enabled = true;
			}

			public void SetData(Matrix Base, Matrix Rows, Matrix Cols)
			{
				this.CopyData[0] = Base;
				this.CopyData[1] = Rows;
				this.CopyData[2] = Cols;
				this.CloneData[0] = Base.Clone();
				if ((object)Rows == null)
				{
					this.CloneData[1] = null;
				}
				else
				{
					this.CloneData[1] = Rows.Clone();
				}
				if ((object)Cols == null)
				{
					this.CloneData[2] = null;
				}
				else
				{
					this.CloneData[2] = Cols.Clone();
				}
				this.miUndo.Enabled = true;
			}

			public void Undo()
			{
				this.dgv.CellValueChanged -= MyProject.Forms.frmMain.dgv_CellValueChanged;
				int num = 0;
				checked
				{
					do
					{
						if (unchecked((object)this.CopyData[num]) != null)
						{
							int num2 = this.CopyData[num].NoRows - 1;
							for (int i = 0; i <= num2; i++)
							{
								int num3 = this.CopyData[num].NoCols - 1;
								for (int j = 0; j <= num3; j++)
								{
									if (this.CopyData[num][i, j] != this.CloneData[num][i, j])
									{
										this.CopyData[num][i, j] = this.CloneData[num][i, j];
										switch (num)
										{
										case 0:
											this.dgv[j, i].Value = this.CloneData[num][i, j];
											this.dgv[j, i].Style.BackColor = Color.Aqua;
											break;
										case 1:
											this.dgv[j, i + this.CopyData[0].NoRows].Value = this.CloneData[num][i, j];
											this.dgv[j, i + this.CopyData[0].NoRows].Style.BackColor = Color.Aqua;
											break;
										case 2:
											this.dgv[j + this.CopyData[0].NoCols, i].Value = this.CloneData[num][i, j];
											this.dgv[j + this.CopyData[0].NoCols, i].Style.BackColor = Color.Aqua;
											break;
										}
									}
								}
							}
						}
						num++;
					}
					while (num <= 2);
					this.dgv.CellValueChanged += MyProject.Forms.frmMain.dgv_CellValueChanged;
					this.miUndo.Enabled = false;
				}
			}

			public UndoData(ToolStripMenuItem miUndo, DataGridView dgv)
			{
				this.CopyData = new Matrix[3];
				this.CloneData = new Matrix[3];
				this.miUndo = miUndo;
				this.dgv = dgv;
			}
		}

		public class clsMenuHandler
		{
			public class MenuState
			{
				public ToolStripMenuItem mi;

				public bool[] Settings;

				public MenuState(ToolStripMenuItem mi, bool[] Settings)
				{
					this.Settings = new bool[3];
					this.mi = mi;
					this.Settings = Settings;
				}
			}

			private frmMain frmMe;

			public List<MenuState> arrSettings;

			public List<ToolStripItem> arrOptions;

			public List<ToolStripMenuItem> RecentlyEnabled;

			public clsMenuHandler(frmMain frmMe)
			{
				bool[] array = new bool[3]
				{
					false,
					false,
					true
				};
				bool[] array2 = new bool[3]
				{
					false,
					true,
					false
				};
				bool[] array3 = new bool[3]
				{
					false,
					true,
					true
				};
				this.RecentlyEnabled = new List<ToolStripMenuItem>();
				this.arrSettings = new List<MenuState>();
				this.arrOptions = new List<ToolStripItem>();
				this.frmMe = frmMe;
				this.arrOptions.Add(frmMe.miPrint);
				this.arrOptions.Add(frmMe.miCopy);
				this.arrOptions.Add(frmMe.miPrint1);
				this.arrOptions.Add(frmMe.miCopy1);
				this.arrOptions.Add(frmMe.midgvCopy);
				frmMain frmMain = null;
			}

			public void SetMenuStates()
			{
				if (this.frmMe.BEADat.isDataRegionalized)
				{
					this.frmMe.miAggregate.Enabled = false;
				}
				else
				{
					this.frmMe.miAggregate.Enabled = true;
				}
				if (this.frmMe.BEADat.eDataState == BEAData.DataState.Empty)
				{
					this.frmMe.RegButton.Enabled = false;
					this.frmMe.miPaste.Enabled = false;
					this.frmMe.miPaste1.Enabled = false;
					this.frmMe.midgvPaste.Enabled = false;
					this.frmMe.TSB4.Enabled = this.frmMe.copyHeadersStatus;
					this.frmMe.ViewToolStripMenuItem.Enabled = false;
					this.frmMe.RawDataToolStripMenuItem.Enabled = false;
					this.frmMe.EditToolStripMenuItem.Enabled = false;
					this.frmMe.miAggregate.Enabled = true;
				}
				if (this.frmMe.BEADat.worksum.DataTypeCode == SummaryDat.bcode.REGION_DATA)
				{
					this.frmMe.RegButton.Enabled = false;
					this.frmMe.miRegionalize.Enabled = false;
					this.frmMe.StateToolStripMenuItem.Enabled = false;
				}
				else
				{
					this.frmMe.RegButton.Enabled = true;
					this.frmMe.miRegionalize.Enabled = true;
					this.frmMe.StateToolStripMenuItem.Enabled = true;
				}
				if (this.frmMe.BEADat.eDataState == BEAData.DataState.Loaded)
				{
					this.frmMe.TSB4.Enabled = true;
					this.frmMe.ViewToolStripMenuItem.Enabled = true;
					this.frmMe.RawDataToolStripMenuItem.Enabled = true;
					this.frmMe.EditToolStripMenuItem.Enabled = true;
					this.frmMe.miAggregate.Enabled = true;
				}
				if (!this.frmMe.BEADat.isRequirementTablesCalculated)
				{
					goto IL_0225;
				}
				goto IL_0225;
				IL_0225:
				if (this.frmMe.BEADat.isEdited)
				{
					this.frmMe.miSave.Enabled = true;
					this.frmMe.miSave1.Enabled = true;
				}
				else
				{
					this.frmMe.miSave.Enabled = false;
					this.frmMe.miSave1.Enabled = false;
				}
				byte b = default(byte);
				switch (this.frmMe.BEADat.eDataState)
				{
				case BEAData.DataState.Empty:
					b = 0;
					break;
				case BEAData.DataState.Loaded:
					b = 1;
					break;
				}
				if (this.frmMe.BEADat.isRequirementTablesCalculated)
				{
					b = 2;
				}
				this.RecentlyEnabled.Clear();
				foreach (MenuState arrSetting in this.arrSettings)
				{
					if (arrSetting.Settings[b])
					{
						if (!arrSetting.mi.Enabled)
						{
							this.RecentlyEnabled.Add(arrSetting.mi);
						}
						arrSetting.mi.Enabled = true;
					}
					else
					{
						arrSetting.mi.Enabled = false;
					}
				}
				foreach (ToolStripItem arrOption in this.arrOptions)
				{
					arrOption.Enabled = this.frmMe.bDataDisplayed;
				}
			}
		}

		private IContainer components;

		public MultistateForm Multistatefrom;

		public double[] MePersonalIncomeM;

		public double[] MeDisposableIncomeM;

		private DataWrapREADER DataCollection;

		private DataWrapREADER RefDataCollection;

		public BEAData BEADat;

		public TabStorage TabStore;

		private DispData DispDat;

		private bool bCoeff;

		private ArrayList DirectTabList;

		private ViewOptions options;

		private clsMenuHandler MenuHandle;

		private UndoData UndoDat;

		public bool bDataDisplayed;

		public bool copyHeadersStatus;

		private string MainTabText;

		private GenDataStatus GovernmentAggStatus;

		public double DD;

		private ToolTip myToolTip;

		public bool AL_cboxChecked;

		public bool AK_cboxChecked;

		public bool AR_cboxChecked;

		public bool AZ_cboxChecked;

		public bool CA_cboxChecked;

		public bool CO_cboxChecked;

		public bool CT_cboxChecked;

		public bool DC_cboxChecked;

		public bool DE_cboxChecked;

		public bool FL_cboxChecked;

		public bool GA_cboxChecked;

		public bool IA_cboxChecked;

		public bool ID_cboxChecked;

		public bool IL_cboxChecked;

		public bool IN_cboxChecked;

		public bool KS_cboxChecked;

		public bool KY_cboxChecked;

		public bool LA_cboxChecked;

		public bool MA_cboxChecked;

		public bool ME_cboxChecked;

		public bool MI_cboxChecked;

		public bool MN_cboxChecked;

		public bool MD_cboxChecked;

		public bool MO_cboxChecked;

		public bool MS_cboxChecked;

		public bool MT_cboxChecked;

		public bool NE_cboxChecked;

		public bool ND_cboxChecked;

		public bool NC_cboxChecked;

		public bool NH_cboxChecked;

		public bool NJ_cboxChecked;

		public bool NM_cboxChecked;

		public bool NY_cboxChecked;

		public bool NV_cboxChecked;

		public bool HI_cboxChecked;

		public bool OH_cboxChecked;

		public bool OK_cboxChecked;

		public bool OR_cboxChecked;

		public bool PA_cboxChecked;

		public bool RI_cboxChecked;

		public bool SC_cboxChecked;

		public bool SD_cboxChecked;

		public bool TN_cboxChecked;

		public bool TX_cboxChecked;

		public bool UT_cboxChecked;

		public bool VA_cboxChecked;

		public bool VT_cboxChecked;

		public bool WA_cboxChecked;

		public bool WI_cboxChecked;

		public bool WV_cboxChecked;

		public bool WY_cboxChecked;

		public string Disclaimer;

		public PrintDocument MyPrintDocument;

		public DataGridViewPrinter MyDataGridViewPrinter;

        public MenuStrip mnu1;

        public usrDataGridView dgv;

        public ToolStripMenuItem miLoadData;
        public ToolStripMenuItem miExit;
        public ToolStripMenuItem miAbout;
        public ToolStripMenuItem miDocument;
        public ToolStripMenuItem miOptions;
        public ToolStripMenuItem miCopy1;
        public ToolStripMenuItem miPaste1;
        public ContextMenuStrip dgvMenu;
        public ToolStripMenuItem midgvCopy;
        public ToolStripMenuItem midgvPaste;
        public ToolStripMenuItem midgvCopyHdrs;
        public ToolStripMenuItem midgvUndo;
        public Label Table_Label;
        public ToolStripButton miSave;
        public ToolStripButton miPrint;
        public ToolStripButton miCopy;
        public ToolStripButton miPaste;

        public ToolStripButton miDecDecimal;
        public ToolStripButton miIncDecimal;
        public ToolStrip ToolStrip1;

        public TabControl TabMain;
        public TabPage TabPage1;

        public TabPage TabPage2;

        public ToolStripButton ToolStripButton2;
        public ToolStripButton ToolStripButton1;

        public ToolStripButton RegButton;
        public ToolStripMenuItem PreferencesToolStripMenuItem;

        public ToolStripMenuItem IncreateDecimalsToolStripMenuItem;
        public ToolStripMenuItem DecreaseDecimalsToolStripMenuItem;

        public ToolStripMenuItem CopyWHeadersToolStripMenuItem;

        public ToolStripMenuItem UseAbbrevHeadersToolStripMenuItem;

        public ToolStripMenuItem OptionsTool;
        public ToolStripMenuItem miSave1;
        public ToolStripMenuItem miPrint1;

        public ToolStripMenuItem ImpactsToolStripMenuItem;

        public ToolStripMenuItem miImpactInd1;
        public ToolStripMenuItem miImpactInd2;
        public ToolStripMenuItem miImpactComm1;
        public ToolStripMenuItem miImpactComm2;
        public ToolStripMenuItem miRegionalize;
        public ToolStripMenuItem miWorkspaceSummary;
        public ToolStripMenuItem EditToolStripMenuItem;

        public ToolStripMenuItem RawDataToolStripMenuItem;

        public ToolStripMenuItem miAggregate;
        public ToolStripMenuItem miLoadAgg;

        public ToolStripMenuItem miNewAgg;
        public ToolStripSeparator _ToolStripSeparator3;

        public ToolStripMenuItem ModifyDataToolStripMenuItem;

        public ToolStripMenuItem miStateData;
        public ToolStripMenuItem miCRateMenuItem;
        public ToolStripMenuItem miEmployment;
        public ToolStripMenuItem miFTERatio;
        public ToolStripMenuItem miGRPMenuItem;
        public ToolStripMenuItem ViewToolStripMenuItem;
        public ToolStripMenuItem UseEditMenuItem;
        public ToolStripMenuItem MakeToolStripMenuItem;
        public ToolStripButton TSB4;
        public ToolStripComboBox YearSelect_CBoxTS;
        public ToolStripMenuItem FinalDemandToolStripMenuItem;
        public ToolStripSeparator toolStripSeparator7;

        public ToolStripMenuItem StateToolStripMenuItem;
        public ToolStripMenuItem ViewIOAccountsToolStripMenuItem;
        public ToolStripMenuItem RequirementsTablesToolStripMenuItem;
        public ToolStripMenuItem MultipliersToolStripMenuItem;
        public ToolStripMenuItem miGRP;
        public ToolStripMenuItem SectorDistributionsToolStripMenuItem;
        public ToolStripMenuItem IndustryAccountsToolStripMenuItem;
        public ToolStripMenuItem CommodityAccountsToolStripMenuItem;
        public ToolStripMenuItem IndustryLaborToolStripMenuItem;
        public ToolStripMenuItem CommodityTradeDistributionsToolStripMenuItem;
        public ToolStripButton ToolStripButton4;
        public ToolStripMenuItem ProgramResetToolStripMenuItem;
        public ToolStripMenuItem DeactivateToolStripMenuItem;
        public ToolStripMenuItem ActivateToolStripMenuItem;
        public ComboBox YSCbox;

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(frmMain));
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			this.miLoadData = new ToolStripMenuItem();
			this.miSave1 = new ToolStripMenuItem();
			this.miPrint1 = new ToolStripMenuItem();
			this.PreferencesToolStripMenuItem = new ToolStripMenuItem();
			this.IncreateDecimalsToolStripMenuItem = new ToolStripMenuItem();
			this.DecreaseDecimalsToolStripMenuItem = new ToolStripMenuItem();
			this.CopyWHeadersToolStripMenuItem = new ToolStripMenuItem();
			this.UseAbbrevHeadersToolStripMenuItem = new ToolStripMenuItem();
			this.OptionsTool = new ToolStripMenuItem();
			this.ProgramResetToolStripMenuItem = new ToolStripMenuItem();
			this.miExit = new ToolStripMenuItem();
			this.ViewToolStripMenuItem = new ToolStripMenuItem();
			this.ViewIOAccountsToolStripMenuItem = new ToolStripMenuItem();
			this.RawDataToolStripMenuItem = new ToolStripMenuItem();
			this.miStateData = new ToolStripMenuItem();
			this.miCRateMenuItem = new ToolStripMenuItem();
			this.miEmployment = new ToolStripMenuItem();
			this.miFTERatio = new ToolStripMenuItem();
			this.miGRPMenuItem = new ToolStripMenuItem();
			this.miGRP = new ToolStripMenuItem();
			this.RequirementsTablesToolStripMenuItem = new ToolStripMenuItem();
			this.MultipliersToolStripMenuItem = new ToolStripMenuItem();
			this.SectorDistributionsToolStripMenuItem = new ToolStripMenuItem();
			this.IndustryAccountsToolStripMenuItem = new ToolStripMenuItem();
			this.CommodityAccountsToolStripMenuItem = new ToolStripMenuItem();
			this.IndustryLaborToolStripMenuItem = new ToolStripMenuItem();
			this.CommodityTradeDistributionsToolStripMenuItem = new ToolStripMenuItem();
			this.EditToolStripMenuItem = new ToolStripMenuItem();
			this.miAggregate = new ToolStripMenuItem();
			this.miNewAgg = new ToolStripMenuItem();
			this.miLoadAgg = new ToolStripMenuItem();
			this._ToolStripSeparator3 = new ToolStripSeparator();
			this.ModifyDataToolStripMenuItem = new ToolStripMenuItem();
			this.UseEditMenuItem = new ToolStripMenuItem();
			this.MakeToolStripMenuItem = new ToolStripMenuItem();
			this.FinalDemandToolStripMenuItem = new ToolStripMenuItem();
			this.StateToolStripMenuItem = new ToolStripMenuItem();
			this.miWorkspaceSummary = new ToolStripMenuItem();
			this.miAbout = new ToolStripMenuItem();
			this.miDocument = new ToolStripMenuItem();
			this.ActivateToolStripMenuItem = new ToolStripMenuItem();
			this.DeactivateToolStripMenuItem = new ToolStripMenuItem();
			this.miRegionalize = new ToolStripMenuItem();
			this.ImpactsToolStripMenuItem = new ToolStripMenuItem();
			this.miImpactInd1 = new ToolStripMenuItem();
			this.miImpactInd2 = new ToolStripMenuItem();
			this.miImpactComm1 = new ToolStripMenuItem();
			this.miImpactComm2 = new ToolStripMenuItem();
			this.miOptions = new ToolStripMenuItem();
			this.miCopy1 = new ToolStripMenuItem();
			this.miPaste1 = new ToolStripMenuItem();
			this.mnu1 = new MenuStrip();
			this.dgvMenu = new ContextMenuStrip(this.components);
			this.midgvCopy = new ToolStripMenuItem();
			this.midgvPaste = new ToolStripMenuItem();
			this.midgvUndo = new ToolStripMenuItem();
			this.midgvCopyHdrs = new ToolStripMenuItem();
			this.Table_Label = new Label();
			this.miSave = new ToolStripButton();
			this.miPrint = new ToolStripButton();
			this.miCopy = new ToolStripButton();
			this.miPaste = new ToolStripButton();
			this.miDecDecimal = new ToolStripButton();
			this.miIncDecimal = new ToolStripButton();
			this.ToolStrip1 = new ToolStrip();
			this.ToolStripButton2 = new ToolStripButton();
			this.ToolStripButton4 = new ToolStripButton();
			this.ToolStripButton1 = new ToolStripButton();
			this.RegButton = new ToolStripButton();
			this.TSB4 = new ToolStripButton();
			this.YearSelect_CBoxTS = new ToolStripComboBox();
			this.toolStripSeparator7 = new ToolStripSeparator();
			this.TabMain = new TabControl();
			this.TabPage1 = new TabPage();
			this.TabPage2 = new TabPage();
			this.dgv = new usrDataGridView();
			ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem();
			ToolStripMenuItem toolStripMenuItem2 = new ToolStripMenuItem();
			ToolStripMenuItem toolStripMenuItem3 = new ToolStripMenuItem();
			ToolStripMenuItem toolStripMenuItem4 = new ToolStripMenuItem();
			ToolStripMenuItem toolStripMenuItem5 = new ToolStripMenuItem();
			ToolStripMenuItem toolStripMenuItem6 = new ToolStripMenuItem();
			ToolStripSeparator toolStripSeparator = new ToolStripSeparator();
			ToolStripSeparator toolStripSeparator2 = new ToolStripSeparator();
			ToolStripSeparator toolStripSeparator3 = new ToolStripSeparator();
			ToolStripSeparator toolStripSeparator4 = new ToolStripSeparator();
			this.mnu1.SuspendLayout();
			this.dgvMenu.SuspendLayout();
			this.ToolStrip1.SuspendLayout();
			this.TabMain.SuspendLayout();
			this.TabPage1.SuspendLayout();
			((ISupportInitialize)this.dgv).BeginInit();
			base.SuspendLayout();
			toolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[6]
			{
				this.miLoadData,
				this.miSave1,
				this.miPrint1,
				this.PreferencesToolStripMenuItem,
				this.ProgramResetToolStripMenuItem,
				this.miExit
			});
			toolStripMenuItem.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			toolStripMenuItem.Name = "miFile";
			toolStripMenuItem.Size = new Size(44, 23);
			toolStripMenuItem.Text = "File";
			toolStripMenuItem.Click += this.miFile_Click;
			this.miLoadData.Name = "miLoadData";
			this.miLoadData.Size = new Size(173, 24);
			this.miLoadData.Text = "Load Data";
			//this.miSave1.Image = (Image)componentResourceManager.GetObject("miSave1.Image");
			this.miSave1.Name = "miSave1";
			this.miSave1.Size = new Size(173, 24);
			this.miSave1.Text = "Save Data";
			//this.miPrint1.Image = (Image)componentResourceManager.GetObject("miPrint1.Image");
			this.miPrint1.Name = "miPrint1";
			this.miPrint1.Size = new Size(173, 24);
			this.miPrint1.Text = "Print";
			this.PreferencesToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[3]
			{
				toolStripMenuItem2,
				toolStripMenuItem3,
				this.OptionsTool
			});
			this.PreferencesToolStripMenuItem.Name = "PreferencesToolStripMenuItem";
			this.PreferencesToolStripMenuItem.Size = new Size(173, 24);
			this.PreferencesToolStripMenuItem.Text = "Preferences";
			toolStripMenuItem2.DropDownItems.AddRange(new ToolStripItem[2]
			{
				this.IncreateDecimalsToolStripMenuItem,
				this.DecreaseDecimalsToolStripMenuItem
			});
			toolStripMenuItem2.Name = "ViewToolStripMenuItem1";
			toolStripMenuItem2.Size = new Size(129, 24);
			toolStripMenuItem2.Text = "Display";
			//this.IncreateDecimalsToolStripMenuItem.Image = (Image)componentResourceManager.GetObject("IncreateDecimalsToolStripMenuItem.Image");
			this.IncreateDecimalsToolStripMenuItem.Name = "IncreateDecimalsToolStripMenuItem";
			this.IncreateDecimalsToolStripMenuItem.Size = new Size(203, 24);
			this.IncreateDecimalsToolStripMenuItem.Text = "Increase Decimals";
			//this.DecreaseDecimalsToolStripMenuItem.Image = (Image)componentResourceManager.GetObject("DecreaseDecimalsToolStripMenuItem.Image");
			this.DecreaseDecimalsToolStripMenuItem.Name = "DecreaseDecimalsToolStripMenuItem";
			this.DecreaseDecimalsToolStripMenuItem.Size = new Size(203, 24);
			this.DecreaseDecimalsToolStripMenuItem.Text = "Decrease Decimals";
			toolStripMenuItem3.DropDownItems.AddRange(new ToolStripItem[2]
			{
				this.CopyWHeadersToolStripMenuItem,
				this.UseAbbrevHeadersToolStripMenuItem
			});
			//toolStripMenuItem3.Image = (Image)componentResourceManager.GetObject("OtherToolStripMenuItem.Image");
			toolStripMenuItem3.Name = "OtherToolStripMenuItem";
			toolStripMenuItem3.Size = new Size(129, 24);
			toolStripMenuItem3.Text = "Other";
			this.CopyWHeadersToolStripMenuItem.Name = "CopyWHeadersToolStripMenuItem";
			this.CopyWHeadersToolStripMenuItem.Size = new Size(215, 24);
			this.CopyWHeadersToolStripMenuItem.Text = "Not Copying Headers";
			this.UseAbbrevHeadersToolStripMenuItem.Name = "UseAbbrevHeadersToolStripMenuItem";
			this.UseAbbrevHeadersToolStripMenuItem.Size = new Size(215, 24);
			this.UseAbbrevHeadersToolStripMenuItem.Text = "Use Abbrev Headers";
			this.OptionsTool.Name = "OptionsTool";
			this.OptionsTool.Size = new Size(129, 24);
			this.OptionsTool.Text = "Options";
			this.ProgramResetToolStripMenuItem.Name = "ProgramResetToolStripMenuItem";
			this.ProgramResetToolStripMenuItem.Size = new Size(173, 24);
			this.ProgramResetToolStripMenuItem.Text = "Program Reset";
			this.miExit.Name = "miExit";
			this.miExit.Size = new Size(173, 24);
			this.miExit.Text = "Exit";
			toolStripMenuItem4.DropDownItems.AddRange(new ToolStripItem[3]
			{
				this.ViewToolStripMenuItem,
				this.EditToolStripMenuItem,
				this.miWorkspaceSummary
			});
			toolStripMenuItem4.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			toolStripMenuItem4.Name = "miViewData";
			toolStripMenuItem4.Size = new Size(52, 23);
			toolStripMenuItem4.Text = "Data";
			this.ViewToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[6]
			{
				this.ViewIOAccountsToolStripMenuItem,
				this.RawDataToolStripMenuItem,
				this.miGRP,
				this.RequirementsTablesToolStripMenuItem,
				this.MultipliersToolStripMenuItem,
				this.SectorDistributionsToolStripMenuItem
			});
			this.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem";
			this.ViewToolStripMenuItem.Size = new Size(212, 24);
			this.ViewToolStripMenuItem.Text = "View";
			this.ViewIOAccountsToolStripMenuItem.Name = "ViewIOAccountsToolStripMenuItem";
			this.ViewIOAccountsToolStripMenuItem.Size = new Size(214, 24);
			this.ViewIOAccountsToolStripMenuItem.Text = "IO Accounts";
			this.RawDataToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[5]
			{
				this.miStateData,
				this.miCRateMenuItem,
				this.miEmployment,
				this.miFTERatio,
				this.miGRPMenuItem
			});
			this.RawDataToolStripMenuItem.Name = "RawDataToolStripMenuItem";
			this.RawDataToolStripMenuItem.Size = new Size(214, 24);
			this.RawDataToolStripMenuItem.Text = "State Industry Data";
			this.miStateData.Name = "miStateData";
			this.miStateData.Size = new Size(308, 24);
			this.miStateData.Text = "By State";
			this.miCRateMenuItem.Name = "miCRateMenuItem";
			this.miCRateMenuItem.Size = new Size(308, 24);
			this.miCRateMenuItem.Text = "Compensation Rates (All States)";
			this.miEmployment.Name = "miEmployment";
			this.miEmployment.Size = new Size(308, 24);
			this.miEmployment.Text = "Employment (All States)";
			this.miFTERatio.Name = "miFTERatio";
			this.miFTERatio.Size = new Size(308, 24);
			this.miFTERatio.Text = "FTE-Job Ratios";
			this.miGRPMenuItem.Name = "miGRPMenuItem";
			this.miGRPMenuItem.Size = new Size(308, 24);
			this.miGRPMenuItem.Text = "Gross Industrial Product (All States)";
			this.miGRP.Name = "miGRP";
			this.miGRP.Size = new Size(214, 24);
			this.miGRP.Text = "Gross Product";
			this.RequirementsTablesToolStripMenuItem.Name = "RequirementsTablesToolStripMenuItem";
			this.RequirementsTablesToolStripMenuItem.Size = new Size(214, 24);
			this.RequirementsTablesToolStripMenuItem.Text = "Requirements Tables";
			this.MultipliersToolStripMenuItem.Name = "MultipliersToolStripMenuItem";
			this.MultipliersToolStripMenuItem.Size = new Size(214, 24);
			this.MultipliersToolStripMenuItem.Text = "Multipliers";
			this.SectorDistributionsToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[4]
			{
				this.IndustryAccountsToolStripMenuItem,
				this.CommodityAccountsToolStripMenuItem,
				this.IndustryLaborToolStripMenuItem,
				this.CommodityTradeDistributionsToolStripMenuItem
			});
			this.SectorDistributionsToolStripMenuItem.Name = "SectorDistributionsToolStripMenuItem";
			this.SectorDistributionsToolStripMenuItem.Size = new Size(214, 24);
			this.SectorDistributionsToolStripMenuItem.Text = "Sector Distributions";
			this.IndustryAccountsToolStripMenuItem.Name = "IndustryAccountsToolStripMenuItem";
			this.IndustryAccountsToolStripMenuItem.Size = new Size(278, 24);
			this.IndustryAccountsToolStripMenuItem.Text = "Industry Accounts";
			this.CommodityAccountsToolStripMenuItem.Name = "CommodityAccountsToolStripMenuItem";
			this.CommodityAccountsToolStripMenuItem.Size = new Size(278, 24);
			this.CommodityAccountsToolStripMenuItem.Text = "Commodity Accounts";
			this.IndustryLaborToolStripMenuItem.Name = "IndustryLaborToolStripMenuItem";
			this.IndustryLaborToolStripMenuItem.Size = new Size(278, 24);
			this.IndustryLaborToolStripMenuItem.Text = "Industry Labor";
			this.CommodityTradeDistributionsToolStripMenuItem.Name = "CommodityTradeDistributionsToolStripMenuItem";
			this.CommodityTradeDistributionsToolStripMenuItem.Size = new Size(278, 24);
			this.CommodityTradeDistributionsToolStripMenuItem.Text = "Commodity Trade Distributions";
			this.EditToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[2]
			{
				this.miAggregate,
				this.ModifyDataToolStripMenuItem
			});
			this.EditToolStripMenuItem.Name = "EditToolStripMenuItem";
			this.EditToolStripMenuItem.Size = new Size(212, 24);
			this.EditToolStripMenuItem.Text = "Edit";
			this.miAggregate.DropDownItems.AddRange(new ToolStripItem[3]
			{
				this.miNewAgg,
				this.miLoadAgg,
				this._ToolStripSeparator3
            });
			this.miAggregate.Name = "miAggregate";
			this.miAggregate.Size = new Size(158, 24);
			this.miAggregate.Text = "Aggregation";
			this.miNewAgg.Name = "miNewAgg";
			this.miNewAgg.Size = new Size(145, 24);
			this.miNewAgg.Text = "Aggregate";
			this.miLoadAgg.Enabled = false;
			this.miLoadAgg.Name = "miLoadAgg";
			this.miLoadAgg.Size = new Size(145, 24);
			this.miLoadAgg.Text = "Browse...";
			this._ToolStripSeparator3.Name = "ToolStripSeparator3";
			this._ToolStripSeparator3.Size = new Size(142, 6);
			this.ModifyDataToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[4]
			{
				this.UseEditMenuItem,
				this.MakeToolStripMenuItem,
				this.FinalDemandToolStripMenuItem,
				this.StateToolStripMenuItem
			});
			this.ModifyDataToolStripMenuItem.Name = "ModifyDataToolStripMenuItem";
			this.ModifyDataToolStripMenuItem.Size = new Size(158, 24);
			this.ModifyDataToolStripMenuItem.Text = "Modify Data";
			this.UseEditMenuItem.Name = "UseEditMenuItem";
			this.UseEditMenuItem.Size = new Size(167, 24);
			this.UseEditMenuItem.Text = "Use";
			this.MakeToolStripMenuItem.Name = "MakeToolStripMenuItem";
			this.MakeToolStripMenuItem.Size = new Size(167, 24);
			this.MakeToolStripMenuItem.Text = "Make";
			this.FinalDemandToolStripMenuItem.Name = "FinalDemandToolStripMenuItem";
			this.FinalDemandToolStripMenuItem.Size = new Size(167, 24);
			this.FinalDemandToolStripMenuItem.Text = "Final Demand";
			this.StateToolStripMenuItem.Name = "StateToolStripMenuItem";
			this.StateToolStripMenuItem.Size = new Size(167, 24);
			this.StateToolStripMenuItem.Text = "State";
			this.miWorkspaceSummary.Name = "miWorkspaceSummary";
			this.miWorkspaceSummary.Size = new Size(212, 24);
			this.miWorkspaceSummary.Text = "Workspace Summary";
			toolStripMenuItem5.DropDownItems.AddRange(new ToolStripItem[4]
			{
				this.miAbout,
				this.miDocument,
				this.ActivateToolStripMenuItem,
				this.DeactivateToolStripMenuItem
			});
			toolStripMenuItem5.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			toolStripMenuItem5.Name = "miHelp";
			toolStripMenuItem5.Size = new Size(51, 23);
			toolStripMenuItem5.Text = "Help";
			toolStripMenuItem5.Click += this.miHelp_Click;
			this.miAbout.Name = "miAbout";
			this.miAbout.Size = new Size(177, 24);
			this.miAbout.Text = "About";
			this.miDocument.Name = "miDocument";
			this.miDocument.Size = new Size(177, 24);
			this.miDocument.Text = "Documentation";
			this.ActivateToolStripMenuItem.Name = "ActivateToolStripMenuItem";
			this.ActivateToolStripMenuItem.Size = new Size(177, 24);
			this.ActivateToolStripMenuItem.Text = "Activate...";
			this.ActivateToolStripMenuItem.ToolTipText = "Activate an IO-Snap License";
			this.DeactivateToolStripMenuItem.Name = "DeactivateToolStripMenuItem";
			this.DeactivateToolStripMenuItem.Size = new Size(177, 24);
			this.DeactivateToolStripMenuItem.Text = "Deactivate...";
			this.DeactivateToolStripMenuItem.ToolTipText = "Deactivate current IO-Snap License";
			toolStripMenuItem6.DropDownItems.AddRange(new ToolStripItem[2]
			{
				this.miRegionalize,
				this.ImpactsToolStripMenuItem
			});
			toolStripMenuItem6.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			toolStripMenuItem6.Name = "miDescTools";
			toolStripMenuItem6.Size = new Size(75, 23);
			toolStripMenuItem6.Text = "Analysis";
			toolStripMenuItem6.Click += this.miDescTools_Click;
			this.miRegionalize.Name = "miRegionalize";
			this.miRegionalize.Size = new Size(153, 24);
			this.miRegionalize.Text = "Regionalize";
			this.ImpactsToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[4]
			{
				this.miImpactInd1,
				this.miImpactInd2,
				this.miImpactComm1,
				this.miImpactComm2
			});
			this.ImpactsToolStripMenuItem.Name = "ImpactsToolStripMenuItem";
			this.ImpactsToolStripMenuItem.Size = new Size(153, 24);
			this.ImpactsToolStripMenuItem.Text = "Impacts";
			this.miImpactInd1.Name = "miImpactInd1";
			this.miImpactInd1.Size = new Size(244, 24);
			this.miImpactInd1.Text = "Type 1 Industry-Driven";
			this.miImpactInd2.Name = "miImpactInd2";
			this.miImpactInd2.Size = new Size(244, 24);
			this.miImpactInd2.Text = "Type 2 Industry-Driven";
			this.miImpactComm1.Name = "miImpactComm1";
			this.miImpactComm1.Size = new Size(244, 24);
			this.miImpactComm1.Text = "Type 1 Commodity-Driven";
			this.miImpactComm2.Name = "miImpactComm2";
			this.miImpactComm2.Size = new Size(244, 24);
			this.miImpactComm2.Text = "Type 2 Commodity-Driven";
			toolStripSeparator.Name = "ToolStripSeparator5";
			toolStripSeparator.Size = new Size(185, 6);
			toolStripSeparator2.Name = "toolStripSeparator";
			toolStripSeparator2.Size = new Size(6, 23);
			toolStripSeparator3.Name = "toolStripSeparator1";
			toolStripSeparator3.Size = new Size(6, 23);
			toolStripSeparator4.Name = "ToolStripSeparator2";
			toolStripSeparator4.Size = new Size(6, 23);
			this.miOptions.DropDownItems.AddRange(new ToolStripItem[2]
			{
				this.miCopy1,
				this.miPaste1
			});
			this.miOptions.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.miOptions.Name = "miOptions";
			this.miOptions.Size = new Size(46, 23);
			this.miOptions.Text = "Edit";
			//this.miCopy1.Image = (Image)componentResourceManager.GetObject("miCopy1.Image");
			this.miCopy1.Name = "miCopy1";
			this.miCopy1.ShortcutKeyDisplayString = "CTRL + C";
			this.miCopy1.Size = new Size(177, 24);
			this.miCopy1.Text = "Copy";
			//this.miPaste1.Image = (Image)componentResourceManager.GetObject("miPaste1.Image");
			this.miPaste1.Name = "miPaste1";
			this.miPaste1.Size = new Size(177, 24);
			this.miPaste1.Text = "Paste";
			this.mnu1.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.mnu1.Items.AddRange(new ToolStripItem[5]
			{
				toolStripMenuItem,
				this.miOptions,
				toolStripMenuItem4,
				toolStripMenuItem6,
				toolStripMenuItem5
			});
			this.mnu1.Location = new Point(0, 0);
			this.mnu1.Name = "mnu1";
			this.mnu1.Padding = new Padding(6, 1, 0, 1);
			this.mnu1.Size = new Size(659, 25);
			this.mnu1.TabIndex = 0;
			this.mnu1.Text = "MenuStrip1";
			this.dgvMenu.Items.AddRange(new ToolStripItem[5]
			{
				this.midgvCopy,
				this.midgvPaste,
				this.midgvUndo,
				toolStripSeparator,
				this.midgvCopyHdrs
			});
			this.dgvMenu.Name = "dgvMenu";
			this.dgvMenu.Size = new Size(189, 98);
			this.midgvCopy.Name = "midgvCopy";
			this.midgvCopy.Size = new Size(188, 22);
			this.midgvCopy.Text = "Copy";
			this.midgvPaste.Name = "midgvPaste";
			this.midgvPaste.Size = new Size(188, 22);
			this.midgvPaste.Text = "Paste";
			this.midgvUndo.Enabled = false;
			this.midgvUndo.Name = "midgvUndo";
			this.midgvUndo.Size = new Size(188, 22);
			this.midgvUndo.Text = "Undo";
			this.midgvCopyHdrs.Name = "midgvCopyHdrs";
			this.midgvCopyHdrs.Size = new Size(188, 22);
			this.midgvCopyHdrs.Text = "Not Copying Headers";
			this.Table_Label.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.Table_Label.AutoSize = true;
			this.Table_Label.Font = new Font("Calibri", 11f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Table_Label.Location = new Point(8, 217);
			this.Table_Label.Margin = new Padding(2, 0, 2, 0);
			this.Table_Label.Name = "Table_Label";
			this.Table_Label.RightToLeft = RightToLeft.Yes;
			this.Table_Label.Size = new Size(46, 18);
			this.Table_Label.TabIndex = 3;
			this.Table_Label.Text = "Ready";
			this.Table_Label.TextAlign = ContentAlignment.MiddleCenter;
			this.miSave.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.miSave.Image = (Image)componentResourceManager.GetObject("miSave.Image");
			this.miSave.ImageTransparentColor = Color.Magenta;
			this.miSave.Name = "miSave";
			this.miSave.Size = new Size(28, 28);
			this.miSave.Text = "&Save";
			this.miSave.ToolTipText = "Save this Data";
			this.miPrint.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.miPrint.Image = (Image)componentResourceManager.GetObject("miPrint.Image");
			this.miPrint.ImageTransparentColor = Color.Magenta;
			this.miPrint.Name = "miPrint";
			this.miPrint.Size = new Size(28, 28);
			this.miPrint.Text = "&Print";
			this.miPrint.ToolTipText = "Print";
			this.miCopy.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.miCopy.Image = (Image)componentResourceManager.GetObject("miCopy.Image");
			this.miCopy.ImageTransparentColor = Color.Magenta;
			this.miCopy.Name = "miCopy";
			this.miCopy.Size = new Size(28, 28);
			this.miCopy.Text = "&Copy";
			this.miCopy.ToolTipText = "Copy selected Data";
			this.miPaste.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.miPaste.Image = (Image)componentResourceManager.GetObject("miPaste.Image");
			this.miPaste.ImageTransparentColor = Color.Magenta;
			this.miPaste.Name = "miPaste";
			this.miPaste.Size = new Size(28, 28);
			this.miPaste.Text = "&Paste";
			this.miDecDecimal.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.miDecDecimal.Image = (Image)componentResourceManager.GetObject("miDecDecimal.Image");
			this.miDecDecimal.ImageTransparentColor = Color.Magenta;
			this.miDecDecimal.Name = "miDecDecimal";
			this.miDecDecimal.Size = new Size(28, 28);
			this.miDecDecimal.Text = "ToolStripButton1";
			this.miDecDecimal.ToolTipText = "Decrease Decimals";
			this.miIncDecimal.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.miIncDecimal.Image = (Image)componentResourceManager.GetObject("miIncDecimal.Image");
			this.miIncDecimal.ImageTransparentColor = Color.Magenta;
			this.miIncDecimal.Name = "miIncDecimal";
			this.miIncDecimal.Size = new Size(28, 28);
			this.miIncDecimal.Text = "ToolStripButton2";
			this.miIncDecimal.ToolTipText = "Increase Decimals";
			this.ToolStrip1.Dock = DockStyle.None;
			this.ToolStrip1.GripMargin = new Padding(0);
			this.ToolStrip1.ImageScalingSize = new Size(24, 24);
			this.ToolStrip1.Items.AddRange(new ToolStripItem[16]
			{
				this.ToolStripButton2,
				this.miSave,
				this.miPrint,
				toolStripSeparator2,
				this.miCopy,
				this.miPaste,
				toolStripSeparator3,
				this.miDecDecimal,
				this.miIncDecimal,
				toolStripSeparator4,
				this.ToolStripButton4,
				this.ToolStripButton1,
				this.RegButton,
				this.TSB4,
				this.YearSelect_CBoxTS,
				this.toolStripSeparator7
			});
			this.ToolStrip1.LayoutStyle = ToolStripLayoutStyle.Flow;
			this.ToolStrip1.Location = new Point(0, 21);
			this.ToolStrip1.Margin = new Padding(0, 0, 0, 3);
			this.ToolStrip1.Name = "ToolStrip1";
			this.ToolStrip1.Size = new Size(755, 31);
			this.ToolStrip1.Stretch = true;
			this.ToolStrip1.TabIndex = 2;
			this.ToolStrip1.Text = "ToolStrip1";
			this.ToolStripButton2.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.ToolStripButton2.Image = (Image)componentResourceManager.GetObject("ToolStripButton2.Image");
			this.ToolStripButton2.ImageTransparentColor = Color.Magenta;
			this.ToolStripButton2.Name = "ToolStripButton2";
			this.ToolStripButton2.Size = new Size(28, 28);
			this.ToolStripButton2.Text = "ToolStripButton2";
			this.ToolStripButton2.ToolTipText = "Load Data";
			this.ToolStripButton4.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.ToolStripButton4.Image = (Image)componentResourceManager.GetObject("ToolStripButton4.Image");
			this.ToolStripButton4.ImageTransparentColor = Color.Magenta;
			this.ToolStripButton4.Name = "ToolStripButton4";
			this.ToolStripButton4.Size = new Size(28, 28);
			this.ToolStripButton4.Text = "ToolStripButton1";
			this.ToolStripButton4.ToolTipText = "Remove All Tabs";
			this.ToolStripButton1.DisplayStyle = ToolStripItemDisplayStyle.Image;
			//this.ToolStripButton1.Image = (Image)componentResourceManager.GetObject("ToolStripButton1.Image");
			this.ToolStripButton1.ImageTransparentColor = Color.Magenta;
			this.ToolStripButton1.Name = "ToolStripButton1";
			this.ToolStripButton1.Size = new Size(28, 28);
			this.ToolStripButton1.Text = "ToolStripButton1";
			this.ToolStripButton1.ToolTipText = "Remove Current Tab";
			this.RegButton.Font = new Font("Calibri", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			//this.RegButton.Image = (Image)componentResourceManager.GetObject("RegButton.Image");
			this.RegButton.ImageTransparentColor = Color.Magenta;
			this.RegButton.Name = "RegButton";
			this.RegButton.Size = new Size(101, 28);
			this.RegButton.Text = "Regionalize";
			this.RegButton.ToolTipText = "Start Regionalization";
			this.TSB4.Font = new Font("Calibri", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			//this.TSB4.Image = (Image)componentResourceManager.GetObject("TSB4.Image");
			this.TSB4.ImageTransparentColor = Color.Magenta;
			this.TSB4.Name = "TSB4";
			this.TSB4.Size = new Size(117, 28);
			this.TSB4.Text = "Table Headers";
			this.TSB4.ToolTipText = "Changes table headers copy status ";
			this.YearSelect_CBoxTS.AutoToolTip = true;
			this.YearSelect_CBoxTS.Font = new Font("Calibri", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.YearSelect_CBoxTS.Name = "YearSelect_CBoxTS";
			this.YearSelect_CBoxTS.Size = new Size(258, 25);
			this.YearSelect_CBoxTS.ToolTipText = "ghghghjghg";
			this.toolStripSeparator7.AutoSize = false;
			this.toolStripSeparator7.Name = "toolStripSeparator7";
			this.toolStripSeparator7.Size = new Size(6, 23);
			this.TabMain.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.TabMain.Controls.Add(this.TabPage1);
			this.TabMain.Controls.Add(this.TabPage2);
			this.TabMain.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.TabMain.HotTrack = true;
			this.TabMain.Location = new Point(0, 50);
			this.TabMain.Margin = new Padding(2, 5, 2, 2);
			this.TabMain.Name = "TabMain";
			this.TabMain.RightToLeft = RightToLeft.Yes;
			this.TabMain.SelectedIndex = 0;
			this.TabMain.Size = new Size(659, 169);
			this.TabMain.TabIndex = 9;
			this.TabPage1.Controls.Add(this.dgv);
			this.TabPage1.Location = new Point(4, 23);
			this.TabPage1.Margin = new Padding(2);
			this.TabPage1.Name = "TabPage1";
			this.TabPage1.Padding = new Padding(2);
			this.TabPage1.Size = new Size(651, 142);
			this.TabPage1.TabIndex = 0;
			this.TabPage1.Text = "Workspace Summary";
			this.TabPage1.UseVisualStyleBackColor = true;
			this.TabPage2.Location = new Point(4, 23);
			this.TabPage2.Margin = new Padding(2);
			this.TabPage2.Name = "TabPage2";
			this.TabPage2.Padding = new Padding(2);
			this.TabPage2.Size = new Size(651, 142);
			this.TabPage2.TabIndex = 1;
			this.TabPage2.Text = "TabPage2";
			this.TabPage2.UseVisualStyleBackColor = true;
			this.dgv.AllowUserToAddRows = false;
			this.dgv.AllowUserToDeleteRows = false;
			dataGridViewCellStyle.BackColor = Color.FromArgb(224, 224, 224);
			this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle;
			this.dgv.BackgroundColor = SystemColors.Desktop;
			this.dgv.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.BackColor = SystemColors.Control;
			dataGridViewCellStyle2.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle2.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.True;
			this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
			this.dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgv.ContextMenuStrip = this.dgvMenu;
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle3.BackColor = SystemColors.Window;
			dataGridViewCellStyle3.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle3.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle3.Format = "#,##0.0######";
			dataGridViewCellStyle3.NullValue = null;
			dataGridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = DataGridViewTriState.False;
			this.dgv.DefaultCellStyle = dataGridViewCellStyle3;
			this.dgv.Dock = DockStyle.Fill;
			this.dgv.EditMode = DataGridViewEditMode.EditOnKeystroke;
			this.dgv.Location = new Point(2, 2);
			this.dgv.Name = "dgv";
			this.dgv.RightToLeft = RightToLeft.No;
			this.dgv.RowHeadersWidth = 175;
			this.dgv.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgv.RowTemplate.Height = 24;
			this.dgv.SelectionMode = DataGridViewSelectionMode.CellSelect;
			this.dgv.Size = new Size(647, 138);
			this.dgv.TabIndex = 1;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = AutoScaleMode.Dpi;
			base.AutoSizeMode = AutoSizeMode.GrowAndShrink;
			base.ClientSize = new Size(659, 235);
			base.Controls.Add(this.ToolStrip1);
			base.Controls.Add(this.mnu1);
			base.Controls.Add(this.TabMain);
			base.Controls.Add(this.Table_Label);
			this.Font = new Font("Calibri", 10f, FontStyle.Regular, GraphicsUnit.Point, 0);
			////base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MainMenuStrip = this.mnu1;
			this.MinimumSize = new Size(439, 263);
			base.Name = "frmMain";
			this.RightToLeftLayout = true;
			base.SizeGripStyle = SizeGripStyle.Show;
			this.Text = "IO-Snap";
			base.WindowState = FormWindowState.Maximized;
			this.mnu1.ResumeLayout(false);
			this.mnu1.PerformLayout();
			this.dgvMenu.ResumeLayout(false);
			this.ToolStrip1.ResumeLayout(false);
			this.ToolStrip1.PerformLayout();
			this.TabMain.ResumeLayout(false);
			this.TabPage1.ResumeLayout(false);
			((ISupportInitialize)this.dgv).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public frmMain()
		{
			base.Load += this.frmMain_Load;
			this.MePersonalIncomeM = new double[101];
			this.MeDisposableIncomeM = new double[101];
			this.DirectTabList = new ArrayList();
			this.options = new ViewOptions();
			this.copyHeadersStatus = false;
			this.MainTabText = "Workspace Summary";
			this.GovernmentAggStatus = new GenDataStatus();
			this.myToolTip = new ToolTip();
			this.AL_cboxChecked = false;
			this.AK_cboxChecked = false;
			this.AR_cboxChecked = false;
			this.AZ_cboxChecked = false;
			this.CA_cboxChecked = false;
			this.CO_cboxChecked = false;
			this.CT_cboxChecked = false;
			this.DC_cboxChecked = false;
			this.DE_cboxChecked = false;
			this.FL_cboxChecked = false;
			this.GA_cboxChecked = false;
			this.IA_cboxChecked = false;
			this.ID_cboxChecked = false;
			this.IL_cboxChecked = false;
			this.IN_cboxChecked = false;
			this.KS_cboxChecked = false;
			this.KY_cboxChecked = false;
			this.LA_cboxChecked = false;
			this.MA_cboxChecked = false;
			this.ME_cboxChecked = false;
			this.MI_cboxChecked = false;
			this.MN_cboxChecked = false;
			this.MD_cboxChecked = false;
			this.MO_cboxChecked = false;
			this.MS_cboxChecked = false;
			this.MT_cboxChecked = false;
			this.NE_cboxChecked = false;
			this.ND_cboxChecked = false;
			this.NC_cboxChecked = false;
			this.NH_cboxChecked = false;
			this.NJ_cboxChecked = false;
			this.NM_cboxChecked = false;
			this.NY_cboxChecked = false;
			this.NV_cboxChecked = false;
			this.HI_cboxChecked = false;
			this.OH_cboxChecked = false;
			this.OK_cboxChecked = false;
			this.OR_cboxChecked = false;
			this.PA_cboxChecked = false;
			this.RI_cboxChecked = false;
			this.SC_cboxChecked = false;
			this.SD_cboxChecked = false;
			this.TN_cboxChecked = false;
			this.TX_cboxChecked = false;
			this.UT_cboxChecked = false;
			this.VA_cboxChecked = false;
			this.VT_cboxChecked = false;
			this.WA_cboxChecked = false;
			this.WI_cboxChecked = false;
			this.WV_cboxChecked = false;
			this.WY_cboxChecked = false;
			this.Disclaimer = "\r\n==================================================\r\n\r\n\t\t\tIO-SNAP WARRANTY\r\n\r\n==================================================\r\n\r\nTHIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS, DEVELOPERS,\r\nAND DISTRIBUTORS 'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES,\r\nINCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\r\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\r\nDISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS, DEVELOPERS,\r\nOR DISTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,\r\nSPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT\r\nNOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;\r\nLOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) \r\nHOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN\r\nCONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR\r\nOTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,\r\nEVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.";
			this.InitializeComponent();
			programLoadingProgressBar programLoadingProgressBar = new programLoadingProgressBar();
			LicenseDat licenseDat = new LicenseDat();
			if (licenseDat.Isvalid)
			{
				MyProject.Forms.frmNotify.Show("Your " + licenseDat.LicenseType + " License Will Expire on " + Conversions.ToString(licenseDat.ExpirationDate), null);
				string licenseType = licenseDat.LicenseType;
				if (Operators.CompareString(licenseType, "Student", false) != 0)
				{
					if (Operators.CompareString(licenseType, "Demo", false) != 0)
					{
						if (Operators.CompareString(licenseType, "Full", false) == 0)
						{
							this.Text = "IO-Snap";
						}
					}
					else
					{
						this.Text = "IO-Snap (Demo Mode)";
					}
				}
				else
				{
					this.Text = "IO-Snap (Academic Use)";
				}
			}
			else
			{
				RegistrationForm registrationForm = new RegistrationForm();
				if (registrationForm.ShowDialog() == DialogResult.OK)
				{
					if (registrationForm.isValid)
					{
						licenseDat = registrationForm.License;
						MyProject.Forms.frmNotify.Show(licenseDat.LicenseType + " Version of IO-Snap Registered\r\nYour License Will Expire on " + Conversions.ToString(licenseDat.ExpirationDate), null);
						string licenseType2 = licenseDat.LicenseType;
						if (Operators.CompareString(licenseType2, "Student", false) != 0)
						{
							if (Operators.CompareString(licenseType2, "Demo", false) != 0)
							{
								if (Operators.CompareString(licenseType2, "Full", false) == 0)
								{
									this.Text = "IO-Snap";
								}
							}
							else
							{
								this.Text = "IO-Snap (Demo Mode)";
							}
						}
						else
						{
							this.Text = "IO-Snap (Academic Use)";
						}
					}
					else
					{
						Interaction.MsgBox("Demo Mode : Expires on " + Conversions.ToString(licenseDat.ExpirationDate), MsgBoxStyle.OkOnly, null);
						this.Text = "IO-Snap (Demo Mode)";
					}
				}
				else
				{
					string text = "";
					text = FileSystem.CurDir() + "\\License.dat";
					if (File.Exists(text))
					{
						Interaction.MsgBox("Demo Mode : Expires on " + Conversions.ToString(licenseDat.ExpirationDate), MsgBoxStyle.OkOnly, null);
						this.Text = "IO-Snap (Demo Mode)";
					}
					else
					{
						Environment.Exit(0);
					}
				}
			}
			string currentDirectory = Directory.GetCurrentDirectory();
			if (DateTime.Compare(DateTime.Today.Date, licenseDat.ExpirationDate) > 0)
			{
				Interaction.MsgBox("You have reached the demo usage limit.\r\nPlease acquire full version If you wish To Continue.\r\nThe program will now quit.", MsgBoxStyle.OkOnly, null);
				Environment.Exit(0);
			}
			this.GovernmentAggStatus.Initiate();
			programLoadingProgressBar.Show();
			programLoadingProgressBar.setMax(30);
			programLoadingProgressBar.setValue(0);
			this.BEADat = new BEAData();
			checked
			{
				programLoadingProgressBar.setValue(programLoadingProgressBar.getValue() + 1);
				this.BEADat.worksum = new SummaryDat();
				this.BEADat.worksum.InitSetup();
				programLoadingProgressBar.setValue(programLoadingProgressBar.getValue() + 1);
				this.UndoDat = new UndoData(this.midgvUndo, this.dgv);
				programLoadingProgressBar.setValue(programLoadingProgressBar.getValue() + 1);
				programLoadingProgressBar.setValue(programLoadingProgressBar.getValue() + 1);
				this.DataCollection = new DataWrapREADER();
				programLoadingProgressBar.setValue(programLoadingProgressBar.getValue() + 1);
				this.DataCollection.loadData(ref programLoadingProgressBar, ref licenseDat.LicenseType);
				programLoadingProgressBar.setValue(programLoadingProgressBar.getValue() + 1);
				this.RefDataCollection = new DataWrapREADER();
			}
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = this.DataCollection.bealist.GetEnumerator();
				while (enumerator.MoveNext())
				{
					BEAData bEAData = (BEAData)enumerator.Current;
					BEAData bEAData2 = (BEAData)bEAData.Clone();
					this.RefDataCollection.addData(ref bEAData2);
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			checked
			{
				programLoadingProgressBar.setValue(programLoadingProgressBar.getValue() + 1);
				this.YearSelect_CBoxTS.Items.Clear();
				int num = this.DataCollection.getNrec() - 1;
				for (int i = 0; i <= num; i++)
				{
					this.YearSelect_CBoxTS.Items.Add(RuntimeHelpers.GetObjectValue(this.DataCollection.getNames()[i]));
					this.DataCollection.getData(i).worksum.CurrentLabel = this.DataCollection.getData(i).worksum.DataLabel;
				}
				programLoadingProgressBar.setValue(programLoadingProgressBar.getValue() + 1);
				this.YearSelect_CBoxTS.DropDownStyle = ComboBoxStyle.DropDownList;
				programLoadingProgressBar.Dispose();
				BEAData data = this.DataCollection.getData(0);
				if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareString(licenseDat.LicenseType, "Demo", false) == 0, Operators.CompareObjectEqual(data.worksum.DataType, "FULL", false))))
				{
					Interaction.MsgBox("Accessing un-authorized version Of data!\r\nThe program will quit.", MsgBoxStyle.OkOnly, null);
					Environment.Exit(0);
				}
				this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.TotalGovernment);
				if (Operators.CompareString(licenseDat.LicenseType, "Demo", false) == 0)
				{
					this.DeactivateToolStripMenuItem.Enabled = false;
				}
				else if (Operators.CompareString(licenseDat.LicenseType, "Full", false) == 0)
				{
					this.ActivateToolStripMenuItem.Enabled = false;
				}
				else
				{
					this.ActivateToolStripMenuItem.Enabled = false;
				}
			}
		}

		private void Aggregate_Click(object sender, EventArgs e)
		{
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			this.BEADat.ChangeGovAggregationLevel();
			BEAData bEAData = new BEAData();
			bEAData.worksum = new SummaryDat();
			bEAData.worksum.InitSetup();
			bEAData = (BEAData)this.BEADat.Clone();
			frmAgg frmAgg;
			if (sender.GetType() == MyProject.Forms.frmAgg.GetType())
			{
				frmAgg = (frmAgg)sender;
			}
			else
			{
				frmAgg = new frmAgg();
				frmAgg.BEADat = bEAData;
			}
			checked
			{
				if (frmAgg.ShowDialog() == DialogResult.OK)
				{
					DateTime now;
					if (this.BEADat.worksum.DataTypeCode == SummaryDat.bcode.BASE_DATA)
					{
						if (!this.BEADat.isEdited)
						{
							string text = " ";
							bool flag = true;
							int num = 1;
							string text2 = "";
							while (flag)
							{
								flag = true;
								while (flag)
								{
									text = bEAData.worksum.datayear + "_v_" + Conversions.ToString(num);
									object value = false;
									int num2 = this.YearSelect_CBoxTS.Items.Count - 1;
									for (int i = 0; i <= num2; i++)
									{
										if (string.Compare(this.YearSelect_CBoxTS.Items[i].ToString(), text) == 0)
										{
											value = true;
										}
									}
									if (Conversions.ToBoolean(value))
									{
										num++;
									}
									else
									{
										flag = false;
									}
								}
								text2 = "";
								while (text2.Length == 0)
								{
									text2 = Interaction.InputBox("Please enter New dataset name", "IO-Snap Dataset Name", text, -1, -1);
								}
								object value2 = false;
								int num3 = this.YearSelect_CBoxTS.Items.Count - 1;
								for (int j = 0; j <= num3; j++)
								{
									if (string.Compare(this.YearSelect_CBoxTS.Items[j].ToString(), text2) == 0)
									{
										value2 = true;
									}
								}
								if (Conversions.ToBoolean(value2))
								{
									Interaction.MsgBox("IO-Snap dataset " + text2 + " already exists. Please enter a different name.", MsgBoxStyle.OkOnly, null);
									flag = true;
								}
								else
								{
									flag = false;
								}
							}
							bEAData.worksum.DataLabel = text2;
							bEAData.worksum.CurrentLabel = text2;
							if (bEAData.worksum.DataTypeCode != SummaryDat.bcode.REGION_DATA)
							{
								bEAData.worksum.DataTypeCode = SummaryDat.bcode.USER_DATA;
							}
							SummaryDat worksum = bEAData.worksum;
							now = DateAndTime.Now;
							worksum.DataTimeStamp = now.ToString();
							bEAData.worksum.requirementsTables = "No";
							bEAData.eDataState = BEAData.DataState.Loaded;
							bEAData.worksum.DataType = RuntimeHelpers.GetObjectValue(this.BEADat.worksum.DataType);
							this.DataCollection.addData(ref bEAData);
							this.YearSelect_CBoxTS.Items.Add(bEAData.worksum.DataLabel);
							this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
						}
						else
						{
							this.BEADat = bEAData;
						}
						this.BEADat.isEdited = true;
						this.TabClean();
						this.MenuHandle.SetMenuStates();
						this.ShowSummary();
						MyProject.Forms.frmNotify.Show("Data Aggregated \r\nNew Dataset " + this.BEADat.worksum.DataLabel + " created", null);
					}
					else
					{
						bEAData.worksum.requirementsTables = "No";
						SummaryDat worksum2 = bEAData.worksum;
						now = DateAndTime.Now;
						worksum2.DataTimeStamp = now.ToString();
						bEAData.worksum.dataaggregated = "Yes";
						this.BEADat = frmAgg.BEADat;
						this.BEADat.isEdited = true;
						this.TabClean();
						this.MenuHandle.SetMenuStates();
						this.ShowSummary();
						MyProject.Forms.frmNotify.Show("Data Aggregated", null);
					}
				}
				this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
				this.BEADat.ChangeGovAggregationLevel();
				this.ShowSummary();
			}
		}

		private void TabRemove_Click(object sender, EventArgs e)
		{
			checked
			{
				if (string.Compare(this.TabMain.SelectedTab.Text, this.MainTabText) != 0)
				{
					int num = this.TabMain.SelectedIndex;
					if (num == this.TabMain.TabCount - 1)
					{
						num--;
					}
					this.TabMain.Controls.Remove(this.TabMain.SelectedTab);
					this.TabMain.SelectTab(num);
				}
			}
		}

		private void T_Click(object sender, EventArgs e)
		{
			this.T_Direct();
		}

		private void T_Direct()
		{
			if (!this.TabMain.SelectedTab.Text.StartsWith("Direct"))
			{
				return;
			}
		}

		private void TabClean()
		{
			this.TabMain.SelectTab(0);
			checked
			{
				if (this.TabMain.TabCount > 1)
				{
					int num = this.TabMain.TabCount - 1;
					for (int i = num; i >= 1; i += -1)
					{
						this.TabMain.Controls.Remove(this.TabMain.Controls[i]);
					}
				}
			}
		}

		private void miLoadAgg_Click(object sender, EventArgs e)
		{
		}

		private void ViewData(object sender, EventArgs e)
		{
			this.ViewDataMain(RuntimeHelpers.GetObjectValue(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null)));
		}

		private void ViewDataMain(object stref)
		{
			this.DispDat = new DispData();
			string frmTitle = "";
			string selectedText = this.YearSelect_CBoxTS.SelectedText;
			byte dispTotals = 0;
			checked
			{
				if (Operators.ConditionalCompareObjectEqual(stref, "miGRPMenuItem", false))
				{
					int num = this.BEADat.StateIDs.StateCodes.Length;
					Matrix matrix = new Matrix(this.BEADat.NTables.getIndustryCount() + 1, num + 1);
					Matrix matrix2 = new Matrix(this.BEADat.NTables.getIndustryCount() + 1, num + 1);
					string[] array = new string[num + 1];
					string[] array2 = new string[this.BEADat.NTables.getIndustryCount() + 1 + 1];
					array[0] = "National";
					int num2 = this.BEADat.NTables.getIndustryCount() - 1;
					for (int i = 0; i <= num2; i++)
					{
						unchecked
						{
							matrix[i, 0] = this.BEADat.get_GOSDouble(i) + this.BEADat.get_ECDouble(i) + this.BEADat.get_TDouble(i);
							Matrix matrix3;
							int industryCount;
							(matrix3 = matrix)[industryCount = this.BEADat.NTables.getIndustryCount(), 0] = matrix3[industryCount, 0] + matrix[i, 0];
							matrix2[i, 0] = 0.0;
						}
					}
					int num3 = num;
					for (int j = 1; j <= num3; j++)
					{
						string text = array[j] = this.BEADat.StateIDs.StateCodes[j - 1];
						double num4 = 0.0;
						int num5 = this.BEADat.NTables.getIndustryCount() - 1;
						for (int k = 0; k <= num5; k++)
						{
							matrix[k, j] = this.BEADat.RegStateData.GetState(ref text).getGDP()[k];
							Matrix matrix3;
							int industryCount;
							int col;
							(matrix3 = matrix)[industryCount = this.BEADat.NTables.getIndustryCount(), col = j] = unchecked(matrix3[industryCount, col] + matrix[k, j]);
							matrix2[k, j] = this.BEADat.RegStateData.GetState(ref text).getGDP_Flag()[k];
						}
					}
					int num6 = this.BEADat.NTables.getIndustryCount() - 1;
					for (int l = 0; l <= num6; l++)
					{
						array2[l] = this.BEADat.NTables.hdrInd[l];
					}
					array2[this.BEADat.NTables.getIndustryCount()] = "Gross Regional Product v2";
					this.DispDat.hasEmployment = true;
					this.DispDat.BaseTable = matrix;
					this.DispDat.ColorTable = matrix2;
					this.DispDat.showCellColor = true;
					this.DispDat.hdrsCol = array;
					this.DispDat.hdrsRow = array2;
					dispTotals = 1;
					frmTitle = "Gross Industrial Product";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miCRateMenuItem", false))
				{
					int num7 = this.BEADat.StateIDs.StateCodes.Length;
					Matrix matrix4 = new Matrix(this.BEADat.NTables.getIndustryCount(), num7 + 1);
					Matrix matrix5 = new Matrix(this.BEADat.NTables.getIndustryCount(), num7 + 1);
					string[] array3 = new string[num7 + 1];
					array3[0] = "National";
					Vector eC = this.BEADat.EC;
					int num8 = this.BEADat.NTables.getIndustryCount() - 1;
					for (int m = 0; m <= num8; m++)
					{
						double num9 = 0.0;
						int num10 = num7;
						for (int n = 1; n <= num10; n++)
						{
							string text2 = this.BEADat.StateIDs.StateCodes[n - 1];
							num9 = unchecked(num9 + this.BEADat.RegStateData.GetState(ref text2).getEC()[m]);
						}
						eC[m] = num9;
					}
					int num11 = this.BEADat.NTables.getIndustryCount() - 1;
					for (int num12 = 0; num12 <= num11; num12++)
					{
						unchecked
						{
							if (string.Compare(this.BEADat.worksum.dataregionalized, "NO") == -1)
							{
								matrix4[num12, 0] = 1000000.0 * this.BEADat.get_ECDouble(num12) / this.BEADat.NTables.Employment[num12];
							}
							else
							{
								matrix4[num12, 0] = 1000000.0 * eC[num12] / this.BEADat.NTables.Employment[num12];
							}
							matrix5[num12, 0] = 0.0;
							if (matrix4[num12, 0] != 0.0)
							{
								continue;
							}
						}
					}
					int num13 = num7;
					for (int num14 = 1; num14 <= num13; num14++)
					{
						string text3 = array3[num14] = this.BEADat.StateIDs.StateCodes[num14 - 1];
						int nrec_base = this.BEADat.RegStateData.GetState(ref text3).nrec_base;
						int num15 = this.BEADat.NTables.getIndustryCount() - 1;
						for (int num16 = 0; num16 <= num15; num16++)
						{
							matrix4[num16, num14] = this.BEADat.RegStateData.GetState(ref text3).getCompensationRate()[num16];
							matrix5[num16, num14] = this.BEADat.RegStateData.GetState(ref text3).getCompensationRate_Flag()[num16];
						}
					}
					this.DispDat.hasEmployment = true;
					this.DispDat.BaseTable = matrix4;
					this.DispDat.ColorTable = matrix5;
					this.DispDat.showCellColor = true;
					this.DispDat.hdrsCol = array3;
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
					dispTotals = 1;
					frmTitle = "Compensation Rates";
					this.DispDat.useGeneralFormatOnly = false;
					int[] array4 = new int[this.DispDat.hdrsCol.Length + 1];
					int num17 = array4.Length - 1;
					for (int num18 = 0; num18 <= num17; num18++)
					{
						array4[num18] = 2;
					}
					this.DispDat.DataFormatTable = array4;
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miEmployment", false))
				{
					int num19 = this.BEADat.StateIDs.StateCodes.Length;
					Matrix matrix6 = new Matrix(this.BEADat.NTables.getIndustryCount() + 1, num19 + 1);
					Matrix matrix7 = new Matrix(this.BEADat.NTables.getIndustryCount() + 1, num19 + 1);
					string[] array5 = new string[num19 + 1];
					array5[0] = "National";
					int num20 = this.BEADat.NTables.getIndustryCount() - 1;
					for (int num21 = 0; num21 <= num20; num21++)
					{
						matrix6[num21, 0] = this.BEADat.NTables.Employment[num21];
						matrix7[num21, 0] = 0.0;
						Matrix matrix3;
						int col;
						(matrix3 = matrix6)[col = this.BEADat.NTables.getIndustryCount(), 0] = unchecked(matrix3[col, 0] + this.BEADat.NTables.Employment[num21]);
					}
					int num22 = num19;
					for (int num23 = 1; num23 <= num22; num23++)
					{
						string text4 = array5[num23] = this.BEADat.StateIDs.StateCodes[num23 - 1];
						int nrec_base2 = this.BEADat.RegStateData.GetState(ref text4).nrec_base;
						int num24 = this.BEADat.NTables.getIndustryCount() - 1;
						for (int num25 = 0; num25 <= num24; num25++)
						{
							matrix6[num25, num23] = this.BEADat.RegStateData.GetState(ref text4).getEmployment()[num25];
							matrix7[num25, num23] = this.BEADat.RegStateData.GetState(ref text4).getEmployment_Flag()[num25];
							unchecked
							{
								Matrix matrix3;
								int industryCount;
								int col;
								(matrix3 = matrix6)[col = this.BEADat.NTables.getIndustryCount(), industryCount = num23] = matrix3[col, industryCount] + this.BEADat.RegStateData.GetState(ref text4).getEmployment()[num25];
								(matrix3 = matrix7)[industryCount = this.BEADat.NTables.getIndustryCount(), col = num23] = matrix3[industryCount, col] + this.BEADat.RegStateData.GetState(ref text4).getEmployment_Flag()[num25];
							}
						}
					}
					this.DispDat.hasEmployment = true;
					this.DispDat.BaseTable = matrix6;
					this.DispDat.ColorTable = matrix7;
					this.DispDat.showCellColor = true;
					this.DispDat.hdrsCol = array5;
					string[] array6 = new string[this.BEADat.NTables.getIndustryCount() + 1 + 1];
					int num26 = this.BEADat.NTables.getIndustryCount() - 1;
					for (int num27 = 0; num27 <= num26; num27++)
					{
						array6[num27] = this.BEADat.NTables.hdrInd[num27];
					}
					array6[this.BEADat.NTables.getIndustryCount()] = "Total Employment";
					this.DispDat.hdrsRow = array6;
					dispTotals = 1;
					if (this.BEADat.GovernmentAggStatus.getFTE_Type() == GenDataStatus.dataEmpStatus.FTE_BASED)
					{
						frmTitle = "Employment (FTE)";
					}
					if (this.BEADat.GovernmentAggStatus.getFTE_Type() == GenDataStatus.dataEmpStatus.JOB_BASED)
					{
						frmTitle = "Employment (Jobs)";
					}
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miUse", false))
				{
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrComm;
					this.DispDat.BaseTable = this.BEADat.Use;
					this.DispDat.ExtraCols = this.BEADat.FinDem;
					this.DispDat.hdrsXCols = this.BEADat.hdrFinDem;
					this.DispDat.ExtraRows = this.BEADat.NTables.VA;
					this.DispDat.hdrsXRows = this.BEADat.hdrVA;
					frmTitle = "National Use";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "Use", false))
				{
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
					this.DispDat.hdrsRow = this.BEADat.hdrComm;
					this.DispDat.BaseTable = this.BEADat.Use;
					this.DispDat.ExtraCols = this.BEADat.FinDem;
					this.DispDat.hdrsXCols = this.BEADat.hdrFinDem;
					this.DispDat.ExtraRows = this.BEADat.NTables.VA;
					this.DispDat.hdrsXRows = this.BEADat.hdrVA;
					frmTitle = "Use";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miRegUse", false))
				{
					this.DispDat.hasEmployment = true;
					this.DispDat.isAfterRegionalization = true;
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
					this.DispDat.hdrsRow = this.BEADat.hdrComm;
					this.DispDat.BaseTable = this.BEADat.RegUse;
					this.DispDat.ExtraCols = this.BEADat.RegFinDem;
					this.DispDat.hdrsXCols = this.BEADat.hdrFinDem;
					this.DispDat.ExtraRows = this.BEADat.RegVA;
					this.DispDat.hdrsXRows = this.BEADat.hdrVA;
					frmTitle = this.BEADat.worksum.regby + ", Regional Use";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miMake", false))
				{
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrComm;
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
					this.DispDat.BaseTable = this.BEADat.NTables.Make();
					int num28 = this.BEADat.NTables.Make().NoCols - 1;
					this.DispDat.ExtraRows = this.BEADat.Import;
					this.DispDat.hdrsXRows = new string[1];
					this.DispDat.hdrsXRows[0] = "Imports";
					frmTitle = "National Make";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "Make", false))
				{
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrComm;
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
					this.DispDat.BaseTable = this.BEADat.NTables.Make();
					int num29 = this.BEADat.NTables.Make().NoCols - 1;
					int num30 = this.BEADat.NTables.getIndustryCount() - 1;
					for (int num31 = 0; num31 <= num30; num31++)
					{
						this.DispDat.BaseTable[num31, num29 - 1] = this.BEADat.Scrap[0, num31];
						this.DispDat.BaseTable[num31, num29] = this.BEADat.NCI[0, num31];
					}
					this.DispDat.ExtraRows = this.BEADat.Import;
					this.DispDat.hdrsXRows = new string[1];
					this.DispDat.hdrsXRows[0] = "Imports";
					frmTitle = "Make";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miRegMake", false))
				{
					this.DispDat.hasEmployment = true;
					this.DispDat.isAfterRegionalization = true;
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrComm;
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
					this.DispDat.BaseTable = this.BEADat.RegMake;
					this.DispDat.ExtraRows = this.BEADat.RegImports;
					this.DispDat.hdrsXRows = new string[1];
					this.DispDat.hdrsXRows[0] = "Imports";
					frmTitle = this.BEADat.worksum.regby + ", Regional Make";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miVA", false))
				{
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
					this.DispDat.hdrsRow = this.BEADat.hdrVA;
					this.DispDat.BaseTable = this.BEADat.NTables.VA;
					frmTitle = "National Value Added";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "VA", false))
				{
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
					this.DispDat.hdrsRow = this.BEADat.hdrVA;
					this.DispDat.BaseTable = this.BEADat.NTables.VA;
					frmTitle = "Value Added";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miRegVA", false))
				{
					this.DispDat.hasEmployment = true;
					this.DispDat.isAfterRegionalization = true;
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
					this.DispDat.hdrsRow = this.BEADat.hdrVA;
					this.DispDat.BaseTable = this.BEADat.RegVA;
					frmTitle = this.BEADat.worksum.regby + ", Regional Value Added";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miFinDem", false))
				{
					this.DispDat.hdrsCol = this.BEADat.hdrFinDem;
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrComm;
					this.DispDat.BaseTable = this.BEADat.FinDem;
					frmTitle = "National Final Demand";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "FinDem", false))
				{
					this.DispDat.hdrsCol = this.BEADat.hdrFinDem;
					this.DispDat.hdrsRow = this.BEADat.hdrComm;
					this.DispDat.BaseTable = this.BEADat.FinDem;
					frmTitle = "Final Demand";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miRegFinDem", false))
				{
					this.DispDat.hasEmployment = true;
					this.DispDat.isAfterRegionalization = true;
					this.DispDat.hdrsCol = this.BEADat.hdrFinDem;
					this.DispDat.hdrsRow = this.BEADat.hdrComm;
					this.DispDat.BaseTable = this.BEADat.RegFinDem;
					frmTitle = this.BEADat.worksum.regby + ", Regional Final Demand";
				}
				else if (Operators.ConditionalCompareObjectEqual(stref, "miFTERatio", false))
				{
					Matrix matrix8 = new Matrix(this.BEADat.NTables.FTE_Ratio.getFTE_RATIO().Count, 1);
					Matrix matrix9 = new Matrix(this.BEADat.NTables.FTE_Ratio.getFTE_RATIO().Count, 1);
					int[] array7 = new int[2];
					int num32 = this.BEADat.NTables.FTE_Ratio.getFTE_RATIO().Count - 1;
					for (int num33 = 0; num33 <= num32; num33++)
					{
						matrix8[num33, 0] = this.BEADat.NTables.FTE_Ratio.getFTE_RATIO()[num33];
						matrix9[num33, 0] = 0.0;
					}
					array7[0] = 2;
					this.DispDat.hasEmployment = false;
					this.DispDat.useGeneralFormatOnly = false;
					this.DispDat.DataFormatTable = array7;
					this.DispDat.BaseTable = matrix8;
					this.DispDat.ColorTable = matrix9;
					this.DispDat.showCellColor = true;
					this.DispDat.hdrsCol = new string[1]
					{
						"FTE's/Job"
					};
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
					dispTotals = 1;
					frmTitle = "FTE-Job Ratios";
				}
				else
				{
					Interaction.MsgBox(Operators.ConcatenateObject("Unknown", stref), MsgBoxStyle.OkOnly, null);
				}
			}
			this.DisplayDataExtTab(frmTitle, (DispTotal)dispTotals);
		}

		private void DisplayDataExtTab(string frmTitle, DispTotal DispTotals)
		{
			TabContent tabContent = new TabContent(frmTitle, (byte)DispTotals);
			int index = -1;
			bool flag = false;
			checked
			{
				if (this.TabMain.TabPages.Count > 1)
				{
					int num = this.TabMain.TabPages.Count - 1;
					for (int i = 1; i <= num; i++)
					{
						if (string.Compare(this.TabMain.Controls[i].Text, frmTitle) == 0)
						{
							flag = true;
							index = i;
						}
					}
				}
				if (flag)
				{
					this.TabMain.TabPages.RemoveAt(index);
				}
				this.TabMain.TabPages.Add(tabContent);
				int index2 = this.TabMain.TabPages.IndexOf(tabContent);
				this.TabMain.SelectTab(index2);
				tabContent.tabsize = this.TabMain.SelectedTab.Size;
				tabContent.dispFormatGeneral = this.BEADat.FormatGeneral;
				tabContent.dispFormatRounded = this.BEADat.FormatRounded;
				tabContent.dispFormatCoefficients = this.BEADat.FormatCoefficients;
				tabContent.view_options.decimalsGenForm = this.BEADat.decimalsFormGen;
				if (frmTitle.Length > 0)
				{
					this.Table_Label.Text = frmTitle;
				}
				tabContent.dispdat = this.DispDat;
				tabContent.setup(ref this.BEADat.worksum, ref this.copyHeadersStatus);
				this.UndoDat.SetData(this.DispDat.BaseTable, this.DispDat.ExtraRows, this.DispDat.ExtraCols);
				this.UndoDat.Enabled = false;
				this.bDataDisplayed = true;
				this.TabStore.addTab(ref tabContent);
				this.MenuHandle.SetMenuStates();
			}
		}

		private void RunGenReqTab_Click(object sender, EventArgs e)
		{
			if (MsgBoxResult.Yes == Interaction.MsgBox("The level of aggregation will be fixed after calculation\r\nClick no and save the model if you want other levels of aggregation output", MsgBoxStyle.YesNo, "Continue with Calculations?"))
			{
				this.BEADat.Calculate();
				this.ClearScreen();
				this.MenuHandle.SetMenuStates();
				MyProject.Forms.frmNotify.Show("Requirements Tables Generated", null);
				this.ShowSummary();
				this.Table_Label.Text = "Requirements Tables Generated";
			}
		}

		private void CoEffSwitch_Click(object sender, EventArgs e)
		{
			this.bCoeff = !this.bCoeff;
            //TODO LAter
			//if (this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum_0024Init.State = 2;
			//		this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum = true;
			//	}
			//	else if (this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024CoEffSwitch_Click_002420211C1280B1_0024frum_0024Init);
			//}
			TabContent tabContent = (TabContent)this.TabMain.SelectedTab;
			DataGridView dgvnn = tabContent.dgvnn;
			string strB = this.BEADat.worksum.DataLabel + this.TabMain.SelectedTab.Text;
			Matrix matrix = new Matrix(dgvnn.RowCount, dgvnn.ColumnCount);
			checked
			{
				int num = dgvnn.RowCount - 1;
				for (int i = 0; i <= num; i++)
				{
					int num2 = dgvnn.ColumnCount - 1;
					for (int j = 0; j <= num2; j++)
					{
						matrix[i, j] = Conversions.ToDouble(dgvnn.Rows[i].Cells[j].Value);
					}
				}
				MatrixStore matrixStore = new MatrixStore(ref strB, ref matrix);
				bool flag = false;
				int num3 = this.DirectTabList.Count - 1;
				for (int k = 0; k <= num3; k++)
				{
					MatrixStore matrixStore2 = unchecked((MatrixStore)this.DirectTabList[k]);
					if (string.Compare(matrixStore2.myName, strB) == 0)
					{
						flag = true;
						matrixStore = matrixStore2;
					}
				}
				if (!flag)
				{
					MatrixStore matrixStore3 = new MatrixStore(ref strB, ref matrix);
					this.DirectTabList.Add(matrixStore3);
					matrixStore = matrixStore3;
				}
				matrix = matrixStore.myMatrix;
				this.bCoeff = matrixStore.myFlag;
				int num4 = dgvnn.RowCount - 1;
				for (int l = 0; l <= num4; l++)
				{
					int num5 = dgvnn.ColumnCount - 1;
					for (int m = 0; m <= num5; m++)
					{
						if (this.bCoeff)
						{
							dgvnn.Rows[l].Cells[m].Value = unchecked(matrix[l, m] * Matrix.ColSum(matrix, m));
						}
						else
						{
							dgvnn.Rows[l].Cells[m].Value = matrix[l, m];
						}
					}
				}
				matrixStore.myFlag = !matrixStore.myFlag;
			}
		}

		private void AdjustViewOptions()
		{
			DataGridView dataGridView;
			string format;
			if (this.TabMain.SelectedIndex == 0)
			{
				dataGridView = this.dgv;
				format = this.options.getGeneralFormat;
			}
			else
			{
				TabContent tabContent = (TabContent)this.TabMain.SelectedTab;
				dataGridView = tabContent.dgvnn;
				format = tabContent.dispFormatGeneral;
			}
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = dataGridView.Columns.GetEnumerator();
				while (enumerator.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
					dataGridViewColumn.DefaultCellStyle.Format = format;
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
		}

		private void SaveWorkspace(object sender, EventArgs e)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Title = "Enter Filename to Save Workspace";
			saveFileDialog.InitialDirectory = FileSystem.CurDir();
			saveFileDialog.Filter = "Workspace files (*.wrksp)|*.wrksp";
			saveFileDialog.FilterIndex = 1;
			saveFileDialog.RestoreDirectory = true;
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				string fileName = saveFileDialog.FileName;
				try
				{
					FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
					binaryFormatter.Serialize(fileStream, "1.0.2012.1");
					binaryFormatter.Serialize(fileStream, this.DataCollection.bealist);
					fileStream.Close();
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
		}

		private void LoadWorkspace(object sender, EventArgs e)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			string path = "";
			bool flag = true;
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Title = "Enter Filename to Load Workspace";
			openFileDialog.InitialDirectory = FileSystem.CurDir();
			openFileDialog.Filter = "Workspace files (*.wrksp)|*.wrksp";
			openFileDialog.FilterIndex = 1;
			openFileDialog.RestoreDirectory = true;
			if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miOpenModel", false))
			{
				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					path = openFileDialog.FileName;
				}
				else
				{
					flag = false;
				}
			}
			else
			{
				path = Conversions.ToString(NewLateBinding.LateGet(sender, null, "tag", new object[0], null, null, null));
			}
			if (flag)
			{
				try
				{
					FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
					BEAData bEAData = new BEAData();
					bEAData.worksum = new SummaryDat();
					bEAData.worksum.InitSetup();
					ArrayList arrayList = new ArrayList();
					string text = "";
					text = Conversions.ToString(binaryFormatter.Deserialize(fileStream));
					if (string.Compare(text, "1.0.2012.1") == 1)
					{
						Interaction.MsgBox("Please update your license to read more recent data.", MsgBoxStyle.OkOnly, null);
					}
					else
					{
						arrayList = (ArrayList)binaryFormatter.Deserialize(fileStream);
						this.DataCollection = new DataWrapREADER();
						this.DataCollection.setEmpty();
						Interaction.MsgBox(text, MsgBoxStyle.OkOnly, null);
						IEnumerator enumerator = default(IEnumerator);
						try
						{
							enumerator = arrayList.GetEnumerator();
							while (enumerator.MoveNext())
							{
								BEAData bEAData2 = (BEAData)enumerator.Current;
								this.DataCollection.addData(ref bEAData2);
							}
						}
						finally
						{
							if (enumerator is IDisposable)
							{
								(enumerator as IDisposable).Dispose();
							}
						}
						this.YearSelect_CBoxTS.Items.Clear();
						checked
						{
							int num = this.DataCollection.bealist.Count - 1;
							for (int i = 0; i <= num; i++)
							{
								this.YearSelect_CBoxTS.Items.Add(this.DataCollection.getData(i).worksum.DataLabel);
							}
							this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
							this.BEADat = this.DataCollection.getData(this.DataCollection.getNrec() - 1);
							this.ClearScreen();
							this.ShowSummary();
							this.MenuHandle.SetMenuStates();
						}
					}
					fileStream.Close();
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message + " ", MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
		}

		private void Start_Regionalization(object sender, EventArgs e)
		{
			if (Operators.CompareString(this.BEADat.worksum.dataaggregated, "No", false) == 0)
			{
				this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
				this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
				this.DispDat = new DispData();
				this.DispDat.isReadOnly = true;
				string text = "Data Regionalized";
				MyProject.Forms.frmRegMethod.bdat = this.BEADat;
				MyProject.Forms.frmRegMethod.worksum = this.BEADat.worksum;
				if (MyProject.Forms.frmRegMethod.ShowDialog() == DialogResult.OK)
				{
					this.BEADat.isDataRegionalized = false;
					this.BEADat.Regionalize();
					if (this.BEADat.isDataRegionalized)
					{
						this.TabClean();
						this.ClearScreen();
						this.MenuHandle.SetMenuStates();
						this.Table_Label.Text = text + " using " + this.BEADat.worksum.regmethod + " data for " + this.BEADat.worksum.regby;
						MyProject.Forms.frmNotify.Show(text + " using " + this.BEADat.worksum.regmethod + " data for " + this.BEADat.worksum.regby, null);
						this.ExtractRegionalData();
					}
				}
				this.Table_Label.Text = "";
				this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
				if (string.Compare(this.BEADat.worksum.dataregionalized, "NO") != -1)
				{
					if (MyProject.Forms.frmRegMethod.rbSelRegMain.Checked)
					{
						string text2 = MyProject.Forms.frmRegMethod.RegionComboBox.Text;
						string stateID = this.BEADat.StateIDs.getStateID(text2);
						this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex] = this.BEADat.RegStateData.GetState(ref stateID).PersonalIncome;
						this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex] = this.BEADat.RegStateData.GetState(ref stateID).DisposableIncome;
					}
					else if (MyProject.Forms.frmRegMethod.rbSelRegion.Checked)
					{
						string text3;
						if (this.AL_cboxChecked)
						{
							double[] mePersonalIncomeM = this.MePersonalIncomeM;
							int selectedIndex = this.YearSelect_CBoxTS.SelectedIndex;
							double num = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData = this.BEADat.RegStateData;
							text3 = "AL";
							mePersonalIncomeM[selectedIndex] = num + regStateData.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM = this.MeDisposableIncomeM;
							int selectedIndex2 = this.YearSelect_CBoxTS.SelectedIndex;
							double num2 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData2 = this.BEADat.RegStateData;
							text3 = "AL";
							meDisposableIncomeM[selectedIndex2] = num2 + regStateData2.GetState(ref text3).DisposableIncome;
						}
						if (this.AK_cboxChecked)
						{
							double[] mePersonalIncomeM2 = this.MePersonalIncomeM;
							int selectedIndex3 = this.YearSelect_CBoxTS.SelectedIndex;
							double num3 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData3 = this.BEADat.RegStateData;
							text3 = "AK";
							mePersonalIncomeM2[selectedIndex3] = num3 + regStateData3.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM2 = this.MeDisposableIncomeM;
							int selectedIndex4 = this.YearSelect_CBoxTS.SelectedIndex;
							double num4 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData4 = this.BEADat.RegStateData;
							text3 = "AK";
							meDisposableIncomeM2[selectedIndex4] = num4 + regStateData4.GetState(ref text3).DisposableIncome;
						}
						if (this.AR_cboxChecked)
						{
							double[] mePersonalIncomeM3 = this.MePersonalIncomeM;
							int selectedIndex5 = this.YearSelect_CBoxTS.SelectedIndex;
							double num5 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData5 = this.BEADat.RegStateData;
							text3 = "AR";
							mePersonalIncomeM3[selectedIndex5] = num5 + regStateData5.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM3 = this.MeDisposableIncomeM;
							int selectedIndex6 = this.YearSelect_CBoxTS.SelectedIndex;
							double num6 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData6 = this.BEADat.RegStateData;
							text3 = "AR";
							meDisposableIncomeM3[selectedIndex6] = num6 + regStateData6.GetState(ref text3).DisposableIncome;
						}
						if (this.AZ_cboxChecked)
						{
							double[] mePersonalIncomeM4 = this.MePersonalIncomeM;
							int selectedIndex7 = this.YearSelect_CBoxTS.SelectedIndex;
							double num7 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData7 = this.BEADat.RegStateData;
							text3 = "AZ";
							mePersonalIncomeM4[selectedIndex7] = num7 + regStateData7.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM4 = this.MeDisposableIncomeM;
							int selectedIndex8 = this.YearSelect_CBoxTS.SelectedIndex;
							double num8 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData8 = this.BEADat.RegStateData;
							text3 = "AZ";
							meDisposableIncomeM4[selectedIndex8] = num8 + regStateData8.GetState(ref text3).DisposableIncome;
						}
						if (this.CA_cboxChecked)
						{
							double[] mePersonalIncomeM5 = this.MePersonalIncomeM;
							int selectedIndex9 = this.YearSelect_CBoxTS.SelectedIndex;
							double num9 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData9 = this.BEADat.RegStateData;
							text3 = "CA";
							mePersonalIncomeM5[selectedIndex9] = num9 + regStateData9.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM5 = this.MeDisposableIncomeM;
							int selectedIndex10 = this.YearSelect_CBoxTS.SelectedIndex;
							double num10 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData10 = this.BEADat.RegStateData;
							text3 = "CA";
							meDisposableIncomeM5[selectedIndex10] = num10 + regStateData10.GetState(ref text3).DisposableIncome;
						}
						if (this.CO_cboxChecked)
						{
							double[] mePersonalIncomeM6 = this.MePersonalIncomeM;
							int selectedIndex11 = this.YearSelect_CBoxTS.SelectedIndex;
							double num11 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData11 = this.BEADat.RegStateData;
							text3 = "CO";
							mePersonalIncomeM6[selectedIndex11] = num11 + regStateData11.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM6 = this.MeDisposableIncomeM;
							int selectedIndex12 = this.YearSelect_CBoxTS.SelectedIndex;
							double num12 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData12 = this.BEADat.RegStateData;
							text3 = "CO";
							meDisposableIncomeM6[selectedIndex12] = num12 + regStateData12.GetState(ref text3).DisposableIncome;
						}
						if (this.CT_cboxChecked)
						{
							double[] mePersonalIncomeM7 = this.MePersonalIncomeM;
							int selectedIndex13 = this.YearSelect_CBoxTS.SelectedIndex;
							double num13 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData13 = this.BEADat.RegStateData;
							text3 = "CT";
							mePersonalIncomeM7[selectedIndex13] = num13 + regStateData13.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM7 = this.MeDisposableIncomeM;
							int selectedIndex14 = this.YearSelect_CBoxTS.SelectedIndex;
							double num14 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData14 = this.BEADat.RegStateData;
							text3 = "CT";
							meDisposableIncomeM7[selectedIndex14] = num14 + regStateData14.GetState(ref text3).DisposableIncome;
						}
						if (this.DC_cboxChecked)
						{
							double[] mePersonalIncomeM8 = this.MePersonalIncomeM;
							int selectedIndex15 = this.YearSelect_CBoxTS.SelectedIndex;
							double num15 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData15 = this.BEADat.RegStateData;
							text3 = "DC";
							mePersonalIncomeM8[selectedIndex15] = num15 + regStateData15.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM8 = this.MeDisposableIncomeM;
							int selectedIndex16 = this.YearSelect_CBoxTS.SelectedIndex;
							double num16 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData16 = this.BEADat.RegStateData;
							text3 = "DC";
							meDisposableIncomeM8[selectedIndex16] = num16 + regStateData16.GetState(ref text3).DisposableIncome;
						}
						if (this.DE_cboxChecked)
						{
							double[] mePersonalIncomeM9 = this.MePersonalIncomeM;
							int selectedIndex17 = this.YearSelect_CBoxTS.SelectedIndex;
							double num17 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData17 = this.BEADat.RegStateData;
							text3 = "DE";
							mePersonalIncomeM9[selectedIndex17] = num17 + regStateData17.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM9 = this.MeDisposableIncomeM;
							int selectedIndex18 = this.YearSelect_CBoxTS.SelectedIndex;
							double num18 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData18 = this.BEADat.RegStateData;
							text3 = "DE";
							meDisposableIncomeM9[selectedIndex18] = num18 + regStateData18.GetState(ref text3).DisposableIncome;
						}
						if (this.FL_cboxChecked)
						{
							double[] mePersonalIncomeM10 = this.MePersonalIncomeM;
							int selectedIndex19 = this.YearSelect_CBoxTS.SelectedIndex;
							double num19 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData19 = this.BEADat.RegStateData;
							text3 = "FL";
							mePersonalIncomeM10[selectedIndex19] = num19 + regStateData19.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM10 = this.MeDisposableIncomeM;
							int selectedIndex20 = this.YearSelect_CBoxTS.SelectedIndex;
							double num20 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData20 = this.BEADat.RegStateData;
							text3 = "FL";
							meDisposableIncomeM10[selectedIndex20] = num20 + regStateData20.GetState(ref text3).DisposableIncome;
						}
						if (this.GA_cboxChecked)
						{
							double[] mePersonalIncomeM11 = this.MePersonalIncomeM;
							int selectedIndex21 = this.YearSelect_CBoxTS.SelectedIndex;
							double num21 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData21 = this.BEADat.RegStateData;
							text3 = "GA";
							mePersonalIncomeM11[selectedIndex21] = num21 + regStateData21.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM11 = this.MeDisposableIncomeM;
							int selectedIndex22 = this.YearSelect_CBoxTS.SelectedIndex;
							double num22 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData22 = this.BEADat.RegStateData;
							text3 = "GA";
							meDisposableIncomeM11[selectedIndex22] = num22 + regStateData22.GetState(ref text3).DisposableIncome;
						}
						if (this.IA_cboxChecked)
						{
							double[] mePersonalIncomeM12 = this.MePersonalIncomeM;
							int selectedIndex23 = this.YearSelect_CBoxTS.SelectedIndex;
							double num23 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData23 = this.BEADat.RegStateData;
							text3 = "IA";
							mePersonalIncomeM12[selectedIndex23] = num23 + regStateData23.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM12 = this.MeDisposableIncomeM;
							int selectedIndex24 = this.YearSelect_CBoxTS.SelectedIndex;
							double num24 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData24 = this.BEADat.RegStateData;
							text3 = "IA";
							meDisposableIncomeM12[selectedIndex24] = num24 + regStateData24.GetState(ref text3).DisposableIncome;
						}
						if (this.ID_cboxChecked)
						{
							double[] mePersonalIncomeM13 = this.MePersonalIncomeM;
							int selectedIndex25 = this.YearSelect_CBoxTS.SelectedIndex;
							double num25 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData25 = this.BEADat.RegStateData;
							text3 = "ID";
							mePersonalIncomeM13[selectedIndex25] = num25 + regStateData25.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM13 = this.MeDisposableIncomeM;
							int selectedIndex26 = this.YearSelect_CBoxTS.SelectedIndex;
							double num26 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData26 = this.BEADat.RegStateData;
							text3 = "ID";
							meDisposableIncomeM13[selectedIndex26] = num26 + regStateData26.GetState(ref text3).DisposableIncome;
						}
						if (this.IL_cboxChecked)
						{
							double[] mePersonalIncomeM14 = this.MePersonalIncomeM;
							int selectedIndex27 = this.YearSelect_CBoxTS.SelectedIndex;
							double num27 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData27 = this.BEADat.RegStateData;
							text3 = "IL";
							mePersonalIncomeM14[selectedIndex27] = num27 + regStateData27.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM14 = this.MeDisposableIncomeM;
							int selectedIndex28 = this.YearSelect_CBoxTS.SelectedIndex;
							double num28 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData28 = this.BEADat.RegStateData;
							text3 = "IL";
							meDisposableIncomeM14[selectedIndex28] = num28 + regStateData28.GetState(ref text3).DisposableIncome;
						}
						if (this.IN_cboxChecked)
						{
							double[] mePersonalIncomeM15 = this.MePersonalIncomeM;
							int selectedIndex29 = this.YearSelect_CBoxTS.SelectedIndex;
							double num29 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData29 = this.BEADat.RegStateData;
							text3 = "IN";
							mePersonalIncomeM15[selectedIndex29] = num29 + regStateData29.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM15 = this.MeDisposableIncomeM;
							int selectedIndex30 = this.YearSelect_CBoxTS.SelectedIndex;
							double num30 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData30 = this.BEADat.RegStateData;
							text3 = "IN";
							meDisposableIncomeM15[selectedIndex30] = num30 + regStateData30.GetState(ref text3).DisposableIncome;
						}
						if (this.KS_cboxChecked)
						{
							double[] mePersonalIncomeM16 = this.MePersonalIncomeM;
							int selectedIndex31 = this.YearSelect_CBoxTS.SelectedIndex;
							double num31 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData31 = this.BEADat.RegStateData;
							text3 = "KS";
							mePersonalIncomeM16[selectedIndex31] = num31 + regStateData31.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM16 = this.MeDisposableIncomeM;
							int selectedIndex32 = this.YearSelect_CBoxTS.SelectedIndex;
							double num32 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData32 = this.BEADat.RegStateData;
							text3 = "KS";
							meDisposableIncomeM16[selectedIndex32] = num32 + regStateData32.GetState(ref text3).DisposableIncome;
						}
						if (this.KY_cboxChecked)
						{
							double[] mePersonalIncomeM17 = this.MePersonalIncomeM;
							int selectedIndex33 = this.YearSelect_CBoxTS.SelectedIndex;
							double num33 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData33 = this.BEADat.RegStateData;
							text3 = "KY";
							mePersonalIncomeM17[selectedIndex33] = num33 + regStateData33.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM17 = this.MeDisposableIncomeM;
							int selectedIndex34 = this.YearSelect_CBoxTS.SelectedIndex;
							double num34 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData34 = this.BEADat.RegStateData;
							text3 = "KY";
							meDisposableIncomeM17[selectedIndex34] = num34 + regStateData34.GetState(ref text3).DisposableIncome;
						}
						if (this.LA_cboxChecked)
						{
							double[] mePersonalIncomeM18 = this.MePersonalIncomeM;
							int selectedIndex35 = this.YearSelect_CBoxTS.SelectedIndex;
							double num35 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData35 = this.BEADat.RegStateData;
							text3 = "LA";
							mePersonalIncomeM18[selectedIndex35] = num35 + regStateData35.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM18 = this.MeDisposableIncomeM;
							int selectedIndex36 = this.YearSelect_CBoxTS.SelectedIndex;
							double num36 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData36 = this.BEADat.RegStateData;
							text3 = "LA";
							meDisposableIncomeM18[selectedIndex36] = num36 + regStateData36.GetState(ref text3).DisposableIncome;
						}
						if (this.MA_cboxChecked)
						{
							double[] mePersonalIncomeM19 = this.MePersonalIncomeM;
							int selectedIndex37 = this.YearSelect_CBoxTS.SelectedIndex;
							double num37 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData37 = this.BEADat.RegStateData;
							text3 = "MA";
							mePersonalIncomeM19[selectedIndex37] = num37 + regStateData37.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM19 = this.MeDisposableIncomeM;
							int selectedIndex38 = this.YearSelect_CBoxTS.SelectedIndex;
							double num38 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData38 = this.BEADat.RegStateData;
							text3 = "MA";
							meDisposableIncomeM19[selectedIndex38] = num38 + regStateData38.GetState(ref text3).DisposableIncome;
						}
						if (this.ME_cboxChecked)
						{
							double[] mePersonalIncomeM20 = this.MePersonalIncomeM;
							int selectedIndex39 = this.YearSelect_CBoxTS.SelectedIndex;
							double num39 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData39 = this.BEADat.RegStateData;
							text3 = "ME";
							mePersonalIncomeM20[selectedIndex39] = num39 + regStateData39.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM20 = this.MeDisposableIncomeM;
							int selectedIndex40 = this.YearSelect_CBoxTS.SelectedIndex;
							double num40 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData40 = this.BEADat.RegStateData;
							text3 = "ME";
							meDisposableIncomeM20[selectedIndex40] = num40 + regStateData40.GetState(ref text3).DisposableIncome;
						}
						if (this.MI_cboxChecked)
						{
							double[] mePersonalIncomeM21 = this.MePersonalIncomeM;
							int selectedIndex41 = this.YearSelect_CBoxTS.SelectedIndex;
							double num41 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData41 = this.BEADat.RegStateData;
							text3 = "MI";
							mePersonalIncomeM21[selectedIndex41] = num41 + regStateData41.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM21 = this.MeDisposableIncomeM;
							int selectedIndex42 = this.YearSelect_CBoxTS.SelectedIndex;
							double num42 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData42 = this.BEADat.RegStateData;
							text3 = "MI";
							meDisposableIncomeM21[selectedIndex42] = num42 + regStateData42.GetState(ref text3).DisposableIncome;
						}
						if (this.MN_cboxChecked)
						{
							double[] mePersonalIncomeM22 = this.MePersonalIncomeM;
							int selectedIndex43 = this.YearSelect_CBoxTS.SelectedIndex;
							double num43 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData43 = this.BEADat.RegStateData;
							text3 = "MN";
							mePersonalIncomeM22[selectedIndex43] = num43 + regStateData43.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM22 = this.MeDisposableIncomeM;
							int selectedIndex44 = this.YearSelect_CBoxTS.SelectedIndex;
							double num44 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData44 = this.BEADat.RegStateData;
							text3 = "MN";
							meDisposableIncomeM22[selectedIndex44] = num44 + regStateData44.GetState(ref text3).DisposableIncome;
						}
						if (this.MD_cboxChecked)
						{
							double[] mePersonalIncomeM23 = this.MePersonalIncomeM;
							int selectedIndex45 = this.YearSelect_CBoxTS.SelectedIndex;
							double num45 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData45 = this.BEADat.RegStateData;
							text3 = "MD";
							mePersonalIncomeM23[selectedIndex45] = num45 + regStateData45.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM23 = this.MeDisposableIncomeM;
							int selectedIndex46 = this.YearSelect_CBoxTS.SelectedIndex;
							double num46 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData46 = this.BEADat.RegStateData;
							text3 = "MD";
							meDisposableIncomeM23[selectedIndex46] = num46 + regStateData46.GetState(ref text3).DisposableIncome;
						}
						if (this.MO_cboxChecked)
						{
							double[] mePersonalIncomeM24 = this.MePersonalIncomeM;
							int selectedIndex47 = this.YearSelect_CBoxTS.SelectedIndex;
							double num47 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData47 = this.BEADat.RegStateData;
							text3 = "MO";
							mePersonalIncomeM24[selectedIndex47] = num47 + regStateData47.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM24 = this.MeDisposableIncomeM;
							int selectedIndex48 = this.YearSelect_CBoxTS.SelectedIndex;
							double num48 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData48 = this.BEADat.RegStateData;
							text3 = "MO";
							meDisposableIncomeM24[selectedIndex48] = num48 + regStateData48.GetState(ref text3).DisposableIncome;
						}
						if (this.MS_cboxChecked)
						{
							double[] mePersonalIncomeM25 = this.MePersonalIncomeM;
							int selectedIndex49 = this.YearSelect_CBoxTS.SelectedIndex;
							double num49 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData49 = this.BEADat.RegStateData;
							text3 = "MS";
							mePersonalIncomeM25[selectedIndex49] = num49 + regStateData49.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM25 = this.MeDisposableIncomeM;
							int selectedIndex50 = this.YearSelect_CBoxTS.SelectedIndex;
							double num50 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData50 = this.BEADat.RegStateData;
							text3 = "MS";
							meDisposableIncomeM25[selectedIndex50] = num50 + regStateData50.GetState(ref text3).DisposableIncome;
						}
						if (this.MT_cboxChecked)
						{
							double[] mePersonalIncomeM26 = this.MePersonalIncomeM;
							int selectedIndex51 = this.YearSelect_CBoxTS.SelectedIndex;
							double num51 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData51 = this.BEADat.RegStateData;
							text3 = "MT";
							mePersonalIncomeM26[selectedIndex51] = num51 + regStateData51.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM26 = this.MeDisposableIncomeM;
							int selectedIndex52 = this.YearSelect_CBoxTS.SelectedIndex;
							double num52 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData52 = this.BEADat.RegStateData;
							text3 = "MT";
							meDisposableIncomeM26[selectedIndex52] = num52 + regStateData52.GetState(ref text3).DisposableIncome;
						}
						if (this.NE_cboxChecked)
						{
							double[] mePersonalIncomeM27 = this.MePersonalIncomeM;
							int selectedIndex53 = this.YearSelect_CBoxTS.SelectedIndex;
							double num53 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData53 = this.BEADat.RegStateData;
							text3 = "NE";
							mePersonalIncomeM27[selectedIndex53] = num53 + regStateData53.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM27 = this.MeDisposableIncomeM;
							int selectedIndex54 = this.YearSelect_CBoxTS.SelectedIndex;
							double num54 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData54 = this.BEADat.RegStateData;
							text3 = "NE";
							meDisposableIncomeM27[selectedIndex54] = num54 + regStateData54.GetState(ref text3).DisposableIncome;
						}
						if (this.ND_cboxChecked)
						{
							double[] mePersonalIncomeM28 = this.MePersonalIncomeM;
							int selectedIndex55 = this.YearSelect_CBoxTS.SelectedIndex;
							double num55 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData55 = this.BEADat.RegStateData;
							text3 = "ND";
							mePersonalIncomeM28[selectedIndex55] = num55 + regStateData55.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM28 = this.MeDisposableIncomeM;
							int selectedIndex56 = this.YearSelect_CBoxTS.SelectedIndex;
							double num56 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData56 = this.BEADat.RegStateData;
							text3 = "ND";
							meDisposableIncomeM28[selectedIndex56] = num56 + regStateData56.GetState(ref text3).DisposableIncome;
						}
						if (this.NC_cboxChecked)
						{
							double[] mePersonalIncomeM29 = this.MePersonalIncomeM;
							int selectedIndex57 = this.YearSelect_CBoxTS.SelectedIndex;
							double num57 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData57 = this.BEADat.RegStateData;
							text3 = "NC";
							mePersonalIncomeM29[selectedIndex57] = num57 + regStateData57.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM29 = this.MeDisposableIncomeM;
							int selectedIndex58 = this.YearSelect_CBoxTS.SelectedIndex;
							double num58 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData58 = this.BEADat.RegStateData;
							text3 = "NC";
							meDisposableIncomeM29[selectedIndex58] = num58 + regStateData58.GetState(ref text3).DisposableIncome;
						}
						if (this.NH_cboxChecked)
						{
							double[] mePersonalIncomeM30 = this.MePersonalIncomeM;
							int selectedIndex59 = this.YearSelect_CBoxTS.SelectedIndex;
							double num59 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData59 = this.BEADat.RegStateData;
							text3 = "NH";
							mePersonalIncomeM30[selectedIndex59] = num59 + regStateData59.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM30 = this.MeDisposableIncomeM;
							int selectedIndex60 = this.YearSelect_CBoxTS.SelectedIndex;
							double num60 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData60 = this.BEADat.RegStateData;
							text3 = "NH";
							meDisposableIncomeM30[selectedIndex60] = num60 + regStateData60.GetState(ref text3).DisposableIncome;
						}
						if (this.NJ_cboxChecked)
						{
							double[] mePersonalIncomeM31 = this.MePersonalIncomeM;
							int selectedIndex61 = this.YearSelect_CBoxTS.SelectedIndex;
							double num61 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData61 = this.BEADat.RegStateData;
							text3 = "NJ";
							mePersonalIncomeM31[selectedIndex61] = num61 + regStateData61.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM31 = this.MeDisposableIncomeM;
							int selectedIndex62 = this.YearSelect_CBoxTS.SelectedIndex;
							double num62 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData62 = this.BEADat.RegStateData;
							text3 = "NJ";
							meDisposableIncomeM31[selectedIndex62] = num62 + regStateData62.GetState(ref text3).DisposableIncome;
						}
						if (this.NM_cboxChecked)
						{
							double[] mePersonalIncomeM32 = this.MePersonalIncomeM;
							int selectedIndex63 = this.YearSelect_CBoxTS.SelectedIndex;
							double num63 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData63 = this.BEADat.RegStateData;
							text3 = "NM";
							mePersonalIncomeM32[selectedIndex63] = num63 + regStateData63.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM32 = this.MeDisposableIncomeM;
							int selectedIndex64 = this.YearSelect_CBoxTS.SelectedIndex;
							double num64 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData64 = this.BEADat.RegStateData;
							text3 = "NM";
							meDisposableIncomeM32[selectedIndex64] = num64 + regStateData64.GetState(ref text3).DisposableIncome;
						}
						if (this.NY_cboxChecked)
						{
							double[] mePersonalIncomeM33 = this.MePersonalIncomeM;
							int selectedIndex65 = this.YearSelect_CBoxTS.SelectedIndex;
							double num65 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData65 = this.BEADat.RegStateData;
							text3 = "NY";
							mePersonalIncomeM33[selectedIndex65] = num65 + regStateData65.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM33 = this.MeDisposableIncomeM;
							int selectedIndex66 = this.YearSelect_CBoxTS.SelectedIndex;
							double num66 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData66 = this.BEADat.RegStateData;
							text3 = "NY";
							meDisposableIncomeM33[selectedIndex66] = num66 + regStateData66.GetState(ref text3).DisposableIncome;
						}
						if (this.NV_cboxChecked)
						{
							double[] mePersonalIncomeM34 = this.MePersonalIncomeM;
							int selectedIndex67 = this.YearSelect_CBoxTS.SelectedIndex;
							double num67 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData67 = this.BEADat.RegStateData;
							text3 = "NV";
							mePersonalIncomeM34[selectedIndex67] = num67 + regStateData67.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM34 = this.MeDisposableIncomeM;
							int selectedIndex68 = this.YearSelect_CBoxTS.SelectedIndex;
							double num68 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData68 = this.BEADat.RegStateData;
							text3 = "NV";
							meDisposableIncomeM34[selectedIndex68] = num68 + regStateData68.GetState(ref text3).DisposableIncome;
						}
						if (this.HI_cboxChecked)
						{
							double[] mePersonalIncomeM35 = this.MePersonalIncomeM;
							int selectedIndex69 = this.YearSelect_CBoxTS.SelectedIndex;
							double num69 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData69 = this.BEADat.RegStateData;
							text3 = "HI";
							mePersonalIncomeM35[selectedIndex69] = num69 + regStateData69.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM35 = this.MeDisposableIncomeM;
							int selectedIndex70 = this.YearSelect_CBoxTS.SelectedIndex;
							double num70 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData70 = this.BEADat.RegStateData;
							text3 = "HI";
							meDisposableIncomeM35[selectedIndex70] = num70 + regStateData70.GetState(ref text3).DisposableIncome;
						}
						if (this.OH_cboxChecked)
						{
							double[] mePersonalIncomeM36 = this.MePersonalIncomeM;
							int selectedIndex71 = this.YearSelect_CBoxTS.SelectedIndex;
							double num71 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData71 = this.BEADat.RegStateData;
							text3 = "OH";
							mePersonalIncomeM36[selectedIndex71] = num71 + regStateData71.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM36 = this.MeDisposableIncomeM;
							int selectedIndex72 = this.YearSelect_CBoxTS.SelectedIndex;
							double num72 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData72 = this.BEADat.RegStateData;
							text3 = "OH";
							meDisposableIncomeM36[selectedIndex72] = num72 + regStateData72.GetState(ref text3).DisposableIncome;
						}
						if (this.OK_cboxChecked)
						{
							double[] mePersonalIncomeM37 = this.MePersonalIncomeM;
							int selectedIndex73 = this.YearSelect_CBoxTS.SelectedIndex;
							double num73 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData73 = this.BEADat.RegStateData;
							text3 = "OK";
							mePersonalIncomeM37[selectedIndex73] = num73 + regStateData73.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM37 = this.MeDisposableIncomeM;
							int selectedIndex74 = this.YearSelect_CBoxTS.SelectedIndex;
							double num74 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData74 = this.BEADat.RegStateData;
							text3 = "OK";
							meDisposableIncomeM37[selectedIndex74] = num74 + regStateData74.GetState(ref text3).DisposableIncome;
						}
						if (this.OR_cboxChecked)
						{
							double[] mePersonalIncomeM38 = this.MePersonalIncomeM;
							int selectedIndex75 = this.YearSelect_CBoxTS.SelectedIndex;
							double num75 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData75 = this.BEADat.RegStateData;
							text3 = "OR";
							mePersonalIncomeM38[selectedIndex75] = num75 + regStateData75.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM38 = this.MeDisposableIncomeM;
							int selectedIndex76 = this.YearSelect_CBoxTS.SelectedIndex;
							double num76 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData76 = this.BEADat.RegStateData;
							text3 = "OR";
							meDisposableIncomeM38[selectedIndex76] = num76 + regStateData76.GetState(ref text3).DisposableIncome;
						}
						if (this.PA_cboxChecked)
						{
							double[] mePersonalIncomeM39 = this.MePersonalIncomeM;
							int selectedIndex77 = this.YearSelect_CBoxTS.SelectedIndex;
							double num77 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData77 = this.BEADat.RegStateData;
							text3 = "PA";
							mePersonalIncomeM39[selectedIndex77] = num77 + regStateData77.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM39 = this.MeDisposableIncomeM;
							int selectedIndex78 = this.YearSelect_CBoxTS.SelectedIndex;
							double num78 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData78 = this.BEADat.RegStateData;
							text3 = "PA";
							meDisposableIncomeM39[selectedIndex78] = num78 + regStateData78.GetState(ref text3).DisposableIncome;
						}
						if (this.RI_cboxChecked)
						{
							double[] mePersonalIncomeM40 = this.MePersonalIncomeM;
							int selectedIndex79 = this.YearSelect_CBoxTS.SelectedIndex;
							double num79 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData79 = this.BEADat.RegStateData;
							text3 = "RI";
							mePersonalIncomeM40[selectedIndex79] = num79 + regStateData79.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM40 = this.MeDisposableIncomeM;
							int selectedIndex80 = this.YearSelect_CBoxTS.SelectedIndex;
							double num80 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData80 = this.BEADat.RegStateData;
							text3 = "RI";
							meDisposableIncomeM40[selectedIndex80] = num80 + regStateData80.GetState(ref text3).DisposableIncome;
						}
						if (this.SC_cboxChecked)
						{
							double[] mePersonalIncomeM41 = this.MePersonalIncomeM;
							int selectedIndex81 = this.YearSelect_CBoxTS.SelectedIndex;
							double num81 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData81 = this.BEADat.RegStateData;
							text3 = "SC";
							mePersonalIncomeM41[selectedIndex81] = num81 + regStateData81.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM41 = this.MeDisposableIncomeM;
							int selectedIndex82 = this.YearSelect_CBoxTS.SelectedIndex;
							double num82 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData82 = this.BEADat.RegStateData;
							text3 = "SC";
							meDisposableIncomeM41[selectedIndex82] = num82 + regStateData82.GetState(ref text3).DisposableIncome;
						}
						if (this.SD_cboxChecked)
						{
							double[] mePersonalIncomeM42 = this.MePersonalIncomeM;
							int selectedIndex83 = this.YearSelect_CBoxTS.SelectedIndex;
							double num83 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData83 = this.BEADat.RegStateData;
							text3 = "SD";
							mePersonalIncomeM42[selectedIndex83] = num83 + regStateData83.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM42 = this.MeDisposableIncomeM;
							int selectedIndex84 = this.YearSelect_CBoxTS.SelectedIndex;
							double num84 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData84 = this.BEADat.RegStateData;
							text3 = "SD";
							meDisposableIncomeM42[selectedIndex84] = num84 + regStateData84.GetState(ref text3).DisposableIncome;
						}
						if (this.TN_cboxChecked)
						{
							double[] mePersonalIncomeM43 = this.MePersonalIncomeM;
							int selectedIndex85 = this.YearSelect_CBoxTS.SelectedIndex;
							double num85 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData85 = this.BEADat.RegStateData;
							text3 = "TN";
							mePersonalIncomeM43[selectedIndex85] = num85 + regStateData85.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM43 = this.MeDisposableIncomeM;
							int selectedIndex86 = this.YearSelect_CBoxTS.SelectedIndex;
							double num86 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData86 = this.BEADat.RegStateData;
							text3 = "TN";
							meDisposableIncomeM43[selectedIndex86] = num86 + regStateData86.GetState(ref text3).DisposableIncome;
						}
						if (this.TX_cboxChecked)
						{
							double[] mePersonalIncomeM44 = this.MePersonalIncomeM;
							int selectedIndex87 = this.YearSelect_CBoxTS.SelectedIndex;
							double num87 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData87 = this.BEADat.RegStateData;
							text3 = "TX";
							mePersonalIncomeM44[selectedIndex87] = num87 + regStateData87.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM44 = this.MeDisposableIncomeM;
							int selectedIndex88 = this.YearSelect_CBoxTS.SelectedIndex;
							double num88 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData88 = this.BEADat.RegStateData;
							text3 = "TX";
							meDisposableIncomeM44[selectedIndex88] = num88 + regStateData88.GetState(ref text3).DisposableIncome;
						}
						if (this.UT_cboxChecked)
						{
							double[] mePersonalIncomeM45 = this.MePersonalIncomeM;
							int selectedIndex89 = this.YearSelect_CBoxTS.SelectedIndex;
							double num89 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData89 = this.BEADat.RegStateData;
							text3 = "UT";
							mePersonalIncomeM45[selectedIndex89] = num89 + regStateData89.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM45 = this.MeDisposableIncomeM;
							int selectedIndex90 = this.YearSelect_CBoxTS.SelectedIndex;
							double num90 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData90 = this.BEADat.RegStateData;
							text3 = "UT";
							meDisposableIncomeM45[selectedIndex90] = num90 + regStateData90.GetState(ref text3).DisposableIncome;
						}
						if (this.VA_cboxChecked)
						{
							double[] mePersonalIncomeM46 = this.MePersonalIncomeM;
							int selectedIndex91 = this.YearSelect_CBoxTS.SelectedIndex;
							double num91 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData91 = this.BEADat.RegStateData;
							text3 = "VA";
							mePersonalIncomeM46[selectedIndex91] = num91 + regStateData91.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM46 = this.MeDisposableIncomeM;
							int selectedIndex92 = this.YearSelect_CBoxTS.SelectedIndex;
							double num92 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData92 = this.BEADat.RegStateData;
							text3 = "VA";
							meDisposableIncomeM46[selectedIndex92] = num92 + regStateData92.GetState(ref text3).DisposableIncome;
						}
						if (this.VT_cboxChecked)
						{
							double[] mePersonalIncomeM47 = this.MePersonalIncomeM;
							int selectedIndex93 = this.YearSelect_CBoxTS.SelectedIndex;
							double num93 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData93 = this.BEADat.RegStateData;
							text3 = "VT";
							mePersonalIncomeM47[selectedIndex93] = num93 + regStateData93.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM47 = this.MeDisposableIncomeM;
							int selectedIndex94 = this.YearSelect_CBoxTS.SelectedIndex;
							double num94 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData94 = this.BEADat.RegStateData;
							text3 = "VT";
							meDisposableIncomeM47[selectedIndex94] = num94 + regStateData94.GetState(ref text3).DisposableIncome;
						}
						if (this.WA_cboxChecked)
						{
							double[] mePersonalIncomeM48 = this.MePersonalIncomeM;
							int selectedIndex95 = this.YearSelect_CBoxTS.SelectedIndex;
							double num95 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData95 = this.BEADat.RegStateData;
							text3 = "WA";
							mePersonalIncomeM48[selectedIndex95] = num95 + regStateData95.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM48 = this.MeDisposableIncomeM;
							int selectedIndex96 = this.YearSelect_CBoxTS.SelectedIndex;
							double num96 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData96 = this.BEADat.RegStateData;
							text3 = "WA";
							meDisposableIncomeM48[selectedIndex96] = num96 + regStateData96.GetState(ref text3).DisposableIncome;
						}
						if (this.WI_cboxChecked)
						{
							double[] mePersonalIncomeM49 = this.MePersonalIncomeM;
							int selectedIndex97 = this.YearSelect_CBoxTS.SelectedIndex;
							double num97 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData97 = this.BEADat.RegStateData;
							text3 = "WI";
							mePersonalIncomeM49[selectedIndex97] = num97 + regStateData97.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM49 = this.MeDisposableIncomeM;
							int selectedIndex98 = this.YearSelect_CBoxTS.SelectedIndex;
							double num98 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData98 = this.BEADat.RegStateData;
							text3 = "WI";
							meDisposableIncomeM49[selectedIndex98] = num98 + regStateData98.GetState(ref text3).DisposableIncome;
						}
						if (this.WV_cboxChecked)
						{
							double[] mePersonalIncomeM50 = this.MePersonalIncomeM;
							int selectedIndex99 = this.YearSelect_CBoxTS.SelectedIndex;
							double num99 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData99 = this.BEADat.RegStateData;
							text3 = "WV";
							mePersonalIncomeM50[selectedIndex99] = num99 + regStateData99.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM50 = this.MeDisposableIncomeM;
							int selectedIndex100 = this.YearSelect_CBoxTS.SelectedIndex;
							double num100 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData100 = this.BEADat.RegStateData;
							text3 = "WV";
							meDisposableIncomeM50[selectedIndex100] = num100 + regStateData100.GetState(ref text3).DisposableIncome;
						}
						if (this.WY_cboxChecked)
						{
							double[] mePersonalIncomeM51 = this.MePersonalIncomeM;
							int selectedIndex101 = this.YearSelect_CBoxTS.SelectedIndex;
							double num101 = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData101 = this.BEADat.RegStateData;
							text3 = "WY";
							mePersonalIncomeM51[selectedIndex101] = num101 + regStateData101.GetState(ref text3).PersonalIncome;
							double[] meDisposableIncomeM51 = this.MeDisposableIncomeM;
							int selectedIndex102 = this.YearSelect_CBoxTS.SelectedIndex;
							double num102 = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex];
							RegionalData regStateData102 = this.BEADat.RegStateData;
							text3 = "WY";
							meDisposableIncomeM51[selectedIndex102] = num102 + regStateData102.GetState(ref text3).DisposableIncome;
						}
						double num103 = Vector.sum(this.BEADat.EC) / BEAData.EC_T_Sum;
						this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex] = this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex] * num103;
						this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex] = this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex] * num103;
					}
					else if (MyProject.Forms.frmRegMethod.rbStateSelection.Checked)
					{
						string text4 = MyProject.Forms.frmRegMethod.comboBoxStateSelection.Text;
						string stateID2 = this.BEADat.StateIDs.getStateID(text4);
						double num104 = Vector.sum(this.BEADat.EC) / BEAData.EC_T_Sum;
						this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex] = this.BEADat.RegStateData.GetState(ref stateID2).PersonalIncome * num104;
						this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex] = this.BEADat.RegStateData.GetState(ref stateID2).DisposableIncome * num104;
					}
					else if (MyProject.Forms.frmRegMethod.rbUserDefined.Checked)
					{
						this.MePersonalIncomeM[this.YearSelect_CBoxTS.SelectedIndex] = this.BEADat.PersonalIncome;
						this.MeDisposableIncomeM[this.YearSelect_CBoxTS.SelectedIndex] = this.BEADat.DisposableIncome;
					}
				}
			}
			else
			{
				Interaction.MsgBox("Aggregated base tables cannot be regionalized", MsgBoxStyle.OkOnly, null);
			}
			this.ShowSummary();
		}

		private void miGRP_Click(object sender, EventArgs e)
		{
			int num = 10;
			string[] array = new string[checked(num + 1)];
			this.BEADat.Calculate();
			DataGridView dataGridView = new DataGridView();
			Matrix baseTable = new Matrix(num, 4);
			this.DispDat = new DispData();
			string text = "";
			byte dispTotals = 1;
			array[0] = "";
			this.DispDat.hdrsCol = new string[4]
			{
				"",
				"",
				"",
				""
			};
			this.DispDat.hdrsRow = array;
			this.DispDat.BaseTable = baseTable;
			text = "Gross Product Accounts";
			this.DispDat.isAfterGRT = true;
			this.DisplayDataExtTab(text, (DispTotal)dispTotals);
			TabContent tabContent = (TabContent)this.TabMain.SelectedTab;
			tabContent.replaceDataGridView(ref dataGridView);
			dataGridView = tabContent.dgvnn;
			dataGridView.CellValueChanged -= this.dgv_CellValueChanged;
			DataGridView dataGridView2 = dataGridView;
			dataGridView2.Columns.Clear();
			dataGridView2.DefaultCellStyle.Format = this.options.getGeneralFormat;
			dataGridView2.RowHeadersVisible = false;
			dataGridView2.Columns.Add("Expenditure ($M)", "Expenditure ($M)");
			dataGridView2.Columns.Add("Exp", "");
			dataGridView2.Columns.Add("Income ($M)", "Income ($M)");
			dataGridView2.Columns.Add("Inc", "");
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = dataGridView2.Columns.GetEnumerator();
				while (enumerator.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
					dataGridViewColumn.MinimumWidth = 250;
					if (dataGridViewColumn.Index == 1 | dataGridViewColumn.Index == 3)
					{
						dataGridViewColumn.MinimumWidth = 150;
						dataGridViewColumn.Width = 150;
					}
					dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
					dataGridViewColumn.ReadOnly = false;
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			DataGridViewRow dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow2 = dataGridViewRow;
			dataGridViewRow2.CreateCells(dataGridView);
			dataGridViewRow2.HeaderCell.Value = "";
			dataGridViewRow2.Cells[0].Value = "Private Consumption";
			dataGridViewRow2.Cells[1].Value = Matrix.ColSum(this.BEADat.FinDem, 0);
			dataGridViewRow2.Cells[2].Value = "Wages";
			dataGridViewRow2.Cells[3].Value = Vector.sum(this.BEADat.EC);
			double num2 = Conversions.ToDouble(dataGridViewRow2.Cells[3].Value);
			dataGridViewRow2 = null;
			dataGridView2.Rows.Add(dataGridViewRow);
			double num3 = 0.0;
			int num4 = checked(this.BEADat.FinDem.NoCols - 1);
			for (int i = 4; i <= num4; i = checked(i + 1))
			{
				num3 += Matrix.ColSum(this.BEADat.FinDem, i);
			}
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow3 = dataGridViewRow;
			dataGridViewRow3.CreateCells(dataGridView);
			dataGridViewRow3.HeaderCell.Value = "";
			dataGridViewRow3.Cells[0].Value = "Government Expenditures";
			dataGridViewRow3.Cells[1].Value = num3;
			dataGridViewRow3.Cells[2].Value = "Government";
			dataGridViewRow3.Cells[3].Value = Vector.sum(this.BEADat.T);
			num2 = Conversions.ToDouble(Operators.AddObject(num2, dataGridViewRow3.Cells[3].Value));
			dataGridViewRow3 = null;
			dataGridView2.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow4 = dataGridViewRow;
			dataGridViewRow4.CreateCells(dataGridView);
			dataGridViewRow4.HeaderCell.Value = "";
			dataGridViewRow4.Cells[0].Value = "Capital";
			dataGridViewRow4.Cells[1].Value = Matrix.ColSum(this.BEADat.FinDem, 1) + Matrix.ColSum(this.BEADat.FinDem, 2);
			dataGridViewRow4.Cells[2].Value = "Gross Operating Supplies";
			dataGridViewRow4.Cells[3].Value = Vector.sum(this.BEADat.GOS);
			num2 = Conversions.ToDouble(Operators.AddObject(num2, dataGridViewRow4.Cells[3].Value));
			dataGridViewRow4 = null;
			dataGridView2.Rows.Add(dataGridViewRow);
			dataGridView2.Rows.Add();
			dataGridViewRow = new DataGridViewRow();
			dataGridViewRow.CreateCells(dataGridView);
			dataGridViewRow.HeaderCell.Value = "";
			dataGridViewRow.Cells[0].Value = "Gross Domestic Expenditure";
			dataGridViewRow.Cells[1].Value = Operators.AddObject(Operators.AddObject(dataGridView2.Rows[0].Cells[1].Value, dataGridView2.Rows[1].Cells[1].Value), dataGridView2.Rows[2].Cells[1].Value);
			dataGridView2.Rows.Add(dataGridViewRow);
			dataGridView2.Rows.Add();
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow5 = dataGridViewRow;
			dataGridViewRow5.CreateCells(dataGridView);
			dataGridViewRow5.HeaderCell.Value = "";
			dataGridViewRow5.Cells[0].Value = "Exports";
			dataGridViewRow5.Cells[1].Value = Matrix.ColSum(this.BEADat.FinDem, 3);
			dataGridViewRow5 = null;
			dataGridView2.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow6 = dataGridViewRow;
			dataGridViewRow6.CreateCells(dataGridView);
			dataGridViewRow6.HeaderCell.Value = "";
			dataGridViewRow6.Cells[0].Value = "Imports";
			dataGridViewRow6.Cells[1].Value = Matrix.RowSum(this.BEADat.Import, 0);
			dataGridViewRow6 = null;
			dataGridView2.Rows.Add(dataGridViewRow);
			dataGridView2.Rows.Add();
			dataGridViewRow = new DataGridViewRow();
			dataGridViewRow.CreateCells(dataGridView);
			dataGridViewRow.HeaderCell.Value = "";
			dataGridViewRow.Cells[0].Value = "Gross Product";
			dataGridViewRow.Cells[1].Value = Operators.SubtractObject(Operators.AddObject(dataGridView2.Rows[4].Cells[1].Value, dataGridView2.Rows[6].Cells[1].Value), dataGridView2.Rows[7].Cells[1].Value);
			dataGridViewRow.Cells[2].Value = "Gross Product";
			dataGridViewRow.Cells[3].Value = Operators.AddObject(Operators.AddObject(dataGridView2.Rows[0].Cells[3].Value, dataGridView2.Rows[1].Cells[3].Value), dataGridView2.Rows[2].Cells[3].Value);
			dataGridView2.Rows.Add(dataGridViewRow);
			dataGridView2 = null;
			this.bDataDisplayed = true;
			this.MenuHandle.SetMenuStates();
			this.Table_Label.Text = "Gross Regional Expenditure";
		}

		private void miDMIxCO_Click(object sender, EventArgs e)
		{
			this.showMultipliers(Conversions.ToString(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null)));
		}

		private void showMultipliers(string snmae)
		{
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			string[] array = new string[4]
			{
				"Simple",
				"Total",
				"Type I",
				"Type II"
			};
			string text = "";
			BEAData bEADat = this.BEADat;
			checked
			{
				Matrix matrix;
				switch (BEAData.ComputeStringHash(snmae))
				{
				case 135011563u:
					if (Operators.CompareString(snmae, "miDMIxCO", false) == 0)
					{
						matrix = new Matrix(this.BEADat.N_INDUSTRIES, 5);
						Matrix matrix3 = bEADat.IxC_DRTo;
						Matrix mat = bEADat.IxC_TRo;
						Matrix mat2 = bEADat.IxC_TRc;
						text = "Disaggregated IxC Output Multipliers";
						this.DispDat.hdrsCol = new string[5]
						{
							"First Round",
							"Flow-on",
							"Income Induced",
							"Type I",
							"Type II"
						};
						int num4 = matrix.NoRows - 1;
						for (int l = 0; l <= num4; l++)
						{
							matrix[l, 0] = Matrix.ColSum(matrix3, l);
							unchecked
							{
								matrix[l, 1] = Matrix.ColSum(mat, l) - matrix[l, 0];
								matrix[l, 3] = Matrix.ColSum(mat, l);
								matrix[l, 4] = Matrix.ColSum(mat2, l) - mat2[checked(mat2.NoRows - 1), l];
								matrix[l, 2] = matrix[l, 4] - matrix[l, 3];
							}
						}
						break;
					}
					goto default;
				case 1746545557u:
					if (Operators.CompareString(snmae, "miDMIxIO", false) == 0)
					{
						matrix = new Matrix(this.BEADat.N_INDUSTRIES, 5);
						Matrix matrix3 = bEADat.Ao;
						Matrix mat = bEADat.LeonOpen;
						Matrix mat2 = bEADat.LeonClosed;
						text = "Disaggregated IxI Output Multipliers";
						this.DispDat.hdrsCol = new string[5]
						{
							"First Round",
							"Flow-on",
							"Income Induced",
							"Type I",
							"Type II"
						};
						int num6 = matrix.NoRows - 1;
						for (int n = 0; n <= num6; n++)
						{
							matrix[n, 0] = Matrix.ColSum(matrix3, n);
							unchecked
							{
								matrix[n, 1] = Matrix.ColSum(mat, n) - matrix[n, 0];
								matrix[n, 3] = Matrix.ColSum(mat, n);
								matrix[n, 4] = Matrix.ColSum(mat2, n) - mat2[checked(mat2.NoRows - 1), n];
								matrix[n, 2] = matrix[n, 4] - matrix[n, 3];
							}
						}
						break;
					}
					goto default;
				case 18237733u:
					if (Operators.CompareString(snmae, "miDMCxCO", false) == 0)
					{
						matrix = new Matrix(this.BEADat.N_INDUSTRIES, 5);
						Matrix matrix3 = bEADat.CxC_DRTo;
						Matrix mat = bEADat.CxC_TRo;
						Matrix mat2 = bEADat.CxC_TRc;
						text = "Disaggregated CxC Output Multipliers";
						this.DispDat.hdrsCol = new string[5]
						{
							"First Round",
							"Flow-on",
							"Income Induced",
							"Type I",
							"Type II"
						};
						int num3 = matrix.NoRows - 1;
						for (int k = 0; k <= num3; k++)
						{
							matrix[k, 0] = Matrix.ColSum(matrix3, k);
							unchecked
							{
								matrix[k, 1] = Matrix.ColSum(mat, k) - matrix[k, 0];
								matrix[k, 3] = Matrix.ColSum(mat, k);
								matrix[k, 4] = Matrix.ColSum(mat2, k) - mat2[checked(mat2.NoRows - 1), k];
								matrix[k, 2] = matrix[k, 4] - matrix[k, 3];
							}
						}
						break;
					}
					goto default;
				case 1645879843u:
					if (Operators.CompareString(snmae, "miDMIxII", false) == 0)
					{
						matrix = new Matrix(this.BEADat.N_INDUSTRIES, 5);
						Matrix matrix5 = (bEADat.EC / bEADat.g).toRowMatrix();
						Vector vector11 = matrix5.ColSum();
						Matrix matrix3 = matrix5 * bEADat.Ao;
						Matrix mat = matrix5 * bEADat.LeonOpen;
						Matrix mat2 = Matrix.Append(matrix5, new Vector(1), new Vector(0)) * bEADat.LeonClosed;
						Vector vector12 = Matrix.ColSum(mat);
						Vector vector13 = Matrix.ColSum(mat2);
						Vector vector14 = new Vector(matrix.NoRows);
						Vector vector15 = new Vector(matrix.NoRows);
						text = "Disaggregated IxI Income Multipliers";
						this.DispDat.hdrsCol = new string[5]
						{
							"Income Coefficient",
							"Simple",
							"Total",
							"Type I",
							"Type II"
						};
						int num5 = matrix.NoRows - 1;
						for (int m = 0; m <= num5; m++)
						{
							unchecked
							{
								vector14[m] = vector12[m] / vector11[m];
								vector15[m] = vector13[m] / vector11[m];
								matrix[m, 0] = vector11[m];
								matrix[m, 1] = vector12[m];
								matrix[m, 2] = vector13[m];
								matrix[m, 3] = vector14[m];
								if (double.IsNaN(matrix[m, 3]))
								{
									matrix[m, 3] = 0.0;
								}
								matrix[m, 4] = vector15[m];
								if (double.IsNaN(matrix[m, 4]))
								{
									matrix[m, 4] = 0.0;
								}
							}
						}
						break;
					}
					goto default;
				case 235677277u:
					if (Operators.CompareString(snmae, "miDMIxCI", false) == 0)
					{
						matrix = new Matrix(this.BEADat.N_INDUSTRIES, 5);
						Matrix matrix4 = (bEADat.EC / bEADat.g).toRowMatrix();
						Vector vector6 = matrix4.ColSum();
						Matrix mat = matrix4 * bEADat.IxC_TRo;
						Matrix mat2 = Matrix.Append(matrix4, new Vector(1), new Vector(0)) * bEADat.IxC_TRc;
						Vector vector7 = Matrix.ColSum(mat);
						Vector vector8 = Matrix.ColSum(mat2);
						Vector vector9 = new Vector(matrix.NoRows);
						Vector vector10 = new Vector(matrix.NoRows);
						text = "Disaggregated IxC Income Multipliers";
						this.DispDat.hdrsCol = new string[5]
						{
							"Income Coefficient",
							"Simple",
							"Total",
							"Type I",
							"Type II"
						};
						int num2 = matrix.NoRows - 1;
						for (int j = 0; j <= num2; j++)
						{
							unchecked
							{
								vector9[j] = vector7[j] / vector6[j];
								vector10[j] = vector8[j] / vector6[j];
								matrix[j, 0] = vector6[j];
								matrix[j, 1] = vector7[j];
								matrix[j, 2] = vector8[j];
								matrix[j, 3] = vector9[j];
								if (double.IsInfinity(matrix[j, 3]))
								{
									matrix[j, 3] = 0.0;
								}
								matrix[j, 4] = vector10[j];
								if (double.IsInfinity(matrix[j, 4]))
								{
									matrix[j, 4] = 0.0;
								}
							}
						}
						break;
					}
					goto default;
				case 1847211271u:
					if (Operators.CompareString(snmae, "miDMIxIE", false) == 0)
					{
						matrix = new Matrix(this.BEADat.N_INDUSTRIES, 5);
						Matrix matrix6 = (!this.BEADat.worksum.dataregionalized.Contains("Yes")) ? (bEADat.NTables.Employment / bEADat.g).toRowMatrix() : (bEADat.NTables.RegEmployment / bEADat.g).toRowMatrix();
						Vector vector16 = matrix6.ColSum();
						Vector vector17 = new Vector(matrix.NoRows);
						Vector vector18 = new Vector(matrix.NoRows);
						Matrix matrix3 = matrix6 * bEADat.Ao;
						Matrix mat = matrix6 * bEADat.LeonOpen;
						Matrix mat2 = Matrix.Append(matrix6, new Vector(1), new Vector(0)) * bEADat.LeonClosed;
						Vector vector19 = Matrix.ColSum(mat);
						Vector vector20 = Matrix.ColSum(mat2);
						int num7 = matrix.NoRows - 1;
						for (int num8 = 0; num8 <= num7; num8++)
						{
							unchecked
							{
								vector17[num8] = vector19[num8] / vector16[num8];
								vector18[num8] = vector20[num8] / vector16[num8];
								matrix[num8, 0] = vector16[num8];
								matrix[num8, 1] = vector19[num8];
								matrix[num8, 2] = vector20[num8];
								matrix[num8, 3] = vector17[num8];
								if (double.IsNaN(matrix[num8, 3]))
								{
									matrix[num8, 3] = 0.0;
								}
								matrix[num8, 4] = vector18[num8];
								if (double.IsNaN(matrix[num8, 4]))
								{
									matrix[num8, 4] = 0.0;
								}
							}
						}
						text = "Disaggregated IxI Employment Multipliers";
						this.DispDat.hasEmployment = true;
						this.DispDat.hdrsCol = new string[5]
						{
							"Employees per $1M Output",
							"Simple",
							"Total",
							"Type I",
							"Type II"
						};
						break;
					}
					goto default;
				case 34345849u:
					if (Operators.CompareString(snmae, "miDMIxCE", false) == 0)
					{
						matrix = new Matrix(this.BEADat.N_INDUSTRIES, 5);
						Matrix matrix2 = (!this.BEADat.worksum.dataregionalized.Contains("Yes")) ? (bEADat.NTables.Employment / bEADat.g).toRowMatrix() : (bEADat.NTables.RegEmployment / bEADat.g).toRowMatrix();
						Vector vector = matrix2.ColSum();
						Vector vector2 = new Vector(matrix.NoRows);
						Vector vector3 = new Vector(matrix.NoRows);
						Matrix matrix3 = matrix2 * bEADat.IxC_DRTo;
						Matrix mat = matrix2 * bEADat.IxC_TRo;
						Matrix mat2 = Matrix.Append(matrix2, new Vector(1), new Vector(0)) * bEADat.IxC_TRc;
						Vector vector4 = Matrix.ColSum(mat);
						Vector vector5 = Matrix.ColSum(mat2);
						int num = matrix.NoRows - 1;
						for (int i = 0; i <= num; i++)
						{
							unchecked
							{
								vector2[i] = vector4[i] / vector[i];
								vector3[i] = vector5[i] / vector[i];
								matrix[i, 0] = vector[i];
								matrix[i, 1] = vector4[i];
								matrix[i, 2] = vector5[i];
								matrix[i, 3] = vector2[i];
								if (double.IsInfinity(matrix[i, 3]))
								{
									matrix[i, 3] = 0.0;
								}
								matrix[i, 4] = vector3[i];
								if (double.IsInfinity(matrix[i, 4]))
								{
									matrix[i, 4] = 0.0;
								}
							}
						}
						text = "Disaggregated IxC Employment Multipliers";
						this.DispDat.hasEmployment = true;
						this.DispDat.hdrsCol = new string[5]
						{
							"Employees per $1M Output",
							"Simple",
							"Total",
							"Type I",
							"Type II"
						};
						break;
					}
					goto default;
				default:
					Interaction.MsgBox("Ooops : " + snmae + " still working on it - Code 7852", MsgBoxStyle.OkOnly, null);
					return;
				}
				bEADat = null;
				this.DispDat.isSortable = true;
				this.DispDat.useGeneralFormatOnly = false;
				int[] array2 = new int[this.DispDat.hdrsCol.Length + 1];
				int num9 = array2.Length - 1;
				for (int num10 = 0; num10 <= num9; num10++)
				{
					array2[num10] = 2;
				}
				this.DispDat.DataFormatTable = array2;
				this.DispDat.BaseTable = matrix;
				DispData dispDat = this.DispDat;
				dispDat.hdrsRow = this.BEADat.NTables.hdrInd;
				dispDat = null;
				this.getStatMat2(matrix, ref this.DispDat);
				this.DisplayDataExtTab(text, DispTotal.No);
			}
		}

		private void miOutputs_Click(object sender, EventArgs e)
		{
			Matrix matrix = new Matrix(this.BEADat.N_INDUSTRIES, 8);
			int col = 0;
			int col2 = 1;
			int col3 = 2;
			int col4 = 3;
			int num = 4;
			int col5 = 5;
			int col6 = 6;
			int col7 = 7;
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			int num2 = checked(this.BEADat.N_INDUSTRIES - 1);
			for (int i = 0; i <= num2; i = checked(i + 1))
			{
				matrix[i, col2] = this.BEADat.NTables.Make().RowSum(i);
				matrix[i, col3] = this.BEADat.get_FinDemVector(i, 0);
				matrix[i, col4] = this.BEADat.get_FinDemVector(i, 1) + this.BEADat.get_FinDemVector(i, 2);
				matrix[i, num] = 0.0;
				int num3 = checked(this.BEADat.FinDem.NoCols - 1);
				for (int j = 4; j <= num3; j = checked(j + 1))
				{
					Matrix matrix2;
					int row;
					int col8;
					(matrix2 = matrix)[row = i, col8 = num] = matrix2[row, col8] + this.BEADat.get_FinDemVector(i, j);
				}
				matrix[i, col5] = this.BEADat.get_FinDemVector(i, 3);
				matrix[i, col6] = this.BEADat.Import[0, i] * -1.0;
				matrix[i, col7] = matrix[i, col3] + matrix[i, col4] + matrix[i, num] + matrix[i, col5] + matrix[i, col6];
				matrix[i, col] = matrix[i, col2] + matrix[i, col7];
			}
			this.DispDat.BaseTable = matrix;
			this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
			this.DispDat.isSortable = true;
			this.DispDat.hdrsCol = new string[8]
			{
				"Total Commodity Output",
				"Intermediate Demand",
				"Consumption Demand",
				"Investment Demand",
				"Government Demand",
				"Exports",
				"Imports",
				"Final Demand"
			};
			this.getStatMat2(matrix, ref this.DispDat);
			this.DisplayDataExtTab("Commodity Accounts", DispTotal.No);
		}

		private void SetupDistributions(Matrix arr)
		{
			int noCols = arr.NoCols;
			int[] cols = new int[2]
			{
				0,
				checked(noCols - 1)
			};
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
			int noRows = arr.NoRows;
			this.DispDat.ExtraRows = this.GetStatsMat(arr, cols);
			this.DispDat.hdrsXRows = new string[6]
			{
				"Total",
				"Average",
				"Minimum",
				"Median",
				"Maximum",
				"Std Dev"
			};
			this.DispDat.BaseTable = arr;
		}

		private void getStatMat2(Matrix arr, ref DispData dispdat)
		{
			Matrix matrix = new Matrix(6, arr.NoCols);
			int noRows = arr.NoRows;
			checked
			{
				int num = (int)Math.Round(unchecked((double)noRows / 2.0));
				int num2 = arr.NoCols - 1;
				for (int i = 0; i <= num2; i++)
				{
					double num3 = 1.7976931348623157E+308;
					double num4 = -1.7976931348623157E+308;
					double[] array = new double[noRows + 1];
					double num5 = 0.0;
					int num6 = noRows - 1;
					double num7 = default(double);
					for (int j = 0; j <= num6; j++)
					{
						num7 = arr[j, i];
						array[j] = arr[j, i];
						num5 = unchecked(num5 + num7);
						if (num7 > num4)
						{
							num4 = num7;
						}
						if (num7 < num3)
						{
							num3 = num7;
						}
					}
					Array.Sort(array);
					matrix[3, i] = array[num];
					matrix[0, i] = num5;
					num5 = (matrix[1, i] = unchecked(num5 / (double)noRows));
					matrix[2, i] = num3;
					matrix[4, i] = num4;
					int num9 = noRows - 1;
					for (int k = 0; k <= num9; k++)
					{
						num7 = unchecked(num7 + Math.Pow(arr[k, i] - num5, 2.0));
					}
					num7 = unchecked(num7 / (double)noRows);
					num7 = (matrix[5, i] = Math.Sqrt(num7));
				}
				dispdat.ExtraRows = matrix;
				dispdat.hdrsXRows = new string[6]
				{
					"Total",
					"Average",
					"Minimum",
					"Median",
					"Maximum",
					"Std Dev"
				};
			}
		}

		private Matrix GetStatsMat(Matrix Arr, int[] Cols)
		{
			int noRows = Arr.NoRows;
			checked
			{
				int num = (int)Math.Round(unchecked((double)noRows / 2.0));
				Matrix matrix = new Matrix(6, Arr.NoCols);
				foreach (int col in Cols)
				{
					double num2 = 1.7976931348623157E+308;
					double num3 = -1.7976931348623157E+308;
					double[] array = new double[noRows + 1];
					double num4 = 0.0;
					int num5 = noRows - 1;
					double num6 = default(double);
					for (int j = 0; j <= num5; j++)
					{
						num6 = Arr[j, col];
						array[j] = Arr[j, col];
						num4 = unchecked(num4 + num6);
						if (num6 > num3)
						{
							num3 = num6;
						}
						if (num6 < num2)
						{
							num2 = num6;
						}
					}
					Array.Sort(array);
					matrix[3, col] = array[num];
					matrix[0, col] = num4;
					num4 = (matrix[1, col] = unchecked(num4 / (double)noRows));
					matrix[2, col] = num2;
					matrix[4, col] = num3;
					int num8 = noRows - 1;
					for (int j = 0; j <= num8; j++)
					{
						num6 = unchecked(num6 + Math.Pow(Arr[j, col] - num4, 2.0));
					}
					num6 = unchecked(num6 / (double)noRows);
					num6 = (matrix[5, col] = Math.Sqrt(num6));
				}
				return matrix;
			}
		}

		private void miTrade_Click(object sender, EventArgs e)
		{
			this.BEADat.Calculate();
			Matrix matrix = new Matrix(this.BEADat.N_INDUSTRIES, 3);
			int num = checked(this.BEADat.N_INDUSTRIES - 1);
			for (int i = 0; i <= num; i = checked(i + 1))
			{
				matrix[i, 0] = this.BEADat.Import[0, i];
				matrix[i, 1] = this.BEADat.get_FinDemVector(i, 3);
				matrix[i, 2] = matrix[i, 1] - matrix[i, 0];
			}
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
			this.DispDat.isSortable = true;
			this.DispDat.BaseTable = matrix;
			this.getStatMat2(matrix, ref this.DispDat);
			this.DispDat.hdrsCol = new string[3]
			{
				"Imports",
				"Exports",
				"Net Trade"
			};
			this.DisplayDataExtTab("Commodity Trade Distributions", DispTotal.No);
		}

		private void miLabor_Click(object sender, EventArgs e)
		{
			this.BEADat.Calculate();
			Matrix matrix = new Matrix(this.BEADat.N_INDUSTRIES, 4);
			int col = 0;
			int col2 = 1;
			int col3 = 2;
			int col4 = 3;
			int num = 4;
			int num2 = 5;
			int num3 = checked(this.BEADat.N_INDUSTRIES - 1);
			for (int i = 0; i <= num3; i = checked(i + 1))
			{
				matrix[i, col2] = this.BEADat.get_ECDouble(i);
				if (this.BEADat.worksum.dataregionalized.Contains("Yes"))
				{
					matrix[i, col] = this.BEADat.NTables.RegEmployment[i];
				}
				else
				{
					matrix[i, col] = this.BEADat.NTables.Employment[i];
				}
				try
				{
					matrix[i, col3] = 1000.0 * matrix[i, col2] / matrix[i, col];
					if (double.IsNaN(matrix[i, col3]))
					{
						matrix[i, col3] = 0.0;
					}
				}
				catch (Exception projectError)
				{
					ProjectData.SetProjectError(projectError);
					matrix[i, col3] = 0.0;
					Information.Err().Clear();
					ProjectData.ClearProjectError();
				}
				try
				{
					matrix[i, col4] = matrix[i, col] / this.BEADat.g[i];
				}
				catch (Exception projectError2)
				{
					ProjectData.SetProjectError(projectError2);
					matrix[i, col4] = 0.0;
					Information.Err().Clear();
					ProjectData.ClearProjectError();
				}
			}
			this.DispDat = new DispData();
			this.DispDat.hasEmployment = true;
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
			this.DispDat.isSortable = true;
			this.DispDat.BaseTable = matrix;
			this.getStatMat2(matrix, ref this.DispDat);
			this.DispDat.hdrsCol = new string[4]
			{
				"Employment by industry",
				"Employee compensation",
				"Compensation per employee ($K)",
				"Employees per $1M Output"
			};
			this.DisplayDataExtTab("Labor Sector Distributions", DispTotal.No);
		}

		private void miIndustryAccounts_Click(object sender, EventArgs e)
		{
			this.BEADat.Calculate();
			Matrix matrix = new Matrix(this.BEADat.N_INDUSTRIES, 6);
			Matrix matrix2 = new Matrix(this.BEADat.N_COMMODITIES, 5);
			Matrix matrix3 = new Matrix(this.BEADat.N_INDUSTRIES, 5);
			Vector vector = this.BEADat.Import.ColSum();
			Vector vector2 = new Vector(this.BEADat.N_COMMODITIES);
			int num4;
			checked
			{
				int num = this.BEADat.N_COMMODITIES - 1;
				for (int i = 0; i <= num; i++)
				{
					vector2[i] = unchecked(this.BEADat.q[i] + this.BEADat.Import[0, i]);
				}
				int num2 = this.BEADat.N_COMMODITIES - 1;
				for (int j = 0; j <= num2; j++)
				{
					matrix2[j, 0] = this.BEADat.get_FinDemVector(j, 0);
					matrix2[j, 0] = this.BEADat.get_FinDemVector(j, 0);
					matrix2[j, 1] = unchecked(this.BEADat.get_FinDemVector(j, 1) + this.BEADat.get_FinDemVector(j, 2));
					matrix2[j, 2] = 0.0;
					int num3 = this.BEADat.FinDem.NoCols - 1;
					for (int k = 4; k <= num3; k++)
					{
						Matrix matrix4;
						int row;
						(matrix4 = matrix2)[row = j, 2] = unchecked(matrix4[row, 2] + this.BEADat.get_FinDemVector(j, k));
					}
					matrix2[j, 3] = this.BEADat.get_FinDemVector(j, 3);
					matrix2[j, 4] = vector[j];
				}
				matrix3 = this.BEADat.D * matrix2;
				num4 = this.BEADat.N_INDUSTRIES - 1;
			}
			for (int l = 0; l <= num4; l = checked(l + 1))
			{
				double num5 = this.BEADat.g[l];
				if (num5 == 999888999888.0)
				{
					num5 = 0.0;
				}
				matrix[l, 0] = num5;
				matrix[l, 1] = num5 - this.BEADat.NTables.VA.ColSum(l);
				matrix[l, 2] = this.BEADat.get_ECDouble(l);
				matrix[l, 3] = this.BEADat.get_GOSDouble(l);
				matrix[l, 4] = this.BEADat.get_TDouble(l);
				matrix[l, 5] = matrix[l, 2] + matrix[l, 3] + matrix[l, 4];
			}
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
			this.DispDat.isSortable = true;
			this.DispDat.BaseTable = matrix;
			this.getStatMat2(matrix, ref this.DispDat);
			this.DispDat.hdrsCol = new string[6]
			{
				"Total industry output",
				"Intermediate input",
				"Employee compensation",
				"Gross operating surplus",
				"Taxes",
				"Total value added"
			};
			this.DisplayDataExtTab("Industry Accounts", DispTotal.No);
		}

		private void PasteClipboard()
		{
			string text = Clipboard.GetText();
			string[] array = text.Split('\r');
			int num = this.dgv.CurrentCell.RowIndex;
			int columnIndex = this.dgv.CurrentCell.ColumnIndex;
			this.UndoDat.SetData(this.DispDat.BaseTable, this.DispDat.ExtraRows, this.DispDat.ExtraCols);
			this.dgv.CellValueChanged -= this.dgv_CellValueChanged;
			string[] array2 = array;
			checked
			{
				int num2 = default(int);
				foreach (string text2 in array2)
				{
					if (num >= this.dgv.RowCount || text2.Length <= 0)
					{
						break;
					}
					string[] array3 = text2.Split('\t');
					int upperBound = array3.GetUpperBound(0);
					for (int j = 0; j <= upperBound; j++)
					{
						if (columnIndex + j < this.dgv.ColumnCount)
						{
							DataGridViewCell dataGridViewCell = unchecked((DataGridView)this.dgv)[columnIndex + j, num];
							if (!dataGridViewCell.ReadOnly)
							{
								if (!dataGridViewCell.Value.Equals(DBNull.Value) && Operators.CompareString(dataGridViewCell.Value.ToString(), array3[j], false) != 0)
								{
									dataGridViewCell.Value = RuntimeHelpers.GetObjectValue(Convert.ChangeType(array3[j], dataGridViewCell.ValueType));
									dataGridViewCell.Style.BackColor = Color.Tomato;
									double value;
									if (num > this.UndoDat.CloneData[0].NoRows)
									{
										string s = Conversions.ToString(dataGridViewCell.Value);
										Matrix extraRows;
										int row;
										int col;
										value = (extraRows = this.DispDat.ExtraRows)[row = num - this.UndoDat.CloneData[0].NoRows, col = columnIndex + j];
										double.TryParse(s, out value);
										extraRows[row, col] = value;
									}
									else if (columnIndex + j > this.UndoDat.CloneData[0].NoCols)
									{
										string s2 = Conversions.ToString(dataGridViewCell.Value);
										Matrix extraRows;
										int col;
										int row;
										value = (extraRows = this.DispDat.ExtraCols)[col = num, row = columnIndex + j - this.UndoDat.CloneData[0].NoCols];
										double.TryParse(s2, out value);
										extraRows[col, row] = value;
									}
									else
									{
										string s3 = Conversions.ToString(dataGridViewCell.Value);
										Matrix extraRows;
										int row;
										int col;
										value = (extraRows = this.DispDat.BaseTable)[row = num, col = columnIndex + j];
										double.TryParse(s3, out value);
										extraRows[row, col] = value;
									}
								}
							}
							else
							{
								num2++;
							}
						}
					}
					num++;
				}
				this.dgv.CellValueChanged += this.dgv_CellValueChanged;
				if (num2 > 0)
				{
					MessageBox.Show(Conversions.ToString(num2) + " updates failed due to read only column setting");
				}
			}
		}

		private void miExit_Click(object sender, EventArgs e)
		{
			Environment.Exit(0);
		}

		private void miDecDecimal_Click(object sender, EventArgs e)
		{
			if (this.TabMain.SelectedIndex == 0)
			{
				DataGridView dgv = this.dgv;
				this.options.decimalsGenForm = Math.Max(0, checked(this.options.decimalsGenForm - 1));
				string getGeneralFormat = this.options.getGeneralFormat;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = dgv.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
						dataGridViewColumn.DefaultCellStyle.Format = getGeneralFormat;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				dgv.Refresh();
			}
			else
			{
				TabContent tabContent = (TabContent)this.TabMain.SelectedTab;
				DataGridView dgv = tabContent.dgvnn;
				if (tabContent.dispdat.useGeneralFormatOnly)
				{
					tabContent.view_options.decimalsGenForm = Math.Max(0, checked(tabContent.view_options.decimalsGenForm - 1));
					string getGeneralFormat = tabContent.view_options.getGeneralFormat;
					IEnumerator enumerator2 = default(IEnumerator);
					try
					{
						enumerator2 = dgv.Columns.GetEnumerator();
						while (enumerator2.MoveNext())
						{
							DataGridViewColumn dataGridViewColumn2 = (DataGridViewColumn)enumerator2.Current;
							dataGridViewColumn2.DefaultCellStyle.Format = getGeneralFormat;
						}
					}
					finally
					{
						if (enumerator2 is IDisposable)
						{
							(enumerator2 as IDisposable).Dispose();
						}
					}
				}
				else
				{
					int num = 0;
					try
					{
						switch (this.DispDat.DataFormatTable[num])
						{
						case 1:
						{
							tabContent.view_options.decimalsGenForm = Math.Max(0, checked(tabContent.view_options.decimalsGenForm - 1));
							string getGeneralFormat = tabContent.view_options.getGeneralFormat;
							IEnumerator enumerator5 = default(IEnumerator);
							try
							{
								enumerator5 = dgv.Columns.GetEnumerator();
								while (enumerator5.MoveNext())
								{
									DataGridViewColumn dataGridViewColumn5 = (DataGridViewColumn)enumerator5.Current;
									dataGridViewColumn5.DefaultCellStyle.Format = getGeneralFormat;
								}
							}
							finally
							{
								if (enumerator5 is IDisposable)
								{
									(enumerator5 as IDisposable).Dispose();
								}
							}
							break;
						}
						case 2:
						{
							tabContent.view_options.decimalsCoeffForm = Math.Max(0, checked(tabContent.view_options.decimalsCoeffForm - 1));
							string getGeneralFormat = tabContent.view_options.getCoefficientFormat;
							IEnumerator enumerator4 = default(IEnumerator);
							try
							{
								enumerator4 = dgv.Columns.GetEnumerator();
								while (enumerator4.MoveNext())
								{
									DataGridViewColumn dataGridViewColumn4 = (DataGridViewColumn)enumerator4.Current;
									dataGridViewColumn4.DefaultCellStyle.Format = getGeneralFormat;
								}
							}
							finally
							{
								if (enumerator4 is IDisposable)
								{
									(enumerator4 as IDisposable).Dispose();
								}
							}
							break;
						}
						case 3:
						{
							IEnumerator enumerator3 = default(IEnumerator);
							try
							{
								enumerator3 = dgv.Columns.GetEnumerator();
								while (enumerator3.MoveNext())
								{
									DataGridViewColumn dataGridViewColumn3 = (DataGridViewColumn)enumerator3.Current;
									dataGridViewColumn3.DefaultCellStyle.Format = tabContent.view_options.getRoundFormat;
								}
							}
							finally
							{
								if (enumerator3 is IDisposable)
								{
									(enumerator3 as IDisposable).Dispose();
								}
							}
							break;
						}
						}
					}
					catch (Exception ex)
					{
						ProjectData.SetProjectError(ex);
						Exception ex2 = ex;
						tabContent.view_options.decimalsGenForm = Math.Max(0, checked(tabContent.view_options.decimalsGenForm - 1));
						string getGeneralFormat = tabContent.view_options.getGeneralFormat;
						IEnumerator enumerator6 = default(IEnumerator);
						try
						{
							enumerator6 = dgv.Columns.GetEnumerator();
							while (enumerator6.MoveNext())
							{
								DataGridViewColumn dataGridViewColumn6 = (DataGridViewColumn)enumerator6.Current;
								dataGridViewColumn6.DefaultCellStyle.Format = getGeneralFormat;
							}
						}
						finally
						{
							if (enumerator6 is IDisposable)
							{
								(enumerator6 as IDisposable).Dispose();
							}
						}
						ProjectData.ClearProjectError();
					}
					num = checked(num + 1);
				}
				dgv.Refresh();
			}
		}

		private void miIncDecimal_Click(object sender, EventArgs e)
		{
			if (this.TabMain.SelectedIndex == 0)
			{
				DataGridView dgv = this.dgv;
				this.options.decimalsGenForm = Math.Min(14, checked(this.options.decimalsGenForm + 1));
				string getGeneralFormat = this.options.getGeneralFormat;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = dgv.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
						dataGridViewColumn.DefaultCellStyle.Format = getGeneralFormat;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				dgv.Refresh();
			}
			else
			{
				TabContent tabContent = (TabContent)this.TabMain.SelectedTab;
				DataGridView dgv = tabContent.dgvnn;
				if (tabContent.dispdat.useGeneralFormatOnly)
				{
					tabContent.view_options.decimalsGenForm = Math.Min(14, checked(tabContent.view_options.decimalsGenForm + 1));
					string getGeneralFormat = tabContent.view_options.getGeneralFormat;
					IEnumerator enumerator2 = default(IEnumerator);
					try
					{
						enumerator2 = dgv.Columns.GetEnumerator();
						while (enumerator2.MoveNext())
						{
							DataGridViewColumn dataGridViewColumn2 = (DataGridViewColumn)enumerator2.Current;
							dataGridViewColumn2.DefaultCellStyle.Format = getGeneralFormat;
						}
					}
					finally
					{
						if (enumerator2 is IDisposable)
						{
							(enumerator2 as IDisposable).Dispose();
						}
					}
				}
				else
				{
					int num = 0;
					try
					{
						switch (this.DispDat.DataFormatTable[num])
						{
						case 1:
						{
							tabContent.view_options.decimalsGenForm = Math.Min(6, checked(tabContent.view_options.decimalsGenForm + 1));
							string getGeneralFormat = tabContent.view_options.getGeneralFormat;
							IEnumerator enumerator5 = default(IEnumerator);
							try
							{
								enumerator5 = dgv.Columns.GetEnumerator();
								while (enumerator5.MoveNext())
								{
									DataGridViewColumn dataGridViewColumn5 = (DataGridViewColumn)enumerator5.Current;
									dataGridViewColumn5.DefaultCellStyle.Format = getGeneralFormat;
								}
							}
							finally
							{
								if (enumerator5 is IDisposable)
								{
									(enumerator5 as IDisposable).Dispose();
								}
							}
							break;
						}
						case 2:
						{
							tabContent.view_options.decimalsCoeffForm = Math.Min(4, checked(tabContent.view_options.decimalsCoeffForm + 1));
							string getGeneralFormat = tabContent.view_options.getCoefficientFormat;
							IEnumerator enumerator4 = default(IEnumerator);
							try
							{
								enumerator4 = dgv.Columns.GetEnumerator();
								while (enumerator4.MoveNext())
								{
									DataGridViewColumn dataGridViewColumn4 = (DataGridViewColumn)enumerator4.Current;
									dataGridViewColumn4.DefaultCellStyle.Format = getGeneralFormat;
								}
							}
							finally
							{
								if (enumerator4 is IDisposable)
								{
									(enumerator4 as IDisposable).Dispose();
								}
							}
							break;
						}
						case 3:
						{
							IEnumerator enumerator3 = default(IEnumerator);
							try
							{
								enumerator3 = dgv.Columns.GetEnumerator();
								while (enumerator3.MoveNext())
								{
									DataGridViewColumn dataGridViewColumn3 = (DataGridViewColumn)enumerator3.Current;
									dataGridViewColumn3.DefaultCellStyle.Format = tabContent.view_options.getRoundFormat;
								}
							}
							finally
							{
								if (enumerator3 is IDisposable)
								{
									(enumerator3 as IDisposable).Dispose();
								}
							}
							break;
						}
						}
					}
					catch (Exception ex)
					{
						ProjectData.SetProjectError(ex);
						Exception ex2 = ex;
						tabContent.view_options.decimalsGenForm = Math.Min(6, checked(tabContent.view_options.decimalsGenForm + 1));
						string getGeneralFormat = tabContent.view_options.getGeneralFormat;
						IEnumerator enumerator6 = default(IEnumerator);
						try
						{
							enumerator6 = dgv.Columns.GetEnumerator();
							while (enumerator6.MoveNext())
							{
								DataGridViewColumn dataGridViewColumn6 = (DataGridViewColumn)enumerator6.Current;
								dataGridViewColumn6.DefaultCellStyle.Format = getGeneralFormat;
							}
						}
						finally
						{
							if (enumerator6 is IDisposable)
							{
								(enumerator6 as IDisposable).Dispose();
							}
						}
						ProjectData.ClearProjectError();
					}
					num = checked(num + 1);
				}
				dgv.Refresh();
			}
		}

		private void miPaste_Click(object sender, EventArgs e)
		{
			this.PasteClipboard();
		}

		private void miCopy_Click(object sender, EventArgs e)
		{
			DataObject clipboardContent;
			if (this.TabMain.SelectedIndex == 0)
			{
				clipboardContent = this.dgv.GetClipboardContent();
			}
			else
			{
				TabContent tabContent = (TabContent)this.TabMain.SelectedTab;
				clipboardContent = tabContent.dgvnn.GetClipboardContent();
			}
			Clipboard.SetDataObject(clipboardContent);
		}

		private void loadData(object sender, EventArgs e)
		{
			string text = "";
			OpenFileDialog openFileDialog = new OpenFileDialog();
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			string text2 = "";
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			this.BEADat.ChangeGovAggregationLevel();
			openFileDialog.Title = "Select data to load";
			openFileDialog.InitialDirectory = MyProject.Computer.FileSystem.SpecialDirectories.MyDocuments + "\\IOSNAP";
			openFileDialog.Filter = "IO-SNAP data files (*.bdat)|*.bdat";
			openFileDialog.FilterIndex = 1;
			openFileDialog.RestoreDirectory = true;
			checked
			{
				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					text = openFileDialog.FileName;
					try
					{
						FileStream fileStream = new FileStream(text, FileMode.Open, FileAccess.Read);
						string s = Conversions.ToString(binaryFormatter.Deserialize(fileStream));
						byte[] bytes = Convert.FromBase64String(s);
						text2 = Encoding.ASCII.GetString(bytes);
						String2Beadata string2Beadata = new String2Beadata();
						BEAData bEAData = string2Beadata.String2B(ref text2);
						if (string2Beadata.isSuccess)
						{
							string text3 = " ";
							bool flag = true;
							int num = 1;
							string text4 = "";
							while (flag)
							{
								flag = true;
								text3 = bEAData.worksum.DataLabel;
								while (flag)
								{
									object value = false;
									int num2 = this.YearSelect_CBoxTS.Items.Count - 1;
									for (int i = 0; i <= num2; i++)
									{
										if (string.Compare(this.YearSelect_CBoxTS.Items[i].ToString(), text3) == 0)
										{
											value = true;
										}
									}
									if (Conversions.ToBoolean(value))
									{
										string text5 = Conversions.ToString(bEAData.worksum.DataTimeStamp);
										string[] array = text5.Split(' ');
										string text6 = text5.Replace("/", "");
										text6 = text6.Replace(":", "");
										text6 = text6.Replace(" ", "");
										text3 = bEAData.worksum.DataLabel + "_" + text6 + "_" + num.ToString();
										text3 = bEAData.worksum.DataLabel + "_" + array[0] + "_copy_" + num.ToString();
										num++;
									}
									else
									{
										flag = false;
									}
								}
							}
							this.DataCollection.addData(ref bEAData);
							this.YearSelect_CBoxTS.Items.Add(text3);
							bEAData.worksum.CurrentLabel = text3;
							this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
							this.TabClean();
							this.ShowSummary();
							this.MenuHandle.SetMenuStates();
						}
						fileStream.Close();
					}
					catch (Exception ex)
					{
						ProjectData.SetProjectError(ex);
						Exception ex2 = ex;
						Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
						ProjectData.ClearProjectError();
					}
					finally
					{
					}
					this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
					this.BEADat.ChangeGovAggregationLevel();
					this.ShowSummary();
				}
			}
		}

		private void SaveData(object sender, EventArgs e)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			this.BEADat.ChangeGovAggregationLevel();
			string s = Beadata2String.B2String(ref this.BEADat);
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.Title = "Enter Filename to Save Edited Data";
			saveFileDialog.InitialDirectory = MyProject.Computer.FileSystem.SpecialDirectories.MyDocuments + "\\IOSNAP";
			saveFileDialog.Filter = "IO-SNAP data files (*.bdat)|*.bdat";
			saveFileDialog.FilterIndex = 1;
			saveFileDialog.RestoreDirectory = true;
			if (saveFileDialog.ShowDialog() == DialogResult.OK)
			{
				string fileName = saveFileDialog.FileName;
				string text = saveFileDialog.FileName + "_s2";
				string text2 = saveFileDialog.FileName + "_s3";
				try
				{
					ASCIIEncoding aSCIIEncoding = new ASCIIEncoding();
					byte[] bytes = aSCIIEncoding.GetBytes(s);
					string graph = Convert.ToBase64String(bytes);
					FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
					binaryFormatter.Serialize(fileStream, graph);
					fileStream.Close();
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
			this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
			this.BEADat.ChangeGovAggregationLevel();
			this.ShowSummary();
		}

		private void miFixedPt_Click(object sender, EventArgs e)
		{
			this.options.@fixed = !this.options.@fixed;
			this.AdjustViewOptions();
		}

		private void miOpenInv_Click(object sender, EventArgs e)
		{
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			this.DispDat.BaseTable = this.BEADat.LeonOpen;
			this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
			this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
			this.DisplayDataExtTab("Open Inverse", DispTotal.Yes);
		}

		private unsafe void miClosedInv_Click(object sender, EventArgs e)
		{
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			this.DispDat.BaseTable = this.BEADat.LeonClosed;
			this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
            //TODO Need to check logic
            //ref string[] hdrsCol;
            //*(ref hdrsCol = ref this.DispDat.hdrsCol) = (string[])Utils.CopyArray(hdrsCol, new string[checked(this.BEADat.N_INDUSTRIES + 1 + 1)]);
            this.DispDat.hdrsCol = (string[])Utils.CopyArray(DispDat.hdrsCol, new string[checked(this.BEADat.N_INDUSTRIES + 1 + 1)]);
            this.DispDat.hdrsCol[checked(this.BEADat.N_INDUSTRIES + 1)] = "Household Expenditures";
			this.DispDat.hdrsRow = this.DispDat.hdrsCol;
			this.DisplayDataExtTab("Closed Inverse", DispTotal.Yes);
		}

		private void miB_Click(object sender, EventArgs e)
		{
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.BaseTable = this.BEADat.Bo;
			this.DispDat.hdrsRow = this.BEADat.hdrComm;
			this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
			this.DisplayDataExtTab("B", DispTotal.Yes);
		}

		private void miD_Click(object sender, EventArgs e)
		{
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.BaseTable = this.BEADat.Bo;
			this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
			this.DispDat.hdrsCol = this.BEADat.hdrComm;
			this.DisplayDataExtTab("D", DispTotal.Yes);
		}

		private void PrintDocument_Click(object sender, EventArgs e)
		{
			if (this.BEADat.UseAbbrHdrs)
			{
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.dgv.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
						dataGridViewColumn.Width = 65;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				this.dgv.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
			}
			this.MyPrintDocument = new PrintDocument();
			if (this.SetupThePrinting())
			{
				this.MyPrintDocument.Print();
			}
		}

		private void MyPrintDocument_PrintPage(object sender, PrintPageEventArgs e)
		{
			e.HasMorePages = this.MyDataGridViewPrinter.DrawDataGridView(e.Graphics);
		}

		private bool SetupThePrinting()
		{
			PrintDialog printDialog = new PrintDialog();
			this.MyPrintDocument.PrintPage += this.MyPrintDocument_PrintPage;
			PrintDialog printDialog2 = printDialog;
			printDialog2.AllowCurrentPage = false;
			printDialog2.AllowPrintToFile = false;
			printDialog2.AllowSelection = false;
			printDialog2.AllowSomePages = false;
			printDialog2.PrintToFile = false;
			printDialog2.ShowHelp = false;
			printDialog2.ShowNetwork = false;
			printDialog2 = null;
			if (printDialog.ShowDialog() != DialogResult.OK)
			{
				return false;
			}
			PrintDocument myPrintDocument = this.MyPrintDocument;
			myPrintDocument.DocumentName = "IO Data";
			myPrintDocument.PrinterSettings = printDialog.PrinterSettings;
			myPrintDocument.DefaultPageSettings = printDialog.PrinterSettings.DefaultPageSettings;
			myPrintDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);
			myPrintDocument.DefaultPageSettings.Landscape = true;
			myPrintDocument = null;
			bool centerOnPage = default(bool);
			this.MyDataGridViewPrinter = new DataGridViewPrinter(this.dgv, this.MyPrintDocument, centerOnPage, false, "", new Font("Tahoma", 18f, FontStyle.Bold, GraphicsUnit.Point), Color.Black, true, false);
			return true;
		}

		private unsafe void ImpactType_1_Click(object sender, EventArgs e)
		{
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			this.BEADat.ChangeGovAggregationLevel();
			this.BEADat.Calculate();
			this.DispDat = new DispData();
			this.DispDat.isAfterGRT = true;
			int industryCount = this.BEADat.NTables.getIndustryCount();
			Matrix matrix = new Matrix(industryCount, 2);
			checked
			{
				Matrix matrix2 = new Matrix(industryCount + 1, 2);
				int commodityCount = this.BEADat.NTables.getCommodityCount();
				Matrix matrix3 = new Matrix(commodityCount - 2, 2);
				Matrix matrix4 = new Matrix(commodityCount - 1, 2);
				double[] array = new double[industryCount + 1];
				Vector vector = new Vector(industryCount);
				Vector vector2 = new Vector(industryCount + 1);
				Vector vector3 = new Vector(commodityCount);
				Vector vector4 = new Vector(commodityCount + 1);
				dialogImpactEntryForm dialogImpactEntryForm;
				string[] array2;
				if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miImpactInd1", false))
				{
					dialogImpactEntryForm = new dialogImpactEntryForm();
					dialogImpactEntryForm.setDialogFrm();
					this.DispDat.isReadOnly = true;
					this.DispDat.hdrsCol = new string[1]
					{
						"Δg"
					};
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
					this.DispDat.BaseTable = Matrix.Inverse(Matrix.IMinus(this.BEADat.Ao)) * vector.toColMatrix();
					Vector vector5 = Matrix.RowSum(this.BEADat.FinDem);
					int num = industryCount - 1;
					for (int i = 0; i <= num; i++)
					{
						matrix[i, 1] = vector5[i];
					}
					dialogImpactEntryForm dialogImpactEntryForm2 = dialogImpactEntryForm;
					dialogImpactEntryForm2.data.hdrsCol = new string[2]
					{
						"Change in Final Demand (million $)",
						"Current Final Demand"
					};
					dialogImpactEntryForm dialogImpactEntryForm3 = dialogImpactEntryForm2;
					NatTables nTables;
					array2 = (nTables = this.BEADat.NTables).hdrInd;
					dialogImpactEntryForm3.setRowHeaders(ref array2);
					nTables.hdrInd = array2;
					dialogImpactEntryForm2.data.BaseTable = matrix;
					dialogImpactEntryForm2.data.mainHeader = "Type 1 Industry-Driven Impacts";
					dialogImpactEntryForm2 = null;
				}
				else if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miImpactInd2", false))
				{
					dialogImpactEntryForm = new dialogImpactEntryForm();
					dialogImpactEntryForm.setDialogFrm();
					this.DispDat.isReadOnly = true;
					this.DispDat.hdrsCol = new string[1]
					{
						"Δg"
					};
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
                    //ref string[] hdrsRow;
                    //*(ref hdrsRow = ref this.DispDat.hdrsRow) = unchecked((string[])Utils.CopyArray(hdrsRow, new string[checked(this.BEADat.NTables.hdrInd.Length + 1 + 1)]));
                    this.DispDat.hdrsRow = unchecked((string[])Utils.CopyArray(DispDat.hdrsRow, new string[checked(this.BEADat.NTables.hdrInd.Length + 1 + 1)]));
                    this.DispDat.hdrsRow[this.DispDat.hdrsRow.Length - 2] = "Households";
					this.DispDat.BaseTable = Matrix.Inverse(Matrix.IMinus(this.BEADat.Ac)) * vector2.toColMatrix();
					Vector vector6 = Matrix.RowSum(this.BEADat.FinDem);
					int num2 = industryCount - 1;
					for (int j = 0; j <= num2; j++)
					{
						matrix2[j, 1] = vector6[j];
					}
					dialogImpactEntryForm dialogImpactEntryForm4 = dialogImpactEntryForm;
					dialogImpactEntryForm4.data.hdrsCol = new string[2]
					{
						"Change in Final Demand (million $)",
						"Current Final Demand"
					};
					dialogImpactEntryForm dialogImpactEntryForm5 = dialogImpactEntryForm4;
					NatTables nTables;
					array2 = (nTables = this.BEADat.NTables).hdrInd;
					dialogImpactEntryForm5.setRowHeaders(ref array2);
					nTables.hdrInd = array2;
					dialogImpactEntryForm4.data.hdrsRow.Add("Households");
					dialogImpactEntryForm4.data.BaseTable = matrix2;
					dialogImpactEntryForm4.data.mainHeader = "Type 2 Industry-Driven Impacts";
					dialogImpactEntryForm4 = null;
				}
				else if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miImpactComm1", false))
				{
					this.DispDat.isReadOnly = true;
					this.DispDat.hdrsRow = this.BEADat.hdrComm;
					Matrix mat = Matrix.InvI_BD(this.BEADat.Bo, this.BEADat.Wo) * vector3.toColMatrix();
					this.DispDat.BaseTable = this.BEADat.Wo * mat;
					dialogImpactEntryForm = new dialogImpactEntryForm();
					dialogImpactEntryForm.setDialogFrm();
					Vector vector7 = Matrix.RowSum(this.BEADat.FinDem);
					int num3 = commodityCount - 3;
					for (int k = 0; k <= num3; k++)
					{
						matrix3[k, 1] = vector7[k];
					}
					dialogImpactEntryForm dialogImpactEntryForm6 = dialogImpactEntryForm;
					dialogImpactEntryForm6.data.hdrsCol = new string[2]
					{
						"Change in Final Demand (million $)",
						"Current Final Demand"
					};
					dialogImpactEntryForm dialogImpactEntryForm7 = dialogImpactEntryForm6;
					BEAData bEADat;
					array2 = (bEADat = this.BEADat).hdrComm;
					dialogImpactEntryForm7.setRowHeaders(ref array2);
					bEADat.hdrComm = array2;
					dialogImpactEntryForm6.data.BaseTable = matrix3;
					dialogImpactEntryForm6.data.mainHeader = "Type 1 Commodity-Driven Impacts";
					dialogImpactEntryForm6 = null;
				}
				else if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miImpactComm2", false))
				{
					this.DispDat.hdrsRow = this.BEADat.hdrComm;
					//ref string[] hdrsRow;
                    //*(ref hdrsRow = ref this.DispDat.hdrsRow) = unchecked((string[])Utils.CopyArray(hdrsRow, new string[checked(this.BEADat.hdrComm.Length + 1 + 1)]));
                    this.DispDat.hdrsRow = unchecked((string[])Utils.CopyArray(DispDat.hdrsRow, new string[checked(this.BEADat.hdrComm.Length + 1 + 1)]));
                    this.DispDat.hdrsRow[this.DispDat.hdrsRow.Length - 2] = this.DispDat.hdrsRow[this.DispDat.hdrsRow.Length - 3];
					this.DispDat.hdrsRow[this.DispDat.hdrsRow.Length - 3] = this.DispDat.hdrsRow[this.DispDat.hdrsRow.Length - 4];
					this.DispDat.hdrsRow[this.DispDat.hdrsRow.Length - 4] = "Households";
					Matrix matrix5 = unchecked((Matrix)Operators.MultiplyObject(this.BEADat.Bc, 1));
					int num4 = matrix5.NoRows - 1;
					for (int l = 0; l <= num4; l++)
					{
						matrix5[l, matrix5.NoCols - 1] = unchecked(matrix5[l, checked(matrix5.NoCols - 1)] * 1.0);
					}
					Vector vector8;
					unchecked
					{
						Matrix mat2 = Matrix.InvI_BD(matrix5, (Matrix)this.BEADat.Wc) * vector4.toColMatrix();
						this.DispDat.BaseTable = (Matrix)this.BEADat.Wc * mat2;
						dialogImpactEntryForm = new dialogImpactEntryForm();
						dialogImpactEntryForm.setDialogFrm();
						vector8 = Matrix.RowSum(this.BEADat.FinDem);
					}
					int num5 = commodityCount - 3;
					for (int m = 0; m <= num5; m++)
					{
						matrix4[m, 1] = vector8[m];
					}
					dialogImpactEntryForm dialogImpactEntryForm8 = dialogImpactEntryForm;
					dialogImpactEntryForm8.data.hdrsCol = new string[2]
					{
						"Change in Final Demand (million $)",
						"Current Final Demand"
					};
					dialogImpactEntryForm dialogImpactEntryForm9 = dialogImpactEntryForm8;
					BEAData bEADat;
					array2 = (bEADat = this.BEADat).hdrComm;
					dialogImpactEntryForm9.setRowHeaders(ref array2);
					bEADat.hdrComm = array2;
					dialogImpactEntryForm8.data.hdrsRow[this.BEADat.hdrComm.Length - 2] = "Households";
					dialogImpactEntryForm8.data.BaseTable = matrix4;
					dialogImpactEntryForm8.data.mainHeader = "Type 2 Commodity-Driven Impacts";
					dialogImpactEntryForm8 = null;
				}
				else
				{
					dialogImpactEntryForm = new dialogImpactEntryForm();
					dialogImpactEntryForm.setDialogFrm();
					Interaction.MsgBox(Operators.ConcatenateObject("Unknown Impact method:  ", NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null)), MsgBoxStyle.OkOnly, null);
				}
				if (dialogImpactEntryForm.ShowDialog() == DialogResult.OK)
				{
					if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miImpactInd1", false))
					{
						Matrix matrix6 = new Matrix(industryCount, 1);
						Matrix matrix7 = new Matrix(industryCount, 6);
						this.BEADat.deltaFDInd = new Matrix(industryCount, 1);
						int num6 = industryCount - 1;
						for (int n = 0; n <= num6; n++)
						{
							this.BEADat.deltaFDInd[n, 0] = dialogImpactEntryForm.data.BaseTable[n, 0];
							matrix6[n, 0] = dialogImpactEntryForm.data.BaseTable[n, 0];
						}
						this.BEADat.IndImpactType1();
						int num7 = industryCount - 1;
						for (int num8 = 0; num8 <= num7; num8++)
						{
							matrix7[num8, 0] = this.BEADat.dgIndImp[num8, 0];
							matrix7[num8, 1] = this.BEADat.dgIndImpEMP[num8, 0];
							matrix7[num8, 2] = this.BEADat.dgIndImpEC[num8, 0];
							matrix7[num8, 3] = this.BEADat.dgIndImpT[num8, 0];
							matrix7[num8, 4] = this.BEADat.dgIndImpGOS[num8, 0];
							matrix7[num8, 5] = this.BEADat.dgIndImpVA[num8, 0];
						}
						this.DispDat.hdrsCol = new string[1]
						{
							"Final Demand Impacts (million $)"
						};
						this.DispDat.hdrsXCols = new string[6]
						{
							"Industry Output Impacts",
							"Employment Impacts",
							"Employee Compensation Impacts",
							"Tax Impacts",
							"Gross Operating Surplus Impacts",
							"Total Value-Added Impacts"
						};
						this.DispDat.hasEmployment = true;
						this.DispDat.BaseTable = matrix6;
						this.DispDat.ExtraCols = matrix7;
						object obj = new Matrix(1, 7);
						Vector vector9 = Matrix.ColSum(matrix6);
						NewLateBinding.LateIndexSet(obj, new object[3]
						{
							0,
							0,
							vector9[0]
						}, null);
						vector9 = Matrix.ColSum(matrix7);
						NewLateBinding.LateIndexSet(obj, new object[3]
						{
							0,
							1,
							vector9[0]
						}, null);
						NewLateBinding.LateIndexSet(obj, new object[3]
						{
							0,
							2,
							vector9[1]
						}, null);
						NewLateBinding.LateIndexSet(obj, new object[3]
						{
							0,
							3,
							vector9[2]
						}, null);
						NewLateBinding.LateIndexSet(obj, new object[3]
						{
							0,
							4,
							vector9[3]
						}, null);
						NewLateBinding.LateIndexSet(obj, new object[3]
						{
							0,
							5,
							vector9[4]
						}, null);
						NewLateBinding.LateIndexSet(obj, new object[3]
						{
							0,
							6,
							vector9[5]
						}, null);
						this.DispDat.ExtraRows = unchecked((Matrix)obj);
						this.DispDat.hdrsXRows = new string[1];
						this.DispDat.hdrsXRows[0] = "Column Sum";
						this.DisplayDataExtTab(dialogImpactEntryForm.data.mainHeader, DispTotal.Unchanged);
					}
					else if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miImpactComm1", false))
					{
						Matrix matrix6 = new Matrix(commodityCount, 2);
						Matrix matrix7 = new Matrix(commodityCount - 2, 6);
						this.BEADat.deltaFDComm = new Matrix(commodityCount, 1);
						int num9 = commodityCount - 3;
						for (int num10 = 0; num10 <= num9; num10++)
						{
							this.BEADat.deltaFDComm[num10, 0] = dialogImpactEntryForm.data.BaseTable[num10, 0];
							matrix6[num10, 0] = dialogImpactEntryForm.data.BaseTable[num10, 0];
						}
						this.BEADat.CommImpact_Type1();
						int num11 = commodityCount - 1;
						for (int num12 = 0; num12 <= num11; num12++)
						{
							matrix6[num12, 1] = this.BEADat.dqCommImp[num12, 0];
						}
						int num13 = commodityCount - 3;
						for (int num14 = 0; num14 <= num13; num14++)
						{
							matrix7[num14, 0] = this.BEADat.dgCommImp[num14, 0];
							matrix7[num14, 1] = this.BEADat.dgCommImpEMP[num14, 0];
							matrix7[num14, 2] = this.BEADat.dgCommImpEC[num14, 0];
							matrix7[num14, 3] = this.BEADat.dgCommImpT[num14, 0];
							matrix7[num14, 4] = this.BEADat.dgCommImpGOS[num14, 0];
							matrix7[num14, 5] = this.BEADat.dgCommImpVA[num14, 0];
						}
						this.DispDat.hdrsCol = new string[2]
						{
							"Final Demand Impacts (million $)",
							"Commodity Output Impacts"
						};
						this.DispDat.hdrsXCols = new string[6]
						{
							"Industry Output Impacts",
							"Employment Impacts",
							"Employee Compensation Impacts",
							"Tax Impacts",
							"Gross Operating Surplus Impacts",
							"Total Value-Added Impacts"
						};
						this.DispDat.hasEmployment = true;
						this.DispDat.BaseTable = matrix6;
						this.DispDat.ExtraCols = matrix7;
						object obj2 = new Matrix(1, 8);
						Vector vector10 = Matrix.ColSum(matrix6);
						NewLateBinding.LateIndexSet(obj2, new object[3]
						{
							0,
							0,
							vector10[0]
						}, null);
						NewLateBinding.LateIndexSet(obj2, new object[3]
						{
							0,
							1,
							vector10[1]
						}, null);
						vector10 = Matrix.ColSum(matrix7);
						NewLateBinding.LateIndexSet(obj2, new object[3]
						{
							0,
							2,
							vector10[0]
						}, null);
						NewLateBinding.LateIndexSet(obj2, new object[3]
						{
							0,
							3,
							vector10[1]
						}, null);
						NewLateBinding.LateIndexSet(obj2, new object[3]
						{
							0,
							4,
							vector10[2]
						}, null);
						NewLateBinding.LateIndexSet(obj2, new object[3]
						{
							0,
							5,
							vector10[3]
						}, null);
						NewLateBinding.LateIndexSet(obj2, new object[3]
						{
							0,
							6,
							vector10[4]
						}, null);
						NewLateBinding.LateIndexSet(obj2, new object[3]
						{
							0,
							7,
							vector10[5]
						}, null);
						this.DispDat.ExtraRows = unchecked((Matrix)obj2);
						this.DispDat.hdrsXRows = new string[1];
						this.DispDat.hdrsXRows[0] = "Column Sum";
						this.DisplayDataExtTab(dialogImpactEntryForm.data.mainHeader, DispTotal.No);
					}
					else if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miImpactComm2", false))
					{
						this.DispDat.isReadOnly = true;
						Matrix matrix6 = new Matrix(commodityCount + 1, 2);
						Matrix matrix7 = new Matrix(commodityCount - 2 + 1, 6);
						this.BEADat.deltaFDComm = new Matrix(commodityCount, 1);
						int num15 = commodityCount - 3;
						for (int num16 = 0; num16 <= num15; num16++)
						{
							this.BEADat.deltaFDComm[num16, 0] = dialogImpactEntryForm.data.BaseTable[num16, 0];
							matrix6[num16, 0] = dialogImpactEntryForm.data.BaseTable[num16, 0];
						}
						this.DD = dialogImpactEntryForm.data.BaseTable[commodityCount - 2, 0];
						matrix6[commodityCount - 2, 0] = dialogImpactEntryForm.data.BaseTable[commodityCount - 2, 0];
						this.BEADat.CommImpact_Type2();
						int num17 = commodityCount - 3;
						for (int num18 = 0; num18 <= num17; num18++)
						{
							matrix6[num18, 1] = this.BEADat.dqCommImp[num18, 0];
						}
						matrix6[commodityCount - 2, 1] = this.BEADat.dqCommImp[commodityCount, 0];
						matrix6[commodityCount - 1, 1] = this.BEADat.dqCommImp[commodityCount - 2, 0];
						matrix6[commodityCount, 1] = this.BEADat.dqCommImp[commodityCount - 1, 0];
						int num19 = commodityCount - 3;
						for (int num20 = 0; num20 <= num19; num20++)
						{
							matrix7[num20, 0] = this.BEADat.dgCommImp[num20, 0];
							matrix7[num20, 1] = this.BEADat.dgCommImpEMP[num20, 0];
							matrix7[num20, 2] = this.BEADat.dgCommImpEC[num20, 0];
							matrix7[num20, 3] = this.BEADat.dgCommImpT[num20, 0];
							matrix7[num20, 4] = this.BEADat.dgCommImpGOS[num20, 0];
							matrix7[num20, 5] = this.BEADat.dgCommImpVA[num20, 0];
						}
						this.DispDat.hdrsCol = new string[2]
						{
							"Final Demand Impacts (million $)",
							"Commodity Output Impacts"
						};
						this.DispDat.hdrsXCols = new string[6]
						{
							"Industry Output Impacts",
							"Employment Impacts",
							"Employment Compensation",
							"Tax Impacts",
							"Gross Operating Surplus Impacts",
							"Total Value-Added Impacts"
						};
						this.DispDat.BaseTable = matrix6;
						this.DispDat.ExtraCols = matrix7;
						object obj3 = new Matrix(1, 8);
						Vector vector11 = Matrix.ColSum(matrix6);
						NewLateBinding.LateIndexSet(obj3, new object[3]
						{
							0,
							0,
							vector11[0]
						}, null);
						unchecked
						{
							NewLateBinding.LateIndexSet(obj3, new object[3]
							{
								0,
								1,
								vector11[1] - matrix6[checked(commodityCount - 2), 1]
							}, null);
							vector11 = Matrix.ColSum(matrix7);
							NewLateBinding.LateIndexSet(obj3, new object[3]
							{
								0,
								2,
								vector11[0]
							}, null);
							NewLateBinding.LateIndexSet(obj3, new object[3]
							{
								0,
								3,
								vector11[1]
							}, null);
							NewLateBinding.LateIndexSet(obj3, new object[3]
							{
								0,
								4,
								vector11[2]
							}, null);
							NewLateBinding.LateIndexSet(obj3, new object[3]
							{
								0,
								5,
								vector11[3]
							}, null);
							NewLateBinding.LateIndexSet(obj3, new object[3]
							{
								0,
								6,
								vector11[4]
							}, null);
							NewLateBinding.LateIndexSet(obj3, new object[3]
							{
								0,
								7,
								vector11[5]
							}, null);
							this.DispDat.ExtraRows = (Matrix)obj3;
							this.DispDat.hdrsXRows = new string[1];
							this.DispDat.hdrsXRows[0] = "Column Sum (excluding Households)";
							this.DisplayDataExtTab(dialogImpactEntryForm.data.mainHeader, DispTotal.No);
						}
					}
					else if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miImpactInd2", false))
					{
						Matrix matrix6 = new Matrix(industryCount + 1, 1);
						Matrix matrix7 = new Matrix(industryCount + 1, 6);
						this.BEADat.deltaFDInd = new Matrix(industryCount + 1, 1);
						int num21 = industryCount;
						for (int num22 = 0; num22 <= num21; num22++)
						{
							this.BEADat.deltaFDInd[num22, 0] = dialogImpactEntryForm.data.BaseTable[num22, 0];
							matrix6[num22, 0] = dialogImpactEntryForm.data.BaseTable[num22, 0];
						}
						this.BEADat.IndImpactType2();
						int num23 = industryCount;
						for (int num24 = 0; num24 <= num23; num24++)
						{
							matrix7[num24, 0] = this.BEADat.dgIndImp[num24, 0];
							matrix7[num24, 1] = this.BEADat.dgIndImpEMP[num24, 0];
							matrix7[num24, 2] = this.BEADat.dgIndImpEC[num24, 0];
							matrix7[num24, 3] = this.BEADat.dgIndImpT[num24, 0];
							matrix7[num24, 4] = this.BEADat.dgIndImpGOS[num24, 0];
							matrix7[num24, 5] = unchecked(this.BEADat.dgIndImpGOS[num24, 0] + this.BEADat.dgIndImpT[num24, 0] + this.BEADat.dgIndImpEC[num24, 0]);
						}
						this.DispDat.hdrsCol = new string[1]
						{
							"Final Demand Impacts (million $)"
						};
						this.DispDat.hdrsXCols = new string[6]
						{
							"Industry Output Impacts",
							"Employment Impacts",
							"Employment Compensation",
							"Tax Impacts",
							"Gross Operating Surplus Impacts",
							"Total Value-Added Impacts"
						};
						this.DispDat.hasEmployment = true;
						this.DispDat.BaseTable = matrix6;
						this.DispDat.ExtraCols = matrix7;
						object obj4 = new Matrix(1, 7);
						Vector vector12 = Matrix.ColSum(matrix6);
						NewLateBinding.LateIndexSet(obj4, new object[3]
						{
							0,
							0,
							vector12[0]
						}, null);
						vector12 = Matrix.ColSum(matrix7);
						unchecked
						{
							NewLateBinding.LateIndexSet(obj4, new object[3]
							{
								0,
								1,
								vector12[0] - matrix7[industryCount, 0]
							}, null);
							NewLateBinding.LateIndexSet(obj4, new object[3]
							{
								0,
								2,
								vector12[1]
							}, null);
							NewLateBinding.LateIndexSet(obj4, new object[3]
							{
								0,
								3,
								vector12[2]
							}, null);
							NewLateBinding.LateIndexSet(obj4, new object[3]
							{
								0,
								4,
								vector12[3]
							}, null);
							NewLateBinding.LateIndexSet(obj4, new object[3]
							{
								0,
								5,
								vector12[4]
							}, null);
							NewLateBinding.LateIndexSet(obj4, new object[3]
							{
								0,
								6,
								vector12[5]
							}, null);
							this.DispDat.ExtraRows = (Matrix)obj4;
							this.DispDat.hdrsXRows = new string[1];
							this.DispDat.hdrsXRows[0] = "Column Sum (excluding Households)";
							this.DisplayDataExtTab(dialogImpactEntryForm.data.mainHeader, DispTotal.No);
						}
					}
				}
				this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
				this.BEADat.ChangeGovAggregationLevel();
				this.ShowSummary();
			}
		}

		private unsafe void miShowAllMults_Click(object sender, EventArgs e)
		{
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			this.DispDat.hdrsCol = new string[7]
			{
				"IxI Output",
				"IxI Income",
				"IxI Employment",
				"IxC Output",
				"IxC Income",
				"IxC Employment",
				"CxC Output"
			};
			this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
			Vector col = new Vector(0);
			string frmTitle;
			if (Operators.ConditionalCompareObjectEqual(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null), "miMultiplierso", false))
			{
				frmTitle = "Type I Multipliers";
				this.DispDat.BaseTable = this.BEADat.IxIOutputo().toRowMatrix();
				this.DispDat.BaseTable.Append(new Vector(1), new Vector(0));
				this.DispDat.BaseTable.Append(new Vector(1), new Vector(0));
				Matrix baseTable = this.DispDat.BaseTable;
				baseTable.Append(col, this.BEADat.IxIInco());
				baseTable.Append(col, this.BEADat.IxIEmpo());
				baseTable.Append(col, this.BEADat.IxCOutputo());
				baseTable.Append(col, this.BEADat.IxCInco());
				baseTable.Append(col, this.BEADat.IxCEmpo());
				baseTable.Append(col, this.BEADat.CxCOutputo());
				baseTable = null;
				double[,] toArray = this.DispDat.BaseTable.toArray;
				toArray = (double[,])Utils.CopyArray(toArray, new double[7, checked(this.BEADat.N_INDUSTRIES + 1)]);
				this.DispDat.hasEmployment = true;
				this.DispDat.BaseTable = new Matrix(toArray);
			}
			else
			{
				frmTitle = "Type II Multipliers";
				this.DispDat.hdrsRow = (string[])this.DispDat.hdrsRow.Clone();
                //ref string[] hdrsRow;
                //*(ref hdrsRow = ref this.DispDat.hdrsRow) = (string[])Utils.CopyArray(hdrsRow, new string[checked(this.BEADat.N_INDUSTRIES + 1 + 1)]);
                this.DispDat.hdrsRow = (string[])Utils.CopyArray(DispDat.hdrsRow, new string[checked(this.BEADat.N_INDUSTRIES + 1 + 1)]);
                double[,] toArray2;
				checked
				{
					this.DispDat.hdrsRow[this.BEADat.N_INDUSTRIES + 1] = "Households";
					this.DispDat.BaseTable = this.BEADat.IxIOutputc().toRowMatrix();
					this.DispDat.BaseTable.Append(new Vector(1), new Vector(0));
					this.DispDat.BaseTable.Append(new Vector(1), new Vector(0));
					this.DispDat.hasEmployment = true;
					Matrix baseTable2 = this.DispDat.BaseTable;
					baseTable2.Append(col, this.BEADat.IxIIncc());
					baseTable2.Append(col, this.BEADat.IxIEmpc());
					baseTable2.Append(col, this.BEADat.IxCOutputc());
					baseTable2.Append(col, this.BEADat.IxCIncc());
					baseTable2.Append(col, this.BEADat.IxCEmpc());
					baseTable2.Append(col, this.BEADat.CxCOutputc());
					baseTable2 = null;
					int num = 3;
					do
					{
						this.DispDat.BaseTable[num, -3] = this.DispDat.BaseTable[num, -1];
						num++;
					}
					while (num <= 6);
					toArray2 = this.DispDat.BaseTable.toArray;
				}
				toArray2 = (double[,])Utils.CopyArray(toArray2, new double[7, checked(this.BEADat.N_COMMODITIES - 1 + 1)]);
				this.DispDat.BaseTable = new Matrix(toArray2);
			}
			this.DispDat.BaseTable = Matrix.Transpose(this.DispDat.BaseTable);
			this.DisplayDataExtTab(frmTitle, DispTotal.No);
		}

		private void DataReq_Click(object sender, EventArgs e)
		{
			string text = Conversions.ToString(NewLateBinding.LateGet(sender, null, "name", new object[0], null, null, null));
			this.DisplayTotDirRT(ref text);
			NewLateBinding.LateSetComplex(sender, null, "name", new object[1]
			{
				text
			}, null, null, true, false);
		}

		private unsafe void DisplayTotDirRT(ref string sname)
		{
			this.DispDat = new DispData();
			this.DispDat.isReadOnly = true;
			this.DispDat.isAfterGRT = true;
			string text = sname;
			string frmTitle = "";
			checked
			{
				if (Operators.CompareString(text.Substring(text.Length - 3, 1), "I", false) == 0)
				{
					this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
				}
				else
				{
					this.DispDat.hdrsRow = this.BEADat.hdrComm;
				}
				if (text.EndsWith("I"))
				{
					this.DispDat.hdrsCol = this.BEADat.NTables.hdrInd;
				}
				else
				{
					this.DispDat.hdrsCol = this.BEADat.hdrComm;
				}
				if (Operators.CompareString(Conversions.ToString(text[text.Length - 4]), "c", false) == 0)
				{
					unchecked
					{
						this.DispDat.hdrsCol = (string[])this.DispDat.hdrsCol.Clone();
						this.DispDat.hdrsRow = (string[])this.DispDat.hdrsRow.Clone();
                        //ref string[] hdrsCol;
                        //*(ref hdrsCol = ref this.DispDat.hdrsCol) = (string[])Utils.CopyArray(hdrsCol, new string[checked(this.DispDat.hdrsCol.Length + 1)]);
                        //*(ref hdrsCol = ref this.DispDat.hdrsRow) = (string[])Utils.CopyArray(hdrsCol, new string[checked(this.DispDat.hdrsRow.Length + 1)]);
                        this.DispDat.hdrsCol = (string[])Utils.CopyArray(DispDat.hdrsCol, new string[checked(this.DispDat.hdrsCol.Length + 1)]);
                        this.DispDat.hdrsRow = (string[])Utils.CopyArray(DispDat.hdrsRow, new string[checked(this.DispDat.hdrsRow.Length + 1)]);
                    }
					this.DispDat.hdrsCol[this.DispDat.hdrsCol.Length - 1] = "Households";
					this.DispDat.hdrsRow[this.DispDat.hdrsRow.Length - 1] = "Households";
				}
				switch (BEAData.ComputeStringHash(text))
				{
				case 2301315148u:
					if (Operators.CompareString(text, "miDRToIxI", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.Ao;
						frmTitle = "Direct Requirements Table Open I x I";
					}
					break;
				case 1433653468u:
					if (Operators.CompareString(text, "miDRToCxC", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.CxC_DRTo;
						frmTitle = "Direct Requirements Table Open C x C";
					}
					break;
				case 2469091338u:
					if (Operators.CompareString(text, "miDRToIxC", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.IxC_DRTo;
						frmTitle = "Direct Requirements Table Open I x C";
					}
					break;
				case 815937806u:
					if (Operators.CompareString(text, "miDRTcIxC", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.IxC_DRTc;
						frmTitle = "Direct Requirements Table Closed I x C";
					}
					break;
				case 648161616u:
					if (Operators.CompareString(text, "miDRTcIxI", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.Ac;
						frmTitle = "Direct Requirements Table Closed I x I";
					}
					break;
				case 1969508704u:
					if (Operators.CompareString(text, "miDRTcCxC", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.CxC_DRTc;
						frmTitle = "Direct Requirements Table Closed C x C";
					}
					break;
				case 2539732074u:
					if (Operators.CompareString(text, "miTRoCxC", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.CxC_TRo;
						frmTitle = "Total Requirements Table Open C x C";
					}
					break;
				case 3651829436u:
					if (Operators.CompareString(text, "miTRoIxC", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.IxC_TRo;
						frmTitle = "Total Requirements Table Open I x C";
					}
					break;
				case 3819605626u:
					if (Operators.CompareString(text, "miTRoIxI", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.LeonOpen;
						frmTitle = "Total Requirements Table Open I x I";
					}
					break;
				case 3358515302u:
					if (Operators.CompareString(text, "miTRcCxC", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.CxC_TRc;
						frmTitle = "Total Requirements Table Closed C x C";
					}
					break;
				case 980357000u:
					if (Operators.CompareString(text, "miTRcIxC", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.IxC_TRc;
						frmTitle = "Total Requirements Table Closed I x C";
					}
					break;
				case 879691286u:
					if (Operators.CompareString(text, "miTRcIxI", false) == 0)
					{
						this.DispDat.BaseTable = this.BEADat.LeonClosed;
						frmTitle = "Total Requirements Table Closed I x I";
					}
					break;
				}
				this.DispDat.useGeneralFormatOnly = false;
				int[] array = new int[this.DispDat.hdrsCol.Length + 1];
				int num = array.Length - 1;
				for (int i = 0; i <= num; i++)
				{
					array[i] = 2;
				}
				this.DispDat.DataFormatTable = array;
				this.DisplayDataExtTab(frmTitle, DispTotal.Yes);
				this.T_Direct();
			}
		}

		private void miInputs_Click(object sender, EventArgs e)
		{
			Interaction.MsgBox("In development", MsgBoxStyle.OkOnly, null);
		}

		private void WorkspaceSummaryToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.TabMain.SelectTab(0);
			this.ShowSummary();
		}

		private void ShowSummary()
		{
			int width = 350;
			this.dgv.CellValueChanged -= this.dgv_CellValueChanged;
			usrDataGridView dgv = this.dgv;
			dgv.Columns.Clear();
			dgv.DefaultCellStyle.Format = this.options.getGeneralFormat;
			dgv.RowHeadersVisible = true;
			dgv.Columns.Add("", "");
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = dgv.Columns.GetEnumerator();
				while (enumerator.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
					dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			DataGridViewRow dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow2 = dataGridViewRow;
			dataGridViewRow2.CreateCells(this.dgv);
			dataGridViewRow2.HeaderCell.Value = "Data label";
			dataGridViewRow2.Cells[0].Value = this.BEADat.worksum.DataLabel;
			dataGridViewRow2 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow3 = dataGridViewRow;
			dataGridViewRow3.CreateCells(this.dgv);
			dataGridViewRow3.HeaderCell.Value = "Year";
			dataGridViewRow3.Cells[0].Value = this.BEADat.worksum.datayear;
			dataGridViewRow3 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow4 = dataGridViewRow;
			dataGridViewRow4.CreateCells(this.dgv);
			dataGridViewRow4.HeaderCell.Value = "Data type";
			dataGridViewRow4.Cells[0].Value = Operators.ConcatenateObject(Operators.ConcatenateObject(Operators.ConcatenateObject(this.BEADat.worksum.getDataType(), " ("), this.BEADat.worksum.DataType), ")");
			dataGridViewRow4 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow5 = dataGridViewRow;
			dataGridViewRow5.CreateCells(this.dgv);
			dataGridViewRow5.HeaderCell.Value = "Are data regionalized";
			dataGridViewRow5.Cells[0].Value = this.BEADat.worksum.dataregionalized;
			dataGridViewRow5 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow6 = dataGridViewRow;
			dataGridViewRow6.CreateCells(this.dgv);
			dataGridViewRow6.HeaderCell.Value = "Regionalization method";
			dataGridViewRow6.Cells[0].Value = this.BEADat.worksum.regmethod;
			dataGridViewRow6 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow7 = dataGridViewRow;
			dataGridViewRow7.CreateCells(this.dgv);
			dataGridViewRow7.HeaderCell.Value = "Regionalized by region";
			dataGridViewRow7.Cells[0].Value = this.BEADat.worksum.regby;
			dataGridViewRow7 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow8 = dataGridViewRow;
			dataGridViewRow8.CreateCells(this.dgv);
			dataGridViewRow8.HeaderCell.Value = "Requirement tables generated";
			dataGridViewRow8.Cells[0].Value = this.BEADat.worksum.requirementsTables;
			dataGridViewRow8 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow9 = dataGridViewRow;
			dataGridViewRow9.CreateCells(this.dgv);
			dataGridViewRow9.HeaderCell.Value = "Number of industries ";
			int num = 0;
			num = this.BEADat.N_INDUSTRIES;
			dataGridViewRow9.Cells[0].Value = (Conversions.ToString(num) ?? "");
			dataGridViewRow9 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow10 = dataGridViewRow;
			dataGridViewRow10.CreateCells(this.dgv);
			dataGridViewRow10.HeaderCell.Value = "Number of commodities ";
			int n_COMMODITIES = this.BEADat.N_COMMODITIES;
			dataGridViewRow10.Cells[0].Value = (Conversions.ToString(n_COMMODITIES) ?? "");
			dataGridViewRow10 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow11 = dataGridViewRow;
			dataGridViewRow11.CreateCells(this.dgv);
			dataGridViewRow11.HeaderCell.Value = "Are data aggregated ";
			dataGridViewRow11.Cells[0].Value = this.BEADat.worksum.dataaggregated;
			dataGridViewRow11 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow12 = dataGridViewRow;
			dataGridViewRow12.CreateCells(this.dgv);
			dataGridViewRow12.HeaderCell.Value = "Method of government data aggregation";
			switch (this.BEADat.GovernmentAggStatus.getAggLevel())
			{
			case GenDataStatus.dataStatusType.FedStateLocal:
				dataGridViewRow12.Cells[0].Value = "Federal and State with Local";
				break;
			case GenDataStatus.dataStatusType.ImputedGov:
				dataGridViewRow12.Cells[0].Value = "Imputed IO";
				break;
			case GenDataStatus.dataStatusType.TotalGovernment:
				dataGridViewRow12.Cells[0].Value = "Total Government";
				break;
			default:
				dataGridViewRow12.Cells[0].Value = "Unknown";
				break;
			}
			dataGridViewRow12 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow13 = dataGridViewRow;
			dataGridViewRow13.CreateCells(this.dgv);
			dataGridViewRow13.HeaderCell.Value = "Employment units ";
			dataGridViewRow13.Cells[0].Value = RuntimeHelpers.GetObjectValue(this.BEADat.worksum.getEmploymentStatus());
			dataGridViewRow13 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow14 = dataGridViewRow;
			dataGridViewRow14.CreateCells(this.dgv);
			dataGridViewRow14.HeaderCell.Value = "Include table headers when copying data";
			dataGridViewRow14.Cells[0].Value = this.copyHeadersStatus;
			dataGridViewRow14 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow15 = dataGridViewRow;
			dataGridViewRow15.CreateCells(this.dgv);
			dataGridViewRow15.HeaderCell.Value = "Data version number";
			dataGridViewRow15.Cells[0].Value = this.BEADat.worksum.DataVersion;
			dataGridViewRow15 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow16 = dataGridViewRow;
			dataGridViewRow16.CreateCells(this.dgv);
			dataGridViewRow16.HeaderCell.Value = "Data timestamp";
			dataGridViewRow16.Cells[0].Value = RuntimeHelpers.GetObjectValue(this.BEADat.worksum.DataTimeStamp);
			dataGridViewRow16 = null;
			dgv.Rows.Add(dataGridViewRow);
			dgv.Rows.Add();
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow17 = dataGridViewRow;
			dataGridViewRow17.CreateCells(this.dgv);
			dataGridViewRow17.HeaderCell.Value = "Financial Units";
			dataGridViewRow17.Cells[0].Value = "$M";
			dataGridViewRow17 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow18 = dataGridViewRow;
			dataGridViewRow18.CreateCells(this.dgv);
			dataGridViewRow18.HeaderCell.Value = "Employment Units";
			dataGridViewRow18.Cells[0].Value = "Ones";
			dataGridViewRow18 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow19 = dataGridViewRow;
			dataGridViewRow19.CreateCells(this.dgv);
			dataGridViewRow19.HeaderCell.Value = "Compensation Units";
			dataGridViewRow19.Cells[0].Value = "$M";
			dataGridViewRow19 = null;
			dgv.Rows.Add(dataGridViewRow);
			dataGridViewRow = new DataGridViewRow();
			DataGridViewRow dataGridViewRow20 = dataGridViewRow;
			dataGridViewRow20.CreateCells(this.dgv);
			dataGridViewRow20.HeaderCell.Value = "Compensation Rates";
			dataGridViewRow20.Cells[0].Value = "$/Employee";
			dataGridViewRow20 = null;
			dgv.Rows.Add(dataGridViewRow);
			dgv.Rows.Add();
			IEnumerator enumerator2 = default(IEnumerator);
			try
			{
				enumerator2 = dgv.Columns.GetEnumerator();
				while (enumerator2.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn2 = (DataGridViewColumn)enumerator2.Current;
					dataGridViewColumn2.Width = width;
				}
			}
			finally
			{
				if (enumerator2 is IDisposable)
				{
					(enumerator2 as IDisposable).Dispose();
				}
			}
			dgv.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
			dgv = null;
			this.bDataDisplayed = true;
			this.MenuHandle.SetMenuStates();
			this.Table_Label.Text = "Workspace Summary";
			this.YearSelect_CBoxTS.ToolTipText = Conversions.ToString(this.BEADat.worksum.getSummary(ref this.BEADat));
		}

		private void miScale_Click(object sender, EventArgs e)
		{
			MyProject.Forms.frmNotify.Show("Test message", null);
		}

		private void miNewClone_Click(object sender, EventArgs e)
		{
			frmMain frmMain = new frmMain();
			frmMain.BEADat = (BEAData)this.BEADat.Clone();
			frmMain.options = (ViewOptions)this.options.Clone();
			frmMain.Show();
			MyProject.Forms.frmNotify.Show("Data in new window\r\nis independant", null);
		}

		private void miNewCopy_Click(object sender, EventArgs e)
		{
			frmMain frmMain = new frmMain();
			frmMain.BEADat = this.BEADat;
			frmMain.options = (ViewOptions)this.options.Clone();
			frmMain.Show();
			MyProject.Forms.frmNotify.Show("Data in new window is dependant\r\nChanges in one window will impact\r\ndata in the other", null);
		}

		private void frmMain_Load(object sender, EventArgs e)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(FileSystem.CurDir());
			this.MenuHandle = new clsMenuHandler(this);
			this.MenuHandle.SetMenuStates();
			FileInfo[] files = directoryInfo.GetFiles("*.wrksp", SearchOption.AllDirectories);
			FileInfo[] array = files;
			foreach (FileInfo fileInfo in array)
			{
				ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem();
				toolStripMenuItem.Name = "miMod" + fileInfo.Name;
				toolStripMenuItem.Text = fileInfo.Name;
				toolStripMenuItem.Tag = fileInfo.FullName;
				toolStripMenuItem.Click += this.LoadWorkspace;
			}
			this.YearSelect_CBoxTS.SelectedIndex = checked(this.DataCollection.getNrec() - 1);
			this.myToolTip.InitialDelay = 0;
			this.YSCbox = this.YearSelect_CBoxTS.ComboBox;
			this.changeCopyHeaderSettings(this.copyHeadersStatus);
			this.dgv.BackgroundImage = Image.FromFile("background.jpg");
			base.SetStyle(ControlStyles.UserPaint, true);
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			base.SetStyle(ControlStyles.DoubleBuffer, true);
			string currentDirectory = Directory.GetCurrentDirectory();
			string text = FileVersionInfo.GetVersionInfo(currentDirectory + "\\IOSnapUpdater.exe").FileVersion.ToString();
			WebClient webClient = new WebClient();
			HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://www.econalyze.com/IO-snapUpdaterFiles/UpdaterVersion.txt");
			HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
			string text2 = streamReader.ReadToEnd();
			if (Operators.CompareString(text2.ToString(), text.ToString(), false) > 0)
			{
				if (File.Exists(currentDirectory + "\\IOSnapUpdater.exe"))
				{
					File.Delete(currentDirectory + "\\IOSnapUpdater.exe");
				}
				webClient.DownloadFileAsync(new Uri("http://www.econalyze.com/IO-snapUpdaterFiles/IOSnapUpdater.exe"), currentDirectory + "\\IOSnapUpdater.exe");
			}
			HttpWebRequest httpWebRequest2 = (HttpWebRequest)WebRequest.Create("http://www.econalyze.com/IO-snapUpdaterFiles/Version.txt");
			HttpWebResponse httpWebResponse2 = (HttpWebResponse)httpWebRequest2.GetResponse();
			StreamReader streamReader2 = new StreamReader(httpWebResponse2.GetResponseStream());
			string text3 = streamReader2.ReadToEnd();
			string productVersion = Application.ProductVersion;
			if (Operators.CompareString(text3.ToString(), productVersion.ToString(), false) > 0)
			{
				object left = MessageBox.Show("A newer version of IO-Snap is available.\r\n\r\n      Your current version is: " + productVersion + "\r\nThe most recent version is: " + text3 + "\r\n" + "\r\nWould you like to update it now?", "IO-Snap Update Available!", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);
				if (Operators.ConditionalCompareObjectEqual(left, DialogResult.Yes, false))
				{
					MessageBox.Show("IO-Snap will now close and launch the updater");
					Process.Start(currentDirectory + "\\IOSnapUpdater.exe");
					Environment.Exit(0);
				}
			}
		}

		private bool CheckLicense()
		{
			bool flag = false;
			LicenseDat licenseDat = new LicenseDat();
			return licenseDat.Isvalid;
		}

		public void ClearScreen()
		{
			this.dgv.Columns.Clear();
			this.dgv.Rows.Clear();
			this.bDataDisplayed = false;
		}

		private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			DataGridViewCell dataGridViewCell = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex];
			int rowIndex = dataGridViewCell.RowIndex;
			int columnIndex = dataGridViewCell.ColumnIndex;
			double value;
			if (rowIndex < this.DispDat.BaseTable.NoRows && columnIndex < this.DispDat.BaseTable.NoCols)
			{
				this.UndoDat.SetData(this.DispDat.BaseTable);
				string s = Conversions.ToString(dataGridViewCell.Value);
				Matrix baseTable;
				int row;
				int col;
				value = (baseTable = this.DispDat.BaseTable)[row = rowIndex, col = columnIndex];
				double.TryParse(s, out value);
				baseTable[row, col] = value;
			}
			else if ((object)this.DispDat.ExtraRows != null && rowIndex >= this.DispDat.BaseTable.NoRows && columnIndex < this.DispDat.ExtraRows.NoRows)
			{
				rowIndex = checked(rowIndex - this.DispDat.BaseTable.NoRows);
				this.UndoDat.SetData(this.DispDat.ExtraRows);
				string s2 = Conversions.ToString(dataGridViewCell.Value);
				Matrix baseTable;
				int col;
				int row;
				value = (baseTable = this.DispDat.ExtraRows)[col = rowIndex, row = columnIndex];
				double.TryParse(s2, out value);
				baseTable[col, row] = value;
			}
			else if ((object)this.DispDat.ExtraCols != null && columnIndex >= this.DispDat.BaseTable.NoCols && rowIndex < this.DispDat.ExtraCols.NoCols)
			{
				columnIndex = checked(columnIndex - this.DispDat.BaseTable.NoCols);
				this.UndoDat.SetData(this.DispDat.ExtraCols);
				string s3 = Conversions.ToString(dataGridViewCell.Value);
				Matrix baseTable;
				int row;
				int col;
				value = (baseTable = this.DispDat.ExtraCols)[row = rowIndex, col = columnIndex];
				double.TryParse(s3, out value);
				baseTable[row, col] = value;
			}
			else
			{
				Interaction.MsgBox("That cell cannot be edited", MsgBoxStyle.OkOnly, null);
				this.dgv.CellValueChanged -= this.dgv_CellValueChanged;
				dataGridViewCell.Value = "X";
				this.dgv.CellValueChanged += this.dgv_CellValueChanged;
			}
		}

		private void midgvUndo_Click(object sender, EventArgs e)
		{
			this.UndoDat.Undo();
		}

		private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
		}

		private void miAbout_Click(object sender, EventArgs e)
		{
			frmAboutBox frmAboutBox = new frmAboutBox();
			if (frmAboutBox.ShowDialog() != DialogResult.OK)
			{
				return;
			}
		}

		public void changeCopyHeaderSettings(bool b)
		{
			this.copyHeadersStatus = b;
			if (this.copyHeadersStatus)
			{
				this.TSB4.Text = "WH";
			}
			else
			{
				this.TSB4.Text = "NH";
			}
			int selectedIndex = this.TabMain.SelectedIndex;
			bool flag = false;
			if (this.TabMain.TabPages.Count > 1)
			{
				int num = checked(this.TabMain.TabPages.Count - 1);
				for (int i = 1; i <= num; i = checked(i + 1))
				{
					this.TabMain.SelectTab(i);
					TabContent tabContent = (TabContent)this.TabMain.SelectedTab;
					if (this.copyHeadersStatus)
					{
						tabContent.dgvnn.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
					}
					else
					{
						tabContent.dgvnn.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
					}
				}
			}
			this.TabMain.SelectTab(selectedIndex);
		}

		private void OptionsTool_Click(object sender, EventArgs e)
		{
			ProgramOptionsForm programOptionsForm = new ProgramOptionsForm(ref this.BEADat, this.copyHeadersStatus);
			if (programOptionsForm.ShowDialog() == DialogResult.OK)
			{
				if (programOptionsForm.TableHeaderChange)
				{
					this.changeCopyHeaderSettings(programOptionsForm.getCopyHeaderStatus());
					this.copyHeadersStatus = programOptionsForm.getCopyHeaderStatus();
				}
				if (programOptionsForm.EmploymentChange)
				{
					if (this.TabMain.TabPages.Count > 1)
					{
						ArrayList arrayList = new ArrayList();
						int num = checked(this.TabMain.TabPages.Count - 1);
						for (int i = 1; i <= num; i = checked(i + 1))
						{
							this.TabMain.SelectTab(i);
							TabContent tabContent = (TabContent)this.TabMain.SelectedTab;
							if (tabContent.hasEmployment | tabContent.dispdat.isAfterGRT | tabContent.dispdat.isAfterRegionalization)
							{
								arrayList.Add(tabContent);
							}
						}
						IEnumerator enumerator = default(IEnumerator);
						try
						{
							enumerator = arrayList.GetEnumerator();
							while (enumerator.MoveNext())
							{
								object objectValue = RuntimeHelpers.GetObjectValue(enumerator.Current);
								this.TabMain.TabPages.Remove((TabPage)objectValue);
							}
						}
						finally
						{
							if (enumerator is IDisposable)
							{
								(enumerator as IDisposable).Dispose();
							}
						}
					}
					this.MenuHandle.SetMenuStates();
					this.TabMain.SelectTab(0);
				}
				if (programOptionsForm.isGovAggLevelChanged)
				{
					this.TabMain.SelectTab(0);
					if (this.TabMain.TabPages.Count > 1)
					{
						ArrayList arrayList2 = new ArrayList();
						int num2 = checked(this.TabMain.TabPages.Count - 1);
						for (int j = 1; j <= num2; j = checked(j + 1))
						{
							this.TabMain.SelectTab(j);
							TabContent value = (TabContent)this.TabMain.SelectedTab;
							arrayList2.Add(value);
						}
						IEnumerator enumerator2 = default(IEnumerator);
						try
						{
							enumerator2 = arrayList2.GetEnumerator();
							while (enumerator2.MoveNext())
							{
								object objectValue2 = RuntimeHelpers.GetObjectValue(enumerator2.Current);
								this.TabMain.TabPages.Remove((TabPage)objectValue2);
							}
						}
						finally
						{
							if (enumerator2 is IDisposable)
							{
								(enumerator2 as IDisposable).Dispose();
							}
						}
					}
					this.BEADat.ChangeGovAggregationLevel();
				}
				if (programOptionsForm.isFormatChanged && this.TabMain.TabPages.Count > 1)
				{
					int selectedIndex = this.TabMain.SelectedIndex;
					int num3 = checked(this.TabMain.TabPages.Count - 1);
					for (int k = 1; k <= num3; k = checked(k + 1))
					{
						this.TabMain.SelectTab(k);
						TabContent tabContent2 = (TabContent)this.TabMain.SelectedTab;
						ViewOptions viewOptions = new ViewOptions();
						viewOptions.GenFormBase = this.BEADat.FormatGeneral;
						viewOptions.CoeffFormatBase = this.BEADat.FormatCoefficients;
						viewOptions.RoundFormatBase = this.BEADat.FormatRounded;
						tabContent2.view_options = viewOptions;
						tabContent2.SetFormatContent();
						DataGridView dgvnn = tabContent2.dgvnn;
						dgvnn.Refresh();
						tabContent2.Refresh();
					}
					this.TabMain.SelectTab(selectedIndex);
				}
				this.ShowSummary();
			}
		}

		private void StateData_Display(object sender, EventArgs e)
		{
			StateCodeNames stateCodes = this.BEADat.stateCodes;
			SelectStateForm selectStateForm = new SelectStateForm(ref this.BEADat.stateCodes);
			string text = Conversions.ToString(selectStateForm.GetState());
			if (!text.Equals(selectStateForm.EmptyString))
			{
				string stateID = stateCodes.getStateID(text);
				StateData state = this.BEADat.RegStateData.GetState(ref stateID);
				this.DispDat = new DispData();
				string text2 = "";
				byte dispTotals;
				checked
				{
					Matrix matrix = new Matrix(state.getEmployment().Count + 1, 6);
					Matrix matrix2 = new Matrix(state.getEmployment_Flag().Count + 1, 6);
					int count = state.getEmployment().Count;
					int num = count - 1;
					for (int i = 0; i <= num; i++)
					{
						matrix[i, 0] = state.getEmployment()[i];
						matrix[i, 1] = state.getCompensationRate()[i];
						matrix[i, 2] = state.getEC()[i];
						matrix[i, 3] = state.getGOS()[i];
						matrix[i, 4] = state.getT()[i];
						matrix[i, 5] = state.getGDP()[i];
						matrix2[i, 0] = state.getEmployment_Flag()[i];
						matrix2[i, 1] = state.getCompensationRate_Flag()[i];
						matrix2[i, 2] = state.getEC_Flag()[i];
						matrix2[i, 3] = state.getGOS_Flag()[i];
						matrix2[i, 4] = state.getT_Flag()[i];
						matrix2[i, 5] = state.getGDP_Flag()[i];
						unchecked
						{
							Matrix matrix3;
							int row;
							(matrix3 = matrix)[row = count, 0] = matrix3[row, 0] + state.getEmployment()[i];
							(matrix3 = matrix)[row = count, 1] = matrix3[row, 1] + state.getCompensationRate()[i];
							(matrix3 = matrix)[row = count, 2] = matrix3[row, 2] + state.getEC()[i];
							(matrix3 = matrix)[row = count, 3] = matrix3[row, 3] + state.getGOS()[i];
							(matrix3 = matrix)[row = count, 4] = matrix3[row, 4] + state.getT()[i];
							(matrix3 = matrix)[row = count, 5] = matrix3[row, 5] + state.getGDP()[i];
							(matrix3 = matrix2)[row = count, 0] = matrix3[row, 0] + state.getEmployment_Flag()[i];
							(matrix3 = matrix2)[row = count, 1] = matrix3[row, 1] + state.getCompensationRate_Flag()[i];
							(matrix3 = matrix2)[row = count, 2] = matrix3[row, 2] + state.getEC_Flag()[i];
							(matrix3 = matrix2)[row = count, 3] = matrix3[row, 3] + state.getGOS_Flag()[i];
							(matrix3 = matrix2)[row = count, 4] = matrix3[row, 4] + state.getT_Flag()[i];
							(matrix3 = matrix2)[row = count, 5] = matrix3[row, 5] + state.getGDP_Flag()[i];
						}
					}
					this.DispDat.hasEmployment = true;
					this.DispDat.hdrsCol = new string[6]
					{
						"Employment",
						"Compensation per Employee",
						"Employee Compensation",
						"Gross Operating Surplus",
						"Taxes",
						"Gross Industry Product"
					};
					string[] array = new string[state.getEmployment().Count + 1 + 1];
					int num2 = state.getEmployment().Count - 1;
					for (int j = 0; j <= num2; j++)
					{
						array[j] = this.BEADat.NTables.hdrInd[j];
					}
					array[state.getEmployment().Count] = "Total";
					this.DispDat.hdrsRow = array;
					this.DispDat.BaseTable = matrix;
					this.DispDat.ColorTable = matrix2;
					this.DispDat.showCellColor = true;
					dispTotals = 1;
					text2 = text;
				}
				this.DisplayDataExtTab(text2, (DispTotal)dispTotals);
			}
			selectStateForm.Dispose();
		}

		private void miRegData_Click(object sender, EventArgs e)
		{
			string regby = this.BEADat.worksum.regby;
			StateData regionSelected = this.BEADat.RegionSelected;
			this.DispDat = new DispData();
			string text = "";
			int nrec_imputed = regionSelected.nrec_imputed;
			byte dispTotals;
			checked
			{
				Matrix matrix = default(Matrix);
				Matrix matrix2 = default(Matrix);
				switch (this.BEADat.RegMethod)
				{
				case BEAData.eMethod.Employment:
				{
					matrix = new Matrix(regionSelected.getEmployment().Count, 3);
					matrix2 = new Matrix(regionSelected.getEmployment_Flag().Count, 3);
					int num2 = nrec_imputed - 1;
					for (int j = 0; j <= num2; j++)
					{
						matrix[j, 0] = regionSelected.getEmployment()[j];
						matrix[j, 1] = this.BEADat.NTables.Employment[j];
						matrix[j, 2] = this.BEADat.epsilonReg[j];
						matrix2[j, 0] = regionSelected.getEmployment_Flag()[j];
						matrix2[j, 1] = 0.0;
						matrix2[j, 2] = 0.0;
					}
					this.DispDat.hdrsCol = new string[3]
					{
						"Employment (Region)",
						"Employment (National)",
						"Epsilon"
					};
					break;
				}
				case BEAData.eMethod.Income:
				{
					matrix = new Matrix(regionSelected.getEmployment().Count, 3);
					matrix2 = new Matrix(regionSelected.getEmployment_Flag().Count, 3);
					int num5 = nrec_imputed - 1;
					for (int m = 0; m <= num5; m++)
					{
						matrix[m, 0] = regionSelected.getEC()[m];
						matrix[m, 1] = this.BEADat.get_ECDouble(m);
						matrix[m, 2] = this.BEADat.epsilonReg[m];
						matrix2[m, 0] = regionSelected.getEC_Flag()[m];
						matrix2[m, 1] = 0.0;
						matrix2[m, 2] = 0.0;
					}
					this.DispDat.hdrsCol = new string[3]
					{
						"Employee Compensation (Region)",
						"Employee Compensation (National)",
						"Epsilon"
					};
					break;
				}
				case BEAData.eMethod.Income2:
				{
					matrix = new Matrix(regionSelected.getEmployment().Count, 3);
					matrix2 = new Matrix(regionSelected.getEmployment_Flag().Count, 3);
					int num3 = nrec_imputed - 1;
					for (int k = 0; k <= num3; k++)
					{
						matrix[k, 0] = regionSelected.getEC()[k];
						matrix[k, 1] = this.BEADat.get_ECDouble(k);
						matrix[k, 2] = this.BEADat.epsilonReg[k];
						matrix2[k, 0] = regionSelected.getEC_Flag()[k];
						matrix2[k, 1] = 0.0;
						matrix2[k, 2] = 0.0;
					}
					this.DispDat.hdrsCol = new string[3]
					{
						"Employee Compensation (Region)",
						"Employee Compensation (National)",
						"Epsilon"
					};
					break;
				}
				case BEAData.eMethod.Output:
				{
					matrix = new Matrix(regionSelected.getEmployment().Count, 3);
					matrix2 = new Matrix(regionSelected.getEmployment_Flag().Count, 3);
					int num4 = nrec_imputed - 1;
					for (int l = 0; l <= num4; l++)
					{
						matrix[l, 0] = regionSelected.getEC()[l];
						matrix[l, 1] = this.BEADat.get_ECDouble(l);
						matrix[l, 2] = this.BEADat.epsilonReg[l];
						matrix2[l, 0] = regionSelected.getEC_Flag()[l];
						matrix2[l, 1] = 0.0;
						matrix2[l, 2] = 0.0;
					}
					this.DispDat.hdrsCol = new string[3]
					{
						"Employee Compensation (Region)",
						"Employee Compensation (National)",
						"Epsilon"
					};
					break;
				}
				case BEAData.eMethod.ValueAdded:
				{
					matrix = new Matrix(regionSelected.getEmployment().Count, 7);
					matrix2 = new Matrix(regionSelected.getEmployment_Flag().Count, 7);
					int num = nrec_imputed - 1;
					for (int i = 0; i <= num; i++)
					{
						matrix[i, 0] = regionSelected.getEmployment()[i];
						matrix[i, 1] = regionSelected.getCompensationRate()[i];
						matrix[i, 2] = regionSelected.getEC()[i];
						matrix[i, 3] = regionSelected.getGOS()[i];
						matrix[i, 4] = regionSelected.getT()[i];
						matrix[i, 5] = regionSelected.getGDP()[i];
						matrix[i, 6] = this.BEADat.epsilonReg[i];
						matrix2[i, 0] = regionSelected.getEmployment_Flag()[i];
						matrix2[i, 1] = regionSelected.getCompensationRate_Flag()[i];
						matrix2[i, 2] = regionSelected.getEC_Flag()[i];
						matrix2[i, 3] = regionSelected.getGOS_Flag()[i];
						matrix2[i, 4] = regionSelected.getT_Flag()[i];
						matrix2[i, 5] = regionSelected.getGDP_Flag()[i];
						matrix2[i, 6] = 0.0;
					}
					this.DispDat.hdrsCol = new string[7]
					{
						"Employment",
						"Compensation Ratios",
						"Adjusted Employee Compensation",
						"Adjusted Gross Operating Surplus",
						"Adjusted Taxes",
						"Gross Regional Product",
						"Epsilon"
					};
					break;
				}
				default:
					Interaction.MsgBox("Unknown regionalization method", MsgBoxStyle.OkOnly, null);
					break;
				}
				this.DispDat.hasEmployment = true;
				this.DispDat.hdrsRow = this.BEADat.NTables.hdrInd;
				this.DispDat.BaseTable = matrix;
				this.DispDat.ColorTable = matrix2;
				this.DispDat.showCellColor = true;
				dispTotals = 1;
				text = "Regionalization data for " + regby;
			}
			this.DisplayDataExtTab(text, (DispTotal)dispTotals);
		}

		private void CopyWithHeadersButton_Click(object sender, EventArgs e)
		{
			if (this.copyHeadersStatus)
			{
				this.copyHeadersStatus = false;
			}
			else
			{
				this.copyHeadersStatus = true;
			}
			this.changeCopyHeaderSettings(this.copyHeadersStatus);
			this.ShowSummary();
		}

		private void Edit_Use_Click(object sender, EventArgs e)
		{
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			this.BEADat.ChangeGovAggregationLevel();
			BEAData bEAData = new BEAData();
			bEAData.worksum = new SummaryDat();
			bEAData.worksum.InitSetup();
			bEAData = (BEAData)this.BEADat.Clone();
			FormEditUse formEditUse = new FormEditUse(ref bEAData);
			if (formEditUse.ShowDialog() == DialogResult.OK)
			{
				this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
				DialogResult dialogResult = DialogResult.Yes;
				BEAData bEAData2 = new BEAData();
				bEAData2 = (BEAData)this.BEADat.Clone();
				object left = 0;
				checked
				{
					int num = bEAData.FinDem.NoRows - 1;
					for (int i = 0; i <= num; i++)
					{
						if (Math.Abs(unchecked(bEAData.Import[0, i] - bEAData2.Import[0, i])) > 10.0 && bEAData.Import[0, i] < 0.0)
						{
							left = Operators.AddObject(left, 1);
						}
					}
					this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
					this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
					if (Operators.ConditionalCompareObjectGreater(left, 0, false))
					{
						dialogResult = MessageBox.Show("Saving these edits result in one or more negative \"Imports\" values, which might lead to unexpected or inconsistent results.  Disregard the warning and proceed anyway?", "Warning..", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
					}
					if (dialogResult == DialogResult.Yes)
					{
						if (this.BEADat.worksum.DataTypeCode == SummaryDat.bcode.BASE_DATA)
						{
							if (!this.BEADat.isEdited)
							{
								string text = " ";
								bool flag = true;
								int num2 = 1;
								string text2 = "";
								while (flag)
								{
									flag = true;
									while (flag)
									{
										text = bEAData.worksum.datayear + "_v_" + Conversions.ToString(num2);
										object value = false;
										int num3 = this.YearSelect_CBoxTS.Items.Count - 1;
										for (int j = 0; j <= num3; j++)
										{
											if (string.Compare(this.YearSelect_CBoxTS.Items[j].ToString(), text) == 0)
											{
												value = true;
											}
										}
										if (Conversions.ToBoolean(value))
										{
											num2++;
										}
										else
										{
											flag = false;
										}
									}
									text2 = "";
									while (text2.Length == 0)
									{
										text2 = Interaction.InputBox("Please enter new dataset name", "IOSNAP Dataset Name", text, -1, -1);
									}
									object value2 = false;
									int num4 = this.YearSelect_CBoxTS.Items.Count - 1;
									for (int k = 0; k <= num4; k++)
									{
										if (string.Compare(this.YearSelect_CBoxTS.Items[k].ToString(), text2) == 0)
										{
											value2 = true;
										}
									}
									if (Conversions.ToBoolean(value2))
									{
										Interaction.MsgBox("IOSNAP dataset " + text2 + " already exists. Please enter a different name.", MsgBoxStyle.OkOnly, null);
										flag = true;
									}
									else
									{
										flag = false;
									}
								}
								bEAData.worksum.DataLabel = text2;
								bEAData.worksum.CurrentLabel = text2;
								if (bEAData.worksum.DataTypeCode != SummaryDat.bcode.REGION_DATA)
								{
									bEAData.worksum.DataTypeCode = SummaryDat.bcode.USER_DATA;
								}
								bEAData.worksum.DataTimeStamp = DateAndTime.Now.ToString();
								bEAData.worksum.requirementsTables = "No";
								bEAData.eDataState = BEAData.DataState.Loaded;
								bEAData.worksum.DataType = RuntimeHelpers.GetObjectValue(this.BEADat.worksum.DataType);
								this.DataCollection.addData(ref bEAData);
								this.YearSelect_CBoxTS.Items.Add(bEAData.worksum.DataLabel);
								this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
							}
							else
							{
								this.BEADat = bEAData;
							}
							this.BEADat.isEdited = true;
							this.TabClean();
							this.MenuHandle.SetMenuStates();
							MyProject.Forms.frmNotify.Show("USE table edited \r\nNew Dataset " + this.BEADat.worksum.DataLabel + " created", null);
						}
						else
						{
							this.BEADat = bEAData;
							this.BEADat.isEdited = true;
							this.TabClean();
							this.MenuHandle.SetMenuStates();
							MyProject.Forms.frmNotify.Show("USE table edited ", null);
						}
					}
					else
					{
						this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
						this.Edit_Use_Click(RuntimeHelpers.GetObjectValue(sender), e);
					}
				}
			}
			this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
			this.BEADat.ChangeGovAggregationLevel();
			this.ShowSummary();
		}

		private void Edit_Make_Click(object sender, EventArgs e)
		{
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			this.BEADat.ChangeGovAggregationLevel();
			BEAData bEAData = new BEAData();
			bEAData.worksum = new SummaryDat();
			bEAData.worksum.InitSetup();
			bEAData = (BEAData)this.BEADat.Clone();
			bEAData.ChangeGovAggregationLevel();
			FormEditMake formEditMake = new FormEditMake(ref bEAData);
			if (formEditMake.ShowDialog() == DialogResult.OK)
			{
				this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
				DialogResult dialogResult = DialogResult.Yes;
				BEAData bEAData2 = new BEAData();
				bEAData2 = (BEAData)this.BEADat.Clone();
				object left = 0;
				checked
				{
					int num = bEAData.FinDem.NoRows - 1;
					for (int i = 0; i <= num; i++)
					{
						if (Math.Abs(unchecked(bEAData.Import[0, i] - bEAData2.Import[0, i])) > 10.0 && bEAData.Import[0, i] < 0.0)
						{
							left = Operators.AddObject(left, 1);
						}
					}
					this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
					this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
					if (Operators.ConditionalCompareObjectGreater(left, 0, false))
					{
						dialogResult = MessageBox.Show("Saving these edits result in one or more negative \"Imports\" values, which might lead to unexpected or inconsistent results.  Disregard the warning and proceed anyway?", "Warning..", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
					}
					if (dialogResult == DialogResult.Yes)
					{
						if (this.BEADat.worksum.DataTypeCode == SummaryDat.bcode.BASE_DATA)
						{
							if (!this.BEADat.isEdited)
							{
								string text = " ";
								bool flag = true;
								int num2 = 1;
								string text2 = "";
								while (flag)
								{
									flag = true;
									while (flag)
									{
										text = bEAData.worksum.datayear + "_v_" + Conversions.ToString(num2);
										object value = false;
										int num3 = this.YearSelect_CBoxTS.Items.Count - 1;
										for (int j = 0; j <= num3; j++)
										{
											if (string.Compare(this.YearSelect_CBoxTS.Items[j].ToString(), text) == 0)
											{
												value = true;
											}
										}
										if (Conversions.ToBoolean(value))
										{
											num2++;
										}
										else
										{
											flag = false;
										}
									}
									text2 = "";
									while (text2.Length == 0)
									{
										text2 = Interaction.InputBox("Please enter new dataset name", "IOSNAP Dataset Name", text, -1, -1);
									}
									object value2 = false;
									int num4 = this.YearSelect_CBoxTS.Items.Count - 1;
									for (int k = 0; k <= num4; k++)
									{
										if (string.Compare(this.YearSelect_CBoxTS.Items[k].ToString(), text2) == 0)
										{
											value2 = true;
										}
									}
									if (Conversions.ToBoolean(value2))
									{
										Interaction.MsgBox("IOSNAP dataset " + text2 + " already exists. Please enter a different name.", MsgBoxStyle.OkOnly, null);
										flag = true;
									}
									else
									{
										flag = false;
									}
								}
								bEAData.worksum.DataLabel = text2;
								bEAData.worksum.CurrentLabel = text2;
								if (bEAData.worksum.DataTypeCode != SummaryDat.bcode.REGION_DATA)
								{
									bEAData.worksum.DataTypeCode = SummaryDat.bcode.USER_DATA;
								}
								bEAData.worksum.DataTimeStamp = DateAndTime.Now.ToString();
								bEAData.worksum.requirementsTables = "No";
								bEAData.eDataState = BEAData.DataState.Loaded;
								bEAData.worksum.DataType = RuntimeHelpers.GetObjectValue(this.BEADat.worksum.DataType);
								this.DataCollection.addData(ref bEAData);
								this.YearSelect_CBoxTS.Items.Add(bEAData.worksum.DataLabel);
								this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
							}
							else
							{
								this.BEADat = bEAData;
							}
							this.BEADat.isEdited = true;
							this.TabClean();
							this.MenuHandle.SetMenuStates();
							MyProject.Forms.frmNotify.Show("MAKE table edited \r\nNew Dataset " + this.BEADat.worksum.DataLabel + " created", null);
						}
						else
						{
							this.BEADat = bEAData;
							this.BEADat.isEdited = true;
							this.TabClean();
							this.MenuHandle.SetMenuStates();
							MyProject.Forms.frmNotify.Show("MAKE table edited ", null);
						}
					}
					else
					{
						this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
						this.Edit_Make_Click(RuntimeHelpers.GetObjectValue(sender), e);
					}
				}
			}
			this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
			this.BEADat.ChangeGovAggregationLevel();
			this.ShowSummary();
		}

		private void YearSelect_cbox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = Conversions.ToString(this.YearSelect_CBoxTS.SelectedItem);
            //TODO Later
			//if (this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 2;
			//		this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = false;
			//	}
			//	else if (this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread_0024Init);
			//}
			string text2 = "All previous data will be erased upon loading\r\nClick NO to save any data you want to preserve";
            //if (this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread)
            if (this.YearSelect_CBoxTS.SelectedIndex != -1)
            {
				this.BEADat = this.DataCollection.getData(this.YearSelect_CBoxTS.SelectedIndex);
				this.TabStore = this.DataCollection.getTabs(this.YearSelect_CBoxTS.SelectedIndex);
				this.myToolTip.Active = false;
				this.TabClean();
				this.ShowSummary();
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.TabStore.TabsShown.GetEnumerator();
					while (enumerator.MoveNext())
					{
						TabContent value = (TabContent)enumerator.Current;
						this.TabMain.TabPages.Add(value);
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				this.Table_Label.Text = "Data selected for year " + this.BEADat.worksum.datayear;
			}
			else
			{
				this.BEADat = this.DataCollection.getData(this.YearSelect_CBoxTS.SelectedIndex);
				this.TabStore = this.DataCollection.getTabs(this.YearSelect_CBoxTS.SelectedIndex);
				this.TabClean();
				this.ShowSummary();
				this.Table_Label.Text = "Data selected for default year of " + this.BEADat.worksum.datayear;
                //TODO Later
				//this._0024STATIC_0024YearSelect_cbox_SelectedIndexChanged_002420211C1280B1_0024notfirtsread = true;
			}
		}

		private void RestoreCurrentDataYearToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string prompt = "All changes will be erased upon restoration!\r\nClick Cancel and save any progress you want to preserve.";
			if (Interaction.MsgBox(prompt, MsgBoxStyle.OkCancel, "Continue Restoring?") == MsgBoxResult.Ok)
			{
				if (this.YearSelect_CBoxTS.SelectedIndex > checked(this.RefDataCollection.getNrec() - 1))
				{
					Interaction.MsgBox("Cannot replace this data!", MsgBoxStyle.OkOnly, null);
				}
				else
				{
					BEAData bEAData = new BEAData();
					SummaryDat summaryDat = bEAData.worksum = new SummaryDat();
					bEAData = (this.BEADat = (BEAData)this.RefDataCollection.getData(this.YearSelect_CBoxTS.SelectedIndex).Clone());
					this.DataCollection.bealist.RemoveAt(this.YearSelect_CBoxTS.SelectedIndex);
					this.DataCollection.bealist.Insert(this.YearSelect_CBoxTS.SelectedIndex, bEAData);
					this.TabClean();
					this.ShowSummary();
					this.MenuHandle.SetMenuStates();
				}
			}
			else
			{
				Interaction.MsgBox("Error 783V3", MsgBoxStyle.OkOnly, null);
			}
		}

		private void RestoreBaseData(object sender, EventArgs e)
		{
			string prompt = "All changes will be erased upon restoration!\r\nClick Cancel and save any progress you want to preserve.";
			if (Interaction.MsgBox(prompt, MsgBoxStyle.OkCancel, "Continue Restoring?") == MsgBoxResult.Ok)
			{
				try
				{
					int num = 0;
					this.YearSelect_CBoxTS.Items.Clear();
					this.DataCollection = new DataWrapREADER();
					this.DataCollection.setEmpty();
					IEnumerator enumerator = default(IEnumerator);
					try
					{
						enumerator = this.RefDataCollection.bealist.GetEnumerator();
						while (enumerator.MoveNext())
						{
							BEAData bEAData = (BEAData)enumerator.Current;
							BEAData bEAData2 = new BEAData();
							SummaryDat summaryDat = bEAData2.worksum = new SummaryDat();
							bEAData2 = (this.BEADat = (BEAData)bEAData.Clone());
							this.DataCollection.addData(ref bEAData2);
							this.YearSelect_CBoxTS.Items.Add(bEAData2.worksum.DataLabel);
							if (Operators.CompareString(bEAData2.worksum.CurrentLabel, "", false) == 0)
							{
								bEAData2.worksum.CurrentLabel = bEAData2.worksum.DataLabel;
							}
							num = checked(num + 1);
						}
					}
					finally
					{
						if (enumerator is IDisposable)
						{
							(enumerator as IDisposable).Dispose();
						}
					}
					checked
					{
						this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
						this.BEADat = this.DataCollection.getData(this.DataCollection.getNrec() - 1);
						this.ClearScreen();
						this.TabClean();
						this.ShowSummary();
						this.MenuHandle.SetMenuStates();
					}
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
			else
			{
				Interaction.MsgBox("Base Data Restoration Canceled.", MsgBoxStyle.OkOnly, null);
			}
		}

		private void CustomRegionSetup(object sender, EventArgs e)
		{
			RegSett regSett = new RegSett(ref this.BEADat);
			if (regSett.ShowDialog() == DialogResult.OK)
			{
				this.BEADat.RegStateData.addState("X1", ref regSett.regiondat);
				this.BEADat.stateCodes.addState("X1", regSett.regiondat.Name);
				Interaction.MsgBox(this.BEADat.stateCodes.getStateName("X1"), MsgBoxStyle.OkOnly, null);
				Interaction.MsgBox("Region " + regSett.regiondat.Name, MsgBoxStyle.OkOnly, null);
				this.ShowSummary();
			}
		}

		private void Edit_FinalDemand(object sender, EventArgs e)
		{
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			this.BEADat.ChangeGovAggregationLevel();
			BEAData bEAData = new BEAData();
			bEAData.worksum = new SummaryDat();
			bEAData.worksum.InitSetup();
			bEAData = (BEAData)this.BEADat.Clone();
			FormEditFD formEditFD = new FormEditFD(ref bEAData);
			if (formEditFD.ShowDialog() == DialogResult.OK)
			{
				this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
				DialogResult dialogResult = DialogResult.Yes;
				BEAData bEAData2 = new BEAData();
				bEAData2 = (BEAData)this.BEADat.Clone();
				object left = 0;
				checked
				{
					int num = bEAData.FinDem.NoRows - 1;
					for (int i = 0; i <= num; i++)
					{
						if (Math.Abs(unchecked(bEAData.Import[0, i] - bEAData2.Import[0, i])) > 10.0 && bEAData.Import[0, i] < 0.0)
						{
							left = Operators.AddObject(left, 1);
						}
					}
					this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
					this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
					if (Operators.ConditionalCompareObjectGreater(left, 0, false))
					{
						dialogResult = MessageBox.Show("Saving these edits result in one or more negative \"Imports\" values, which might lead to unexpected or inconsistent results.  Disregard the warning and proceed anyway?", "Warning..", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
					}
					if (dialogResult == DialogResult.Yes)
					{
						if (this.BEADat.worksum.DataTypeCode == SummaryDat.bcode.BASE_DATA)
						{
							if (!bEAData.isEdited)
							{
								string text = " ";
								bool flag = true;
								int num2 = 1;
								string text2 = "";
								while (flag)
								{
									flag = true;
									while (flag)
									{
										text = bEAData.worksum.datayear + "_v_" + Conversions.ToString(num2);
										object value = false;
										int num3 = this.YearSelect_CBoxTS.Items.Count - 1;
										for (int j = 0; j <= num3; j++)
										{
											if (string.Compare(this.YearSelect_CBoxTS.Items[j].ToString(), text) == 0)
											{
												value = true;
											}
										}
										if (Conversions.ToBoolean(value))
										{
											num2++;
										}
										else
										{
											flag = false;
										}
									}
									text2 = "";
									while (text2.Length == 0)
									{
										text2 = Interaction.InputBox("Please enter new dataset name", "IOSNAP Dataset Name", text, -1, -1);
									}
									object value2 = false;
									int num4 = this.YearSelect_CBoxTS.Items.Count - 1;
									for (int k = 0; k <= num4; k++)
									{
										if (string.Compare(this.YearSelect_CBoxTS.Items[k].ToString(), text2) == 0)
										{
											value2 = true;
										}
									}
									if (Conversions.ToBoolean(value2))
									{
										Interaction.MsgBox("IOSNAP dataset " + text2 + " already exists. Please enter a different name.", MsgBoxStyle.OkOnly, null);
										flag = true;
									}
									else
									{
										flag = false;
									}
								}
								bEAData.worksum.DataLabel = text2;
								bEAData.worksum.CurrentLabel = text2;
								if (bEAData.worksum.DataTypeCode != SummaryDat.bcode.REGION_DATA)
								{
									bEAData.worksum.DataTypeCode = SummaryDat.bcode.USER_DATA;
								}
								bEAData.worksum.DataTimeStamp = DateAndTime.Now.ToString();
								bEAData.worksum.requirementsTables = "No";
								bEAData.eDataState = BEAData.DataState.Loaded;
								bEAData.worksum.DataType = RuntimeHelpers.GetObjectValue(this.BEADat.worksum.DataType);
								this.DataCollection.addData(ref bEAData);
								this.YearSelect_CBoxTS.Items.Add(bEAData.worksum.DataLabel);
								this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
							}
							else
							{
								this.BEADat = bEAData;
							}
							this.BEADat.isEdited = true;
							this.TabClean();
							this.MenuHandle.SetMenuStates();
							MyProject.Forms.frmNotify.Show("Final Demand Edited \r\nNew Dataset " + this.BEADat.worksum.DataLabel + " created", null);
						}
						else
						{
							this.BEADat = bEAData;
							this.BEADat.isEdited = true;
							this.TabClean();
							this.MenuHandle.SetMenuStates();
							MyProject.Forms.frmNotify.Show("Final Demand Edited ", null);
						}
					}
					else
					{
						this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
						this.Edit_FinalDemand(RuntimeHelpers.GetObjectValue(sender), e);
					}
				}
			}
			this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
			this.BEADat.ChangeGovAggregationLevel();
			this.ShowSummary();
		}

		private void DataCodingToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string text = "";
			FileStream fileStream = new FileStream(FileSystem.CurDir() + "\\cdata0908_temp.dat", FileMode.Open);
			StreamWriter streamWriter = new StreamWriter(FileSystem.CurDir() + "\\data0908_prop_temp.dat", false);
			checked
			{
				byte[] array = new byte[(int)fileStream.Length + 1];
				fileStream.Read(array, 0, (int)fileStream.Length);
				text = Convert.ToBase64String(array);
				streamWriter.Write(text);
				streamWriter.Close();
				fileStream.Close();
			}
		}

		private void ExtractRegionalizedDataToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.ExtractRegionalData();
		}

		private void ExtractRegionalData()
		{
			BEAData bEAData = new BEAData();
			bEAData.worksum = new SummaryDat();
			bEAData.worksum.InitSetup();
			bEAData = (BEAData)this.BEADat.Clone();
			NatTables nTables = bEAData.NTables;
			Matrix matrix = this.BEADat.RegMake.Clone();
			nTables.SetMake(ref matrix);
			bEAData.Use = this.BEADat.RegUse.Clone();
			bEAData.FinDem = this.BEADat.RegFinDem.Clone();
			bEAData.NTables.VA = this.BEADat.RegVA;
			bEAData.Scrap = this.BEADat.RegScrap;
			bEAData.Import = this.BEADat.RegImports;
			bEAData.NCI = this.BEADat.RegNCI;
			bEAData.isDataRegionalized = false;
			string text = " ";
			string text2 = "";
			bool flag = true;
			int num = 1;
			string regby = this.BEADat.worksum.regby;
			checked
			{
				while (flag)
				{
					flag = true;
					while (flag)
					{
						text = bEAData.worksum.datayear + "_" + regby + "_r_" + Conversions.ToString(num);
						object value = false;
						int num2 = this.YearSelect_CBoxTS.Items.Count - 1;
						for (int i = 0; i <= num2; i++)
						{
							if (string.Compare(this.YearSelect_CBoxTS.Items[i].ToString(), text) == 0)
							{
								value = true;
							}
						}
						if (Conversions.ToBoolean(value))
						{
							num++;
						}
						else
						{
							flag = false;
						}
					}
					text2 = "";
					while (text2.Length == 0)
					{
						text2 = Interaction.InputBox("Please enter new dataset name", "IOSNAP Dataset Name", text, -1, -1);
					}
					object value2 = false;
					int num3 = this.YearSelect_CBoxTS.Items.Count - 1;
					for (int j = 0; j <= num3; j++)
					{
						if (string.Compare(this.YearSelect_CBoxTS.Items[j].ToString(), text2) == 0)
						{
							value2 = true;
						}
					}
					if (Conversions.ToBoolean(value2))
					{
						Interaction.MsgBox("IOSNAP dataset " + text2 + " already exists. Please enter a different name.", MsgBoxStyle.OkOnly, null);
						flag = true;
					}
					else
					{
						flag = false;
					}
				}
				bEAData.worksum.DataLabel = text2;
				bEAData.worksum.CurrentLabel = text2;
				bEAData.worksum.DataTypeCode = SummaryDat.bcode.REGION_DATA;
				bEAData.worksum.DataTimeStamp = DateAndTime.Now.ToString();
				bEAData.worksum.DataType = RuntimeHelpers.GetObjectValue(this.BEADat.worksum.DataType);
				bEAData.isFromRegionalData = true;
				bEAData.ExtractNCIFromMake();
				bEAData.ExtractScrapFromMake();
				this.DataCollection.addData(ref bEAData);
				this.BEADat.isDataRegionalized = false;
				this.BEADat.worksum.SetRegionalizationStatus(false);
				this.BEADat.worksum.regby = "";
				this.BEADat.worksum.regmethod = "";
				this.YearSelect_CBoxTS.Items.Add(bEAData.worksum.DataLabel);
				this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
				this.BEADat.isEdited = true;
				this.MenuHandle.SetMenuStates();
				MyProject.Forms.frmNotify.Show("Regional data extracted: " + bEAData.worksum.DataLabel, null);
				this.ShowSummary();
				this.Table_Label.Text = "Regional data extracted";
			}
		}

		private void Edit_State(object sender, EventArgs e)
		{
			this.GovernmentAggStatus.setTMP_AggLevel(this.GovernmentAggStatus.getAggLevel());
			this.GovernmentAggStatus.setAggLevel(GenDataStatus.dataStatusType.ImputedGov);
			this.BEADat.ChangeGovAggregationLevel();
			this.EditStateMain();
			this.GovernmentAggStatus.setAggLevel(this.GovernmentAggStatus.getTMP_AggLevel());
			this.BEADat.ChangeGovAggregationLevel();
			this.ShowSummary();
		}

		public unsafe void EditStateMain()
		{
			StateCodeNames stateCodes = this.BEADat.stateCodes;
			SelectStateForm selectStateForm = new SelectStateForm(ref this.BEADat.stateCodes);
			string text = Conversions.ToString(selectStateForm.GetState());
			if (!text.Equals(selectStateForm.EmptyString))
			{
				string stateID = stateCodes.getStateID(text);
				FormEditState formEditState = new FormEditState(ref this.BEADat, stateID, text);
				if (formEditState.ShowDialog() == DialogResult.OK)
				{
					object obj;
					if (this.BEADat.worksum.DataTypeCode == SummaryDat.bcode.BASE_DATA)
					{
						BEAData bEAData = new BEAData();
						bEAData.worksum = new SummaryDat();
						bEAData.worksum.InitSetup();
						bEAData = (BEAData)this.BEADat.Clone();
						string text2 = " ";
						bool flag = true;
						int num = 1;
						string text3 = "";
						RegionalData regStateData;
						string stname;
						checked
						{
							while (flag)
							{
								flag = true;
								while (flag)
								{
									text2 = bEAData.worksum.datayear + "_v_" + Conversions.ToString(num);
									object value = false;
									int num2 = this.YearSelect_CBoxTS.Items.Count - 1;
									for (int i = 0; i <= num2; i++)
									{
										if (string.Compare(this.YearSelect_CBoxTS.Items[i].ToString(), text2) == 0)
										{
											value = true;
										}
									}
									if (Conversions.ToBoolean(value))
									{
										num++;
									}
									else
									{
										flag = false;
									}
								}
								text3 = "";
								while (text3.Length == 0)
								{
									text3 = Interaction.InputBox("Please enter new dataset name", "IOSNAP Dataset Name", text2, -1, -1);
								}
								object value2 = false;
								int num3 = this.YearSelect_CBoxTS.Items.Count - 1;
								for (int j = 0; j <= num3; j++)
								{
									if (string.Compare(this.YearSelect_CBoxTS.Items[j].ToString(), text3) == 0)
									{
										value2 = true;
									}
								}
								if (Conversions.ToBoolean(value2))
								{
									Interaction.MsgBox("IOSNAP dataset " + text3 + " already exists. Please enter a different name.", MsgBoxStyle.OkOnly, null);
									flag = true;
								}
								else
								{
									flag = false;
								}
							}
							bEAData.worksum.DataLabel = text3;
							bEAData.worksum.CurrentLabel = text3;
							if (bEAData.worksum.DataTypeCode != SummaryDat.bcode.REGION_DATA)
							{
								bEAData.worksum.DataTypeCode = SummaryDat.bcode.USER_DATA;
							}
							bEAData.worksum.requirementsTables = "No";
							bEAData.eDataState = BEAData.DataState.Loaded;
							bEAData.worksum.DataType = RuntimeHelpers.GetObjectValue(this.BEADat.worksum.DataType);
							this.DataCollection.addData(ref bEAData);
							this.YearSelect_CBoxTS.Items.Add(bEAData.worksum.DataLabel);
							this.YearSelect_CBoxTS.SelectedIndex = this.DataCollection.getNrec() - 1;
							regStateData = this.BEADat.RegStateData;
							stname = stateID;
						}
                        //TODO Need to check logic
						//ref StateData newStatData;
						//obj = *(object*)(ref newStatData = ref formEditState.newStatData);
						regStateData.replaceState(stname, ref formEditState.newStatData);
                        //StateData newStatData = (StateData)obj;
						this.BEADat.isEdited = true;
						this.TabClean();
						this.MenuHandle.SetMenuStates();
						MyProject.Forms.frmNotify.Show("New Data Created - " + this.BEADat.worksum.DataLabel + "\r\nState " + text + "  Successfully Edited", null);
					}
					else
					{
						RegionalData regStateData2 = this.BEADat.RegStateData;
						string stname2 = stateID;
                        //ref StateData newStatData;
                        //obj = *(object*)(ref newStatData = ref formEditState.newStatData);
                        //regStateData2.replaceState(stname2, ref obj);
                        regStateData2.replaceState(stname2, ref formEditState.newStatData);
                        //newStatData = (StateData)obj;
                        this.TabClean();
						this.MenuHandle.SetMenuStates();
						MyProject.Forms.frmNotify.Show("State " + text + "  Successfully Edited", null);
					}
				}
			}
		}

		private void ViewIOAccounts(object sender, EventArgs e)
		{
			int selectedIndex = this.YearSelect_CBoxTS.SelectedIndex;
			ViewIO_Accounts_Form viewIO_Accounts_Form = new ViewIO_Accounts_Form(ref this.DataCollection, ref selectedIndex);
			if (viewIO_Accounts_Form.ShowDialog() == DialogResult.OK)
			{
				string label = viewIO_Accounts_Form.getLabel();
				this.YearSelect_CBoxTS.SelectedItem = label;
				if (viewIO_Accounts_Form.isNational & (viewIO_Accounts_Form.isRegionDataOnly | viewIO_Accounts_Form.isBothRegNatData))
				{
					if (viewIO_Accounts_Form.isMake)
					{
						this.ViewDataMain("miRegMake");
					}
					if (viewIO_Accounts_Form.isUse)
					{
						this.ViewDataMain("miRegUse");
					}
					if (viewIO_Accounts_Form.isFD)
					{
						this.ViewDataMain("miRegFinDem");
					}
					if (viewIO_Accounts_Form.isVA)
					{
						this.ViewDataMain("miRegVA");
					}
				}
				if (viewIO_Accounts_Form.isNational & (!viewIO_Accounts_Form.isRegionDataOnly | viewIO_Accounts_Form.isBothRegNatData))
				{
					if (viewIO_Accounts_Form.isMake)
					{
						this.ViewDataMain("miMake");
					}
					if (viewIO_Accounts_Form.isUse)
					{
						this.ViewDataMain("miUse");
					}
					if (viewIO_Accounts_Form.isFD)
					{
						this.ViewDataMain("miFinDem");
					}
					if (viewIO_Accounts_Form.isVA)
					{
						this.ViewDataMain("miVA");
					}
				}
				if (viewIO_Accounts_Form.isUserRegistered)
				{
					if (viewIO_Accounts_Form.isMake)
					{
						this.ViewDataMain("Make");
					}
					if (viewIO_Accounts_Form.isUse)
					{
						this.ViewDataMain("Use");
					}
					if (viewIO_Accounts_Form.isFD)
					{
						this.ViewDataMain("FinDem");
					}
					if (viewIO_Accounts_Form.isVA)
					{
						this.ViewDataMain("VA");
					}
				}
			}
		}

		private void DisplayRequirementsTables(object sender, EventArgs e)
		{
			ref DataWrapREADER dataCollection = ref this.DataCollection;
			ToolStripComboBox yearSelect_CBoxTS;
			int selectedIndex = (yearSelect_CBoxTS = this.YearSelect_CBoxTS).SelectedIndex;
			RequirementsTablesForm requirementsTablesForm = new RequirementsTablesForm(ref dataCollection, ref selectedIndex);
			yearSelect_CBoxTS.SelectedIndex = selectedIndex;
			RequirementsTablesForm requirementsTablesForm2 = requirementsTablesForm;
			if (requirementsTablesForm2.ShowDialog() == DialogResult.OK)
			{
				string label = requirementsTablesForm2.getLabel();
				this.YearSelect_CBoxTS.SelectedItem = label;
				this.BEADat.Calculate();
				string text;
				if (requirementsTablesForm2.isDRO)
				{
					if (requirementsTablesForm2.isIxI)
					{
						text = "miDRToIxI";
						this.DisplayTotDirRT(ref text);
					}
					if (requirementsTablesForm2.isIxC)
					{
						text = "miDRToIxC";
						this.DisplayTotDirRT(ref text);
					}
					if (requirementsTablesForm2.isCxC)
					{
						text = "miDRToCxC";
						this.DisplayTotDirRT(ref text);
					}
				}
				if (requirementsTablesForm2.isDRC)
				{
					if (requirementsTablesForm2.isIxI)
					{
						text = "miDRTcIxI";
						this.DisplayTotDirRT(ref text);
					}
					if (requirementsTablesForm2.isIxC)
					{
						text = "miDRTcIxC";
						this.DisplayTotDirRT(ref text);
					}
					if (requirementsTablesForm2.isCxC)
					{
						text = "miDRTcCxC";
						this.DisplayTotDirRT(ref text);
					}
				}
				if (requirementsTablesForm2.isTRO)
				{
					if (requirementsTablesForm2.isIxI)
					{
						text = "miTRoIxI";
						this.DisplayTotDirRT(ref text);
					}
					if (requirementsTablesForm2.isIxC)
					{
						text = "miTRoIxC";
						this.DisplayTotDirRT(ref text);
					}
					if (requirementsTablesForm2.isCxC)
					{
						text = "miTRoCxC";
						this.DisplayTotDirRT(ref text);
					}
				}
				if (requirementsTablesForm2.isTRC)
				{
					if (requirementsTablesForm2.isIxI)
					{
						text = "miTRcIxI";
						this.DisplayTotDirRT(ref text);
					}
					if (requirementsTablesForm2.isIxC)
					{
						text = "miTRcIxC";
						this.DisplayTotDirRT(ref text);
					}
					if (requirementsTablesForm2.isCxC)
					{
						text = "miTRcCxC";
						this.DisplayTotDirRT(ref text);
					}
				}
			}
		}

		private void MultipliersToolStripMenuItem_Click(object sender, EventArgs e)
		{
			int selectedIndex = this.YearSelect_CBoxTS.SelectedIndex;
			ViewMultipliers_Form viewMultipliers_Form = new ViewMultipliers_Form(ref this.DataCollection, ref selectedIndex);
			if (viewMultipliers_Form.ShowDialog() == DialogResult.OK)
			{
				string label = viewMultipliers_Form.getLabel();
				this.YearSelect_CBoxTS.SelectedItem = label;
				this.BEADat.Calculate();
				if (viewMultipliers_Form.isEmployment)
				{
					if (viewMultipliers_Form.isIxI)
					{
						this.showMultipliers("miDMIxIE");
					}
					if (viewMultipliers_Form.isIxC)
					{
						this.showMultipliers("miDMIxCE");
					}
				}
				if (viewMultipliers_Form.isIncome)
				{
					if (viewMultipliers_Form.isIxI)
					{
						this.showMultipliers("miDMIxII");
					}
					if (viewMultipliers_Form.isIxC)
					{
						this.showMultipliers("miDMIxCI");
					}
				}
				if (viewMultipliers_Form.isOutput)
				{
					if (viewMultipliers_Form.isIxI)
					{
						this.showMultipliers("miDMIxIO");
					}
					if (viewMultipliers_Form.isIxC)
					{
						this.showMultipliers("miDMIxCO");
					}
					if (viewMultipliers_Form.isCxC)
					{
						this.showMultipliers("miDMCxCO");
					}
				}
			}
		}

		private void MyTabControl_SelectedIndexChanged(object sender, EventArgs e)
		{
			int selectedIndex = this.TabMain.SelectedIndex;
			TabPage selectedTab = this.TabMain.SelectedTab;
			this.Table_Label.Text = selectedTab.Text;
		}

		private void ToolStripButton4_Click(object sender, EventArgs e)
		{
			checked
			{
				if (string.Compare(this.TabMain.SelectedTab.Text, this.MainTabText) != 0)
				{
					for (int num = this.TabMain.TabCount - 1; num > 0; num--)
					{
						this.TabMain.SelectTab(num);
						this.TabMain.Controls.Remove(this.TabMain.SelectedTab);
					}
					this.TabMain.SelectTab(0);
				}
			}
		}

		private void miDocument_Click(object sender, EventArgs e)
		{
			Process.Start("https://www.io-snap.com/support/");
		}

		private void miHelp_Click(object sender, EventArgs e)
		{
		}

		private void DeactivateToolStripMenuItem_Click(object sender, EventArgs e)
		{
			bool flag = false;
			LicenseDat licenseDat = new LicenseDat();
			if (licenseDat.ReadLicenseFile())
			{
				if (licenseDat.Isvalid)
				{
					MySqlConnection mySqlConnection = new MySqlConnection("server=fretb001.mysql.guardedhost.com; database=fretb001_iosnap_db;uid=fretb001_iosnap_db;password=D-tg2jXmk4g8");
					string text2 = default(string);
					try
					{
						NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
						string right = allNetworkInterfaces[0].GetPhysicalAddress().ToString();
						string expression = Strings.Replace(licenseDat.LicenseKey, "/", "", 1, -1, CompareMethod.Binary);
						string text = Strings.Replace(expression, "<", "", 1, -1, CompareMethod.Binary);
						text2 = text;
						string cmdText = "Select * from IO_Snap_DB where LicKey='" + text2 + "'";
						object instance = new MySqlCommand(cmdText, mySqlConnection);
						mySqlConnection.Open();
						MySqlDataReader mySqlDataReader = (MySqlDataReader)NewLateBinding.LateGet(instance, null, "ExecuteReader", new object[0], null, null, null);
						mySqlDataReader.Read();
						int num = Conversions.ToInteger(mySqlDataReader[5]);
						int num2 = 0;
						checked
						{
							int num3 = 5 + num;
							for (int i = 6; i <= num3; i++)
							{
								if (Operators.CompareString(Convert.ToString(RuntimeHelpers.GetObjectValue(mySqlDataReader[i])), right, false) == 0)
								{
									num2 = i;
								}
							}
							mySqlConnection.Close();
							num2 -= 5;
							string text3 = "MAC" + Convert.ToString(num2);
							try
							{
								object value = "Update IO_Snap_DB set  " + text3 + " =@" + text3 + "  where LicKey='" + text2 + "'";
								instance = new MySqlCommand(Conversions.ToString(value), mySqlConnection);
								NewLateBinding.LateSetComplex(NewLateBinding.LateGet(NewLateBinding.LateGet(instance, null, "Parameters", new object[0], null, null, null), null, "Add", new object[1]
								{
									new MySqlParameter(("@" + text3) ?? "", MySqlDbType.VarChar, 50)
								}, null, null, null), null, "Value", new object[1]
								{
									"0"
								}, null, null, false, true);
								mySqlConnection.Open();
								NewLateBinding.LateCall(instance, null, "ExecuteNonQuery", new object[0], null, null, null, true);
							}
							catch (Exception ex)
							{
								ProjectData.SetProjectError(ex);
								Exception ex2 = ex;
								Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
								ProjectData.ClearProjectError();
							}
							finally
							{
								mySqlConnection.Close();
							}
						}
					}
					finally
					{
						mySqlConnection.Close();
					}
					string text4 = "";
					text4 = FileSystem.CurDir() + "\\License.dat";
					if (File.Exists(text4))
					{
						try
						{
							File.Delete(text4);
							string prompt = "License Deactivation was successfully completed, you may now reuse the license key.\r\n Key : " + text2;
							Interaction.MsgBox(prompt, MsgBoxStyle.OkOnly, null);
							Interaction.MsgBox("The program will now exit!", MsgBoxStyle.OkOnly, null);
							try
							{
								Process.Start("http://www.io-snap.com/");
							}
							catch (Exception ex3)
							{
								ProjectData.SetProjectError(ex3);
								Exception ex4 = ex3;
								ProjectData.ClearProjectError();
							}
							Environment.Exit(0);
						}
						catch (Exception ex5)
						{
							ProjectData.SetProjectError(ex5);
							Exception ex6 = ex5;
							Interaction.MsgBox(ex6.Message, MsgBoxStyle.OkOnly, null);
							ProjectData.ClearProjectError();
						}
					}
				}
				else
				{
					Interaction.MsgBox("License is not valid!", MsgBoxStyle.OkOnly, null);
				}
			}
			else
			{
				Interaction.MsgBox("No License exists to deactivate!", MsgBoxStyle.OkOnly, null);
			}
		}

		private void ActivateToolStripMenuItem_Click(object sender, EventArgs e)
		{
			LicenseDat licenseDat = new LicenseDat();
			RegistrationForm registrationForm = new RegistrationForm();
			if (registrationForm.ShowDialog() == DialogResult.OK)
			{
				if (registrationForm.isValid)
				{
					licenseDat = registrationForm.License;
					licenseDat.saveLicense();
					MyProject.Forms.frmNotify.Show(licenseDat.LicenseType + " Version of IOSNAP Registered\r\nYour License Will Expire on " + Conversions.ToString(licenseDat.ExpirationDate), null);
				}
				else
				{
					Interaction.MsgBox("Demo Mode : Expires on " + Conversions.ToString(licenseDat.ExpirationDate), MsgBoxStyle.OkOnly, null);
					this.Text = "IO-SNAP (Demo Mode)";
				}
			}
			else
			{
				Interaction.MsgBox("Demo Mode : Expires on " + Conversions.ToString(licenseDat.ExpirationDate), MsgBoxStyle.OkOnly, null);
				this.Text = "IO-SNAP (Demo Mode)";
			}
			Interaction.MsgBox("The program will be restarted to apply a new setting", MsgBoxStyle.OkOnly, null);
			Application.Exit();
			Process.Start(Application.ExecutablePath);
		}

		private void ActivateToolStripMenuItem_MouseHover(object sender, EventArgs e)
		{
		}

		private void miDescTools_Click(object sender, EventArgs e)
		{
		}

		private void ViewToolStripMenuItem_Click(object sender, EventArgs e)
		{
		}

		private void miFile_Click(object sender, EventArgs e)
		{
		}
    }
}
