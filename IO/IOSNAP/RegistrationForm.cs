using IOSNAP.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using MySql.Data.MySqlClient;
using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class RegistrationForm : Form
	{
		private IContainer components;

		public static DateTime SDate = Conversions.ToDate("01/01/2001");

		public bool isValid;

		public DateTime StartingDate;

		public DateTime ExpirationDate;

		public string LicenseKey;

		public string DataVersion;

		public string ProgramVersion;

		public int LicenseLength;

		public string LicenseType;

		public LicenseDat License;

		private int[] LicenseLengthTable;

		private int[] Dversion;

		private string CodeValue;

		private char[] CxCodes;

        public Button RegisterButton;
        public Button DemoButton;
        public Label Label2;

        public TextBox RegistrationCodeTextBox;

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(RegistrationForm));
			this.RegisterButton = new Button();
			this.DemoButton = new Button();
			this.Label2 = new Label();
			this.RegistrationCodeTextBox = new TextBox();
			base.SuspendLayout();
			this.RegisterButton.DialogResult = DialogResult.OK;
			this.RegisterButton.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.RegisterButton.Location = new Point(180, 108);
			this.RegisterButton.Margin = new Padding(2);
			this.RegisterButton.Name = "RegisterButton";
			this.RegisterButton.Size = new Size(123, 42);
			this.RegisterButton.TabIndex = 0;
			this.RegisterButton.Text = "Register";
			this.RegisterButton.UseVisualStyleBackColor = true;
			this.DemoButton.DialogResult = DialogResult.Cancel;
			this.DemoButton.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.DemoButton.Location = new Point(23, 108);
			this.DemoButton.Margin = new Padding(2);
			this.DemoButton.Name = "DemoButton";
			this.DemoButton.Size = new Size(109, 42);
			this.DemoButton.TabIndex = 1;
			this.DemoButton.Text = "Run Demo";
			this.DemoButton.UseVisualStyleBackColor = true;
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Bold | FontStyle.Underline, GraphicsUnit.Point, 0);
			this.Label2.Location = new Point(20, 52);
			this.Label2.Margin = new Padding(2, 0, 2, 0);
			this.Label2.Name = "Label2";
			this.Label2.Size = new Size(108, 13);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "Registration Code";
			this.RegistrationCodeTextBox.Location = new Point(129, 50);
			this.RegistrationCodeTextBox.Margin = new Padding(2);
			this.RegistrationCodeTextBox.Name = "RegistrationCodeTextBox";
			this.RegistrationCodeTextBox.Size = new Size(176, 20);
			this.RegistrationCodeTextBox.TabIndex = 5;
			base.AutoScaleDimensions = new SizeF(6f, 13f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(356, 195);
			base.Controls.Add(this.RegistrationCodeTextBox);
			base.Controls.Add(this.Label2);
			base.Controls.Add(this.DemoButton);
			base.Controls.Add(this.RegisterButton);
			////base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.Margin = new Padding(2);
			base.Name = "RegistrationForm";
			this.Text = "Registration Form";
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public RegistrationForm()
		{
			base.Load += this.RegistrationForm_Load;
			this.isValid = false;
			this.StartingDate = DateTime.Now;
			this.ExpirationDate = Conversions.ToDate("01/01/2020");
			this.LicenseKey = "AAAABBBBCCCCDDDD";
			this.DataVersion = "0.0";
			this.ProgramVersion = "0.0";
			this.LicenseLength = Conversions.ToInteger("0");
			this.LicenseType = "Demo";
			string text = "0000000000000000";
			this.License = new LicenseDat(ref text);
			this.LicenseLengthTable = new int[12]
			{
				0,
				1,
				2,
				3,
				6,
				9,
				12,
				15,
				18,
				24,
				36,
				48
			};
			this.Dversion = new int[10]
			{
				0,
				1,
				2,
				3,
				4,
				5,
				6,
				7,
				8,
				9
			};
			this.CodeValue = "";
			this.CxCodes = new char[36]
			{
				'A',
				'B',
				'C',
				'D',
				'E',
				'F',
				'G',
				'H',
				'I',
				'J',
				'K',
				'L',
				'M',
				'N',
				'O',
				'P',
				'Q',
				'R',
				'S',
				'T',
				'U',
				'V',
				'W',
				'X',
				'Y',
				'Z',
				'0',
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9'
			};
			try
			{
				MyProject.Computer.Network.Ping("www.google.com");
			}
			catch (Exception ex)
			{
				ProjectData.SetProjectError(ex);
				Exception ex2 = ex;
				Interaction.MsgBox("You must be connected to the internet to complete your registration.\r\nPlease connect and try again.", MsgBoxStyle.OkOnly, null);
				Environment.Exit(0);
				ProjectData.ClearProjectError();
			}
			this.InitializeComponent();
			this.RegistrationCodeTextBox.Text = "";
		}

		private bool ValidateCode(string c)
		{
			bool flag = false;
			bool flag2 = true;
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			string text = allNetworkInterfaces[0].GetPhysicalAddress().ToString();
			MySqlConnection mySqlConnection = new MySqlConnection("server=fretb001.mysql.guardedhost.com; database=fretb001_iosnap_db;uid=fretb001_iosnap_db;password=D-tg2jXmk4g8");
			MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter("Select * from IO_Snap_DB where LicKey=@LicKey", mySqlConnection);
			mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@LicKey", MySqlDbType.VarChar, 50)).Value = c;
			DataTable dataTable = new DataTable();
			mySqlDataAdapter.Fill(dataTable);
			DateTime now2;
			int num = default(int);
			if (dataTable.Rows.Count == 0)
			{
				Interaction.MsgBox("The product key you have entered does not appear to be a valid.", MsgBoxStyle.OkOnly, null);
				try
				{
					Process.Start("http://www.io-snap.com/");
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
				Environment.Exit(0);
			}
			else
			{
				mySqlDataAdapter = new MySqlDataAdapter("Select * from IO_Snap_DB where LicKey=@LicKey And ExpDate=@ExpDate", mySqlConnection);
				mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@LicKey", MySqlDbType.VarChar, 50)).Value = c;
				mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@ExpDate", MySqlDbType.VarChar, 50)).Value = "2001-01-01";
				DataTable dataTable2 = new DataTable();
				mySqlDataAdapter.Fill(dataTable2);
				if (dataTable2.Rows.Count == 1)
				{
					try
					{
						string cmdText = "Update IO_Snap_DB set  MAC1=@MAC1, InsDate=@InsDate, ExpDate=@ExpDate where LicKey='" + c + "'";
						object instance = new MySqlCommand(cmdText, mySqlConnection);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(NewLateBinding.LateGet(instance, null, "Parameters", new object[0], null, null, null), null, "Add", new object[1]
						{
							new MySqlParameter("@MAC1", MySqlDbType.VarChar, 50)
						}, null, null, null), null, "Value", new object[1]
						{
							text
						}, null, null, false, true);
						DateTime now = DateAndTime.Now;
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(NewLateBinding.LateGet(instance, null, "Parameters", new object[0], null, null, null), null, "Add", new object[1]
						{
							new MySqlParameter("@InsDate", MySqlDbType.VarChar, 45)
						}, null, null, null), null, "Value", new object[1]
						{
							now.ToString("yyyy-MM-dd")
						}, null, null, false, true);
						now2 = DateTime.Now;
						DateTime dateTime = now2.AddYears(1);
						NewLateBinding.LateSetComplex(NewLateBinding.LateGet(NewLateBinding.LateGet(instance, null, "Parameters", new object[0], null, null, null), null, "Add", new object[1]
						{
							new MySqlParameter("@ExpDate", MySqlDbType.VarChar, 45)
						}, null, null, null), null, "Value", new object[1]
						{
							dateTime.ToString("yyyy-MM-dd")
						}, null, null, false, true);
						mySqlConnection.Open();
						NewLateBinding.LateCall(instance, null, "ExecuteNonQuery", new object[0], null, null, null, true);
					}
					catch (Exception ex3)
					{
						ProjectData.SetProjectError(ex3);
						Exception ex4 = ex3;
						Interaction.MsgBox(ex4.Message, MsgBoxStyle.OkOnly, null);
						ProjectData.ClearProjectError();
					}
					finally
					{
						mySqlConnection.Close();
					}
					mySqlDataAdapter = new MySqlDataAdapter("Select * from IO_Snap_DB where LicKey=@LicKey And LicType=@LicType", mySqlConnection);
					mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@LicKey", MySqlDbType.VarChar, 50)).Value = c;
					mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@LicType", MySqlDbType.VarChar, 2)).Value = "S";
					DataTable dataTable3 = new DataTable();
					mySqlDataAdapter.Fill(dataTable3);
					num = ((dataTable3.Rows.Count == 1) ? 1 : 5);
				}
				else
				{
					string cmdText2 = "Select * from IO_Snap_DB where LicKey='" + c + "'";
					object instance2 = new MySqlCommand(cmdText2, mySqlConnection);
					mySqlConnection.Open();
					MySqlDataReader mySqlDataReader = (MySqlDataReader)NewLateBinding.LateGet(instance2, null, "ExecuteReader", new object[0], null, null, null);
					DateTime dateTime2 = default(DateTime);
					while (mySqlDataReader.Read())
					{
						dateTime2 = mySqlDataReader.GetDateTime(4);
						RegistrationForm.SDate = mySqlDataReader.GetDateTime(3);
					}
					mySqlConnection.Close();
					DateTime today = DateTime.Today;
					if (DateTime.Compare(dateTime2, today) < 0)
					{
						Interaction.MsgBox("Your license has expired. Please renew license.", MsgBoxStyle.OkOnly, null);
						try
						{
							Process.Start("http://www.io-snap.com/");
						}
						catch (Exception ex5)
						{
							ProjectData.SetProjectError(ex5);
							Exception ex6 = ex5;
							Interaction.MsgBox(ex6.Message, MsgBoxStyle.OkOnly, null);
							ProjectData.ClearProjectError();
						}
						Environment.Exit(0);
					}
					else
					{
						mySqlConnection.Open();
						mySqlDataReader = (MySqlDataReader)NewLateBinding.LateGet(instance2, null, "ExecuteReader", new object[0], null, null, null);
						mySqlDataReader.Read();
						int num2 = 0;
						int num3 = Conversions.ToInteger(mySqlDataReader[5]);
						checked
						{
							int num4 = 5 + num3;
							for (int i = 6; i <= num4; i++)
							{
								if (Operators.CompareString(Convert.ToString(RuntimeHelpers.GetObjectValue(mySqlDataReader[i])), text, false) == 0)
								{
									num2 = 1;
								}
							}
							mySqlConnection.Close();
							if (num2 == 0)
							{
								mySqlConnection.Open();
								mySqlDataReader = unchecked((MySqlDataReader)NewLateBinding.LateGet(instance2, null, "ExecuteReader", new object[0], null, null, null));
								mySqlDataReader.Read();
								int num5 = 0;
								int num6 = 5 + num3;
								for (int j = 6; j <= num6; j++)
								{
									if (Operators.CompareString(Convert.ToString(RuntimeHelpers.GetObjectValue(mySqlDataReader[j])), "0", false) == 0)
									{
										num5 = j;
									}
								}
								mySqlConnection.Close();
								if (num5 == 0)
								{
									Interaction.MsgBox("You have reached the maximum Key usage limitation.", MsgBoxStyle.OkOnly, null);
									try
									{
										Process.Start("http://www.io-snap.com/");
									}
									catch (Exception ex7)
									{
										ProjectData.SetProjectError(ex7);
										Exception ex8 = ex7;
										Interaction.MsgBox(ex8.Message, MsgBoxStyle.OkOnly, null);
										ProjectData.ClearProjectError();
									}
									Environment.Exit(0);
								}
								else
								{
									num5 -= 5;
									try
									{
										string text2 = "MAC" + Convert.ToString(num5);
										string cmdText3 = "Update IO_Snap_DB set  " + text2 + " =@" + text2 + " where LicKey='" + c + "'";
										object instance3 = new MySqlCommand(cmdText3, mySqlConnection);
										mySqlConnection.Open();
										NewLateBinding.LateSetComplex(NewLateBinding.LateGet(NewLateBinding.LateGet(instance3, null, "Parameters", new object[0], null, null, null), null, "Add", new object[1]
										{
											new MySqlParameter(("@" + text2) ?? "", MySqlDbType.VarChar, 50)
										}, null, null, null), null, "Value", new object[1]
										{
											text
										}, null, null, false, true);
										NewLateBinding.LateCall(instance3, null, "ExecuteNonQuery", new object[0], null, null, null, true);
									}
									catch (Exception ex9)
									{
										ProjectData.SetProjectError(ex9);
										Exception ex10 = ex9;
										Interaction.MsgBox(ex10.Message, MsgBoxStyle.OkOnly, null);
										ProjectData.ClearProjectError();
									}
									finally
									{
										mySqlConnection.Close();
									}
									mySqlDataAdapter = new MySqlDataAdapter("Select * from IO_Snap_DB where LicKey=@LicKey And LicType=@LicType", mySqlConnection);
									mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@LicKey", MySqlDbType.VarChar, 50)).Value = c;
									mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@LicType", MySqlDbType.VarChar, 2)).Value = "S";
									DataTable dataTable4 = new DataTable();
									mySqlDataAdapter.Fill(dataTable4);
									num = ((dataTable4.Rows.Count == 1) ? 1 : 5);
								}
							}
							else
							{
								mySqlDataAdapter = new MySqlDataAdapter("Select * from IO_Snap_DB where LicKey=@LicKey And LicType=@LicType", mySqlConnection);
								mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@LicKey", MySqlDbType.VarChar, 50)).Value = c;
								mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@LicType", MySqlDbType.VarChar, 2)).Value = "S";
								DataTable dataTable5 = new DataTable();
								mySqlDataAdapter.Fill(dataTable5);
								num = ((dataTable5.Rows.Count == 1) ? 1 : 5);
							}
						}
					}
				}
			}
			if (DateTime.Compare(RegistrationForm.SDate, Conversions.ToDate("2001-01-01")) == 0)
			{
				this.StartingDate = DateAndTime.Now;
				switch (num)
				{
				case 1:
					now2 = DateAndTime.Now;
					this.ExpirationDate = now2.AddYears(1);
					break;
				case 3:
					now2 = DateAndTime.Now;
					this.ExpirationDate = now2.AddMonths(3);
					break;
				case 5:
					now2 = DateAndTime.Now;
					this.ExpirationDate = now2.AddYears(1);
					break;
				}
			}
			else
			{
				this.StartingDate = RegistrationForm.SDate;
				switch (num)
				{
				case 1:
					this.ExpirationDate = RegistrationForm.SDate.AddYears(1);
					break;
				case 3:
					now2 = DateAndTime.Now;
					this.ExpirationDate = now2.AddMonths(3);
					break;
				case 5:
					this.ExpirationDate = RegistrationForm.SDate.AddYears(1);
					break;
				}
			}
			if (flag2)
			{
				this.ProgramVersion = Application.ProductVersion.ToString();
				this.DataVersion = "1.0";
				this.LicenseLength = 12;
				switch (num)
				{
				case 1:
					this.LicenseType = "Student";
					break;
				case 3:
					this.LicenseType = "Demo";
					mySqlConnection = new MySqlConnection("server=fretb001.mysql.guardedhost.com; database=fretb001_iosnap_db;uid=fretb001_iosnap_db;password=D-tg2jXmk4g8");
					mySqlDataAdapter = new MySqlDataAdapter("Select * from IO_Snap_Demo_DB where MAC=@MAC", mySqlConnection);
					mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@MAC", MySqlDbType.VarChar, 50)).Value = text;
					mySqlDataAdapter.Fill(dataTable);
					if (dataTable.Rows.Count == 0)
					{
						string cmdText4 = "insert into IO_Snap_Demo_DB (MAC) values (@MAC)";
						object instance4 = new MySqlCommand(cmdText4, mySqlConnection);
						object instance5 = NewLateBinding.LateGet(instance4, null, "Parameters", new object[0], null, null, null);
						object[] obj = new object[2]
						{
							"@MAC",
							text
						};
						object[] array = obj;
						bool[] obj2 = new bool[2]
						{
							false,
							true
						};
						bool[] array2 = obj2;
						NewLateBinding.LateCall(instance5, null, "AddWithValue", obj, null, null, obj2, true);
						if (array2[1])
						{
							text = (string)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(array[1]), typeof(string));
						}
						mySqlConnection.Open();
						NewLateBinding.LateCall(instance4, null, "ExecuteNonQuery", new object[0], null, null, null, true);
						mySqlConnection.Close();
					}
					else
					{
						Interaction.MsgBox("You have reached the demo usage limit.\r\nPlease acquire full version If you wish To Continue.\r\nThe program will now quit.", MsgBoxStyle.OkOnly, null);
						try
						{
							Process.Start("http://www.io-snap.com/");
						}
						catch (Exception ex11)
						{
							ProjectData.SetProjectError(ex11);
							Exception ex12 = ex11;
							Interaction.MsgBox(ex12.Message, MsgBoxStyle.OkOnly, null);
							ProjectData.ClearProjectError();
						}
						Environment.Exit(0);
					}
					break;
				case 5:
					this.LicenseType = "Full";
					break;
				default:
					flag2 = false;
					this.LicenseType = "Unknown";
					break;
				}
			}
			if (flag2)
			{
				flag = true;
			}
			if (flag)
			{
				this.License = new LicenseDat(ref c);
				this.License.ExpirationDate = this.ExpirationDate;
				this.License.StartingDate = this.StartingDate;
				this.License.LicenseLength = this.LicenseLength;
				this.License.LicenseType = this.LicenseType;
				this.License.DataVersion = this.DataVersion;
				this.License.ProgramVersion = this.ProgramVersion;
				this.License.ActualMAC = this.getMAC();
				this.OldLicenseDelet();
				this.License.saveLicense();
			}
			return flag;
		}

		public object OldLicenseDelet()
		{
			string text = "";
			text = FileSystem.CurDir() + "\\License.dat";
			object result = default(object);
			if (File.Exists(text))
			{
				try
				{
					File.Delete(text);
					return result;
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
					return result;
				}
			}
			return result;
		}

		private int getIntVal(char c, int r, char s)
		{
			int num = 0;
			char c2 = this.RecodeBy(c, r);
			return this.getNumDiff(c2, s);
		}

		private int getNumDiff(char c1, char c2)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			checked
			{
				int num4 = this.CxCodes.Length - 1;
				for (int i = 0; i <= num4; i++)
				{
					if (this.CxCodes[i] == c1)
					{
						num2 = i;
					}
					if (this.CxCodes[i] == c2)
					{
						num3 = i;
					}
				}
				if (num3 >= num2)
				{
					return num3 - num2;
				}
				return this.CxCodes.Length - num2 + num3;
			}
		}

		private string getMAC()
		{
			string result = "";
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			int num = 0;
			NetworkInterface[] array = allNetworkInterfaces;
			foreach (NetworkInterface networkInterface in array)
			{
				if (num == 0)
				{
					result = networkInterface.GetPhysicalAddress().ToString();
				}
				num = checked(num + 1);
			}
			return result;
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			this.LicenseKey = this.RegistrationCodeTextBox.Text.ToString();
			this.isValid = this.ValidateCode(this.LicenseKey);
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			DataTable dataTable = new DataTable();
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			string text = allNetworkInterfaces[0].GetPhysicalAddress().ToString();
			MySqlConnection mySqlConnection = new MySqlConnection("server=fretb001.mysql.guardedhost.com; database=fretb001_iosnap_db;uid=fretb001_iosnap_db;password=D-tg2jXmk4g8");
			this.LicenseType = "Demo";
			this.OldLicenseDelet();
			mySqlConnection = new MySqlConnection("server=fretb001.mysql.guardedhost.com; database=fretb001_iosnap_db;uid=fretb001_iosnap_db;password=D-tg2jXmk4g8");
			MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter("Select * from IO_Snap_Demo_DB where MAC=@MAC", mySqlConnection);
			mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter("@MAC", MySqlDbType.VarChar, 50)).Value = text;
			mySqlDataAdapter.Fill(dataTable);
			if (dataTable.Rows.Count == 0)
			{
				string cmdText = "insert into IO_Snap_Demo_DB (MAC) values (@MAC)";
				object instance = new MySqlCommand(cmdText, mySqlConnection);
				object instance2 = NewLateBinding.LateGet(instance, null, "Parameters", new object[0], null, null, null);
				object[] obj = new object[2]
				{
					"@MAC",
					text
				};
				object[] array = obj;
				bool[] obj2 = new bool[2]
				{
					false,
					true
				};
				bool[] array2 = obj2;
				NewLateBinding.LateCall(instance2, null, "AddWithValue", obj, null, null, obj2, true);
				if (array2[1])
				{
					text = (string)Conversions.ChangeType(RuntimeHelpers.GetObjectValue(array[1]), typeof(string));
				}
				mySqlConnection.Open();
				NewLateBinding.LateCall(instance, null, "ExecuteNonQuery", new object[0], null, null, null, true);
				mySqlConnection.Close();
				this.StartingDate = DateAndTime.Now;
				string text2 = "0000";
				this.License = new LicenseDat(ref text2);
				this.ExpirationDate = DateAndTime.Now.AddMonths(3);
				this.License.ExpirationDate = this.ExpirationDate;
				this.License.StartingDate = this.StartingDate;
				this.License.LicenseLength = this.LicenseLength;
				this.License.LicenseType = this.LicenseType;
				this.License.DataVersion = this.DataVersion;
				this.License.ProgramVersion = this.ProgramVersion;
				this.License.ActualMAC = this.getMAC();
				this.License.saveLicense();
			}
			else
			{
				Interaction.MsgBox("You have reached the demo usage limit.\r\nPlease acquire full version If you wish to continue.\r\nThe program will now quit.", MsgBoxStyle.OkOnly, null);
				try
				{
					Process.Start("http://www.io-snap.com/");
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
				Environment.Exit(0);
			}
			this.isValid = false;
		}

		private char RecodeBy(char Chart, int Nidx)
		{
			char c = char.ToUpper(Chart);
			int num = 0;
			checked
			{
				int num2 = this.CxCodes.Length - 1;
				for (int i = 0; i <= num2; i++)
				{
					if (this.CxCodes[i] == c)
					{
						num = i;
					}
				}
				num += Nidx;
				if (num < 0)
				{
					num = -1 * num;
				}
				while (num >= this.CxCodes.Length)
				{
					num -= this.CxCodes.Length;
				}
				return this.CxCodes[num];
			}
		}

		private void RegistrationForm_Load(object sender, EventArgs e)
		{
		}
	}
}
