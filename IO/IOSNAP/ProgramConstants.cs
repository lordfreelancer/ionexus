namespace IOSNAP
{
	public class ProgramConstants
	{
		public const string PROGRAM_VERSION = "1.0.0";

		public const string DATA_VERSION = "1.0.2012.1";
	}
}
