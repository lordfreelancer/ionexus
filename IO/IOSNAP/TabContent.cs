using Matrix_Lib;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace IOSNAP
{
	[Serializable]
	public class TabContent : TabPage
	{
        public static bool DispTot;
        private enum DispTotal : byte
		{
			Yes,
			No,
			Unchanged
		}

		public usrDataGridView dgvnn;

		private string _frmTitle;

		private DispTotal DispTotals;

		public Size tabsize;

		public string dispFormatGeneral;

		public string dispFormatRounded;

		public string dispFormatCoefficients;

		public ViewOptions view_options;

		private int cellwidth;

		public DispData dispdat;

		public bool hasEmployment;

		private bool bCoeff;

		private Vector colSum;

		public TabContent(string frmTitle, byte p2)
		{
			this.dispFormatGeneral = "";
			this.dispFormatRounded = "";
			this.dispFormatCoefficients = "";
			this.cellwidth = 150;
			this.hasEmployment = false;
			this.bCoeff = false;
			this._frmTitle = frmTitle;
			this.DispTotals = (DispTotal)p2;
			this.view_options = new ViewOptions();
		}

		public void setup(ref SummaryDat ws, ref bool copyHeadersStatus)
		{
			this.view_options.GenFormBase = this.dispFormatGeneral;
			this.view_options.CoeffFormatBase = this.dispFormatCoefficients;
			this.view_options.RoundFormatBase = this.dispFormatRounded;
			this.hasEmployment = this.dispdat.hasEmployment;
			this.Text = this._frmTitle;
			this.dgvnn = new usrDataGridView();
			this.dgvnn.BackgroundImage = Image.FromFile("background.jpg");
			this.dgvnn.AllowUserToAddRows = false;
			if (copyHeadersStatus)
			{
				this.dgvnn.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
			}
			else
			{
				this.dgvnn.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
			}
			base.Controls.Add(this.dgvnn);
			this.dgvnn.SelectionMode = DataGridViewSelectionMode.CellSelect;
			this.dgvnn.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvnn.Size = this.tabsize;
			this.dgvnn.RightToLeft = RightToLeft.No;
			this.dgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnn.RowHeadersWidth = 175;
			this.dgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvnn.RowTemplate.Height = 24;
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle.BackColor = SystemColors.Control;
			dataGridViewCellStyle.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvnn.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle2.BackColor = SystemColors.Window;
			dataGridViewCellStyle2.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle2.Format = this.view_options.getGeneralFormat;
			dataGridViewCellStyle2.NullValue = null;
			dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
			this.dgvnn.DefaultCellStyle = dataGridViewCellStyle2;
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = SystemColors.Control;
			dataGridViewCellStyle3.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle3.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = DataGridViewTriState.True;
			this.dgvnn.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			switch (this.DispTotals)
			{
			case DispTotal.No:
				DispTot = false;
				break;
			case DispTotal.Yes:
				DispTot = true;
				break;
			}
			if (this.bCoeff | DispTot)
			{
				this.colSum = this.dispdat.getColSum();
			}
			usrDataGridView usrDataGridView = this.dgvnn;
			usrDataGridView.Columns.Clear();
			string[] hdrsCol = this.dispdat.hdrsCol;
			foreach (string text in hdrsCol)
			{
				usrDataGridView.Columns.Add(text, text);
			}
			checked
			{
				if (this.dispdat.BaseTable.NoCols > this.dispdat.hdrsCol.Length)
				{
					int num = this.dispdat.hdrsCol.Length;
					int num2 = this.dispdat.BaseTable.NoCols - 1;
					for (int j = num; j <= num2; j++)
					{
						usrDataGridView.Columns.Add("Col " + Conversions.ToString(j), "Col " + Conversions.ToString(j));
					}
				}
				this.SetFormatContent();
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = usrDataGridView.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = unchecked((DataGridViewColumn)enumerator.Current);
						dataGridViewColumn.ReadOnly = true;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				usrDataGridView.RowHeadersVisible = true;
				int num3 = this.dispdat.BaseTable.NoRows - 1;
				for (int j = 0; j <= num3; j++)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					int num4 = this.dispdat.BaseTable.NoCols - 1;
					for (int k = 0; k <= num4; k++)
					{
						dataGridViewRow.Cells[k].Value = this.dispdat.BaseTable[j, k];
						if (this.dispdat.showCellColor)
						{
							try
							{
								if (this.dispdat.ColorTable[j, k] > 0.0)
								{
									dataGridViewRow.Cells[k].Style.BackColor = Color.LightYellow;
								}
							}
							catch (Exception ex)
							{
								ProjectData.SetProjectError(ex);
								Exception ex2 = ex;
								ProjectData.ClearProjectError();
							}
						}
					}
					dataGridViewRow.HeaderCell.Value = this.dispdat.hdrsRow[j];
					usrDataGridView.Rows.Add(dataGridViewRow);
				}
				if (unchecked((object)this.dispdat.ExtraCols) != null)
				{
					int noCols = this.dispdat.BaseTable.NoCols;
					int num5 = this.dispdat.ExtraCols.NoCols - 1;
					for (int l = 0; l <= num5; l++)
					{
						int index = usrDataGridView.Columns.Add(this.dispdat.hdrsXCols[l], this.dispdat.hdrsXCols[l]);
						usrDataGridView.Columns[index].DefaultCellStyle.BackColor = Color.LightBlue;
						int num6 = this.dispdat.ExtraCols.NoRows - 1;
						for (int j = 0; j <= num6; j++)
						{
							usrDataGridView.Rows[j].Cells[noCols + l].Value = this.dispdat.ExtraCols[j, l];
						}
						usrDataGridView.Columns[index].ReadOnly = true;
					}
				}
				if (unchecked((object)this.dispdat.ExtraRows) != null)
				{
					int num7 = this.dispdat.ExtraRows.NoRows - 1;
					for (int j = 0; j <= num7; j++)
					{
						DataGridViewRow dataGridViewRow = new DataGridViewRow();
						dataGridViewRow.CreateCells(this.dgvnn);
						dataGridViewRow.DefaultCellStyle.BackColor = Color.LightBlue;
						dataGridViewRow.HeaderCell.Value = this.dispdat.hdrsXRows[j];
						int num8 = this.dispdat.ExtraRows.NoCols - 1;
						for (int m = 0; m <= num8; m++)
						{
							if (this.bCoeff)
							{
								dataGridViewRow.Cells[m].Value = unchecked(this.dispdat.ExtraRows[j, m] / this.colSum[m]);
							}
							else
							{
								dataGridViewRow.Cells[m].Value = this.dispdat.ExtraRows[j, m];
							}
						}
						usrDataGridView.Rows.Add(dataGridViewRow);
					}
				}
				IEnumerator enumerator2 = default(IEnumerator);
				try
				{
					enumerator2 = usrDataGridView.Columns.GetEnumerator();
					while (enumerator2.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn2 = unchecked((DataGridViewColumn)enumerator2.Current);
						dataGridViewColumn2.Width = this.cellwidth;
						if (this.dispdat.isSortable)
						{
							dataGridViewColumn2.SortMode = DataGridViewColumnSortMode.Automatic;
						}
						else
						{
							dataGridViewColumn2.SortMode = DataGridViewColumnSortMode.NotSortable;
						}
					}
				}
				finally
				{
					if (enumerator2 is IDisposable)
					{
						(enumerator2 as IDisposable).Dispose();
					}
				}
				usrDataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
				usrDataGridView = null;
				if (DispTot)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					dataGridViewRow.ReadOnly = true;
					dataGridViewRow.DefaultCellStyle.BackColor = Color.LightGray;
					dataGridViewRow.HeaderCell.Value = "Column Sum";
					int num9 = this.dispdat.BaseTable.NoCols - 1;
					for (int n = 0; n <= num9; n++)
					{
						dataGridViewRow.Cells[n].Value = this.colSum[n];
					}
					this.dgvnn.Rows.Add(dataGridViewRow);
					int count = this.dgvnn.Columns.Count;
					int index2 = this.dgvnn.Columns.Add("RowSum", "Row Sum");
					this.dgvnn.Columns[index2].DefaultCellStyle.BackColor = Color.LightGray;
					Vector vector = this.dispdat.BaseTable.RowSum();
					if (unchecked((object)this.dispdat.ExtraCols) != null)
					{
						int num10 = vector.Count - 1;
						for (int j = 0; j <= num10; j++)
						{
							int num11 = this.dispdat.ExtraCols.NoCols - 1;
							for (int num12 = 0; num12 <= num11; num12++)
							{
								Vector vector2;
								int row;
								(vector2 = vector)[row = j] = unchecked(vector2[row] + this.dispdat.ExtraCols[j, num12]);
							}
						}
					}
					int num13 = vector.Count - 1;
					for (int j = 0; j <= num13; j++)
					{
						this.dgvnn.Rows[j].Cells[count].Value = vector[j];
					}
				}
				this.dgvnn.TopLeftHeaderCell.Value = ws.datayear;
				this.dgvnn.TopLeftHeaderCell.Style.Font = new Font("Calibri", 14f, FontStyle.Bold, GraphicsUnit.Point, 0);
			}
			IEnumerator enumerator3 = default(IEnumerator);
			try
			{
				enumerator3 = this.dgvnn.Columns.GetEnumerator();
				while (enumerator3.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn3 = (DataGridViewColumn)enumerator3.Current;
					dataGridViewColumn3.MinimumWidth = this.cellwidth;
					dataGridViewColumn3.SortMode = DataGridViewColumnSortMode.NotSortable;
					dataGridViewColumn3.ReadOnly = false;
				}
			}
			finally
			{
				if (enumerator3 is IDisposable)
				{
					(enumerator3 as IDisposable).Dispose();
				}
			}
			this.dispdat.showCellColor = false;
		}

		public void replaceDataGridView(ref DataGridView dsg)
		{
			this.dgvnn.Rows.Clear();
		}

		public void SetFormatContent()
		{
			usrDataGridView usrDataGridView = this.dgvnn;
			if (this.dispdat.useGeneralFormatOnly)
			{
				usrDataGridView.DefaultCellStyle.Format = this.view_options.getGeneralFormat;
			}
			else
			{
				int num = 0;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = usrDataGridView.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
						try
						{
							switch (this.dispdat.DataFormatTable[num])
							{
							case 1:
								dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getGeneralFormat;
								break;
							case 2:
								dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getCoefficientFormat;
								break;
							case 3:
								dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getRoundFormat;
								break;
							default:
								dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getGeneralFormat;
								break;
							}
						}
						catch (Exception ex)
						{
							ProjectData.SetProjectError(ex);
							Exception ex2 = ex;
							dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getGeneralFormat;
							ProjectData.ClearProjectError();
						}
						num = checked(num + 1);
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
			}
			usrDataGridView = null;
		}

		private void InitializeComponent()
		{
			base.SuspendLayout();
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.ResumeLayout(false);
		}
	}
}
