using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class FormEditState : Form
	{
		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label4")]
		private Label _Label4;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("headersListBox")]
		private ListBox _headersListBox;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("label_ElementEdited")]
		private Label _label_ElementEdited;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("tbar")]
		private TrackBar _tbar;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("tbar2")]
		private TrackBar _tbar2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("UndoButton")]
		private Button _UndoButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("dgvnn")]
		private DataGridView _dgvnn;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		private BEAData bdat;

		private string[] headersCols;

		private ArrayList headersRows;

		public ViewOptions view_options;

		public bool isEdited;

		private Vector modifiedColumn;

		private Color non_edit_color;

		private Color edit_color;

		private int cellwidth;

		public StateData statedat;

		private string statecode;

		public bool isChangeApproved;

		public StateData newStatData;

		internal virtual Label Label4
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual ListBox headersListBox
		{
			[CompilerGenerated]
			get
			{
				return this._headersListBox;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.ListBoxRegions_SelectedIndexChanged;
				ListBox headersListBox = this._headersListBox;
				if (headersListBox != null)
				{
					headersListBox.SelectedIndexChanged -= value2;
				}
				this._headersListBox = value;
				headersListBox = this._headersListBox;
				if (headersListBox != null)
				{
					headersListBox.SelectedIndexChanged += value2;
				}
			}
		}

		internal virtual Label label_ElementEdited
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual TrackBar tbar
		{
			[CompilerGenerated]
			get
			{
				return this._tbar;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.tbar_Changed;
				TrackBar tbar = this._tbar;
				if (tbar != null)
				{
					tbar.ValueChanged -= value2;
				}
				this._tbar = value;
				tbar = this._tbar;
				if (tbar != null)
				{
					tbar.ValueChanged += value2;
				}
			}
		}

		internal virtual TrackBar tbar2
		{
			[CompilerGenerated]
			get
			{
				return this._tbar2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.tbar2_Changed;
				TrackBar tbar = this._tbar2;
				if (tbar != null)
				{
					tbar.ValueChanged -= value2;
				}
				this._tbar2 = value;
				tbar = this._tbar2;
				if (tbar != null)
				{
					tbar.ValueChanged += value2;
				}
			}
		}

		internal virtual Button UndoButton
		{
			[CompilerGenerated]
			get
			{
				return this._UndoButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.UndoButton_Click;
				Button undoButton = this._UndoButton;
				if (undoButton != null)
				{
					undoButton.Click -= value2;
				}
				this._UndoButton = value;
				undoButton = this._UndoButton;
				if (undoButton != null)
				{
					undoButton.Click += value2;
				}
			}
		}

		internal virtual Button Button1
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual DataGridView dgvnn
		{
			[CompilerGenerated]
			get
			{
				return this._dgvnn;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				DataGridViewCellEventHandler value2 = this.dgv3_CellClick;
				DataGridViewCellEventHandler value3 = this.dgv3_CellChange;
				DataGridViewCellEventHandler value4 = this.dgv3_CellValueChanged;
				DataGridViewCellMouseEventHandler value5 = this.CellHeader_Click;
				DataGridView dgvnn = this._dgvnn;
				if (dgvnn != null)
				{
					dgvnn.CellClick -= value2;
					dgvnn.CellEndEdit -= value3;
					dgvnn.CellValueChanged -= value4;
					dgvnn.RowHeaderMouseClick -= value5;
				}
				this._dgvnn = value;
				dgvnn = this._dgvnn;
				if (dgvnn != null)
				{
					dgvnn.CellClick += value2;
					dgvnn.CellEndEdit += value3;
					dgvnn.CellValueChanged += value4;
					dgvnn.RowHeaderMouseClick += value5;
				}
			}
		}

		internal virtual Button Button2
		{
			[CompilerGenerated]
			get
			{
				return this._Button2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Button2_Click;
				Button button = this._Button2;
				if (button != null)
				{
					button.Click -= value2;
				}
				this._Button2 = value;
				button = this._Button2;
				if (button != null)
				{
					button.Click += value2;
				}
			}
		}

		internal virtual Label Label2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.Label4 = new Label();
			this.headersListBox = new ListBox();
			this.label_ElementEdited = new Label();
			this.tbar = new TrackBar();
			this.tbar2 = new TrackBar();
			this.UndoButton = new Button();
			this.Button1 = new Button();
			this.dgvnn = new DataGridView();
			this.Button2 = new Button();
			this.Label2 = new Label();
			((ISupportInitialize)this.tbar).BeginInit();
			((ISupportInitialize)this.tbar2).BeginInit();
			((ISupportInitialize)this.dgvnn).BeginInit();
			base.SuspendLayout();
			this.Label4.AutoSize = true;
			this.Label4.Font = new Font("Calibri", 24f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label4.ForeColor = SystemColors.HotTrack;
			this.Label4.Location = new Point(30, 10);
			this.Label4.Name = "Label4";
			this.Label4.Size = new Size(87, 39);
			this.Label4.TabIndex = 36;
			this.Label4.Text = "State";
			this.headersListBox.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			this.headersListBox.FormattingEnabled = true;
			this.headersListBox.ItemHeight = 14;
			this.headersListBox.Location = new Point(40, 84);
			this.headersListBox.Margin = new Padding(3, 4, 3, 4);
			this.headersListBox.Name = "headersListBox";
			this.headersListBox.Size = new Size(351, 704);
			this.headersListBox.TabIndex = 37;
			this.label_ElementEdited.AutoSize = true;
			this.label_ElementEdited.Font = new Font("Calibri", 11f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.label_ElementEdited.ForeColor = SystemColors.HotTrack;
			this.label_ElementEdited.Location = new Point(613, 46);
			this.label_ElementEdited.Name = "label_ElementEdited";
			this.label_ElementEdited.Size = new Size(48, 18);
			this.label_ElementEdited.TabIndex = 38;
			this.label_ElementEdited.Text = "Label4";
			this.tbar.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			this.tbar.Location = new Point(12, 84);
			this.tbar.Margin = new Padding(3, 4, 3, 4);
			this.tbar.Name = "tbar";
			this.tbar.Orientation = Orientation.Vertical;
			this.tbar.RightToLeft = RightToLeft.No;
			this.tbar.RightToLeftLayout = true;
			this.tbar.Size = new Size(45, 709);
			this.tbar.TabIndex = 39;
			this.tbar.TickStyle = TickStyle.None;
			this.tbar2.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			this.tbar2.LargeChange = 1;
			this.tbar2.Location = new Point(397, 99);
			this.tbar2.Margin = new Padding(3, 4, 3, 4);
			this.tbar2.Name = "tbar2";
			this.tbar2.Orientation = Orientation.Vertical;
			this.tbar2.RightToLeft = RightToLeft.No;
			this.tbar2.RightToLeftLayout = true;
			this.tbar2.Size = new Size(45, 621);
			this.tbar2.TabIndex = 40;
			this.tbar2.TickStyle = TickStyle.None;
			this.UndoButton.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.UndoButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.UndoButton.Location = new Point(617, 743);
			this.UndoButton.Margin = new Padding(3, 4, 3, 4);
			this.UndoButton.Name = "UndoButton";
			this.UndoButton.Size = new Size(140, 50);
			this.UndoButton.TabIndex = 43;
			this.UndoButton.Text = "Reset";
			this.UndoButton.UseVisualStyleBackColor = true;
			this.Button1.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.Button1.DialogResult = DialogResult.Cancel;
			this.Button1.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button1.Location = new Point(850, 743);
			this.Button1.Margin = new Padding(3, 4, 3, 4);
			this.Button1.Name = "Button1";
			this.Button1.Size = new Size(140, 50);
			this.Button1.TabIndex = 42;
			this.Button1.Text = "Cancel";
			this.Button1.UseVisualStyleBackColor = true;
			this.dgvnn.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvnn.BackgroundColor = SystemColors.Control;
			this.dgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnn.Location = new Point(442, 99);
			this.dgvnn.Margin = new Padding(3, 4, 3, 4);
			this.dgvnn.Name = "dgvnn";
			this.dgvnn.RowTemplate.Height = 28;
			this.dgvnn.Size = new Size(730, 621);
			this.dgvnn.TabIndex = 44;
			this.Button2.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.Button2.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button2.Location = new Point(1026, 743);
			this.Button2.Margin = new Padding(3, 4, 3, 4);
			this.Button2.Name = "Button2";
			this.Button2.Size = new Size(136, 50);
			this.Button2.TabIndex = 45;
			this.Button2.Text = "Proceed";
			this.Button2.UseVisualStyleBackColor = true;
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Calibri", 9.75f, FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label2.ForeColor = Color.Red;
			this.Label2.Location = new Point(448, 80);
			this.Label2.Name = "Label2";
			this.Label2.Size = new Size(686, 15);
			this.Label2.TabIndex = 47;
			this.Label2.Text = "Please edit highlighted rows. Gross Product must equal the sum of Employee Compensation, Gross Operating Surplus, and Taxes.";
			base.AutoScaleDimensions = new SizeF(6f, 14f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(1184, 822);
			base.Controls.Add(this.Label2);
			base.Controls.Add(this.Button2);
			base.Controls.Add(this.dgvnn);
			base.Controls.Add(this.UndoButton);
			base.Controls.Add(this.Button1);
			base.Controls.Add(this.label_ElementEdited);
			base.Controls.Add(this.headersListBox);
			base.Controls.Add(this.Label4);
			base.Controls.Add(this.tbar);
			base.Controls.Add(this.tbar2);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.Margin = new Padding(3, 4, 3, 4);
			base.MinimizeBox = false;
			this.MinimumSize = new Size(1192, 860);
			base.Name = "FormEditState";
			this.Text = "State data editing form";
			((ISupportInitialize)this.tbar).EndInit();
			((ISupportInitialize)this.tbar2).EndInit();
			((ISupportInitialize)this.dgvnn).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public FormEditState(ref BEAData b, string st_code, string stname)
		{
			base.Load += this.Form_Load;
			this.headersCols = new string[5]
			{
				"Employment",
				"Employee Compensation",
				"Gross Operating Surplus",
				"Taxes",
				"Gross Regional product"
			};
			this.headersRows = new ArrayList();
			this.isEdited = false;
			this.non_edit_color = Color.LightGray;
			this.edit_color = SystemColors.Window;
			this.cellwidth = 150;
			this.isChangeApproved = false;
			this.InitializeComponent();
			this.bdat = b;
			this.statecode = st_code;
			this.statedat = this.bdat.RegStateData.GetState(ref this.statecode);
			this.newStatData = (StateData)this.statedat.Clone();
			checked
			{
				int num = this.bdat.NTables.Make().NoRows - 1;
				for (int i = 0; i <= num; i++)
				{
					this.headersRows.Add(this.bdat.NTables.hdrInd[i]);
				}
				this.tbar.Maximum = this.headersRows.Count - 1;
				this.tbar.Minimum = 0;
				this.tbar.Value = this.headersRows.Count - 1;
				this.tbar2.Maximum = this.headersRows.Count - 1;
				this.tbar2.Minimum = 0;
				this.tbar2.Value = this.headersRows.Count - 1;
				int num2 = 0;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.headersRows.GetEnumerator();
					while (enumerator.MoveNext())
					{
						string item = Conversions.ToString(enumerator.Current);
						num2++;
						try
						{
							this.headersListBox.Items.Add(item);
						}
						catch (Exception ex)
						{
							ProjectData.SetProjectError(ex);
							Exception ex2 = ex;
							Interaction.MsgBox(ex2.Message + Conversions.ToString(num2), MsgBoxStyle.OkOnly, null);
							ProjectData.ClearProjectError();
						}
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				this.label_ElementEdited.Text = this.headersCols[0];
				this.headersListBox.SelectedIndex = 0;
				this.Label4.Text = stname + "   (" + st_code + ")";
				this.Label2.Hide();
				this.view_options = new ViewOptions();
			}
		}

		private void Form_Load(object sender, EventArgs e)
		{
			this.dgvnn.Columns.Clear();
			this.dgvnn.RightToLeft = RightToLeft.No;
			this.dgvnn.RowHeadersWidth = 275;
			this.dgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvnn.RowTemplate.Height = 24;
			checked
			{
				int num = this.headersCols.Length - 1;
				for (int i = 0; i <= num; i++)
				{
					string text = this.headersCols[i];
					this.dgvnn.Columns.Add(text, text);
				}
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.dgvnn.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = unchecked((DataGridViewColumn)enumerator.Current);
						dataGridViewColumn.MinimumWidth = 150;
						dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				this.dgvnn.RowHeadersVisible = true;
				this.dgvnn.AllowUserToAddRows = false;
				int num2 = this.statedat.getEmployment().Count - 1;
				for (int j = 0; j <= num2; j++)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					decimal d = new decimal(this.statedat.getEmployment()[j]);
					DataGridViewCell dataGridViewCell = dataGridViewRow.Cells[0];
					decimal num3 = decimal.Round(d, 2);
					dataGridViewCell.Value = num3.ToString("f2");
					d = new decimal(this.statedat.getEC()[j]);
					DataGridViewCell dataGridViewCell2 = dataGridViewRow.Cells[1];
					num3 = decimal.Round(d, 2);
					dataGridViewCell2.Value = num3.ToString("f2");
					d = new decimal(this.statedat.getGOS()[j]);
					DataGridViewCell dataGridViewCell3 = dataGridViewRow.Cells[2];
					num3 = decimal.Round(d, 2);
					dataGridViewCell3.Value = num3.ToString("f2");
					d = new decimal(this.statedat.getT()[j]);
					DataGridViewCell dataGridViewCell4 = dataGridViewRow.Cells[3];
					num3 = decimal.Round(d, 2);
					dataGridViewCell4.Value = num3.ToString("f2");
					d = new decimal(this.statedat.getGDP()[j]);
					DataGridViewCell dataGridViewCell5 = dataGridViewRow.Cells[4];
					num3 = decimal.Round(d, 2);
					dataGridViewCell5.Value = num3.ToString("f2");
					dataGridViewRow.HeaderCell.Value = RuntimeHelpers.GetObjectValue(this.headersRows[j]);
					this.dgvnn.Rows.Add(dataGridViewRow);
				}
				int num4 = this.headersCols.Length - 1;
				for (int k = 0; k <= num4; k++)
				{
					this.dgvnn.Columns[k].DefaultCellStyle.BackColor = this.edit_color;
				}
			}
		}

		private void tbar_Changed(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init);
			//}
			//if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf > 0)
			//{
			//	int selectedIndex = checked(this.headersRows.Count - this.tbar.Value - 1);
			//	this.headersListBox.SelectedIndex = selectedIndex;
			//}
			//this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf = 1;
			this.freezeDisp(true);
		}

		private void tbar4_Changed(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf_0024Init);
			//}
			//if (this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf <= 0)
			//{
			//	goto IL_0086;
			//}
			//goto IL_0086;
			//IL_0086:
			//this._0024STATIC_0024tbar4_Changed_002420211C1280B1_0024isf = 1;
			this.freezeDisp(true);
		}

		private void tbar2_Changed(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init);
			//}
			//if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf > 0)
			//{
			//	int selectedIndex = checked(this.headersRows.Count - this.tbar2.Value - 1);
			//	this.headersListBox.SelectedIndex = selectedIndex;
			//}
			//this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf = 1;
			this.freezeDisp(true);
		}

		private void ListBoxRegions_SelectedIndexChanged(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State = 2;
			//		this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall = 0;
			//	}
			//	else if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init);
			//}
			//if (Operators.ConditionalCompareObjectGreater(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall, 0, false))
			//{
			//	string text = this.headersListBox.SelectedItem.ToString().Trim();
			//	int selectedIndex = this.headersListBox.SelectedIndex;
			//	this.UpdateDispRow(selectedIndex);
			//}
			//this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall = 1;
			this.freezeDisp(true);
		}

		private void freezeDisp(bool dispstate)
		{
		}

		private void dgv3_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView)sender;
			dataGridView.BeginEdit(true);
			this.headersListBox.SelectedIndex = this.dgvnn.CurrentRow.Index;
			if (!this.isEdited)
			{
				this.label_ElementEdited.Text = this.headersCols[this.dgvnn.CurrentCell.ColumnIndex];
			}
		}

		private void dgv3_CellChange(object sender, DataGridViewCellEventArgs e)
		{
			int columnIndex = e.ColumnIndex;
			int rowIndex = e.RowIndex;
			double num = 0.0;
			double num2 = 0.0;
			double num3 = 0.0;
			double num4 = 0.0;
			double num5 = 0.0;
			if (this.dgvnn.Rows[rowIndex].Cells[4].Style.BackColor == Color.Tomato)
			{
				num2 = Conversions.ToDouble(this.dgvnn.Rows[rowIndex].Cells[1].Value);
				num4 = Conversions.ToDouble(this.dgvnn.Rows[rowIndex].Cells[2].Value);
				num3 = Conversions.ToDouble(this.dgvnn.Rows[rowIndex].Cells[3].Value);
				num5 = Conversions.ToDouble(this.dgvnn.Rows[rowIndex].Cells[4].Value);
				if (Math.Abs(num2 + num4 + num3 - num5) <= 1.0)
				{
					this.dgvnn.Rows[rowIndex].Cells[1].Style.BackColor = this.edit_color;
					this.dgvnn.Rows[rowIndex].Cells[2].Style.BackColor = this.edit_color;
					this.dgvnn.Rows[rowIndex].Cells[3].Style.BackColor = this.edit_color;
					this.dgvnn.Rows[rowIndex].Cells[4].Style.BackColor = this.edit_color;
				}
			}
		}

		private void dgv3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
		}

		private void UpdateDispRow(int idx)
		{
			int columnIndex = this.dgvnn.CurrentCell.ColumnIndex;
			checked
			{
				this.tbar.Value = this.headersRows.Count - idx - 1;
				this.tbar2.Value = this.headersRows.Count - idx - 1;
				this.headersListBox.SelectedIndex = idx;
				this.dgvnn.CurrentCell = this.dgvnn.Rows[idx].Cells[columnIndex];
			}
		}

		private void updateDisp(int idx)
		{
			int selectedIndex = this.headersListBox.SelectedIndex;
			this.label_ElementEdited.Text = this.headersCols[idx];
			checked
			{
				if (idx > 0)
				{
					int num = idx - 1;
					for (int i = 0; i <= num; i++)
					{
						this.dgvnn.Columns[i].ReadOnly = true;
						this.dgvnn.Columns[i].DefaultCellStyle.BackColor = this.non_edit_color;
					}
				}
				if (idx < this.headersCols.Length - 1)
				{
					int num2 = idx + 1;
					int num3 = this.headersCols.Length - 1;
					for (int j = num2; j <= num3; j++)
					{
						this.dgvnn.Columns[j].ReadOnly = true;
						this.dgvnn.Columns[j].DefaultCellStyle.BackColor = this.non_edit_color;
					}
				}
				this.dgvnn.Columns[idx].ReadOnly = false;
				this.dgvnn.Columns[idx].DefaultCellStyle.BackColor = this.edit_color;
				this.dgvnn.CurrentCell = this.dgvnn.Rows[selectedIndex].Cells[idx];
			}
		}

		private void CellHeader_Click(object sender, EventArgs e)
		{
			int num = 0;
			num = this.dgvnn.CurrentRow.Index;
			this.dgvnn.ClearSelection();
			this.dgvnn.CurrentCell = this.dgvnn.Rows[num].Cells[0];
			this.dgvnn.CurrentCell.Selected = true;
			if (!this.isEdited)
			{
				this.headersListBox.SelectedIndex = num;
			}
		}

		private void ProceedButton_Click(object sender, EventArgs e)
		{
			object obj = MessageBox.Show("Do you want to proceed?", "Alert", MessageBoxButtons.YesNoCancel);
		}

		private void UndoButton_Click(object sender, EventArgs e)
		{
			checked
			{
				int num = this.statedat.getEmployment().Count - 1;
				for (int i = 0; i <= num; i++)
				{
					decimal d = new decimal(this.statedat.getEmployment()[i]);
					DataGridViewCell dataGridViewCell = this.dgvnn.Rows[i].Cells[0];
					decimal num2 = decimal.Round(d, 2);
					dataGridViewCell.Value = num2.ToString("f2");
					d = new decimal(this.statedat.getEC()[i]);
					DataGridViewCell dataGridViewCell2 = this.dgvnn.Rows[i].Cells[1];
					num2 = decimal.Round(d, 2);
					dataGridViewCell2.Value = num2.ToString("f2");
					d = new decimal(this.statedat.getGOS()[i]);
					DataGridViewCell dataGridViewCell3 = this.dgvnn.Rows[i].Cells[2];
					num2 = decimal.Round(d, 2);
					dataGridViewCell3.Value = num2.ToString("f2");
					d = new decimal(this.statedat.getT()[i]);
					DataGridViewCell dataGridViewCell4 = this.dgvnn.Rows[i].Cells[3];
					num2 = decimal.Round(d, 2);
					dataGridViewCell4.Value = num2.ToString("f2");
					d = new decimal(this.statedat.getGDP()[i]);
					DataGridViewCell dataGridViewCell5 = this.dgvnn.Rows[i].Cells[4];
					num2 = decimal.Round(d, 2);
					dataGridViewCell5.Value = num2.ToString("f2");
					this.dgvnn.Rows[i].Cells[1].Style.BackColor = this.edit_color;
					this.dgvnn.Rows[i].Cells[2].Style.BackColor = this.edit_color;
					this.dgvnn.Rows[i].Cells[3].Style.BackColor = this.edit_color;
					this.dgvnn.Rows[i].Cells[4].Style.BackColor = this.edit_color;
				}
				this.Label2.Hide();
			}
		}

		private void Button2_Click(object sender, EventArgs e)
		{
			double num = 0.0;
			double num2 = 0.0;
			double num3 = 0.0;
			double num4 = 0.0;
			double num5 = 0.0;
			int num6 = 0;
			this.Label2.Hide();
			checked
			{
				int num7 = this.dgvnn.RowCount - 1;
				for (int i = 0; i <= num7; i++)
				{
					num2 = Conversions.ToDouble(this.dgvnn.Rows[i].Cells[1].Value);
					num4 = Conversions.ToDouble(this.dgvnn.Rows[i].Cells[2].Value);
					num3 = Conversions.ToDouble(this.dgvnn.Rows[i].Cells[3].Value);
					num5 = Conversions.ToDouble(this.dgvnn.Rows[i].Cells[4].Value);
					this.dgvnn.Rows[i].DefaultCellStyle.BackColor = this.edit_color;
					if (Math.Abs(unchecked(num2 + num4 + num3 - num5)) > 1.0)
					{
						this.Label2.Show();
						this.dgvnn.Rows[i].Cells[1].Style.BackColor = Color.Tomato;
						this.dgvnn.Rows[i].Cells[2].Style.BackColor = Color.Tomato;
						this.dgvnn.Rows[i].Cells[3].Style.BackColor = Color.Tomato;
						this.dgvnn.Rows[i].Cells[4].Style.BackColor = Color.Tomato;
						num6++;
					}
				}
				if (num6 == 0)
				{
					object left = MessageBox.Show("Do you want to proceed?", "Alert", MessageBoxButtons.YesNo);
					if (!Operators.ConditionalCompareObjectEqual(left, DialogResult.No, false) && Operators.ConditionalCompareObjectEqual(left, DialogResult.Yes, false))
					{
						this.isChangeApproved = true;
						Vector vector = new Vector(this.statedat.getEmployment().Count);
						Vector vector2 = new Vector(this.statedat.getEC().Count);
						Vector vector3 = new Vector(this.statedat.getGOS().Count);
						Vector vector4 = new Vector(this.statedat.getT().Count);
						Vector vector5 = new Vector(this.statedat.getGDP().Count);
						int num8 = vector.Count - 1;
						for (int j = 0; j <= num8; j++)
						{
							vector[j] = Conversions.ToDouble(this.dgvnn.Rows[j].Cells[0].Value);
							vector2[j] = Conversions.ToDouble(this.dgvnn.Rows[j].Cells[1].Value);
							vector3[j] = Conversions.ToDouble(this.dgvnn.Rows[j].Cells[2].Value);
							vector4[j] = Conversions.ToDouble(this.dgvnn.Rows[j].Cells[3].Value);
							vector5[j] = Conversions.ToDouble(this.dgvnn.Rows[j].Cells[4].Value);
						}
						this.newStatData.setEmployment(ref vector);
						this.newStatData.setEC(ref vector2);
						this.newStatData.setGOS(vector3);
						this.newStatData.setT(vector4);
						this.newStatData.setGDP(vector5);
						base.DialogResult = DialogResult.OK;
						base.Close();
					}
				}
			}
		}
	}
}
