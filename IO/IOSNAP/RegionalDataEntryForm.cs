using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class RegionalDataEntryForm : Form
	{
        static bool DispTot;

        private enum DispTotal : byte
		{
			Yes,
			No,
			Unchanged
		}

		private IContainer components;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("OKButton")]
		private Button _OKButton;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("CancelButtonRGD")]
		private Button _CancelButtonRGD;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("dgvnns")]
		private DataGridView _dgvnns;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Button1")]
		private Button _Button1;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Label2")]
		private Label _Label2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Rebalance2")]
		private Button _Rebalance2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Button2")]
		private Button _Button2;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[AccessedThroughProperty("Button3")]
		private Button _Button3;

		private string _frmTitle;

		public EditRegData dispdat;

		public ViewOptions view_options;

		public Size tabsize;

		public string dispFormatGeneral;

		public string dispFormatRounded;

		public string dispFormatCoefficients;

		public bool hasEmployment;

		private DispTotal DispTotals;

		private bool bCoeff;

		private Vector colSum;

		private int cellwidth;

		private int NumCol;

		private int NumRow;

		private double[] oEmp;

		private double[] oEC;

		private double[] oT;

		private double[] oGOS;

		private double[] oGDP;

		private double EC_T_Sum;

		private double GDP_T_Sum;

		private Color edit_color;

		internal virtual Button OKButton
		{
			[CompilerGenerated]
			get
			{
				return this._OKButton;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.OKButton_Click;
				Button oKButton = this._OKButton;
				if (oKButton != null)
				{
					oKButton.Click -= value2;
				}
				this._OKButton = value;
				oKButton = this._OKButton;
				if (oKButton != null)
				{
					oKButton.Click += value2;
				}
			}
		}

		internal virtual Button CancelButtonRGD
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual DataGridView dgvnns
		{
			[CompilerGenerated]
			get
			{
				return this._dgvnns;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				DataGridViewEditingControlShowingEventHandler value2 = this.dgvnns_EditingControlShowing;
				DataGridViewCellValidatingEventHandler value3 = this.dgvnns_CellValidating;
				DataGridViewCellEventHandler value4 = this.dgvnns_CellValueChanged;
				DataGridViewCellEventHandler value5 = this.dgvnns_CellClick;
				DataGridView dgvnns = this._dgvnns;
				if (dgvnns != null)
				{
					dgvnns.EditingControlShowing -= value2;
					dgvnns.CellValidating -= value3;
					dgvnns.CellValueChanged -= value4;
					dgvnns.CellClick -= value5;
				}
				this._dgvnns = value;
				dgvnns = this._dgvnns;
				if (dgvnns != null)
				{
					dgvnns.EditingControlShowing += value2;
					dgvnns.CellValidating += value3;
					dgvnns.CellValueChanged += value4;
					dgvnns.CellClick += value5;
				}
			}
		}

		internal virtual Button Button1
		{
			[CompilerGenerated]
			get
			{
				return this._Button1;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Button1_Click;
				Button button = this._Button1;
				if (button != null)
				{
					button.Click -= value2;
				}
				this._Button1 = value;
				button = this._Button1;
				if (button != null)
				{
					button.Click += value2;
				}
			}
		}

		internal virtual Label Label2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button Rebalance2
		{
			[CompilerGenerated]
			get
			{
				return this._Rebalance2;
			}
			[MethodImpl(MethodImplOptions.Synchronized)]
			[CompilerGenerated]
			set
			{
				EventHandler value2 = this.Rebalance2_Click;
				Button rebalance = this._Rebalance2;
				if (rebalance != null)
				{
					rebalance.Click -= value2;
				}
				this._Rebalance2 = value;
				rebalance = this._Rebalance2;
				if (rebalance != null)
				{
					rebalance.Click += value2;
				}
			}
		}

		internal virtual Button Button2
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		internal virtual Button Button3
		{
			get;
			[MethodImpl(MethodImplOptions.Synchronized)]
			set;
		}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.OKButton = new Button();
			this.CancelButtonRGD = new Button();
			this.dgvnns = new DataGridView();
			this.Button1 = new Button();
			this.Label2 = new Label();
			this.Rebalance2 = new Button();
			this.Button2 = new Button();
			this.Button3 = new Button();
			((ISupportInitialize)this.dgvnns).BeginInit();
			base.SuspendLayout();
			this.OKButton.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.OKButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.OKButton.Location = new Point(726, 596);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new Size(113, 45);
			this.OKButton.TabIndex = 0;
			this.OKButton.Text = "Proceed";
			this.OKButton.UseVisualStyleBackColor = true;
			this.CancelButtonRGD.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.CancelButtonRGD.DialogResult = DialogResult.Cancel;
			this.CancelButtonRGD.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.CancelButtonRGD.Location = new Point(607, 596);
			this.CancelButtonRGD.Name = "CancelButtonRGD";
			this.CancelButtonRGD.Size = new Size(113, 45);
			this.CancelButtonRGD.TabIndex = 1;
			this.CancelButtonRGD.Text = "Cancel";
			this.CancelButtonRGD.UseVisualStyleBackColor = true;
			this.dgvnns.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnns.Location = new Point(17, 25);
			this.dgvnns.Name = "dgvnns";
			this.dgvnns.RowTemplate.Height = 28;
			this.dgvnns.Size = new Size(820, 553);
			this.dgvnns.TabIndex = 2;
			this.Button1.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.Button1.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Button1.Location = new Point(17, 595);
			this.Button1.Name = "Button1";
			this.Button1.Size = new Size(113, 45);
			this.Button1.TabIndex = 3;
			this.Button1.Text = "Paste";
			this.Button1.UseVisualStyleBackColor = true;
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Calibri", 9.75f, FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label2.ForeColor = Color.Red;
			this.Label2.Location = new Point(28, 7);
			this.Label2.Name = "Label2";
			this.Label2.Size = new Size(686, 15);
			this.Label2.TabIndex = 48;
			this.Label2.Text = "Please edit highlighted rows. Gross Product must equal the sum of Employee Compensation, Gross Operating Surplus, and Taxes.";
			this.Rebalance2.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Rebalance2.Location = new Point(136, 596);
			this.Rebalance2.Name = "Rebalance2";
			this.Rebalance2.Size = new Size(113, 44);
			this.Rebalance2.TabIndex = 52;
			this.Rebalance2.Text = "Rebalance?";
			this.Rebalance2.UseVisualStyleBackColor = true;
			this.Button2.BackColor = Color.LemonChiffon;
			this.Button2.Enabled = false;
			this.Button2.FlatAppearance.BorderSize = 0;
			this.Button2.FlatStyle = FlatStyle.Flat;
			this.Button2.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Button2.Location = new Point(847, 59);
			this.Button2.Name = "Button2";
			this.Button2.Size = new Size(125, 46);
			this.Button2.TabIndex = 54;
			this.Button2.Text = "Changes made by user";
			this.Button2.UseVisualStyleBackColor = false;
			this.Button3.BackColor = Color.GreenYellow;
			this.Button3.Enabled = false;
			this.Button3.FlatAppearance.BorderSize = 0;
			this.Button3.FlatStyle = FlatStyle.Flat;
			this.Button3.Font = new Font("Calibri", 12f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.Button3.Location = new Point(847, 111);
			this.Button3.Name = "Button3";
			this.Button3.Size = new Size(125, 46);
			this.Button3.TabIndex = 55;
			this.Button3.Text = "Changes made by IO-Snap";
			this.Button3.UseVisualStyleBackColor = false;
			base.AutoScaleDimensions = new SizeF(6f, 14f);
			base.AutoScaleMode = AutoScaleMode.Font;
			this.AutoScroll = true;
			base.ClientSize = new Size(980, 664);
			base.Controls.Add(this.Button3);
			base.Controls.Add(this.Button2);
			base.Controls.Add(this.Rebalance2);
			base.Controls.Add(this.Label2);
			base.Controls.Add(this.Button1);
			base.Controls.Add(this.dgvnns);
			base.Controls.Add(this.CancelButtonRGD);
			base.Controls.Add(this.OKButton);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "RegionalDataEntryForm";
			this.Text = "Regional Data Entry Form";
			((ISupportInitialize)this.dgvnns).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public RegionalDataEntryForm()
		{
			base.Load += this.RegionalDataEntryForm_Load;
			this._frmTitle = "hhhh";
			this.dispFormatGeneral = "";
			this.dispFormatRounded = "";
			this.dispFormatCoefficients = "";
			this.hasEmployment = false;
			this.bCoeff = false;
			this.cellwidth = 130;
			this.EC_T_Sum = 0.0;
			this.GDP_T_Sum = 0.0;
			this.edit_color = SystemColors.Window;
			this.InitializeComponent();
			this.view_options = new ViewOptions();
		}

		public void testData()
		{
			int num = 0;
			this.dgvnns.SelectionMode = DataGridViewSelectionMode.CellSelect;
			this.dgvnns.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvnns.RightToLeft = RightToLeft.No;
			this.dgvnns.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnns.RowHeadersWidth = 150;
			this.dgvnns.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvnns.RowTemplate.Height = 24;
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle.BackColor = SystemColors.Window;
			dataGridViewCellStyle.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle.Format = this.view_options.getGeneralFormat;
			dataGridViewCellStyle.NullValue = null;
			dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = DataGridViewTriState.False;
			this.dgvnns.DefaultCellStyle = dataGridViewCellStyle;
			this.dgvnns.AllowUserToAddRows = false;
			string[] hdrsCol = this.dispdat.hdrsCol;
			foreach (string text in hdrsCol)
			{
				num = this.dgvnns.Columns.Add(text, text);
			}
			checked
			{
				if (this.dispdat.BaseTable.NoCols > this.dispdat.hdrsCol.Length)
				{
					int num2 = this.dispdat.hdrsCol.Length;
					int num3 = this.dispdat.BaseTable.NoCols - 1;
					for (int j = num2; j <= num3; j++)
					{
						num = this.dgvnns.Columns.Add("Col " + Conversions.ToString(j), "Col " + Conversions.ToString(j));
					}
				}
				this.SetFormatContent();
				int num4 = this.dispdat.BaseTable.NoRows - 1;
				for (int k = 0; k <= num4; k++)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnns);
					int num5 = this.dispdat.BaseTable.NoCols - 1;
					for (int l = 0; l <= num5; l++)
					{
						dataGridViewRow.Cells[l].Value = this.dispdat.BaseTable[k, l];
					}
					dataGridViewRow.HeaderCell.Value = this.dispdat.hdrsRow[k];
					this.dgvnns.Rows.Add(dataGridViewRow);
				}
			}
			IEnumerator enumerator = default(IEnumerator);
			try
			{
				enumerator = this.dgvnns.Columns.GetEnumerator();
				while (enumerator.MoveNext())
				{
					DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
					dataGridViewColumn.Width = this.cellwidth;
					if (this.dispdat.isSortable)
					{
						dataGridViewColumn.SortMode = DataGridViewColumnSortMode.Automatic;
					}
					else
					{
						dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
					}
				}
			}
			finally
			{
				if (enumerator is IDisposable)
				{
					(enumerator as IDisposable).Dispose();
				}
			}
			this.dgvnns.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
		}

		public void ProcessData()
		{
			this.dgvnns = new DataGridView();
			this.ClearScreen();
			this.dgvnns.Refresh();
			this.view_options.GenFormBase = this.dispFormatGeneral;
			this.view_options.CoeffFormatBase = this.dispFormatCoefficients;
			this.view_options.RoundFormatBase = this.dispFormatRounded;
			this.hasEmployment = this.dispdat.hasEmployment;
			Interaction.MsgBox("ha ha ha", MsgBoxStyle.OkOnly, null);
			this.Text = this._frmTitle;
			this.dgvnns.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
			this.dgvnns.SelectionMode = DataGridViewSelectionMode.CellSelect;
			this.dgvnns.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvnns.Size = this.tabsize;
			this.dgvnns.RightToLeft = RightToLeft.No;
			this.dgvnns.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnns.RowHeadersWidth = 175;
			this.dgvnns.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvnns.RowTemplate.Height = 24;
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle.BackColor = SystemColors.Control;
			dataGridViewCellStyle.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = DataGridViewTriState.True;
			this.dgvnns.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle;
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle2.BackColor = SystemColors.Window;
			dataGridViewCellStyle2.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle2.Format = this.view_options.getGeneralFormat;
			dataGridViewCellStyle2.NullValue = null;
			dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
			this.dgvnns.DefaultCellStyle = dataGridViewCellStyle2;
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = SystemColors.Control;
			dataGridViewCellStyle3.Font = new Font("Microsoft Sans Serif", 8f, FontStyle.Regular, GraphicsUnit.Point, 0);
			dataGridViewCellStyle3.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = DataGridViewTriState.True;
			this.dgvnns.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			switch (this.DispTotals)
			{
			case DispTotal.No:
                    //this.ProcessData..DispTotals = true;
                DispTot = false;
				break;
			case DispTotal.Yes:
                    DispTot = true;
                    break;
			}
			if (this.bCoeff | DispTot)
			{
				this.colSum = this.dispdat.getColSum();
			}
			DataGridView dgvnns = this.dgvnns;
			dgvnns.Columns.Clear();
			string[] hdrsCol = this.dispdat.hdrsCol;
			foreach (string text in hdrsCol)
			{
				dgvnns.Columns.Add(text, text);
			}
			checked
			{
				if (this.dispdat.BaseTable.NoCols > this.dispdat.hdrsCol.Length)
				{
					int num = this.dispdat.hdrsCol.Length;
					int num2 = this.dispdat.BaseTable.NoCols - 1;
					for (int j = num; j <= num2; j++)
					{
						dgvnns.Columns.Add("Col " + Conversions.ToString(j), "Col " + Conversions.ToString(j));
					}
				}
				this.SetFormatContent();
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = dgvnns.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = unchecked((DataGridViewColumn)enumerator.Current);
						dataGridViewColumn.ReadOnly = true;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				dgvnns.RowHeadersVisible = true;
				int num3 = this.dispdat.BaseTable.NoRows - 1;
				for (int j = 0; j <= num3; j++)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnns);
					int num4 = this.dispdat.BaseTable.NoCols - 1;
					for (int k = 0; k <= num4; k++)
					{
						dataGridViewRow.Cells[k].Value = this.dispdat.BaseTable[j, k];
						if (this.dispdat.showCellColor)
						{
							try
							{
								if (this.dispdat.ColorTable[j, k] > 0.0)
								{
									dataGridViewRow.Cells[k].Style.BackColor = Color.LightYellow;
								}
							}
							catch (Exception ex)
							{
								ProjectData.SetProjectError(ex);
								Exception ex2 = ex;
								ProjectData.ClearProjectError();
							}
						}
					}
					dataGridViewRow.HeaderCell.Value = this.dispdat.hdrsRow[j];
					dgvnns.Rows.Add(dataGridViewRow);
				}
				if (unchecked((object)this.dispdat.ExtraCols) != null)
				{
					int noCols = this.dispdat.BaseTable.NoCols;
					int num5 = this.dispdat.ExtraCols.NoCols - 1;
					for (int l = 0; l <= num5; l++)
					{
						int index = dgvnns.Columns.Add(this.dispdat.hdrsXCols[l], this.dispdat.hdrsXCols[l]);
						dgvnns.Columns[index].DefaultCellStyle.BackColor = Color.LightBlue;
						int num6 = this.dispdat.ExtraCols.NoRows - 1;
						for (int j = 0; j <= num6; j++)
						{
							dgvnns.Rows[j].Cells[noCols + l].Value = this.dispdat.ExtraCols[j, l];
						}
						dgvnns.Columns[index].ReadOnly = true;
					}
				}
				if (unchecked((object)this.dispdat.ExtraRows) != null)
				{
					int num7 = this.dispdat.ExtraRows.NoRows - 1;
					for (int j = 0; j <= num7; j++)
					{
						DataGridViewRow dataGridViewRow = new DataGridViewRow();
						dataGridViewRow.CreateCells(this.dgvnns);
						dataGridViewRow.DefaultCellStyle.BackColor = Color.LightBlue;
						dataGridViewRow.HeaderCell.Value = this.dispdat.hdrsXRows[j];
						int num8 = this.dispdat.ExtraRows.NoCols - 1;
						for (int m = 0; m <= num8; m++)
						{
							if (this.bCoeff)
							{
								dataGridViewRow.Cells[m].Value = unchecked(this.dispdat.ExtraRows[j, m] / this.colSum[m]);
							}
							else
							{
								dataGridViewRow.Cells[m].Value = this.dispdat.ExtraRows[j, m];
							}
						}
						dgvnns.Rows.Add(dataGridViewRow);
					}
				}
				IEnumerator enumerator2 = default(IEnumerator);
				try
				{
					enumerator2 = dgvnns.Columns.GetEnumerator();
					while (enumerator2.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn2 = unchecked((DataGridViewColumn)enumerator2.Current);
						dataGridViewColumn2.Width = this.cellwidth;
						if (this.dispdat.isSortable)
						{
							dataGridViewColumn2.SortMode = DataGridViewColumnSortMode.Automatic;
						}
						else
						{
							dataGridViewColumn2.SortMode = DataGridViewColumnSortMode.NotSortable;
						}
					}
				}
				finally
				{
					if (enumerator2 is IDisposable)
					{
						(enumerator2 as IDisposable).Dispose();
					}
				}
				dgvnns.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
				dgvnns = null;
				if (DispTot)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnns);
					dataGridViewRow.ReadOnly = true;
					dataGridViewRow.DefaultCellStyle.BackColor = Color.LightGray;
					dataGridViewRow.HeaderCell.Value = "Column Sum";
					int num9 = this.dispdat.BaseTable.NoCols - 1;
					for (int n = 0; n <= num9; n++)
					{
						dataGridViewRow.Cells[n].Value = this.colSum[n];
					}
					this.dgvnns.Rows.Add(dataGridViewRow);
					int count = this.dgvnns.Columns.Count;
					int index2 = this.dgvnns.Columns.Add("RowSum", "Row Sum");
					this.dgvnns.Columns[index2].DefaultCellStyle.BackColor = Color.LightGray;
					Vector vector = this.dispdat.BaseTable.RowSum();
					if (unchecked((object)this.dispdat.ExtraCols) != null)
					{
						int num10 = vector.Count - 1;
						for (int j = 0; j <= num10; j++)
						{
							int num11 = this.dispdat.ExtraCols.NoCols - 1;
							for (int num12 = 0; num12 <= num11; num12++)
							{
								Vector vector2;
								int row;
								(vector2 = vector)[row = j] = unchecked(vector2[row] + this.dispdat.ExtraCols[j, num12]);
							}
						}
					}
					int num13 = vector.Count - 1;
					for (int j = 0; j <= num13; j++)
					{
						this.dgvnns.Rows[j].Cells[count].Value = vector[j];
					}
				}
				this.dispdat.showCellColor = false;
				this.dgvnns.Refresh();
			}
		}

		private void OKButton_Click(object sender, EventArgs e)
		{
			checked
			{
				if (this.NumCol > 3)
				{
					double num = 0.0;
					double num2 = 0.0;
					double num3 = 0.0;
					double num4 = 0.0;
					double num5 = 0.0;
					int num6 = 0;
					int num7 = this.dgvnns.RowCount - 1;
					for (int i = 0; i <= num7; i++)
					{
						num2 = Conversions.ToDouble(this.dgvnns.Rows[i].Cells[1].Value);
						num4 = Conversions.ToDouble(this.dgvnns.Rows[i].Cells[2].Value);
						num3 = Conversions.ToDouble(this.dgvnns.Rows[i].Cells[3].Value);
						num5 = Conversions.ToDouble(this.dgvnns.Rows[i].Cells[4].Value);
						this.dgvnns.Rows[i].DefaultCellStyle.BackColor = this.edit_color;
						if (Math.Abs(unchecked(num2 + num4 + num3 - num5)) > 1.0)
						{
							this.Label2.Show();
							this.Rebalance2.BackColor = Color.LightGreen;
							this.dgvnns.Rows[i].ErrorText = "Gross Product must equal the sum of Employee Compensation, Gross Operating Surplus, and Taxes.";
							num6++;
						}
						else if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectNotEqual(this.oEmp[i], this.dgvnns.Rows[i].Cells[0].Value, false), Operators.CompareObjectEqual(this.oEC[i], this.dgvnns.Rows[i].Cells[1].Value, false))))
						{
							this.Rebalance2.BackColor = Color.LightGreen;
							this.dgvnns.Rows[i].ErrorText = "Employment changes may necessitate Compensation adjustments";
							num6++;
						}
						else if (Conversions.ToBoolean(Operators.AndObject(Operators.AndObject(Operators.CompareObjectEqual(this.oEmp[i], this.dgvnns.Rows[i].Cells[0].Value, false), Operators.CompareObjectNotEqual(this.oEC[i], this.dgvnns.Rows[i].Cells[1].Value, false)), Math.Abs(unchecked(num2 + num4 + num3 - num5)) < 1.0)))
						{
							this.Rebalance2.BackColor = Color.LightGreen;
							this.dgvnns.Rows[i].ErrorText = "Compensation changes may necessitate Employment adjustments";
							num6++;
						}
					}
					if (num6 != 0)
					{
						return;
					}
				}
				BEAData.EC_T_Sum = this.EC_T_Sum;
				BEAData.GDP_T_Sum = this.GDP_T_Sum;
				base.DialogResult = DialogResult.OK;
				base.Close();
			}
		}

		public void SetFormatContent()
		{
			DataGridView dgvnns = this.dgvnns;
			if (this.dispdat.useGeneralFormatOnly)
			{
				dgvnns.DefaultCellStyle.Format = this.view_options.getGeneralFormat;
			}
			else
			{
				int num = 0;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = dgvnns.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = (DataGridViewColumn)enumerator.Current;
						try
						{
							switch (this.dispdat.DataFormatTable[num])
							{
							case 1:
								dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getGeneralFormat;
								break;
							case 2:
								dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getCoefficientFormat;
								break;
							case 3:
								dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getRoundFormat;
								break;
							default:
								dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getGeneralFormat;
								break;
							}
						}
						catch (Exception ex)
						{
							ProjectData.SetProjectError(ex);
							Exception ex2 = ex;
							dataGridViewColumn.DefaultCellStyle.Format = this.view_options.getGeneralFormat;
							ProjectData.ClearProjectError();
						}
						num = checked(num + 1);
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
			}
			dgvnns = null;
		}

		private void ClearScreen()
		{
			this.dgvnns.Rows.Clear();
		}

		private void TextBox_keyPress(object sender, KeyPressEventArgs e)
		{
			if (!char.IsDigit(Conversions.ToChar(Conversions.ToString(e.KeyChar))))
			{
				e.Handled = true;
			}
		}

		private void dgvnns_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			((TextBox)e.Control).KeyPress += this.TextBox_keyPress;
		}

		private void dgvnns_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			DataGridViewCell dataGridViewCell = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex];
		}

		private void dgvnns_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			DataGridViewCell dataGridViewCell = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex];
			int rowIndex = dataGridViewCell.RowIndex;
			int columnIndex = dataGridViewCell.ColumnIndex;
			if (rowIndex < this.dispdat.BaseTable.NoRows && columnIndex < this.dispdat.BaseTable.NoCols)
			{
				string s = Conversions.ToString(dataGridViewCell.Value);
				Matrix baseTable;
				int row;
				int col;
				double value = (baseTable = this.dispdat.BaseTable)[row = rowIndex, col = columnIndex];
				double.TryParse(s, out value);
				baseTable[row, col] = value;
			}
		}

		private void CopySelection()
		{
			Clipboard.Clear();
			Clipboard.SetText(this.dgvnns.SelectedCells.ToString(), TextDataFormat.Text);
		}

		private void Paste(bool overwriteSelectiona)
		{
			string text = Clipboard.GetText();
			string[] array = text.Split('\r');
			int num = 0;
			int num2 = 0;
			string[] array2 = new string[3]
			{
				"\r\n",
				"\t",
				" "
			};
			int num3 = array.Length;
			string[] array3 = array;
			checked
			{
				foreach (string text2 in array3)
				{
					if (num != num3 - 1)
					{
						string[] array4 = text2.Split('\t');
						array4[0] = array4[0].Trim();
						num2 = 0;
						string[] array5 = array4;
						foreach (string text3 in array5)
						{
							if (num2 < this.dgvnns.ColumnCount - this.dgvnns.SelectedCells[0].ColumnIndex & num < this.dgvnns.RowCount - this.dgvnns.SelectedCells[0].RowIndex)
							{
								this.dgvnns.Rows[num + this.dgvnns.SelectedCells[0].RowIndex].Cells[num2 + this.dgvnns.SelectedCells[0].ColumnIndex].Value = text3.Trim(' ');
							}
							num2++;
						}
					}
					num++;
				}
			}
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			this.Paste(true);
		}

		private void dgvnns_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView)sender;
			dataGridView.BeginEdit(true);
		}

		private void RegionalDataEntryForm_Load(object sender, EventArgs e)
		{
			this.NumCol = this.dgvnns.ColumnCount;
			this.NumRow = this.dgvnns.RowCount;
			this.Label2.Hide();
			checked
			{
				if (this.NumCol > 3)
				{
					double num = 0.0;
					double num2 = 0.0;
					double num3 = 0.0;
					double num4 = 0.0;
					double num5 = 0.0;
					int num6 = 0;
					int num7 = this.dgvnns.RowCount - 1;
					for (int i = 0; i <= num7; i++)
					{
						num2 = Conversions.ToDouble(this.dgvnns.Rows[i].Cells[1].Value);
						num4 = Conversions.ToDouble(this.dgvnns.Rows[i].Cells[2].Value);
						num3 = Conversions.ToDouble(this.dgvnns.Rows[i].Cells[3].Value);
						num5 = Conversions.ToDouble(this.dgvnns.Rows[i].Cells[4].Value);
						unchecked
						{
							if (Math.Abs(num2 + num4 + num3 - num5) > 1.0)
							{
								num5 = num2 + num4 + num3;
								this.dgvnns.Rows[i].Cells[4].Value = num5;
							}
						}
					}
				}
				this.oEmp = new double[this.dgvnns.RowCount + 1];
				this.oEC = new double[this.dgvnns.RowCount + 1];
				this.oGOS = new double[this.dgvnns.RowCount + 1];
				this.oT = new double[this.dgvnns.RowCount + 1];
				this.oGDP = new double[this.dgvnns.RowCount + 1];
				double num8 = 0.0;
				if (this.NumCol > 3)
				{
					int num9 = this.dgvnns.RowCount - 1;
					for (int j = 0; j <= num9; j++)
					{
						this.oEmp[j] = Conversions.ToDouble(this.dgvnns.Rows[j].Cells[0].Value);
						this.oEC[j] = Conversions.ToDouble(this.dgvnns.Rows[j].Cells[1].Value);
						this.oGOS[j] = Conversions.ToDouble(this.dgvnns.Rows[j].Cells[2].Value);
						this.oT[j] = Conversions.ToDouble(this.dgvnns.Rows[j].Cells[3].Value);
						this.oGDP[j] = Conversions.ToDouble(this.dgvnns.Rows[j].Cells[4].Value);
						unchecked
						{
							this.GDP_T_Sum += this.oGDP[j];
							this.EC_T_Sum += this.oEC[j];
						}
					}
				}
			}
		}

		private void Rebalance2_Click(object sender, EventArgs e)
		{
			double num = 1.0;
			checked
			{
				int[] array = new int[this.dgvnns.RowCount + 1];
				int num2 = this.dgvnns.RowCount - 1;
				for (int i = 0; i <= num2; i++)
				{
					if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectNotEqual(this.oEmp[i], this.dgvnns.Rows[i].Cells[0].Value, false), Operators.CompareObjectEqual(this.oEC[i], this.dgvnns.Rows[i].Cells[1].Value, false))))
					{
						array[i] = 2;
						this.dgvnns.Rows[i].Cells[0].Style.BackColor = Color.LemonChiffon;
						num = Conversions.ToDouble(Operators.DivideObject(this.dgvnns.Rows[i].Cells[0].Value, this.oEmp[i]));
						this.dgvnns.Rows[i].Cells[1].Value = unchecked(this.oEC[i] * num);
					}
				}
				int num3 = this.dgvnns.RowCount - 1;
				for (int j = 0; j <= num3; j++)
				{
					if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectNotEqual(this.oEmp[j], this.dgvnns.Rows[j].Cells[0].Value, false), Operators.CompareObjectNotEqual(this.oEC[j], this.dgvnns.Rows[j].Cells[1].Value, false))))
					{
						this.dgvnns.Rows[j].Cells[0].Style.BackColor = Color.LemonChiffon;
						this.dgvnns.Rows[j].Cells[1].Style.BackColor = Color.LemonChiffon;
					}
				}
				int num4 = this.dgvnns.RowCount - 1;
				int num5 = default(int);
				for (int k = 0; k <= num4; k++)
				{
					num5 = 0;
					if (Operators.ConditionalCompareObjectNotEqual(this.oGDP[k], this.dgvnns.Rows[k].Cells[4].Value, false))
					{
						this.dgvnns.Rows[k].Cells[4].Style.BackColor = Color.LemonChiffon;
						num5 += 1000;
					}
					if (Operators.ConditionalCompareObjectNotEqual(this.oEC[k], this.dgvnns.Rows[k].Cells[1].Value, false))
					{
						this.dgvnns.Rows[k].Cells[1].Style.BackColor = Color.LemonChiffon;
						num5 += 100;
					}
					if (Operators.ConditionalCompareObjectNotEqual(this.oT[k], this.dgvnns.Rows[k].Cells[3].Value, false))
					{
						this.dgvnns.Rows[k].Cells[3].Style.BackColor = Color.LemonChiffon;
						num5 += 10;
					}
					if (Operators.ConditionalCompareObjectNotEqual(this.oGOS[k], this.dgvnns.Rows[k].Cells[2].Value, false))
					{
						this.dgvnns.Rows[k].Cells[2].Style.BackColor = Color.LemonChiffon;
						num5++;
					}
					unchecked
					{
						switch (num5)
						{
						case 1000:
							num = Conversions.ToDouble(Operators.DivideObject(this.dgvnns.Rows[k].Cells[4].Value, this.oGDP[k]));
							this.dgvnns.Rows[k].Cells[1].Value = Operators.MultiplyObject(this.dgvnns.Rows[k].Cells[1].Value, num);
							this.dgvnns.Rows[k].Cells[1].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[2].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[3].Style.BackColor = Color.GreenYellow;
							if (this.oGOS[k] > 0.0)
							{
								this.dgvnns.Rows[k].Cells[2].Value = Operators.MultiplyObject(this.dgvnns.Rows[k].Cells[2].Value, num);
							}
							else if (Operators.ConditionalCompareObjectGreater(this.dgvnns.Rows[k].Cells[4].Value, this.oGDP[k], false))
							{
								this.dgvnns.Rows[k].Cells[2].Value = Operators.SubtractObject(Operators.AddObject(this.oGOS[k], this.dgvnns.Rows[k].Cells[4].Value), this.oGDP[k]);
							}
							else
							{
								this.dgvnns.Rows[k].Cells[2].Value = Operators.MultiplyObject(this.dgvnns.Rows[k].Cells[2].Value, num);
							}
							this.dgvnns.Rows[k].Cells[3].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.dgvnns.Rows[k].Cells[2].Value);
							break;
						case 100:
							num = Conversions.ToDouble(Operators.DivideObject(this.dgvnns.Rows[k].Cells[1].Value, this.oEC[k]));
							this.dgvnns.Rows[k].Cells[4].Value = Operators.MultiplyObject(this.dgvnns.Rows[k].Cells[4].Value, num);
							this.dgvnns.Rows[k].Cells[4].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[2].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[3].Style.BackColor = Color.GreenYellow;
							if (this.oGOS[k] > 0.0)
							{
								this.dgvnns.Rows[k].Cells[2].Value = Operators.MultiplyObject(this.dgvnns.Rows[k].Cells[2].Value, num);
							}
							else if (Operators.ConditionalCompareObjectGreater(this.dgvnns.Rows[k].Cells[4].Value, this.oGDP[k], false))
							{
								this.dgvnns.Rows[k].Cells[2].Value = Operators.SubtractObject(Operators.AddObject(this.oGOS[k], this.dgvnns.Rows[k].Cells[4].Value), this.oGDP[k]);
							}
							else
							{
								this.dgvnns.Rows[k].Cells[2].Value = Operators.MultiplyObject(this.dgvnns.Rows[k].Cells[2].Value, num);
							}
							this.dgvnns.Rows[k].Cells[3].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.dgvnns.Rows[k].Cells[2].Value);
							break;
						case 10:
							this.dgvnns.Rows[k].Cells[2].Value = Operators.SubtractObject(this.oGOS[k], Operators.SubtractObject(this.dgvnns.Rows[k].Cells[3].Value, this.oT[k]));
							this.dgvnns.Rows[k].Cells[2].Style.BackColor = Color.GreenYellow;
							break;
						case 1:
							this.dgvnns.Rows[k].Cells[3].Value = Operators.SubtractObject(this.oT[k], Operators.SubtractObject(this.dgvnns.Rows[k].Cells[2].Value, this.oGOS[k]));
							this.dgvnns.Rows[k].Cells[3].Style.BackColor = Color.GreenYellow;
							break;
						case 1100:
							num = Conversions.ToDouble(Operators.DivideObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.oGDP[k] - this.oEC[k]));
							this.dgvnns.Rows[k].Cells[2].Value = num * this.oGOS[k];
							this.dgvnns.Rows[k].Cells[2].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[3].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.dgvnns.Rows[k].Cells[2].Value);
							this.dgvnns.Rows[k].Cells[3].Style.BackColor = Color.GreenYellow;
							break;
						case 1010:
							this.dgvnns.Rows[k].Cells[1].Value = Operators.MultiplyObject(this.oEC[k], Operators.DivideObject(this.dgvnns.Rows[k].Cells[4].Value, this.oGDP[k]));
							this.dgvnns.Rows[k].Cells[1].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[2].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.dgvnns.Rows[k].Cells[3].Value);
							this.dgvnns.Rows[k].Cells[2].Style.BackColor = Color.GreenYellow;
							break;
						case 1001:
							this.dgvnns.Rows[k].Cells[1].Value = Operators.DivideObject(this.dgvnns.Rows[k].Cells[4].Value, this.oGDP[k]);
							this.dgvnns.Rows[k].Cells[1].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[3].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.dgvnns.Rows[k].Cells[2].Value);
							this.dgvnns.Rows[k].Cells[3].Style.BackColor = Color.GreenYellow;
							break;
						case 110:
							this.dgvnns.Rows[k].Cells[4].Value = Operators.MultiplyObject(this.oGDP[k], Operators.DivideObject(this.dgvnns.Rows[k].Cells[1].Value, this.oEC[k]));
							this.dgvnns.Rows[k].Cells[4].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[2].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.dgvnns.Rows[k].Cells[3].Value);
							this.dgvnns.Rows[k].Cells[2].Style.BackColor = Color.GreenYellow;
							break;
						case 101:
							this.dgvnns.Rows[k].Cells[4].Value = Operators.MultiplyObject(this.oGDP[k], Operators.DivideObject(this.dgvnns.Rows[k].Cells[1].Value, this.oEC[k]));
							this.dgvnns.Rows[k].Cells[4].Style.BackColor = Color.GreenYellow;
							this.dgvnns.Rows[k].Cells[3].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[2].Value), this.dgvnns.Rows[k].Cells[1].Value);
							this.dgvnns.Rows[k].Cells[3].Style.BackColor = Color.GreenYellow;
							break;
						case 11:
							num = Conversions.ToDouble(this.dgvnns.Rows[k].Cells[2].Value);
							this.dgvnns.Rows[k].Cells[4].Value = Operators.AddObject(Operators.AddObject(this.dgvnns.Rows[k].Cells[3].Value, num), this.dgvnns.Rows[k].Cells[1].Value);
							this.dgvnns.Rows[k].Cells[4].Style.BackColor = Color.GreenYellow;
							break;
						case 1110:
							this.dgvnns.Rows[k].Cells[2].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.dgvnns.Rows[k].Cells[3].Value);
							this.dgvnns.Rows[k].Cells[2].Style.BackColor = Color.GreenYellow;
							break;
						case 1101:
							this.dgvnns.Rows[k].Cells[3].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[1].Value), this.dgvnns.Rows[k].Cells[2].Value);
							this.dgvnns.Rows[k].Cells[3].Style.BackColor = Color.GreenYellow;
							break;
						case 1011:
							this.dgvnns.Rows[k].Cells[1].Value = Operators.SubtractObject(Operators.SubtractObject(this.dgvnns.Rows[k].Cells[4].Value, this.dgvnns.Rows[k].Cells[3].Value), this.dgvnns.Rows[k].Cells[2].Value);
							break;
						case 111:
							num = Conversions.ToDouble(this.dgvnns.Rows[k].Cells[2].Value);
							this.dgvnns.Rows[k].Cells[4].Value = Operators.AddObject(Operators.AddObject(this.dgvnns.Rows[k].Cells[1].Value, num), this.dgvnns.Rows[k].Cells[3].Value);
							this.dgvnns.Rows[k].Cells[4].Style.BackColor = Color.GreenYellow;
							break;
						case 1111:
							Interaction.MsgBox("Manual adjustment mandatory", MsgBoxStyle.OkOnly, null);
							break;
						}
					}
				}
				int num6 = this.dgvnns.RowCount - 1;
				for (int l = 0; l <= num6; l++)
				{
					if (Conversions.ToBoolean(Operators.AndObject(Operators.AndObject(Operators.CompareObjectEqual(this.oEmp[l], this.dgvnns.Rows[l].Cells[0].Value, false), Operators.CompareObjectNotEqual(this.oEC[l], this.dgvnns.Rows[l].Cells[1].Value, false)), num5 != 1111)))
					{
						this.dgvnns.Rows[l].Cells[0].Style.BackColor = Color.GreenYellow;
						num = Conversions.ToDouble(Operators.DivideObject(this.dgvnns.Rows[l].Cells[1].Value, this.oEC[l]));
						this.dgvnns.Rows[l].Cells[0].Value = unchecked(this.oEmp[l] * num);
					}
				}
				int num7 = this.dgvnns.RowCount - 1;
				for (int m = 0; m <= num7; m++)
				{
					if (array[m] == 2)
					{
						this.dgvnns.Rows[m].Cells[1].Style.BackColor = Color.GreenYellow;
					}
				}
				double num8 = 0.0;
				if (this.NumCol > 3)
				{
					int num9 = this.dgvnns.RowCount - 1;
					for (int n = 0; n <= num9; n++)
					{
						this.oEmp[n] = Conversions.ToDouble(this.dgvnns.Rows[n].Cells[0].Value);
						this.oEC[n] = Conversions.ToDouble(this.dgvnns.Rows[n].Cells[1].Value);
						this.oGOS[n] = Conversions.ToDouble(this.dgvnns.Rows[n].Cells[2].Value);
						this.oT[n] = Conversions.ToDouble(this.dgvnns.Rows[n].Cells[3].Value);
						this.oGDP[n] = Conversions.ToDouble(this.dgvnns.Rows[n].Cells[4].Value);
					}
				}
				int num10 = this.dgvnns.RowCount - 1;
				for (int num11 = 0; num11 <= num10; num11++)
				{
					this.dgvnns.Rows[num11].ErrorText = "";
				}
				if (this.NumCol > 3)
				{
					double num12 = 0.0;
					double num13 = 0.0;
					double num14 = 0.0;
					double num15 = 0.0;
					double num16 = 0.0;
					int num17 = 0;
					int num18 = this.dgvnns.RowCount - 1;
					for (int num19 = 0; num19 <= num18; num19++)
					{
						num13 = Conversions.ToDouble(this.dgvnns.Rows[num19].Cells[1].Value);
						num15 = Conversions.ToDouble(this.dgvnns.Rows[num19].Cells[2].Value);
						num14 = Conversions.ToDouble(this.dgvnns.Rows[num19].Cells[3].Value);
						num16 = Conversions.ToDouble(this.dgvnns.Rows[num19].Cells[4].Value);
						this.dgvnns.Rows[num19].DefaultCellStyle.BackColor = this.edit_color;
						if (Math.Abs(unchecked(num13 + num15 + num14 - num16)) > 1.0)
						{
							this.Label2.Show();
							this.Rebalance2.BackColor = Color.LightGreen;
							this.dgvnns.Rows[num19].ErrorText = "Gross Product must equal the sum of Employee Compensation, Gross Operating Surplus, and Taxes.";
							num17++;
						}
						else if (Conversions.ToBoolean(Operators.AndObject(Operators.CompareObjectNotEqual(this.oEmp[num19], this.dgvnns.Rows[num19].Cells[0].Value, false), Operators.CompareObjectEqual(this.oEC[num19], this.dgvnns.Rows[num19].Cells[1].Value, false))))
						{
							this.Rebalance2.BackColor = Color.LightGreen;
							this.dgvnns.Rows[num19].ErrorText = "Employment changes may necessitate Compensation adjustments";
							num17++;
						}
						else if (Conversions.ToBoolean(Operators.AndObject(Operators.AndObject(Operators.CompareObjectEqual(this.oEmp[num19], this.dgvnns.Rows[num19].Cells[0].Value, false), Operators.CompareObjectNotEqual(this.oEC[num19], this.dgvnns.Rows[num19].Cells[1].Value, false)), Math.Abs(unchecked(num13 + num15 + num14 - num16)) < 1.0)))
						{
							this.Rebalance2.BackColor = Color.LightGreen;
							this.dgvnns.Rows[num19].ErrorText = "Compensation changes may necessitate Employment adjustments";
							num17++;
						}
					}
				}
				this.Label2.Hide();
			}
		}
	}
}
