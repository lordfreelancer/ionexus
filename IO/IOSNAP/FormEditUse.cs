using Matrix_Lib;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace IOSNAP
{
	[DesignerGenerated]
	public class FormEditUse : Form
	{
		public IContainer components;

        public Button ProceedButton;

        public Button Cancel_Button;

       public DataGridView dgvnn;

       public TrackBar tbar;

       public Button UndoButton;

        public ListBox headersListBox;

        public Label Label1;


        public Label Label2;

        public TrackBar tbar2;

        public DataGridView summarydgvnn;

        public Label Label4;

        public Label label_sectoredited;

        public ToolTip ToolTip1;

		private ArrayList headersCols;

		private string[] headersRows;

		public ViewOptions view_options;

		private int cellwidth;

		private BEAData bdat;

		public bool isEdited;

		private Vector modifiedColumn;

		private Color non_edit_color;

		private Color edit_color;

		private Color s_color;

		private const string ed_field = ".";

		//internal virtual Button ProceedButton
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._ProceedButton;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.ProceedButton_Click;
		//		Button proceedButton = this._ProceedButton;
		//		if (proceedButton != null)
		//		{
		//			proceedButton.Click -= value2;
		//		}
		//		this._ProceedButton = value;
		//		proceedButton = this._ProceedButton;
		//		if (proceedButton != null)
		//		{
		//			proceedButton.Click += value2;
		//		}
		//	}
		//}

		//internal virtual Button Cancel_Button
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual DataGridView dgvnn
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._dgvnn;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		DataGridViewCellEventHandler value2 = this.dgv3_CellClick;
		//		DataGridViewCellEventHandler value3 = this.dgv3_CellChange;
		//		DataGridViewCellEventHandler value4 = this.dgv3_CellValueChanged;
		//		DataGridViewCellMouseEventHandler value5 = this.CellHeader_Click;
		//		DataGridView dgvnn = this._dgvnn;
		//		if (dgvnn != null)
		//		{
		//			dgvnn.CellClick -= value2;
		//			dgvnn.CellEndEdit -= value3;
		//			dgvnn.CellValueChanged -= value4;
		//			dgvnn.ColumnHeaderMouseClick -= value5;
		//		}
		//		this._dgvnn = value;
		//		dgvnn = this._dgvnn;
		//		if (dgvnn != null)
		//		{
		//			dgvnn.CellClick += value2;
		//			dgvnn.CellEndEdit += value3;
		//			dgvnn.CellValueChanged += value4;
		//			dgvnn.ColumnHeaderMouseClick += value5;
		//		}
		//	}
		//}

		//internal virtual TrackBar tbar
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._tbar;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.tbar_Changed;
		//		TrackBar tbar = this._tbar;
		//		if (tbar != null)
		//		{
		//			tbar.ValueChanged -= value2;
		//		}
		//		this._tbar = value;
		//		tbar = this._tbar;
		//		if (tbar != null)
		//		{
		//			tbar.ValueChanged += value2;
		//		}
		//	}
		//}

		//internal virtual Button UndoButton
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._UndoButton;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.Undo_Click;
		//		Button undoButton = this._UndoButton;
		//		if (undoButton != null)
		//		{
		//			undoButton.Click -= value2;
		//		}
		//		this._UndoButton = value;
		//		undoButton = this._UndoButton;
		//		if (undoButton != null)
		//		{
		//			undoButton.Click += value2;
		//		}
		//	}
		//}

		//internal virtual ListBox headersListBox
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._headersListBox;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.ListBoxRegions_SelectedIndexChanged;
		//		ListBox headersListBox = this._headersListBox;
		//		if (headersListBox != null)
		//		{
		//			headersListBox.SelectedIndexChanged -= value2;
		//		}
		//		this._headersListBox = value;
		//		headersListBox = this._headersListBox;
		//		if (headersListBox != null)
		//		{
		//			headersListBox.SelectedIndexChanged += value2;
		//		}
		//	}
		//}

		//internal virtual Label Label1
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual Label Label2
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual TrackBar tbar2
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._tbar2;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.tbar2_Changed;
		//		TrackBar tbar = this._tbar2;
		//		if (tbar != null)
		//		{
		//			tbar.ValueChanged -= value2;
		//		}
		//		this._tbar2 = value;
		//		tbar = this._tbar2;
		//		if (tbar != null)
		//		{
		//			tbar.ValueChanged += value2;
		//		}
		//	}
		//}

		//internal virtual DataGridView summarydgvnn
		//{
		//	[CompilerGenerated]
		//	get
		//	{
		//		return this._summarydgvnn;
		//	}
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	[CompilerGenerated]
		//	set
		//	{
		//		EventHandler value2 = this.dgvnsummary_selectionchanged;
		//		DataGridView summarydgvnn = this._summarydgvnn;
		//		if (summarydgvnn != null)
		//		{
		//			summarydgvnn.SelectionChanged -= value2;
		//		}
		//		this._summarydgvnn = value;
		//		summarydgvnn = this._summarydgvnn;
		//		if (summarydgvnn != null)
		//		{
		//			summarydgvnn.SelectionChanged += value2;
		//		}
		//	}
		//}

		//internal virtual Label Label4
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual Label label_sectoredited
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		//internal virtual ToolTip ToolTip1
		//{
		//	get;
		//	[MethodImpl(MethodImplOptions.Synchronized)]
		//	set;
		//}

		[DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.components != null)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			this.ProceedButton = new Button();
			this.Cancel_Button = new Button();
			this.dgvnn = new DataGridView();
			this.tbar = new TrackBar();
			this.UndoButton = new Button();
			this.headersListBox = new ListBox();
			this.Label1 = new Label();
			this.Label2 = new Label();
			this.tbar2 = new TrackBar();
			this.summarydgvnn = new DataGridView();
			this.Label4 = new Label();
			this.label_sectoredited = new Label();
			this.ToolTip1 = new ToolTip(this.components);
			((ISupportInitialize)this.dgvnn).BeginInit();
			((ISupportInitialize)this.tbar).BeginInit();
			((ISupportInitialize)this.tbar2).BeginInit();
			((ISupportInitialize)this.summarydgvnn).BeginInit();
			base.SuspendLayout();
			this.ProceedButton.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.ProceedButton.DialogResult = DialogResult.OK;
			this.ProceedButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.ProceedButton.Location = new Point(895, 732);
			this.ProceedButton.Margin = new Padding(3, 4, 3, 4);
			this.ProceedButton.Name = "ProceedButton";
			this.ProceedButton.Size = new Size(140, 50);
			this.ProceedButton.TabIndex = 0;
			this.ProceedButton.Text = "Proceed";
			this.ToolTip1.SetToolTip(this.ProceedButton, "Press to start processing entered Use table changes");
			this.ProceedButton.UseVisualStyleBackColor = true;
			this.Cancel_Button.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.Cancel_Button.DialogResult = DialogResult.Cancel;
			this.Cancel_Button.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.Cancel_Button.Location = new Point(749, 732);
			this.Cancel_Button.Margin = new Padding(3, 4, 3, 4);
			this.Cancel_Button.Name = "Cancel_Button";
			this.Cancel_Button.Size = new Size(140, 50);
			this.Cancel_Button.TabIndex = 1;
			this.Cancel_Button.Text = "Cancel";
			this.ToolTip1.SetToolTip(this.Cancel_Button, "Quit this form without commiting to any changes");
			this.Cancel_Button.UseVisualStyleBackColor = true;
			this.dgvnn.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvnn.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
			this.dgvnn.BackgroundColor = SystemColors.Control;
			this.dgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvnn.Location = new Point(440, 176);
			this.dgvnn.Margin = new Padding(3, 4, 3, 4);
			this.dgvnn.Name = "dgvnn";
			this.dgvnn.RowTemplate.Height = 28;
			this.dgvnn.Size = new Size(688, 533);
			this.dgvnn.TabIndex = 2;
			this.tbar.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			this.tbar.Location = new Point(12, 144);
			this.tbar.Margin = new Padding(3, 4, 3, 4);
			this.tbar.Name = "tbar";
			this.tbar.Orientation = Orientation.Vertical;
			this.tbar.RightToLeft = RightToLeft.No;
			this.tbar.RightToLeftLayout = true;
			this.tbar.Size = new Size(69, 638);
			this.tbar.TabIndex = 3;
			this.tbar.TickStyle = TickStyle.None;
			this.ToolTip1.SetToolTip(this.tbar, "Slide selected Industry sector ");
			this.UndoButton.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.UndoButton.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
			this.UndoButton.Location = new Point(490, 732);
			this.UndoButton.Margin = new Padding(3, 4, 3, 4);
			this.UndoButton.Name = "UndoButton";
			this.UndoButton.Size = new Size(140, 50);
			this.UndoButton.TabIndex = 4;
			this.UndoButton.Tag = "";
			this.UndoButton.Text = "Reset";
			this.ToolTip1.SetToolTip(this.UndoButton, "Reset the Use editing form");
			this.UndoButton.UseVisualStyleBackColor = true;
			this.headersListBox.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			this.headersListBox.Font = new Font("Verdana", 8f, FontStyle.Regular, GraphicsUnit.Point, 0);
			this.headersListBox.FormattingEnabled = true;
			this.headersListBox.ItemHeight = 18;
			this.headersListBox.Location = new Point(53, 144);
			this.headersListBox.Margin = new Padding(3, 4, 3, 4);
			this.headersListBox.Name = "headersListBox";
			this.headersListBox.Size = new Size(370, 616);
			this.headersListBox.TabIndex = 8;
			this.Label1.AutoSize = true;
			this.Label1.Font = new Font("Calibri", 10f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label1.ForeColor = SystemColors.ControlText;
			this.Label1.Location = new Point(463, 144);
			this.Label1.Name = "Label1";
			this.Label1.Size = new Size(120, 24);
			this.Label1.TabIndex = 14;
			this.Label1.Text = "Commodities";
			this.Label2.AutoSize = true;
			this.Label2.Font = new Font("Calibri", 12f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label2.ForeColor = SystemColors.HotTrack;
			this.Label2.Location = new Point(114, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new Size(174, 29);
			this.Label2.TabIndex = 15;
			this.Label2.Text = "List of Industries";
			this.tbar2.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
			this.tbar2.Location = new Point(589, 134);
			this.tbar2.Margin = new Padding(3, 4, 3, 4);
			this.tbar2.Name = "tbar2";
			this.tbar2.Size = new Size(539, 69);
			this.tbar2.TabIndex = 17;
			this.tbar2.TickStyle = TickStyle.None;
			this.ToolTip1.SetToolTip(this.tbar2, "Slide to change Industry sector");
			this.summarydgvnn.AccessibleRole = AccessibleRole.None;
			this.summarydgvnn.AllowUserToDeleteRows = false;
			this.summarydgvnn.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
			this.summarydgvnn.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
			this.summarydgvnn.BackgroundColor = SystemColors.Control;
			this.summarydgvnn.BorderStyle = BorderStyle.None;
			this.summarydgvnn.CausesValidation = false;
			this.summarydgvnn.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.summarydgvnn.Location = new Point(579, 13);
			this.summarydgvnn.Margin = new Padding(3, 4, 3, 4);
			this.summarydgvnn.MultiSelect = false;
			this.summarydgvnn.Name = "summarydgvnn";
			this.summarydgvnn.ReadOnly = true;
			this.summarydgvnn.RowTemplate.Height = 28;
			this.summarydgvnn.ScrollBars = ScrollBars.None;
			this.summarydgvnn.Size = new Size(310, 99);
			this.summarydgvnn.TabIndex = 18;
			this.ToolTip1.SetToolTip(this.summarydgvnn, "Summary table");
			this.Label4.AutoSize = true;
			this.Label4.Font = new Font("Calibri", 24f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.Label4.ForeColor = SystemColors.HotTrack;
			this.Label4.Location = new Point(43, 23);
			this.Label4.Name = "Label4";
			this.Label4.Size = new Size(181, 59);
			this.Label4.TabIndex = 35;
			this.Label4.Text = "Full Use";
			this.label_sectoredited.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
			this.label_sectoredited.AutoSize = true;
			this.label_sectoredited.Font = new Font("Calibri", 11f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 0);
			this.label_sectoredited.ForeColor = SystemColors.HotTrack;
			this.label_sectoredited.Location = new Point(436, 104);
			this.label_sectoredited.Name = "label_sectoredited";
			this.label_sectoredited.Size = new Size(72, 27);
			this.label_sectoredited.TabIndex = 36;
			this.label_sectoredited.Text = "Label4";
			base.AutoScaleDimensions = new SizeF(9f, 22f);
			base.AutoScaleMode = AutoScaleMode.Font;
			this.AutoScroll = true;
			base.ClientSize = new Size(1140, 826);
			base.Controls.Add(this.dgvnn);
			base.Controls.Add(this.label_sectoredited);
			base.Controls.Add(this.Label4);
			base.Controls.Add(this.summarydgvnn);
			base.Controls.Add(this.Label2);
			base.Controls.Add(this.Label1);
			base.Controls.Add(this.headersListBox);
			base.Controls.Add(this.UndoButton);
			base.Controls.Add(this.tbar);
			base.Controls.Add(this.Cancel_Button);
			base.Controls.Add(this.ProceedButton);
			base.Controls.Add(this.tbar2);
			this.Font = new Font("Calibri", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
			base.Margin = new Padding(3, 4, 3, 4);
			base.MinimizeBox = false;
			this.MinimumSize = new Size(1148, 873);
			base.Name = "FormEditUse";
			this.Text = "The USE table editing form";
			((ISupportInitialize)this.dgvnn).EndInit();
			((ISupportInitialize)this.tbar).EndInit();
			((ISupportInitialize)this.tbar2).EndInit();
			((ISupportInitialize)this.summarydgvnn).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public unsafe FormEditUse(ref BEAData b)
		{
			base.Load += this.Form_Load;
			this.headersCols = new ArrayList();
			this.cellwidth = 125;
			this.isEdited = false;
			this.non_edit_color = Color.LightGray;
			this.edit_color = SystemColors.Window;
			this.s_color = SystemColors.ScrollBar;
			this.InitializeComponent();
			this.bdat = b;
			string[] hdrInd = this.bdat.NTables.hdrInd;
			foreach (string value in hdrInd)
			{
				this.headersCols.Add(value);
			}
			this.headersRows = this.bdat.hdrComm;
            //TODO Need to check logic
            //ref string[] reference;
            //*(ref reference = ref this.headersRows) = (string[])Utils.CopyArray(reference, new string[checked(this.bdat.hdrComm.Length + this.bdat.hdrVA.Length + 1)]);
            Utils.CopyArray(this.headersRows, new string[checked(this.bdat.hdrComm.Length + this.bdat.hdrVA.Length + 1)]);
            int num = this.bdat.hdrComm.Length;
			checked
			{
				int num2 = this.bdat.hdrComm.Length + this.bdat.hdrVA.Length - 1;
				for (int j = num; j <= num2; j++)
				{
					this.headersRows[j] = this.bdat.hdrVA[j - this.bdat.hdrComm.Length];
				}
				this.tbar2.Maximum = this.headersCols.Count - 1;
				this.tbar2.Minimum = 0;
				this.tbar2.Value = 0;
				this.tbar.Maximum = this.headersCols.Count - 1;
				this.tbar.Minimum = 0;
				this.tbar.Value = this.headersCols.Count - 1;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.headersCols.GetEnumerator();
					while (enumerator.MoveNext())
					{
						string item = Conversions.ToString(enumerator.Current);
						this.headersListBox.Items.Add(item);
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				this.headersListBox.SelectedIndex = 0;
				this.label_sectoredited.Text = Conversions.ToString(Operators.ConcatenateObject(this.headersCols[0], "."));
				this.view_options = new ViewOptions();
			}
		}

		private void Form_Load(object sender, EventArgs e)
		{
			this.dgvnn.Columns.Clear();
			this.summarydgvnn.Columns.Clear();
			this.dgvnn.RightToLeft = RightToLeft.No;
			this.dgvnn.RowHeadersWidth = 275;
			this.dgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.dgvnn.RowTemplate.Height = 24;
			this.summarydgvnn.RightToLeft = RightToLeft.No;
			this.summarydgvnn.RowHeadersWidth = 150;
			this.summarydgvnn.RowTemplate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			this.summarydgvnn.RowTemplate.Height = 24;
			checked
			{
				int num = this.headersCols.Count - 1;
				for (int i = 0; i <= num; i++)
				{
					string text = Conversions.ToString(this.headersCols[i]);
					this.dgvnn.Columns.Add(text, text);
				}
				this.summarydgvnn.Columns.Add("a", "a");
				this.summarydgvnn.ColumnHeadersVisible = false;
				int num2 = 1;
				IEnumerator enumerator = default(IEnumerator);
				try
				{
					enumerator = this.dgvnn.Columns.GetEnumerator();
					while (enumerator.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn = unchecked((DataGridViewColumn)enumerator.Current);
						dataGridViewColumn.MinimumWidth = this.cellwidth;
						dataGridViewColumn.SortMode = DataGridViewColumnSortMode.NotSortable;
						if (num2 == 1)
						{
							dataGridViewColumn.ReadOnly = false;
						}
						else
						{
							dataGridViewColumn.ReadOnly = true;
							dataGridViewColumn.DefaultCellStyle.BackColor = Color.LightGray;
						}
						num2++;
					}
				}
				finally
				{
					if (enumerator is IDisposable)
					{
						(enumerator as IDisposable).Dispose();
					}
				}
				this.dgvnn.RowHeadersVisible = true;
				this.dgvnn.AllowUserToAddRows = false;
				IEnumerator enumerator2 = default(IEnumerator);
				try
				{
					enumerator2 = this.summarydgvnn.Columns.GetEnumerator();
					while (enumerator2.MoveNext())
					{
						DataGridViewColumn dataGridViewColumn2 = unchecked((DataGridViewColumn)enumerator2.Current);
						dataGridViewColumn2.MinimumWidth = 150;
						dataGridViewColumn2.SortMode = DataGridViewColumnSortMode.NotSortable;
						dataGridViewColumn2.ReadOnly = true;
						dataGridViewColumn2.DefaultCellStyle.BackColor = this.s_color;
						dataGridViewColumn2.Selected = false;
					}
				}
				finally
				{
					if (enumerator2 is IDisposable)
					{
						(enumerator2 as IDisposable).Dispose();
					}
				}
				this.summarydgvnn.RowHeadersVisible = true;
				this.summarydgvnn.IsAccessible = false;
				int num3 = this.bdat.hdrComm.Length - 1;
				decimal num5;
				DataGridViewRow dataGridViewRow;
				for (int j = 0; j <= num3; j++)
				{
					dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					int num4 = this.bdat.Use.NoCols - 1;
					for (int k = 0; k <= num4; k++)
					{
						decimal d = new decimal(this.bdat.Use[j, k]);
						DataGridViewCell dataGridViewCell = dataGridViewRow.Cells[k];
						num5 = decimal.Round(d, 2);
						dataGridViewCell.Value = num5.ToString("f2");
					}
					dataGridViewRow.HeaderCell.Value = this.headersRows[j];
					this.dgvnn.Rows.Add(dataGridViewRow);
				}
				int num6 = this.bdat.hdrVA.Length - 1;
				for (int l = 0; l <= num6; l++)
				{
					dataGridViewRow = new DataGridViewRow();
					dataGridViewRow.CreateCells(this.dgvnn);
					int num7 = this.bdat.Use.NoCols - 1;
					for (int m = 0; m <= num7; m++)
					{
						decimal d2 = new decimal(this.bdat.NTables.VA[l, m]);
						DataGridViewCell dataGridViewCell2 = dataGridViewRow.Cells[m];
						num5 = decimal.Round(d2, 2);
						dataGridViewCell2.Value = num5.ToString("f2");
					}
					dataGridViewRow.HeaderCell.Value = this.headersRows[this.bdat.hdrComm.Length + l];
					this.dgvnn.Rows.Add(dataGridViewRow);
				}
				dataGridViewRow = new DataGridViewRow();
				dataGridViewRow.CreateCells(this.summarydgvnn);
				decimal d3 = new decimal(this.getColSumUse(0));
				DataGridViewCell dataGridViewCell3 = dataGridViewRow.Cells[0];
				num5 = decimal.Round(d3, 2);
				dataGridViewCell3.Value = num5.ToString("f2");
				dataGridViewRow.HeaderCell.Value = "Edited Total";
				this.summarydgvnn.Rows.Add(dataGridViewRow);
				dataGridViewRow = new DataGridViewRow();
				dataGridViewRow.CreateCells(this.summarydgvnn);
				DataGridViewCell dataGridViewCell4 = dataGridViewRow.Cells[0];
				num5 = decimal.Round(d3, 2);
				dataGridViewCell4.Value = num5.ToString("f2");
				dataGridViewRow.HeaderCell.Value = "Pre-Edit Total";
				this.summarydgvnn.Rows.Add(dataGridViewRow);
				dataGridViewRow = new DataGridViewRow();
				dataGridViewRow.CreateCells(this.summarydgvnn);
				d3 = Conversions.ToDecimal(Operators.SubtractObject(this.summarydgvnn.Rows[0].Cells[0].Value, this.summarydgvnn.Rows[1].Cells[0].Value));
				DataGridViewCell dataGridViewCell5 = dataGridViewRow.Cells[0];
				num5 = decimal.Round(d3, 2);
				dataGridViewCell5.Value = num5.ToString("f2");
				dataGridViewRow.HeaderCell.Value = "Difference";
				dataGridViewRow.DefaultCellStyle.BackColor = Color.LightSteelBlue;
				this.summarydgvnn.Rows.Add(dataGridViewRow);
				this.summarydgvnn.AllowUserToAddRows = false;
				int count = this.dgvnn.Rows.Count;
				this.modifiedColumn = new Vector(count);
			}
		}

		private void dgvnsummary_selectionchanged(object sender, EventArgs e)
		{
			this.summarydgvnn.ClearSelection();
		}

		private double getColSumUse(int id)
		{
			Vector vector = new Vector(1);
			checked
			{
				if (id < this.bdat.Use.NoCols)
				{
					vector = new Vector(this.bdat.Use.NoRows + this.bdat.NTables.VA.NoRows);
					int num = this.bdat.Use.NoRows - 1;
					for (int i = 0; i <= num; i++)
					{
						vector[i] = this.bdat.Use[i, id];
					}
					int num2 = 0;
					int noRows = this.bdat.Use.NoRows;
					int num3 = this.bdat.Use.NoRows + this.bdat.NTables.VA.NoRows + -1;
					for (int j = noRows; j <= num3; j++)
					{
						vector[j] = this.bdat.NTables.VA[num2, id];
						num2++;
					}
				}
				else
				{
					vector = new Vector(this.bdat.FinDem.NoRows);
					int num4 = this.bdat.FinDem.NoRows - 1;
					for (int k = 0; k <= num4; k++)
					{
						vector[k] = this.bdat.get_FinDemVector(k, id - this.bdat.Use.NoCols);
					}
				}
				return Vector.sum(vector);
			}
		}

		private void ListBoxRegions_SelectedIndexChanged(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State = 2;
			//		this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall = 0;
			//	}
			//	else if (this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall_0024Init);
			//}
			//if (Operators.ConditionalCompareObjectGreater(this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall, 0, false))
			//{
			//	string text = this.headersListBox.SelectedItem.ToString().Trim();
			//	int selectedIndex = this.headersListBox.SelectedIndex;
			//	this.updateDisp(selectedIndex);
			//}
			//this._0024STATIC_0024ListBoxRegions_SelectedIndexChanged_002420211C1280B1_0024fcall = 1;
			this.freezeDisp(true);
		}

		private void updateDisp(int idx)
		{
			string text = this.headersListBox.SelectedItem.ToString().Trim();
			checked
			{
				this.tbar.Value = this.headersCols.Count - idx - 1;
				this.tbar2.Value = idx;
				this.headersListBox.SelectedIndex = idx;
				this.label_sectoredited.Text = Conversions.ToString(Operators.ConcatenateObject(this.headersCols[idx], "."));
				if (idx > 0)
				{
					int num = idx - 1;
					for (int i = 0; i <= num; i++)
					{
						this.dgvnn.Columns[i].ReadOnly = true;
						this.dgvnn.Columns[i].DefaultCellStyle.BackColor = this.non_edit_color;
					}
				}
				if (idx < this.headersCols.Count - 1)
				{
					int num2 = idx + 1;
					int num3 = this.headersCols.Count - 1;
					for (int j = num2; j <= num3; j++)
					{
						this.dgvnn.Columns[j].ReadOnly = true;
						this.dgvnn.Columns[j].DefaultCellStyle.BackColor = this.non_edit_color;
					}
				}
				this.dgvnn.Columns[idx].ReadOnly = false;
				this.dgvnn.Columns[idx].DefaultCellStyle.BackColor = this.edit_color;
				this.dgvnn.CurrentCell = this.dgvnn.Rows[0].Cells[idx];
				int noRows = this.bdat.Use.NoRows;
				decimal d = new decimal(this.getColSumUse(idx));
				this.summarydgvnn.Rows[1].Cells[0].Value = decimal.Round(d, 2).ToString("f2");
				this.updateSummary(idx);
			}
		}

		private void updateSummary(int idx2)
		{
			decimal num;
			decimal d;
			if (this.isEdited)
			{
				d = new decimal(this.getEditedColumnSum());
				DataGridViewCell dataGridViewCell = this.summarydgvnn.Rows[0].Cells[0];
				num = decimal.Round(d, 2);
				dataGridViewCell.Value = num.ToString("f2");
			}
			else
			{
				d = new decimal(this.getColSumUse(idx2));
				DataGridViewCell dataGridViewCell2 = this.summarydgvnn.Rows[0].Cells[0];
				num = decimal.Round(d, 2);
				dataGridViewCell2.Value = num.ToString("f2");
			}
			d = Conversions.ToDecimal(Operators.SubtractObject(this.summarydgvnn.Rows[0].Cells[0].Value, this.summarydgvnn.Rows[1].Cells[0].Value));
			DataGridViewCell dataGridViewCell3 = this.summarydgvnn.Rows[2].Cells[0];
			num = decimal.Round(d, 2);
			dataGridViewCell3.Value = num.ToString("f2");
		}

		private void ToNextButton_Click(object sender, EventArgs e)
		{
			int selectedIndex = this.headersListBox.SelectedIndex;
			checked
			{
				if (selectedIndex < this.headersCols.Count - 1)
				{
					this.headersListBox.SelectedIndex = selectedIndex + 1;
				}
				this.freezeDisp(true);
			}
		}

		private void ToPreviousButton_Click(object sender, EventArgs e)
		{
			int selectedIndex = this.headersListBox.SelectedIndex;
			if (selectedIndex > 0)
			{
				this.headersListBox.SelectedIndex = checked(selectedIndex - 1);
			}
			this.freezeDisp(true);
		}

		private void tbar_Changed(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf_0024Init);
			//}
			//if (this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf > 0)
			//{
			//	int selectedIndex = checked(this.headersCols.Count - this.tbar.Value - 1);
			//	this.headersListBox.SelectedIndex = selectedIndex;
			//}
			//this._0024STATIC_0024tbar_Changed_002420211C1280B1_0024isf = 1;
		}

		private void tbar2_Changed(object sender, EventArgs e)
		{
            //TODO Later
			//if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf_0024Init);
			//}
			//if (this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf > 0)
			//{
			//	int value = this.tbar2.Value;
			//	this.headersListBox.SelectedIndex = value;
			//}
			//this._0024STATIC_0024tbar2_Changed_002420211C1280B1_0024isf = 1;
		}

		private void dgv3_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dataGridView = (DataGridView)sender;
			dataGridView.BeginEdit(true);
			int columnIndex = e.ColumnIndex;
			int rowIndex = e.RowIndex;
		}

		private void dgv3_CellChange(object sender, DataGridViewCellEventArgs e)
		{
            //TODO LATER
			//if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init == null)
			//{
			//	Interlocked.CompareExchange<StaticLocalInitFlag>(ref this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init, new StaticLocalInitFlag(), (StaticLocalInitFlag)null);
			//}
			//Monitor.Enter(this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init);
			//try
			//{
			//	if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State == 0)
			//	{
			//		this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State = 2;
			//		this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf = 0;
			//	}
			//	else if (this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State == 2)
			//	{
			//		throw new IncompleteInitialization();
			//	}
			//}
			//finally
			//{
			//	this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init.State = 1;
			//	Monitor.Exit(this._0024STATIC_0024dgv3_CellChange_002420211C12814D_0024isf_0024Init);
			//}
			int columnIndex = e.ColumnIndex;
			int rowIndex = e.RowIndex;
			checked
			{
				double num = default(double);
				if (double.TryParse(Conversions.ToString(this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value), out num))
				{
					decimal d = new decimal(num);
					string strB = Conversions.ToString(this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value);
					string text = decimal.Round(d, 2).ToString("f2");
					if (string.Compare(text, strB) != 0)
					{
						this.dgvnn.Rows[rowIndex].Cells[columnIndex].Value = text;
					}
					int selectedIndex = this.headersListBox.SelectedIndex;
					double num2 = default(double);
					if (selectedIndex < this.bdat.Use.NoCols)
					{
						num2 = ((rowIndex >= this.bdat.Use.NoRows) ? this.bdat.NTables.VA[rowIndex - this.bdat.Use.NoRows, selectedIndex] : this.bdat.Use[rowIndex, selectedIndex]);
					}
					else if (rowIndex < this.bdat.Use.NoRows)
					{
						num2 = this.bdat.get_FinDemVector(rowIndex, selectedIndex - this.bdat.Use.NoCols);
					}
					if (num != num2)
					{
						this.freezeDisp(false);
						this.isEdited = true;
					}
				}
				if (this.isEdited)
				{
					this.updateSummary(0);
				}
			}
		}

		private void dgv3_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
		}

		private void freezeDisp(bool dispstate)
		{
			this.headersListBox.Enabled = dispstate;
			this.tbar2.Enabled = dispstate;
			this.tbar.Enabled = dispstate;
		}

		private void Undo_Click(object sender, EventArgs e)
		{
			this.freezeDisp(true);
			this.isEdited = false;
			int selectedIndex = this.headersListBox.SelectedIndex;
			checked
			{
				if (selectedIndex > 0)
				{
					this.headersListBox.SelectedIndex = selectedIndex - 1;
					this.headersListBox.SelectedIndex = selectedIndex;
				}
				else
				{
					this.headersListBox.SelectedIndex = selectedIndex + 1;
					this.headersListBox.SelectedIndex = selectedIndex;
				}
				int num = this.bdat.hdrComm.Length - 1;
				decimal num3;
				for (int i = 0; i <= num; i++)
				{
					DataGridViewRow dataGridViewRow = new DataGridViewRow();
					dataGridViewRow = this.dgvnn.Rows[i];
					int num2 = this.bdat.Use.NoCols - 1;
					for (int j = 0; j <= num2; j++)
					{
						decimal d = new decimal(this.bdat.Use[i, j]);
						DataGridViewCell dataGridViewCell = dataGridViewRow.Cells[j];
						num3 = decimal.Round(d, 2);
						dataGridViewCell.Value = num3.ToString("f2");
					}
				}
				int num4 = this.bdat.hdrVA.Length - 1;
				for (int k = 0; k <= num4; k++)
				{
					DataGridViewRow dataGridViewRow2 = new DataGridViewRow();
					dataGridViewRow2 = this.dgvnn.Rows[this.bdat.hdrComm.Length + k];
					int num5 = this.bdat.Use.NoCols - 1;
					for (int l = 0; l <= num5; l++)
					{
						decimal d2 = new decimal(this.bdat.NTables.VA[k, l]);
						DataGridViewCell dataGridViewCell2 = dataGridViewRow2.Cells[l];
						num3 = decimal.Round(d2, 2);
						dataGridViewCell2.Value = num3.ToString("f2");
					}
				}
				this.freezeDisp(true);
			}
		}

		private double getEditedColumnSum()
		{
			double num = 0.0;
			double num2 = 0.0;
			int num3 = this.dgvnn.Rows.Count;
			Vector vector = new Vector(num3);
			int selectedIndex = this.headersListBox.SelectedIndex;
			checked
			{
				if (selectedIndex >= this.bdat.Use.NoCols)
				{
					num3 -= this.bdat.NTables.VA.NoRows;
				}
				int num4 = num3 - 1;
				for (int i = 0; i <= num4; i++)
				{
					if (!double.TryParse(Conversions.ToString(this.dgvnn.Rows[i].Cells[selectedIndex].Value), out num2))
					{
						decimal d = default(decimal);
						try
						{
							if (selectedIndex < this.bdat.Use.NoCols)
							{
								d = new decimal(this.bdat.Use[i, selectedIndex]);
							}
						}
						catch (Exception ex)
						{
							ProjectData.SetProjectError(ex);
							Exception ex2 = ex;
							Interaction.MsgBox(ex2.Message + "  >" + Conversions.ToString(i), MsgBoxStyle.OkOnly, null);
							ProjectData.ClearProjectError();
						}
						this.dgvnn.Rows[i].Cells[1].Value = decimal.Round(d, 2).ToString("f2");
					}
				}
				int num5 = num3 - 1;
				for (int j = 0; j <= num5; j++)
				{
					vector[j] = Conversions.ToDouble(this.dgvnn.Rows[j].Cells[selectedIndex].Value);
				}
				return Vector.sum(vector);
			}
		}

		private void ProceedButton_Click(object sender, EventArgs e)
		{
			int selectedIndex = this.headersListBox.SelectedIndex;
			double num = 0.0;
			Matrix matrix = new Matrix(this.bdat.Use.NoRows, this.bdat.Use.NoCols);
			Matrix matrix2 = new Matrix(this.bdat.NTables.VA.NoRows, this.bdat.NTables.VA.NoCols);
			double num2 = 0.0;
			double num3 = 0.0;
			double num4 = 0.0;
			checked
			{
				int num5 = this.dgvnn.Rows.Count - 1;
				for (int i = 0; i <= num5; i++)
				{
					num2 = Conversions.ToDouble(Operators.AddObject(num2, this.dgvnn.Rows[i].Cells[selectedIndex].Value));
				}
				int num6 = this.bdat.Use.NoRows - 1;
				for (int j = 0; j <= num6; j++)
				{
					num3 = unchecked(num3 + this.bdat.Use[j, selectedIndex]);
				}
				int num7 = this.bdat.NTables.VA.NoRows - 1;
				for (int k = 0; k <= num7; k++)
				{
					num3 = unchecked(num3 + this.bdat.NTables.VA[k, selectedIndex]);
				}
				num4 = unchecked(num2 / num3);
				try
				{
					int num8 = this.bdat.Use.NoRows - 1;
					for (int l = 0; l <= num8; l++)
					{
						int num9 = this.bdat.Use.NoCols - 1;
						for (int m = 0; m <= num9; m++)
						{
							matrix[l, m] = Conversions.ToDouble(this.dgvnn.Rows[l].Cells[m].Value);
						}
					}
					int num10 = this.bdat.NTables.VA.NoRows - 1;
					for (int n = 0; n <= num10; n++)
					{
						int num11 = this.bdat.Use.NoCols - 1;
						for (int num12 = 0; num12 <= num11; num12++)
						{
							matrix2[n, num12] = Conversions.ToDouble(this.dgvnn.Rows[this.bdat.Use.NoRows + n].Cells[num12].Value);
						}
					}
					int num13 = this.bdat.NTables.Make().NoCols - 1;
					for (int num14 = 0; num14 <= num13; num14++)
					{
						this.bdat.NTables.Make()[selectedIndex, num14] = unchecked(num4 * this.bdat.NTables.Make()[selectedIndex, num14]);
					}
					NatTables nTables = this.bdat.NTables;
					Matrix matrix3 = this.bdat.NTables.Make();
					nTables.SetMake(ref matrix3);
					this.bdat.Use = matrix;
					this.bdat.NTables.VA = matrix2;
					this.bdat.ImportExportUpdate();
					if (selectedIndex < this.bdat.NTables.VA.NoRows - 2)
					{
						unchecked
						{
							this.bdat.NTables.Employment[selectedIndex] = num4 * this.bdat.NTables.Employment[selectedIndex];
							this.bdat.NTables.Employment_JOBS[selectedIndex] = num4 * this.bdat.NTables.Employment_JOBS[selectedIndex];
							this.bdat.NTables.Employment_FTE[selectedIndex] = num4 * this.bdat.NTables.Employment_FTE[selectedIndex];
						}
					}
				}
				catch (Exception ex)
				{
					ProjectData.SetProjectError(ex);
					Exception ex2 = ex;
					Interaction.MsgBox("Error while transferring data. Ship abandoned!", MsgBoxStyle.OkOnly, null);
					Interaction.MsgBox(ex2.Message, MsgBoxStyle.OkOnly, null);
					ProjectData.ClearProjectError();
				}
			}
		}

		private void CellHeader_Click(object sender, DataGridViewCellMouseEventArgs e)
		{
			int columnIndex = e.ColumnIndex;
			int index = this.dgvnn.CurrentRow.Index;
			this.dgvnn.ClearSelection();
			this.dgvnn.CurrentCell = this.dgvnn.Rows[index].Cells[columnIndex];
			this.dgvnn.CurrentCell.Selected = true;
			if (!this.isEdited)
			{
				this.headersListBox.SelectedIndex = columnIndex;
			}
		}
	}
}
