using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Deployment.Application;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace IOSNAP.My
{
	[GeneratedCode("MyTemplate", "11.0.0.0")]
	[EditorBrowsable(EditorBrowsableState.Never)]
	internal class MyApplication : WindowsFormsApplicationBase
	{
		[MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        [STAThread]
        internal static void Main(string[] Args)
		{
			try
			{
				Application.SetCompatibleTextRenderingDefault(WindowsFormsApplicationBase.UseCompatibleTextRendering);
			}
			finally
			{
			}
			MyProject.Application.Run(Args);
		}

		private void MyApplication_Startup(object sender, StartupEventArgs e)
		{
			MyApplication.CheckForShortcut();
		}

		private static void CheckForShortcut()
		{
			if (ApplicationDeployment.IsNetworkDeployed)
			{
				ApplicationDeployment currentDeployment = ApplicationDeployment.CurrentDeployment;
				if (currentDeployment.IsFirstRun)
				{
					Assembly executingAssembly = Assembly.GetExecutingAssembly();
					string text = string.Empty;
					string text2 = string.Empty;
					if (Attribute.IsDefined(executingAssembly, typeof(AssemblyCompanyAttribute)))
					{
						AssemblyCompanyAttribute assemblyCompanyAttribute = (AssemblyCompanyAttribute)Attribute.GetCustomAttribute(executingAssembly, typeof(AssemblyCompanyAttribute));
						text = assemblyCompanyAttribute.Company;
					}
					if (Attribute.IsDefined(executingAssembly, typeof(AssemblyDescriptionAttribute)))
					{
						AssemblyDescriptionAttribute assemblyDescriptionAttribute = (AssemblyDescriptionAttribute)Attribute.GetCustomAttribute(executingAssembly, typeof(AssemblyDescriptionAttribute));
						text2 = assemblyDescriptionAttribute.Description;
					}
					if (text.Length > 0 && text2.Length > 0)
					{
						string empty = string.Empty;
						empty = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + text2 + ".appref-ms";
						string empty2 = string.Empty;
						empty2 = Environment.GetFolderPath(Environment.SpecialFolder.Programs) + "\\" + text + "\\" + text2 + ".appref-ms";
						File.Copy(empty2, empty, true);
					}
				}
			}
		}

		[DebuggerStepThrough]
		public MyApplication()
			: base(AuthenticationMode.Windows)
		{
			base.Startup += this.MyApplication_Startup;
			base.IsSingleInstance = false;
			base.EnableVisualStyles = true;
			base.SaveMySettingsOnExit = true;
			base.ShutdownStyle = ShutdownMode.AfterAllFormsClose;
		}

		[DebuggerStepThrough]
		protected override void OnCreateMainForm()
		{
			base.MainForm = MyProject.Forms.frmMain;
		}
	}
}
