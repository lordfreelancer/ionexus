using Microsoft.VisualBasic;
using Microsoft.VisualBasic.ApplicationServices;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace IOSNAP.My
{
	[StandardModule]
	[HideModuleName]
	[GeneratedCode("MyTemplate", "11.0.0.0")]
	internal sealed class MyProject
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		[MyGroupCollection("System.Windows.Forms.Form", "Create__Instance__", "Dispose__Instance__", "My.MyProject.Forms")]
		internal sealed class MyForms
		{
			[ThreadStatic]
			private static Hashtable m_FormBeingCreated;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public dialogImpactEntryForm m_dialogImpactEntryForm;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public DialogProgress m_DialogProgress;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmAboutBox m_frmAboutBox;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmAgg m_frmAgg;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmGetYear m_frmGetYear;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmImpactEntry m_frmImpactEntry;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmMain m_frmMain;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmNotify m_frmNotify;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmReg m_frmReg;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public frmRegMethod m_frmRegMethod;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public programLoadingProgressBar m_programLoadingProgressBar;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public RegionalDataEntryForm m_RegionalDataEntryForm;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public RegistrationForm m_RegistrationForm;

			[EditorBrowsable(EditorBrowsableState.Never)]
			public UserDefinedRegionForm m_UserDefinedRegionForm;

			public dialogImpactEntryForm dialogImpactEntryForm
			{
				[DebuggerHidden]
				get
				{
					this.m_dialogImpactEntryForm = MyForms.Create__Instance__(this.m_dialogImpactEntryForm);
					return this.m_dialogImpactEntryForm;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_dialogImpactEntryForm)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<dialogImpactEntryForm>(ref this.m_dialogImpactEntryForm);
					}
				}
			}

			public DialogProgress DialogProgress
			{
				[DebuggerHidden]
				get
				{
					this.m_DialogProgress = MyForms.Create__Instance__(this.m_DialogProgress);
					return this.m_DialogProgress;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_DialogProgress)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<DialogProgress>(ref this.m_DialogProgress);
					}
				}
			}

			public frmAboutBox frmAboutBox
			{
				[DebuggerHidden]
				get
				{
					this.m_frmAboutBox = MyForms.Create__Instance__(this.m_frmAboutBox);
					return this.m_frmAboutBox;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_frmAboutBox)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<frmAboutBox>(ref this.m_frmAboutBox);
					}
				}
			}

			public frmAgg frmAgg
			{
				[DebuggerHidden]
				get
				{
					this.m_frmAgg = MyForms.Create__Instance__(this.m_frmAgg);
					return this.m_frmAgg;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_frmAgg)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<frmAgg>(ref this.m_frmAgg);
					}
				}
			}

			public frmGetYear frmGetYear
			{
				[DebuggerHidden]
				get
				{
					this.m_frmGetYear = MyForms.Create__Instance__(this.m_frmGetYear);
					return this.m_frmGetYear;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_frmGetYear)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<frmGetYear>(ref this.m_frmGetYear);
					}
				}
			}

			public frmImpactEntry frmImpactEntry
			{
				[DebuggerHidden]
				get
				{
					this.m_frmImpactEntry = MyForms.Create__Instance__(this.m_frmImpactEntry);
					return this.m_frmImpactEntry;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_frmImpactEntry)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<frmImpactEntry>(ref this.m_frmImpactEntry);
					}
				}
			}

			public frmMain frmMain
			{
				[DebuggerHidden]
				get
				{
					this.m_frmMain = MyForms.Create__Instance__(this.m_frmMain);
					return this.m_frmMain;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_frmMain)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<frmMain>(ref this.m_frmMain);
					}
				}
			}

			public frmNotify frmNotify
			{
				[DebuggerHidden]
				get
				{
					this.m_frmNotify = MyForms.Create__Instance__(this.m_frmNotify);
					return this.m_frmNotify;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_frmNotify)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<frmNotify>(ref this.m_frmNotify);
					}
				}
			}

			public frmReg frmReg
			{
				[DebuggerHidden]
				get
				{
					this.m_frmReg = MyForms.Create__Instance__(this.m_frmReg);
					return this.m_frmReg;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_frmReg)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<frmReg>(ref this.m_frmReg);
					}
				}
			}

			public frmRegMethod frmRegMethod
			{
				[DebuggerHidden]
				get
				{
					this.m_frmRegMethod = MyForms.Create__Instance__(this.m_frmRegMethod);
					return this.m_frmRegMethod;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_frmRegMethod)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<frmRegMethod>(ref this.m_frmRegMethod);
					}
				}
			}

			public programLoadingProgressBar programLoadingProgressBar
			{
				[DebuggerHidden]
				get
				{
					this.m_programLoadingProgressBar = MyForms.Create__Instance__(this.m_programLoadingProgressBar);
					return this.m_programLoadingProgressBar;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_programLoadingProgressBar)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<programLoadingProgressBar>(ref this.m_programLoadingProgressBar);
					}
				}
			}

			public RegionalDataEntryForm RegionalDataEntryForm
			{
				[DebuggerHidden]
				get
				{
					this.m_RegionalDataEntryForm = MyForms.Create__Instance__(this.m_RegionalDataEntryForm);
					return this.m_RegionalDataEntryForm;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_RegionalDataEntryForm)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<RegionalDataEntryForm>(ref this.m_RegionalDataEntryForm);
					}
				}
			}

			public RegistrationForm RegistrationForm
			{
				[DebuggerHidden]
				get
				{
					this.m_RegistrationForm = MyForms.Create__Instance__(this.m_RegistrationForm);
					return this.m_RegistrationForm;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_RegistrationForm)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<RegistrationForm>(ref this.m_RegistrationForm);
					}
				}
			}

			public UserDefinedRegionForm UserDefinedRegionForm
			{
				[DebuggerHidden]
				get
				{
					this.m_UserDefinedRegionForm = MyForms.Create__Instance__(this.m_UserDefinedRegionForm);
					return this.m_UserDefinedRegionForm;
				}
				[DebuggerHidden]
				set
				{
					if (value != this.m_UserDefinedRegionForm)
					{
						if (value != null)
						{
							throw new ArgumentException("Property can only be set to Nothing");
						}
						this.Dispose__Instance__<UserDefinedRegionForm>(ref this.m_UserDefinedRegionForm);
					}
				}
			}

			[DebuggerHidden]
			private static T Create__Instance__<T>(T Instance) where T : Form, new()
			{
				if (Instance == null || Instance.IsDisposed)
				{
					if (MyForms.m_FormBeingCreated != null)
					{
						if (MyForms.m_FormBeingCreated.ContainsKey(typeof(T)))
						{
							throw new InvalidOperationException(Utils.GetResourceString("WinForms_RecursiveFormCreate"));
						}
					}
					else
					{
						MyForms.m_FormBeingCreated = new Hashtable();
					}
					MyForms.m_FormBeingCreated.Add(typeof(T), null);
					try
					{
						return new T();
					}
					catch (TargetInvocationException ex2)// when (/*OpCode not supported: BlockContainer*/)
					{
						//TargetInvocationException ex2;
						string resourceString = Utils.GetResourceString("WinForms_SeeInnerException", ex2.InnerException.Message);
						throw new InvalidOperationException(resourceString, ex2.InnerException);
					}
					finally
					{
						MyForms.m_FormBeingCreated.Remove(typeof(T));
					}
				}
				return Instance;
			}

			[DebuggerHidden]
			private void Dispose__Instance__<T>(ref T instance) where T : Form
			{
				instance.Dispose();
				instance = null;
			}

			[DebuggerHidden]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public MyForms()
			{
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public override bool Equals(object o)
			{
				return base.Equals(RuntimeHelpers.GetObjectValue(o));
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			internal new Type GetType()
			{
				return typeof(MyForms);
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			public override string ToString()
			{
				return base.ToString();
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[MyGroupCollection("System.Web.Services.Protocols.SoapHttpClientProtocol", "Create__Instance__", "Dispose__Instance__", "")]
		internal sealed class MyWebServices
		{
			[EditorBrowsable(EditorBrowsableState.Never)]
			[DebuggerHidden]
			public override bool Equals(object o)
			{
				return base.Equals(RuntimeHelpers.GetObjectValue(o));
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			[DebuggerHidden]
			public override int GetHashCode()
			{
				return base.GetHashCode();
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			[DebuggerHidden]
			internal new Type GetType()
			{
				return typeof(MyWebServices);
			}

			[EditorBrowsable(EditorBrowsableState.Never)]
			[DebuggerHidden]
			public override string ToString()
			{
				return base.ToString();
			}

			[DebuggerHidden]
			private static T Create__Instance__<T>(T instance) where T : new()
			{
				if (instance == null)
				{
					return new T();
				}
				return instance;
			}

			[DebuggerHidden]
			private void Dispose__Instance__<T>(ref T instance)
			{
				instance = default(T);
			}

			[DebuggerHidden]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public MyWebServices()
			{
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[ComVisible(false)]
		internal sealed class ThreadSafeObjectProvider<T> where T : new()
		{
			[CompilerGenerated]
			[ThreadStatic]
			private static T m_ThreadStaticValue;

			internal T GetInstance
			{
				[DebuggerHidden]
				get
				{
					if (ThreadSafeObjectProvider<T>.m_ThreadStaticValue == null)
					{
						ThreadSafeObjectProvider<T>.m_ThreadStaticValue = new T();
					}
					return ThreadSafeObjectProvider<T>.m_ThreadStaticValue;
				}
			}

			[DebuggerHidden]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public ThreadSafeObjectProvider()
			{
			}
		}

		private static readonly ThreadSafeObjectProvider<MyComputer> m_ComputerObjectProvider = new ThreadSafeObjectProvider<MyComputer>();

		private static readonly ThreadSafeObjectProvider<MyApplication> m_AppObjectProvider = new ThreadSafeObjectProvider<MyApplication>();

		private static readonly ThreadSafeObjectProvider<User> m_UserObjectProvider = new ThreadSafeObjectProvider<User>();

		private static ThreadSafeObjectProvider<MyForms> m_MyFormsObjectProvider = new ThreadSafeObjectProvider<MyForms>();

		private static readonly ThreadSafeObjectProvider<MyWebServices> m_MyWebServicesObjectProvider = new ThreadSafeObjectProvider<MyWebServices>();

		[HelpKeyword("My.Computer")]
		internal static MyComputer Computer
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_ComputerObjectProvider.GetInstance;
			}
		}

		[HelpKeyword("My.Application")]
		internal static MyApplication Application
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_AppObjectProvider.GetInstance;
			}
		}

		[HelpKeyword("My.User")]
		internal static User User
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_UserObjectProvider.GetInstance;
			}
		}

		[HelpKeyword("My.Forms")]
		internal static MyForms Forms
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_MyFormsObjectProvider.GetInstance;
			}
		}

		[HelpKeyword("My.WebServices")]
		internal static MyWebServices WebServices
		{
			[DebuggerHidden]
			get
			{
				return MyProject.m_MyWebServicesObjectProvider.GetInstance;
			}
		}
	}
}
