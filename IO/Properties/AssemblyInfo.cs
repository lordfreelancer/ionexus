using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: AssemblyTitle("BEA-Annual IO Data")]
[assembly: AssemblyDescription("IO-Snap")]
[assembly: AssemblyCompany("IO-Snap")]
[assembly: AssemblyProduct("IO-Snap is a software application with many analytical features including all aspects of fundamental input-output analyses.")]
[assembly: AssemblyCopyright("Copyright © 2011 West Virginia University")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("2f16a107-b440-44ec-bf89-14ec6d1fc233")]
[assembly: AssemblyFileVersion("1.0.1.2")]
[assembly: NeutralResourcesLanguage("en")]
[assembly: AssemblyVersion("1.0.1.2")]
